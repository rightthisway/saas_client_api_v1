package com.rtf.client.signup.util;

public class MessageConstant {
	
	
	public static final String PARAM_PFM= "pfm";
	
	public static final String HTTP_RESP =  "ottresp";
	
	public static final String  MSG_SUCCESS = "Sucess";
	public static final String  MSG_CLIENT_COMMONM_ERR = "Something went wrong. Please try again sometime later";
	public static final String  MSG_ERROR_PROCESS= "Processing Error";
	
	public static final String INVALID_IP = "Invalid Ip address.";
	public static final String INVALID_CLIENT_ID = "Invalid client id.";
	
	public static final String INVALID_EMAIL = "Enter Valid Email.";
	public static final String INVALID_PWD = "Enter Valid Password. " ;
	public static final String INVALID_RESETPWD_TOK="Invalid reset password token.";
	public static final String MANDATORY_PWD = "Password is Mandatory.";
	public static final String PWD_MAX_CHAR =  "Password can have maximum 12 letter.";
	public static final String CUST_NOT_REGISTERED = "Customer is not registered";
	public static final String ERR_PWD_RESET = "Unaable to validate details.";
	public static final String ERR_PWD_RESET_UPD = "Unaable to Process Password Reset at this moment. Pease Try agaiin Later.";
	public static final String PWD_RESET_SUCCESS = "Password updated successfully.";
	public static final String PWD_VAL_MSG =  "Password should be minimum 4 characters either a digit, character or alphanumeric:";
	public static final String USER_SIGNEDUP_SUCCESS = "Signed up successfully. Your user id is : ";
	public static final String  EMAIL_NOT_AVAIL = "This email is already registered. Please choose a different email:";
	
	
	public static final String EMAIN_NOT_REGISTERED = "This Email id is not registered.";
	public static final String INVALID_EMAIL_PWD= "Email id or Password does not match.";
	public static final String INVALID_EXT_TOKEN="Invalid external token.";
	
	public static final String MANDATORY_SIGNUP_TYPE =  "Singup type is mandatory:";
	public static final String INVALID_USER_ID = "Invalid userid:";
	
	public static final String INVALID_SCHOOL_NAME = "Enter Valid School Name:";
	public static final String ERROR_EMAILRESETLINK ="Error occured while generating password reset link.";
	public static final String  PWD_RESET_OK="Password reset instruction sent on email.";
	
	// GENERAL 
	public static final String FACEBOOK = "FACEBOOK";
	public static final String GOOGLE = "GOOGLE";
	public static final String NORMAL_LOGIN="NORMAL";
	public static final String  FROM_MAIL = "sales@rewardthefan.com";
	public static final String MAIL_RESET_MSG_SUB="Reset password instruction. ";
	
	
	public static final String MAIL_RESET_MSG_1="Please click on link to reset your password. LINK : ";
	
	

}
