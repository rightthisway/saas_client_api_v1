/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.client.signup.util;

import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 * The Class MailManager.
 */
public final class MailManager {

	/**
	 * The Class MailAuthenticator.
	 */
	static class MailAuthenticator extends Authenticator {

		/** The username. */
		private final String username;

		/** The password. */
		private final String password;

		/**
		 * Instantiates a new mail authenticator.
		 *
		 * @param username the username
		 * @param password the password
		 */
		public MailAuthenticator(String username, String password) {
			this.username = username;
			this.password = password;
		}

		/**
		 * Gets the password authentication.
		 *
		 * @return the password authentication
		 */
		@Override
		public PasswordAuthentication getPasswordAuthentication() {
			return new PasswordAuthentication(username, password);
		}
	}

	/**
	 * Generic Method used to Send Email
	 *
	 * @param to      the to
	 * @param from    the from
	 * @param subject the subject
	 * @param text    the text
	 * @throws Exception the exception
	 */
	public static void sendMailNow(String to, String from, String subject, String text) throws Exception {
		// Get the session object
		Properties props = System.getProperties();
		props.put("mail.smtp.host", "mail.smtp2go.com");
		props.put("mail.smtp.port", 587);
		props.put("mail.transport.protocol", "smtp");
		props.put("mail.smtp.connectiontimeout", 30000);
		props.put("mail.smtp.timeout", 30000);
		props.put("mail.smtp.socketFactory.port", 587);
		props.put("mail.smtp.socketFactory.fallback", "false");
		props.put("mail.smtp.quitwait", "false");

		Session session;
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.auth", "true");

		Authenticator auth = new MailAuthenticator("sales@rewardthefan.com", "@Het12can");
		session = Session.getInstance(props, auth);

		// compose the message
		try {
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress(from));
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
			message.setSubject(subject);
			message.setText(text);
			// Send message
			Transport.send(message);
		} catch (Exception mex) {
			mex.printStackTrace();
			throw mex;
		}
	}
}
