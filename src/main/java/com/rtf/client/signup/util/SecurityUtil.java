/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.client.signup.util;

import java.security.MessageDigest;
import java.util.Base64;
import java.util.Base64.Decoder;
import java.util.Base64.Encoder;

import org.apache.commons.codec.binary.Hex;

import com.rtf.saas.exception.SaasProcessException;
import com.rtf.saas.util.GenUtil;

/**
 * The Class SecurityUtil.
 */
public class SecurityUtil {

	/**
	 * Decrypt  any Given string.
	 *
	 * @param text the text
	 * @return the string
	 */
	public static String decryptString(String text) {
		try {
			Decoder decoder = Base64.getDecoder();
			return new String(decoder.decode(text));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Encrypt  any Given string.
	 *
	 * @param password the password
	 * @return the string
	 */
	public static String encryptPasswordString(String password) throws Exception {

		String retValue = null;
		if (!GenUtil.isNullOrEmpty(password)) {
			MessageDigest algorithm;
			try {
				algorithm = MessageDigest.getInstance("SHA-1");
				byte[] digest = algorithm.digest(password.getBytes());
				retValue = new String(Hex.encodeHex(digest));
			} catch (Exception e) {
				throw new SaasProcessException(e.getLocalizedMessage());
			}

		}

		return retValue;
	}

	/**
	 * Encrypt string.
	 *
	 * @param text the text
	 * @return the string
	 */
	public static String encryptString(String text) {
		try {
			Encoder encoder = Base64.getEncoder();
			return encoder.encodeToString(text.getBytes());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return text;
	}
}
