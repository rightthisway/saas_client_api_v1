/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.client.signup.service;

import java.util.Random;

import javax.servlet.ServletContext;

import com.rtf.client.signup.dvo.CustomerPasswordResetDVO;
import com.rtf.common.dao.ClientCustomerDAO;
import com.rtf.common.dao.CustomerPasswordResetDAO;
import com.rtf.common.dvo.ClientConfigDVO;
import com.rtf.common.dvo.ClientCustomerDVO;
import com.rtf.common.service.ClientConfigService;
import com.rtf.livt.util.LivtContestUtil;

/**
 * The Class CustomerSignupService.
 */
public class CustomerSignupService {

	/**
	 * Method used to Generate user id for a Client .
	 *
	 * @param clientCustoemr the client custoemr
	 * @return the boolean
	 * @throws Exception the exception
	 */
	public static Boolean generateUserId(ClientCustomerDVO clientCustoemr) throws Exception {

		try {
			String nameArr[] = clientCustoemr.getEmail().split("@");
			if (nameArr.length >= 1) {
				String tempuserId = nameArr[0];
				boolean isunqueUserId = false;
				String existingCustoemr = null;
				Random random = null;
				while (!isunqueUserId) {
					existingCustoemr = ClientCustomerDAO.getClientCustomerUserId(clientCustoemr.getClId(), tempuserId);
					if (existingCustoemr == null) {
						clientCustoemr.setUserId(tempuserId);
						clientCustoemr.setFname(tempuserId);
						isunqueUserId = true;
						break;
					} else {
						random = new Random();
						tempuserId = nameArr[0] + random.nextInt(9) + random.nextInt(9) + random.nextInt(9);
					}
				}
				clientCustoemr.setUserId(tempuserId);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return true;
	}
	
	
	
	/**
	 * Method used to Generate new user id for Customer.
	 *
	 * @param clientCustoemr the client customer
	 * @return the boolean
	 * @throws Exception the exception
	 */
	
	public static Boolean generateNewUserId(ClientCustomerDVO clientCustoemr,ServletContext appCtx,String coId) throws Exception {
		String userId = "";
		try {
			
			
			/*
			 * ClientConfigDVO dvo =
			 * ClientConfigService.getContestConfigByKey(clientCustoemr.getClId(), "LVT",
			 * "IS_USERID_AUTO_GENERATED"); if(dvo == null ||
			 * dvo.getValue().equalsIgnoreCase("NO")) {
			 * clientCustoemr.setUserId(clientCustoemr.getCuId()); return true; }
			 */
			
			
			/*
			 * if("RTFeCOMM".equalsIgnoreCase(clientCustoemr.getClId()) ||
			 * "AMIT202002CID".equalsIgnoreCase(clientCustoemr.getClId()) ) {
			 * clientCustoemr.setUserId(clientCustoemr.getCuId()); return true; }
			 */
			 			
			
			String prefix = LivtContestUtil.getUserIdPrefix(clientCustoemr.getClId());
			if(prefix == null){
				prefix = "";
			}
			
			userId = prefix;
			Long rndNum = LivtContestUtil.getNextUserIdForClient(clientCustoemr.getClId(),appCtx,coId);
			userId += rndNum;
			System.out.println("UID GENERATED :    PREFIX = "+prefix);
			System.out.println("UID GENERATED :    RDMNUM = "+rndNum);
			clientCustoemr.setUserId(userId);
			System.out.println("UID GENERATED :    UID = "+userId);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return true;
	}

	/**
	 * Gets the Fetyches the Customer Reset Pasword Record for given Client and Customer Id 
	 *
	 * @param clId   the cl id
	 * @param custId the cust id
	 * @param token  the token
	 * @return the cust pass reset record
	 * @throws Exception the exception
	 */
	public static CustomerPasswordResetDVO getCustPassResetRecord(String clId, String custId, String token)
			throws Exception {
		CustomerPasswordResetDVO dvo = null;
		try {
			dvo = CustomerPasswordResetDAO.getCustPassResetRecord(clId, custId, token);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return dvo;
	}

	/**
	 * Save customer password reset record.
	 *
	 * @param reset the reset
	 * @return the boolean
	 * @throws Exception the exception
	 */
	public static Boolean saveCustPassResetRecord(CustomerPasswordResetDVO reset) throws Exception {
		Boolean isSaved = false;
		try {
			isSaved = CustomerPasswordResetDAO.saveCustPassResetRecord(reset);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return isSaved;
	}

	/**
	 * Update customer password reset record.
	 *
	 * @param reset the reset
	 * @return the boolean
	 * @throws Exception the exception
	 */
	public static Boolean updateCustPassResetRecord(CustomerPasswordResetDVO reset) throws Exception {
		Boolean isUpdated = false;
		try {
			isUpdated = CustomerPasswordResetDAO.updateCustPassResetRecord(reset);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return isUpdated;
	}

}
