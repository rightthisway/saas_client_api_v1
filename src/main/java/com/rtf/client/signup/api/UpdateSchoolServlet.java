/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.client.signup.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;

import com.rtf.client.signup.dto.CustomerLoginDTO;
import com.rtf.client.signup.util.MessageConstant;
import com.rtf.common.dao.ClientCustomerDAO;
import com.rtf.common.dvo.ClientCustomerDVO;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GenUtil;
import com.rtf.saas.util.GsonUtil;

/**
 * The Class UpdateSchoolServlet.
 */
@WebServlet("/updateschool.json")
public class UpdateSchoolServlet extends RtfSaasBaseServlet {

	/**
	 *
	 */
	private static final long serialVersionUID = -5891436274319477785L;

	/**
	 *  Generate Http Response for Client.Response, data is sent in JSON format.
	 *
* @param request        the  HttpServlet request
* @param response        the  HttpServlet response
	 * @param rtfSaasBaseDTO RTF Base Data Transfer Object
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("ottresp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();
	}

	/**
	 * Process request.Method used to login customers of  Schools Clients playing on the web view.
	 *
	 * @param request  the HttpServletRequest
	 * @param response the HttpServletResponse
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String emailStr = request.getParameter("email");
		String schoolName = request.getParameter("schoolName");
		String clientId = request.getParameter("clId");
		CustomerLoginDTO respDTO = new CustomerLoginDTO();
		respDTO.setSts(0);
		Timestamp stTime = GenUtil.getCurrentTimeStamp();
		String actMsg =StringUtils.EMPTY;		
		try {
			if (emailStr == null || emailStr.isEmpty()) {
				actMsg = MessageConstant.INVALID_EMAIL;
				setClientMessage(respDTO, actMsg, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (schoolName == null || schoolName.isEmpty()) {
				actMsg = "Select Valid School Name.";
				setClientMessage(respDTO, "Select Valid School Name.", null);
				generateResponse(request, response, respDTO);
				return;
			}

			if (clientId == null || clientId.isEmpty()) {
				actMsg = MessageConstant.INVALID_CLIENT_ID;
				setClientMessage(respDTO, MessageConstant.INVALID_CLIENT_ID, null);
				generateResponse(request, response, respDTO);
				return;
			}
			ClientCustomerDVO clientCustoemr = ClientCustomerDAO.getClientCustomerWithPasswordByCustomerId(clientId,
					emailStr);
			if (clientCustoemr == null) {
				actMsg = MessageConstant.EMAIN_NOT_REGISTERED;
				setClientMessage(respDTO,actMsg, null);
				generateResponse(request, response, respDTO);
				return;
			} else {
				respDTO.setSts(1);
				clientCustoemr.setUpldate(new Date());
				clientCustoemr.setSchoolName(schoolName);
				ClientCustomerDAO.updateClientCustomerSchoolName(clientCustoemr);
			}
			respDTO.setClId(clientCustoemr.getClId());
			respDTO.setCuID(clientCustoemr.getCuId());
			respDTO.setUseremail(clientCustoemr.getEmail());
			respDTO.setUserId(clientCustoemr.getUserId());
			respDTO.setSchname(clientCustoemr.getSchoolName());
			generateResponse(request, response, respDTO);
			actMsg=MessageConstant.MSG_SUCCESS;
			return;

		} catch (Exception e) {
			actMsg = MessageConstant.MSG_ERROR_PROCESS;
			respDTO.setSts(0);
			e.printStackTrace();
			setClientMessage(respDTO, MessageConstant.MSG_CLIENT_COMMONM_ERR, null);
			generateResponse(request, response, respDTO);
		} finally {
			/*
			 * actMsg = actMsg + ":" + emailStr + ":" + schoolName;
			 * SaaSApiTrackingService.insertApiTrackingDetails(respDTO.getClId(),
			 * respDTO.getCuID(), PluginProdTypeEnums.LOGIN.name(),
			 * GenUtil.getServPath(request), respDTO.getCoId(), actMsg,
			 * GenUtil.getIp(request), "pfm", "sessionId", GenUtil.getDesc(request), //
			 * session & description stTime, GenUtil.getCurrentTimeStamp(), // Start Time &
			 * End Time 1, respDTO.getSts(), deviceInfo); // Cluster Node Id & response
			 * status
			 */		}
		return;
	}
}
