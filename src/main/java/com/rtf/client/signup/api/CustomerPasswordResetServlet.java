/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.client.signup.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;

import com.rtf.client.signup.dvo.CustomerPasswordResetDVO;
import com.rtf.client.signup.service.CustomerSignupService;
import com.rtf.client.signup.util.MessageConstant;
import com.rtf.client.signup.util.SecurityUtil;
import com.rtf.common.dao.ClientCustomerDAO;
import com.rtf.common.dvo.ClientCustomerDVO;
import com.rtf.common.service.SaaSApiTrackingService;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GenUtil;
import com.rtf.saas.util.GsonUtil;
import com.rtf.saas.util.PluginProdTypeEnums;

/**
 * The Class CustomerPasswordResetServlet.
 */
@WebServlet("/saasresetpassword.json")
public class CustomerPasswordResetServlet extends RtfSaasBaseServlet {

	/**
	 *
	 */
	private static final long serialVersionUID = 5909076649577343206L;

	/**
	 *  Generate Http Response for Client.Response, data is sent in JSON format.
	 *
* @param request        the  HttpServlet request
* @param response        the  HttpServlet response
	 * @param rtfSaasBaseDTO RTF Base Data Transfer Object
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("signresp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();
	}

	/**
	 * Process request.Method is used to Reset Customers Password Based on Token sent in Email during Reset of Password
	 *
	 * @param request  the HttpServletRequest
	 * @param response the HttpServletResponse
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		RtfSaasBaseDTO respDTO = new RtfSaasBaseDTO();
		Timestamp stTime = GenUtil.getCurrentTimeStamp();
		String actMsg =StringUtils.EMPTY;
		String deviceInfo = StringUtils.EMPTY;
		String clId = null;
		String token = request.getParameter("token");
		String pwd = request.getParameter("pwd");
		try {
			respDTO.setSts(0);
			if (token == null || token.isEmpty()) {
				actMsg = MessageConstant.INVALID_RESETPWD_TOK;
				setClientMessage(respDTO, actMsg, null);
				generateResponse(request, response, respDTO);
				return;
			}

			String tok[] = token.split("~~");
			if (tok.length < 3) {
				actMsg = MessageConstant.INVALID_RESETPWD_TOK;
				setClientMessage(respDTO, actMsg, null);
				generateResponse(request, response, respDTO);
				return;
			}

			clId = SecurityUtil.decryptString(tok[0]);
			String custId = SecurityUtil.decryptString(tok[1]);
			String uuid = tok[2];

			if (pwd == null || pwd.isEmpty()) {
				actMsg = MessageConstant.MANDATORY_PWD;
				setClientMessage(respDTO, actMsg, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (pwd.length() > 12) {
				actMsg =MessageConstant.PWD_MAX_CHAR;
				setClientMessage(respDTO, actMsg, null);
				generateResponse(request, response, respDTO);
				return;
			}
			ClientCustomerDVO cust = ClientCustomerDAO.getClientCustomerByCustomerId(clId, custId);
			if (cust == null) {
				actMsg = MessageConstant.CUST_NOT_REGISTERED;
				setClientMessage(respDTO, actMsg, null);
				generateResponse(request, response, respDTO);
				return;
			}
			CustomerPasswordResetDVO reset = CustomerSignupService.getCustPassResetRecord(clId, custId, uuid);
			if (reset == null || (reset.getIsValidated() != null && reset.getIsValidated())) {
				actMsg = MessageConstant.ERR_PWD_RESET;
				setClientMessage(respDTO, actMsg, null);
				generateResponse(request, response, respDTO);
				return;
			}

			reset.setIsValidated(true);
			boolean isUpdate = CustomerSignupService.updateCustPassResetRecord(reset);
			if (!isUpdate) {
				actMsg = MessageConstant.ERR_PWD_RESET_UPD;
				setClientMessage(respDTO, actMsg, null);
				generateResponse(request, response, respDTO);
				return;
			}
			cust.setPassword(SecurityUtil.encryptPasswordString(pwd));
			isUpdate = ClientCustomerDAO.updateClientCustomerPassword(cust);
			if (!isUpdate) {
				actMsg =MessageConstant.ERR_PWD_RESET_UPD;
				setClientMessage(respDTO, actMsg, null);
				generateResponse(request, response, respDTO);
				return;
			}

			respDTO.setSts(1);
			respDTO.setMsg(MessageConstant.PWD_RESET_SUCCESS);
			generateResponse(request, response, respDTO);
			return;
		} catch (Exception e) {
			actMsg = MessageConstant.MSG_CLIENT_COMMONM_ERR;
			e.printStackTrace();
			setClientMessage(respDTO, actMsg, null);
			generateResponse(request, response, respDTO);
			return;
		} finally {
			/*
			 * SaaSApiTrackingService.insertApiTrackingDetails(clId, respDTO.getCuID(),
			 * PluginProdTypeEnums.LOGIN.name(), GenUtil.getServPath(request),
			 * respDTO.getCoId(), actMsg, GenUtil.getIp(request), "pfm", "sessionId",
			 * GenUtil.getDesc(request), // session & description stTime,
			 * GenUtil.getCurrentTimeStamp(), // Start Time & End Time 1, respDTO.getSts(),
			 * deviceInfo); // Cluster Node Id & response status
			 */		}

	}

}
