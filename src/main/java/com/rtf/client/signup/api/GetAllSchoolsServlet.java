/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.client.signup.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rtf.client.signup.dto.SchoolsListDTO;
import com.rtf.client.signup.util.MessageConstant;
import com.rtf.common.dao.SchoolsListDAO;
import com.rtf.common.dvo.SchoolsListDVO;
import com.rtf.common.util.SchoolsListComparator;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GsonUtil;

/**
 * The Class GetAllSchoolsServlet.
 */
@WebServlet("/getschoolslist.json")
public class GetAllSchoolsServlet extends RtfSaasBaseServlet {

	/**
	 *
	 */
	private static final long serialVersionUID = -577905065046990187L;

	/**
	 *  Generate Http Response for Client.Response, data is sent in JSON format.
	 *
* @param request        the  HttpServlet request
* @param response        the  HttpServlet response
	 * @param rtfSaasBaseDTO RTF Base Data Transfer Object
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("ottresp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();
	}

	/**
	 * Process request.Method to fetchj Listing of all Schools from DataBase Records
	 *
	 * @param request  the HttpServletRequest
	 * @param response the HttpServletResponse
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		SchoolsListDTO respDTO = new SchoolsListDTO();
		respDTO.setSts(0);
		try {
			List<SchoolsListDVO> list = SchoolsListDAO.getAllActiveSchoolsList();
			if (list != null && !list.isEmpty()) {
				Collections.sort(list, new SchoolsListComparator());
			}
			respDTO.setList(list);
			respDTO.setSts(1);
			generateResponse(request, response, respDTO);
			return;
		} catch (Exception e) {
			respDTO.setSts(0);
			e.printStackTrace();
			setClientMessage(respDTO, MessageConstant.MSG_CLIENT_COMMONM_ERR, null);
			generateResponse(request, response, respDTO);
		}
		return;
	}

}
