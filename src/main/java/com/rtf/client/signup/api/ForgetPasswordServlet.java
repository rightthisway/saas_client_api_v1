/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.client.signup.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;

import com.rtf.client.signup.dvo.CustomerPasswordResetDVO;
import com.rtf.client.signup.service.CustomerSignupService;
import com.rtf.client.signup.util.MailManager;
import com.rtf.client.signup.util.MessageConstant;
import com.rtf.client.signup.util.SecurityUtil;
import com.rtf.common.dao.ClientCustomerDAO;
import com.rtf.common.dvo.ClientCustomerDVO;
import com.rtf.common.dvo.ConfigDVO;
import com.rtf.common.service.FirestoreConfigService;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GenUtil;
import com.rtf.saas.util.GsonUtil;

/**
 * The Class ForgetPasswordServlet.
 */
@WebServlet("/forgetpassword.json")
public class ForgetPasswordServlet extends RtfSaasBaseServlet {

	/**
	 *
	 */
	private static final long serialVersionUID = 7429317790103818279L;

	/**
	 *  Generate Http Response for Client.Response, data is sent in JSON format.
	 *
* @param request        the  HttpServlet request
* @param response        the  HttpServlet response
	 * @param rtfSaasBaseDTO RTF Base Data Transfer Object
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("ottresp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();

	}

	/**
	 * Process request.Method Used to Generate Forgot Password Link . Reset Link wil be sent in email to customer's mail Id
	 *
	 * @param request  the HttpServletRequest
	 * @param response the HttpServletResponse
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		RtfSaasBaseDTO respDTO = new RtfSaasBaseDTO();

		Timestamp stTime = GenUtil.getCurrentTimeStamp();
		String actMsg =StringUtils.EMPTY;	
		String clId = request.getParameter("clId");
		String email = request.getParameter("email");

		try {

			respDTO.setSts(0);
			respDTO.setClId(clId);

			if (clId == null || clId.isEmpty()) {
				setClientMessage(respDTO, MessageConstant.INVALID_CLIENT_ID, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (email == null || email.isEmpty()) {
				setClientMessage(respDTO, MessageConstant.INVALID_CLIENT_ID, null);
				generateResponse(request, response, respDTO);
				return;
			}
			email = email.toLowerCase();

			String regex = "^(.+)@(.+)$";
			Pattern pattern = Pattern.compile(regex);
			Matcher match = pattern.matcher(email);
			if (!match.matches()) {
				setClientMessage(respDTO, "Please enter valid email address.", null);
				generateResponse(request, response, respDTO);
				return;
			}
			ClientCustomerDVO cust = ClientCustomerDAO.getClientCustomerByEmail(clId, email);
			if (cust == null) {
				setClientMessage(respDTO, "Entered email is not registered in system.", null);
				generateResponse(request, response, respDTO);
				return;
			}
			ConfigDVO config = null;
			if (cust.getFsConfId() != null && cust.getFsConfId() > 0) {
				config = FirestoreConfigService.getClientConfigforCustomer(clId, cust.getFsConfId());
			}

			if (config == null || config.getSaasweburl() == null || config.getSaasweburl().isEmpty()) {
				List<ConfigDVO> configs = FirestoreConfigService.getClientConfig(clId, null);
				if (configs == null || configs.isEmpty()) {
					setClientMessage(respDTO, "Customer details not found in system.", null);
					generateResponse(request, response, respDTO);
					return;
				}
				config = configs.get(0);
			}

			String token = UUID.randomUUID().toString();
			String queryString = SecurityUtil.encryptString(clId) + "~~" + SecurityUtil.encryptString(email) + "~~"
					+ token;

			CustomerPasswordResetDVO reset = new CustomerPasswordResetDVO();
			reset.setBaseUrl(config.getSaasweburl());
			reset.setClId(clId);
			reset.setCrDate(new Date());
			reset.setCustEmail(cust.getEmail());
			reset.setCustId(cust.getCuId());
			reset.setIsValidated(false);
			reset.setToken(token);
			reset.setValidateDate(null);

			Boolean isSaved = CustomerSignupService.saveCustPassResetRecord(reset);
			if (!isSaved) {
				setClientMessage(respDTO, "Error occured while generating password reset link.", null);
				generateResponse(request, response, respDTO);
				return;
			}

			String text = "Please click on link to reset your password. LINK : " + reset.getBaseUrl() + "#/reset?token="
					+ queryString;

			try {
				MailManager.sendMailNow(cust.getEmail(), "sales@rewardthefan.com", "Your password for Trivia Game.",
						text);
			} catch (Exception e) {
				e.printStackTrace();
				setClientMessage(respDTO, "Something went wrong while sending email, please try again later.", null);
				generateResponse(request, response, respDTO);
				return;
			}
			respDTO.setSts(1);
			respDTO.setClId(clId);
			respDTO.setMsg("Password reset instruction sent on email.");
			generateResponse(request, response, respDTO);
			return;
		} catch (Exception e) {
			e.printStackTrace();
			setClientMessage(respDTO, "Something went wrong, please try again later.", null);
			generateResponse(request, response, respDTO);
			return;
		} finally {

			/*
			 * SaaSApiTrackingService.insertApiTrackingDetails(clId, respDTO.getCuID(),
			 * PluginProdTypeEnums.LOGIN.name(), GenUtil.getServPath(request),
			 * respDTO.getCoId(), actMsg, GenUtil.getIp(request), "pfm", "sessionId",
			 * GenUtil.getDesc(request), // session & description stTime,
			 * GenUtil.getCurrentTimeStamp(), // Start Time & End Time 1, respDTO.getSts(),
			 * deviceInfo); // Cluster Node Id & response status
			 */		}

	}

}
