/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.client.signup.api;

import java.io.IOException;
import java.sql.Timestamp;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;

import com.rtf.client.signup.dto.CustomerLoginDTO;
import com.rtf.client.signup.util.MessageConstant;
import com.rtf.client.signup.util.SecurityUtil;
import com.rtf.common.dao.ClientCustomerDAO;
import com.rtf.common.dvo.ClientCustomerDVO;
import com.rtf.common.service.SaaSApiTrackingService;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GenUtil;
import com.rtf.saas.util.PluginProdTypeEnums;

/**
 * The Class CustomerLoginServlet.
 */
@WebServlet("/clientcustomerlogin.json")
public class CustomerLoginServlet extends RtfSaasBaseServlet {

	private static final long serialVersionUID = -7199517383973337528L;

	/**
	 * Generate Http Response for Client.Response, data is sent in JSON format.
	 *
	 * @param request        the Http request
	 * @param response       the Http  response
	 * @param rtfSaasBaseDTO  RTF base DTO
	 * @throws ServletException  servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			final RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {			
		generateCommonAPIResponse(request,response,rtfSaasBaseDTO,MessageConstant.HTTP_RESP);
		
		/*
		 * Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		 * map.put(MessageConstant.HTTP_RESP, rtfSaasBaseDTO); String ottrespStr =
		 * GsonUtil.getJasksonObjMapper().writeValueAsString(map); PrintWriter out =
		 * response.getWriter(); response.setContentType("application/json");
		 * response.setCharacterEncoding("UTF-8");
		 * response.addHeader("Access-Control-Allow-Origin", "*");
		 * response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		 * response.addHeader("Access-Control-Allow-Headers",
		 * "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		 * out.print(ottrespStr); out.flush(); out.close();
		 */
	}

	/**
	 * 
	 * This Method validates the User's Login Password based credentials. 
	 * @param request  the http request
	 * @param response the http response
	 * @throws ServletException  servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String emailStr = request.getParameter("email");
		String pwd = request.getParameter("pwd");
		String clientId = request.getParameter("clId");
		CustomerLoginDTO respDTO = new CustomerLoginDTO();
		respDTO.setSts(0);	
		String actMsg =StringUtils.EMPTY;	
		try {
			if (emailStr == null || emailStr.isEmpty()) {
				actMsg = MessageConstant.INVALID_EMAIL;
				setClientMessage(respDTO, MessageConstant.INVALID_EMAIL, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (pwd == null || pwd.isEmpty()) {
				actMsg = MessageConstant.INVALID_PWD;
				setClientMessage(respDTO, MessageConstant.INVALID_PWD, null);
				generateResponse(request, response, respDTO);
				return;
			}
			
			ClientCustomerDVO clientCustoemr = ClientCustomerDAO.getClientCustomerWithPasswordByCustomerId(clientId,
					emailStr);
			if (clientCustoemr == null) {
				actMsg =MessageConstant.EMAIN_NOT_REGISTERED;
				setClientMessage(respDTO,MessageConstant.EMAIN_NOT_REGISTERED, null);
				generateResponse(request, response, respDTO);
				return;
			} else {
				if (!clientCustoemr.getPassword().equals(SecurityUtil.encryptPasswordString(pwd))) {
					actMsg = MessageConstant.INVALID_EMAIL_PWD;
					setClientMessage(respDTO, MessageConstant.INVALID_EMAIL_PWD, null);
					generateResponse(request, response, respDTO);
					return;
				}
				respDTO.setSts(1);
				setClientMessage(respDTO, null, "");
			}

			respDTO.setClId(clientCustoemr.getClId());
			respDTO.setCuID(clientCustoemr.getCuId());
			respDTO.setUseremail(clientCustoemr.getEmail());
			respDTO.setUserId(clientCustoemr.getUserId());
			respDTO.setSchname(clientCustoemr.getSchoolName());
			actMsg = MessageConstant.MSG_SUCCESS;
			generateResponse(request, response, respDTO);
			return;

		} catch (Exception e) {
			actMsg = MessageConstant.MSG_ERROR_PROCESS;
			respDTO.setSts(0);
			e.printStackTrace();
			setClientMessage(respDTO, MessageConstant.MSG_CLIENT_COMMONM_ERR, null);
			generateResponse(request, response, respDTO);
		} finally {
			/*
			 * SaaSApiTrackingService.insertApiTrackingDetails(clientId, respDTO.getCuID(),
			 * PluginProdTypeEnums.LOGIN.name(), GenUtil.getServPath(request),
			 * respDTO.getCoId(), actMsg, GenUtil.getIp(request), MessageConstant.PARAM_PFM,
			 * "sessionId", GenUtil.getDesc(request), stTime, GenUtil.getCurrentTimeStamp(),
			 * 1, respDTO.getSts(), deviceInfo);
			 */
		}
		return;
	}

}
