/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.client.signup.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;

import com.rtf.client.signup.dto.CustomerLoginDTO;
import com.rtf.client.signup.service.CustomerSignupService;
import com.rtf.client.signup.util.MessageConstant;
import com.rtf.common.dao.ClientCustomerDAO;
import com.rtf.common.dao.ClientCustomerEmailVerifyDAO;
import com.rtf.common.dvo.ClientCustomerDVO;
import com.rtf.common.dvo.ClientCustomerEmailVerifyDVO;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GenUtil;
import com.rtf.saas.util.GsonUtil;

/**
 * The Class SaasUpdateSignupEmailServlet.
 */
@WebServlet("/saasupdateSignupEmail.json")
public class SaasUpdateSignupEmailServlet extends RtfSaasBaseServlet {

	/**
	 *
	 */
	private static final long serialVersionUID = 1461795220912730285L;

	/**
	 *  Generate Http Response for Client.Response, data is sent in JSON format.
	 *
* @param request        the  HttpServlet request
* @param response        the  HttpServlet response
	 * @param rtfSaasBaseDTO RTF Base Data Transfer Object
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("ottresp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();
	}

	/**
	 * Process request. Method is used to Onboard customer for a Client .
	 *
	 * @param request  the HttpServletRequest
	 * @param response the HttpServletResponse
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String emailStr = request.getParameter("email");
		String clientId = request.getParameter("clId");
		String signUpType = request.getParameter("signUpType");
		String tokenId = request.getParameter("tokenId");
		String fbUid = request.getParameter("fbuid");
		Timestamp stTime = GenUtil.getCurrentTimeStamp();
		String actMsg = StringUtils.EMPTY;	
		CustomerLoginDTO respDTO = new CustomerLoginDTO();
		respDTO.setSts(0);
		try {
			if (emailStr == null || emailStr.isEmpty()) {
				actMsg = MessageConstant.INVALID_EMAIL;
				setClientMessage(respDTO, actMsg, null);
				generateResponse(request, response, respDTO);
				return;
			}

			if (clientId == null || clientId.isEmpty()) {
				actMsg = MessageConstant.INVALID_CLIENT_ID;
				setClientMessage(respDTO, MessageConstant.INVALID_CLIENT_ID, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (signUpType == null || signUpType.isEmpty()) {
				actMsg = "Singup type is mendatory.";
				setClientMessage(respDTO, "Singup type is mendatory.", null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (fbUid == null || fbUid.isEmpty()) {
				actMsg = "Invalid userid.";
				setClientMessage(respDTO, "Invalid userid.", null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (!signUpType.equalsIgnoreCase("FACEBOOK")) {// && !signUpType.equalsIgnoreCase("GOOGLE")
				actMsg = "Invalid Signup Type.";
				setClientMessage(respDTO, "Invalid Signup Type.", null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (tokenId == null || tokenId.isEmpty()) {
				actMsg = "Invalid token it.";
				setClientMessage(respDTO, "Invalid token it.", null);
				generateResponse(request, response, respDTO);
				return;
			}

			ClientCustomerEmailVerifyDVO tempCustoemr = ClientCustomerEmailVerifyDAO
					.getClientCustomerEmailVerifyByCustomerIdandFbUserid(clientId, fbUid);
			if (tempCustoemr == null) {
				if (!emailStr.equalsIgnoreCase("withphone@g.com") && !emailStr.equalsIgnoreCase("withphone@g.com")) {
					actMsg = "Invalid token id.";
					setClientMessage(respDTO, "Invalid token id.", null);
					generateResponse(request, response, respDTO);
					return;
				}
			}

			ClientCustomerDVO clientCustoemr = ClientCustomerDAO.getClientCustomerWithPasswordByCustomerId(clientId,
					emailStr);
			if (clientCustoemr != null) {
				if (!emailStr.equalsIgnoreCase("withphone@g.com") && !emailStr.equalsIgnoreCase("withphone@g.com")) {
					actMsg = "Email Already exists.";
					setClientMessage(respDTO, "Email Already exists.", null);
					generateResponse(request, response, respDTO);
					return;
				}
			}

			clientCustoemr = new ClientCustomerDVO();
			clientCustoemr.setClId(clientId);
			clientCustoemr.setEmail(emailStr);
			clientCustoemr.setCuId(emailStr);
			clientCustoemr.setSignUpType(signUpType);
			clientCustoemr.setUpldate(new Date());
			clientCustoemr.setFbTokenId(tokenId);
			clientCustoemr.setFbUid(fbUid);

			if ((!emailStr.equalsIgnoreCase("withphone@g.com") && !emailStr.equalsIgnoreCase("withphone@g.com"))
					|| clientCustoemr.getUserId() == null) {
				CustomerSignupService.generateUserId(clientCustoemr);
			}
			String ce = String.format("%06d", new Random().nextInt(100000));
			clientCustoemr.setCe(ce);
			ClientCustomerDAO.saveClientCustomer(clientCustoemr);

			respDTO.setSts(1);
			setClientMessage(respDTO, null, "Signed up successfully. Your user id is : " + clientCustoemr.getUserId());
			actMsg=MessageConstant.MSG_SUCCESS;
			respDTO.setClId(clientCustoemr.getClId());
			respDTO.setCuID(clientCustoemr.getCuId());
			respDTO.setUseremail(clientCustoemr.getEmail());
			respDTO.setUserId(clientCustoemr.getUserId());
			respDTO.setCe(ce);
			generateResponse(request, response, respDTO);
			return;

		} catch (Exception e) {
			actMsg = MessageConstant.MSG_ERROR_PROCESS;
			respDTO.setSts(0);
			e.printStackTrace();
			setClientMessage(respDTO, MessageConstant.MSG_CLIENT_COMMONM_ERR, null);
			generateResponse(request, response, respDTO);
		} finally {
			/*
			 * actMsg = actMsg + ":" + emailStr + ":" + signUpType;
			 * SaaSApiTrackingService.insertApiTrackingDetails(respDTO.getClId(),
			 * respDTO.getCuID(), PluginProdTypeEnums.LOGIN.name(),
			 * GenUtil.getServPath(request), respDTO.getCoId(), actMsg,
			 * GenUtil.getIp(request), "pfm", "sessionId", GenUtil.getDesc(request), //
			 * session & description stTime, GenUtil.getCurrentTimeStamp(), // Start Time &
			 * End Time 1, respDTO.getSts(), deviceInfo); // Cluster Node Id & response
			 * status
			 */		}
		return;
	}

}
