/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.client.signup.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;

import com.rtf.client.signup.dto.CustomerLoginDTO;
import com.rtf.client.signup.service.CustomerSignupService;
import com.rtf.client.signup.util.MessageConstant;
import com.rtf.client.signup.util.SecurityUtil;
import com.rtf.common.dao.ClientCustomerDAO;
import com.rtf.common.dao.ClientCustomerEmailVerifyDAO;
import com.rtf.common.dvo.ClientCustomerDVO;
import com.rtf.common.dvo.ClientCustomerEmailVerifyDVO;
import com.rtf.common.service.SaaSApiTrackingService;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GenUtil;
import com.rtf.saas.util.GsonUtil;
import com.rtf.saas.util.PluginProdTypeEnums;

/**
 * The Class SaasCustSignUpServlet.
 */
@WebServlet("/saascustomersignup.json")
public class SaasCustSignUpServlet extends RtfSaasBaseServlet {

	/**
	 *
	 */
	private static final long serialVersionUID = 1649351626213625121L;

	/**
	 *  Generate Http Response for Client.Response, data is sent in JSON format.
	 *
* @param request        the  HttpServlet request
* @param response        the  HttpServlet response
	 * @param rtfSaasBaseDTO RTF Base Data Transfer Object
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("ottresp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();
	}

	/**
	 * Process request.Method used to On board Clients Customer from Web View.
	 *
	 * @param request  the HttpServletRequest
	 * @param response the HttpServletResponse
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String emailStr = request.getParameter("email");
		String pwd = request.getParameter("pwd");
		String clientId = request.getParameter("clId");
		String signUpType = request.getParameter("signUpType");
		String tokenId = request.getParameter("tokenId");
		String fbUid = request.getParameter("fbuid");
		Timestamp stTime = GenUtil.getCurrentTimeStamp();
		String actMsg = StringUtils.EMPTY;	
		String phone = null, googleToken = null, fbToken = null, fbuserId = null;
		CustomerLoginDTO respDTO = new CustomerLoginDTO();
		respDTO.setSts(0);
		try {

			if (clientId == null || clientId.isEmpty()) {
				actMsg =  MessageConstant.INVALID_CLIENT_ID;
				setClientMessage(respDTO, MessageConstant.INVALID_CLIENT_ID, null);
				generateResponse(request, response, respDTO);
				return;
			}

			if (signUpType == null || signUpType.isEmpty()) {
				actMsg = MessageConstant.MANDATORY_SIGNUP_TYPE;
				setClientMessage(respDTO, actMsg, null);
				generateResponse(request, response, respDTO);
				return;
			}

			boolean isNoEmailSignup = false;
			if (signUpType.equalsIgnoreCase(MessageConstant.FACEBOOK)) {
				pwd = null;
				if (tokenId == null || tokenId.isEmpty()) {
					actMsg = MessageConstant.INVALID_EXT_TOKEN;
					setClientMessage(respDTO,actMsg, null);
					generateResponse(request, response, respDTO);
					return;
				}
				if (fbUid == null || fbUid.isEmpty()) {
					actMsg = MessageConstant.INVALID_USER_ID;
					setClientMessage(respDTO, actMsg, null);
					generateResponse(request, response, respDTO);
					return;
				}
				if (emailStr == null || emailStr.isEmpty()) {
					// phone = phoneStr;
					isNoEmailSignup = true;
				}
				fbToken = tokenId;
				fbuserId = fbUid;

			} else if (signUpType.equalsIgnoreCase(MessageConstant.GOOGLE)) {
				if (emailStr == null || emailStr.isEmpty()) {
					actMsg = MessageConstant.INVALID_EMAIL;
					setClientMessage(respDTO, actMsg, null);
					generateResponse(request, response, respDTO);
					return;
				}
				if (tokenId == null || tokenId.isEmpty()) {
					actMsg = MessageConstant.INVALID_EXT_TOKEN;
					setClientMessage(respDTO, actMsg, null);
					generateResponse(request, response, respDTO);
					return;
				}
				googleToken = tokenId;
			} else {
				if (emailStr == null || emailStr.isEmpty()) {
					actMsg = MessageConstant.INVALID_EMAIL;
					setClientMessage(respDTO, actMsg, null);
					generateResponse(request, response, respDTO);
					return;
				}
				if (pwd == null || pwd.isEmpty()) {
					actMsg = MessageConstant.INVALID_PWD;
					setClientMessage(respDTO, actMsg, null);
					generateResponse(request, response, respDTO);
					return;
				}
				if (pwd.length() < 4) {
					actMsg = MessageConstant.PWD_VAL_MSG;
					setClientMessage(respDTO,
							MessageConstant.PWD_VAL_MSG, null);
					generateResponse(request, response, respDTO);
					return;
				}
			}

			if (!isNoEmailSignup) {
				ClientCustomerDVO clientCustoemr = ClientCustomerDAO.getClientCustomerWithPasswordByCustomerId(clientId,
						emailStr);
				if (clientCustoemr == null) {
					clientCustoemr = new ClientCustomerDVO();
					clientCustoemr.setClId(clientId);
					clientCustoemr.setEmail(emailStr);
					clientCustoemr.setCuId(emailStr);
					clientCustoemr.setSignUpType(signUpType);
					clientCustoemr.setUpldate(new Date());

					clientCustoemr.setTokenId(googleToken);
					clientCustoemr.setFbTokenId(fbToken);
					clientCustoemr.setPassword(SecurityUtil.encryptPasswordString(pwd));
					clientCustoemr.setFbUid(fbuserId);
					String ce = String.format("%06d", new Random().nextInt(100000));
					clientCustoemr.setCe(ce);
					CustomerSignupService.generateUserId(clientCustoemr);

					ClientCustomerDAO.saveClientCustomer(clientCustoemr);

					actMsg=MessageConstant.MSG_SUCCESS;
					respDTO.setSts(1);
					setClientMessage(respDTO, null,
							MessageConstant.USER_SIGNEDUP_SUCCESS + clientCustoemr.getUserId());
				} else {

					if (signUpType.equalsIgnoreCase(MessageConstant.NORMAL_LOGIN)) {
						actMsg =MessageConstant.EMAIL_NOT_AVAIL;
						setClientMessage(respDTO, actMsg,
								null);
						generateResponse(request, response, respDTO);
						return;
					} else {
						boolean isUpdated = false;
						if (signUpType.equalsIgnoreCase(MessageConstant.FACEBOOK)) {
							if ((clientCustoemr.getFbUid() != null && !clientCustoemr.getFbUid().equals(fbUid))) {
								actMsg = MessageConstant.INVALID_EXT_TOKEN;
								setClientMessage(respDTO, actMsg, null);
								generateResponse(request, response, respDTO);
								return;
							}
							if ((clientCustoemr.getFbTokenId() == null
									|| !clientCustoemr.getFbTokenId().equals(fbToken))
									|| clientCustoemr.getFbUid() == null || !clientCustoemr.getFbUid().equals(fbUid)) {
								clientCustoemr.setFbTokenId(fbToken);
								clientCustoemr.setFbUid(fbUid);

								isUpdated = true;
							}

						}
						if (signUpType.equalsIgnoreCase(MessageConstant.GOOGLE)) {
							if (clientCustoemr.getTokenId() == null
									|| !clientCustoemr.getTokenId().equals(googleToken)) {
								clientCustoemr.setTokenId(googleToken);
								isUpdated = true;
							}
						}

						if (isUpdated) {
							clientCustoemr.setUpldate(new Date());
							ClientCustomerDAO.updateClientCustomer(clientCustoemr);
						}
					}
				}
				actMsg=MessageConstant.MSG_SUCCESS;
				respDTO.setSts(1);
				respDTO.setClId(clientCustoemr.getClId());
				respDTO.setCuID(clientCustoemr.getCuId());
				respDTO.setUseremail(clientCustoemr.getEmail());
				respDTO.setUserId(clientCustoemr.getUserId());
				respDTO.setCe(clientCustoemr.getCe());

			} else {

				ClientCustomerDVO clientCustoemr = ClientCustomerDAO.getClientCustomerWithPasswordByFbUserId(clientId,
						fbUid);
				if (clientCustoemr == null) {
					ClientCustomerEmailVerifyDVO tempCustomer = new ClientCustomerEmailVerifyDVO();
					tempCustomer.setClId(clientId);
					tempCustomer.setPhone(phone);
					tempCustomer.setCuId(emailStr);
					tempCustomer.setFbTokenId(fbToken);
					tempCustomer.setFbUid(fbUid);
					ClientCustomerEmailVerifyDAO.saveClientCustomerEmailVerify(tempCustomer);
					respDTO.setSts(1);
					respDTO.setSem(Boolean.TRUE);
					setClientMessage(respDTO, null, "");
				} else {
					respDTO.setClId(clientCustoemr.getClId());
					respDTO.setCuID(clientCustoemr.getCuId());
					respDTO.setUseremail(clientCustoemr.getEmail());
					respDTO.setUserId(clientCustoemr.getUserId());
					respDTO.setCe(clientCustoemr.getCe());
					respDTO.setSts(1);
					setClientMessage(respDTO, null, "");
				}
			}
			actMsg=MessageConstant.MSG_SUCCESS;
			generateResponse(request, response, respDTO);
			return;

		} catch (Exception e) {
			actMsg = MessageConstant.MSG_ERROR_PROCESS;
			respDTO.setSts(0);
			e.printStackTrace();
			setClientMessage(respDTO, MessageConstant.MSG_CLIENT_COMMONM_ERR, null);
			generateResponse(request, response, respDTO);
		} finally {
			/*
			 * actMsg = actMsg + ":" + emailStr + ":" + signUpType;
			 * SaaSApiTrackingService.insertApiTrackingDetails(clientId, respDTO.getCuID(),
			 * PluginProdTypeEnums.LOGIN.name(), GenUtil.getServPath(request),
			 * respDTO.getCoId(), actMsg, GenUtil.getIp(request), "pfm", "sessionId",
			 * GenUtil.getDesc(request), // session & description stTime,
			 * GenUtil.getCurrentTimeStamp(), // Start Time & End Time 1, respDTO.getSts(),
			 * deviceInfo); // Cluster Node Id & response status
			 */		}
		return;
	}

}
