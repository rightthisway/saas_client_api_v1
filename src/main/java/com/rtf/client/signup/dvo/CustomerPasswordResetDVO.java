/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.client.signup.dvo;

import java.util.Date;

/**
 * The Class CustomerPasswordResetDVO.
 */
public class CustomerPasswordResetDVO {

	/** The cl id. */
	private String clId;

	/** The cust id. */
	private String custId;

	/** The cust email. */
	private String custEmail;

	/** The token. */
	private String token;

	/** The base url. */
	private String baseUrl;

	/** The cr date. */
	private Date crDate;

	/** The is validated. */
	private Boolean isValidated;

	/** The validate date. */
	private Date validateDate;

	/**
	 * Gets the base url.
	 *
	 * @return the base url
	 */
	public String getBaseUrl() {
		return baseUrl;
	}

	/**
	 * Gets the cl id.
	 *
	 * @return the cl id
	 */
	public String getClId() {
		return clId;
	}

	/**
	 * Gets the cr date.
	 *
	 * @return the cr date
	 */
	public Date getCrDate() {
		return crDate;
	}

	/**
	 * Gets the cust email.
	 *
	 * @return the cust email
	 */
	public String getCustEmail() {
		return custEmail;
	}

	/**
	 * Gets the cust id.
	 *
	 * @return the cust id
	 */
	public String getCustId() {
		return custId;
	}

	/**
	 * Gets the checks if is validated.
	 *
	 * @return the checks if is validated
	 */
	public Boolean getIsValidated() {
		return isValidated;
	}

	/**
	 * Gets the token.
	 *
	 * @return the token
	 */
	public String getToken() {
		return token;
	}

	/**
	 * Gets the validate date.
	 *
	 * @return the validate date
	 */
	public Date getValidateDate() {
		return validateDate;
	}

	/**
	 * Sets the base url.
	 *
	 * @param baseUrl the new base url
	 */
	public void setBaseUrl(String baseUrl) {
		this.baseUrl = baseUrl;
	}

	/**
	 * Sets the cl id.
	 *
	 * @param clId the new cl id
	 */
	public void setClId(String clId) {
		this.clId = clId;
	}

	/**
	 * Sets the cr date.
	 *
	 * @param crDate the new cr date
	 */
	public void setCrDate(Date crDate) {
		this.crDate = crDate;
	}

	/**
	 * Sets the cust email.
	 *
	 * @param custEmail the new cust email
	 */
	public void setCustEmail(String custEmail) {
		this.custEmail = custEmail;
	}

	/**
	 * Sets the cust id.
	 *
	 * @param custId the new cust id
	 */
	public void setCustId(String custId) {
		this.custId = custId;
	}

	/**
	 * Sets the checks if is validated.
	 *
	 * @param isValidated the new checks if is validated
	 */
	public void setIsValidated(Boolean isValidated) {
		this.isValidated = isValidated;
	}

	/**
	 * Sets the token.
	 *
	 * @param token the new token
	 */
	public void setToken(String token) {
		this.token = token;
	}

	/**
	 * Sets the validate date.
	 *
	 * @param validateDate the new validate date
	 */
	public void setValidateDate(Date validateDate) {
		this.validateDate = validateDate;
	}

}
