/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.client.signup.dto;

import com.rtf.saas.arch.RtfSaasBaseDTO;

/**
 * The Class CustomerLoginDTO.
 */
public class CustomerLoginDTO extends RtfSaasBaseDTO {
	/** The useremail. */
	private String useremail;

	/** The userfname. */
	private String userfname;

	/** The userlname. */
	private String userlname;

	/** The userphone. */
	private String userphone;

	/** The user id. */
	private String userId;

	/** The ssch. */
	private Boolean ssch = false;// show school name

	/** The sem. */
	private Boolean sem = false;// show email

	/** The schname. */
	private String schname;

	/** The ce. */
	private String ce;

	public CustomerLoginDTO() {

	}

	/**
	 * Gets the ce.
	 *
	 * @return the ce
	 */
	public String getCe() {
		return ce;
	}

	/**
	 * Gets the schname.
	 *
	 * @return the schname
	 */
	public String getSchname() {
		return schname;
	}

	/**
	 * Gets the sem.
	 *
	 * @return the sem
	 */
	public Boolean getSem() {
		if (sem == null) {
			sem = false;
		}
		return sem;
	}

	/**
	 * Gets the ssch.
	 *
	 * @return the ssch
	 */
	public Boolean getSsch() {
		if (ssch == null) {
			ssch = false;
		}
		return ssch;
	}

	/**
	 * Gets the useremail.
	 *
	 * @return the useremail
	 */
	public String getUseremail() {
		return useremail;
	}

	/**
	 * Gets the userfname.
	 *
	 * @return the userfname
	 */
	public String getUserfname() {
		return userfname;
	}

	/**
	 * Gets the user id.
	 *
	 * @return the user id
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * Gets the userlname.
	 *
	 * @return the userlname
	 */
	public String getUserlname() {
		return userlname;
	}

	/**
	 * Gets the userphone.
	 *
	 * @return the userphone
	 */
	public String getUserphone() {
		return userphone;
	}

	/**
	 * Sets the ce.
	 *
	 * @param ce the new ce
	 */
	public void setCe(String ce) {
		this.ce = ce;
	}

	/**
	 * Sets the schname.
	 *
	 * @param schname the new schname
	 */
	public void setSchname(String schname) {
		this.schname = schname;
	}

	/**
	 * Sets the sem.
	 *
	 * @param sem the new sem
	 */
	public void setSem(Boolean sem) {
		this.sem = sem;
	}

	/**
	 * Sets the ssch.
	 *
	 * @param ssch the new ssch
	 */
	public void setSsch(Boolean ssch) {

		this.ssch = ssch;
	}

	/**
	 * Sets the useremail.
	 *
	 * @param useremail the new useremail
	 */
	public void setUseremail(String useremail) {
		this.useremail = useremail;
	}

	/**
	 * Sets the userfname.
	 *
	 * @param userfname the new userfname
	 */
	public void setUserfname(String userfname) {
		this.userfname = userfname;
	}

	/**
	 * Sets the user id.
	 *
	 * @param userId the new user id
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * Sets the userlname.
	 *
	 * @param userlname the new userlname
	 */
	public void setUserlname(String userlname) {
		this.userlname = userlname;
	}

	/**
	 * Sets the userphone.
	 *
	 * @param userphone the new userphone
	 */
	public void setUserphone(String userphone) {
		this.userphone = userphone;
	}

}
