/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.client.signup.dto;

import java.util.List;

import com.rtf.common.dvo.SchoolsListDVO;
import com.rtf.saas.arch.RtfSaasBaseDTO;

/**
 * The Class SchoolsListDTO.
 */
public class SchoolsListDTO extends RtfSaasBaseDTO {
	/** The list. */
	List<SchoolsListDVO> list;

	public SchoolsListDTO() {

	}

	/**
	 * Gets the list.
	 *
	 * @return the list
	 */
	public List<SchoolsListDVO> getList() {
		return list;
	}

	/**
	 * Sets the list.
	 *
	 * @param list the new list
	 */
	public void setList(List<SchoolsListDVO> list) {
		this.list = list;
	}

}
