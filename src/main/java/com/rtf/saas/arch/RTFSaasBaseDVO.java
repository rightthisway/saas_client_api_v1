/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.saas.arch;

import java.util.Date;

import com.rtf.saas.util.DateFormatUtil;

/**
 * The Class RTFSaasBaseDVO.
 */
public class RTFSaasBaseDVO {

	/** The upd dt. */
	private Date updDt;

	/** The cre date. */
	private Date creDate;

	/** The cr date time str. */
	private String crDateTimeStr;

	/** The up date time str. */
	private String upDateTimeStr;

	/** The upd by. */
	private String updBy;

	/** The cre by. */
	private String creBy;

	/**
	 * Gets the cr date time str.
	 *
	 * @return the cr date time str
	 */
	public String getCrDateTimeStr() {
		crDateTimeStr = DateFormatUtil.getDateMMDDYYYYYHHmmss(creDate);
		return crDateTimeStr;
	}

	/**
	 * Gets the cre by.
	 *
	 * @return the cre by
	 */
	public String getCreBy() {
		return creBy;
	}

	/**
	 * Gets the cre date.
	 *
	 * @return the cre date
	 */
	public Date getCreDate() {
		return creDate;
	}

	/**
	 * Gets the up date time str.
	 *
	 * @return the up date time str
	 */
	public String getUpDateTimeStr() {
		upDateTimeStr = DateFormatUtil.getDateMMDDYYYYYHHmmss(updDt);
		return upDateTimeStr;
	}

	/**
	 * Gets the upd by.
	 *
	 * @return the upd by
	 */
	public String getUpdBy() {
		return updBy;
	}

	/**
	 * Gets the upd dt.
	 *
	 * @return the upd dt
	 */
	public Date getUpdDt() {
		return updDt;
	}

	/**
	 * Sets the cr date time str.
	 *
	 * @param crDateTimeStr the new cr date time str
	 */
	public void setCrDateTimeStr(String crDateTimeStr) {
		this.crDateTimeStr = crDateTimeStr;
	}

	/**
	 * Sets the cre by.
	 *
	 * @param creBy the new cre by
	 */
	public void setCreBy(String creBy) {
		this.creBy = creBy;
	}

	/**
	 * Sets the cre date.
	 *
	 * @param creDate the new cre date
	 */
	public void setCreDate(Date creDate) {
		this.creDate = creDate;
	}

	/**
	 * Sets the up date time str.
	 *
	 * @param upDateTimeStr the new up date time str
	 */
	public void setUpDateTimeStr(String upDateTimeStr) {
		this.upDateTimeStr = upDateTimeStr;
	}

	/**
	 * Sets the upd by.
	 *
	 * @param updBy the new upd by
	 */
	public void setUpdBy(String updBy) {
		this.updBy = updBy;
	}

	/**
	 * Sets the upd dt.
	 *
	 * @param updDt the new upd dt
	 */
	public void setUpdDt(Date updDt) {
		this.updDt = updDt;
	}

}
