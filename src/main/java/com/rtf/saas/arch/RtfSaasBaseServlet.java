/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.saas.arch;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rtf.client.signup.util.MessageConstant;
import com.rtf.saas.util.GsonUtil;
import com.rtf.saas.util.SAASMessages;

/**
 * The Class RtfSaasBaseServlet.
 */
abstract public class RtfSaasBaseServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	/**
	 * Do get.
	 *
	 * @param request  the HttpServletRequest
	 * @param response the HttpServletResponse
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		setEncoding(request, response);
		processRequest(request, response);
	}

	/**
	 * Do post.
	 *
	 * @param request  the HttpServletRequest
	 * @param response the HttpServletResponse
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		setEncoding(request, response);
		processRequest(request, response);
	}

	/**
	 * Generate Http Response for Client.Response, data is sent in JSON format.
	 *
	 * @param request        the HttpServlet request
	 * @param response       the HttpServlet response
	 * @param RtfSaasBaseDTO RTF Base Data Transfer Object
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	public abstract void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO RtfSaasBaseDTO) throws ServletException, IOException;

	/**
	 * Process request.
	 *
	 * @param request  the HttpServletRequest
	 * @param response the HttpServletResponse
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	public abstract void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException;

	/**
	 * Sets the client message.
	 *
	 * @param rtfSaasBaseDTO     RTF Base Data Transfer Object
	 * @param customErrMsg       the custom err msg
	 * @param customsucesMessage the customsuces message
	 */
	protected void setClientMessage(RtfSaasBaseDTO rtfSaasBaseDTO, String customErrMsg, String customsucesMessage) {
		if (rtfSaasBaseDTO == null) {
			rtfSaasBaseDTO = new RtfSaasBaseDTO();
		}
		if (rtfSaasBaseDTO.getSts() == null) {
			rtfSaasBaseDTO.setSts(0);
		}
		if (rtfSaasBaseDTO.getSts() == 1) {
			if (customsucesMessage == null)
				customsucesMessage = SAASMessages.GEN_SUCCESS_MSG;
			rtfSaasBaseDTO.setMsg(customsucesMessage);
		} else {
			if (customErrMsg == null)
				customErrMsg = SAASMessages.GEN_ERR_MSG;
			rtfSaasBaseDTO.setMsg(customErrMsg);
		}

	}

	/**
	 * Sets the client message.
	 *
	 * @param rtfSaasBaseDTO     RTF Base Data Transfer Object
	 * @param customErrMsg       the custom err msg
	 * @param customsucesMessage the customsuces message
	 * @param isGenMsg           the is gen msg
	 */
	protected void setClientMessage(RtfSaasBaseDTO rtfSaasBaseDTO, String customErrMsg, String customsucesMessage,
			Boolean isGenMsg) {
		if (rtfSaasBaseDTO == null) {
			rtfSaasBaseDTO = new RtfSaasBaseDTO();
		}
		if (rtfSaasBaseDTO.getSts() == null) {
			rtfSaasBaseDTO.setSts(0);
		}
		if (rtfSaasBaseDTO.getSts() == 1) {
			if (customsucesMessage == null)
				customsucesMessage = SAASMessages.GEN_SUCCESS_MSG;
			rtfSaasBaseDTO.setMsg(customsucesMessage);
		} else {
			if (customErrMsg == null)
				customErrMsg = SAASMessages.GEN_ERR_MSG;
			rtfSaasBaseDTO.setMsg(customErrMsg);
		}

	}

	/**
	 * Sets the encoding.
	 *
	 * @param request  the HttpServletRequest
	 * @param response the HttpServletResponse
	 */
	private void setEncoding(HttpServletRequest request, HttpServletResponse response) {
		try {
			request.setCharacterEncoding("UTF-8");
			response.setContentType("application/json;charset=UTF-8");
			response.setCharacterEncoding("UTF-8");
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	/**
	 * Generate Http Response for Client.Response data is sent in JSON format.
	 *
	 * @param request        the Http request
	 * @param response       the Http response
	 * @param rtfSaasBaseDTO RTF base DTO
	 * @throws ServletException servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	public void generateCommonAPIResponse(HttpServletRequest request, HttpServletResponse response,
			final RtfSaasBaseDTO rtfSaasBaseDTO, String respJsonKey) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put(respJsonKey, rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();
		out.close();
	}

}