/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.saas.arch;

/**
 * The Class RtfSaasBaseDTO.
 */
public class RtfSaasBaseDTO {

	/** The cl id. */
	// Client or Organization Id
	private String clId;

	/** The cu ID. */
	// Raw Customer Id as recieved from Originating Request URL
	private String cuID;

	/** The msg. */
	private String msg;

	/** The sts. */
	private Integer sts;

	/** The co id. */
	private String coId;

	/** The play id. */
	private String playId;

	/**
	 * Gets the cl id.
	 *
	 * @return the cl id
	 */
	public String getClId() {
		return clId;
	}

	/**
	 * Gets the co id.
	 *
	 * @return the co id
	 */
	public String getCoId() {
		return coId;
	}

	/**
	 * Gets the cu ID.
	 *
	 * @return the cu ID
	 */
	public String getCuID() {
		return cuID;
	}

	/**
	 * Gets the msg.
	 *
	 * @return the msg
	 */
	public String getMsg() {
		return msg;
	}

	/**
	 * Gets the play id.
	 *
	 * @return the play id
	 */
	public String getPlayId() {
		return playId;
	}

	/**
	 * Gets the sts.
	 *
	 * @return the sts
	 */
	public Integer getSts() {
		return sts;
	}

	/**
	 * Sets the cl id.
	 *
	 * @param clId the new cl id
	 */
	public void setClId(String clId) {
		this.clId = clId;
	}

	/**
	 * Sets the co id.
	 *
	 * @param coId the new co id
	 */
	public void setCoId(String coId) {
		this.coId = coId;
	}

	/**
	 * Sets the cu ID.
	 *
	 * @param cuID the new cu ID
	 */
	public void setCuID(String cuID) {
		this.cuID = cuID;
	}

	/**
	 * Sets the msg.
	 *
	 * @param msg the new msg
	 */
	public void setMsg(String msg) {
		this.msg = msg;
	}

	/**
	 * Sets the play id.
	 *
	 * @param playId the new play id
	 */
	public void setPlayId(String playId) {
		this.playId = playId;
	}

	/**
	 * Sets the sts.
	 *
	 * @param sts the new sts
	 */
	public void setSts(Integer sts) {
		this.sts = sts;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "RtfSaasBaseDTO [clId=" + clId + ", cuID=" + cuID + ", msg=" + msg + ", sts=" + sts + ", coId=" + coId
				+ ", playId=" + playId + "]";
	}

}
