/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.saas.db;

import java.util.ResourceBundle;

import com.datastax.driver.core.AuthProvider;
import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.ConsistencyLevel;
import com.datastax.driver.core.HostDistance;
import com.datastax.driver.core.PlainTextAuthProvider;
import com.datastax.driver.core.PoolingOptions;
import com.datastax.driver.core.QueryOptions;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.policies.LoadBalancingPolicy;

/**
 * The Class RTFCassandraConnector.
 */
public class RTFCassandraConnector {

	/** The cluster. */
	private static Cluster cluster;

	/** The session. */
	private static Session session;

	/** The node. */
	private static String node = "10.0.1.111";

	/** The key space. */
	private static String keySpace = "rtfquizmasterks";

	/** The user name. */
	private static String userName = "cassandra";

	/** The password. */
	private static String password = "casTe#43tedra";

	/** The port. */
	private static int port = 9042;

	/** The node two. */
	private static String nodeTwo;

	/** The is session created. */
	private static Boolean isSessionCreated = false;

	/** The load balancing policy. */
	static LoadBalancingPolicy loadBalancingPolicy;

	/** The pooling options. */
	static PoolingOptions poolingOptions;

	/** The is with pooling. */
	static Boolean isWithPooling = false;

	/**
	 * Connect.
	 *
	 * @throws Exception the exception
	 */
	public static void connect() throws Exception {

		PoolingOptions tempPoolingOptions = new PoolingOptions();
		tempPoolingOptions.setMaxRequestsPerConnection(HostDistance.LOCAL, 32768)
				.setMaxRequestsPerConnection(HostDistance.REMOTE, 32000);

		tempPoolingOptions.setConnectionsPerHost(HostDistance.LOCAL, 4, 10).setConnectionsPerHost(HostDistance.REMOTE,
				2, 4);

		cluster = Cluster.builder().addContactPoint(node).withPort(port).withCredentials(userName, password)
				.withPoolingOptions(tempPoolingOptions).build();
		session = cluster.connect(keySpace);

		loadBalancingPolicy = cluster.getConfiguration().getPolicies().getLoadBalancingPolicy();
		poolingOptions = cluster.getConfiguration().getPoolingOptions();
		isWithPooling = true;
	}

	/**
	 * Connect with clusters.
	 *
	 * @throws Exception the exception
	 */
	public static void connectWithClusters() throws Exception {

		PoolingOptions tempPoolingOptions = new PoolingOptions();
		tempPoolingOptions.setMaxRequestsPerConnection(HostDistance.LOCAL, 32768)
				.setMaxRequestsPerConnection(HostDistance.REMOTE, 32000);

		tempPoolingOptions.setConnectionsPerHost(HostDistance.LOCAL, 4, 10).setConnectionsPerHost(HostDistance.REMOTE,
				2, 4);

		QueryOptions qo = new QueryOptions().setConsistencyLevel(ConsistencyLevel.ALL);

		AuthProvider authProvider = new PlainTextAuthProvider(userName, password);
		cluster = Cluster.builder().addContactPoints(node, nodeTwo).withPort(port).withAuthProvider(authProvider)
				.withPoolingOptions(poolingOptions).withQueryOptions(qo).build();

		session = cluster.connect(keySpace);

		loadBalancingPolicy = cluster.getConfiguration().getPolicies().getLoadBalancingPolicy();
		poolingOptions = cluster.getConfiguration().getPoolingOptions();
		isWithPooling = true;
	}

	/**
	 * Gets the key space.
	 *
	 * @return the key space
	 */
	public static String getKeySpace() {
		return keySpace;
	}

	/**
	 * Gets the rtf sass cass key space.
	 *
	 * @return the rtf sass cass key space
	 */
	public static String getRtfSassCassKeySpace() {
		return "rtfquizmasterks.";
	}

	/**
	 * Gets the session.
	 *
	 * @return the session
	 */
	public static Session getSession() {
		try {
			if (!isSessionCreated) {
				connect();
				isSessionCreated = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return session;
	}

	/**
	 * Load application values.
	 *
	 * @throws Exception the exception
	 */
	public static void loadApplicationValues() throws Exception {
		try {
			ResourceBundle resourceBundle = ResourceBundle.getBundle("commonsettings");
			node = resourceBundle.getString("rtf.cassandra.db.host.ip");
			port = Integer.parseInt(resourceBundle.getString("rtf.cassandra.db.port").trim());
			keySpace = resourceBundle.getString("rtf.cassandra.db.keySpace");
			userName = resourceBundle.getString("rtf.cassandra.db.user.name");
			password = resourceBundle.getString("rtf.cassandra.db.password");

			connect();

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}

	}

	/**
	 * Close.
	 */
	public void close() {
		cluster.close();
	}

}