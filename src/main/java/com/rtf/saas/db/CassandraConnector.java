/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.saas.db;

import java.util.ResourceBundle;

import com.datastax.driver.core.AuthProvider;
import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.ConsistencyLevel;
import com.datastax.driver.core.HostDistance;
import com.datastax.driver.core.PlainTextAuthProvider;
import com.datastax.driver.core.PoolingOptions;
import com.datastax.driver.core.QueryOptions;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.policies.LoadBalancingPolicy;

/**
 * The Class CassandraConnector.
 */
public class CassandraConnector {

	/** The cluster. */
	private static Cluster cluster;

	/** The session. */
	private static Session session;

	/** The readcluster. */
	private static Cluster readcluster;

	/** The readsession. */
	private static Session readsession;

	/** The node. */
	private static String node;
	/** The key space. */
	private static String keySpace = "saasprodclientadminwcid";

	/** The user name. */
	private static String userName;

	/** The password. */
	private static String password;

	/** The port. */
	private static int port;

	/** The saas play cass key space. */
	private static String saasPlayCassKeySpace = "saasprodclientadminwcid.";

	/** The node two. */
	private static String nodeTwo;

	/** The is session created. */
	private static Boolean isSessionCreated = false;

	/** The isread session created. */
	private static Boolean isreadSessionCreated = false;

	/** The load balancing policy. */
	static LoadBalancingPolicy loadBalancingPolicy;

	/** The pooling options. */
	static PoolingOptions poolingOptions;

	/** The is with pooling. */
	static Boolean isWithPooling = false;

	/**
	 * Connect.
	 *
	 * @throws Exception the exception
	 */
	public static void connect() throws Exception {

		PoolingOptions tempPoolingOptions = new PoolingOptions();
		tempPoolingOptions.setMaxRequestsPerConnection(HostDistance.LOCAL, 32768)
				.setMaxRequestsPerConnection(HostDistance.REMOTE, 32000);

		tempPoolingOptions.setConnectionsPerHost(HostDistance.LOCAL, 4, 10).setConnectionsPerHost(HostDistance.REMOTE,
				2, 4);

		cluster = Cluster.builder().addContactPoints(node).withPort(port).withCredentials(userName, password)
				.withPoolingOptions(tempPoolingOptions).build();
		session = cluster.connect(keySpace);

		loadBalancingPolicy = cluster.getConfiguration().getPolicies().getLoadBalancingPolicy();
		poolingOptions = cluster.getConfiguration().getPoolingOptions();
		isWithPooling = true;
	}

	/**
	 * Connectforread.
	 *
	 * @throws Exception the exception
	 */
	public static void connectforread() throws Exception {

		PoolingOptions tempPoolingOptions = new PoolingOptions();
		tempPoolingOptions.setMaxRequestsPerConnection(HostDistance.LOCAL, 32768)
				.setMaxRequestsPerConnection(HostDistance.REMOTE, 32000);

		tempPoolingOptions.setConnectionsPerHost(HostDistance.LOCAL, 10, 10).setConnectionsPerHost(HostDistance.REMOTE,
				2, 4);
		QueryOptions qOptions = new QueryOptions().setConsistencyLevel(ConsistencyLevel.LOCAL_ONE);
		String region = "us-east-1";
		String endpoint = "cassandra." + region + ".amazonaws.com";

		readcluster = Cluster.builder().addContactPoint(endpoint).withPort(9142)
				.withCredentials("casskeyspaces-at-088589160794", "QFz8TChSU0zGOF5vFLMPltyHJSeWum6guxFC2/CxFUY=")
				.withPoolingOptions(tempPoolingOptions).withSSL().withQueryOptions(qOptions).build();

		readsession = readcluster.connect("prodsaastriviamanageplayks");

		loadBalancingPolicy = readcluster.getConfiguration().getPolicies().getLoadBalancingPolicy();
		poolingOptions = readcluster.getConfiguration().getPoolingOptions();
		isWithPooling = true;

	}

	/**
	 * Connect key space.
	 *
	 * @throws Exception the exception
	 */
	public static void connectKeySpace() throws Exception {

		PoolingOptions tempPoolingOptions = new PoolingOptions();
		tempPoolingOptions.setMaxRequestsPerConnection(HostDistance.LOCAL, 32768)
				.setMaxRequestsPerConnection(HostDistance.REMOTE, 32000);

		tempPoolingOptions.setConnectionsPerHost(HostDistance.LOCAL, 10, 10).setConnectionsPerHost(HostDistance.REMOTE,
				2, 4);
		QueryOptions qOptions = new QueryOptions().setConsistencyLevel(ConsistencyLevel.LOCAL_QUORUM);
		String region = "us-east-1";
		String endpoint = "cassandra." + region + ".amazonaws.com";

		cluster = Cluster.builder().addContactPoint(endpoint).withPort(9142)
				.withCredentials("casskeyspaces-at-088589160794", "QFz8TChSU0zGOF5vFLMPltyHJSeWum6guxFC2/CxFUY=")
				.withPoolingOptions(tempPoolingOptions).withSSL().withQueryOptions(qOptions).build();

		session = cluster.connect("prodsaastriviamanageplayks");

		loadBalancingPolicy = cluster.getConfiguration().getPolicies().getLoadBalancingPolicy();
		poolingOptions = cluster.getConfiguration().getPoolingOptions();
		isWithPooling = true;
	}

	/**
	 * Connect with clusters.
	 *
	 * @throws Exception the exception
	 */
	public static void connectWithClusters() throws Exception {

		PoolingOptions tempPoolingOptions = new PoolingOptions();
		tempPoolingOptions.setMaxRequestsPerConnection(HostDistance.LOCAL, 32768)
				.setMaxRequestsPerConnection(HostDistance.REMOTE, 32000);

		tempPoolingOptions.setConnectionsPerHost(HostDistance.LOCAL, 4, 10).setConnectionsPerHost(HostDistance.REMOTE,
				2, 4);

		QueryOptions qo = new QueryOptions().setConsistencyLevel(ConsistencyLevel.ALL);

		AuthProvider authProvider = new PlainTextAuthProvider(userName, password);
		cluster = Cluster.builder().addContactPoints(node, nodeTwo).withPort(port).withAuthProvider(authProvider)
				.withPoolingOptions(poolingOptions).withQueryOptions(qo).build();

		session = cluster.connect(keySpace);

		loadBalancingPolicy = cluster.getConfiguration().getPolicies().getLoadBalancingPolicy();
		poolingOptions = cluster.getConfiguration().getPoolingOptions();
		isWithPooling = true;
	}

	/**
	 * Gets the key space.
	 *
	 * @return the key space
	 */
	public static String getKeySpace() {
		return keySpace;
	}

	/**
	 * Gets the read session.
	 *
	 * @return the read session
	 */
	public static Session getreadSession() {
		try {
			if (!isreadSessionCreated) {
				connectforread();
				isreadSessionCreated = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return readsession;
	}

	/**
	 * Gets the rtf sass cass key space.
	 *
	 * @return the rtf sass cass key space
	 */
	public static String getRtfSassCassKeySpace() {
		return saasPlayCassKeySpace;
	}

	/**
	 * Gets the session.
	 *
	 * @return the session
	 */
	public static Session getSession() {
		try {
			if (!isSessionCreated) {
				connect();
				isSessionCreated = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return session;
	}

	// @Override
	/**
	 * Load application values.
	 *
	 * @throws Exception the exception
	 */
	public static void loadApplicationValues() throws Exception {
		try {
			ResourceBundle resourceBundle = ResourceBundle.getBundle("commonsettings");
			node = resourceBundle.getString("saas.cassandra.db.host.ip");
			port = Integer.parseInt(resourceBundle.getString("saas.cassandra.db.port").trim());
			keySpace = resourceBundle.getString("saas.cassandra.db.keySpace");
			userName = resourceBundle.getString("saas.cassandra.db.user.name");
			password = resourceBundle.getString("saas.cassandra.db.password");

			saasPlayCassKeySpace = resourceBundle.getString("saas.cassandra.play.link.keyspace");

			connect();
			isSessionCreated = true;

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}

	}

	/**
	 * Close.
	 */
	public void close() {
		cluster.close();
	}

}