/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.saas.util;

/**
 * The Class SAASMessages.
 */
public class SAASMessages {

	/** The Constant GEN_ERR_MSG. */
	public static final String GEN_ERR_MSG = "Something went wrong. Please try again";

	/** The Constant GEN_SUCCESS_MSG. */
	public static final String GEN_SUCCESS_MSG = "Success";

}
