/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.saas.util;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import org.apache.commons.lang3.StringUtils;

/**
 * The Class DateFormatUtil.
 */
public class DateFormatUtil {

	/** The dt. */
	private static SimpleDateFormat dt = new SimpleDateFormat("MM/dd/yy");

	/** The time ft. */
	private static SimpleDateFormat timeFt = new SimpleDateFormat("hh:mmaa z");

	/**
	 * Gets the date in Format MMDDYYYYYH hmmss.
	 *
	 * @param date the date
	 * @return the date MMDDYYYYYH hmmss
	 */
	public static String getDateMMDDYYYYYHHmmss(Date date) {
		String formatedDate = "--";

		if (date == null)
			return formatedDate;
		if (date.getTime() == -10)
			return formatedDate;

		try {

			SimpleDateFormat simpDate = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
			formatedDate = simpDate.format(date);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return formatedDate;

	}

	/**
	 * Gets the formatted date in String MMDDYYYYHHMM .
	 *
	 * @param timeInMilli the time in millis
	 * @return the MMDDYYYYHHMM string
	 */
	public static String getMMDDYYYYHHMMString(Long timeInMilli) {
		String result = null;
		try {
			if (timeInMilli == null) {
				return null;
			}
			Date date = new Date(timeInMilli);
			DateFormat formatDateTime = new SimpleDateFormat("MM/dd/yyyy hh:mm a");
			result = formatDateTime.format(date);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * Gets the Pattern of  MMDDYYYY string.
	 *
	 * @param timeInMilli the time in milli
	 * @return the MMDDYYYY string
	 */
	public static String getMMDDYYYYString(Long timeInMilli) {
		String result = null;
		try {
			if (timeInMilli == null) {
				return null;
			}
			Date date = new Date(timeInMilli);
			DateFormat formatDateTime = new SimpleDateFormat("MM/dd/yyyy");
			result = formatDateTime.format(date);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * Gets the next contest start time msg.
	 *
	 * @param stDate the st date
	 * @return the next contest start time msg
	 */
	public static String getNextContestStartTimeMsg(Long stDate) {
		String contestStartTime =StringUtils.EMPTY;
		if (stDate == null) {
			return StringUtils.EMPTY;
		}
		Date startDate = new Date(stDate);
		try {
			Date today = dt.parse(dt.format(new Date()));
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.DAY_OF_MONTH, 1);
			Date tomorrow = dt.parse(dt.format(new Date(cal.getTimeInMillis())));
			Date contestDate = dt.parse(dt.format(startDate));
			if (contestDate.compareTo(today) == 0) {
				contestStartTime = "Today " + timeFt.format(startDate);
				return contestStartTime;
			} else if (contestDate.compareTo(tomorrow) == 0) {
				contestStartTime = "Tomorrow " + timeFt.format(startDate);
				return contestStartTime;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		contestStartTime = dt.format(startDate) + " " + timeFt.format(startDate);
		return contestStartTime;
	}

	/**
	 * Gets the rounded value string.
	 *
	 * @param value the value
	 * @return the rounded value string
	 * @throws Exception the exception
	 */
	public static String getRoundedValueString(Double value) throws Exception {
		return String.format("%.2f", value);
	}

	/**
	 * Gets the rounded value string with comma.
	 *
	 * @param value the value
	 * @return the rounded value string with comma
	 * @throws Exception the exception
	 */
	public static String getRoundedValueStringWithComma(Double value) throws Exception {
		DecimalFormat anotherFormat = (DecimalFormat) NumberFormat.getNumberInstance(Locale.US);
		anotherFormat.setMaximumFractionDigits(0);
		anotherFormat.setGroupingUsed(true);
		anotherFormat.setGroupingSize(3);
		String result = anotherFormat.format(value);
		return result;
	}

	/**
	 * Instantiates a new date format util.
	 */
	public DateFormatUtil() {
		// TODO Auto-generated constructor stub
	}

}
