/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.saas.util;

/**
 * The Class PluginConfigConstants.
 */
public class PluginConfigConstants {

	/** The Constant SNW_CONTEST_INTERVAL_TIME_IN_MINS. */
	public static final String SNW_CONTEST_INTERVAL_TIME_IN_MINS = "SNW_CONTEST_INTERVAL_TIME_IN_MINS";

}
