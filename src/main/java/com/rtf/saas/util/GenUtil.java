/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.saas.util;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

/**
 * The Class GenUtil.
 */
public class GenUtil {

	/**
	 * Current time millis.
	 *
	 * @return the long
	 */
	public static long currentTimeMillis() {
		return System.currentTimeMillis();
	}

	/**
	 * Gets the current time stamp.
	 *
	 * @return the current time stamp
	 */
	public static java.sql.Timestamp getCurrentTimeStamp() {
		java.util.Date today = new java.util.Date();
		return new java.sql.Timestamp(today.getTime());
	}

	/**
	 * Gets the desc.
	 *
	 * @param request the request
	 * @return the desc
	 */
	public static String getDesc(HttpServletRequest request) {
		String desc = "";
		if (request != null) {
			desc = request.getQueryString();
		}
		return desc;

	}

	/**
	 * Gets the exception as string.
	 *
	 * @param ex the ex
	 * @return the exception as string
	 */
	public static String getExceptionAsString(Exception ex) {
		StringWriter sw = new StringWriter();
		ex.printStackTrace(new PrintWriter(sw));
		return sw.toString();
	}

	/**
	 * Gets the ip.
	 *
	 * @param request the request
	 * @return the ip
	 */
	public static String getIp(HttpServletRequest request) {
		String remoteAddr = "";
		if (request != null) {
			remoteAddr = request.getHeader("X-FORWARDED-FOR");
			if (remoteAddr == null || "".equals(remoteAddr)) {
				remoteAddr = request.getRemoteAddr();
			}
		}

		return remoteAddr;
	}

	/**
	 * Gets the randon UUID.
	 *
	 * @return the randon UUID
	 */
	public static String getRandonUUID() {
		final String uuid = UUID.randomUUID().toString().replace("-", "");
		return uuid;
	}

	/**
	 * Gets the server path.
	 *
	 * @param request the request
	 * @return the serv path
	 */
	public static String getServPath(HttpServletRequest request) {

		String servletPath = "";
		if (request != null) {
			servletPath = request.getRequestURI();
		}
		return servletPath;
	}

	/**
	 * Checks if is empty string.
	 *
	 * @param s the s
	 * @return true, if is empty string
	 */
	public static boolean isEmptyString(String s) {
		if (s == null || s.isEmpty() || isNullOrWhiteSpace(s)) {
			return true;
		}
		return false;
	}

	/**
	 * Checks if is null or empty.
	 *
	 * @param s the s
	 * @return true, if is null or empty
	 */
	public static boolean isNullOrEmpty(String s) {
		if (s == null || s.isEmpty() || s.trim().isEmpty()) {
			return true;
		}
		return false;
	}

	/**
	 * Checks if is null or white space.
	 *
	 * @param s the s
	 * @return true, if is null or white space
	 */
	public static boolean isNullOrWhiteSpace(String s) {
		if (s == null || s.trim().isEmpty()) {
			return true;
		}
		return false;
	}

}
