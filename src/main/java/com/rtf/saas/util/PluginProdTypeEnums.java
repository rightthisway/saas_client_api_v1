/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.saas.util;

/**
 * The Enum PluginProdTypeEnums.
 */
public enum PluginProdTypeEnums {

	/** The ott. */
	OTT,
	/** The snw. */
	SNW,
	/** The live. */
	LIVE,
	/** The login. */
	LOGIN

}
