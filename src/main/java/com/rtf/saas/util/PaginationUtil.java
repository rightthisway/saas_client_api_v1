/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.saas.util;

import org.apache.commons.lang3.StringUtils;

/**
 * The Class PaginationUtil.
 */
public class PaginationUtil {

	/** The Constant PAGESIZE. */
	public static final Integer PAGESIZE = 10;

	/**
	 * Gets the pagination query.
	 *
	 * @param pageNo the page no
	 * @return the pagination query
	 */
	public static String getPaginationQuery(String pageNo) {
		String query = StringUtils.EMPTY;
		if (pageNo != null && !pageNo.isEmpty()) {
			query = " OFFSET (" + pageNo + "-1)*" + PaginationUtil.PAGESIZE + " ROWS FETCH NEXT "
					+ PaginationUtil.PAGESIZE + "  ROWS ONLY";
		}
		return query;
	}
}
