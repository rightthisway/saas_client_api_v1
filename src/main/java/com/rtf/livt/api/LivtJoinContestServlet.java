/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rtf.common.dao.ClientCustomerDAO;
import com.rtf.common.dvo.ClientCustomerDVO;
import com.rtf.common.util.Messages;
import com.rtf.livt.cass.dao.ContestParticipantsDAO;
import com.rtf.livt.dto.LivtJoinContestDTO;
import com.rtf.livt.dvo.ContestDVO;
import com.rtf.livt.dvo.ContestParticipantsDVO;
import com.rtf.livt.service.LivtService;
import com.rtf.livt.util.LivtContestUtil;
import com.rtf.livt.util.TextUtil;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GsonUtil;

/**
 * The Class LivtJoinContestServlet.
 */

@WebServlet("/livtJoinContest.json")
public class LivtJoinContestServlet extends RtfSaasBaseServlet {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Generate Http Response for Client.Response, data is sent in JSON format.
	 *
	 * @param request        the HttpServlet request
	 * @param response       the HttpServlet response
	 * @param rtfSaasBaseDTO RTF Base Data Transfer Object
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("livtresp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();
	}

	/**
	 * Process request.Method to Update Status of Participants as Active when they
	 * enter the Live Gane
	 *
	 * @param request  the HttpServletRequest
	 * @param response the HttpServletResponse
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		LivtJoinContestDTO dto = new LivtJoinContestDTO();
		dto.setSts(0);
		Date start = new Date();
		String contestIdStr = request.getParameter("coId");
		String customerIdStr = request.getParameter("cuId");
		String cclientIdStr = request.getParameter("clId");
		//String loginIp = request.getParameter("lipa");
		String resMsg = "";
		//String deviceType = request.getParameter("dyType");
		//String apiHitStartTimeStr = request.getParameter("ahstm");// - api hit start time
		String contestId = null;
		//String platForm = request.getParameter("pfm");
		Boolean iserrorTrack = true;
		try {
			String contestType = "WEB";
			if (TextUtil.isEmptyOrNull(customerIdStr)) {
				resMsg = Messages.INVALID_CUSTOMER_ID + customerIdStr;
				setClientMessage(dto, resMsg, null, true);
				generateResponse(request, response, dto);
				return;
			}
			if (TextUtil.isEmptyOrNull(cclientIdStr)) {
				resMsg = Messages.INVALID_CLIENT_ID + customerIdStr;
				setClientMessage(dto, Messages.INVALID_CLIENT_ID, null, true);
				generateResponse(request, response, dto);
				return;
			}
			if (TextUtil.isEmptyOrNull(contestIdStr)) {
				resMsg = Messages.INVALID_CONTEST_ID + contestIdStr;
				setClientMessage(dto, Messages.INVALID_CONTEST_ID, null, true);
				generateResponse(request, response, dto);
				return;
			}
			ContestDVO contest = LivtContestUtil.getCurrentContestByContestId(cclientIdStr, contestIdStr);
			if (contest == null) {
				resMsg = Messages.LIVT_MSG_CONTEST_NOT_START + contestType;
				setClientMessage(dto, resMsg, null, true);
				generateResponse(request, response, dto);
				return;
			}
			ClientCustomerDVO customer = null;
			ContestParticipantsDVO participant = ContestParticipantsDAO
					.getContestParticipantByContestIdAndCustomerIdForVal(cclientIdStr, contestIdStr, customerIdStr);
			if (participant == null) {
				customer = ClientCustomerDAO.getClientCustomerForValidationByCustomerId(cclientIdStr, customerIdStr);
				if (customer == null) {
					resMsg = Messages.INVALID_CUSTOMER_ID + customerIdStr;
					setClientMessage(dto, Messages.INVALID_CUSTOMER_ID, null, true);
					generateResponse(request, response, dto);
					return;
				}
			}
			if (contest != null) {
				dto = LivtContestUtil.updateJoinContestCustomersCount(dto, cclientIdStr, contest.getCoId(),
						customerIdStr, contest);
				if (participant == null || !participant.getStatus().equals(Messages.LIVT_STATUS_ACTIVE)) {

					if (participant == null) {
						participant = new ContestParticipantsDVO();
						participant.setJnDate(new Date());
						participant.setuId(customer.getUserId());
					} else {
						if (participant.getJnDate() == null) {
							participant.setJnDate(new Date());
						}
					}
					participant.setClId(cclientIdStr);
					participant.setCoId(contest.getCoId());
					participant.setCuId(customerIdStr);
					//participant.setPfm(platForm);
					//participant.setIpAdd(loginIp);
					participant.setStatus(Messages.LIVT_STATUS_ACTIVE);
					ContestParticipantsDAO.saveForJoin(participant);
					LivtService.updateJoinCounter(cclientIdStr, contest.getCoId());
				}
			}
			setClientMessage(dto, Messages.LIVT_GEN_SUCC_MSG, null, true);
			iserrorTrack = false;
			dto.setSts(1);
			dto.setCoId(contest.getCoId());
			generateResponse(request, response, dto);
			resMsg = Messages.LIVT_GEN_SUCC_MSG + customerIdStr + ":" + contest.getCoId() + ":" + contestType;
			return;
		} catch (Exception e) {
			resMsg = Messages.LIVT_ERR_FETCH_JOINCO_DETS;
			e.printStackTrace();
			setClientMessage(dto, Messages.LIVT_ERR_FETCH_JOINCO_DETS, null, true);
			generateResponse(request, response, dto);
			return;
		} finally {

			/*
			 * if(TrackingUtil.allowAllTracking || iserrorTrack) {
			 * TrackingUtil.contestAPITrackingForDeviceTimeTracking(cclientIdStr, platForm,
			 * deviceType, request.getHeader("deviceId"), WebServiceActionType.JOINCONTEST,
			 * resMsg, contestId, customerIdStr, start, new Date(),
			 * request.getHeader("X-Forwarded-For"), null, apiHitStartTimeStr, dto.getSts(),
			 * null); }
			 */
		}
	}

}
