/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;

import com.rtf.common.dao.ClientCustomerDAO;
import com.rtf.common.dvo.ClientCustomerDVO;
import com.rtf.common.util.Messages;
import com.rtf.livt.dto.LivtCustomerListDTO;
import com.rtf.livt.dvo.ContestDVO;
import com.rtf.livt.util.LivtContestUtil;
import com.rtf.livt.util.TextUtil;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GsonUtil;

/**
 * The Class LivtLiveCustListServlet.
 */

@WebServlet("/livtLiveCustList.json")
public class LivtLiveCustListServlet extends RtfSaasBaseServlet {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Generate Http Response for Client.Response, data is sent in JSON format.
	 *
	 * @param request        the HttpServlet request
	 * @param response       the HttpServlet response
	 * @param rtfSaasBaseDTO RTF Base Data Transfer Object
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("livtresp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();

	}

	/**
	 * Process request.Method to fetch List of Live Participant User Id's.
	 *
	 * @param request  the HttpServletRequest
	 * @param response the HttpServletResponse
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		LivtCustomerListDTO dto = new LivtCustomerListDTO();
		dto.setSts(0);
		Date start = new Date();
		String clientId = request.getParameter("clId");
		String customerIdStr = request.getParameter("cuId");
		String contestIdStr = request.getParameter("coId");
		String pageNoStr = request.getParameter("pNo");
		//String platForm = request.getParameter("pfm");
		//String deviceType = request.getParameter("dyType");
		//String apiHitStartTimeStr = request.getParameter("ahstm");// - api hit start time
		String resMsg = StringUtils.EMPTY;
		Boolean iserrorTrack = true;
		try {
			if (TextUtil.isEmptyOrNull(clientId)) {
				setClientMessage(dto, Messages.INVALID_CLIENT_ID, null, true);
				generateResponse(request, response, dto);
				return;
			}

			if (TextUtil.isEmptyOrNull(contestIdStr)) {
				resMsg = Messages.INVALID_CONTEST_ID + contestIdStr;
				setClientMessage(dto, "Invalid ContestId.", null, true);
				generateResponse(request, response, dto);
				return;
			}
			ClientCustomerDVO customer = ClientCustomerDAO.getClientCustomerByCustomerId(clientId, customerIdStr);
			if (customer == null) {
				resMsg = Messages.INVALID_CUSTOMER_ID + customerIdStr;
				setClientMessage(dto, Messages.INVALID_CUSTOMER_ID, null, true);
				generateResponse(request, response, dto);
				return;
			}
			ContestDVO contest = LivtContestUtil.getCurrentContestByContestId(clientId, contestIdStr);
			if (contest == null) {
				resMsg = Messages.INVALID_CONTEST_ID + contestIdStr;
				setClientMessage(dto, Messages.INVALID_CONTEST_ID, null, true);
				generateResponse(request, response, dto);
				return;
			}
			dto = LivtContestUtil.getLiveCustomerDetailsInfoFromCache(clientId, contestIdStr, dto);
			resMsg = Messages.LIVT_GEN_SUCC_MSG + contestIdStr + ":coId:" + contestIdStr + ":pNo:" + pageNoStr;
			dto.setSts(1);
			iserrorTrack = false;
			generateResponse(request, response, dto);
			return;

		} catch (Exception e) {
			resMsg = Messages.LIVT_ERR_FETCH_CUST_LIST;
			e.printStackTrace();
			setClientMessage(dto, Messages.LIVT_ERR_FETCH_CUST_LIST, null, true);
			generateResponse(request, response, dto);
			return;
		} finally {
			/*
			 * if(TrackingUtil.allowAllTracking || iserrorTrack) {
			 * TrackingUtil.contestAPITrackingForDeviceTimeTracking(clientId, platForm,
			 * deviceType, request.getHeader("deviceId"),
			 * WebServiceActionType.GETLIVECUSTLIST, resMsg, contestIdStr, null, start, new
			 * Date(), request.getHeader("X-Forwarded-For"), null, apiHitStartTimeStr,
			 * dto.getSts(), null); }
			 */

		}

	}

}
