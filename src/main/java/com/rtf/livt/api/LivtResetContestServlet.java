/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;

import com.rtf.common.util.Messages;
import com.rtf.livt.dto.LivtCommonDTO;
import com.rtf.livt.util.LivtContestUtil;
import com.rtf.livt.util.TextUtil;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GsonUtil;

/**
 * The Class LivtResetContestServlet.
 */

@WebServlet("/livtResetContest.json")
public class LivtResetContestServlet extends RtfSaasBaseServlet {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Generate HttpServletResponse for Client. Response, data is sent in JSON
	 * format.
	 *
	 * @param request        the HttpServlet request
	 * @param response       the HttpServlet response
	 * @param rtfSaasBaseDTO RTF Base Data Transfer Object
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("livtresp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();

	}

	/**
	 * Process request.Method used to Reset Contest Related Data. Primarily removes
	 * Cache data related to the contest. All Data in Cassandara related to Client
	 * Id is truncated
	 * 
	 * @param request  the HttpServletRequest
	 * @param response the HttpServletResponse
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String clientIdStr = request.getParameter("clId");
		//String platForm = request.getParameter("pfm");
		String contestIDStr = request.getParameter("coId");
		String clearDbDataStr = request.getParameter("clearDbData");

		LivtCommonDTO dto = new LivtCommonDTO();
		dto.setSts(0);
		dto.setClId(clientIdStr);
		//Date start = new Date();
		String resMsg = StringUtils.EMPTY;

		try {

			if (TextUtil.isEmptyOrNull(contestIDStr)) {
				resMsg = Messages.INVALID_CONTEST_ID;
				setClientMessage(dto, resMsg, null, false);
				generateResponse(request, response, dto);
				return;

			}
			if (TextUtil.isEmptyOrNull(clientIdStr)) {
				resMsg = Messages.MANDATORY_PARAM_CLIENT_ID;
				setClientMessage(dto, Messages.MANDATORY_PARAM_CLIENT_ID, null, false);
				generateResponse(request, response, dto);
				return;
			}
			LivtContestUtil.clearCacheDataByContestId(clientIdStr, contestIDStr);
			ServletContext appCtx = getServletConfig().getServletContext();  
			appCtx.removeAttribute(clientIdStr);  	

			String cassDataRemoved = Messages.STATUS_TEXT_NO;
			if (clearDbDataStr == null || clearDbDataStr.equals("Y")) {
				LivtContestUtil.truncateCassandraContestData(clientIdStr, contestIDStr);
				cassDataRemoved = Messages.STATUS_TEXT_YES;
			}
			resMsg = "successfully:cassDataRemoved:" + contestIDStr + ":" + cassDataRemoved + ":" + clearDbDataStr;
			dto.setCoId(contestIDStr);
			dto.setSts(1);
			setClientMessage(dto, Messages.PROCESS_SUCESS_RESET, null, false);
			generateResponse(request, response, dto);
			return;
		} catch (Exception e) {
			resMsg =  Messages.PROCESS_ERR_RESET;
			e.printStackTrace();
			setClientMessage(dto, Messages.PROCESS_ERR_RESET, null, false);
			generateResponse(request, response, dto);
			return;
		} finally {
			/*
			 * TrackingUtil.contestAPITracking(clientIdStr, platForm, null,
			 * request.getHeader("deviceId"), WebServiceActionType.REMOVECASSCONTESTCACHE,
			 * resMsg, contestIDStr, null, start, new Date(),
			 * request.getHeader("X-Forwarded-For"), null, dto.getSts(), null);
			 */
			if (dto.getSts() == 0) {
				dto.setMsg(resMsg);
			}
		}

	}

}
