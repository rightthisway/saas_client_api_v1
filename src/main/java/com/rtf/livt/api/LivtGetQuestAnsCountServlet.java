/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;

import com.rtf.common.util.Messages;
import com.rtf.livt.dto.LivtCommonDTO;
import com.rtf.livt.dvo.ContestDVO;
import com.rtf.livt.dvo.LivtContestQuestionDVO;
import com.rtf.livt.util.LivtContestUtil;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GsonUtil;

/**
 * The Class LivtGetQuestAnsCountServlet.
 */

@WebServlet("/livtGetQuestAnsCount.json")
public class LivtGetQuestAnsCountServlet extends RtfSaasBaseServlet {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Generate Http Response for Client.Response, data is sent in JSON format.
	 *
	 * @param request        the HttpServlet request
	 * @param response       the HttpServlet response
	 * @param rtfSaasBaseDTO RTF Base Data Transfer Object
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("livtresp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();

	}

	/**
	 * Process request.Method to fetch total of Each Question Answered Count by
	 * Customers
	 *
	 * @param request  the HttpServletRequest
	 * @param response the HttpServletResponse
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String clientId = request.getParameter("clId");
		String contestIdStr = request.getParameter("coId");
		String questionNoStr = request.getParameter("qNo");
		String questionIdStr = request.getParameter("qId");
		//String platForm = request.getParameter("pfm");
		//String deviceType = request.getParameter("dyType");
		String resMsg = StringUtils.EMPTY;
		LivtCommonDTO dto = new LivtCommonDTO();
		dto.setSts(0);
		dto.setClId(clientId);
		Date start = new Date();
		try {
			ContestDVO contest = LivtContestUtil.getCurrentContestByContestId(clientId, contestIdStr);
			if (contest == null) {
				resMsg = Messages.INVALID_CONTEST_ID + contestIdStr;
				setClientMessage(dto, Messages.INVALID_CONTEST_ID, null, false);
				generateResponse(request, response, dto);
				return;
			}

			LivtContestQuestionDVO quizQuestion = LivtContestUtil.getLivtContestQuestionById(clientId, contestIdStr,
					Integer.parseInt(questionIdStr), Integer.parseInt(questionNoStr));
			if (quizQuestion == null || !quizQuestion.getConqsnid().equals(Integer.parseInt(questionIdStr))) {
				resMsg = Messages.INVALID_QUESTION_ID + questionIdStr;

				setClientMessage(dto, Messages.INVALID_QUESTION_ID, null, false);
				generateResponse(request, response, dto);
				return;
			}
			LivtContestUtil.updateContestCurrentQuestions(clientId, quizQuestion);
			resMsg = Messages.LIVT_GEN_SUCC_MSG + questionIdStr;
			setClientMessage(dto, Messages.LIVT_MSG_CONTEST_STARTED, null, false);
			dto.setSts(1);
			dto.setCoId(contest.getCoId());
			generateResponse(request, response, dto);

		} catch (Exception e) {
			resMsg = Messages.LIVT_ERR_CON_ANSWERCOUNT;
			e.printStackTrace();
			setClientMessage(dto, Messages.LIVT_ERR_CON_ANSWERCOUNT, null, false);
			generateResponse(request, response, dto);
			return;
		} finally {
			/*
			 * TrackingUtil.contestAPITracking(clientId, platForm, deviceType,
			 * request.getHeader("deviceId"), WebServiceActionType.GETQUESTANSCOUNT, resMsg,
			 * contestIdStr, null, start, new Date(), request.getHeader("X-Forwarded-For"),
			 * null, dto.getSts(), questionNoStr);
			 */
		}
	}
}
