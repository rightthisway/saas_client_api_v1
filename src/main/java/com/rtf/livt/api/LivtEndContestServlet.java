/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rtf.common.util.Messages;
import com.rtf.custom.service.GenericRewardMigrationService;
import com.rtf.livt.dto.LivtCommonDTO;
import com.rtf.livt.util.LivtContestUtil;
import com.rtf.livt.util.TextUtil;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GsonUtil;

/**
 * The Class LivtEndContestServlet.
 */

@WebServlet("/livtEndContest.json")
public class LivtEndContestServlet extends RtfSaasBaseServlet {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new LivtEndContestServlet
	 */
	public LivtEndContestServlet() {
		super();
	}

	/**
	 * Creates the reward migration job details.
	 *
	 * @param cclientIdStr the client id str
	 * @param contestIdStr the contest id str
	 * @throws Exception the exception
	 */
	private void createRewardMigrationJobDetails(String cclientIdStr, String contestIdStr) throws Exception {
		GenericRewardMigrationService.createRewardMigrationJobNamesOnEndContest(cclientIdStr, contestIdStr);

	}

	/**
	 * Generate Http Response for Client.Response, data is sent in JSON format.
	 *
	 * @param request        the HttpServlet request
	 * @param response       the HttpServlet response
	 * @param rtfSaasBaseDTO RTF Base Data Transfer Object
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("livtresp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();

	}

	/**
	 * Process request.Update Status of Contest to END for Live Contest  Status records
	 *
	 * @param request  the HttpServletRequest
	 * @param response the HttpServletResponse
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String clientId = request.getParameter("clId");
		String contestIdStr = request.getParameter("coId");
		//String platForm = request.getParameter("platForm");
		//String deviceType = request.getParameter("deviceType");
		String endType = request.getParameter("eType");
		LivtCommonDTO dto = new LivtCommonDTO();
		dto.setSts(0);
		dto.setClId(clientId);
		//Date start = new Date();
		String resMsg = "";
		try {

			if (TextUtil.isEmptyOrNull(contestIdStr)) {
				resMsg = Messages.INVALID_CONTEST_ID + contestIdStr;
				setClientMessage(dto, resMsg, null, false);
				generateResponse(request, response, dto);
				return;
			}
			try {
				if (endType != null && endType.equals(Messages.FORCESTOP_CONTEST)) {
					// CassContestUtil.updatePostMigrationStats();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			try {
				createRewardMigrationJobDetails(clientId, contestIdStr);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			LivtContestUtil.refreshEndContestData(clientId, contestIdStr);
			
			ServletContext appCtx = getServletConfig().getServletContext();  
			appCtx.removeAttribute(clientId);  	
			
			resMsg = Messages.LIVT_GEN_SUCC_MSG;
			dto.setSts(1);
			dto.setCoId(contestIdStr);
			setClientMessage(dto, Messages.LIVT_CONTEST_ENDED, null, false);
			generateResponse(request, response, dto);
			return;
		} catch (Exception e) {
			e.printStackTrace();
			setClientMessage(dto, Messages.LIVT_ERR_FETCH_CON_INFO, null, false);
			generateResponse(request, response, dto);
			return;
		} finally {
			/*
			 * TrackingUtil.contestAPITracking(clientId, platForm, deviceType,
			 * request.getHeader("deviceId"), WebServiceActionType.ENDQUIZCONTEST, resMsg,
			 * contestIdStr, null, start, new Date(), request.getHeader("X-Forwarded-For"),
			 * null, dto.getSts(), null);
			 */
		}

	}

}
