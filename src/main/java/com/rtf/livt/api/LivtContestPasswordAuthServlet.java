/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rtf.common.util.Messages;
import com.rtf.livt.cass.dao.LiveContestCassDAO;
import com.rtf.livt.dto.LivtCommonDTO;
import com.rtf.livt.dvo.ContestDVO;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GsonUtil;

/**
 * The Class LivtContestPasswordAuthServlet.
 */
@WebServlet("/livtcontpasswordauth.json")
public class LivtContestPasswordAuthServlet extends RtfSaasBaseServlet {

	/**
	 *
	 */
	private static final long serialVersionUID = 5131156973564867715L;

	/**
	 * Generate Http Response for Client.Response, data is sent in JSON format.
	 *
	 * @param request        the HttpServlet request
	 * @param response       the HttpServlet response
	 * @param rtfSaasBaseDTO RTF Base Data Transfer Object
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("livtresp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();
	}

	/**
	 * Process request.Method perform validation of Contest Pass/word for User
	 *
	 * @param request  the HttpServletRequest
	 * @param response the HttpServletResponse
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String contestIdStr = request.getParameter("coId");
		String customerIdStr = request.getParameter("cuId");
		String clientIdStr = request.getParameter("clId");
		String pwd = request.getParameter("pwd");
		LivtCommonDTO respDTO = new LivtCommonDTO();
		respDTO.setClId(clientIdStr);
		respDTO.setCoId(contestIdStr);
		respDTO.setSts(0);
		try {
			if (clientIdStr == null || clientIdStr.isEmpty()) {
				setClientMessage(respDTO, Messages.INVALID_CLIENT_ID, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (contestIdStr == null || contestIdStr.isEmpty()) {
				setClientMessage(respDTO, Messages.INVALID_CONTEST_ID, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (customerIdStr == null || customerIdStr.isEmpty()) {
				setClientMessage(respDTO, Messages.INVALID_CUSTOMER_ID, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (pwd == null || pwd.isEmpty()) {
				setClientMessage(respDTO, Messages.LIVT_INCORRECT_PASS, null);
				generateResponse(request, response, respDTO);
				return;
			}

			ContestDVO co = LiveContestCassDAO.getContestByContestId(clientIdStr, contestIdStr);
			if (co == null) {
				setClientMessage(respDTO, Messages.INVALID_CONTEST_ID, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (co.getIsPwd() == null || !co.getIsPwd()) {
				setClientMessage(respDTO, Messages.INVALID_CONTEST_ID, null);
				generateResponse(request, response, respDTO);
				return;
			}
			if (!co.getConPwd().equalsIgnoreCase(pwd)) {
				setClientMessage(respDTO,  Messages.LIVT_INCORRECT_PASS, null);
				generateResponse(request, response, respDTO);
				return;
			}
			respDTO.setSts(1);
			generateResponse(request, response, respDTO);
			return;

		} catch (Exception e) {
			e.printStackTrace();
			setClientMessage(respDTO, Messages.LIVT_ERR_PASS_PROCESS, null);
			generateResponse(request, response, respDTO);
			return;
		}
	}

}
