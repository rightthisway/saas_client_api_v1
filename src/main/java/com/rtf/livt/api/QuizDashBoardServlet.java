/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;

import com.rtf.common.util.Messages;
import com.rtf.livt.dto.CassError;
import com.rtf.livt.dto.DashboardInfo;
import com.rtf.livt.util.TrackingUtil;
import com.rtf.livt.util.URLUtil;
import com.rtf.saas.util.GsonUtil;

/**
 * The Class QuizDashBoardServlet.
 */

@WebServlet("/livtGetDashboardInfo.json")
public class QuizDashBoardServlet extends HttpServlet {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new quiz dash board servlet.
	 */
	public QuizDashBoardServlet() {
		super();
	}

	/**
	 * Do get.
	 *
	 * @param request  the HttpServletRequest
	 * @param response the HttpServletResponse
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		process(request, response);

	}

	/**
	 * Do post.
	 *
	 * @param request  the HttpServletRequest
	 * @param response the HttpServletResponse
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		process(request, response);
	}

	/**
	 *  Generate Http Response for Client.Response, data is sent in JSON format.
	 *
	 * @param response      the response
	 * @param dashboardInfo the dashboard info
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	public void generateResponse(HttpServletResponse response, DashboardInfo dashboardInfo)
			throws ServletException, IOException {
		try (PrintWriter out = response.getWriter();) {
			Map<String, DashboardInfo> map = new HashMap<String, DashboardInfo>();
			map.put("dashboardInfo", dashboardInfo);
			String jsondashboardInfo = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
			// PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			response.addHeader("Access-Control-Allow-Origin", "*");
			response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
			response.addHeader("Access-Control-Allow-Headers",
					"X-Requested-With,Content-Type,x-sign,x-token,x-platform");
			out.print(jsondashboardInfo);
			out.flush();
		}
	}

	/**
	 * Process.
	 *
	 * @param request  the HttpServletRequest
	 * @param response the HttpServletResponse
	 * @return the http servlet response
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	protected HttpServletResponse process(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		DashboardInfo dashboardInfo = new DashboardInfo();
		CassError error = new CassError();
		Date start = new Date();
		String resMsg =StringUtils.EMPTY;
		String clientIdStr = request.getParameter("clId");
		String customerIdStr = request.getParameter("cuId");
		//String platForm = request.getParameter("pfm");
		//String deviceType = request.getParameter("dyType");
		//String appVersion = request.getParameter("aVn");
		//String apiHitStartTimeStr = request.getParameter("ahstm");// - api hit start time
		Boolean iserrorTrack = true;
		try {
			iserrorTrack = false;
			dashboardInfo.setSts(1);
			dashboardInfo.sethRwds(false);
			resMsg = Messages.LIVT_GEN_SUCC_MSG + customerIdStr;
		} catch (Exception e) {
			resMsg = Messages.PROCESS_ERR_LOAD_DASHBOARD;
			e.printStackTrace();
			error.setDesc(URLUtil.genericErrorMsg);
			dashboardInfo.setErr(error);
			dashboardInfo.setSts(0);
			generateResponse(response, dashboardInfo);
			return response;
		} finally {
			try {
				if(TrackingUtil.allowAllTracking || iserrorTrack) {
					/*
					 * TrackingUtil.contestAPITrackingForDeviceTimeTracking(clientIdStr, platForm,
					 * deviceType, request.getHeader("deviceId"),
					 * WebServiceActionType.GETDASHBOARDINFO, resMsg, null, customerIdStr, start,
					 * new Date(), request.getHeader("X-Forwarded-For"), appVersion,
					 * apiHitStartTimeStr, dashboardInfo.getSts(), null);
					 */
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		generateResponse(response, dashboardInfo);
		return response;
	}

}
