/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rtf.common.dao.ClientCustomerDAO;
import com.rtf.common.dvo.ClientCustomerDVO;
import com.rtf.common.util.Messages;
import com.rtf.livt.cass.dao.ContestParticipantsDAO;
import com.rtf.livt.dto.LivtCommonDTO;
import com.rtf.livt.dvo.ContestParticipantsDVO;
import com.rtf.livt.service.LivtService;
import com.rtf.livt.util.TextUtil;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GsonUtil;

/**
 * The Class LivtExitContestServlet.
 */

@WebServlet("/livtExitContest.json")
public class LivtExitContestServlet extends RtfSaasBaseServlet {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Generate Http Response for Client.Response, data is sent in JSON format.
	 *
	 * @param request        the HttpServlet request
	 * @param response       the HttpServlet response
	 * @param rtfSaasBaseDTO RTF Base Data Transfer Object
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("livtresp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();

	}

	/**
	 * Process request.Update Status of Contest Participants to EXIT for Live Contest  Status records
	 *
	 * @param request  the HttpServletRequest
	 * @param response the HttpServletResponse
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		LivtCommonDTO dto = new LivtCommonDTO();
		dto.setSts(0);
		Date start = new Date();
		String clientIdStr = request.getParameter("clId");
		String customerIdStr = request.getParameter("cuId");
		String contestIdStr = request.getParameter("coId");		
		String timedur = request.getParameter("ltd");
		//String platForm = request.getParameter("pfm");
		String resMsg = "";
		//String deviceType = request.getParameter("dyType");
		//String apiHitStartTimeStr = request.getParameter("ahstm");// - api hit start time
		Boolean iserrorTrack = true;
		Integer tdur=0;

		try {

			if (TextUtil.isEmptyOrNull(customerIdStr)) {
				resMsg = Messages.INVALID_CUSTOMER_ID + customerIdStr;
				setClientMessage(dto, resMsg, null, true);
				generateResponse(request, response, dto);
				return;
			}
			if (TextUtil.isEmptyOrNull(clientIdStr)) {
				resMsg = Messages.INVALID_CLIENT_ID + clientIdStr;
				setClientMessage(dto, Messages.INVALID_CLIENT_ID, null, true);
				generateResponse(request, response, dto);
				return;

			}
			try {
				tdur = Integer.parseInt(timedur);
			}catch(Exception ex) {
				ex.printStackTrace();
				//Ignore Exception
			}
			ClientCustomerDVO customer = null;
			ContestParticipantsDVO participant = ContestParticipantsDAO
					.getContestParticipantByContestIdAndCustomerIdForVal(clientIdStr, contestIdStr, customerIdStr);
			if (participant == null) {
				customer = ClientCustomerDAO.getClientCustomerForValidationByCustomerId(clientIdStr, customerIdStr);
				if (customer == null) {
					resMsg = Messages.INVALID_CUSTOMER_ID  + customerIdStr;
					setClientMessage(dto, Messages.INVALID_CUSTOMER_ID , null, true);
					generateResponse(request, response, dto);
					return;
				}
			}
			try {
				boolean isUserIdUpdateFlag = false;
				if (participant == null || !participant.getStatus().equalsIgnoreCase(Messages.LIVT_STATUS_EXIT)) {
					if (participant == null) {
						participant = new ContestParticipantsDVO();
						participant.setuId(customer.getUserId());
						isUserIdUpdateFlag = true;
					} else {
						if (participant.getStatus().equalsIgnoreCase(Messages.LIVT_STATUS_ACTIVE)) {
							LivtService.updateExitCounter(clientIdStr, contestIdStr);
						}
					}
					participant.setClId(clientIdStr);
					participant.setCoId(contestIdStr);
					participant.setCuId(customerIdStr);
					participant.setStatus(Messages.LIVT_STATUS_EXIT);
					participant.setExDate(new Date());
					participant.setTdur(tdur);
					if (isUserIdUpdateFlag) {
						ContestParticipantsDAO.updateForExitContestWithUserId(participant);
					} else {
						ContestParticipantsDAO.updateForExitContest(participant);
					}
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
			resMsg = Messages.LIVT_GEN_SUCC_MSG + contestIdStr + ":" + customerIdStr;
			setClientMessage(dto, "Success", null, true);
			dto.setSts(1);
			iserrorTrack = false;
			generateResponse(request, response, dto);

		} catch (Exception e) {
			resMsg = Messages.LIVT_ERR_FETCH_EXIT_STATUS;
			e.printStackTrace();
			setClientMessage(dto, Messages.LIVT_ERR_FETCH_EXIT_STATUS, null, true);
			generateResponse(request, response, dto);
			return;

		} finally {
			/*
			 * if(TrackingUtil.allowAllTracking || iserrorTrack) {
			 * TrackingUtil.contestAPITrackingForDeviceTimeTracking(clientIdStr, platForm,
			 * deviceType, request.getHeader("deviceId"), WebServiceActionType.EXITCONTEST,
			 * resMsg, contestIdStr, customerIdStr, start, new Date(),
			 * request.getHeader("X-Forwarded-For"), null, apiHitStartTimeStr, dto.getSts(),
			 * null); }
			 */
		}

	}

}
