/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.api;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Document;

import com.rtf.common.util.CommonCacheUtil;
import com.rtf.common.util.Messages;
import com.rtf.livt.dto.CassError;
import com.rtf.livt.service.LivtService;
import com.rtf.livt.util.CommonRespInfo;
import com.rtf.livt.util.URLUtil;
import com.rtf.saas.util.GsonUtil;

/**
 * The Class LoadApplicationValuesServlet.
 */
@WebServlet(urlPatterns = "/livtLoadAppValue.json", loadOnStartup = 1)

public class LoadApplicationValuesServlet extends HttpServlet {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	

	/**
	 * Execute load appp values.
	 *
	 * @throws Exception the exception
	 */
	public static void executeLoadApppValues() throws Exception {		
		CommonCacheUtil.loadApplicationValues();
		//LivtContestUtil.computeLoadTestClusterData();
	}
	/**
	 * Gets the tomcat port from config xml.
	 *
	 * @param serverXml the server xml
	 * @return the tomcat port from config xml
	 */
	public static Integer getTomcatPortFromConfigXml(File serverXml) {

		Integer port;
		try {
			DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
			domFactory.setNamespaceAware(true); // never forget this!
			DocumentBuilder builder = domFactory.newDocumentBuilder();
			Document doc = builder.parse(serverXml);
			XPathFactory factory = XPathFactory.newInstance();
			XPath xpath = factory.newXPath();
			XPathExpression expr = xpath
					.compile("/Server/Service[@name='Catalina']/Connector[count(@scheme)=0]/@port[1]");
			String result = (String) expr.evaluate(doc, XPathConstants.STRING);
			port = result != null && result.length() > 0 ? Integer.valueOf(result) : null;
		} catch (Exception e) {
			port = null;
		}
		return port;
	}

	/**
	 * Instantiates a new load application values servlet.
	 */
	public LoadApplicationValuesServlet() {
		super();
	}

	/**
	 * Do get.
	 *
	 * @param request  the HttpServletRequest
	 * @param response the HttpServletResponse
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		process(request, response);
	}

	/**
	 * Do post.
	 *
	 * @param request  the HttpServletRequest
	 * @param response the HttpServletResponse
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		process(request, response);
	}

	/**
	 * Generate HttpServletResponse for Client.Response, data is sent in JSON format.
	 *
	 * @param response       the HttpServlet response
	 * @param commonRespInfo the common resp info
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	public void generateResponse(HttpServletResponse response, CommonRespInfo commonRespInfo)
			throws ServletException, IOException {
		Map<String, CommonRespInfo> map = new HashMap<String, CommonRespInfo>();
		map.put("commonRespInfo", commonRespInfo);
		String jsondashboardInfo = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		out.print(jsondashboardInfo);
		out.flush();
	}

	/**
	 * Call the executeLoadApppValues() method which internally calls 
	 * Loading of Cassandra & SQL DB connection configuration.
	 * Fire Store Client Configuration is also loaded in cache
	 *
	 * @throws ServletException the servlet exception
	 */
	@Override
	public void init() throws ServletException {
		try {
			  executeLoadApppValues();			  
			  String nodeId=  LivtService.fetchNodeNumberForServerIP(); 
			  System.out.println(" Loaded Node Values from conf " + nodeId );
			  ServletContext appCtx = getServletConfig().getServletContext();  
			  appCtx.setAttribute("nodeId", nodeId); 
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Method to Load all the Application related Cache Data on Server Startup.
	 *
	 * @param request  the HttpServletRequest
	 * @param response the HttpServletResponse
	 * @return the http servlet response
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	protected HttpServletResponse process(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		CommonRespInfo commonRespInfo = new CommonRespInfo();
		CassError error = new CassError();
		Date start = new Date();
		String resMsg = StringUtils.EMPTY;

		String contestId = null;
		try {
			executeLoadApppValues();
			commonRespInfo.setSts(1);
			commonRespInfo.setMsg(Messages.PROCESS_SUCESS_LOAD_APP);
			resMsg = Messages.LIVT_GEN_SUCC_MSG;
		} catch (Exception e) {
			resMsg = Messages.PROCESS_ERR_LOAD_APP;
			e.printStackTrace();
			error.setDesc(URLUtil.genericErrorMsg);
			commonRespInfo.setErr(error);
			commonRespInfo.setSts(0);

			generateResponse(response, commonRespInfo);
			return response;

		} finally {
			/*
			 * TrackingUtil.contestAPITracking(null, null, null,
			 * request.getHeader("deviceId"), WebServiceActionType.LOADAPPLICATIONVALUES,
			 * resMsg, contestId, null, start, new Date(),
			 * request.getHeader("X-Forwarded-For"), null, commonRespInfo.getSts(), null);
			 */
		}
		generateResponse(response, commonRespInfo);
		return response;
	}

}
