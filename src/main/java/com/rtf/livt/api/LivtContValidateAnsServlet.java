/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rtf.common.dao.ClientCustomerDAO;
import com.rtf.common.dvo.ClientCustomerDVO;
import com.rtf.common.util.Messages;
import com.rtf.custom.dvo.RTFCustCouponCodeDVO;
import com.rtf.custom.service.RTFCouponCodeService;
import com.rtf.livt.cass.dao.CustContAnswersDAO;
import com.rtf.livt.cass.dao.CustContDtlsDAO;
import com.rtf.livt.dto.LivtContestValidateAnsDTO;
import com.rtf.livt.dvo.ContestDVO;
import com.rtf.livt.dvo.CustContAnswersDVO;
import com.rtf.livt.dvo.CustContDtlsDVO;
import com.rtf.livt.dvo.LivtContestQuestionDVO;
import com.rtf.livt.service.LivtService;
import com.rtf.livt.util.LivtContestUtil;
import com.rtf.livt.util.TextUtil;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GsonUtil;

/**
 * The Class LivtContValidateAnsServlet.
 */

@WebServlet("/livtContValidateAns.json")
public class LivtContValidateAnsServlet extends RtfSaasBaseServlet {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Generate Http Response for Client.Response, data is sent in JSON format.
	 *
	 * @param request        the HttpServlet request
	 * @param response       the HttpServlet response
	 * @param rtfSaasBaseDTO RTF Base Data Transfer Object
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("livtresp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();

	}

	/**
	 * Process request.Method to validate User Answer option selected for Live
	 * contest question
	 *
	 * @param request  the HttpServletRequest
	 * @param response the HttpServletResponse
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred./
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		LivtContestValidateAnsDTO dto = new LivtContestValidateAnsDTO();
		dto.setSts(0);
		Date start = new Date();
		String resMsg = "";
		String clientIdStr = request.getParameter("clId");
		String customerIdStr = request.getParameter("cuId");
		String contestIdStr = request.getParameter("coId");
		String questionNoStr = request.getParameter("qNo");
		String questionIdStr = request.getParameter("qId");
		String answerOption = request.getParameter("ans");
		String fbTimeStr = request.getParameter("fbctm");// - forebase callback time
		//String apiHitStartTimeStr = request.getParameter("ahstm");// - api hit start time
		//String netspeedStr = request.getParameter("netspd");// - mobile net speed

		String encKey = request.getParameter("qenck");
		String description = "";
		Boolean iserrorTrack = true;

		try {

			if (TextUtil.isEmptyOrNull(customerIdStr)) {
				resMsg = Messages.INVALID_CUSTOMER_ID + customerIdStr;
				description = "qId=" + questionIdStr + ",qNo=" + questionNoStr + ",ans=" + answerOption + ",cuId="
						+ customerIdStr + ",coId=" + contestIdStr;
				setClientMessage(dto, resMsg, null, true);
				generateResponse(request, response, dto);
				return;

			}
			if (TextUtil.isEmptyOrNull(clientIdStr)) {
				resMsg = Messages.INVALID_CLIENT_ID + clientIdStr;
				description = "qId=" + questionIdStr + ",qNo=" + questionNoStr + ",ans=" + answerOption + ",cuId="
						+ customerIdStr + ",coId=" + contestIdStr;
				setClientMessage(dto, Messages.INVALID_CLIENT_ID, null, true);
				generateResponse(request, response, dto);
				return;
			}
			if (TextUtil.isEmptyOrNull(encKey)) {
				resMsg = Messages.INVALID_ENC_KY + encKey;
				description = "qId=" + questionIdStr + ",qNo=" + questionNoStr + ",ans=" + answerOption + ",cuId="
						+ customerIdStr + ",coId=" + contestIdStr;
				setClientMessage(dto, null, null, true);
				generateResponse(request, response, dto);
				return;
			}
			ClientCustomerDVO customer = ClientCustomerDAO.getClientCustomerForValidationByCustomerId(clientIdStr,
					customerIdStr);
			if (customer == null) {
				resMsg = Messages.INVALID_CUSTOMER_ID + customerIdStr;
				setClientMessage(dto, "Customer ID not Exists.", null, true);
				generateResponse(request, response, dto);
				return;
			}
			Integer questionId = null;
			try {
				questionId = Integer.parseInt(questionIdStr.trim());
			} catch (Exception e) {
				resMsg = Messages.INVALID_QUESTION_ID + questionIdStr;
				description = "qId=" + questionIdStr + ",qNo=" + questionNoStr + ",ans=" + answerOption + ",cuId="
						+ customerIdStr + ",coId=" + contestIdStr;
				setClientMessage(dto, resMsg, null, true);
				generateResponse(request, response, dto);
				return;
			}
			Integer questionNo = null;
			try {
				questionNo = Integer.parseInt(questionNoStr.trim());
			} catch (Exception e) {
				resMsg = Messages.INVALID_QUESTION_NO + questionNoStr;
				description = "qId=" + questionIdStr + ",qNo=" + questionNoStr + ",ans=" + answerOption + ",cuId="
						+ customerIdStr + ",coId=" + contestIdStr;
				setClientMessage(dto, resMsg, null, true);
				generateResponse(request, response, dto);
				return;
			}
			ContestDVO contest = LivtContestUtil.getCurrentContestByContestId(clientIdStr, contestIdStr);
			if (contest == null) {
				resMsg = Messages.INVALID_CONTEST_ID + contestIdStr;
				description = "qId=" + questionIdStr + ",qNo=" + questionNoStr + ",ans=" + answerOption + ",cuId="
						+ customerIdStr + ",coId=" + contestIdStr;
				setClientMessage(dto, resMsg, null, true);
				generateResponse(request, response, dto);
				return;
			}
			LivtContestQuestionDVO quizQuestion = LivtContestUtil.getLivtContestQuestionById(clientIdStr, contestIdStr,
					questionId, questionNo);

			if (quizQuestion == null || !quizQuestion.getEncKey().equals(encKey)) {

				resMsg = Messages.INVALID_QUESTION_ID + questionIdStr + ":" + encKey;
				description = "qId=" + questionIdStr + ",qNo=" + questionNoStr + ",ans=" + answerOption + ",cuId="
						+ customerIdStr + ",coId=" + contestIdStr;

				setClientMessage(dto, resMsg, null, true);
				generateResponse(request, response, dto);
				return;
			}
			if (!quizQuestion.getQsnseqno().equals(questionNo)) {
				resMsg = Messages.INVALID_QUESTION_NO_OR_ID + questionNoStr;
				description = "qId=" + questionIdStr + ",qNo=" + questionNoStr + ",ans=" + answerOption + ",cuId="
						+ customerIdStr + ",coId=" + contestIdStr;
				setClientMessage(dto, resMsg, null, true);
				generateResponse(request, response, dto);
				return;
			}

			String ansResMsg = "";
			Integer sfLevelQNo = 0;
			CustContDtlsDVO custContDtls = LivtContestUtil.getCustomerAnswers(customer.getClId(), customer.getCuId(),
					contest.getCoId());
			if (sfLevelQNo < questionNo) {
				if (custContDtls != null) {
					if (custContDtls.getLqNo().equals(questionNo)) {
						description = "qId=" + questionIdStr + ",qNo=" + questionNoStr + ",ans=" + answerOption
								+ ",cuId=" + customerIdStr + ",coId=" + contestIdStr;
						resMsg = Messages.LIVT_ANSWER_ALREADY_SELECTED + questionNoStr;
						setClientMessage(dto, resMsg, null, true);
						generateResponse(request, response, dto);
						return;
					} else if (contest.getIsElimination() != null && contest.getIsElimination()) {
						if (custContDtls.getIsLqCrt() == null || !custContDtls.getIsLqCrt()) {
							description = "qId=" + questionIdStr + ",qNo=" + questionNoStr + ",ans=" + answerOption
									+ ",cuId=" + customerIdStr + ",coId=" + contestIdStr;
							resMsg = Messages.LIVT_ANSWER_PREV_WRONG_SEL + questionNoStr;
							setClientMessage(dto, Messages.LIVT_ANSWER_PREV_WRONG_SEL_CONTINE_WATCH, null, true);
							generateResponse(request, response, dto);
							return;
						}
						if (questionNo != (custContDtls.getLqNo() + 1)) {
							ansResMsg += ":Success You didn't answer last question:qId=" + questionIdStr + ",qNo="
									+ questionNoStr + ",ans=" + answerOption + ".";
						} else if (custContDtls.getIsLqCrt() == null || !custContDtls.getIsLqCrt()
								&& (null == custContDtls.getIsLqLife() || !custContDtls.getIsLqLife())) {
							ansResMsg += ":Success You didn't answer last question:qId=" + questionIdStr + ",qNo="
									+ questionNoStr + ",ans=" + answerOption + ".";

						}
					}
				} else {
					if (contest.getIsElimination() != null && contest.getIsElimination()
							&& quizQuestion.getQsnseqno() > 1) {
						ansResMsg += ":Success You didn't answer last question:qId=" + questionIdStr + ",qNo="
								+ questionNoStr + ",ans=" + answerOption + ".";
					}
				}
			}
			Boolean isAutoFlag = false;
			Integer startQno = 0, endQno = 0;
			if (custContDtls != null) {
				if (questionNo - custContDtls.getLqNo() > 1) {
					startQno = custContDtls.getLqNo() + 1;
					endQno = questionNo - 1;
					isAutoFlag = true;
				}
			} else if (questionNo > 1) {
				startQno = 1;
				endQno = questionNo - 1;
				isAutoFlag = true;
			}
			if (isAutoFlag) {
				if (sfLevelQNo >= endQno) {
					isAutoFlag = false;

				} else if (sfLevelQNo >= startQno) {
					startQno = sfLevelQNo + 1;

				}
			}
			if (isAutoFlag) {
				try {
					custContDtls = LivtContestUtil.computeAutoCreateAnswers(clientIdStr, customerIdStr, contestIdStr,
							startQno, endQno, ansResMsg, custContDtls, contest);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			Double cumulativeRewards = 0.0;
			Integer cumulativeLifeLineUsed = 0;
			Integer cumulativecustAns = 0;
			Double sflRwds = 0.0;
			if (custContDtls != null) {
				if (custContDtls.getAnsRwds() != null) {
					cumulativeRewards = custContDtls.getAnsRwds();
				}
				if (custContDtls.getCuLife() != null) {
					cumulativeLifeLineUsed = custContDtls.getCuLife();
				}
				if (custContDtls.getSflRwds() != null) {
					sflRwds = custContDtls.getSflRwds();
				}
				if (custContDtls.getCuAns() != null) {
					cumulativecustAns = custContDtls.getCuAns();
				}
			}
			boolean isCrtAns = false;
			if (contest.getCoType().equals(Messages.FEEDBACK_TYPE)) {// || !contest.getIsElimination()
				isCrtAns = true;
			} else {
				if (answerOption.equalsIgnoreCase(quizQuestion.getCorans())) {
					isCrtAns = true;
				}
			}
			if (isCrtAns) {
				dto.setIsCrt(Boolean.TRUE);
				cumulativecustAns = cumulativecustAns + 1;
				if (contest.getIsElimination() != null && contest.getIsElimination()) {
					if (contest.getqSize().equals(questionNo)) {
						LivtContestUtil.updateContestWinners(clientIdStr, contest.getCoId(), customerIdStr, contest);
					}
				} else {
					if (contest.getqSize() <= cumulativecustAns) {
						LivtContestUtil.updateContestWinners(clientIdStr, contest.getCoId(), customerIdStr, contest);
					}
				}
			}
			CustContAnswersDVO custContAns = new CustContAnswersDVO();
			custContAns.setClId(clientIdStr);
			custContAns.setCuId(customerIdStr);
			custContAns.setCoId(contestIdStr);
			custContAns.setqId(questionId);
			custContAns.setqNo(questionNo);
			custContAns.setAns(answerOption);
			custContAns.setIsCrt(dto.getIsCrt());
			custContAns.setCrDate(new Date());
			custContAns.setIsLife(false);
			custContAns.setFbCallbackTime(fbTimeStr);
			custContAns.setAnswerTime(null);
			custContAns.setRetryCount(0);
			custContAns.setIsAutoCreated(Boolean.FALSE);
			custContAns.setaRwdType(contest.getQueRwdType());
			if (isCrtAns) {
				custContAns.setaRwds(quizQuestion.getAnsRwdVal());
				if (quizQuestion.getAnsRwdVal() != null) {
					cumulativeRewards = cumulativeRewards + quizQuestion.getAnsRwdVal();

					if (sfLevelQNo >= questionNo) {
						sflRwds = sflRwds + quizQuestion.getAnsRwdVal();
					}
				}
			}
			custContAns.setCuRwds(cumulativeRewards);
			custContAns.setCuLife(cumulativeLifeLineUsed);
			CustContAnswersDAO.save(custContAns);
			LivtService.updateAnswerOptionCounter(clientIdStr, contestIdStr, questionNo, answerOption);
			if (custContDtls == null) {
				custContDtls = new CustContDtlsDVO();
				custContDtls.setCoId(custContAns.getCoId());
				custContDtls.setCuId(custContAns.getCuId());
				custContDtls.setClId(clientIdStr);
				custContDtls.setAnsRwdType(contest.getQueRwdType());
			}
			if (custContDtls.getLqNo() == null || custContDtls.getLqNo() <= custContAns.getqNo()) {
				custContDtls.setLqNo(custContAns.getqNo());
				custContDtls.setIsLqCrt(custContAns.getIsCrt());
				custContDtls.setIsLqLife(custContAns.getIsLife());
			}
			custContDtls.setAnsRwds(custContAns.getCuRwds());
			custContDtls.setCuLife(custContAns.getCuLife());
			custContDtls.setSflRwds(sflRwds);
			custContDtls.setCuAns(cumulativecustAns);
			CustContDtlsDAO.save(custContDtls);
			if (ansResMsg.isEmpty()) {
				resMsg = "Success:";
				description = "Success:Id=" + questionIdStr + ",qNo=" + questionNoStr + ",ans=" + answerOption + ".";

			} else {
				resMsg = ansResMsg;
				description = ansResMsg;
			}
			if (Messages.DISCOUNT_CODE.equalsIgnoreCase(contest.getQueRwdType())) {
				try {
					if (isCrtAns) {
						RTFCustCouponCodeDVO dvo = new RTFCustCouponCodeDVO();
						dvo.setClId(clientIdStr);
						dvo.setCuId(customerIdStr);
						dvo.setCoId(contestIdStr);
						dvo.setqId(questionId);
						dvo.setqNo(questionNo);
						dvo.setWintyp(Messages.QUEAND_ANSTYPE);
						dvo.setCpncde(quizQuestion.getCoupcde());
						dvo.setDisc(new BigDecimal(0.0));
						dvo.setRunNo(contest.getContRunningNo());
						/*if (contest.getIsElimination() != null && contest.getIsElimination()) {
							RTFCouponCodeService.createOrUpdateCouponCode(dvo);

						} else {
							if (quizQuestion.getQsnseqno() == 1 || (cumulativecustAns == questionNo)) {
								RTFCouponCodeService.createOrUpdateCouponCode(dvo);

							}
						}*/
						RTFCouponCodeService.createOrUpdateCouponCode(dvo);
					}
				} catch (Exception ex) {
					ex.printStackTrace();

				}
			}
			iserrorTrack = false;
			dto.setSts(1);
			setClientMessage(dto, "Success", null, true);
			generateResponse(request, response, dto);
			return;
		} catch (Exception e) {
			description = "qId=" + questionIdStr + ",qNo=" + questionNoStr + ",ans=" + answerOption + ",cuId="
					+ customerIdStr + ",coId=" + contestIdStr;
			e.printStackTrace();
			resMsg = "Error occurred while Fetching Validate Answer Info.";
			setClientMessage(dto, "Error occurred while Fetching Validate Answer Info.", null, true);
			generateResponse(request, response, dto);
			return;
		} finally {
			/*
			 * String deviceInfo = netspeedStr; if(TrackingUtil.allowAllTracking ||
			 * iserrorTrack) {
			 * ``	G9S	//9ts9ms
			 * .contestValidateAnswerAPITrackingForDeviceTimeTracking(
			 * clientIdStr, request.getHeader("x-platform"), null,
			 * request.getHeader("deviceId"), WebServiceActionType.CONTVALIDATEANS, resMsg,
			 * questionNoStr, questionIdStr, answerOption, contestIdStr, customerIdStr,
			 * start, new Date(), request.getHeader("X-Forwarded-For"), apiHitStartTimeStr,
			 * fbTimeStr, deviceInfo, dto.getSts(), questionNoStr, null, null, description);
			 * }
			 */
		}
	}
}
