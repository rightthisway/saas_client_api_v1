/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rtf.common.util.Messages;
import com.rtf.livt.dto.LivtCommonDTO;
import com.rtf.livt.dvo.ContestDVO;
import com.rtf.livt.service.CassToSQLMigrationService;
import com.rtf.livt.util.LivtClientDataUploadUtil;
import com.rtf.livt.util.LivtContestUtil;
import com.rtf.livt.util.LivtDataMigrationUtil;
import com.rtf.livt.util.TextUtil;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.exception.SaasProcessException;
import com.rtf.saas.util.GsonUtil;

/**
 * The Class LivtContestMigrationServlet.
 */

@WebServlet("/livtmigrateCassData.json")
public class LivtContestMigrationServlet extends RtfSaasBaseServlet {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Generate Http Response for Client.Response, data is sent in JSON format.
	 *
	 * @param request        the HttpServlet request
	 * @param response       the HttpServlet response
	 * @param rtfSaasBaseDTO RTF Base Data Transfer Object
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("livtresp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();

	}

	/**
	 * Process request.Method to perform migration of contest data
	 *
	 * @param request  the HttpServletRequest
	 * @param response the HttpServletResponse
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String clientIdStr = request.getParameter("clId");
		String contestIdStr = request.getParameter("coId");
		//String platForm = request.getParameter("pfm");
		//String deviceType = request.getParameter("deviceType");

		LivtCommonDTO dto = new LivtCommonDTO();
		dto.setSts(0);
		dto.setClId(clientIdStr);
		Date start = new Date();
		String resMsg = "";

		try {

			if (TextUtil.isEmptyOrNull(contestIdStr)) {
				resMsg = Messages.MANDATORY_PARAM_CONTEST_ID + contestIdStr;
				setClientMessage(dto, "Contest Id is mandatory.", null, false);
				generateResponse(request, response, dto);
				return;
			}

			if (TextUtil.isEmptyOrNull(clientIdStr)) {
				resMsg = Messages.MANDATORY_PARAM_CLIENT_ID + contestIdStr;
				setClientMessage(dto, "Client Id is mandatory", null, false);
				generateResponse(request, response, dto);
				return;
			}

			ContestDVO contest = LivtContestUtil.getContestByIDForStartContestFromSQL(clientIdStr, contestIdStr);
			if (contest == null) {
				resMsg = Messages.INVALID_CONTEST_ID + contestIdStr;
				setClientMessage(dto, resMsg, null, false);
				generateResponse(request, response, dto);
				return;
			}

			Integer sts = CassToSQLMigrationService.migrateCassdataToSql(clientIdStr, contest.getCoId(), contest);
			if (sts == 0)
				throw new SaasProcessException(Messages.LIVT_ERR_MIGRATION_PROCESS);

			try {
				LivtClientDataUploadUtil.uploadLivtWinnerUploadtoClient(clientIdStr, contestIdStr, contest);

			} catch (Exception e) {
				e.printStackTrace();
			}

			try {
				LivtClientDataUploadUtil.uploadCustomerRewardsToClient(clientIdStr, contestIdStr,
						contest.getContRunningNo(), false);

			} catch (Exception e) {
				e.printStackTrace();
				/*
				 * throw new SaasProcessException(
				 * "[LivtClientDataUploadUtil.uploadCustomerRewardsToClient] Error processing Migration of data"
				 * );
				 */	}
			try {
				LivtDataMigrationUtil.migrateLivtWinnersRewards(clientIdStr, contest.getCoId());
			} catch (Exception e) {
				e.printStackTrace();
				/*
				 * throw new SaasProcessException(
				 * " [LivtDataMigrationUtil.migrateLivtWinnersRewards] Error processing Migration of data"
				 * );
				 */
			}
			
			try {
				LivtDataMigrationUtil.migrateCustomerCart(clientIdStr, contest.getCoId());
			} catch (Exception e) {
				e.printStackTrace();
				/*
				 * throw new SaasProcessException(
				 * " [LivtDataMigrationUtil.migrateLivtWinnersRewards] Error processing Migration of data"
				 * );
				 */
			}
			
			
			try {
				LivtDataMigrationUtil.migrateAllContestDataToReportModule(clientIdStr, contest.getCoId());
			} catch (Exception e) {
				e.printStackTrace();
				/*
				 * throw new SaasProcessException(
				 * " [LivtDataMigrationUtil.migrateLivtWinnersRewards] Error processing Migration of data"
				 * );
				 */
			}

			resMsg = Messages.LIVT_GEN_SUCC_MSG + contestIdStr;
			setClientMessage(dto, "Migration of  Contest data is completed.", null, false);
			dto.setSts(1);
			generateResponse(request, response, dto);
			return;

		} catch (Exception e) {
			resMsg = Messages.LIVT_ERR_MIGRATION_PROCESS + contestIdStr;
			e.printStackTrace();
			setClientMessage(dto, resMsg, null, false);
			generateResponse(request, response, dto);
			return;
		} finally {
			/*
			 * TrackingUtil.contestAPITracking(clientIdStr, platForm, deviceType,
			 * request.getHeader("deviceId"), WebServiceActionType.LIVTMIGRATION, resMsg,
			 * contestIdStr, null, start, new Date(), request.getHeader("X-Forwarded-For"),
			 * null, dto.getSts(), null);
			 */

		}
	}

}
