/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rtf.common.util.Messages;
import com.rtf.livt.dto.CassError;
import com.rtf.livt.dto.LivtJoinContestDTO;
import com.rtf.livt.dvo.ContestDVO;
import com.rtf.livt.util.LivtContestUtil;
import com.rtf.livt.util.TextUtil;
import com.rtf.livt.util.URLUtil;
import com.rtf.saas.util.GsonUtil;

/**
 * The Class ContCustCountServlet.
 */

@WebServlet("/livtContCustCount.json")
public class ContCustCountServlet extends HttpServlet {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new cont cust count servlet.
	 */
	public ContCustCountServlet() {
		super();
	}

	/**
	 * Do HttpServlet DO post Method.
	 *
	 * @param request  the HttpServletRequest
	 * @param response the HttpServletResponse
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		process(request, response);
	}

	/**
	 * Generate Http Response for Client.Response, data is sent in JSON format.
	 *
	 * @param response        the response
	 * @param joinContestInfo the join contest info
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	public void generateResponse(HttpServletResponse response, LivtJoinContestDTO joinContestInfo)
			throws ServletException, IOException {
		Map<String, LivtJoinContestDTO> map = new HashMap<String, LivtJoinContestDTO>();
		map.put("cassJoinContestInfo", joinContestInfo);
		String jsondashboardInfo = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		out.print(jsondashboardInfo);
		out.flush();
	}

	/**
	 * Process.Method to feth the Count of Customer joined for a contest
	 *
	 * @param request  the HttpServletRequest
	 * @param response the HttpServletResponse
	 * @return the HttpServletResponse response
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	protected HttpServletResponse process(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		LivtJoinContestDTO joinContestInfo = new LivtJoinContestDTO();
		CassError error = new CassError();

		String clientId = request.getParameter("clId");
		String contestIdStr = request.getParameter("coId");
		//String platForm = request.getParameter("pfm");
		//String deviceType = request.getParameter("dyType");
		String resMsg = "";
		//Date start = new Date();
		//Boolean iserrorTrack = true;		
		try {

			if (TextUtil.isEmptyOrNull(clientId)) {
				resMsg = Messages.MANDATORY_PARAM_CLIENT_ID + contestIdStr;
				error.setDesc(URLUtil.genericErrorMsg);
				joinContestInfo.setSts(0);
				generateResponse(response, joinContestInfo);
				return response;
			}
			if (TextUtil.isEmptyOrNull(contestIdStr)) {
				resMsg = Messages.MANDATORY_PARAM_CONTEST_ID + contestIdStr;
				error.setDesc(URLUtil.genericErrorMsg);
				joinContestInfo.setSts(0);

				generateResponse(response, joinContestInfo);
				return response;
			}

			ContestDVO quizContest = LivtContestUtil.getCurrentContestByContestId(clientId, contestIdStr);
			if (quizContest == null) {
				resMsg = Messages.INVALID_CONTEST_ID + contestIdStr;
				error.setDesc(URLUtil.genericErrorMsg);
				joinContestInfo.setSts(0);
				generateResponse(response, joinContestInfo);
				return response;
			}
			if (quizContest.getStatus() == null || !quizContest.getStatus().equals(Messages.LIVT_CONTEST_STARTED)) {
				resMsg = Messages.LIVT_MSG_CONTEST_NOT_START + contestIdStr;
				error.setDesc(resMsg);
				joinContestInfo.setSts(0);
				generateResponse(response, joinContestInfo);
				return response;
			}

			Integer totalCustomersCount = LivtContestUtil.getContestCustomersCount(clientId, quizContest.getCoId());
			joinContestInfo.setSts(1);
			//iserrorTrack = false;
			resMsg = Messages.LIVT_GEN_SUCC_MSG + totalCustomersCount;

		} catch (Exception e) {
			resMsg = Messages.LIVT_ERR_CUST_CNT;
			e.printStackTrace();
			error.setDesc(URLUtil.genericErrorMsg);
			joinContestInfo.setSts(0);
			generateResponse(response, joinContestInfo);
			return response;
		} finally {
			/*
			 * if(TrackingUtil.allowAllTracking || iserrorTrack) {
			 * TrackingUtil.contestAPITracking(clientId, platForm, deviceType,
			 * request.getHeader("deviceId"), WebServiceActionType.CONTCUSTCOUNT, resMsg,
			 * contestIdStr, null, start, new Date(), request.getHeader("X-Forwarded-For"),
			 * null, joinContestInfo.getSts(), null); }
			 */
		}

		generateResponse(response, joinContestInfo);
		return response;
	}
}
