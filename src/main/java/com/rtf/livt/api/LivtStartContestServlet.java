/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;

import com.rtf.common.util.Messages;
import com.rtf.livt.dto.LivtCommonDTO;
import com.rtf.livt.dvo.ContestDVO;
import com.rtf.livt.util.LivtContestUtil;
import com.rtf.livt.util.TextUtil;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GsonUtil;

/**
 * The Class LivtStartContestServlet.
 */

@WebServlet("/livtStartContest.json")
public class LivtStartContestServlet extends RtfSaasBaseServlet {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Generate Http Response for Client.Response, data is sent in JSON format.
	 * 
	 * @param request
	 *            the HttpServlet request
	 * @param response
	 *            the HttpServlet response
	 * @param rtfSaasBaseDTO
	 *            RTF Base Data Transfer Object
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("livtresp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();

	}

	/**
	 * Process request.Method is used to Start a Lie Contest . The Contest
	 * Status is changed to LIVE by this method. Fire Store is also updated
	 *
	 * @param request
	 *            the HttpServletRequest
	 * @param response
	 *            the HttpServletResponse
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String clientIdStr = request.getParameter("clId");
		String contestIdStr = request.getParameter("coId");
		//String platForm = request.getParameter("platForm");
		//String deviceType = request.getParameter("deviceType");
        String startId = request.getParameter("uIdstrt");
		LivtCommonDTO dto = new LivtCommonDTO();
		dto.setSts(0);
		dto.setClId(clientIdStr);
		//Date start = new Date();
		String resMsg = StringUtils.EMPTY;
		
		try {

			if (TextUtil.isEmptyOrNull(contestIdStr)) {
				resMsg = Messages.MANDATORY_PARAM_CONTEST_ID + contestIdStr;
				setClientMessage(dto, Messages.MANDATORY_PARAM_CONTEST_ID, null, false);
				generateResponse(request, response, dto);
				return;

			}///
			/*
			 * if (TextUtil.isEmptyOrNull(startId)) { resMsg =
			 * Messages.LIVT_MSG_USERID_START_NUMBER + contestIdStr; setClientMessage(dto,
			 * Messages.LIVT_MSG_USERID_START_NUMBER, null, false);
			 * generateResponse(request, response, dto); return;
			 * 
			 * } AtomicLong startUserId = new AtomicLong(Long.valueOf(startId));
			 * 
			 * LivtContestUtil.setStartUserIdForClient(clientIdStr, startUserId);
			 */

			if (TextUtil.isEmptyOrNull(startId)) {
				resMsg = Messages.LIVT_MSG_USERID_START_NUMBER + contestIdStr;
				setClientMessage(dto, Messages.LIVT_MSG_USERID_START_NUMBER, null, false);
				generateResponse(request, response, dto);
				return;

			}
			Double startUserIdCnt = Double.parseDouble(startId);
			AtomicLong startUserId = new AtomicLong(startUserIdCnt.longValue());
			
			ServletContext appCtx = getServletContext();  
			//appCtx.setAttribute(clientIdStr, startUserId); 
			
			System.out.println("FROM SERVLETCONTEXT value set --> appCtx.getAttribute(clientIdStr)  : " +  appCtx.getAttribute(clientIdStr));
			//LivtContestUtil.setStartUserIdForClient(clientIdStr, startUserId,appCtx);
			
			this.getServletConfig().getServletContext().setAttribute("ABC", startUserId); 
			
			
		
			ContestDVO contest = LivtContestUtil.getContestByIDForStartContest(clientIdStr, contestIdStr);
			System.out.println("CONTEST DATAAAAAAAAAAAAAAAAAAAAAA  : "+contest);
			if (contest == null) {
				resMsg = Messages.INVALID_CONTEST_ID + contestIdStr;//
				setClientMessage(dto, Messages.INVALID_CONTEST_ID, null, false);
				generateResponse(request, response, dto);
				return;
			}
			System.out.println("CONTEST DATAAAAAAAAAAAAAAAAAAAAAA  : "+clientIdStr +"     |     "+contestIdStr);
			Boolean flag = LivtContestUtil.refreshStartContestData(clientIdStr, contestIdStr);
			if (!flag) {
				resMsg = Messages.LIVT_MSG_CONTEST_NOT_START + contestIdStr;
				setClientMessage(dto, resMsg, null, false);
				generateResponse(request, response, dto);
				return;
			}
			resMsg = Messages.LIVT_GEN_SUCC_MSG + contestIdStr;
			setClientMessage(dto, Messages.LIVT_MSG_CONTEST_STARTED, null, false);
			dto.setSts(1);
			dto.setCoId(contest.getCoId());
			generateResponse(request, response, dto);
			return;

		} catch (Exception e) {
			resMsg = Messages.PROCESS_ERR_START_CONT + contestIdStr;
			e.printStackTrace();
			setClientMessage(dto, resMsg, null, false);
			generateResponse(request, response, dto);
			return;
		} finally {
			/*
			 * TrackingUtil.contestAPITracking(clientIdStr, platForm, deviceType,
			 * request.getHeader("deviceId"), WebServiceActionType.STARTQUIZCONTEST, resMsg,
			 * contestIdStr, null, start, new Date(), request.getHeader("X-Forwarded-For"),
			 * null, dto.getSts(), null);
			 */
		}

	}

}
