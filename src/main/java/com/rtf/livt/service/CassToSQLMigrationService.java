/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.rtf.common.dao.CustomerCumulativeRewardsDAO;
import com.rtf.common.dvo.CustomerCumulativeRewardsDVO;
import com.rtf.common.util.DatabaseConnections;
import com.rtf.livt.cass.dao.CassRtfApiTrackingDAO;
import com.rtf.livt.cass.dao.ContestGrandWinnerCassDAO;
import com.rtf.livt.cass.dao.ContestParticipantsDAO;
import com.rtf.livt.cass.dao.ContestWinnersCassDAO;
import com.rtf.livt.cass.dao.CustContAnswersDAO;
import com.rtf.livt.cass.dao.CustContDtlsDAO;
import com.rtf.livt.cass.dao.LiveContestJackpotWinnerCassDAO;
import com.rtf.livt.dvo.CassRtfApiTracking;
import com.rtf.livt.dvo.ContestDVO;
import com.rtf.livt.dvo.ContestGrandWinnerDVO;
import com.rtf.livt.dvo.ContestJackpotWinnerDVO;
import com.rtf.livt.dvo.ContestParticipantsDVO;
import com.rtf.livt.dvo.ContestWinnerDVO;
import com.rtf.livt.dvo.CustContAnswersDVO;
import com.rtf.livt.dvo.CustContDtlsDVO;
import com.rtf.livt.sql.dao.LivtContestMigrationSQLDAO;
import com.rtf.livt.sql.dvo.LivtContestMigrationStatsDVO;
import com.rtf.livt.util.LivtContestConstants;
import com.rtf.shop.ecom.dvo.EcomCartDVO;

/**
 * The Class CassToSQLMigrationService.
 */
public class CassToSQLMigrationService {

	/** The batch size. */
	public static int batchSize = 10000;

	/**
	 * Batch insert api tracking.
	 *
	 * @param apiTrackingList the api tracking list
	 * @param cliId           the Client ID
	 * @return the integer
	 */
	private static Integer batchInsertApiTracking(List<CassRtfApiTracking> apiTrackingList, String cliId) {
		if (apiTrackingList == null || apiTrackingList.size() == 0)
			return 0;

		String sqlQuery = " INSERT INTO pt_livvx_rtf_api_tracking "
				+ "(clintid,conid,custid,apiname,crdatetime,srtdatetime,enddatetime,actresult,custipaddr,description,deviceinfo,devicetype,"
				+ " platform,sessionid,appver,fbcallbacktime,deviceapitime,resstatus,qsnno,rtcnt,anscnt,nodeid ) "
				+ " VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

		int count = 0;
		PreparedStatement pstmt = null;
		Connection con = null;
		int recSize = apiTrackingList.size();

		try {
			con = DatabaseConnections.getRtfSaasConnection();
			con.setAutoCommit(false);
			pstmt = con.prepareStatement(sqlQuery);

			for (int i = 0; i < recSize; i++) {
				CassRtfApiTracking rtfTracking = apiTrackingList.get(i);
				pstmt.setString(1, rtfTracking.getClId());
				pstmt.setString(2, rtfTracking.getContestId());
				pstmt.setString(3, rtfTracking.getCustId());
				pstmt.setString(4, rtfTracking.getApiName());
				pstmt.setString(5, null); // Created Date is always null.. ??
				try {
					if (null != rtfTracking.getStartDate()) {
						pstmt.setTimestamp(6, new Timestamp(rtfTracking.getStartDate().getTime()));
					} else {
						pstmt.setString(6, null);
					}
				} catch (Exception e) {
					pstmt.setString(6, null);
					e.printStackTrace();
				}
				try {
					if (null != rtfTracking.getEndDate()) {
						pstmt.setTimestamp(7, new Timestamp(rtfTracking.getEndDate().getTime()));
					} else {
						pstmt.setString(7, null);
					}
				} catch (Exception e) {
					pstmt.setString(7, null);
					e.printStackTrace();
				}

				pstmt.setString(8, rtfTracking.getActionResult());
				pstmt.setString(9, rtfTracking.getCustIPAddr());
				pstmt.setString(10, rtfTracking.getDescription());
				pstmt.setString(11, rtfTracking.getDeviceInfo());
				pstmt.setString(12, rtfTracking.getDeviceType());
				pstmt.setString(13, rtfTracking.getPlatForm());
				pstmt.setString(14, rtfTracking.getSessionId());
				pstmt.setString(15, rtfTracking.getAppVerion());
				try {
					if (null != rtfTracking.getFbCallbackTime() && !rtfTracking.getFbCallbackTime().isEmpty()) {
						pstmt.setTimestamp(16, new Timestamp(Long.valueOf(rtfTracking.getFbCallbackTime())));
					} else {
						pstmt.setString(16, null);
					}
				} catch (Exception e) {
					pstmt.setString(16, null);
					e.printStackTrace();
				}
				try {
					if (null != rtfTracking.getDeviceApiStartTime() && !rtfTracking.getDeviceApiStartTime().isEmpty()) {
						pstmt.setTimestamp(17, new Timestamp(Long.valueOf(rtfTracking.getDeviceApiStartTime())));
					} else {
						pstmt.setString(17, null);
					}
				} catch (Exception e) {
					pstmt.setString(17, null);
					e.printStackTrace();
				}
				pstmt.setInt(18, rtfTracking.getResStatus());
				pstmt.setString(19, rtfTracking.getQuestSlNo());
				pstmt.setInt(20, rtfTracking.getRetryCount());
				pstmt.setInt(21, rtfTracking.getAnsCount());
				pstmt.setInt(22, rtfTracking.getNodeId());
				pstmt.addBatch();
				count++;
				if (count == batchSize) {
					try {
						pstmt.executeBatch();
						con.commit();
						pstmt.clearBatch();
						count = 0;
					} catch (SQLException ex) {

						ex.printStackTrace();

					} catch (Exception ex) {
						ex.printStackTrace();
					}
				}
			}
			if (count > 0) {

				try {
					pstmt.executeBatch();
					con.commit();
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}

		} catch (Exception e) {

			e.printStackTrace();
			recSize = -1;

		} finally {
			try {
				if (pstmt != null)
					pstmt.close();
				if (con != null) {
					con.setAutoCommit(true);
					DatabaseConnections.closeConnection(con);
				}
			} catch (Exception ex) {

			}
		}

		return recSize;
	}

	/**
	 * Batch insert contest grand winners.
	 *
	 * @param custWinnerList the cust winner list
	 * @param cliId          the Client ID
	 * @param conRunningNo  the Contest  running no
	 * @return the integer
	 */
	private static Integer batchInsertContestGrandWinners(List<ContestGrandWinnerDVO> custWinnerList, String cliId,
			Integer conRunningNo) {
		if (custWinnerList == null || custWinnerList.size() == 0)
			return 0;
		String sqlQuery = "INSERT INTO pt_livvx_contest_grand_winners (clintid,conid,custid,rwdtype,rwdval,crdated,upddate,status,conrunningno) "
				+ " VALUES (?,?,?,?,?,?,?,'ACTIVE',?)";
		int count = 0;
		PreparedStatement pstmt = null;
		Connection con = null;
		int recSize = custWinnerList.size();
		try {
			con = DatabaseConnections.getRtfSaasConnection();
			con.setAutoCommit(false);
			pstmt = con.prepareStatement(sqlQuery);

			for (int i = 0; i < recSize; i++) {
				ContestGrandWinnerDVO grandWinner = custWinnerList.get(i);
				pstmt.setString(1, grandWinner.getClId());
				pstmt.setString(2, grandWinner.getCoId());
				pstmt.setString(3, grandWinner.getCuId());
				pstmt.setString(4, grandWinner.getRwdType());
				pstmt.setDouble(5, grandWinner.getRwdVal());

				if (grandWinner.getCrDate() != null) {
					pstmt.setTimestamp(6, new Timestamp(grandWinner.getCrDate().getTime()));
				} else {
					pstmt.setString(6, null);
				}
				if (grandWinner.getUpDate() != null) {
					pstmt.setTimestamp(7, new Timestamp(grandWinner.getUpDate().getTime()));
				} else {
					pstmt.setString(7, null);
				}
				pstmt.setInt(8, conRunningNo);
				pstmt.addBatch();
				count++;
				if (count == batchSize) {
					pstmt.executeBatch();
					con.commit();
					pstmt.clearBatch();
					count = 0;
				}
			}
			if (count > 0) {
				pstmt.executeBatch();
				con.commit();
			}

		} catch (Exception e) {
			e.printStackTrace();
			recSize = -1;

		} finally {
			try {
				if (pstmt != null)
					pstmt.close();
				if (con != null) {
					con.setAutoCommit(true);
					DatabaseConnections.closeConnection(con);
				}
			} catch (Exception ex) {

			}
		}

		return recSize;
	}

	/**
	 * Batch insert contest jackpot winners.
	 *
	 * @param custWinnerList the cust winner list
	 * @param cliId          the Client ID
	 * @param contRunningNo the Contest t running no
	 * @return the integer
	 */
	private static Integer batchInsertContestJackpotWinners(List<ContestJackpotWinnerDVO> custWinnerList, String cliId,
			Integer contRunningNo) {
		if (custWinnerList == null || custWinnerList.size() == 0)
			return 0;
		String sqlQuery = "INSERT INTO pt_livvx_contest_jakpot_winners (clintid,conid,custid,conqsnid,jcktype,rwdtype,rwdval,status,credate,qsnseqno,conrunningno) "
				+ " VALUES (?,?,?,?,?,?,?,?,?,?,?)";
		int count = 0;
		PreparedStatement pstmt = null;
		Connection con = null;
		int recSize = custWinnerList.size();

		try {
			con = DatabaseConnections.getRtfSaasConnection();
			con.setAutoCommit(false);
			pstmt = con.prepareStatement(sqlQuery);

			for (int i = 0; i < recSize; i++) {
				ContestJackpotWinnerDVO grandWinner = custWinnerList.get(i);
				pstmt.setString(1, grandWinner.getClId());
				pstmt.setString(2, grandWinner.getCoId());
				pstmt.setString(3, grandWinner.getCuId());
				pstmt.setInt(4, grandWinner.getqId());
				pstmt.setString(5, grandWinner.getjType());
				pstmt.setString(6, grandWinner.getRwdType());
				pstmt.setDouble(7, grandWinner.getRwdVal());
				pstmt.setString(8, grandWinner.getStatus());
				if (grandWinner.getCrDate() != null) {
					pstmt.setTimestamp(9, new Timestamp(grandWinner.getCrDate()));
				} else {
					pstmt.setString(9, null);
				}
				pstmt.setInt(10, grandWinner.getqNo());
				pstmt.setInt(11, contRunningNo);
				pstmt.addBatch();
				count++;
				if (count == batchSize) {
					pstmt.executeBatch();
					con.commit();
					pstmt.clearBatch();
					count = 0;
				}
			}
			if (count > 0) {
				pstmt.executeBatch();
				con.commit();
			}

		} catch (Exception e) {
			e.printStackTrace();
			recSize = -1;

		} finally {
			try {
				if (pstmt != null)
					pstmt.close();
				if (con != null) {
					con.setAutoCommit(true);
					DatabaseConnections.closeConnection(con);
				}
			} catch (Exception ex) {

			}
		}

		return recSize;
	}

	/**
	 * Batch insert contest winners.
	 *
	 * @param custWinnerList the cust winner list
	 * @param cliId          the Client ID
	 * @param conRunningNo  the Contest  running no
	 * @return the integer
	 */
	private static Integer batchInsertContestWinners(List<ContestWinnerDVO> custWinnerList, String cliId,
			Integer conRunningNo) {
		if (custWinnerList == null || custWinnerList.size() == 0)
			return 0;
		String sqlQuery = "INSERT INTO pt_livvx_contest_winner(clintid,conid,custid,rwdtype,rwdval,credate,upddate,status,conrunningno) "// creby,updby,upddate,gwrwdtype,gwrwdval,noofchances
				+ "VALUES (?,?,?,?,?,?,?,'ACTIVE',?)";
		int count = 0;
		PreparedStatement pstmt = null;
		Connection con = null;
		int recSize = custWinnerList.size();
		Integer processedCount = 0;

		try {
			con = DatabaseConnections.getRtfSaasConnection();
			con.setAutoCommit(false);
			pstmt = con.prepareStatement(sqlQuery);

			for (int i = 0; i < recSize; i++) {
				ContestWinnerDVO contWinner = custWinnerList.get(i);
				pstmt.setString(1, contWinner.getClId());
				pstmt.setString(2, contWinner.getCoId());
				pstmt.setString(3, contWinner.getCuId());
				pstmt.setString(4, contWinner.getRwdType());
				pstmt.setDouble(5, contWinner.getRwdVal());

				if (contWinner.getCrDate() != null) {
					pstmt.setTimestamp(6, new Timestamp(contWinner.getCrDate().getTime()));
				} else {
					pstmt.setString(6, null);
				}
				if (contWinner.getUpDate() != null) {
					pstmt.setTimestamp(7, new Timestamp(contWinner.getUpDate().getTime()));
				} else {
					pstmt.setString(7, null);
				}
				pstmt.setInt(8, conRunningNo);
				pstmt.addBatch();
				count++;
				if (count == batchSize) {
					pstmt.executeBatch();
					con.commit();
					pstmt.clearBatch();
					processedCount = processedCount + count;
					count = 0;
				}
			}
			if (count > 0) {
				pstmt.executeBatch();
				con.commit();
				processedCount = processedCount + count;
			}

		} catch (Exception e) {
			e.printStackTrace();
			recSize = -1;

		} finally {
			try {
				if (pstmt != null)
					pstmt.close();
				if (con != null) {
					con.setAutoCommit(true);
					DatabaseConnections.closeConnection(con);
				}
			} catch (Exception ex) {

			}
		}
		return processedCount;
	}

	/**
	 * Batch insert Customer contest details.
	 *
	 * @param custContDtlsList the Customer contest dtls list
	 * @param cliId            the Client ID
	 * @param conRunningNo    the Contest  running no
	 * @return the integer
	 */
	private static Integer batchInsertCustContestDetails(List<CustContDtlsDVO> custContDtlsList, String cliId,
			Integer conRunningNo) {
		if (custContDtlsList == null || custContDtlsList.size() == 0)
			return 0;
		String query = "INSERT INTO pt_livvx_customer_contest_dtls(clintid,conid,custid,ansrwdtype,ansrwdval,"
				+ "custans,custlife,lqcrt,lqlife,lqno,sflrwds,conrunningno) " + " VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
		int count = 0;
		PreparedStatement pstmt = null;
		Connection con = null;
		int recSize = custContDtlsList.size();

		try {
			con = DatabaseConnections.getRtfSaasConnection();
			con.setAutoCommit(false);
			pstmt = con.prepareStatement(query);

			for (int i = 0; i < recSize; i++) {
				CustContDtlsDVO custContDtls = custContDtlsList.get(i);

				pstmt.setString(1, custContDtls.getClId());
				pstmt.setString(2, custContDtls.getCoId());
				pstmt.setString(3, custContDtls.getCuId());
				pstmt.setString(4, custContDtls.getAnsRwdType());
				pstmt.setDouble(5, custContDtls.getAnsRwds());
				pstmt.setInt(6, custContDtls.getCuAns());
				pstmt.setInt(7, custContDtls.getCuLife());
				pstmt.setBoolean(8, custContDtls.getIsLqCrt());
				pstmt.setBoolean(9, custContDtls.getIsLqLife());
				pstmt.setInt(10, custContDtls.getLqNo());
				pstmt.setDouble(11, custContDtls.getSflRwds());
				pstmt.setInt(12, conRunningNo);

				pstmt.addBatch();
				count++;
				if (count == batchSize) {
					pstmt.executeBatch();
					con.commit();
					pstmt.clearBatch();
					count = 0;
				}
			}
			if (count > 0) {
				pstmt.executeBatch();
				con.commit();
			}

		} catch (Exception e) {
			e.printStackTrace();
			recSize = -1;

		} finally {
			try {
				if (pstmt != null)
					pstmt.close();
				if (con != null) {
					con.setAutoCommit(true);
					DatabaseConnections.closeConnection(con);
				}
			} catch (Exception ex) {

			}
		}

		return recSize;
	}

	/**
	 * Batch insert customer contest reward.
	 *
	 * @param custRwdList  the customer reward list
	 * @param cliId        the Client ID
	 * @param contestId   the Contest test id
	 * @param conRunningNothe Contest  running no
	 * @return the integer
	 */
	private static Integer batchInsertCustContRewardStats(List<CustomerCumulativeRewardsDVO> custRwdList, String cliId,
			String contestId, Integer conRunningNo) {
		if (custRwdList == null || custRwdList.size() == 0)
			return 0;
		String sqlQuery = "INSERT INTO pt_livvx_customer_contest_rwds_stats (clintid,conid,custid,conrunningno,reward_type,reward_val,live_rewards,"
				+ "prev_rwd_val,prev_live_rwds,created_date) " + " VALUES (?,?,?,?,?,?,?,?,?,getdate())";
		int count = 0;
		PreparedStatement pstmt = null;
		Connection con = null;
		int recSize = custRwdList.size();

		try {
			con = DatabaseConnections.getRtfSaasConnection();
			con.setAutoCommit(false);
			pstmt = con.prepareStatement(sqlQuery);

			for (int i = 0; i < recSize; i++) {
				CustomerCumulativeRewardsDVO custRwdObj = custRwdList.get(i);
				pstmt.setString(1, custRwdObj.getClId());
				pstmt.setString(2, contestId);
				pstmt.setString(3, custRwdObj.getCuId());
				pstmt.setInt(4, conRunningNo);
				pstmt.setString(5, custRwdObj.getRwdType());
				pstmt.setDouble(6, custRwdObj.getRwdVal());
				pstmt.setDouble(7, custRwdObj.getLiveRwdVal());
				pstmt.setDouble(8, custRwdObj.getPrevRwdVal());
				pstmt.setDouble(9, custRwdObj.getPrevLivRwdVal());

				pstmt.addBatch();
				count++;
				if (count == batchSize) {
					pstmt.executeBatch();
					con.commit();
					pstmt.clearBatch();
					count = 0;
				}
			}
			if (count > 0) {
				pstmt.executeBatch();
				con.commit();
			}

		} catch (Exception e) {
			e.printStackTrace();
			recSize = -1;

		} finally {
			try {
				if (pstmt != null)
					pstmt.close();
				if (con != null) {
					con.setAutoCommit(true);
					DatabaseConnections.closeConnection(con);
				}
			} catch (Exception ex) {

			}
		}

		return recSize;
	}

	/**
	 * Batch insert SQL live contest participants.
	 *
	 * @param partList     the part list
	 * @param clientId     the client id
	 * @param conRunningNothe Contest  running no
	 * @return the integer
	 */
	private static Integer batchInsertSQLLiveContestParticipants(List<ContestParticipantsDVO> partList, String clientId,
			Integer conRunningNo) {
		if (partList == null || partList.size() == 0)
			return 0;
		String sqlQuery = "insert into pt_livvx_contest_participants( clintid,conid,custid,jndate,exdate,ipadd,plform,status,conrunningno,timedur)"
				+ "  values (?,?,?,?,?,?,?,?,?,?)";
		int count = 0;
		PreparedStatement pstmt = null;
		Connection con = null;
		int recSize = partList.size();

		try {
			con = DatabaseConnections.getRtfSaasConnection();
			con.setAutoCommit(false);
			pstmt = con.prepareStatement(sqlQuery);

			for (int i = 0; i < recSize; i++) {
				ContestParticipantsDVO dvo = partList.get(i);
				pstmt.setString(1, dvo.getClId());
				pstmt.setString(2, dvo.getCoId());
				pstmt.setString(3, dvo.getCuId());
				if (dvo.getJnDate() != null) {
					pstmt.setTimestamp(4, new Timestamp(dvo.getJnDate().getTime()));
				} else {
					pstmt.setString(4, null);
				}
				if (dvo.getExDate() != null) {
					pstmt.setTimestamp(5, new Timestamp(dvo.getExDate().getTime()));
				} else {
					pstmt.setString(5, null);
				}
				pstmt.setString(6, dvo.getIpAdd());
				pstmt.setString(7, dvo.getPfm());
				pstmt.setString(8, dvo.getStatus());
				pstmt.setInt(9, conRunningNo);
				pstmt.setInt(10, dvo.getTdur());
				pstmt.addBatch();
				count++;
				if (count == batchSize) {

					pstmt.executeBatch();
					con.commit();
					pstmt.clearBatch();

					count = 0;
				}
			}
			if (count > 0) {
				pstmt.executeBatch();
				con.commit();
			}

		} catch (Exception e) {
			e.printStackTrace();
			recSize = -1;

		} finally {
			try {
				if (pstmt != null)
					pstmt.close();
				if (con != null) {
					con.setAutoCommit(true);
					DatabaseConnections.closeConnection(con);
				}
			} catch (Exception ex) {

			}
		}

		return recSize;
	}

	/**
	 * Batch insert SQL live cus contest answers.
	 *
	 * @param custContestAnsList the cust contest ans list
	 * @param cliId              the Client ID
	 * @param conRunningNo      the Contest  running no
	 * @return the integer
	 */
	private static Integer batchInsertSQLLiveCusContestAnswers(List<CustContAnswersDVO> custContestAnsList,
			String cliId, Integer conRunningNo) {
		if (custContestAnsList == null || custContestAnsList.size() == 0) {
			return 0;
		}

		String query = "INSERT INTO pt_livvx_customer_answer(clintid,conid,custid,qsnseqno,conqsnid,custans,"
				+ " iscrtans,ansrwdtype,ansrwdval,credate,autocreated,islife,rt_count,fb_callback_time,anstime,upddate,conrunningno ) "// ,curwds,custlife
				+ "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

		int count = 0;
		PreparedStatement pstmt = null;
		Connection con = null;
		int recSize = custContestAnsList.size();

		try {
			con = DatabaseConnections.getRtfSaasConnection();
			con.setAutoCommit(false);
			pstmt = con.prepareStatement(query);

			for (CustContAnswersDVO custAns : custContestAnsList) {
				pstmt.setString(1, custAns.getClId());
				pstmt.setString(2, custAns.getCoId());
				pstmt.setString(3, custAns.getCuId());
				pstmt.setInt(4, custAns.getqNo());
				pstmt.setInt(5, custAns.getqId());
				pstmt.setString(6, custAns.getAns());
				pstmt.setBoolean(7, custAns.getIsCrt());
				pstmt.setString(8, custAns.getaRwdType());
				pstmt.setDouble(9, custAns.getaRwds());
				if (custAns.getCrDate() != null) {
					pstmt.setTimestamp(10, new Timestamp(custAns.getCrDate().getTime()));
				} else {
					pstmt.setString(10, null);
				}
				pstmt.setBoolean(11, custAns.getIsAutoCreated());
				pstmt.setBoolean(12, custAns.getIsLife());
				pstmt.setInt(13, custAns.getRetryCount());
				try {
					if (custAns.getFbCallbackTime() != null) {
						pstmt.setTimestamp(14, new Timestamp(Long.valueOf(custAns.getFbCallbackTime())));
					} else {
						pstmt.setString(14, null);
					}
				} catch (Exception e) {
					pstmt.setString(14, null);
				}
				try {
					if (custAns.getAnswerTime() != null) {
						pstmt.setTimestamp(15, new Timestamp(Long.valueOf(custAns.getAnswerTime())));
					} else {
						pstmt.setString(15, null);
					}
				} catch (Exception e) {
					pstmt.setString(15, null);
				}

				if (custAns.getUpDate() != null) {
					pstmt.setTimestamp(16, new Timestamp(custAns.getUpDate().getTime()));
				} else {
					pstmt.setString(16, null);
				}
				pstmt.setInt(17, conRunningNo);

				pstmt.addBatch();
				count++;
				if (count == batchSize) {

					pstmt.executeBatch();
					con.commit();
					pstmt.clearBatch();
					count = 0;
				}
			}
			if (count > 0) {
				pstmt.executeBatch();
				con.commit();
			}

		} catch (Exception e) {
			e.printStackTrace();
			recSize = -1;

		} finally {
			try {
				if (pstmt != null)
					pstmt.close();
				if (con != null) {
					con.setAutoCommit(true);
					DatabaseConnections.closeConnection(con);
				}
			} catch (Exception ex) {

			}
		}

		return recSize;
	}

	/**
	 * Compute rewards accumulated for Client Customer
	 * 
	 * @param clientId    the client id
	 * @param custId      the customer id
	 * @param rwdtype     the rewardtype
	 * @param rwdVal      the reward val
	 * @param custRwdsMap the customer reward map
	 */
	public static void computeRewardObj(String clientId, String custId, String rwdtype, Double rwdVal,
			Map<String, CustomerCumulativeRewardsDVO> custRwdsMap) {

		String key = custId + "_" + rwdtype;
		CustomerCumulativeRewardsDVO custRwdObj = custRwdsMap.get(key);
		if (custRwdObj == null) {
			custRwdObj = new CustomerCumulativeRewardsDVO();
			custRwdObj.setClId(clientId);
			custRwdObj.setCuId(custId);
			custRwdObj.setRwdType(rwdtype);
			custRwdObj.setLiveRwdVal(rwdVal);
		} else {
			Double tempVal = custRwdObj.getLiveRwdVal() + rwdVal;
			custRwdObj.setLiveRwdVal(tempVal);
		}
		custRwdObj.setIsEdited(true);
		custRwdsMap.put(key, custRwdObj);
	}

	/**
	 * Contest reward credit process.
	 *
	 * @param clId         the client id
	 * @param contestId   the Contest test id
	 * @param contest     the Contest test
	 * @param conRunningNothe Contest  running no
	 * @return the integer
	 * @throws Exception the exception
	 */
	public static Integer contestRewardCreditProcess(String clId, String contestId, ContestDVO contest,
			Integer conRunningNo) throws Exception {

		/* Contest Reward Credit Process - Starts */
		LivtContestMigrationStatsDVO obj = LivtContestMigrationSQLDAO.getContestMigrationStatsByJobName(clId, contestId,
				conRunningNo, LivtContestConstants.CONT_CUST_RWDS_CREDIT_JOB);
		if (obj != null) {
			return 0;
		}
		obj = new LivtContestMigrationStatsDVO();
		obj.setClId(clId);
		obj.setCoId(contestId);
		obj.setJobName(LivtContestConstants.CONT_CUST_RWDS_CREDIT_JOB);
		obj.setStatus("STARTED");
		obj.setContRunningNo(contest.getContRunningNo());
		LivtContestMigrationSQLDAO.saveContestMigrationStats(clId, obj);

		new Date();

		List<ContestGrandWinnerDVO> grandWinnerList = ContestGrandWinnerCassDAO.getContestGrandWinnersByContestId(clId,
				contestId);
		List<ContestWinnerDVO> custWinnerList = ContestWinnersCassDAO.getContestWinnersForMigration(clId, contestId);
		List<ContestJackpotWinnerDVO> jackpotWinnersList = LiveContestJackpotWinnerCassDAO
				.getAllJackpotWinenrsForMigration(clId, contestId);
		List<CustContDtlsDVO> custContDtlsList = CustContDtlsDAO.getAllCustContDtls(clId, contestId);
		List<ContestParticipantsDVO> cassContPrticipantsList = new ArrayList<ContestParticipantsDVO>();
		if (contest.getPartiRwdVal() > 0) {
			cassContPrticipantsList = ContestParticipantsDAO.getAllParticipantsByContestId(clId, contestId);
		}

		Map<String, CustomerCumulativeRewardsDVO> custRwdsMap = CustomerCumulativeRewardsDAO
				.getCustCumulativeRewards(clId);

		for (ContestGrandWinnerDVO grandWinner : grandWinnerList) {
			if (grandWinner.getRwdVal() <= 0) {
				continue;
			}
			computeRewardObj(clId, grandWinner.getCuId(), grandWinner.getRwdType(), grandWinner.getRwdVal(),
					custRwdsMap);
		}

		for (ContestWinnerDVO winner : custWinnerList) {
			if (winner.getRwdVal() <= 0) {
				continue;
			}
			computeRewardObj(clId, winner.getCuId(), winner.getRwdType(), winner.getRwdVal(), custRwdsMap);
		}
		for (ContestJackpotWinnerDVO jackpotWinner : jackpotWinnersList) {
			if (jackpotWinner.getRwdVal() <= 0) {
				continue;
			}
			computeRewardObj(clId, jackpotWinner.getCuId(), jackpotWinner.getRwdType(), jackpotWinner.getRwdVal(),
					custRwdsMap);
		}

		for (CustContDtlsDVO custContDtls : custContDtlsList) {
			if (custContDtls.getAnsRwds() <= 0) {
				continue;
			}
			computeRewardObj(clId, custContDtls.getCuId(), custContDtls.getAnsRwdType(), custContDtls.getAnsRwds(),
					custRwdsMap);
		}
		if (contest.getPartiRwdVal() > 0 && cassContPrticipantsList != null && cassContPrticipantsList.size() > 0) {
			for (ContestParticipantsDVO participants : cassContPrticipantsList) {
				computeRewardObj(clId, participants.getCuId(), contest.getPartiRwdType(), contest.getPartiRwdVal(),
						custRwdsMap);
			}
		}
		List<CustomerCumulativeRewardsDVO> custRwdList = new ArrayList<CustomerCumulativeRewardsDVO>(
				custRwdsMap.values());
		List<CustomerCumulativeRewardsDVO> tempList = new ArrayList<CustomerCumulativeRewardsDVO>();
		List<CustomerCumulativeRewardsDVO> custRwdMigList = new ArrayList<CustomerCumulativeRewardsDVO>();
		Integer count = 0;
		for (CustomerCumulativeRewardsDVO rwdObj : custRwdList) {
			if (!rwdObj.getIsEdited()) {
				continue;
			}
			custRwdMigList.add(rwdObj);
			tempList.add(rwdObj);
			count++;

			if (count >= 1000) {
				CustomerCumulativeRewardsDAO.batchUpdateLiveRewards(tempList);
				tempList = new ArrayList<CustomerCumulativeRewardsDVO>();
				count = 0;
			}
		}
		if (count > 0) {
			CustomerCumulativeRewardsDAO.batchUpdateLiveRewards(tempList);
			tempList = new ArrayList<CustomerCumulativeRewardsDVO>();
		}

		obj.setCaasDataCount(custRwdMigList.size());
		obj.setSqlDataCount(custRwdMigList.size());
		obj.setStatus("COMPLETED");
		LivtContestMigrationSQLDAO.updateContestMigrationStatus(clId, obj);

		/* Contest Reward Credit Process - Ends */

		/* Contest Reward Stats Migration - Starts */

		obj = LivtContestMigrationSQLDAO.getContestMigrationStatsByJobName(clId, contestId, conRunningNo,
				LivtContestConstants.CONT_CUST_RWDS_STATS_MIG_JOB);
		obj = new LivtContestMigrationStatsDVO();
		obj.setClId(clId);
		obj.setCoId(contestId);
		obj.setJobName(LivtContestConstants.CONT_CUST_RWDS_STATS_MIG_JOB);
		obj.setStatus("STARTED");
		obj.setCaasDataCount(custRwdMigList.size());
		obj.setContRunningNo(contest.getContRunningNo());
		LivtContestMigrationSQLDAO.saveContestMigrationStats(clId, obj);

		Integer recsMigrated = batchInsertCustContRewardStats(custRwdMigList, clId, contestId, conRunningNo);

		obj.setSqlDataCount(recsMigrated);
		obj.setStatus("COMPLETED");
		LivtContestMigrationSQLDAO.updateContestMigrationStatus(clId, obj);

		/* Contest Reward Stats Migration - Ends */

		migrateContestGrandWinners(grandWinnerList, clId, contestId, conRunningNo, false);

		migrateContestWinners(custWinnerList, clId, contestId, conRunningNo, false);

		migrateContestJackpotWinners(jackpotWinnersList, clId, contestId, conRunningNo, false);

		migrateCustContestDtls(custContDtlsList, clId, contestId, conRunningNo, false);

		migrateContestParticipants(cassContPrticipantsList, clId, contestId, conRunningNo, false);

		return 1;
	}

	/**
	 * Migrate API tracking details.
	 *
	 * @param clientId      the client id
	 * @param contestId    the Contest test id
	 * @param contRunningNothe Contest t running no
	 * @return the integer
	 * @throws Exception the exception
	 */
	private static Integer migrateAPITrackingDetails(String clientId, String contestId, Integer contRunningNo)
			throws Exception {
		try {
			LivtContestMigrationStatsDVO obj = LivtContestMigrationSQLDAO.getContestMigrationStatsByJobName(clientId,
					contestId, contRunningNo, LivtContestConstants.CONT_RTF_API_TRACKING_MIG_JOB);
			if (obj != null) {
				return -1;
			}
			obj = new LivtContestMigrationStatsDVO();
			obj.setClId(clientId);
			obj.setCoId(contestId);
			obj.setJobName(LivtContestConstants.CONT_RTF_API_TRACKING_MIG_JOB);
			obj.setStatus("STARTED");
			obj.setContRunningNo(contRunningNo);
			LivtContestMigrationSQLDAO.saveContestMigrationStats(clientId, obj);

			List<CassRtfApiTracking> apiTrackingList = CassRtfApiTrackingDAO.getAllClientsTrackingData(clientId);
			if (apiTrackingList != null) {
				obj.setCaasDataCount(apiTrackingList.size());
			}
			Integer recsMigrated = batchInsertApiTracking(apiTrackingList, clientId);

			CassRtfApiTrackingDAO.deleteTrackingDataByClientId(clientId);

			obj.setSqlDataCount(recsMigrated);
			obj.setStatus("COMPLETED");
			LivtContestMigrationSQLDAO.updateContestMigrationStatus(clientId, obj);

			return recsMigrated;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Migrate cassdata to sql.Main M,ethod which will invoke individual Table level Migration
	 *
	 * @param clId      the cl id
	 * @param contestIdthe Contest test id
	 * @param contest  the Contest test
	 * @return the integer
	 * @throws Exception the exception
	 */
	public static Integer migrateCassdataToSql(String clId, String contestId, ContestDVO contest) throws Exception {

		Integer contRunningNo = contest.getContRunningNo();
		contestRewardCreditProcess(clId, contestId, contest, contRunningNo);
		try {
			migrateContestCustomerAnswers(null, clId, contestId, contRunningNo, true);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		try {
			migrateAPITrackingDetails(clId, contestId, contRunningNo);
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return 1;
	}

	/**
	 * Migrate contest customer answers.
	 *
	 * @param custContAnswers the customer contest answers
	 * @param clientId        the client id
	 * @param contestId      the Contest test id
	 * @param contRunningNo  the Contest t running no
	 * @param fetchRecFlag    the fetch rec flag
	 * @return the boolean
	 * @throws Exception the exception
	 */
	public static Boolean migrateContestCustomerAnswers(List<CustContAnswersDVO> custContAnswers, String clientId,
			String contestId, Integer contRunningNo, Boolean fetchRecFlag) throws Exception {

		LivtContestMigrationStatsDVO obj = LivtContestMigrationSQLDAO.getContestMigrationStatsByJobName(clientId,
				contestId, contRunningNo, LivtContestConstants.CONT_CUST_ANSWER_MIG_JOB);
		if (obj != null) {
			return false;
		}
		obj = new LivtContestMigrationStatsDVO();
		obj.setClId(clientId);
		obj.setCoId(contestId);
		obj.setJobName(LivtContestConstants.CONT_CUST_ANSWER_MIG_JOB);
		obj.setStatus("STARTED");
		obj.setContRunningNo(contRunningNo);
		if (custContAnswers != null) {
			obj.setCaasDataCount(custContAnswers.size());
		}
		LivtContestMigrationSQLDAO.saveContestMigrationStats(clientId, obj);

		if (fetchRecFlag) {
			custContAnswers = CustContAnswersDAO.getCustContAnswersByContId(clientId, contestId);
			if (custContAnswers != null) {
				obj.setCaasDataCount(custContAnswers.size());
			}
		}

		Integer recsMigrated = batchInsertSQLLiveCusContestAnswers(custContAnswers, clientId, contRunningNo);

		obj.setSqlDataCount(recsMigrated);
		obj.setStatus("COMPLETED");
		LivtContestMigrationSQLDAO.updateContestMigrationStatus(clientId, obj);

		return true;
	}

	/**
	 * Migrate contest grand winners.
	 *
	 * @param grandWinnerList the grand winner list
	 * @param clientId        the client id
	 * @param contestId      the Contest test id
	 * @param contRunningNo  the Contest t running no
	 * @param fetchRecFlag    the fetch rec flag
	 * @return the boolean
	 * @throws Exception the exception
	 */
	public static Boolean migrateContestGrandWinners(List<ContestGrandWinnerDVO> grandWinnerList, String clientId,
			String contestId, Integer contRunningNo, Boolean fetchRecFlag) throws Exception {

		LivtContestMigrationStatsDVO obj = LivtContestMigrationSQLDAO.getContestMigrationStatsByJobName(clientId,
				contestId, contRunningNo, LivtContestConstants.CONT_GRAND_WINNERS_MIG_JOB);
		if (obj != null) {
			return false;
		}
		obj = new LivtContestMigrationStatsDVO();
		obj.setClId(clientId);
		obj.setCoId(contestId);
		obj.setJobName(LivtContestConstants.CONT_GRAND_WINNERS_MIG_JOB);
		obj.setStatus("STARTED");
		obj.setContRunningNo(contRunningNo);
		if (grandWinnerList != null) {
			obj.setCaasDataCount(grandWinnerList.size());
		}
		LivtContestMigrationSQLDAO.saveContestMigrationStats(clientId, obj);

		if (fetchRecFlag) {
			grandWinnerList = ContestGrandWinnerCassDAO.getContestGrandWinnersByContestId(clientId, contestId);
			if (grandWinnerList != null) {
				obj.setCaasDataCount(grandWinnerList.size());
			}
		}

		Integer recsMigrated = batchInsertContestGrandWinners(grandWinnerList, clientId, contRunningNo);

		obj.setSqlDataCount(recsMigrated);
		obj.setStatus("COMPLETED");
		LivtContestMigrationSQLDAO.updateContestMigrationStatus(clientId, obj);

		return true;
	}

	/**
	 * Migrate contest jackpot winners.
	 *
	 * @param custJackpotWinnerList the customer jackpot winner list
	 * @param clientId             the client id
	 * @param contestId            the Contest test id
	 * @param contRunningNo        the Contest  running no
	 * @param fetchRecFlag         the fetch rec flag
	 * @return the boolean
	 * @throws Exception the exception
	 */
	public static Boolean migrateContestJackpotWinners(List<ContestJackpotWinnerDVO> custJackpotWinnerList,
			String clientId, String contestId, Integer contRunningNo, Boolean fetchRecFlag) throws Exception {

		LivtContestMigrationStatsDVO obj = LivtContestMigrationSQLDAO.getContestMigrationStatsByJobName(clientId,
				contestId, contRunningNo, LivtContestConstants.CONT_JACKPOT_WINNERS_MIG_JOB);
		if (obj != null) {
			return false;
		}
		obj = new LivtContestMigrationStatsDVO();
		obj.setClId(clientId);
		obj.setCoId(contestId);
		obj.setJobName(LivtContestConstants.CONT_JACKPOT_WINNERS_MIG_JOB);
		obj.setStatus("STARTED");
		obj.setContRunningNo(contRunningNo);
		if (custJackpotWinnerList != null) {
			obj.setCaasDataCount(custJackpotWinnerList.size());
		}
		LivtContestMigrationSQLDAO.saveContestMigrationStats(clientId, obj);
		if (fetchRecFlag) {
			custJackpotWinnerList = LiveContestJackpotWinnerCassDAO.getAllJackpotWinenrsForMigration(clientId,
					contestId);
			if (custJackpotWinnerList != null) {
				obj.setCaasDataCount(custJackpotWinnerList.size());
			}
		}

		Integer recsMigrated = batchInsertContestJackpotWinners(custJackpotWinnerList, clientId, contRunningNo);

		obj.setSqlDataCount(recsMigrated);
		obj.setStatus("COMPLETED");
		LivtContestMigrationSQLDAO.updateContestMigrationStatus(clientId, obj);

		return true;
	}

	/**
	 * Migrate contest participants.
	 *
	 * @param contParticipantsList the Contest  participants list Object ContestParticipantsDVO
	 * @param clientId            the client id
	 * @param contestId           the Contest id
	 * @param contRunningNo       the Contest running no
	 * @param fetchRecFlag        the fetch rec flag
	 * @return the boolean
	 * @throws Exception the exception
	 */
	public static Boolean migrateContestParticipants(List<ContestParticipantsDVO> contParticipantsList, String clientId,
			String contestId, Integer contRunningNo, Boolean fetchRecFlag) throws Exception {

		LivtContestMigrationStatsDVO obj = LivtContestMigrationSQLDAO.getContestMigrationStatsByJobName(clientId,
				contestId, contRunningNo, LivtContestConstants.CONT_PARTICIPANTS_MIG_JOB);
		if (obj != null) {
			return false;
		}
		obj = new LivtContestMigrationStatsDVO();
		obj.setClId(clientId);
		obj.setCoId(contestId);
		obj.setJobName(LivtContestConstants.CONT_PARTICIPANTS_MIG_JOB);
		obj.setStatus("STARTED");
		obj.setContRunningNo(contRunningNo);
		if (contParticipantsList != null) {
			obj.setCaasDataCount(contParticipantsList.size());
		}
		LivtContestMigrationSQLDAO.saveContestMigrationStats(clientId, obj);

		if (fetchRecFlag || contParticipantsList == null || contParticipantsList.isEmpty()) {
			contParticipantsList = ContestParticipantsDAO.getAllParticipantsByContestId(clientId, contestId);
			if (contParticipantsList != null) {
				obj.setCaasDataCount(contParticipantsList.size());
			}
		}

		Integer recsMigrated = batchInsertSQLLiveContestParticipants(contParticipantsList, clientId, contRunningNo);

		obj.setSqlDataCount(recsMigrated);
		obj.setStatus("COMPLETED");
		LivtContestMigrationSQLDAO.updateContestMigrationStatus(clientId, obj);

		return true;
	}

	/**
	 * Migrate contest winners.
	 *
	 * @param custWinnerList the customer winner list IObject ContestWinnerDVO
	 * @param clientId       the client id
	 * @param contestId     the Contest  id
	 * @param contRunningNo the Contest  running no
	 * @param fetchRecFlag   the fetch rec flag
	 * @return the boolean
	 * @throws Exception the exception
	 */
	public static Boolean migrateContestWinners(List<ContestWinnerDVO> custWinnerList, String clientId,
			String contestId, Integer contRunningNo, Boolean fetchRecFlag) throws Exception {

		LivtContestMigrationStatsDVO obj = LivtContestMigrationSQLDAO.getContestMigrationStatsByJobName(clientId,
				contestId, contRunningNo, LivtContestConstants.CONT_WINNER_MIG_JOB);
		if (obj != null) {
			return false;
		}
		obj = new LivtContestMigrationStatsDVO();
		obj.setClId(clientId);
		obj.setCoId(contestId);
		obj.setJobName(LivtContestConstants.CONT_WINNER_MIG_JOB);
		obj.setStatus("STARTED");
		obj.setContRunningNo(contRunningNo);
		if (custWinnerList != null) {
			obj.setCaasDataCount(custWinnerList.size());
		}
		LivtContestMigrationSQLDAO.saveContestMigrationStats(clientId, obj);

		if (fetchRecFlag) {
			custWinnerList = ContestWinnersCassDAO.getContestWinnersForMigration(clientId, contestId);
			if (custWinnerList != null) {
				obj.setCaasDataCount(custWinnerList.size());
			}
		}

		Integer recsMigrated = batchInsertContestWinners(custWinnerList, clientId, contRunningNo);

		obj.setSqlDataCount(recsMigrated);
		obj.setStatus("COMPLETED");
		LivtContestMigrationSQLDAO.updateContestMigrationStatus(clientId, obj);

		return true;
	}

	/**
	 * Migrate customer contest details.
	 *
	 * @param custDtlsList  the customer details list object CustContDtlsDVO 
	 * @param clientId      the client id
	 * @param contestId    the Contest  id
	 * @param contRunningNo the Contest  running no
	 * @param fetchRecFlag  the fetch rec flag
	 * @return the boolean
	 * @throws Exception the exception
	 */
	public static Boolean migrateCustContestDtls(List<CustContDtlsDVO> custDtlsList, String clientId, String contestId,
			Integer contRunningNo, Boolean fetchRecFlag) throws Exception {

		LivtContestMigrationStatsDVO obj = LivtContestMigrationSQLDAO.getContestMigrationStatsByJobName(clientId,
				contestId, contRunningNo, LivtContestConstants.CONT_CUST_DTLS_MIG_JOB);
		if (obj != null) {
			return false;
		}
		obj = new LivtContestMigrationStatsDVO();
		obj.setClId(clientId);
		obj.setCoId(contestId);
		obj.setJobName(LivtContestConstants.CONT_CUST_DTLS_MIG_JOB);
		obj.setStatus("STARTED");
		obj.setContRunningNo(contRunningNo);
		if (custDtlsList != null) {
			obj.setCaasDataCount(custDtlsList.size());
		}
		LivtContestMigrationSQLDAO.saveContestMigrationStats(clientId, obj);
		if (fetchRecFlag) {
			custDtlsList = CustContDtlsDAO.getAllCustContDtls(clientId, contestId);
			if (custDtlsList != null) {
				obj.setCaasDataCount(custDtlsList.size());
			}
		}
		Integer recsMigrated = batchInsertCustContestDetails(custDtlsList, clientId, contRunningNo);

		obj.setSqlDataCount(recsMigrated);
		obj.setStatus("COMPLETED");
		LivtContestMigrationSQLDAO.updateContestMigrationStatus(clientId, obj);

		return true;
	}

	/**
	 * Process cassandra to SQL  migration.Triggered Manually from API Call
	 *
	 * @param clId          the client id
	 * @param coId          the Contest id
	 * @param contest      the Contest test
	 * @param contRunningNo the Contest  running no
	 * @return the integer
	 * @throws Exception the exception
	 */
	public static Integer processCassToSQLManualMigration(String clId, String coId, ContestDVO contest,
			Integer contRunningNo) throws Exception {
		try {
			contestRewardCreditProcess(clId, coId, contest, contRunningNo);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		try {
			migrateContestGrandWinners(null, clId, coId, contRunningNo, true);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		try {
			migrateContestWinners(null, clId, coId, contRunningNo, true);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		try {
			migrateContestJackpotWinners(null, clId, coId, contRunningNo, true);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		try {
			migrateCustContestDtls(null, clId, coId, contRunningNo, true);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		try {
			migrateContestParticipants(null, clId, coId, contRunningNo, true);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		try {
			migrateContestCustomerAnswers(null, clId, coId, contRunningNo, true);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		try {
			migrateAPITrackingDetails(clId, coId, contRunningNo);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return 1;
	}
	
	
	/**
	 * Batch insert SQL live cus contest answers.
	 *
	 * @param custContestAnsList the cust contest ans list
	 * @param cliId              the Client ID
	 * @param conRunningNo      the Contest  running no
	 * @return the integer
	 */
	private static Integer batchInsertSQLLiveCusContestCart(List<CustContAnswersDVO> custContestAnsList,
			String cliId, Integer conRunningNo) {
		if (custContestAnsList == null || custContestAnsList.size() == 0) {
			return 0;
		}

		String query = "INSERT INTO pt_livvx_customer_answer(clintid,conid,custid,qsnseqno,conqsnid,custans,"
				+ " iscrtans,ansrwdtype,ansrwdval,credate,autocreated,islife,rt_count,fb_callback_time,anstime,upddate,conrunningno ) "// ,curwds,custlife
				+ "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

		int count = 0;
		PreparedStatement pstmt = null;
		Connection con = null;
		int recSize = custContestAnsList.size();

		try {
			con = DatabaseConnections.getRtfSaasConnection();
			con.setAutoCommit(false);
			pstmt = con.prepareStatement(query);

			for (CustContAnswersDVO custAns : custContestAnsList) {
				pstmt.setString(1, custAns.getClId());
				pstmt.setString(2, custAns.getCoId());
				pstmt.setString(3, custAns.getCuId());
				pstmt.setInt(4, custAns.getqNo());
				pstmt.setInt(5, custAns.getqId());
				pstmt.setString(6, custAns.getAns());
				pstmt.setBoolean(7, custAns.getIsCrt());
				pstmt.setString(8, custAns.getaRwdType());
				pstmt.setDouble(9, custAns.getaRwds());
				if (custAns.getCrDate() != null) {
					pstmt.setTimestamp(10, new Timestamp(custAns.getCrDate().getTime()));
				} else {
					pstmt.setString(10, null);
				}
				pstmt.setBoolean(11, custAns.getIsAutoCreated());
				pstmt.setBoolean(12, custAns.getIsLife());
				pstmt.setInt(13, custAns.getRetryCount());
				try {
					if (custAns.getFbCallbackTime() != null) {
						pstmt.setTimestamp(14, new Timestamp(Long.valueOf(custAns.getFbCallbackTime())));
					} else {
						pstmt.setString(14, null);
					}
				} catch (Exception e) {
					pstmt.setString(14, null);
				}
				try {
					if (custAns.getAnswerTime() != null) {
						pstmt.setTimestamp(15, new Timestamp(Long.valueOf(custAns.getAnswerTime())));
					} else {
						pstmt.setString(15, null);
					}
				} catch (Exception e) {
					pstmt.setString(15, null);
				}

				if (custAns.getUpDate() != null) {
					pstmt.setTimestamp(16, new Timestamp(custAns.getUpDate().getTime()));
				} else {
					pstmt.setString(16, null);
				}
				pstmt.setInt(17, conRunningNo);

				pstmt.addBatch();
				count++;
				if (count == batchSize) {

					pstmt.executeBatch();
					con.commit();
					pstmt.clearBatch();
					count = 0;
				}
			}
			if (count > 0) {
				pstmt.executeBatch();
				con.commit();
			}

		} catch (Exception e) {
			e.printStackTrace();
			recSize = -1;

		} finally {
			try {
				if (pstmt != null)
					pstmt.close();
				if (con != null) {
					con.setAutoCommit(true);
					DatabaseConnections.closeConnection(con);
				}
			} catch (Exception ex) {

			}
		}

		return recSize;
	}
	
	/**
	 * Batch insert Customer Cart details from CASSANDRA to SQL .
	 *
	 * @param custContestAnsList the cust contest ans list
	 * @param cliId              the Client ID
	 * @param conRunningNo      the Contest  running no
	 * @return the integer
	 */
	public static Integer batchInsertSQLLiveCusContestCart(List<EcomCartDVO> custCartlst,
			String cliId, String coId) {
		if (custCartlst == null || custCartlst.size() == 0) {
			return 0;
		}	
		
	StringBuffer sb = new StringBuffer( "  insert into pt_shopm_customer_shopping_cart ( ");
	sb.append( " clintid,conid,custid,mercid,itmsid ");
	sb.append(" , buyunits ,buyprice,cartstatus ") ;
	sb.append(" ,credate ,updby");
	sb.append(" , unittype ,slrpitms_id,pbrand ");
	sb.append(" , pname ,pprc_dtl,preg_min_prc,psel_min_prc )");
	sb.append(" values  (? , ? , ? , ? , ? , ");
	sb.append(" ? , ? , ? ,  ");
	sb.append(" ? , ? , ");
	sb.append(" ? , ? , ? , ");
	sb.append(" ? , ? , ? , ?  )");		
		int count = 0;
		PreparedStatement pstmt = null;
		Connection con = null;
		int recSize = custCartlst.size();

		try {
			con = DatabaseConnections.getRtfSaasConnection();
			con.setAutoCommit(false);
			pstmt = con.prepareStatement(sb.toString());

			for (EcomCartDVO dvo : custCartlst) {
				pstmt.setString(1, cliId);
				pstmt.setString(2, coId);
				pstmt.setString(3, dvo.getCustId());
				pstmt.setInt(4, dvo.getMercId());
				pstmt.setInt(5, dvo.getItmsId());
				
				pstmt.setInt(6, 1);
				pstmt.setDouble(7,0.0);
				pstmt.setString(8,"ACTIVE");
				
				if (dvo.getCredate() != null) {
					pstmt.setTimestamp(9, new Timestamp(dvo.getCredate().getTime()));
				} else {
					pstmt.setString(9, null);
				}
				pstmt.setString(10, "SYSTEM");
				
				pstmt.setString(11, dvo.getUnitType());				
				pstmt.setString(12, dvo.getMercProdid());
				pstmt.setString(13, dvo.getPbrand());
				
				pstmt.setString(14, dvo.getProdName());
				pstmt.setString(15, dvo.getProdPriceDtl());
				pstmt.setDouble(16, dvo.getRegPrc());
				pstmt.setDouble(17, dvo.getSellPrc());				

				pstmt.addBatch();
				count++;
				if (count == batchSize) {

					pstmt.executeBatch();
					con.commit();
					pstmt.clearBatch();
					count = 0;
				}
			}
			if (count > 0) {
				pstmt.executeBatch();
				con.commit();
			}

		} catch (Exception e) {
			e.printStackTrace();
			recSize = -1;

		} finally {
			try {
				if (pstmt != null)
					pstmt.close();
				if (con != null) {
					con.setAutoCommit(true);
					DatabaseConnections.closeConnection(con);
				}
			} catch (Exception ex) {

			}
		}

		return recSize;
	}
	
	
	/*
	 * public static void fetchAnswerOptionDetailsForReport(String clId, String
	 * conId,CustAnswerDTO dto) throws Exception {
	 * 
	 * Connection conn = null; CallableStatement stmt = null; ResultSet rs = null;
	 * try { String query =
	 * "{call spGetAudienceParticipationAnswerDetailsByShow(?,?,?)}"; conn =
	 * DatabaseConnections.getSaasAdminReportDBConnection(); stmt =
	 * conn.prepareCall(query); stmt.setString(1, clId); stmt.setString(2, conId);
	 * stmt.setInt(3, 1); rs = stmt.executeQuery();
	 * 
	 * ResultSetMetaData rsmetadata = rs.getMetaData(); int columnCount =
	 * rsmetadata.getColumnCount(); List<List<String>> dataList = new
	 * ArrayList<List<String>>(); List<String> colNames = new ArrayList<String>();
	 * String dynamicColname = "Customer Answer"; for (int i=1; i<=columnCount; i++
	 * ) { String name = rsmetadata.getColumnName(i); colNames.add(name); } while
	 * (rs.next()) { List<String> subList = new ArrayList<String> ();
	 * subList.add(rs.getString("Contest Name"));
	 * subList.add(rs.getString("Contest Date"));
	 * subList.add(rs.getString("Customer Id"));
	 * 
	 * for(int i = 1 ; i <= 10; i ++ ) { String colname = "Q" + i + " " +
	 * dynamicColname; System.out.println(colname); if(colNames.contains(colname)) {
	 * subList.add(rs.getString(colname)); } }
	 * subList.add(rs.getString("Total Answered Correct"));
	 * subList.add(rs.getString("Total Questions Attempted"));
	 * dataList.add(subList); } dto.setDatasArryList(dataList);
	 * dto.setColNameLst(colNames); return dto; } catch (Exception ex) {
	 * ex.printStackTrace(); } finally { try { rs.close(); stmt.close();
	 * conn.close(); } catch (Exception ex) { ex.printStackTrace(); } } return dto;
	 * }
	 */

}
