/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.service;

import java.io.File;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.exceptions.DriverException;
import com.rtf.livt.api.LoadApplicationValuesServlet;
import com.rtf.livt.dvo.CustContAnswersCountDVO;
import com.rtf.livt.sql.dao.SQLDaoUtil;
import com.rtf.livt.sql.dvo.RtfConfigContestClusterNodes;
import com.rtf.saas.db.CassandraConnector;
import com.rtf.saas.util.GenUtil;

/**
 * The Class LivtService.
 */
public class LivtService {

	/**
	 * Delete contest participants count data by client id.
	 *
	 * @param clientId the client id
	 */
	public static void deleteContestParticipantsCountDataByClientId(String clientId) {
		CassandraConnector.getSession().executeAsync("DELETE FROM pt_livvx_contest_participants_cnt where clintid=?",
				clientId);
	}

	/**
	 * Delete customer answers count data by client id.
	 *
	 * @param clientId the client id
	 */
	public static void deleteCustomerAnswersCountDataByClientId(String clientId) {
		CassandraConnector.getSession().executeAsync("DELETE FROM pt_livvx_customer_answer_cnt where clintid=?",
				clientId);

	}

	/**
	 * Gets the all contest P articipants count by client id.
	 *
	 * @param clientId the client id
	 * @return the all contest P articipants count by client id
	 */
	public static List<CustContAnswersCountDVO> getAllContestPArticipantsCountByClientId(String clientId) {
		final ResultSet results = CassandraConnector.getSession()
				.execute("SELECT * from pt_livvx_contest_participants_cnt WHERE clintid=?", clientId);
		List<CustContAnswersCountDVO> contestWinners = new ArrayList<CustContAnswersCountDVO>();

		if (results != null) {
			for (Row row : results) {
				contestWinners.add(new CustContAnswersCountDVO(row.getString("clintid"), row.getString("conid"),
						row.getLong("cnt")));
			}
		}
		return contestWinners;
	}

	/**
	 * Gets the all customer answers count by client id.
	 *
	 * @param clientId the client id
	 * @return the all customer answers count by client id
	 */
	public static List<CustContAnswersCountDVO> getAllCustomerAnswersCountByClientId(String clientId) {
		final ResultSet results = CassandraConnector.getSession()
				.execute("SELECT * from pt_livvx_customer_answer_cnt WHERE clintid=?", clientId);
		List<CustContAnswersCountDVO> contestWinners = new ArrayList<CustContAnswersCountDVO>();
		if (results != null) {
			for (Row row : results) {
				contestWinners.add(new CustContAnswersCountDVO(row.getString("clintid"), row.getString("conid"),
						row.getInt("qsnseqno"), row.getString("custans"), row.getLong("cnt")));
			}
		}
		return contestWinners;
	}

	/**
	 * Gets the live contest total cust count.
	 *
	 * @param clId the cl id
	 * @param coId the co id
	 * @return the live contest total cust count
	 */
	public static Integer getLiveContestTotalCustCount(String clId, String coId) {
		Integer count = 0;
		try {
			final ResultSet results = CassandraConnector.getSession().execute(
					"SELECT cnt as cCount from pt_livvx_contest_participants_cnt WHERE clintid=? and conid=? ", clId,
					coId);
			Row row = results.one();
			if (row != null) {
				Long value = row.getLong("cCount");
				count = value.intValue();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return count;
	}

	/**
	 * Reset contest participants count data by client id.
	 *
	 * @param clientId the client id
	 * @param coID     the co ID
	 * @param count    the count
	 */
	public static void resetContestParticipantsCountDataByClientId(String clientId, String coID, Long count) {
		Session session = CassandraConnector.getSession();
		try {
			String sql = " update pt_livvx_contest_participants_cnt set cnt=cnt-? where clintid=? and conid=? ";
			PreparedStatement statement = session.prepare(sql.toString());
			BoundStatement boundStatement = statement.bind(count, clientId, coID);
			session.executeAsync(boundStatement);

		} catch (final DriverException de) {
			de.printStackTrace();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	/**
	 * Reset customer answers count data by client id.
	 *
	 * @param clId   the cl id
	 * @param coID   the co ID
	 * @param qNo    the q no
	 * @param usrAns the usr ans
	 * @param count  the count
	 */
	public static void resetCustomerAnswersCountDataByClientId(String clId, String coID, Integer qNo, String usrAns,
			Long count) {
		Session session = CassandraConnector.getSession();
		try {
			String sql = " update pt_livvx_customer_answer_cnt set cnt=cnt-? where clintid=? and conid=? and qsnseqno=? and custans=? ";
			PreparedStatement statement = session.prepare(sql.toString());
			BoundStatement boundStatement = statement.bind(count, clId, coID, qNo, usrAns);
			session.executeAsync(boundStatement);

		} catch (final DriverException de) {
			de.printStackTrace();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	/**
	 * Update answer option counter.
	 *
	 * @param clId   the cl id
	 * @param coID   the co ID
	 * @param qNo    the q no
	 * @param usrAns the usr ans
	 * @throws Exception the exception
	 */
	public static void updateAnswerOptionCounter(String clId, String coID, Integer qNo, String usrAns)
			throws Exception {
		Session session = CassandraConnector.getSession();
		try {
			String sql = " update pt_livvx_customer_answer_cnt set cnt=cnt+1 where clintid=? and conid=? and qsnseqno=? and custans=? ";
			PreparedStatement statement = session.prepare(sql.toString());
			BoundStatement boundStatement = statement.bind(clId, coID, qNo, usrAns);
			session.executeAsync(boundStatement);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Update exit counter.
	 *
	 * @param clId the cl id
	 * @param coID the co ID
	 */
	public static void updateExitCounter(String clId, String coID) {
		Session session = CassandraConnector.getSession();
		try {
			String sql = " update pt_livvx_contest_participants_cnt set cnt=cnt-1 where clintid=? and conid=? ";
			PreparedStatement statement = session.prepare(sql.toString());
			BoundStatement boundStatement = statement.bind(clId, coID);
			session.executeAsync(boundStatement);

		} catch (final DriverException de) {
			de.printStackTrace();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	/**
	 * Update join counter.
	 *
	 * @param clId the cl id
	 * @param coID the co ID
	 * @throws Exception the exception
	 */
	public static void updateJoinCounter(String clId, String coID) throws Exception {
		Session session = CassandraConnector.getSession();

		try {
			String sql = " update pt_livvx_contest_participants_cnt  set cnt=cnt+1 where clintid=? and conid=? ";
			PreparedStatement statement = session.prepare(sql.toString());
			BoundStatement boundStatement = statement.bind(clId, coID);
			session.executeAsync(boundStatement);

		} catch (final DriverException de) {
			de.printStackTrace();
			throw de;

		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}

	}
	
	public static Map<String,List<RtfConfigContestClusterNodes>> fetchAllClusterNodes() throws Exception{
		List<RtfConfigContestClusterNodes> list = SQLDaoUtil.getAllRtfConfigClusterNodeDetail();		
		if(list == null ) throw new Exception(" CLUSTER NODE CONF NOT SET FOR  CLIENTS");		
		Map<String,List<RtfConfigContestClusterNodes>> allClientMap = new LinkedHashMap<String,List<RtfConfigContestClusterNodes>> () ;
		for(RtfConfigContestClusterNodes nodedets:list) {
			String clintId = nodedets.getClintId();
			List<RtfConfigContestClusterNodes> nodeList = allClientMap.get(clintId);
			if(nodeList == null) {
				allClientMap.put(clintId, new ArrayList<RtfConfigContestClusterNodes>());
			}			
			allClientMap.get(clintId).add(nodedets);
		}
		return allClientMap;	
	}

	/*
	 * public static Integer fetchNodeNumberForServerIP(String ip , Integer port )
	 * throws Exception{ Map<String,List<RtfConfigContestClusterNodes>> allClientMap
	 * = fetchAllClusterNodes(); Set<String> clindIdskset = allClientMap.keySet();
	 * Iterator<String> itr = clindIdskset.iterator(); Integer index = 0; outerloop:
	 * while(itr.hasNext()) { String clintId = itr.next();
	 * List<RtfConfigContestClusterNodes> nodedetlst = allClientMap.get(clintId);
	 * for(RtfConfigContestClusterNodes nodedet:nodedetlst) { URL url = new
	 * URL(nodedet.getUrl()); String dbIp = url.getHost(); Integer dbPort =
	 * url.getPort();
	 * 
	 * if(port == dbPort && ip.equals(dbIp)) { index = nodedetlst.indexOf(nodedet);
	 * System.out.println(" Matches at index " + index);
	 * System.out.println("IP AND PORT  " + port + ": DB PORT " + dbPort +" -- " +
	 * ip + ": DBIP " + dbIp); break outerloop; } }
	 * 
	 * }
	 * 
	 * System.out.println(" Matches at index " + index); return index + 1; }
	 */
	/*
	 * public static void fetchCustIdStartRangeForServerIP() throws Exception {
	 * 
	 * List<String> ipPortDet = getServerURI(); String ip = ipPortDet.get(0);
	 * Integer portNo = Integer.valueOf(ipPortDet.get(1)); Integer startNo =
	 * fetchNodeNumberForServerIP(ip, portNo) ;
	 * 
	 * }
	 */
	
	/*
	 * public static Integer fetchNodeNumberForServerIP(String ip , Integer port)
	 * throws Exception{ Map<String,List<RtfConfigContestClusterNodes>> allClientMap
	 * = fetchAllClusterNodes(); Integer index = 0;
	 * List<RtfConfigContestClusterNodes> nodedetlst = allClientMap.get(clintId);
	 * for(RtfConfigContestClusterNodes nodedet:nodedetlst) { URL url = new
	 * URL(nodedet.getUrl()); String dbIp = url.getHost(); Integer dbPort =
	 * url.getPort(); if(port == dbPort && ip.equals(dbIp)) { index =
	 * nodedetlst.indexOf(nodedet); System.out.println(" Matches at index " +
	 * index); System.out.println("IP AND PORT  " + port + ": DB PORT " + dbPort
	 * +" -- " + ip + ": DBIP " + dbIp); } }
	 * 
	 * System.out.println(" Matches at index " + index); return index + 1; }
	 */
	

	public static String getServerURI() throws Exception {
		String location = System.getProperty("catalina.base") + "/conf/server.xml";
		File serverXml = new File(location);
		Integer port = LoadApplicationValuesServlet.getTomcatPortFromConfigXml(serverXml);
		System.out.println("[CLUSTERNODE IP ]" + InetAddress.getLocalHost().getHostAddress());
		System.out.println("[CLUSTERNODE PORT ]" + port);
		return InetAddress.getLocalHost().getHostAddress() + "/" + port;
	}
	
	public static String fetchNodeNumberForServerIP() throws Exception {		
		String uriPath = getServerURI() ;		
		if(GenUtil.isNullOrEmpty(uriPath) ){
			System.out.println(" NODE/PORT  IS NULL : COULD NOT READ VALUE FROM SERVER CONFIG " + uriPath );			
			throw new Exception(" NODE/PORT IS NULL : COULD NOT READ VALUE FROM SERVER CONFIG " +  uriPath );
		}
		RtfConfigContestClusterNodes node = SQLDaoUtil.getRtfConfigClusterNodeDetailforPortnIP(uriPath);		
		if(node == null || GenUtil.isNullOrEmpty(node.getSharedId())) {
			System.out.println(" NODE details IS NULL : ADD ENTRY IN CLUSTER CONF TABLE FOR " + uriPath);
			throw new Exception(" NODE details IS NULL : ADD ENTRY IN CLUSTER CONF TABLE FOR " + uriPath);
		}
		return node.getSharedId();
	}
	
	
}
