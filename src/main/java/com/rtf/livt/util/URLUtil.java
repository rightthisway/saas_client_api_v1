/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.util;

import java.io.File;
import java.net.InetAddress;
import java.text.SimpleDateFormat;
import java.util.List;

import com.rtf.livt.api.LoadApplicationValuesServlet;
import com.rtf.livt.sql.dao.SQLDaoUtil;
import com.rtf.livt.sql.dvo.RtfConfigContestClusterNodes;

/**
 * The Class URLUtil.
 */
public class URLUtil {

	/** The date format. */

	/** The generic error msg. */
	public static String genericErrorMsg = "Something went wrong, Please try again.";

	/** The base path. */
	public static String basePath = "";

	/** The email image base path. */
	public static String emailImageBasePath = "";

	/** The event share link base url. */
	public static String eventShareLinkBaseUrl = "";

	/** The api server svg maps base URL. */
	public static String apiServerSvgMapsBaseURL = "";

	/** The dp prfeix code. */
	public static String DP_PRFEIX_CODE = "";

	/** The dp folder name. */
	public static String DP_FOLDER_NAME = "";

	/** The reward the fan server svg maps base url. */
	public static String rewardTheFanServerSvgMapsBaseUrl;

	/** The api server base url. */
	public static String apiServerBaseUrl;

	/** The reward the fan server base url. */
	public static String rewardTheFanServerBaseUrl;

	/** The paypal environment. */
	public static String paypalEnvironment;

	/** The default dp prfeix code. */
	public static String DEFAULT_DP_PRFEIX_CODE = "";

	/** The default dp folder name. */
	public static String DEFAULT_DP_FOLDER_NAME = "";

	/** The from email. */
	public static String fromEmail;

	/** The bcc emails. */
	public static String bccEmails;

	/** The from email for promo. */
	public static String fromEmailForPromo;

	/** The rtf image shared path. */
	public static String rtfImageSharedPath;

	/** The rtf shared drive user name. */
	public static String rtfSharedDriveUserName;

	/** The rtf shared drive password. */
	public static String rtfSharedDrivePassword;

	/** The is production environment. */
	public static Boolean isProductionEnvironment;

	/** The is master node server. */
	public static Boolean isMasterNodeServer;

	/** The is shared drive on. */
	public static Boolean isSharedDriveOn = false;

	/** The svg text file path. */
	public static String svgTextFilePath;

	/** The image server URL. */
	public static String imageServerURL;

	/** The customer dp directory. */
	public static String CUSTOMER_DP_DIRECTORY = "dp";

	/** The aws S 3 url. */
	public static String awsS3Url = "https://rtfservermedia.s3.amazonaws.com/";

	/** The cluster node id. */
	public static Integer clusterNodeId = 0;

	/** The shared id. */
	public static String sharedId;

	/**
	 * Compute cluster node id.
	 *
	 * @throws Exception the exception
	 */
	public static void computeClusterNodeId() throws Exception {
		String ipAddress = InetAddress.getLocalHost().getHostAddress();
		String tomcatBaseDirectory = System.getProperty("catalina.base");
		String nodeDirectoryPath = ipAddress + "/" + tomcatBaseDirectory;

		String location = tomcatBaseDirectory + "/conf/server.xml";
		File serverXml = new File(location);
		Integer port = LoadApplicationValuesServlet.getTomcatPortFromConfigXml(serverXml);
		nodeDirectoryPath = ipAddress + "/" + port;

		List<RtfConfigContestClusterNodes> clusterNodeList = SQLDaoUtil.getAllRtfConfigClusterNodeDetailsBudweiser();
		if (clusterNodeList != null) {
			for (RtfConfigContestClusterNodes clusterNode : clusterNodeList) {
				if (clusterNode.getDirectoryPath() != null
						&& clusterNode.getDirectoryPath().equalsIgnoreCase(nodeDirectoryPath)) {
					clusterNodeId = clusterNode.getId();
					sharedId = clusterNode.getSharedId();
					break;
				}
			}
		}
	}

	/**
	 * Gets the cards image path.
	 *
	 * @return the cards image path
	 */
	public static String getCardsImagePath() {
		if (URLUtil.isSharedDriveOn) {
			return rtfImageSharedPath + "SvgMaps//Cards//";
		} else {
			return basePath + "SvgMaps//Cards//";
		}
	}

	/**
	 * Gets the customer profile image path.
	 *
	 * @return the customer profile image path
	 */
	public static String getCustomerProfileImagePath() {
		if (URLUtil.isSharedDriveOn) {
			return rtfImageSharedPath + "SvgMaps//" + DP_FOLDER_NAME + "//";
		} else {
			return basePath + "SvgMaps//" + DP_FOLDER_NAME + "//";
		}
	}

	/**
	 * Gets the customer profile pic url.
	 *
	 * @param fileName the file name
	 * @return the customer profile pic url
	 */
	public static String getCustomerProfilePicUrl(String fileName) {
		return "http://52.201.48.95:90/staticfiles/" + fileName;
	}

	/**
	 * Gets the default profile pic file.
	 *
	 * @param defaultImagePath the default image path
	 * @return the default profile pic file
	 */
	public static String getDefaultProfilePicFile(String defaultImagePath) {
		String fileLocation = "";
		try {
			fileLocation = URLUtil.getSVGMapPath() + DEFAULT_DP_FOLDER_NAME + "//" + defaultImagePath + ".jpg";
		} catch (Exception e) {
			e.printStackTrace();
		}
		return fileLocation;
	}

	/**
	 * Gets the explore cards image path.
	 *
	 * @return the explore cards image path
	 */
	public static String getExploreCardsImagePath() {
		if (URLUtil.isSharedDriveOn) {
			return rtfImageSharedPath + "SvgMaps//Cards//Explore//";
		} else {
			return basePath + "SvgMaps//Cards//Explore//";
		}
	}

	/**
	 * Gets the payment icon image path.
	 *
	 * @return the payment icon image path
	 */
	public static String getPaymentIconImagePath() {
		if (URLUtil.isSharedDriveOn) {
			return rtfImageSharedPath + "SvgMaps//PaymentIcons//";
		} else {
			return basePath + "SvgMaps//PaymentIcons//";
		}
	}

	/**
	 * Gets the profile pic file.
	 *
	 * @param custImagePath the cust image path
	 * @return the profile pic file
	 */
	public static String getProfilePicFile(String custImagePath) {
		String fileLocation = "";
		try {
			fileLocation = getSVGMapPath() + DP_FOLDER_NAME + "//" + custImagePath;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return fileLocation;
	}

	/**
	 * Gets the profile pic folder path not using.
	 *
	 * @return the profile pic folder path not using
	 */
	public static String getProfilePicFolderPathNotUsing() {
		return URLUtil.basePath + "SvgMaps//" + DP_FOLDER_NAME + "//";
	}

	/**
	 * Gets the promotional dialog image path.
	 *
	 * @return the promotional dialog image path
	 */
	public static String getPromotionalDialogImagePath() {
		if (URLUtil.isSharedDriveOn) {
			return rtfImageSharedPath + "SvgMaps//PromotionalDialog//";
		} else {
			return basePath + "SvgMaps//PromotionalDialog//";
		}
	}

	/**
	 * Gets the regular image path.
	 *
	 * @return the regular image path
	 */
	public static String getRegularImagePath() {
		if (URLUtil.isSharedDriveOn) {
			return rtfImageSharedPath + "SvgMaps//";
		} else {
			return basePath + "SvgMaps//";
		}
	}

	/**
	 * Gets the stored svg api url.
	 *
	 * @return the stored svg api url
	 */
	public static String getStoredSvgApiUrl() {
		return apiServerSvgMapsBaseURL + "Stored_SVG/";
	}

	/**
	 * Gets the stored SVG map path.
	 *
	 * @return the stored SVG map path
	 */
	public static String getStoredSVGMapPath() {
		if (URLUtil.isSharedDriveOn) {
			return rtfImageSharedPath + "SvgMaps//Stored_SVG//";
		} else {
			return basePath + "SvgMaps//Stored_SVG//";
		}
	}

	/**
	 * Gets the SVG map path.
	 *
	 * @return the SVG map path
	 */
	public static String getSVGMapPath() {
		if (URLUtil.isSharedDriveOn) {
			return rtfImageSharedPath + "SvgMaps//";
		} else {
			return basePath + "SvgMaps//";
		}
	}

	/**
	 * Gets the SVG text folder path.
	 *
	 * @return the SVG text folder path
	 */
	public static String getSVGTextFolderPath() {
		if (URLUtil.isSharedDriveOn) {
			return rtfImageSharedPath + "Texts//";
		} else {
			return "/rtw/SVG//Texts/";
		}
	}

	/**
	 * Load application values.
	 *
	 * @throws Exception the exception
	 */
	public static void loadApplicationValues() throws Exception {

		try {
			setZonesPropertValues();

		} catch (Exception e) {
			basePath = "////C://Tomcat 7.0_Tomcat7.production//webapps//";
			emailImageBasePath = "////C://Tomcat 7.0_Tomcat7.production//webapps//ROOT//resources//email-resources//";
			eventShareLinkBaseUrl = "https://www.rewardthefan.com/";
			apiServerSvgMapsBaseURL = "https://api.rewardthefan.com/SvgMaps/";
			DP_PRFEIX_CODE = "REWARDTHEFAN_DP";
			DP_FOLDER_NAME = "CUSTOMER_DP";
			DEFAULT_DP_PRFEIX_CODE = "RTF_DEFAULT_DP";
			DEFAULT_DP_FOLDER_NAME = "DEFAULT_DP";
			rtfImageSharedPath = "smb://10.0.0.91//d$//Rewardthefan//";
			rtfSharedDriveUserName = "dev";
			rtfSharedDrivePassword = "Rb#32H8!";
			isProductionEnvironment = true;
			isMasterNodeServer = false;
			isSharedDriveOn = false;
			svgTextFilePath = "//rtw//SVG//Texts//";
			imageServerURL = "http://18.213.28.88";
			e.printStackTrace();
		}
	}

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 * @throws Exception the exception
	 */
	public static void main(String[] args) throws Exception {
		computeClusterNodeId();
	}

	/**
	 * Profile pic base directory.
	 *
	 * @return the string
	 */
	public static String profilePicBaseDirectory() {
		String fileLocation = "";
		try {
			fileLocation = getSVGMapPath() + DP_FOLDER_NAME + "//";
		} catch (Exception e) {
			e.printStackTrace();
		}
		return fileLocation;
	}

	/**
	 * Profile pic for summary.
	 *
	 * @param fileName the file name
	 * @return the string
	 */
	public static String profilePicForSummary(String fileName) {
		String fileUrl = awsS3Url + CUSTOMER_DP_DIRECTORY + "/" + fileName;
		return fileUrl;
	}

	/**
	 * Profile pic for summary not using.
	 *
	 * @param fileName the file name
	 * @return the string
	 */
	public static String profilePicForSummaryNotUsing(String fileName) {
		String fileLocation = "";
		try {
			if (URLUtil.isSharedDriveOn) {
				fileLocation = getCustomerProfilePicUrl(fileName);
			} else {
				if (URLUtil.isProductionEnvironment) {
					// fileLocation = apiServerSvgMapsBaseURL+DP_FOLDER_NAME+"/"+fileName;
					fileLocation = imageServerURL + "/" + fileName;
				} else {
					fileLocation = apiServerSvgMapsBaseURL + DP_FOLDER_NAME + "/" + fileName;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return fileLocation;
	}

	/**
	 * Profile pic upload directory.
	 *
	 * @param profilePicPrefix the profile pic prefix
	 * @param customerId       the customer id
	 * @param ext              the ext
	 * @return the string
	 */
	public static String profilePicUploadDirectory(String profilePicPrefix, Integer customerId, String ext) {
		String fileLocation = "";
		try {
			fileLocation = getSVGMapPath() + DP_FOLDER_NAME + "//" + profilePicPrefix + "_" + customerId + "." + ext;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return fileLocation;
	}

	/**
	 * Profile pic upload directory old.
	 *
	 * @param profilePicPrefix the profile pic prefix
	 * @param customerId       the customer id
	 * @param ext              the ext
	 * @return the string
	 */
	public static String profilePicUploadDirectoryOld(String profilePicPrefix, Integer customerId, String ext) {
		String fileLocation = "";
		try {
			fileLocation = URLUtil.basePath + "SvgMaps//" + DP_FOLDER_NAME + "//" + profilePicPrefix + "_" + customerId
					+ "." + ext;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return fileLocation;
	}

	/**
	 * Profile pic web UR by image name.
	 *
	 * @param fileName the file name
	 * @return the string
	 */
	public static String profilePicWebURByImageName(String fileName) {
		String fileLocation = "";
		try {
			if (URLUtil.isSharedDriveOn) {
				fileLocation = getCustomerProfilePicUrl(fileName);
			} else {
				fileLocation = URLUtil.apiServerSvgMapsBaseURL + DP_FOLDER_NAME + "/" + fileName;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return fileLocation;
	}

	/**
	 * Sets the zones propert values.
	 */
	public static void setZonesPropertValues() {
		// ResourceBundle resourceBundle = ResourceBundle.getBundle("commonsettings");

		// properties = zonesProperty;
	}
}