/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.rtf.common.dao.RTFDataMigrationDAO;
import com.rtf.common.dvo.RTFCustomerDVO;
import com.rtf.livt.dvo.ContestDVO;
import com.rtf.livt.dvo.ContestGrandWinnerDVO;
import com.rtf.livt.dvo.ContestWinnerDVO;
import com.rtf.livt.dvo.CustContAnswersDVO;
import com.rtf.livt.service.CassToSQLMigrationService;
import com.rtf.livt.sql.dao.LiveContestSQLDAO;
import com.rtf.livt.sql.dao.LivtContestGrandWinnerDAO;
import com.rtf.livt.sql.dao.LivtContestWinnerDAO;
import com.rtf.livt.sql.dao.LivtCustomerAnswerDAO;
import com.rtf.livt.sql.dao.SQLDaoUtil;
import com.rtf.shop.ecom.dao.EcomCustomerCartCassDAO;
import com.rtf.shop.ecom.dvo.EcomCartDVO;

/**
 * The Class LivtDataMigrationUtil.
 */
public class LivtDataMigrationUtil {

	/**
	 * Migrate livt winners rewards.
	 *
	 * @param clId the client id
	 * @param coId the contest id
	 */
	public static void migrateLivtWinnersRewards(String clId, String coId) {
		if (clId != null && !clId.equalsIgnoreCase("PROD2020REWARDTHEFAN")) {
			return;
		}
		Map<String, Double> rwdMap = new HashMap<String, Double>();
		List<RTFCustomerDVO> customers = new ArrayList<RTFCustomerDVO>();
		try {
			ContestDVO contest = LiveContestSQLDAO.getContestByContestId(clId, coId);
			if (contest == null) {
				return;
			}

			if (contest.getQueRwdType() != null && contest.getQueRwdType().equalsIgnoreCase("POINTS")) {
				List<CustContAnswersDVO> crtAnsList = LivtCustomerAnswerDAO.getAllCorrectAnswerByCoId(clId, coId);
				for (CustContAnswersDVO ans : crtAnsList) {
					if (ans.getaRwdType() != null && ans.getaRwdType().equalsIgnoreCase("POINTS")) {
						if (rwdMap.get(ans.getCuId().trim()) != null) {
							rwdMap.put(ans.getCuId().trim(), rwdMap.get(ans.getCuId().trim()) + ans.getaRwds());
						} else {
							rwdMap.put(ans.getCuId().trim(), ans.getaRwds());
						}
					}
				}
			}

			if (contest.getSumRwdType() != null && contest.getSumRwdType().equalsIgnoreCase("POINTS")) {
				List<ContestWinnerDVO> winners = LivtContestWinnerDAO.getAllWinnersByCoId(clId, coId);
				for (ContestWinnerDVO win : winners) {
					if (win.getRwdType() != null && win.getRwdType().equalsIgnoreCase("POINTS")) {
						if (rwdMap.get(win.getCuId().trim()) != null) {
							rwdMap.put(win.getCuId().trim(), rwdMap.get(win.getCuId().trim()) + win.getRwdVal());
						} else {
							rwdMap.put(win.getCuId().trim(), win.getRwdVal());
						}
					}
				}
			}

			if (contest.getWinRwdType() != null && contest.getWinRwdType().equalsIgnoreCase("POINTS")
					&& contest.getIsLotEnbl()) {
				List<ContestGrandWinnerDVO> gWinners = LivtContestGrandWinnerDAO.getAllWinnersByCoId(clId, coId);
				for (ContestGrandWinnerDVO win : gWinners) {
					if (win.getRwdType() != null && win.getRwdType().equalsIgnoreCase("POINTS")) {
						if (rwdMap.get(win.getCuId().trim()) != null) {
							rwdMap.put(win.getCuId().trim(), rwdMap.get(win.getCuId().trim()) + win.getRwdVal());
						} else {
							rwdMap.put(win.getCuId().trim(), win.getRwdVal());
						}
					}
				}
			}

			String insertQuery = null;
			String updQuery = null;
			Integer result = null;
			RTFCustomerDVO cust = null;
			if (!rwdMap.isEmpty()) {
				for (Entry<String, Double> map : rwdMap.entrySet()) {
					try {
						cust = RTFDataMigrationDAO.getRTFCustId(map.getKey().trim());
						if (cust == null || cust.getId() == null) {
							continue;
						}
						cust.setRtfPoints(cust.getRtfPoints() + map.getValue().intValue());
						insertQuery = RTFDataMigrationDAO.insertRTFPointsForTracking(cust, map.getValue().intValue(),
								coId, "SAAS-LIVE");
						updQuery = RTFDataMigrationDAO.updateCustomerRTFPoints(cust);

						result = RTFDataMigrationDAO.updateSaaSRewardsToRTF(insertQuery);
						if (result != null && result > 0) {
							result = RTFDataMigrationDAO.updateSaaSRewardsToRTF(updQuery);
							if (result != null && result > 0) {
								customers.add(cust);
								LivtCustomerAnswerDAO.updateCustAnswerMigrationStatus(clId, coId, map.getKey().trim());
								LivtContestWinnerDAO.updateWinnerMigrationStatus(clId, coId, map.getKey().trim());
								LivtContestGrandWinnerDAO.updateWinnerMigrationStatus(clId, coId, map.getKey().trim());
							}
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}

			RTFDataMigrationDAO.updateDataToCass(customers);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}	

	public static void migrateCustomerCart(String clId, String coId) throws Exception{	
		
		List<EcomCartDVO> cassCartList =  EcomCustomerCartCassDAO.fetchAllCustomerCart( clId,  coId) ;
		
		System.out.println(" Cart Iytems for Migration : " + cassCartList.size());
		if(cassCartList == null || cassCartList.size() == 0 ) return ;		
		CassToSQLMigrationService.batchInsertSQLLiveCusContestCart(cassCartList, clId,  coId);	
		System.out.println(" Cart Items  Migration : Completed" ) ;
		
	}

	public static void migrateAllContestDataToReportModule(String clId, String coId) throws Exception{
				
		System.out.println(" Start Report db Migration : ");
		SQLDaoUtil.migrateLiveContestDataToReportDB(clId , coId , 1 );
		System.out.println(" Report db Migration : Completed" ) ;
		
	}
	
	
	public static void main(String[] args) throws Exception{
		migrateCustomerCart("AMIT202002CID", "AMIT202002CID1625332563539");
	}

}
