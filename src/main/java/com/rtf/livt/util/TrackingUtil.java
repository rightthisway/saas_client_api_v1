/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.util;

import java.util.Date;
import java.util.UUID;

import com.rtf.livt.cass.dao.CassRtfApiTrackingDAO;
import com.rtf.livt.dvo.CassRtfApiTracking;

/**
 * The Class TrackingUtil.
 */
public class TrackingUtil {

	public static Boolean allowAllTracking = Boolean.TRUE;
	
	/**
	 * Contest API tracking.
	 *
	 * @param clientId     the client id
	 * @param platForm     the plat form
	 * @param deviceType   the device type
	 * @param sessionId    the session id
	 * @param actionType   the action type
	 * @param actionResult the action result
	 * @param contestId    the contest id
	 * @param custId       the cust id
	 * @param startDate    the start date
	 * @param endDate      the end date
	 * @param ip           the ip
	 * @param appVersion   the app version
	 * @param resStatus    the res status
	 * @param questionSlNo the question sl no
	 */
	public static void contestAPITracking(String clientId, String platForm, String deviceType, String sessionId,
			WebServiceActionType actionType, String actionResult, String contestId, String custId, Date startDate,
			Date endDate, String ip, String appVersion, Integer resStatus, String questionSlNo) {
		try {
			if (actionResult == null) {
				actionResult = "";
			}

			if (appVersion == null) {
				appVersion = "";
			}
			if (platForm == null) {
				platForm = "";
			}
			if (ip == null) {
				ip = "";
			}

			CassRtfApiTracking obj = new CassRtfApiTracking(clientId, UUID.randomUUID(), startDate, endDate, ip,
					actionType.toString(), platForm, deviceType, sessionId, actionResult, custId, contestId,
					actionResult, "", appVersion, resStatus, questionSlNo, URLUtil.clusterNodeId);

			new CassRtfApiTrackingDAO().save(obj);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Contest API tracking for device time tracking.
	 *
	 * @param clientId           the client id
	 * @param platForm           the plat form
	 * @param deviceType         the device type
	 * @param sessionId          the session id
	 * @param actionType         the action type
	 * @param actionResult       the action result
	 * @param contestId          the contest id
	 * @param custId             the cust id
	 * @param startDate          the start date
	 * @param endDate            the end date
	 * @param ip                 the ip
	 * @param appVersion         the app version
	 * @param apiHitStartTimeStr the api hit start time str
	 * @param resStatus          the res status
	 * @param questionSlNo       the question sl no
	 */
	public static void contestAPITrackingForDeviceTimeTracking(String clientId, String platForm, String deviceType,
			String sessionId, WebServiceActionType actionType, String actionResult, String contestId, String custId,
			Date startDate, Date endDate, String ip, String appVersion, String apiHitStartTimeStr, Integer resStatus,
			String questionSlNo) {
		try {
			if (actionResult == null) {
				actionResult = "";
			}

			if (appVersion == null) {
				appVersion = "";
			}
			if (platForm == null) {
				platForm = "";
			}
			if (ip == null) {
				ip = "";
			}

			CassRtfApiTracking obj = new CassRtfApiTracking(clientId, UUID.randomUUID(), startDate, endDate, ip,
					actionType.toString(), platForm, deviceType, sessionId, actionResult, custId, contestId,
					actionResult, "", appVersion, apiHitStartTimeStr, "", resStatus, questionSlNo,
					URLUtil.clusterNodeId);

			new CassRtfApiTrackingDAO().saveForDeviceTimeTracking(obj);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Contest validate answer API tracking for device time tracking.
	 *
	 * @param clientId           the client id
	 * @param platForm           the plat form
	 * @param deviceType         the device type
	 * @param sessionId          the session id
	 * @param actionType         the action type
	 * @param actionResult       the action result
	 * @param questionNo         the question no
	 * @param questionId         the question id
	 * @param answer             the answer
	 * @param contestId          the contest id
	 * @param custId             the cust id
	 * @param startDate          the start date
	 * @param endDate            the end date
	 * @param ip                 the ip
	 * @param apiHitStartTimeStr the api hit start time str
	 * @param fbTimeStr          the fb time str
	 * @param deviceInfo         the device info
	 * @param resStatus          the res status
	 * @param questionSlNo       the question sl no
	 * @param retryCount         the retry count
	 * @param ansCount           the ans count
	 * @param description        the description
	 */
	public static void contestValidateAnswerAPITrackingForDeviceTimeTracking(String clientId, String platForm,
			String deviceType, String sessionId, WebServiceActionType actionType, String actionResult,
			String questionNo, String questionId, String answer, String contestId, String custId, Date startDate,
			Date endDate, String ip, String apiHitStartTimeStr, String fbTimeStr, String deviceInfo, Integer resStatus,
			String questionSlNo, Integer retryCount, Integer ansCount, String description) {
		try {
			if (actionResult == null) {
				actionResult = "";
			}
			if (description == null) {
				description = actionResult + " :qNo : " + questionNo + " :qId: " + questionId + " :ans:" + answer;
			}
			if (platForm == null) {
				platForm = "";
			}
			if (ip == null) {
				ip = "";
			}

			CassRtfApiTracking obj = new CassRtfApiTracking(clientId, UUID.randomUUID(), startDate, endDate, ip,
					actionType.toString(), platForm, deviceType, sessionId, actionResult, custId, contestId,
					description, deviceInfo, "", apiHitStartTimeStr, fbTimeStr, resStatus, questionSlNo, retryCount,
					ansCount, URLUtil.clusterNodeId);
			new CassRtfApiTrackingDAO().saveForDeviceTimeTracking(obj);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
