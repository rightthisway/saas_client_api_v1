/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.util;

import java.sql.Connection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.rtf.common.util.DatabaseConnections;
import com.rtf.custom.dao.ContestMigrationDtlsDAO;
import com.rtf.custom.dto.ContestMigrationDtlsDVO;
import com.rtf.custom.dto.GenRewardDTO;
import com.rtf.custom.dvo.GenRewardDVO;
import com.rtf.custom.service.GenericRewardMigrationService;
import com.rtf.custom.utils.Constants;
import com.rtf.livt.dto.LivtWinnerUploadDTO;
import com.rtf.livt.dvo.ContestDVO;
import com.rtf.livt.sql.dao.LivtContestWinnerDAO;
import com.rtf.saas.exception.RTFDataAccessException;
import com.rtf.saas.exception.SaasProcessException;
import com.rtf.saas.util.GenUtil;
import com.rtf.saas.util.GsonUtil;
import com.rtf.shop.ecom.dvo.GenericResp;
import com.rtf.shop.ecom.util.GsonCustomConfig;
import com.rtf.shop.ecom.util.HTTPUtil;

/**
 * The Class LivtClientDataUploadUtil.
 */
public class LivtClientDataUploadUtil {

	/**
	 * Check job process status.
	 *
	 * @param conn          the connection Object
	 * @param clId           the Client ID 
	 * @param coId           the Contest ID 
	 * @param contRunningNo the contest running no
	 * @param jobName       the job name
	 * @param forcerunFlag  the forcerun flag
	 * @return the contest migration dtls DVO
	 * @throws Exception the exception
	 */
	public static synchronized ContestMigrationDtlsDVO checkJobProcessStatus(Connection conn, String clId, String coId,
			Integer contRunningNo, String jobName, Boolean forcerunFlag) throws Exception {

		ContestMigrationDtlsDVO migdetsdvo = ContestMigrationDtlsDAO.getPendingContestMigrationStatsByJobName(conn,
				clId, jobName, Constants.MIGRATION_STATUS_PENDING, coId, contRunningNo);

		if (migdetsdvo == null || !(migdetsdvo.getJobName().equals(jobName))) {
			return null;
		}
		ContestMigrationDtlsDAO.updateContestMigrationStatus(conn, clId, coId, jobName,
				Constants.MIGRATION_STATUS_STARTED, migdetsdvo.getContRunningNo());

		return migdetsdvo;
	}

	/**
	 * Process customer reward upload.
	 *
	 * @param dto               the dto
	 * @param rtfcartbaseapiurl the rtfcartbaseapiurl
	 * @return the integer
	 * @throws Exception the exception
	 */
	public static Integer processCustomerRewardUpload(GenRewardDTO dto, String rtfcartbaseapiurl) throws Exception {

		String data = null;

		if (GenUtil.isEmptyString(rtfcartbaseapiurl)) {
			throw new SaasProcessException("RTF Customer Reward Upload API URL NOT INITIALIZED ");
		}
		String RTFAPI_API_URL = rtfcartbaseapiurl;
		GenericResp respDto = null;
		try {

			String winnersUploadDto = GsonUtil.getJasksonObjMapper().writeValueAsString(dto);

			Map<String, String> dataMap = new HashMap<String, String>();
			dataMap.put("custRwds", winnersUploadDto);
			dataMap.put("configId", "configIdwinnerupload");
			data = HTTPUtil.execute(dataMap, RTFAPI_API_URL);
			if (data == null)
				return 0;
			Gson gson = GsonCustomConfig.getGsonBuilder();
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			respDto = gson.fromJson(((JsonObject) jsonObject.get("genericResp")), GenericResp.class);

			if (respDto == null)
				return 0;
			return respDto.getStatus();
		} catch (Exception e) {
			e.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(e), e);
		}
	}

	/**
	 * Process livt contest winners to client upload.
	 *
	 * @param winneruploadDto   the winnerupload dto
	 * @param rtfcartbaseapiurl the rtfcartbaseapiurl
	 * @return the integer
	 * @throws Exception the exception
	 */
	public static Integer processLivtContestWinnersToClientUpload(LivtWinnerUploadDTO winneruploadDto,
			String rtfcartbaseapiurl) throws Exception {

		String data = null;

		if (GenUtil.isEmptyString(rtfcartbaseapiurl)) {
			throw new SaasProcessException("RTF Winner Uplaod API URL NOT INITIALIZED ");
		}
		String RTFAPI_API_URL = rtfcartbaseapiurl;
		GenericResp dto = null;
		try {

			String winnersUploadDto = GsonUtil.getJasksonObjMapper().writeValueAsString(winneruploadDto);

			Map<String, String> dataMap = new HashMap<String, String>();
			dataMap.put("winners", winnersUploadDto);
			dataMap.put("configId", "configIdwinnerupload");
			data = HTTPUtil.execute(dataMap, RTFAPI_API_URL);
			if (data == null)
				return 0;
			Gson gson = GsonCustomConfig.getGsonBuilder();
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			dto = gson.fromJson(((JsonObject) jsonObject.get("genericResp")), GenericResp.class);

			if (dto == null)
				return 0;
			return dto.getStatus();
		} catch (Exception e) {
			e.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(e), e);
		}
	}

	/**
	 * Upload customer rewards to client.
	 *
	 * @param clId           the Client ID 
	 * @param coId           the Contest ID 
	 * @param contRunningNo the contest running no
	 * @param forcerunFlag  the forcerun flag
	 * @return the boolean
	 * @throws Exception the exception
	 */
	public static Boolean uploadCustomerRewardsToClient(String clId, String coId, Integer contRunningNo,
			Boolean forcerunFlag) throws Exception {
		ResourceBundle resourceBundle = ResourceBundle.getBundle("commonsettings");
		String winnerUploadUrl = resourceBundle.getString("saas.client.livt.rwd.upload.url");
		String rtfClientId = resourceBundle.getString("saas.client.rewardthefan.client.id");

		Connection conn = null;
		try {
			if (clId != null && !clId.equalsIgnoreCase(rtfClientId)) {
				return null;
			}
			GenRewardDTO dto = new GenRewardDTO();
			String rwdTyp = "REWARD POINTS";
			String jobName = Constants.JOB_PREFIX + rwdTyp;
			conn = DatabaseConnections.getRtfSaasConnection();

			if (!forcerunFlag) {
				ContestMigrationDtlsDVO migdetsdvo = checkJobProcessStatus(conn, clId, coId, contRunningNo, jobName,
						forcerunFlag);
				if (migdetsdvo == null) {
					dto.setSts(0);
					dto.setMsg(" NO PENDING JOBS FOR REWARDTYPE " + rwdTyp + " CLIENTID " + clId);
					return false;
				}
			}
			List<GenRewardDVO> lst = GenericRewardMigrationService.fetchcustomerRewardDetails(conn, rwdTyp, clId, coId,
					contRunningNo);
			dto.setLst(lst);
			dto.setSts(1);
			dto.setCoId(coId);
			dto.setConrunno(contRunningNo);

			processCustomerRewardUpload(dto, winnerUploadUrl);

			ContestMigrationDtlsDAO.updateContestMigrationStatus(conn, clId, coId, jobName,
					Constants.MIGRATION_STATUS_PROVIDED, contRunningNo);

		} catch (Exception e) {
			e.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(e), e);
		} finally {
			conn.close();
		}
		return true;

	}

	/**
	 * Upload contest winner rewards .
	 *
	 * @param clId     the Client ID 
	 * @param coId     the Contest ID 
	 * @param contest the contest
	 * @return the integer
	 * @throws Exception the exception
	 */
	public static Integer uploadLivtWinnerUploadtoClient(String clId, String coId, ContestDVO contest)
			throws Exception {
		ResourceBundle resourceBundle = ResourceBundle.getBundle("commonsettings");
		String winnerUploadUrl = resourceBundle.getString("saas.client.livt.winner.upload.url");
		String rtfClientId = resourceBundle.getString("saas.client.rewardthefan.client.id");

		if (clId != null && !clId.equalsIgnoreCase(rtfClientId)) {
			return null;
		}
		LivtWinnerUploadDTO winneruploadDto = new LivtWinnerUploadDTO();
		winneruploadDto.setClId(clId);
		winneruploadDto.setCoId(coId);
		winneruploadDto.setConStartDat(new Date(contest.getStDate()));
		winneruploadDto.setCrNo(contest.getContRunningNo());
		List<String> list = LivtContestWinnerDAO.getContestWinnersCustomerIdByContestId(clId, coId);
		winneruploadDto.setCustList(list);
		Integer resp = processLivtContestWinnersToClientUpload(winneruploadDto, winnerUploadUrl);

		return resp;

	}
}
