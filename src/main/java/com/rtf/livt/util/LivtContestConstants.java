/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.util;

/**
 * The Class LivtContestConstants.
 */
public class LivtContestConstants {

	/** The cont participants mig job. */
	public static String CONT_PARTICIPANTS_MIG_JOB = "CONT_PARTICIPANTS";

	/** The cont cust dtls mig job. */
	public static String CONT_CUST_DTLS_MIG_JOB = "CUST_CONT_DTLS";

	/** The cont cust answer mig job. */
	public static String CONT_CUST_ANSWER_MIG_JOB = "CUST_ANSWERS";

	/** The cont winner mig job. */
	public static String CONT_WINNER_MIG_JOB = "CONT_WINNERS";

	/** The cont grand winners mig job. */
	public static String CONT_GRAND_WINNERS_MIG_JOB = "CONT_GRAND_WINNERS";

	/** The cont jackpot winners mig job. */
	public static String CONT_JACKPOT_WINNERS_MIG_JOB = "CONT_JACKPOT_WINNERS";

	/** The cont cust rwds stats mig job. */
	public static String CONT_CUST_RWDS_STATS_MIG_JOB = "CUST_CONT_RWDS_STATS";

	/** The cont cust rwds credit job. */
	public static String CONT_CUST_RWDS_CREDIT_JOB = "CONT_RWDS_CREDIT";

	/** The cont rtf api tracking mig job. */
	public static String CONT_RTF_API_TRACKING_MIG_JOB = "API_TRACKING";
}
