/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.util;

import com.rtf.livt.dto.CassError;
import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * The Class CommonRespInfo.
 */
@XStreamAlias("CommonRespInfo")
public class CommonRespInfo {

	/** The sts. */
	private Integer sts;

	/** The err. */
	private CassError err;

	/** The msg. */
	private String msg;

	/**
	 * Gets the err.
	 *
	 * @return the err
	 */
	public CassError getErr() {
		return err;
	}

	/**
	 * Gets the msg.
	 *
	 * @return the msg
	 */
	public String getMsg() {
		return msg;
	}

	/**
	 * Gets the sts.
	 *
	 * @return the sts
	 */
	public Integer getSts() {
		return sts;
	}

	/**
	 * Sets the err.
	 *
	 * @param err the new err
	 */
	public void setErr(CassError err) {
		this.err = err;
	}

	/**
	 * Sets the msg.
	 *
	 * @param msg the new msg
	 */
	public void setMsg(String msg) {
		this.msg = msg;
	}

	/**
	 * Sets the sts.
	 *
	 * @param sts the new sts
	 */
	public void setSts(Integer sts) {
		this.sts = sts;
	}

}
