/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The Class TextUtil.
 */
public class TextUtil {

	/** The vs team pattern. */
	private static Pattern vsTeamPattern = Pattern.compile("(.*)\\s+[vV][sS]\\.?\\s+(.*)");

	/** The words to remove for stripping. */
	public static Collection<String> wordsToRemoveForStripping;

	/** The words to remove for keywords. */
	public static Collection<String> wordsToRemoveForKeywords;

	/** The words to remove for venue. */
	public static Collection<String> wordsToRemoveForVenue;

	/** The phone reg ex. */
	public static String phoneRegEx = "[^0-9]";

	static {
		// only use the singular form
		wordsToRemoveForStripping = new ArrayList<String>();
		wordsToRemoveForStripping.add("stadium");
		wordsToRemoveForStripping.add("ticket");
		wordsToRemoveForStripping.add("event");
		wordsToRemoveForStripping.add("theater");
		wordsToRemoveForStripping.add("state");
		wordsToRemoveForStripping.add("football");
		wordsToRemoveForStripping.add("basketball");
		wordsToRemoveForStripping.add("baseball");

		wordsToRemoveForKeywords = new ArrayList<String>();
		wordsToRemoveForKeywords.add("and");
		wordsToRemoveForKeywords.add("or");
		wordsToRemoveForKeywords.add("off");
		wordsToRemoveForKeywords.add("of");
		wordsToRemoveForKeywords.add("vs");
		wordsToRemoveForKeywords.add("the");
		wordsToRemoveForKeywords.add("a");
		wordsToRemoveForKeywords.addAll(wordsToRemoveForStripping);

		wordsToRemoveForVenue = new ArrayList<String>();
		wordsToRemoveForVenue.add("\\.");
		wordsToRemoveForVenue.add("st");
		wordsToRemoveForVenue.add("street");
		wordsToRemoveForVenue.add("ave");
		wordsToRemoveForVenue.add("avenue");
		wordsToRemoveForVenue.add("blvd");
		wordsToRemoveForVenue.add("at");
		wordsToRemoveForVenue.add("of");
	}

	/**
	 * Check men womens compatibility.
	 *
	 * @param name1 the name 1
	 * @param name2 the name 2
	 * @return true, if successful
	 */
	public static boolean checkMenWomensCompatibility(String name1, String name2) {
		name1 = " " + name1.toLowerCase() + " ";
		boolean event1HasMen = name1.matches(".*[^o]men.*");
		boolean event1HasWomen = name1.matches(".*women.*");

		name2 = " " + name2.toLowerCase() + " ";
		boolean event2HasMen = name2.matches(".*[^o]men.*");
		boolean event2HasWomen = name2.matches(".*women.*");

		if ((event1HasMen && event1HasWomen && event2HasMen && event2HasWomen)
				|| (event1HasMen && !event1HasWomen && event2HasMen && !event2HasWomen)
				|| (!event1HasMen && event1HasWomen && !event2HasMen && event2HasWomen)
				|| (!event1HasMen && !event1HasWomen) || (!event2HasMen && !event2HasWomen)) {
			return true;
		}
		return false;
	}

	/**
	 * Compute similarity.
	 *
	 * @param name1 the name 1
	 * @param name2 the name 2
	 * @return the int
	 */
	public static int computeSimilarity(String name1, String name2) {
		// if there is game 1 in keywords1 but is not present in game 2 the similarity
		// is 0
		// if we have something like conference: Team A vs Team B
		// make sure that conference is part of both keywords1 and keywords2

		int numOccurences = 0;

		// both have to contains YN or none
		if (name1.contains("YN ") != name2.contains("YN ")) {
			return 0;
		}

		if (name1.contains(":")) {
			String conference = name1.replaceAll(":.*", "").toLowerCase();
			if (!name2.toLowerCase().contains(conference)) {
				return 0;
			}

			numOccurences -= getKeywordsFromName(conference).split(" ").length;
		} else if (name2.contains(":")) {
			String conference = name2.replaceAll(":.*", "").toLowerCase();
			if (!name1.toLowerCase().contains(conference)) {
				return 0;
			}
			numOccurences -= getKeywordsFromName(conference).split(" ").length;
		}

		String keywords1 = getKeywordsFromName(name1);
		String keywords2 = getKeywordsFromName(name2);

		// if one contains zone, the other has to contain zone
		if (keywords1.contains("zone") != keywords2.contains("zone")) {
			return 0;
		}

		// if one contains the word "men(s)" and then other "women(s)" then
		// they are not compatible
		if (!checkMenWomensCompatibility(name1, name2)) {
			return 0;
		}

		keywords2 = " " + keywords2 + " ";
		boolean game = false;
		boolean session = false;

		for (String token : keywords1.split(" ")) {
			if (token.toLowerCase().equals("game")) {
				if (!keywords2.contains("game")) {
					return 0;
				}
				game = true;
			} else if (token.toLowerCase().equals("session")) {
				if (!keywords2.contains("session")) {
					return 0;
				}
				session = true;
			} else if ((session || game) && token.matches("^\\d+$")) {
				if (!keywords2.contains(" " + token + " ")) {
					return 0;
				}
			}

			if (keywords2.contains(" " + token + " ")) {
				numOccurences++;
			}
		}

		if (game) {
			numOccurences -= 3; // remove home game X
		}

		if (session) {
			numOccurences -= 2; // remove session X
		}

		return numOccurences;
	}

	/**
	 * Gets the contest tickets confirm dialog.
	 *
	 * @return the contest tickets confirm dialog
	 */
	public static String getContestTicketsConfirmDialog() {
		return "<font color=#FFFFFF>By accepting these Grand Prize Tickets, you agree to our</font> <a href=\"https://www.rewardthefan.com/PrivacyPolicy\"><font color=#000000>Privacy Policy</font></a>, <a href=\"https://www.rewardthefan.com/Terms\"><font color=#000000>Terms of Use</font></a>, and <a href=\"https://www.rewardthefan.com/Rules\"><font color=#000000>Rules of the Game</font>.</a>";
	}

	/**
	 * Gets the customer promo alert text.
	 *
	 * @return the customer promo alert text
	 */
	public static String getCustomerPromoAlertText() {
		return "You are successfully registered and your promotional code has been sent to your registered email address";
	}

	/**
	 * Gets the customer share link validation message.
	 *
	 * @param fontSizeStr the font size str
	 * @return the customer share link validation message
	 */
	public static String getCustomerShareLinkValidationMessage(String fontSizeStr) {
		return "<font " + fontSizeStr
				+ " color=#FFFFFF>This Perk is available to registered customers who have made a purchase from us</font>";
	}

	/**
	 * Gets the ebay search string from name.
	 *
	 * @param eventName the event name
	 * @return the ebay search string from name
	 */
	public static String getEbaySearchStringFromName(String eventName) {
		return eventName;
	}

	/**
	 * Gets the filtered keywords from keyword.
	 *
	 * @param eventName the event name
	 * @return the filtered keywords from keyword
	 */
	public static String getFilteredKeywordsFromKeyword(String eventName) {
		eventName = eventName.toLowerCase();

		String result = stripName(eventName);

		Collection<String> keywords = new ArrayList<String>();
		boolean game = false;

		for (String token : result.split(" ")) {
			if (token.toLowerCase().equals("game") || token.toLowerCase().equals("session")) {
				game = true;
			} else if (game && token.matches("^\\d+$")) {
				game = false;
			} else if (token.length() < 2) { // skip words too short. Set it to 1 because of u2
				continue;
			}

			if (wordsToRemoveForKeywords.contains(token)) {
				continue;
			}

			keywords.add(token);
		}

		String keywordString = "";
		for (String word : keywords) {
			keywordString += word + " ";
		}

		return keywordString.trim();
	}

	/**
	 * Gets the keywords from name.
	 *
	 * @param eventName the event name
	 * @return the keywords from name
	 */
	public static String getKeywordsFromName(String eventName) {
		eventName = eventName.toLowerCase();

		String result = stripName(eventName);

		eventName = eventName.replaceAll("-", " ");

		Collection<String> keywords = new ArrayList<String>();
		boolean game = false;

		for (String token : result.split(" ")) {
			if (token.toLowerCase().equals("game") || token.toLowerCase().equals("session")) {
				game = true;
			} else if (game && token.matches("^\\d+$")) {
				game = false;
			} else if (token.length() < 2) { // skip words too short. Set it to 1 because of u2
				continue;
			}

			if (wordsToRemoveForKeywords.contains(token)) {
				continue;
			}

			keywords.add(token);
		}

		String keywordString = "";
		for (String word : keywords) {
			keywordString += word + " ";
		}

		return keywordString.trim();
	}

	/**
	 * Gets the keywords from venue.
	 *
	 * @param venue the venue
	 * @return the keywords from venue
	 */
	public static String getKeywordsFromVenue(String venue) {
		venue = venue.toLowerCase();
		String result = "";
		for (String token : venue.split(" ")) {
			if (wordsToRemoveForVenue.contains(token)) {
				continue;
			}
			result = result + token + " ";
		}
		return result.trim();
	}

	/**
	 * Gets the non sports loyal fan email body.
	 *
	 * @param loyalFanName   the loyal fan name
	 * @param parentCategory the parent category
	 * @param country        the country
	 * @return the non sports loyal fan email body
	 */
	public static String getNonSportsLoyalFanEmailBody(String loyalFanName, String parentCategory, String country) {
		String message = "Congratulations! - you have declared yourself a LOYAL FAN of <b>" + loyalFanName
				+ "</b> STATE <b>" + parentCategory + "</b>. " + "<br> <br>Every time you purchase tickets for All <b>"
				+ parentCategory + "</b> in <b>" + loyalFanName + "," + country + "</b>, "
				+ "you will receive a <b>10%</b> discount. <br> <br> Your LOYAL FAN status does not expire."
				+ "<br> <br> Enjoy! ";
		return message;
	}

	/**
	 * Gets the search string from name.
	 *
	 * @param eventName the event name
	 * @return the search string from name
	 */
	public static String getSearchStringFromName(String eventName) {
		// if the event name is A vs. B, then remove vs. B
		String result = stripName(eventName);
		Matcher matcher = vsTeamPattern.matcher(result);
		if (matcher.find()) {
			return getKeywordsFromName(matcher.group(1));
		} else {
			return getFilteredKeywordsFromKeyword(result);
		}
	}

	/**
	 * Gets the sports loyal fan email body.
	 *
	 * @param teamName the team name
	 * @return the sports loyal fan email body
	 */
	public static String getSportsLoyalFanEmailBody(String teamName) {
		String message = "Congratulations! - you have declared yourself a LOYAL FAN of <b>" + teamName + "</b>. "
				+ "<br> <br>Every time you purchase tickets for <b>" + teamName + "</b>, whether at home or away, "
				+ "you will receive a <b>10%</b> discount. <br> <br> Your LOYAL FAN status does not expire."
				+ "<br> <br> Enjoy! ";
		return message;
	}

	/**
	 * Checks if is empty or null.
	 *
	 * @param value the value
	 * @return true, if is empty or null
	 */
	public static boolean isEmptyOrNull(String value) {

		if (null == value || value.isEmpty()) {
			return true;
		}
		return false;
	}

	/**
	 * Checks if is keywords match.
	 *
	 * @param keyword1 the keyword 1
	 * @param keyword2 the keyword 2
	 * @return true, if is keywords match
	 */
	public static boolean isKeywordsMatch(String keyword1, String keyword2) {
		keyword1 = keyword1.replaceAll("-", " ").replaceAll(":", " ").replaceAll("&", " ");
		keyword2 = keyword2.replaceAll("-", " ").replaceAll(":", " ").replaceAll("&", " ");
		if (keyword1.equalsIgnoreCase(keyword2) || keyword1.toLowerCase().contains(keyword2.toLowerCase())
				|| keyword2.toLowerCase().contains(keyword1.toLowerCase())) {
			return true;
		}
		List<String> keyword1List = new ArrayList<String>();
		List<String> keyword2List = new ArrayList<String>();
		for (String keyword : keyword1.split("\\s+")) {
			if (wordsToRemoveForKeywords.contains(keyword)) {
				continue;
			}
			keyword1List.add(keyword);
		}

		for (String keyword : keyword2.split("\\s+")) {
			if (wordsToRemoveForKeywords.contains(keyword)) {
				continue;
			}
			keyword2List.add(keyword);
		}
		int count = 0;
		for (String keyword : keyword1List) {
			if (keyword2List.contains(keyword)) {
				count++;
			}
		}
		if ((keyword1List.size() > 1 && (keyword2List.size() > 1))
				&& (count >= keyword1List.size() / 2 || count >= keyword2List.size() / 2)) {
			return true;
		} else if (count == 1) {
			return true;
		}
		return false;
	}

	/**
	 * Checks if is similar.
	 *
	 * @param name1 the name 1
	 * @param name2 the name 2
	 * @return true, if is similar
	 */
	public static boolean isSimilar(String name1, String name2) {
		return isSimilar(name1, name2, 3);
	}

	/**
	 * Checks if is similar.
	 *
	 * @param name1  the name 1
	 * @param name2  the name 2
	 * @param degree the degree
	 * @return true, if is similar
	 */
	// if number of words of name1 or name2 is less then degree = min of them
	public static boolean isSimilar(String name1, String name2, int degree) {
		int numOccurences = computeSimilarity(name1, name2);

		if (numOccurences >= degree) {
			return true;
		}

		String strippedKeywords1 = getKeywordsFromName(name1.replaceAll(".*:", ""))
				.replaceAll("\\w+ (?:home|away) game \\d+", "").replaceAll("session \\d+", "").replaceAll("zone", "");
		String strippedKeywords2 = getKeywordsFromName(name2.replaceAll(".*:", ""))
				.replaceAll("\\w+ (?:home|away) game \\d+", "").replaceAll("session \\d+", "").replaceAll("zone", "");

		int min = Math.min(strippedKeywords1.split(" ").length, strippedKeywords2.split(" ").length);

		if (min - 1 <= 0) {
			if (numOccurences > 0) {
				return true;
			}
			return false;
		}

		if (numOccurences >= min - 1) {
			return true;
		}

		return false;
	}

	/**
	 * Checks if is similar venue.
	 *
	 * @param name1  the name 1
	 * @param name2  the name 2
	 * @param degree the degree
	 * @return true, if is similar venue
	 */
	public static boolean isSimilarVenue(String name1, String name2, int degree) {
		name1 = getKeywordsFromVenue(name1);
		name2 = getKeywordsFromVenue(name2);
		return isSimilar(name1, name2, degree);
	}

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {

	}

	/**
	 * Removes the all whitespaces.
	 *
	 * @param str the str
	 * @return the string
	 */
	public static String removeAllWhitespaces(String str) {
		return str.replaceAll("\\s", "");
	}

	/**
	 * Removes the comments and punctuations.
	 *
	 * @param name the name
	 * @return the string
	 */
	/*
	 * remove punctuation remove whatever is between ()
	 */
	public static String removeCommentsAndPunctuations(String name) {
		// remove comments whatever is between braces
		String result = name.replaceAll("\\(.*\\)", "");
		result = result.replaceAll("\\[.*\\]", "");

		// remove punctuation
		result = result.replaceAll("[\\.,;\\:\\!\\?\\<\\>\\-_/]", " ");
		return result;
	}

	/**
	 * Removes the extra whitespaces.
	 *
	 * @param str the str
	 * @return the string
	 */
	public static String removeExtraWhitespaces(String str) {
		return str.replaceAll("\\s+", " ").trim();
	}

	/**
	 * Removes the non word characters.
	 *
	 * @param str the str
	 * @return the string
	 */
	public static String removeNonWordCharacters(String str) {
		return str.replaceAll("\\W+", "");
	}

	/**
	 * Scrub sporting event name.
	 *
	 * @param name the name
	 * @return the string
	 */
	public static String scrubSportingEventName(String name) {
		name = name.replaceAll("[fF]ootball", "");
		name = name.replaceAll("[bB]asketball", "");
		name = name.replaceAll("[bB]aseball", "");
		name = name.replaceAll("[tT]ickets", "");
		name = name.replaceAll("[pP]re-season", "");
		name = name.replaceAll("season", "");

		name = TextUtil.removeCommentsAndPunctuations(name);
		name = name.replaceAll("- .*", "");
		name = name.replaceAll("\\s+", " ");
		return name;
	}

	/**
	 * Strip ebay name.
	 *
	 * @param eventName the event name
	 * @return the string
	 */
	public static String stripEbayName(String eventName) {
		eventName = eventName.replaceAll(" .*", "");

		eventName = eventName.toLowerCase();

		// remove years
		String result = eventName.replaceAll("20\\d{2}", "");
		result = removeCommentsAndPunctuations(result);

		for (String word : wordsToRemoveForStripping) {
			result = result.replaceAll(word + "(?:s)", " ");
		}

		result.replaceAll("\\s+", " ");

		return result.trim();
	}

	/**
	 * Strip name.
	 *
	 * @param eventName the event name
	 * @return the string
	 */
	public static String stripName(String eventName) {
		// remove years
		String result = eventName.replaceAll("20\\d{2}", "");
		// remove comments whatever is between braces
		result = eventName.replaceAll("\\(.*\\)", "");
		result = result.replaceAll("\\[.*\\]", "");

		// remove punctuation
		result = result.replaceAll("[,;\\:\\!\\?\\<\\>_/]", " ");

		for (String word : wordsToRemoveForStripping) {
			result = result.replaceAll(word + "(?:s)", " ");
		}

		result.replaceAll("\\s+", " ");

		return result.trim();
	}

	/**
	 * Trim quotes.
	 *
	 * @param str the str
	 * @return the string
	 */
	public static String trimQuotes(String str) {
		if (str == null) {
			return null;
		}

		str = str.trim();

		if (str.isEmpty()) {
			return str;
		}

		if (str.length() < 2) {
			return str;
		}

		char first = str.charAt(0);
		char last = str.charAt(str.length() - 1);

		if (first == last && (first == '\'' || last == '\"')) {
			str = str.substring(1, str.length() - 1);
			return str.trim();
		}

		return str;
	}

}
