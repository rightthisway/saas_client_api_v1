/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.util;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

import javax.servlet.ServletContext;

import com.rtf.common.dvo.ClientConfigDVO;
import com.rtf.common.dvo.ClientCustomerDVO;
import com.rtf.common.service.ClientConfigService;
import com.rtf.common.util.DatabaseConnections;
import com.rtf.livt.cass.dao.CassDaoUtil;
import com.rtf.livt.cass.dao.ContestGrandWinnerCassDAO;
import com.rtf.livt.cass.dao.ContestParticipantsDAO;
import com.rtf.livt.cass.dao.ContestWinnersCassDAO;
import com.rtf.livt.cass.dao.CustContAnswersDAO;
import com.rtf.livt.cass.dao.CustContDtlsDAO;
import com.rtf.livt.cass.dao.LiveContestCassDAO;
import com.rtf.livt.cass.dao.LiveContestJackpotWinnerCassDAO;
import com.rtf.livt.cass.dao.LivtContestQuestionCassDAO;
import com.rtf.livt.dto.LiveCustDetails;
import com.rtf.livt.dto.LivtCustomerListDTO;
import com.rtf.livt.dto.LivtJoinContestDTO;
import com.rtf.livt.dvo.ContestDVO;
import com.rtf.livt.dvo.ContestWinnerDVO;
import com.rtf.livt.dvo.CustContAnswersCountDVO;
import com.rtf.livt.dvo.CustContAnswersDVO;
import com.rtf.livt.dvo.CustContDtlsDVO;
import com.rtf.livt.dvo.LivtContestQuestionDVO;
import com.rtf.livt.service.LivtService;
import com.rtf.livt.sql.dao.LiveContestSQLDAO;
import com.rtf.livt.sql.dao.SQLDaoUtil;
import com.rtf.livt.sql.dvo.RtfConfigContestClusterNodes;
import com.rtf.saas.db.CassandraConnector;

/**
 * The Class LivtContestUtil.
 */
public class LivtContestUtil {

	/** The started contest map. */
	static Map<String, ContestDVO> startedContestMap = new ConcurrentHashMap<String, ContestDVO>();

	/** The client contest question map. */
	static Map<String, Map<Integer, LivtContestQuestionDVO>> clientContestQuestionMap = new ConcurrentHashMap<String, Map<Integer, LivtContestQuestionDVO>>();

	/** The client contest current questions map. */
	static Map<String, Map<String, LivtContestQuestionDVO>> clientContestCurrentQuestionsMap = new ConcurrentHashMap<String, Map<String, LivtContestQuestionDVO>>(); // HC

	/** The client live customer list map. */
	static Map<String, List<LiveCustDetails>> clientLiveCustomerListMap = new ConcurrentHashMap<String, List<LiveCustDetails>>();

	/** The client last live cust fetch time map. */
	static Map<String, Long> clientLastLiveCustFetchTimeMap = new ConcurrentHashMap<String, Long>();

	/** The client last live cust fetch count map. */
	static Map<String, Integer> clientLastLiveCustFetchCountMap = new ConcurrentHashMap<String, Integer>();

	/** The live cust cache count. */
	private static Integer liveCustCacheCount = 100;
	/** The live cust refresh interval. */
	private static Long liveCustRefreshInterval = 1000 * 30l;

	/** The Super fan level enabled. */
	public static Boolean SuperFanLevelEnabled = Boolean.TRUE;

	/** The client customers map. */
	public static Map<String, Map<String, String>> clientCustomersMap = new HashMap<String, Map<String, String>>();

	// Prod - 470000 to 626700
	/** The start customer id. */
	// Sandbox - 136422 to 206423
	public static Long startCustomerId = 100000000101l;

	/** The end customer id. */
	public static Long endCustomerId = 100001000101l;

	/** The current customer id. */
	public static Long currentCustomerId = startCustomerId;

	/** The c. */
	static private AtomicLong c = new AtomicLong(0);

	/** The User id prefix map. */
	private static Map<String, ClientConfigDVO> uPrefixMap = new HashMap<String, ClientConfigDVO>();

	/** The User id atomic number map. */
	public static Map<String, AtomicLong> clientUserStarIdMap = new ConcurrentHashMap<String, AtomicLong>();

	/**
	 * Adds the Client customers to cache.
	 *
	 * @param clintId
	 *            the Client id
	 * @param custId
	 *            the Customer id
	 * @param encKey
	 *            the enc key
	 * @param userId
	 *            the user id
	 * @throws Exception
	 *             the exception
	 */
	public static void addClintCustomersToCache(String clintId, String custId, String encKey, String userId)
			throws Exception {
		try {
			clientCustomersMap.get(clintId).put(encKey + "_" + custId, userId);
			// clientCustomersMap.get(clintId).put(encKey+"_"+custId, userId);
		} catch (Exception e) {

		}
	}

	/**
	 * Clear cache data by contest id.
	 *
	 * @param clientId
	 *            the client id
	 * @param contestId
	 *            the contest id
	 * @throws Exception
	 *             the exception
	 */
	public static void clearCacheDataByContestId(String clientId, String contestId) throws Exception {

		
		System.out.println(" ********************************** CACHE CLEARED CALLED ");
		
		clientContestQuestionMap.remove(clientId);
		startedContestMap.remove(clientId);
		clientContestCurrentQuestionsMap.remove(clientId);

		clientLiveCustomerListMap.remove(clientId);
		clientLastLiveCustFetchTimeMap.remove(clientId);
		clientLastLiveCustFetchCountMap.remove(clientId);

		clientCustomersMap.remove(clientId);
		clientUserStarIdMap.remove(clientId);
	}

	/**
	 * Clear clint customers to cache.
	 *
	 * @param clintId
	 *            the Client id
	 * @throws Exception
	 *             the exception
	 */
	public static void clearClintCustomersToCache(String clintId) throws Exception {
		Map<String, String> custMap = new HashMap<String, String>();
		clientCustomersMap.put(clintId, custMap);
	}

	/**
	 * Compute auto create answers.
	 *
	 * @param clientId
	 *            the client id
	 * @param customerId
	 *            the customer id
	 * @param contestId
	 *            the contest id
	 * @param startQno
	 *            the start qno
	 * @param endQno
	 *            the end qno
	 * @param ansResMsg
	 *            the ans res msg
	 * @param custContDtls
	 *            the cust cont dtls
	 * @param contest
	 *            the contest
	 * @return the cust cont dtls DVO
	 * @throws Exception
	 *             the exception
	 */
	public static CustContDtlsDVO computeAutoCreateAnswers(String clientId, String customerId, String contestId,
			Integer startQno, Integer endQno, String ansResMsg, CustContDtlsDVO custContDtls, ContestDVO contest)
			throws Exception {

		try {
			boolean isAutoFlag = true;
			if (isAutoFlag) {
				for (int qNo = startQno; qNo <= endQno && endQno > 0; qNo++) {

					LivtContestQuestionDVO autoQuestion = LivtContestUtil
							.getLivtContestQuestionByContestIdandQuestionSlNo(clientId, contestId, qNo);
					if (autoQuestion == null) {
						ansResMsg += "AUTO:Quest Not Exist:" + qNo + ".";
						continue;
					}

					Double cumulativeRewards = 0.0;
					Integer cumulativeLifeLineUsed = 0;
					Integer cumulativecustAns = 0;
					if (custContDtls != null) {
						if (custContDtls.getAnsRwds() != null) {
							cumulativeRewards = custContDtls.getAnsRwds();
						}
						if (custContDtls.getCuLife() != null) {
							cumulativeLifeLineUsed = custContDtls.getCuLife();
						}
						if (custContDtls.getCuAns() != null) {
							cumulativecustAns = custContDtls.getCuAns();
						}
					}

					cumulativecustAns = cumulativecustAns + 1;

					CustContAnswersDVO custContAns = new CustContAnswersDVO();
					custContAns.setClId(clientId);
					custContAns.setCuId(customerId);
					custContAns.setCoId(contestId);
					custContAns.setqId(autoQuestion.getConqsnid());
					custContAns.setqNo(qNo);
					custContAns.setAns(null);
					custContAns.setIsCrt(Boolean.TRUE);
					custContAns.setCrDate(new Date());
					custContAns.setIsLife(false);
					custContAns.setFbCallbackTime(null);
					custContAns.setAnswerTime(null);
					custContAns.setRetryCount(0);
					custContAns.setIsAutoCreated(Boolean.TRUE);

					custContAns.setaRwds(autoQuestion.getAnsRwdVal());
					if (autoQuestion.getAnsRwdVal() != null) {
						cumulativeRewards = cumulativeRewards + autoQuestion.getAnsRwdVal();
					}
					custContAns.setCuRwds(cumulativeRewards);
					custContAns.setCuLife(cumulativeLifeLineUsed);

					CustContAnswersDAO.save(custContAns);
					if (custContDtls == null) {
						custContDtls = new CustContDtlsDVO();
						custContDtls.setCoId(custContAns.getCoId());
						custContDtls.setCuId(custContAns.getCuId());
						custContDtls.setAnsRwdType(contest.getQueRwdType());

					}
					if (custContDtls.getLqNo() == null || custContDtls.getLqNo() <= custContAns.getqNo()) {
						custContDtls.setLqNo(custContAns.getqNo());
						custContDtls.setIsLqCrt(custContAns.getIsCrt());
						custContDtls.setIsLqLife(custContAns.getIsLife());
					}
					custContDtls.setAnsRwds(custContAns.getCuRwds());
					custContDtls.setCuLife(custContAns.getCuLife());
					custContDtls.setCuAns(cumulativecustAns);

					CustContDtlsDAO.save(custContDtls);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return custContDtls;
	}

	/**
	 * Compute load test cluster data.
	 *
	 * @throws Exception
	 *             the exception
	 */
	public static void computeLoadTestClusterData() throws Exception {

		Long startId = 10000001l;
		Integer limit = 3000000;

		List<RtfConfigContestClusterNodes> list = SQLDaoUtil.getAllRtfConfigClusterNodeDetailsBudweiser();
		int nodeSize = list.size();
		URLUtil.computeClusterNodeId();
		int nodeId = URLUtil.clusterNodeId;
		if (nodeId == 0) {
			nodeId = 1;
		}
		int index = 0;
		for (RtfConfigContestClusterNodes obj : list) {
			index++;
			if (obj.getId().equals(nodeId)) {
				break;
			}
		}

		long custSizePerNode = (long) Math.floor(limit / nodeSize);

		startCustomerId = startId + (custSizePerNode * (index - 1)) + 1;
		endCustomerId = startCustomerId + custSizePerNode - 1;
		currentCustomerId = startCustomerId;
		setAtomicValue(startCustomerId);

	}

	/**
	 * Gets the contest by ID for start contest.
	 *
	 * @param clientId
	 *            the client id
	 * @param contestId
	 *            the contest id
	 * @return the contest by ID for start contest
	 * @throws Exception
	 *             the exception
	 */
	public static ContestDVO getContestByIDForStartContest(String clientId, String contestId) throws Exception {
		return LiveContestCassDAO.getContestByContestId(clientId, contestId);
	}

	/**
	 * Gets the contest by ID for start contest from SQL.
	 *
	 * @param clientId
	 *            the client id
	 * @param contestId
	 *            the contest id
	 * @return the contest by ID for start contest from SQL
	 * @throws Exception
	 *             the exception
	 */
	public static ContestDVO getContestByIDForStartContestFromSQL(String clientId, String contestId) throws Exception {
		return LiveContestSQLDAO.getContestByContestId(clientId, contestId);
	}

	/**
	 * Gets the contest current questions.
	 *
	 * @param clientId
	 *            the client id
	 * @param contestId
	 *            the contest id
	 * @return the contest current questions
	 * @throws Exception
	 *             the exception
	 */
	public static LivtContestQuestionDVO getContestCurrentQuestions(String clientId, String contestId)
			throws Exception {
		Map<String, LivtContestQuestionDVO> contestCurrentQuestionsMap = clientContestCurrentQuestionsMap.get(clientId);
		if (contestCurrentQuestionsMap != null) {
			return contestCurrentQuestionsMap.get(contestId);
		}
		return null;
	}

	/**
	 * Gets the contest customers count.
	 *
	 * @param clientId
	 *            the client id
	 * @param contestId
	 *            the contest id
	 * @return the contest customers count
	 * @throws Exception
	 *             the exception
	 */
	public static Integer getContestCustomersCount(String clientId, String contestId) throws Exception {
		Integer count = 0;
		count = ContestParticipantsDAO.getLiveCustomerCount(clientId, contestId);
		return count;

	}

	/**
	 * Gets the current contest by contest id.
	 *
	 * @param clientId
	 *            the client id
	 * @param contestId
	 *            the contest id
	 * @return the current contest by contest id
	 * @throws Exception
	 *             the exception
	 */
	public static ContestDVO getCurrentContestByContestId(String clientId, String contestId) throws Exception {

		ContestDVO startedContest = startedContestMap.get(clientId);	
	
		if (startedContest == null) {
			return LiveContestCassDAO.getContestByContestId(clientId, contestId);
		}
		if (startedContest != null && startedContest.getCoId().equals(contestId)) {
			return startedContest;
		}
		return null;
	}

	/**
	 * Gets the live contest ID for given client ID.
	 *
	 * @param clientId
	 *            the client id
	 * @return the current contest id
	 * @throws Exception
	 *             the exception
	 */
	public static String getLiveRunningContestByClientId(String clientId) throws Exception {

		/*
		 * ContestDVO startedContest = startedContestMap.get(clientId);
		 * System.out.println("MAP11111111111 ==== "+startedContestMap); if
		 * (startedContest == null) { startedContest =
		 * LiveContestCassDAO.getLiveContestByClientId(clientId);
		 * System.out.println("MAP2222222222 ==== "+startedContest);
		 * if(startedContest!=null){ startedContestMap.put(clientId, startedContest);
		 * System.out.println("MAP333333333 ==== "+startedContestMap); return
		 * startedContest.getCoId(); } return null; }
		 */		
		ContestDVO startedContest = LiveContestCassDAO.getCurrentLiveContestByClientId(clientId);		
		if(startedContest == null )  {
			System.out.println("No Contest in active status for Client " + clientId);
			throw new Exception(" No Contest in active status " + clientId);
		}
		return startedContest.getCoId();
		
	}
	
	
	public static String getLiveRunningContestByClientIdForSaasConfig(String clientId) throws Exception {
	
		ContestDVO startedContest = LiveContestCassDAO.getCurrentLiveContestByClientId(clientId);		
		if(startedContest == null )  {
			System.out.println("No Contest in active status for Client " + clientId);
			//throw new Exception(" No Contest in active status " + clientId);
			return null;
		}
		return startedContest.getCoId();
		
	}
	
	

	/**
	 * Gets the current contest by contest id for test.
	 *
	 * @param clientId
	 *            the client id
	 * @param contestId
	 *            the contest id
	 * @return the current contest by contest id for test
	 * @throws Exception
	 *             the exception
	 */
	public static ContestDVO getCurrentContestByContestIdForTest(String clientId, String contestId) throws Exception {
		ContestDVO startedContest = startedContestMap.get(clientId);
		return startedContest;
	}

	/**
	 * Gets the current started contest.
	 *
	 * @param clientId
	 *            the client id
	 * @param contestId
	 *            the contest id
	 * @return the current started contest
	 */
	public static ContestDVO getCurrentStartedContest(String clientId, String contestId) {
		return startedContestMap.get(clientId);
	}

	/**
	 * Gets the customer answers.
	 *
	 * @param clId
	 *            the client Id
	 * @param customerId
	 *            the customer id
	 * @param contestId
	 *            the contest id
	 * @return the customer answers
	 * @throws Exception
	 *             the exception
	 */
	public static CustContDtlsDVO getCustomerAnswers(String clId, String customerId, String contestId)
			throws Exception {
		return CustContDtlsDAO.getCustContDtlsByCustId(clId, contestId, customerId);
	}

	/**
	 * Gets the customer from cache.
	 *
	 * @param cclientIdStr
	 *            the cclient id str
	 * @param custId
	 *            the Customer id
	 * @return the customer from cache
	 */
	public static ClientCustomerDVO getCustomerFromCache(String cclientIdStr, String custId) {
		ClientCustomerDVO dvo = null;
		Map<String, String> custdetMap = clientCustomersMap.get(cclientIdStr);
		if (custdetMap != null && custdetMap.size() > 0) {
			String userId = custdetMap.get(custId + "-" + custId);
			if (userId != null) {
				dvo = new ClientCustomerDVO();
				dvo.setClId(cclientIdStr);
				dvo.setUserId(userId);
				dvo.setCuId(custId);
			}

		}
		return dvo;

	}

	/**
	 * Gets the customer id for load test.
	 *
	 * @return the customer id for load test
	 */
	public static String getCustomerIdForLoadTest() {
		if (currentCustomerId >= endCustomerId) {
			currentCustomerId = startCustomerId;
		}
		currentCustomerId += 1;
		return "" + currentCustomerId;
	}

	/**
	 * Gets the customer id for load test atom.
	 *
	 * @return the customer id for load test atom
	 */
	public static String getCustomerIdForLoadTestAtom() {
		Long tempId = c.getAndIncrement();
		if (tempId >= endCustomerId) {
			setAtomicValue(startCustomerId);
		}
		return "" + tempId;
	}

	/**
	 * Gets the customer id for load test string.
	 *
	 * @return the customer id for load test string
	 */
	public static String getCustomerIdForLoadTestString() {
		if (currentCustomerId >= endCustomerId) {
			currentCustomerId = startCustomerId;
		}
		currentCustomerId += 1;
		return new String("" + currentCustomerId);
	}

	/**
	 * Gets the live customer details info from cache.
	 *
	 * @param clientId
	 *            the client id
	 * @param contestId
	 *            the contest id
	 * @param dto
	 *            the dto
	 * @return the live customer details info from cache
	 * @throws Exception
	 *             the exception
	 */
	public static LivtCustomerListDTO getLiveCustomerDetailsInfoFromCache(String clientId, String contestId,
			LivtCustomerListDTO dto) throws Exception {

		List<LiveCustDetails> liveCustomersList = getLiveCustomersList(clientId, contestId);
		dto.setCustList(liveCustomersList);

		return dto;
	}

	/**
	 * Gets the live customers list.
	 *
	 * @param clientId
	 *            the client id
	 * @param contestId
	 *            the contest id
	 * @return the live customers list
	 */
	public static List<LiveCustDetails> getLiveCustomersList(String clientId, String contestId) {
		Long now = new Date().getTime();
		Long lastLiveCustFetchTime = clientLastLiveCustFetchTimeMap.get(clientId);
		Integer lastLiveCustFetchCount = clientLastLiveCustFetchCountMap.get(clientId);

		if (lastLiveCustFetchTime == null || (now - lastLiveCustFetchTime) >= liveCustRefreshInterval
				|| lastLiveCustFetchCount < liveCustCacheCount) {
			Integer count = refreshLiveCustomersList(clientId, contestId);
			lastLiveCustFetchTime = now;
			lastLiveCustFetchCount = count;

			clientLastLiveCustFetchTimeMap.put(contestId, lastLiveCustFetchTime);
			clientLastLiveCustFetchCountMap.put(contestId, lastLiveCustFetchCount);
		}

		return clientLiveCustomerListMap.get(clientId);
	}

	/**
	 * Gets the livt contest question by contest idand question sl no.
	 *
	 * @param clientId
	 *            the client id
	 * @param contestId
	 *            the contest id
	 * @param questionNo
	 *            the question no
	 * @return the livt contest question by contest idand question sl no
	 */
	public static LivtContestQuestionDVO getLivtContestQuestionByContestIdandQuestionSlNo(String clientId,
			String contestId, Integer questionNo) {
		Map<Integer, LivtContestQuestionDVO> contestQuestionMAp = clientContestQuestionMap.get(clientId);
		if (contestQuestionMAp != null) {
			List<LivtContestQuestionDVO> questions = new ArrayList<LivtContestQuestionDVO>(contestQuestionMAp.values());
			for (LivtContestQuestionDVO quizQuestion : questions) {
				if (quizQuestion.getQsnseqno().equals(questionNo)) {
					return quizQuestion;
				}
			}
		}
		return null;
	}

	/**
	 * Gets the livt contest question by id.
	 *
	 * @param clientId
	 *            the client id
	 * @param contestId
	 *            the contest id
	 * @param questionId
	 *            the question id
	 * @param questionNo
	 *            the question no
	 * @return the livt contest question by id
	 * @throws Exception
	 *             the exception
	 */
	public static LivtContestQuestionDVO getLivtContestQuestionById(String clientId, String contestId,
			Integer questionId, Integer questionNo) throws Exception {

		LivtContestQuestionDVO question = null;
		Map<Integer, LivtContestQuestionDVO> contestQuestionMAp = clientContestQuestionMap.get(clientId);
		if (contestQuestionMAp != null) {
			question = contestQuestionMAp.get(questionId);
		}
		if (question == null) {
			question = LivtContestQuestionCassDAO.getContestQuestioncontestIdAndQuestionId(clientId, contestId,
					questionId);
		}
		return question;
	}

	/**
	 * Gets the random cust id for test.
	 *
	 * @return the random cust id for test
	 * @throws Exception
	 *             the exception
	 */
	public static String getRandomCustIdForTest() throws Exception {
		Long temp = 100000000101l;
		Integer limit = 1500000;

		Random random = new Random();
		Integer val = random.nextInt(limit);
		Long cuid = temp + val;
		return "" + cuid;

	}

	/**
	 * Load application values.
	 *
	 * @throws Exception
	 *             the exception
	 */
	public static void loadApplicationValues() throws Exception {
		URLUtil.loadApplicationValues();
		CassandraConnector.loadApplicationValues();
		DatabaseConnections.loadApplicationValues();

		try {
			URLUtil.computeClusterNodeId();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Refresh cache data by contest id.
	 *
	 * @param clientId
	 *            the client id
	 * @param contestId
	 *            the contest id
	 * @throws Exception
	 *             the exception
	 */
	public static void refreshCacheDataByContestId(String clientId, Integer contestId) throws Exception {

	}

	/**
	 * Refresh end contest data.
	 *
	 * @param clientId
	 *            the client id
	 * @param contestId
	 *            the contest id
	 */
	public static void refreshEndContestData(String clientId, String contestId) {
		clientContestQuestionMap.remove(clientId);
		startedContestMap.remove(clientId);
		clientContestCurrentQuestionsMap.remove(clientId);
	}

	/**
	 * Refresh live customers list.
	 *
	 * @param clientId
	 *            the client id
	 * @param contestId
	 *            the contest id
	 * @return the integer
	 */
	public static Integer refreshLiveCustomersList(String clientId, String contestId) {
		List<LiveCustDetails> list = ContestParticipantsDAO.getAllLiveCustomerListForCache(clientId, contestId);
		if (list != null && !list.isEmpty()) {
			clientLiveCustomerListMap.put(clientId, list);
			return list.size();
		}
		return 0;
	}

	/**
	 * Refresh start contest data.
	 *
	 * @param clientId
	 *            the client id
	 * @param contestId
	 *            the contest id
	 * @return the boolean
	 * @throws Exception
	 *             the exception
	 */
	public static Boolean refreshStartContestData(String clientId, String contestId) throws Exception {
		ContestDVO contest = LiveContestCassDAO.getContestByContestId(clientId, contestId);		
		if (contest != null) {
			startedContestMap.put(clientId, contest);		
			Map<Integer, LivtContestQuestionDVO> contestQuestionMAp = new ConcurrentHashMap<Integer, LivtContestQuestionDVO>();
			List<LivtContestQuestionDVO> questionsList = LivtContestQuestionCassDAO
					.getAllContestQuestionsByContestId(clientId, contestId);
			for (LivtContestQuestionDVO question : questionsList) {
				contestQuestionMAp.put(question.getConqsnid(), question);
			}
			clientContestQuestionMap.put(clientId, contestQuestionMAp);
			ClientConfigDVO conf = ClientConfigService.getContestConfigByKey(clientId, "LVT", "USERID_PREFIX");
			uPrefixMap.put(clientId, conf);
			return true;
		}
		System.out.println("[startedContestMap ] : " +startedContestMap);
		return false;
	}

	/**
	 * Reset contest participants count.
	 *
	 * @param clientId
	 *            the client id
	 */
	public static void resetContestParticipantsCount(String clientId) {
		List<CustContAnswersCountDVO> list = LivtService.getAllContestPArticipantsCountByClientId(clientId);
		for (CustContAnswersCountDVO obj : list) {
			LivtService.resetContestParticipantsCountDataByClientId(obj.getClId(), obj.getCoId(), obj.getCount());
		}
	}

	/**
	 * Reset customer answers count.
	 *
	 * @param clientId
	 *            the client id
	 */
	public static void resetCustomerAnswersCount(String clientId) {
		List<CustContAnswersCountDVO> list = LivtService.getAllCustomerAnswersCountByClientId(clientId);
		for (CustContAnswersCountDVO obj : list) {
			LivtService.resetCustomerAnswersCountDataByClientId(obj.getClId(), obj.getCoId(), obj.getqNo(),
					obj.getOption(), obj.getCount());
		}
	}

	/**
	 * Sets the atomic value.
	 *
	 * @param value
	 *            the new atomic value
	 */
	private static void setAtomicValue(Long value) {
		c.set(value);
	}

	/**
	 * Truncate cassandra contest data.
	 *
	 * @param clientId
	 *            the client id
	 * @param contestIdStr
	 *            the contest id str
	 * @throws Exception
	 *             the exception
	 */
	public static void truncateCassandraContestData(String clientId, String contestIdStr) throws Exception {

		try {

			ContestParticipantsDAO.deleteContestParticipantsDataByClientId(clientId);
			CustContAnswersDAO.deleteCustContAnswersDataByClientId(clientId);
			CustContDtlsDAO.deleteCustContDtlsDataByClientId(clientId);
			ContestWinnersCassDAO.deleteContestWinnerDataByClientId(clientId);
			ContestGrandWinnerCassDAO.deleteContestGrandWinnerDataByClientId(clientId);
			LiveContestJackpotWinnerCassDAO.deleteContestJackpotWinnersDataByClientId(clientId);

			resetContestParticipantsCount(clientId);
			resetCustomerAnswersCount(clientId);

			LivtService.deleteContestParticipantsCountDataByClientId(clientId);
			LivtService.deleteCustomerAnswersCountDataByClientId(clientId);

		} catch (Exception e) {
			throw e;
		}
	}

	/**
	 * Update contest current questions.
	 *
	 * @param clientId
	 *            the client id
	 * @param contestQuestion
	 *            the contest question
	 * @return the boolean
	 * @throws Exception
	 *             the exception
	 */
	public static Boolean updateContestCurrentQuestions(String clientId, LivtContestQuestionDVO contestQuestion)
			throws Exception {
		Map<String, LivtContestQuestionDVO> contestCurrentQuestionsMap = clientContestCurrentQuestionsMap.get(clientId);
		if (contestCurrentQuestionsMap == null) {
			contestCurrentQuestionsMap = new ConcurrentHashMap<String, LivtContestQuestionDVO>();
		}
		contestCurrentQuestionsMap.put(contestQuestion.getConid(), contestQuestion);
		clientContestCurrentQuestionsMap.put(clientId, contestCurrentQuestionsMap);

		return true;
	}

	/**
	 * Update contest rewards to cass customer.
	 *
	 * @param contestId
	 *            the contest id
	 * @return the boolean
	 * @throws Exception
	 *             the exception
	 */
	public static Boolean updateContestRewardsToCassCustomer(Integer contestId) throws Exception {

		return true;
	}

	/**
	 * Update contest winners.
	 *
	 * @param clId
	 *            the client Id
	 * @param contestId
	 *            the contest id
	 * @param customerId
	 *            the customer id
	 * @param contest
	 *            the contest
	 * @return the boolean
	 */
	public static Boolean updateContestWinners(String clId, String contestId, String customerId, ContestDVO contest) {

		ContestWinnerDVO contestWinner = new ContestWinnerDVO();
		contestWinner.setClId(clId);
		contestWinner.setCoId(contestId);
		contestWinner.setCuId(customerId);
		contestWinner.setRwdType(contest.getSumRwdType());

		ContestWinnersCassDAO.save(contestWinner);

		return true;
	}

	/**
	 * Update customer answers.
	 *
	 * @param customerAnswers
	 *            the customer answers
	 * @return the boolean
	 * @throws Exception
	 *             the exception
	 */
	public static Boolean updateCustomerAnswers(CustContAnswersDVO customerAnswers) throws Exception {
		return true;
	}

	/**
	 * Update join contest customers count.
	 *
	 * @param joinContestInfo
	 *            the join contest info
	 * @param clientId
	 *            the client id
	 * @param contestId
	 *            the contest id
	 * @param customerId
	 *            the customer id
	 * @param contest
	 *            the contest
	 * @return the livt join contest DTO
	 * @throws Exception
	 *             the exception
	 */
	public static LivtJoinContestDTO updateJoinContestCustomersCount(LivtJoinContestDTO joinContestInfo,
			String clientId, String contestId, String customerId, ContestDVO contest) throws Exception {
		try {
			CustContDtlsDVO custContDtls = getCustomerAnswers(clientId, customerId, contestId);
			if (custContDtls != null) {
				joinContestInfo.setLqNo(custContDtls.getLqNo());
				if ((custContDtls.getIsLqCrt() != null && custContDtls.getIsLqCrt())
						|| (custContDtls.getIsLqLife() != null && custContDtls.getIsLqLife())) {
					joinContestInfo.setIsLstCrt(Boolean.TRUE);
				} else {
					joinContestInfo.setIsLstCrt(Boolean.FALSE);
				}
				joinContestInfo.setCaRwds(custContDtls.getAnsRwds());
				if (custContDtls.getCuLife() != null && custContDtls.getCuLife() > 0) {
					joinContestInfo.setIsLifeUsed(Boolean.TRUE);
				} else {
					joinContestInfo.setIsLifeUsed(Boolean.FALSE);
				}
			} else {
				joinContestInfo.setLqNo(0);
				joinContestInfo.setIsLstCrt(Boolean.FALSE);
				joinContestInfo.setCaRwds(0.0);
				joinContestInfo.setCaRwdType(contest.getQueRwdType());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return joinContestInfo;
	}

	public static String getUserIdPrefix(String clientId) {
		ClientConfigDVO conf = null;
		try {
			if (uPrefixMap.get(clientId) != null) {
				conf = uPrefixMap.get(clientId);
				System.out.println("!!!!!!!!! ********* PREFIX FROM CACHE  "  + conf.getValue() );
				return conf.getValue();
			}
			conf = ClientConfigService.getContestConfigByKey(clientId, "LVT", "USERID_PREFIX");
			if (conf != null) {
				uPrefixMap.put(clientId, conf);
				System.out.println("DB DB DB DB  ********* PREFIX FROM DB   "  + conf.getValue() );
				return conf.getValue();				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}

	public static AtomicLong getStartUserIdForClient(String clientId) throws Exception {
		AtomicLong userIdstNum = clientUserStarIdMap.get(clientId);
		if (userIdstNum == null) {
			System.out.println(" ERROR INIT VALUE FOR START USER ID Count ");
			throw new Exception(" ERROR INIT VALUE FOR START USER ID Count ");
		}
		return userIdstNum;
	}

	public static void setStartUserIdForClient(String clientId, AtomicLong num,ServletContext appCtx ) throws Exception {
		if (num == null) {
			System.out.println(" SET START COUNTER : ERROR INIT VALUE FOR START USER ID Count ");
			throw new Exception(" SET START COUNTER : ERROR INIT VALUE FOR START USER ID Count ");
		}		
		appCtx.setAttribute(clientId, num); 
		System.out.println("ATOMIC NUMBER SET IN CONTEXT  IS  : "+appCtx.getAttribute(clientId));
	}

	// Check with Load test to Synchronize this method
	public static synchronized Long getNextUserIdForClient(String clientId,ServletContext appCtx,String coId) throws Exception {
		coId = "CON1";
		AtomicLong userIdstNum = clientUserStarIdMap.get(clientId+"-"+coId);
		System.out.println("[Next seq loaded from cache  :: ATOMIC NUMBER SET IN cache  IS  : "+userIdstNum + " for nodeid  " + (String)appCtx.getAttribute("nodeId"));
		if (userIdstNum == null) {
			Long tmpNum = CassDaoUtil.getCustContDtlsLastGeneratedSequence(clientId, (String)appCtx.getAttribute("nodeId"))	;
			System.out.println("[Next seq loaded from DB  :: ATOMIC NUMBER SET IN DB  IS  : "+tmpNum);
			if(tmpNum == null) {
				System.out.println(" GET START RANGE FROM CASS DB :  ERROR NULL INIT VALUE FOR CLIENT  " + clientId);
				throw new Exception("GET START RANGE FROM CASS DB :  ERROR NULL INIT VALUE FOR CLIENT  " + clientId);
			}
			userIdstNum = new AtomicLong(tmpNum.longValue());
		}		
		Long tmplngId = userIdstNum.incrementAndGet();
		appCtx.setAttribute(clientId+"-"+coId, new AtomicLong(tmplngId));		
		clientUserStarIdMap.put(clientId+"-"+coId ,  new AtomicLong(tmplngId));
		CassDaoUtil.updateNextCounterNumber( clientId,  (String)appCtx.getAttribute("nodeId"));
		System.out.println("[ LivtContestUtil.getNextUserIdForClient ] 33333 :: ATOMIC NUMBER SET IN CONTEXT  IS  : "+appCtx.getAttribute(clientId));		
		System.out.println("[ LivtContestUtil.getNextUserIdForClient ] 33333 :: ATOMIC NUMBER SET IN STATIC MAP  : " +clientUserStarIdMap.get(clientId+"-"+coId));
		return tmplngId;
	}

	/**
	 * Update post migration stats.
	 *
	 * @throws Exception
	 *             the exception
	 */
	
	public static void main(String[] args) throws Exception {		
		Long tmpNum = CassDaoUtil.
				getCustContDtlsLastGeneratedSequence("AMIT202002CID","GD090621FBMNXV155806");		
		System.out.println(tmpNum);
	}
	
	public static void updatePostMigrationStats() throws Exception {
	}
}
