/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.util;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * The Class Error.
 */
@XStreamAlias("Error")
public class Error {

	/** The description. */
	private String description;

	/** The button value. */
	private String buttonValue;

	/** The over ride button. */
	private Boolean overRideButton;

	/**
	 * Gets the button value.
	 *
	 * @return the button value
	 */
	public String getButtonValue() {
		return buttonValue;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Gets the over ride button.
	 *
	 * @return the over ride button
	 */
	public Boolean getOverRideButton() {
		if (null == overRideButton) {
			overRideButton = false;
		}
		return overRideButton;
	}

	/**
	 * Sets the button value.
	 *
	 * @param buttonValue the new button value
	 */
	public void setButtonValue(String buttonValue) {
		this.buttonValue = buttonValue;
	}

	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Sets the over ride button.
	 *
	 * @param overRideButton the new over ride button
	 */
	public void setOverRideButton(Boolean overRideButton) {
		this.overRideButton = overRideButton;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "Error [des=" + description + ", btnV=" + buttonValue + ", ovRbtn=" + overRideButton + "]";
	}

}
