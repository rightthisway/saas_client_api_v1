/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.util;

import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * The Class TicketUtil.
 */
public class TicketUtil {

	/** The df. */
	private static DateFormat df = new SimpleDateFormat("MM/dd/yyyy hh:mm aa");

	/** The general admission list. */
	// private static DecimalFormat decimalFormat = new DecimalFormat("#0.00");
	private static List<String> generalAdmissionList = new ArrayList<String>();

	/** The df 2. */
	private static DecimalFormat df2 = new DecimalFormat(".##");

	/** The normal round off. */
	private static DecimalFormat normalRoundOff = new DecimalFormat(".##");

	/** The percentage DF. */
	public static DecimalFormat percentageDF = new DecimalFormat("#.##");

	static {
		if (generalAdmissionList.size() <= 0) {
			generalAdmissionList.add("GA");
			generalAdmissionList.add("General Admission");
			generalAdmissionList.add("Gen Ad");
			generalAdmissionList.add("General Ad");
			generalAdmissionList.add("General Adm");
			generalAdmissionList.add("GenAd");
		}
	}

	/**
	 * Gets the normal rounded value.
	 *
	 * @param value the value
	 * @return the normal rounded value
	 * @throws Exception the exception
	 */
	public static Double getNormalRoundedValue(Double value) throws Exception {
		return Double.valueOf(df2.format(value));
	}

	/**
	 * Gets the rounded up value.
	 *
	 * @param value the value
	 * @return the rounded up value
	 * @throws Exception the exception
	 */
	public static Double getRoundedUpValue(Double value) throws Exception {
		df2.setRoundingMode(RoundingMode.UP);
		return Double.valueOf(df2.format(value));
	}

	/**
	 * Gets the rounded value.
	 *
	 * @param value the value
	 * @return the rounded value
	 * @throws Exception the exception
	 */
	public static Double getRoundedValue(Double value) throws Exception {
		df2.setRoundingMode(RoundingMode.DOWN);
		return Double.valueOf(df2.format(value));
	}

	/**
	 * Gets the rounded value intger.
	 *
	 * @param value the value
	 * @return the rounded value intger
	 * @throws Exception the exception
	 */
	public static Integer getRoundedValueIntger(Double value) throws Exception {
		value = Math.floor(value);
		return value.intValue();
	}

	/**
	 * Gets the rounded value string.
	 *
	 * @param value the value
	 * @return the rounded value string
	 * @throws Exception the exception
	 */
	public static String getRoundedValueString(Double value) throws Exception {
		return String.format("%.2f", value);
	}

}
