/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.util;

/**
 * The Enum WebServiceActionType.
 */
public enum WebServiceActionType {

	/** The home. */
	HOME,
	/** The generalisedsearch. */
	GENERALISEDSEARCH,
	/** The ticket. */
	TICKET,
	/** The holdticket. */
	HOLDTICKET,
	/** The soldticket. */
	SOLDTICKET,
	/** The getorders. */
	GETORDERS,
	/** The extendholdticket. */
	EXTENDHOLDTICKET,
	/** The customerlogin. */
	CUSTOMERLOGIN,
	/** The customersignup. */
	CUSTOMERSIGNUP,
	/** The editcustomer. */
	EDITCUSTOMER,

	/** The resetpassword. */
	RESETPASSWORD,
	/** The resetpasswordrequest. */
	RESETPASSWORDREQUEST,
	/** The getstatecountry. */
	GETSTATECOUNTRY,
	/** The managefavouriteevents. */
	MANAGEFAVOURITEEVENTS,
	/** The ticketgrouplocking. */
	TICKETGROUPLOCKING,
	/** The vieworder. */
	VIEWORDER,
	/** The createorder. */
	CREATEORDER,
	/** The getcustomerrewards. */
	GETCUSTOMERREWARDS,

	/** The generalisedsearchbygeolocation. */
	GENERALISEDSEARCHBYGEOLOCATION,
	/** The generalisedsearchwithoutpagination. */
	GENERALISEDSEARCHWITHOUTPAGINATION,
	/** The paymentgateway. */
	PAYMENTGATEWAY,
	/** The getticketgroup. */
	GETTICKETGROUP,
	/** The editcustomeraddress. */
	EDITCUSTOMERADDRESS,
	/** The updateorderstatus. */
	UPDATEORDERSTATUS,

	/** The changepassword. */
	CHANGEPASSWORD,
	/** The getcustomer. */
	GETCUSTOMER,
	/** The getservertime. */
	GETSERVERTIME,
	/** The getsoldtickets. */
	GETSOLDTICKETS,
	/** The customeremailvalidation. */
	CUSTOMEREMAILVALIDATION,
	/** The removezoneevent. */
	REMOVEZONEEVENT,
	/** The getzoneticketsinformation. */
	GETZONETICKETSINFORMATION,
	/** The emailvenuemap. */
	EMAILVENUEMAP,

	/** The emailcustomerrequest. */
	EMAILCUSTOMERREQUEST,
	/** The getcities. */
	GETCITIES,
	/** The getpaypalcredentials. */
	GETPAYPALCREDENTIALS,
	/** The zoneticketstracking. */
	ZONETICKETSTRACKING,
	/** The stripetransaction. */
	STRIPETRANSACTION,
	/** The getstripecredentials. */
	GETSTRIPECREDENTIALS,

	/** The minicatsgeneralisedsearch. */
	MINICATSGENERALISEDSEARCH,
	/** The vipminicatsgeneralisedsearch. */
	VIPMINICATSGENERALISEDSEARCH,
	/** The lastrowminicatsgeneralisedsearch. */
	LASTROWMINICATSGENERALISEDSEARCH,
	/** The minicatsticket. */
	MINICATSTICKET,
	/** The lastrowminicatsticket. */
	LASTROWMINICATSTICKET,

	/** The vipminicatsticket. */
	VIPMINICATSTICKET,
	/** The autocatsticketgrouplocking. */
	AUTOCATSTICKETGROUPLOCKING,
	/** The getautocatsticketgroup. */
	GETAUTOCATSTICKETGROUP,
	/** The autocatscreateorder. */
	AUTOCATSCREATEORDER,
	/** The autocatsvieworder. */
	AUTOCATSVIEWORDER,
	/** The getautocatsorders. */
	GETAUTOCATSORDERS,

	/** The viplastrowminicatsgeneralisedsearch. */
	VIPLASTROWMINICATSGENERALISEDSEARCH,
	/** The viplastrowminicatsticket. */
	VIPLASTROWMINICATSTICKET,
	/** The presalegeneralisedsearch. */
	PRESALEGENERALISEDSEARCH,
	/** The presaleticket. */
	PRESALETICKET,
	/** The presaleticketgrouplocking. */
	PRESALETICKETGROUPLOCKING,

	/** The getpresaleticketgroup. */
	GETPRESALETICKETGROUP,
	/** The searchartist. */
	SEARCHARTIST,
	/** The getpopularartistforsuperfan. */
	GETPOPULARARTISTFORSUPERFAN,
	/** The getpossibleartist. */
	GETPOSSIBLEARTIST,
	/** The addfavoriteorsuperfan. */
	ADDFAVORITEORSUPERFAN,
	/** The socialloginvalidator. */
	SOCIALLOGINVALIDATOR,

	/** The getpopularartistforfavorite. */
	GETPOPULARARTISTFORFAVORITE,
	/** The gethomecards. */
	GETHOMECARDS,
	/** The setfavouriteevents. */
	SETFAVOURITEEVENTS,
	/** The getexplorecards. */
	GETEXPLORECARDS,
	/** The eventsearchbyexplore. */
	EVENTSEARCHBYEXPLORE,
	/** The getgrandchildcategory. */
	GETGRANDCHILDCATEGORY,

	/** The eventsearch. */
	EVENTSEARCH,
	/** The addcustomeraddress. */
	ADDCUSTOMERADDRESS,
	/** The contestorderaddress. */
	CONTESTORDERADDRESS,
	/** The getgrandchildcategoryforsuperfan. */
	GETGRANDCHILDCATEGORYFORSUPERFAN,
	/** The getcustomerinfo. */
	GETCUSTOMERINFO,
	/** The getloyaltyreward. */
	GETLOYALTYREWARD,
	/** The getvalidateorderpaymentbyrewards. */
	GETVALIDATEORDERPAYMENTBYREWARDS,

	/** The getartistbygrandchild. */
	GETARTISTBYGRANDCHILD,
	/** The getrefferercode. */
	GETREFFERERCODE,
	/** The rtfproductinfo. */
	RTFPRODUCTINFO,
	/** The getfavoriteartistevent. */
	GETFAVORITEARTISTEVENT,
	/** The getcustomerprofilepic. */
	GETCUSTOMERPROFILEPIC,
	/** The getsuperfanleague. */
	GETSUPERFANLEAGUE,
	/** The getfantasyevent. */
	GETFANTASYEVENT,
	/** The getsuperfanteam. */
	GETSUPERFANTEAM,
	/** The getsuperfanordersummary. */
	GETSUPERFANORDERSUMMARY,
	/** The getsuperfanfutureordersummary. */
	GETSUPERFANFUTUREORDERSUMMARY,
	/** The getfutureorderconfirmation. */
	GETFUTUREORDERCONFIRMATION,
	/** The getticketdownload. */
	GETTICKETDOWNLOAD,
	/** The getcrownteam. */
	GETCROWNTEAM,

	/** The getsportscrowntickets. */
	GETSPORTSCROWNTICKETS,
	/** The getotherscrowntickets. */
	GETOTHERSCROWNTICKETS,
	/** The getcrownjewelorders. */
	GETCROWNJEWELORDERS,
	/** The viewcrownjewelorder. */
	VIEWCROWNJEWELORDER,
	/** The validatereferralcode. */
	VALIDATEREFERRALCODE,
	/** The validatereferralcodetictracker. */
	VALIDATEREFERRALCODETICTRACKER,

	/** The getartistinfo. */
	GETARTISTINFO,
	/** The getcustomercards. */
	GETCUSTOMERCARDS,
	/** The getstripetoken. */
	GETSTRIPETOKEN,
	/** The getreferafrienddetailtext. */
	GETREFERAFRIENDDETAILTEXT,
	/** The forgetuseridrequest. */
	FORGETUSERIDREQUEST,
	/** The nextcontestlistrefresh. */
	NEXTCONTESTLISTREFRESH,

	/** The removecustomercard. */
	REMOVECUSTOMERCARD,
	/** The getloyalfandetails. */
	GETLOYALFANDETAILS,
	/** The updatecustomerphonenumber. */
	UPDATECUSTOMERPHONENUMBER,
	/** The validatecustomerloyalfan. */
	VALIDATECUSTOMERLOYALFAN,
	/** The getrewardsbreakup. */
	GETREWARDSBREAKUP,
	/** The customerlogout. */
	CUSTOMERLOGOUT,

	/** The getchildcategory. */
	GETCHILDCATEGORY,
	/** The removecustomeraddress. */
	REMOVECUSTOMERADDRESS,
	/** The getlockedticketstimeleft. */
	GETLOCKEDTICKETSTIMELEFT,
	/** The unlockticketgroup. */
	UNLOCKTICKETGROUP,
	/** The emailcustomerfeedback. */
	EMAILCUSTOMERFEEDBACK,
	/** The stripetransactionforcustomer. */
	STRIPETRANSACTIONFORCUSTOMER,

	/** The paypalfuturepayment. */
	PAYPALFUTUREPAYMENT,
	/** The getuserpreference. */
	GETUSERPREFERENCE,
	/** The adduserpreference. */
	ADDUSERPREFERENCE,
	/** The getstriperefunds. */
	GETSTRIPEREFUNDS,
	/** The createstriperefund. */
	CREATESTRIPEREFUND,
	/** The gethomeslider. */
	GETHOMESLIDER,
	/** The getcitiesbyzipcode. */
	GETCITIESBYZIPCODE,

	/** The sendsms. */
	SENDSMS,
	/** The paypaltracking. */
	PAYPALTRACKING,
	/** The eventsearchheaders. */
	EVENTSEARCHHEADERS,
	/** The validatezipcode. */
	VALIDATEZIPCODE,
	/** The cancelorder. */
	CANCELORDER,
	/** The paypalrefund. */
	PAYPALREFUND,
	/** The paypalrefundtranlist. */
	PAYPALREFUNDTRANLIST,
	/** The invoicerefund. */
	INVOICEREFUND,

	/** The getinvoicerefundlist. */
	GETINVOICEREFUNDLIST,
	/** The downloadnotification. */
	DOWNLOADNOTIFICATION,
	/** The getpasteventorders. */
	GETPASTEVENTORDERS,
	/** The confirmorder. */
	CONFIRMORDER,
	/** The tictrackercreateorder. */
	TICTRACKERCREATEORDER,
	/** The promodialogstatus. */
	PROMODIALOGSTATUS,

	/** The generatecustomerpromocode. */
	GENERATECUSTOMERPROMOCODE,
	/** The getfantasyproductcategories. */
	GETFANTASYPRODUCTCATEGORIES,
	/** The getfantasyeventteams. */
	GETFANTASYEVENTTEAMS,
	/** The createfantasyorder. */
	CREATEFANTASYORDER,
	/** The validatefstpurchase. */
	VALIDATEFSTPURCHASE,

	/** The viewfstorder. */
	VIEWFSTORDER,
	/** The getfstorders. */
	GETFSTORDERS,
	/** The tracksharing. */
	TRACKSHARING,
	/** The referralcontestwinner. */
	REFERRALCONTESTWINNER,
	/** The referralconteststatus. */
	REFERRALCONTESTSTATUS,
	/** The validatecustauthdialog. */
	VALIDATECUSTAUTHDIALOG,
	/** The quizvalidateotp. */
	QUIZVALIDATEOTP,
	/** The quizsavecustomerinfo. */
	QUIZSAVECUSTOMERINFO,

	/** The quizcustomerlogin. */
	QUIZCUSTOMERLOGIN,
	/** The quizcustomersignup. */
	QUIZCUSTOMERSIGNUP,
	/** The quizgetotp. */
	QUIZGETOTP,
	/** The quizrequestshow. */
	QUIZREQUESTSHOW,
	/** The quizsubmittrivia. */
	QUIZSUBMITTRIVIA,
	/** The quizwinnerorder. */
	QUIZWINNERORDER,
	/** The quizsendfriendrequest. */
	QUIZSENDFRIENDREQUEST,
	/** The quizcontestevents. */
	QUIZCONTESTEVENTS,

	/** The quizcontestinfo. */
	QUIZCONTESTINFO,
	/** The quizjoincontest. */
	QUIZJOINCONTEST,
	/** The quizexitcontest. */
	QUIZEXITCONTEST,
	/** The quizcontestcustomercount. */
	QUIZCONTESTCUSTOMERCOUNT,
	/** The quizupdatereferalcode. */
	QUIZUPDATEREFERALCODE,
	/** The quizquestioninfo. */
	QUIZQUESTIONINFO,
	/** The quizvalidateanswers. */
	QUIZVALIDATEANSWERS,
	/** The quizapplylifeline. */
	QUIZAPPLYLIFELINE,

	/** The quizgetquestionanswers. */
	QUIZGETQUESTIONANSWERS,
	/** The quizupdatecontestwinnerrewards. */
	QUIZUPDATECONTESTWINNERREWARDS,
	/** The quizcontestsummary. */
	QUIZCONTESTSUMMARY,
	/** The quizgrandwinners. */
	QUIZGRANDWINNERS,
	/** The quizcustomercountdetails. */
	QUIZCUSTOMERCOUNTDETAILS,
	/** The quizgetcustomerstats. */
	QUIZGETCUSTOMERSTATS,
	/** The quizupdatecustomerstats. */
	QUIZUPDATECUSTOMERSTATS,

	/** The quizgetdiscoverpagedata. */
	QUIZGETDISCOVERPAGEDATA,
	/** The quizcustomerphonesearch. */
	QUIZCUSTOMERPHONESEARCH,
	/** The quizuseridsetup. */
	QUIZUSERIDSETUP,
	/** The quizgetcontestpromocode. */
	QUIZGETCONTESTPROMOCODE,
	/** The quizsendpromocodeemail. */
	QUIZSENDPROMOCODEEMAIL,
	/** The quizcustomerautosearch. */
	QUIZCUSTOMERAUTOSEARCH,
	/** The startquizcontest. */
	STARTQUIZCONTEST,
	/** The endquizcontest. */
	ENDQUIZCONTEST,

	/** The quizcustinvitefriends. */
	QUIZCUSTINVITEFRIENDS,
	/** The getquizmessagetext. */
	GETQUIZMESSAGETEXT,
	/** The quizremovedupphonemap. */
	QUIZREMOVEDUPPHONEMAP,
	/** The orderpayment. */
	ORDERPAYMENT,
	/** The quizgetquestionlifelinecount. */
	QUIZGETQUESTIONLIFELINECOUNT,
	/** The quizcontestremovecache. */
	QUIZCONTESTREMOVECACHE,

	/** The quizrefreshsummarydata. */
	QUIZREFRESHSUMMARYDATA,
	/** The quizmanualnotification. */
	QUIZMANUALNOTIFICATION,
	/** The quizcontestrefreshcache. */
	QUIZCONTESTREFRESHCACHE,
	/** The quizapplyrewardearnlife. */
	QUIZAPPLYREWARDEARNLIFE,
	/** The quizredeemrewardtixpromo. */
	QUIZREDEEMREWARDTIXPROMO,
	/** The quizsendinvitefriendemail. */
	QUIZSENDINVITEFRIENDEMAIL,

	/** The updateappnotificationregid. */
	UPDATEAPPNOTIFICATIONREGID,
	/** The tracknotificationregid. */
	TRACKNOTIFICATIONREGID,
	/** The referralrewardbreak. */
	REFERRALREWARDBREAK,
	/** The referralrewardcontestbreak. */
	REFERRALREWARDCONTESTBREAK,
	/** The affiliaterewardbreak. */
	AFFILIATEREWARDBREAK,
	/** The contpasswordauth. */
	CONTPASSWORDAUTH,

	/** The customerloginbyphoneno. */
	CUSTOMERLOGINBYPHONENO,
	/** The validatephonenologin. */
	VALIDATEPHONENOLOGIN,
	/** The updatecustomersignupdetails. */
	UPDATECUSTOMERSIGNUPDETAILS,
	/** The firebasecallbacktracking. */
	FIREBASECALLBACKTRACKING,
	/** The getdashboardinfo. */
	GETDASHBOARDINFO,
	/** The contvalidateans. */
	CONTVALIDATEANS,
	/** The joincontest. */
	JOINCONTEST,
	/** The exitcontest. */
	EXITCONTEST,

	/** The contapplylife. */
	CONTAPPLYLIFE,
	/** The compcontestwinnerrewards. */
	COMPCONTESTWINNERREWARDS,
	/** The contcustcount. */
	CONTCUSTCOUNT,
	/** The getlivecustlist. */
	GETLIVECUSTLIST,
	/** The getquestanscount. */
	GETQUESTANSCOUNT,
	/** The gethalloffame. */
	GETHALLOFFAME,
	/** The updatecustconteststats. */
	UPDATECUSTCONTESTSTATS,
	/** The removecasscontestcache. */
	REMOVECASSCONTESTCACHE,
	/** The contvalidatemultians. */
	CONTVALIDATEMULTIANS,
	/** The fbcallbacktracking. */
	FBCALLBACKTRACKING,

	/** The forcetruncatecassandra. */
	FORCETRUNCATECASSANDRA,
	/** The triviaticket. */
	TRIVIATICKET,
	/** The triviaorder. */
	TRIVIAORDER,
	/** The triviaorderview. */
	TRIVIAORDERVIEW,
	/** The getsuperfanstats. */
	GETSUPERFANSTATS,
	/** The loadapplicationvalues. */
	LOADAPPLICATIONVALUES,
	/** The postmigrationstatsupdate. */
	POSTMIGRATIONSTATSUPDATE,
	/** The quizminijackpot. */
	QUIZMINIJACKPOT,
	/** The quizmegajackpot. */
	QUIZMEGAJACKPOT,
	/** The cassconnectionstats. */
	CASSCONNECTIONSTATS,
	/** The loadcontsflevels. */
	LOADCONTSFLEVELS,

	/** The applymagicwand. */
	APPLYMAGICWAND,

	/** The livtmigration. */
	LIVTMIGRATION,
	/** The createcoupon. */
	CREATECOUPON

}
