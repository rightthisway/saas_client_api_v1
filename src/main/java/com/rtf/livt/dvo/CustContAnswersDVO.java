/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.dvo;

import java.io.Serializable;
import java.util.Date;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * The Class CustContAnswersDVO.
 */
@XStreamAlias("CustContAnswers")
public class CustContAnswersDVO implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -2033390339665646377L;

	/** The cl id. */
	private String clId;

	/** The cu id. */
	private String cuId;

	/** The co id. */
	private String coId;

	/** The q id. */
	private Integer qId;

	/** The q no. */
	private Integer qNo;

	/** The ans. */
	private String ans;

	/** The is crt. */
	private Boolean isCrt = false;

	/** The is life. */
	private Boolean isLife;

	/** The a rwds. */
	private Double aRwds;

	/** The a rwd type. */
	private String aRwdType;

	/** The fb callback time. */
	private String fbCallbackTime;

	/** The answer time. */
	private String answerTime;

	/** The retry count. */
	private Integer retryCount;

	/** The is auto created. */
	private Boolean isAutoCreated;

	/** The cu rwds. */
	@JsonIgnore
	private Double cuRwds;

	/** The cu life. */
	@JsonIgnore
	private Integer cuLife;

	/** The cr date. */
	@JsonIgnore
	private Date crDate;

	/** The up date. */
	@JsonIgnore
	private Date upDate;

	/**
	 * Instantiates a new cust cont answers DVO.
	 */
	public CustContAnswersDVO() {
	}

	/**
	 * Instantiates a new cust cont answers DVO.
	 *
	 * @param clId   the cl id
	 * @param cuId   the cu id
	 * @param coId   the co id
	 * @param qId    the q id
	 * @param qNo    the q no
	 * @param ans    the ans
	 * @param isCrt  the is crt
	 * @param isLife the is life
	 */
	public CustContAnswersDVO(String clId, String cuId, String coId, Integer qId, Integer qNo, String ans,
			Boolean isCrt, Boolean isLife) {
		this.clId = clId;
		this.cuId = cuId;
		this.coId = coId;
		this.qId = qId;
		this.qNo = qNo;
		this.ans = ans;
		this.isCrt = isCrt;
		this.isLife = isLife;
	}

	/**
	 * Instantiates a new cust cont answers DVO.
	 *
	 * @param clId           the cl id
	 * @param cuId           the cu id
	 * @param coId           the co id
	 * @param qId            the q id
	 * @param qNo            the q no
	 * @param ans            the ans
	 * @param isCrt          the is crt
	 * @param isLife         the is life
	 * @param aRwds          the a rwds
	 * @param crDate         the cr date
	 * @param upDate         the up date
	 * @param fbCallbackTime the fb callback time
	 * @param answerTime     the answer time
	 * @param retryCount     the retry count
	 * @param isAutoCreated  the is auto created
	 * @param aRwdType       the a rwd type
	 */
	public CustContAnswersDVO(String clId, String cuId, String coId, Integer qId, Integer qNo, String ans,
			Boolean isCrt, Boolean isLife, Double aRwds, Date crDate, Date upDate, String fbCallbackTime,
			String answerTime, Integer retryCount, Boolean isAutoCreated, String aRwdType) {
		this.clId = clId;
		this.cuId = cuId;
		this.coId = coId;
		this.qId = qId;
		this.qNo = qNo;
		this.ans = ans;
		this.isCrt = isCrt;
		this.isLife = isLife;
		this.aRwdType = aRwdType;
		this.aRwds = aRwds;
		this.crDate = crDate;
		this.upDate = upDate;
		this.fbCallbackTime = fbCallbackTime;
		this.answerTime = answerTime;
		this.retryCount = retryCount;
		this.isAutoCreated = isAutoCreated;
	}

	/*
	 * public CustContAnswersDVO(String cuId, String coId, Integer qId, Integer qNo,
	 * String ans, Boolean isCrt, Boolean isLife, Double aRwds, Double cuRwds,
	 * Integer cuLife, Date crDate,Date upDate, String fbCallbackTime,String
	 * answerTime,Integer retryCount,Boolean isAutoCreated) { this.cuId = cuId;
	 * this.coId = coId; this.qId = qId; this.qNo = qNo; this.ans = ans; this.isCrt
	 * = isCrt; this.isLife = isLife; this.aRwds = aRwds; this.cuRwds = cuRwds;
	 * this.cuLife = cuLife; this.crDate = crDate; this.upDate = upDate;
	 * this.fbCallbackTime=fbCallbackTime; this.answerTime=answerTime;
	 * this.retryCount=retryCount; this.isAutoCreated=isAutoCreated; }
	 */

	/**
	 * Gets the ans.
	 *
	 * @return the ans
	 */
	public String getAns() {
		return ans;
	}

	/**
	 * Gets the answer time.
	 *
	 * @return the answer time
	 */
	public String getAnswerTime() {
		if (answerTime == null) {
			answerTime = "";
		}
		return answerTime;
	}

	/**
	 * Gets the a rwds.
	 *
	 * @return the a rwds
	 */
	public Double getaRwds() {
		if (aRwds == null) {
			aRwds = 0.0;
		}
		return aRwds;
	}

	/**
	 * Gets the a rwd type.
	 *
	 * @return the a rwd type
	 */
	public String getaRwdType() {
		return aRwdType;
	}

	/**
	 * Gets the cl id.
	 *
	 * @return the cl id
	 */
	public String getClId() {
		return clId;
	}

	/**
	 * Gets the co id.
	 *
	 * @return the co id
	 */
	public String getCoId() {
		return coId;
	}

	/**
	 * Gets the cr date.
	 *
	 * @return the cr date
	 */
	public Date getCrDate() {
		return crDate;
	}

	/**
	 * Gets the cu id.
	 *
	 * @return the cu id
	 */
	public String getCuId() {
		return cuId;
	}

	/**
	 * Gets the cu life.
	 *
	 * @return the cu life
	 */
	public Integer getCuLife() {
		if (cuLife == null) {
			cuLife = 0;
		}
		return cuLife;
	}

	/**
	 * Gets the cu rwds.
	 *
	 * @return the cu rwds
	 */
	public Double getCuRwds() {
		if (cuRwds == null) {
			cuRwds = 0.0;
		}
		return cuRwds;
	}

	/**
	 * Gets the fb callback time.
	 *
	 * @return the fb callback time
	 */
	public String getFbCallbackTime() {
		if (fbCallbackTime == null) {
			fbCallbackTime = "";
		}
		return fbCallbackTime;
	}

	/**
	 * Gets the checks if is auto created.
	 *
	 * @return the checks if is auto created
	 */
	public Boolean getIsAutoCreated() {
		if (isAutoCreated == null) {
			isAutoCreated = false;
		}
		return isAutoCreated;
	}

	/**
	 * Gets the checks if is crt.
	 *
	 * @return the checks if is crt
	 */
	public Boolean getIsCrt() {
		return isCrt;
	}

	/**
	 * Gets the checks if is life.
	 *
	 * @return the checks if is life
	 */
	public Boolean getIsLife() {
		return isLife;
	}

	/**
	 * Gets the q id.
	 *
	 * @return the q id
	 */
	public Integer getqId() {
		return qId;
	}

	/**
	 * Gets the q no.
	 *
	 * @return the q no
	 */
	public Integer getqNo() {
		return qNo;
	}

	/**
	 * Gets the retry count.
	 *
	 * @return the retry count
	 */
	public Integer getRetryCount() {
		if (retryCount == null) {
			retryCount = 0;
		}
		return retryCount;
	}

	/**
	 * Gets the up date.
	 *
	 * @return the up date
	 */
	public Date getUpDate() {
		return upDate;
	}

	/**
	 * Sets the ans.
	 *
	 * @param ans the new ans
	 */
	public void setAns(String ans) {
		this.ans = ans;
	}

	/**
	 * Sets the answer time.
	 *
	 * @param answerTime the new answer time
	 */
	public void setAnswerTime(String answerTime) {
		this.answerTime = answerTime;
	}

	/**
	 * Sets the a rwds.
	 *
	 * @param aRwds the new a rwds
	 */
	public void setaRwds(Double aRwds) {
		this.aRwds = aRwds;
	}

	/**
	 * Sets the a rwd type.
	 *
	 * @param aRwdType the new a rwd type
	 */
	public void setaRwdType(String aRwdType) {
		this.aRwdType = aRwdType;
	}

	/**
	 * Sets the cl id.
	 *
	 * @param clId the new cl id
	 */
	public void setClId(String clId) {
		this.clId = clId;
	}

	/**
	 * Sets the co id.
	 *
	 * @param coId the new co id
	 */
	public void setCoId(String coId) {
		this.coId = coId;
	}

	/**
	 * Sets the cr date.
	 *
	 * @param crDate the new cr date
	 */
	public void setCrDate(Date crDate) {
		this.crDate = crDate;
	}

	/**
	 * Sets the cu id.
	 *
	 * @param cuId the new cu id
	 */
	public void setCuId(String cuId) {
		this.cuId = cuId;
	}

	/**
	 * Sets the cu life.
	 *
	 * @param cuLife the new cu life
	 */
	public void setCuLife(Integer cuLife) {
		this.cuLife = cuLife;
	}

	/**
	 * Sets the cu rwds.
	 *
	 * @param cuRwds the new cu rwds
	 */
	public void setCuRwds(Double cuRwds) {
		this.cuRwds = cuRwds;
	}

	/**
	 * Sets the fb callback time.
	 *
	 * @param fbCallbackTime the new fb callback time
	 */
	public void setFbCallbackTime(String fbCallbackTime) {
		this.fbCallbackTime = fbCallbackTime;
	}

	/**
	 * Sets the checks if is auto created.
	 *
	 * @param isAutoCreated the new checks if is auto created
	 */
	public void setIsAutoCreated(Boolean isAutoCreated) {
		this.isAutoCreated = isAutoCreated;
	}

	/**
	 * Sets the checks if is crt.
	 *
	 * @param isCrt the new checks if is crt
	 */
	public void setIsCrt(Boolean isCrt) {
		this.isCrt = isCrt;
	}

	/**
	 * Sets the checks if is life.
	 *
	 * @param isLife the new checks if is life
	 */
	public void setIsLife(Boolean isLife) {
		this.isLife = isLife;
	}

	/**
	 * Sets the q id.
	 *
	 * @param qId the new q id
	 */
	public void setqId(Integer qId) {
		this.qId = qId;
	}

	/**
	 * Sets the q no.
	 *
	 * @param qNo the new q no
	 */
	public void setqNo(Integer qNo) {
		this.qNo = qNo;
	}

	/**
	 * Sets the retry count.
	 *
	 * @param retryCount the new retry count
	 */
	public void setRetryCount(Integer retryCount) {
		this.retryCount = retryCount;
	}

	/**
	 * Sets the up date.
	 *
	 * @param upDate the new up date
	 */
	public void setUpDate(Date upDate) {
		this.upDate = upDate;
	}

}
