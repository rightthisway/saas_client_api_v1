/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.dvo;

import java.io.Serializable;
import java.util.Date;

/**
 * The Class ContestGrandWinnerDVO.
 */
public class ContestGrandWinnerDVO implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -2157195594537903648L;

	/** The cl id. */
	private String clId;

	/** The co id. */
	private String coId;

	/** The cu id. */
	private String cuId;

	/** The rwd type. */
	private String rwdType;

	/** The rwd val. */
	private Double rwdVal;

	/** The cr date. */
	private Date crDate;

	/** The up date. */
	private Date upDate;

	/** The u id. */
	private String uId;

	/** The img U. */
	private String imgU;

	/** The email. */
	private String email;

	/**
	 * Instantiates a new contest grand winner DVO.
	 */
	public ContestGrandWinnerDVO() {

	}

	/**
	 * Instantiates a new contest grand winner DVO.
	 *
	 * @param clId    the cl id
	 * @param coId    the co id
	 * @param cuId    the cu id
	 * @param rwdType the rwd type
	 * @param rwdVal  the rwd val
	 */
	public ContestGrandWinnerDVO(String clId, String coId, String cuId, String rwdType, Double rwdVal) {
		this.clId = clId;
		this.coId = coId;
		this.cuId = cuId;
		this.rwdType = rwdType;
		this.rwdVal = rwdVal;
	}

	/**
	 * Instantiates a new contest grand winner DVO.
	 *
	 * @param clId    the cl id
	 * @param coId    the co id
	 * @param cuId    the cu id
	 * @param rwdType the rwd type
	 * @param rwdVal  the rwd val
	 * @param crDate  the cr date
	 * @param upDate  the up date
	 */
	public ContestGrandWinnerDVO(String clId, String coId, String cuId, String rwdType, Double rwdVal, Date crDate,
			Date upDate) {
		this.clId = clId;
		this.coId = coId;
		this.cuId = cuId;
		this.rwdType = rwdType;
		this.rwdVal = rwdVal;
		this.crDate = crDate;
		this.upDate = upDate;
	}

	/**
	 * Gets the cl id.
	 *
	 * @return the cl id
	 */
	public String getClId() {
		return clId;
	}

	/**
	 * Gets the co id.
	 *
	 * @return the co id
	 */
	public String getCoId() {
		return coId;
	}

	/**
	 * Gets the cr date.
	 *
	 * @return the cr date
	 */
	public Date getCrDate() {
		return crDate;
	}

	/**
	 * Gets the cu id.
	 *
	 * @return the cu id
	 */
	public String getCuId() {
		return cuId;
	}

	/**
	 * Gets the email.
	 *
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Gets the img U.
	 *
	 * @return the img U
	 */
	public String getImgU() {
		return imgU;
	}

	/**
	 * Gets the rwd type.
	 *
	 * @return the rwd type
	 */
	public String getRwdType() {
		return rwdType;
	}

	/**
	 * Gets the rwd val.
	 *
	 * @return the rwd val
	 */
	public Double getRwdVal() {
		return rwdVal;
	}

	/**
	 * Gets the u id.
	 *
	 * @return the u id
	 */
	public String getuId() {
		return uId;
	}

	/**
	 * Gets the up date.
	 *
	 * @return the up date
	 */
	public Date getUpDate() {
		return upDate;
	}

	/**
	 * Sets the cl id.
	 *
	 * @param clId the new cl id
	 */
	public void setClId(String clId) {
		this.clId = clId;
	}

	/**
	 * Sets the co id.
	 *
	 * @param coId the new co id
	 */
	public void setCoId(String coId) {
		this.coId = coId;
	}

	/**
	 * Sets the cr date.
	 *
	 * @param crDate the new cr date
	 */
	public void setCrDate(Date crDate) {
		this.crDate = crDate;
	}

	/**
	 * Sets the cu id.
	 *
	 * @param cuId the new cu id
	 */
	public void setCuId(String cuId) {
		this.cuId = cuId;
	}

	/**
	 * Sets the email.
	 *
	 * @param email the new email
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * Sets the img U.
	 *
	 * @param imgU the new img U
	 */
	public void setImgU(String imgU) {
		this.imgU = imgU;
	}

	/**
	 * Sets the rwd type.
	 *
	 * @param rwdType the new rwd type
	 */
	public void setRwdType(String rwdType) {
		this.rwdType = rwdType;
	}

	/**
	 * Sets the rwd val.
	 *
	 * @param rwdVal the new rwd val
	 */
	public void setRwdVal(Double rwdVal) {
		this.rwdVal = rwdVal;
	}

	/**
	 * Sets the u id.
	 *
	 * @param uId the new u id
	 */
	public void setuId(String uId) {
		this.uId = uId;
	}

	/**
	 * Sets the up date.
	 *
	 * @param upDate the new up date
	 */
	public void setUpDate(Date upDate) {
		this.upDate = upDate;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "ContestGrandWinnerDVO [clId=" + clId + ", coId=" + coId + ", cuId=" + cuId + ", rwdType=" + rwdType
				+ ", rwdVal=" + rwdVal + ", crDate=" + crDate + ", uId=" + uId + ", imgU=" + imgU + ", email=" + email
				+ "]";
	}
}
