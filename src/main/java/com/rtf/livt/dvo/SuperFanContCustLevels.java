/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.dvo;

import java.io.Serializable;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * The Class SuperFanContCustLevels.
 */
@XStreamAlias("SuperFanContCustLevels")
public class SuperFanContCustLevels implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -3783030334415667311L;

	/** The cu id. */
	private Integer cuId;

	/** The co id. */
	private Integer coId;

	/** The q no. */
	private Integer qNo;

	/** The sf stars. */
	private Integer sfStars;

	/**
	 * Instantiates a new super fan cont cust levels.
	 */
	public SuperFanContCustLevels() {

	}

	/**
	 * Instantiates a new super fan cont cust levels.
	 *
	 * @param cuId    the cu id
	 * @param coId    the co id
	 * @param qNo     the q no
	 * @param sfStars the sf stars
	 */
	public SuperFanContCustLevels(Integer cuId, Integer coId, Integer qNo, Integer sfStars) {
		super();
		this.cuId = cuId;
		this.coId = coId;
		this.qNo = qNo;
		this.sfStars = sfStars;
	}

	/**
	 * Gets the co id.
	 *
	 * @return the co id
	 */
	public Integer getCoId() {
		return coId;
	}

	/**
	 * Gets the cu id.
	 *
	 * @return the cu id
	 */
	public Integer getCuId() {
		return cuId;
	}

	/**
	 * Gets the q no.
	 *
	 * @return the q no
	 */
	public Integer getqNo() {
		if (qNo == null) {
			qNo = 0;
		}
		return qNo;
	}

	/**
	 * Gets the sf stars.
	 *
	 * @return the sf stars
	 */
	public Integer getSfStars() {
		if (sfStars == null) {
			sfStars = 0;
		}
		return sfStars;
	}

	/**
	 * Sets the co id.
	 *
	 * @param coId the new co id
	 */
	public void setCoId(Integer coId) {
		this.coId = coId;
	}

	/**
	 * Sets the cu id.
	 *
	 * @param cuId the new cu id
	 */
	public void setCuId(Integer cuId) {
		this.cuId = cuId;
	}

	/**
	 * Sets the q no.
	 *
	 * @param qNo the new q no
	 */
	public void setqNo(Integer qNo) {
		this.qNo = qNo;
	}

	/**
	 * Sets the sf stars.
	 *
	 * @param sfStars the new sf stars
	 */
	public void setSfStars(Integer sfStars) {
		this.sfStars = sfStars;
	}

}
