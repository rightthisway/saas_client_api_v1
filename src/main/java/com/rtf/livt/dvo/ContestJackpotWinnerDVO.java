/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.dvo;

import java.io.Serializable;

/**
 * The Class ContestJackpotWinnerDVO.
 */
public class ContestJackpotWinnerDVO implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 849171376886715285L;

	/** The cl id. */
	private String clId;

	/** The co id. */
	private String coId;

	/** The q id. */
	private Integer qId;

	/** The q no. */
	private Integer qNo;

	/** The cu id. */
	private String cuId;

	/** The j type. */
	private String jType;

	/** The rwd type. */
	private String rwdType;

	/** The rwd val. */
	private Double rwdVal;

	/** The cr date. */
	private Long crDate;

	/** The status. */
	private String status;

	/** The email. */
	private String email;

	/** The img U. */
	private String imgU;

	/**
	 * Gets the cl id.
	 *
	 * @return the cl id
	 */
	public String getClId() {
		return clId;
	}

	/**
	 * Gets the co id.
	 *
	 * @return the co id
	 */
	public String getCoId() {
		return coId;
	}

	/**
	 * Gets the cr date.
	 *
	 * @return the cr date
	 */
	public Long getCrDate() {
		return crDate;
	}

	/**
	 * Gets the cu id.
	 *
	 * @return the cu id
	 */
	public String getCuId() {
		return cuId;
	}

	/**
	 * Gets the email.
	 *
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Gets the img U.
	 *
	 * @return the img U
	 */
	public String getImgU() {
		return imgU;
	}

	/**
	 * Gets the j type.
	 *
	 * @return the j type
	 */
	public String getjType() {
		return jType;
	}

	/**
	 * Gets the q id.
	 *
	 * @return the q id
	 */
	public Integer getqId() {
		return qId;
	}

	/**
	 * Gets the q no.
	 *
	 * @return the q no
	 */
	public Integer getqNo() {
		return qNo;
	}

	/**
	 * Gets the rwd type.
	 *
	 * @return the rwd type
	 */
	public String getRwdType() {
		return rwdType;
	}

	/**
	 * Gets the rwd val.
	 *
	 * @return the rwd val
	 */
	public Double getRwdVal() {
		return rwdVal;
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Sets the cl id.
	 *
	 * @param clId the new cl id
	 */
	public void setClId(String clId) {
		this.clId = clId;
	}

	/**
	 * Sets the co id.
	 *
	 * @param coId the new co id
	 */
	public void setCoId(String coId) {
		this.coId = coId;
	}

	/**
	 * Sets the cr date.
	 *
	 * @param crDate the new cr date
	 */
	public void setCrDate(Long crDate) {
		this.crDate = crDate;
	}

	/**
	 * Sets the cu id.
	 *
	 * @param cuId the new cu id
	 */
	public void setCuId(String cuId) {
		this.cuId = cuId;
	}

	/**
	 * Sets the email.
	 *
	 * @param email the new email
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * Sets the img U.
	 *
	 * @param imgU the new img U
	 */
	public void setImgU(String imgU) {
		this.imgU = imgU;
	}

	/**
	 * Sets the j type.
	 *
	 * @param jType the new j type
	 */
	public void setjType(String jType) {
		this.jType = jType;
	}

	/**
	 * Sets the q id.
	 *
	 * @param qId the new q id
	 */
	public void setqId(Integer qId) {
		this.qId = qId;
	}

	/**
	 * Sets the q no.
	 *
	 * @param qNo the new q no
	 */
	public void setqNo(Integer qNo) {
		this.qNo = qNo;
	}

	/**
	 * Sets the rwd type.
	 *
	 * @param rwdType the new rwd type
	 */
	public void setRwdType(String rwdType) {
		this.rwdType = rwdType;
	}

	/**
	 * Sets the rwd val.
	 *
	 * @param rwdVal the new rwd val
	 */
	public void setRwdVal(Double rwdVal) {
		this.rwdVal = rwdVal;
	}

	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(String status) {
		this.status = status;
	}

}
