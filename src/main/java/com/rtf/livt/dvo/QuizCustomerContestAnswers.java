/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.dvo;

import java.io.Serializable;
import java.util.Date;

import org.codehaus.jackson.annotate.JsonIgnore;

/**
 * The Class QuizCustomerContestAnswers.
 */
public class QuizCustomerContestAnswers implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -2404055267157287691L;

	/** The id. */
	private Integer id;

	/** The customer id. */
	private String customerId;

	/** The contest id. */
	private String contestId;

	/** The question id. */
	private Integer questionId;

	/** The question S no. */
	private Integer questionSNo;

	/** The answer. */
	private String answer;

	/** The is correct answer. */
	private Boolean isCorrectAnswer;

	/** The is life line used. */
	private Boolean isLifeLineUsed;

	/** The answer rewards. */
	private Double answerRewards;

	/** The fb callback time. */
	private Date fbCallbackTime;

	/** The answer time. */
	private Date answerTime;

	/** The retry count. */
	private Integer retryCount;

	/** The is auto created. */
	private Boolean isAutoCreated;

	/** The rtf points. */
	private Integer rtfPoints;

	/** The cumulative rewards. */
	@JsonIgnore
	private Double cumulativeRewards;

	/** The cumulative life line used. */
	@JsonIgnore
	private Integer cumulativeLifeLineUsed;

	/** The created date time. */
	@JsonIgnore
	private Date createdDateTime;

	/** The updated date time. */
	@JsonIgnore
	private Date updatedDateTime;

	/**
	 * Gets the answer.
	 *
	 * @return the answer
	 */
	public String getAnswer() {
		return answer;
	}

	/**
	 * Gets the answer rewards.
	 *
	 * @return the answer rewards
	 */
	public Double getAnswerRewards() {
		if (answerRewards == null) {
			answerRewards = 0.0;
		}
		return answerRewards;
	}

	/**
	 * Gets the answer time.
	 *
	 * @return the answer time
	 */
	public Date getAnswerTime() {
		return answerTime;
	}

	/**
	 * Gets the contest id.
	 *
	 * @return the contest id
	 */
	public String getContestId() {
		return contestId;
	}

	/**
	 * Gets the created date time.
	 *
	 * @return the created date time
	 */
	public Date getCreatedDateTime() {
		return createdDateTime;
	}

	/**
	 * Gets the cumulative life line used.
	 *
	 * @return the cumulative life line used
	 */
	public Integer getCumulativeLifeLineUsed() {
		return cumulativeLifeLineUsed;
	}

	/**
	 * Gets the cumulative rewards.
	 *
	 * @return the cumulative rewards
	 */
	public Double getCumulativeRewards() {
		return cumulativeRewards;
	}

	/**
	 * Gets the customer id.
	 *
	 * @return the customer id
	 */
	public String getCustomerId() {
		return customerId;
	}

	/**
	 * Gets the fb callback time.
	 *
	 * @return the fb callback time
	 */
	public Date getFbCallbackTime() {
		return fbCallbackTime;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * Gets the checks if is auto created.
	 *
	 * @return the checks if is auto created
	 */
	public Boolean getIsAutoCreated() {
		if (isAutoCreated == null) {
			isAutoCreated = false;
		}
		return isAutoCreated;
	}

	/**
	 * Gets the checks if is correct answer.
	 *
	 * @return the checks if is correct answer
	 */
	public Boolean getIsCorrectAnswer() {
		return isCorrectAnswer;
	}

	/**
	 * Gets the checks if is life line used.
	 *
	 * @return the checks if is life line used
	 */
	public Boolean getIsLifeLineUsed() {
		return isLifeLineUsed;
	}

	/**
	 * Gets the question id.
	 *
	 * @return the question id
	 */
	public Integer getQuestionId() {
		return questionId;
	}

	/**
	 * Gets the question S no.
	 *
	 * @return the question S no
	 */
	public Integer getQuestionSNo() {
		return questionSNo;
	}

	/**
	 * Gets the retry count.
	 *
	 * @return the retry count
	 */
	public Integer getRetryCount() {
		if (retryCount == null) {
			retryCount = 0;
		}
		return retryCount;
	}

	/**
	 * Gets the rtf points.
	 *
	 * @return the rtf points
	 */
	public Integer getRtfPoints() {
		if (rtfPoints == null) {
			rtfPoints = 0;
		}
		return rtfPoints;
	}

	/**
	 * Gets the updated date time.
	 *
	 * @return the updated date time
	 */
	public Date getUpdatedDateTime() {
		return updatedDateTime;
	}

	/**
	 * Sets the answer.
	 *
	 * @param answer the new answer
	 */
	public void setAnswer(String answer) {
		this.answer = answer;
	}

	/**
	 * Sets the answer rewards.
	 *
	 * @param answerRewards the new answer rewards
	 */
	public void setAnswerRewards(Double answerRewards) {
		this.answerRewards = answerRewards;
	}

	/**
	 * Sets the answer time.
	 *
	 * @param answerTime the new answer time
	 */
	public void setAnswerTime(Date answerTime) {
		this.answerTime = answerTime;
	}

	/**
	 * Sets the contest id.
	 *
	 * @param contestId the new contest id
	 */
	public void setContestId(String contestId) {
		this.contestId = contestId;
	}

	/**
	 * Sets the created date time.
	 *
	 * @param createdDateTime the new created date time
	 */
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}

	/**
	 * Sets the cumulative life line used.
	 *
	 * @param cumulativeLifeLineUsed the new cumulative life line used
	 */
	public void setCumulativeLifeLineUsed(Integer cumulativeLifeLineUsed) {
		this.cumulativeLifeLineUsed = cumulativeLifeLineUsed;
	}

	/**
	 * Sets the cumulative rewards.
	 *
	 * @param cumulativeRewards the new cumulative rewards
	 */
	public void setCumulativeRewards(Double cumulativeRewards) {
		this.cumulativeRewards = cumulativeRewards;
	}

	/**
	 * Sets the customer id.
	 *
	 * @param customerId the new customer id
	 */
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	/**
	 * Sets the fb callback time.
	 *
	 * @param fbCallbackTime the new fb callback time
	 */
	public void setFbCallbackTime(Date fbCallbackTime) {
		this.fbCallbackTime = fbCallbackTime;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * Sets the checks if is auto created.
	 *
	 * @param isAutoCreated the new checks if is auto created
	 */
	public void setIsAutoCreated(Boolean isAutoCreated) {
		this.isAutoCreated = isAutoCreated;
	}

	/**
	 * Sets the checks if is correct answer.
	 *
	 * @param isCorrectAnswer the new checks if is correct answer
	 */
	public void setIsCorrectAnswer(Boolean isCorrectAnswer) {
		this.isCorrectAnswer = isCorrectAnswer;
	}

	/**
	 * Sets the checks if is life line used.
	 *
	 * @param isLifeLineUsed the new checks if is life line used
	 */
	public void setIsLifeLineUsed(Boolean isLifeLineUsed) {
		this.isLifeLineUsed = isLifeLineUsed;
	}

	/**
	 * Sets the question id.
	 *
	 * @param questionId the new question id
	 */
	public void setQuestionId(Integer questionId) {
		this.questionId = questionId;
	}

	/**
	 * Sets the question S no.
	 *
	 * @param questionSNo the new question S no
	 */
	public void setQuestionSNo(Integer questionSNo) {
		this.questionSNo = questionSNo;
	}

	/**
	 * Sets the retry count.
	 *
	 * @param retryCount the new retry count
	 */
	public void setRetryCount(Integer retryCount) {
		this.retryCount = retryCount;
	}

	/**
	 * Sets the rtf points.
	 *
	 * @param rtfPoints the new rtf points
	 */
	public void setRtfPoints(Integer rtfPoints) {
		this.rtfPoints = rtfPoints;
	}

	/**
	 * Sets the updated date time.
	 *
	 * @param updatedDateTime the new updated date time
	 */
	public void setUpdatedDateTime(Date updatedDateTime) {
		this.updatedDateTime = updatedDateTime;
	}

}
