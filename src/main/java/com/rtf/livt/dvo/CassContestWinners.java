/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.dvo;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.rtf.livt.util.TicketUtil;
import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * The Class CassContestWinners.
 */
@XStreamAlias("ContestWinners")
public class CassContestWinners implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 6347595369130873076L;

	/** The cl id. */
	private String clId;

	/** The co id. */
	private String coId;

	/** The cu id. */
	private String cuId;

	/** The q id. */
	private Integer qId;

	/** The r tix. */
	private Integer rTix;

	/** The r points. */
	private Double rPoints;

	/** The cr dated. */
	@JsonIgnore
	private Long crDated;

	/** The img P. */
	@JsonIgnore
	private String imgP;// profile image path

	/** The img U. */
	private String imgU;// profile image URL

	/** The u id. */
	private String uId;

	/** The r points st. */
	private String rPointsSt;

	/** The jackpot st. */
	@JsonIgnore
	private String jackpotSt;

	/** The j credit st. */
	private String jCreditSt;

	/** The no of winning chance. */
	@JsonIgnore
	private Integer noOfWinningChance;

	/**
	 * Instantiates a new cass contest winners.
	 */
	public CassContestWinners() {
	}

	/**
	 * Instantiates a new cass contest winners.
	 *
	 * @param clId the cl id
	 * @param coId the co id
	 * @param cuId the cu id
	 */
	public CassContestWinners(String clId, String coId, String cuId) {
		this.clId = clId;
		this.coId = coId;
		this.cuId = cuId;
	}

	/**
	 * Instantiates a new cass contest winners.
	 *
	 * @param clId              the cl id
	 * @param coId              the co id
	 * @param cuId              the cu id
	 * @param rTix              the r tix
	 * @param rPoints           the r points
	 * @param crDated           the cr dated
	 * @param noOfWinningChance the no of winning chance
	 */
	public CassContestWinners(String clId, String coId, String cuId, Integer rTix, Double rPoints, Long crDated,
			Integer noOfWinningChance) {
		this.clId = clId;
		this.coId = coId;
		this.cuId = cuId;
		this.rTix = rTix;
		this.rPoints = rPoints;
		this.crDated = crDated;
		this.noOfWinningChance = noOfWinningChance;
	}

	/**
	 * Instantiates a new cass contest winners.
	 *
	 * @param clId              the cl id
	 * @param coId              the co id
	 * @param cuId              the cu id
	 * @param rTix              the r tix
	 * @param jackpotSt         the jackpot st
	 * @param crDated           the cr dated
	 * @param noOfWinningChance the no of winning chance
	 */
	public CassContestWinners(String clId, String coId, String cuId, Integer rTix, String jackpotSt, Long crDated,
			Integer noOfWinningChance) {
		this.clId = clId;
		this.coId = coId;
		this.cuId = cuId;
		this.rTix = rTix;
		this.jackpotSt = jackpotSt;
		this.crDated = crDated;
		this.noOfWinningChance = noOfWinningChance;
	}

	/**
	 * Gets the cl id.
	 *
	 * @return the cl id
	 */
	public String getClId() {
		return clId;
	}

	/**
	 * Gets the co id.
	 *
	 * @return the co id
	 */
	public String getCoId() {
		return coId;
	}

	/**
	 * Gets the cr dated.
	 *
	 * @return the cr dated
	 */
	public Long getCrDated() {
		return crDated;
	}

	/**
	 * Gets the cu id.
	 *
	 * @return the cu id
	 */
	public String getCuId() {
		return cuId;
	}

	/**
	 * Gets the img P.
	 *
	 * @return the img P
	 */
	public String getImgP() {
		return imgP;
	}

	/**
	 * Gets the img U.
	 *
	 * @return the img U
	 */
	public String getImgU() {
		// imgU = URLUtil.profilePicWebURByImageName(this.imgP);
		// imgU = URLUtil.profilePicForSummary(this.imgP);
		imgU = "https://rtfmedia.s3.amazonaws.com/rtficon.png";
		return imgU;
	}

	/**
	 * Gets the jackpot st.
	 *
	 * @return the jackpot st
	 */
	public String getJackpotSt() {
		if (null == jackpotSt) {
			jackpotSt = "";
		}
		return jackpotSt;
	}

	/**
	 * Gets the j credit st.
	 *
	 * @return the j credit st
	 */
	public String getjCreditSt() {
		if (null == jCreditSt) {
			jCreditSt = "";
		}
		return jCreditSt;
	}

	/**
	 * Gets the no of winning chance.
	 *
	 * @return the no of winning chance
	 */
	public Integer getNoOfWinningChance() {
		if (null == noOfWinningChance) {
			noOfWinningChance = 1;
		}
		return noOfWinningChance;
	}

	/**
	 * Gets the q id.
	 *
	 * @return the q id
	 */
	public Integer getqId() {
		return qId;
	}

	/**
	 * Gets the r points.
	 *
	 * @return the r points
	 */
	public Double getrPoints() {
		if (rPoints == null) {
			rPoints = 0.0;
		}
		return rPoints;
	}

	/**
	 * Gets the r points st.
	 *
	 * @return the r points st
	 */
	public String getrPointsSt() {
		String jakcpotStr = getJackpotSt();
		if (null != jakcpotStr && !jakcpotStr.isEmpty()) {
			rPointsSt = jakcpotStr;
		} else {
			if (rPoints != null) {
				try {
					rPointsSt = TicketUtil.getRoundedValueString(rPoints);
				} catch (Exception e) {
					rPointsSt = "0.00";
					e.printStackTrace();
				}
			} else {
				rPointsSt = "0.00";
			}
		}

		return rPointsSt;
	}

	/**
	 * Gets the r tix.
	 *
	 * @return the r tix
	 */
	public Integer getrTix() {
		return rTix;
	}

	/**
	 * Gets the u id.
	 *
	 * @return the u id
	 */
	public String getuId() {
		return uId;
	}

	/**
	 * Sets the cl id.
	 *
	 * @param clId the new cl id
	 */
	public void setClId(String clId) {
		this.clId = clId;
	}

	/**
	 * Sets the co id.
	 *
	 * @param coId the new co id
	 */
	public void setCoId(String coId) {
		this.coId = coId;
	}

	/**
	 * Sets the cr dated.
	 *
	 * @param crDated the new cr dated
	 */
	public void setCrDated(Long crDated) {
		this.crDated = crDated;
	}

	/**
	 * Sets the cu id.
	 *
	 * @param cuId the new cu id
	 */
	public void setCuId(String cuId) {
		this.cuId = cuId;
	}

	/**
	 * Sets the img P.
	 *
	 * @param imgP the new img P
	 */
	public void setImgP(String imgP) {
		this.imgP = imgP;
	}

	/**
	 * Sets the img U.
	 *
	 * @param imgU the new img U
	 */
	public void setImgU(String imgU) {
		this.imgU = imgU;
	}

	/**
	 * Sets the jackpot st.
	 *
	 * @param jackpotSt the new jackpot st
	 */
	public void setJackpotSt(String jackpotSt) {
		this.jackpotSt = jackpotSt;
	}

	/**
	 * Sets the j credit st.
	 *
	 * @param jCreditSt the new j credit st
	 */
	public void setjCreditSt(String jCreditSt) {
		this.jCreditSt = jCreditSt;
	}

	/**
	 * Sets the no of winning chance.
	 *
	 * @param noOfWinningChance the new no of winning chance
	 */
	public void setNoOfWinningChance(Integer noOfWinningChance) {
		this.noOfWinningChance = noOfWinningChance;
	}

	/**
	 * Sets the q id.
	 *
	 * @param qId the new q id
	 */
	public void setqId(Integer qId) {
		this.qId = qId;
	}

	/**
	 * Sets the r points.
	 *
	 * @param rPoints the new r points
	 */
	public void setrPoints(Double rPoints) {
		this.rPoints = rPoints;
	}

	/**
	 * Sets the r points st.
	 *
	 * @param rPointsSt the new r points st
	 */
	public void setrPointsSt(String rPointsSt) {
		this.rPointsSt = rPointsSt;
	}

	/**
	 * Sets the r tix.
	 *
	 * @param rTix the new r tix
	 */
	public void setrTix(Integer rTix) {
		this.rTix = rTix;
	}

	/**
	 * Sets the u id.
	 *
	 * @param uId the new u id
	 */
	public void setuId(String uId) {
		this.uId = uId;
	}

}
