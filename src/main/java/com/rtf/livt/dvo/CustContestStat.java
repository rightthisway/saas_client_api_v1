/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.dvo;

import java.io.Serializable;

/**
 * The Class CustContestStat.
 */
public class CustContestStat implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 6079333824272656938L;

	/** The lives used. */
	private Integer livesUsed;

	/** The cumulative ans rewards. */
	private Double cumulativeAnsRewards;

	/**
	 * Gets the cumulative ans rewards.
	 *
	 * @return the cumulative ans rewards
	 */
	public Double getCumulativeAnsRewards() {
		return cumulativeAnsRewards;
	}

	/**
	 * Gets the lives used.
	 *
	 * @return the lives used
	 */
	public Integer getLivesUsed() {
		return livesUsed;
	}

	/**
	 * Sets the cumulative ans rewards.
	 *
	 * @param cumulativeAnsRewards the new cumulative ans rewards
	 */
	public void setCumulativeAnsRewards(Double cumulativeAnsRewards) {
		this.cumulativeAnsRewards = cumulativeAnsRewards;
	}

	/**
	 * Sets the lives used.
	 *
	 * @param livesUsed the new lives used
	 */
	public void setLivesUsed(Integer livesUsed) {
		this.livesUsed = livesUsed;
	}

}
