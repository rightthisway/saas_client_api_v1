/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.dvo;

import java.io.Serializable;
import java.text.SimpleDateFormat;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * The Class CassContestGrandWinner.
 */
@XStreamAlias("ContestGrandWinner")
public class CassContestGrandWinner implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1932248913212804511L;

	/** The cl id. */
	private String clId;

	/** The co id. */
	private String coId;

	/** The q id. */
	private Integer qId;

	/** The cu id. */
	private String cuId;

	/** The r tix. */
	private Integer rTix;

	/** The r points. */
	private Double rPoints;

	/** The cr dated. */
	@JsonIgnore
	private Long crDated;

	/** The o id. */
	@JsonIgnore
	private Integer oId;

	/** The status. */
	@JsonIgnore
	private String status;

	/** The ex date. */
	@JsonIgnore
	private Long exDate;

	/** The date time format 1. */
	@JsonIgnore
	private SimpleDateFormat dateTimeFormat1 = new SimpleDateFormat("MM/dd/yyyy hh:mm aa");

	/** The jackpot st. */
	private String jackpotSt;

	/** The j credit st. */
	private String jCreditSt;

	/**
	 * Instantiates a new cass contest grand winner.
	 */
	public CassContestGrandWinner() {
	}

	/**
	 * Instantiates a new cass contest grand winner.
	 *
	 * @param cuId the cu id
	 */
	public CassContestGrandWinner(String cuId) {
		this.cuId = cuId;
	}

	/**
	 * Instantiates a new cass contest grand winner.
	 *
	 * @param clId    the cl id
	 * @param coId    the co id
	 * @param cuId    the cu id
	 * @param rTix    the r tix
	 * @param crDated the cr dated
	 * @param oId     the o id
	 * @param status  the status
	 * @param exDate  the ex date
	 */
	public CassContestGrandWinner(String clId, String coId, String cuId, Integer rTix, Long crDated, Integer oId,
			String status, Long exDate) {
		this.clId = clId;
		this.coId = coId;
		this.cuId = cuId;
		this.rTix = rTix;
		this.crDated = crDated;
		this.oId = oId;
		this.status = status;
		this.exDate = exDate;
	}

	/**
	 * Instantiates a new cass contest grand winner.
	 *
	 * @param clId      the cl id
	 * @param coId      the co id
	 * @param cuId      the cu id
	 * @param qId       the q id
	 * @param crDated   the cr dated
	 * @param status    the status
	 * @param jackpotSt the jackpot st
	 * @param jCreditSt the j credit st
	 */
	public CassContestGrandWinner(String clId, String coId, String cuId, Integer qId, Long crDated, String status,
			String jackpotSt, String jCreditSt) {
		this.clId = clId;
		this.coId = coId;
		this.cuId = cuId;
		this.qId = qId;
		this.crDated = crDated;
		this.status = status;
		this.jackpotSt = jackpotSt;
		this.jCreditSt = jCreditSt;
	}

	/**
	 * Gets the cl id.
	 *
	 * @return the cl id
	 */
	public String getClId() {
		return clId;
	}

	/**
	 * Gets the co id.
	 *
	 * @return the co id
	 */
	public String getCoId() {
		return coId;
	}

	/**
	 * Gets the cr dated.
	 *
	 * @return the cr dated
	 */
	public Long getCrDated() {
		return crDated;
	}

	/**
	 * Gets the cu id.
	 *
	 * @return the cu id
	 */
	public String getCuId() {
		return cuId;
	}

	/**
	 * Gets the ex date.
	 *
	 * @return the ex date
	 */
	public Long getExDate() {
		return exDate;
	}

	/**
	 * Gets the jackpot st.
	 *
	 * @return the jackpot st
	 */
	public final String getJackpotSt() {
		if (null == jackpotSt) {
			jackpotSt = "";
		}
		return jackpotSt;
	}

	/**
	 * Gets the j credit st.
	 *
	 * @return the j credit st
	 */
	public final String getjCreditSt() {
		return jCreditSt;
	}

	/**
	 * Gets the o id.
	 *
	 * @return the o id
	 */
	public Integer getoId() {
		return oId;
	}

	/**
	 * Gets the q id.
	 *
	 * @return the q id
	 */
	public final Integer getqId() {
		return qId;
	}

	/**
	 * Gets the r points.
	 *
	 * @return the r points
	 */
	public Double getrPoints() {
		return rPoints;
	}

	/**
	 * Gets the r tix.
	 *
	 * @return the r tix
	 */
	public Integer getrTix() {
		return rTix;
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Sets the cl id.
	 *
	 * @param clId the new cl id
	 */
	public void setClId(String clId) {
		this.clId = clId;
	}

	/**
	 * Sets the co id.
	 *
	 * @param coId the new co id
	 */
	public void setCoId(String coId) {
		this.coId = coId;
	}

	/**
	 * Sets the cr dated.
	 *
	 * @param crDated the new cr dated
	 */
	public void setCrDated(Long crDated) {
		this.crDated = crDated;
	}

	/**
	 * Sets the cu id.
	 *
	 * @param cuId the new cu id
	 */
	public void setCuId(String cuId) {
		this.cuId = cuId;
	}

	/**
	 * Sets the ex date.
	 *
	 * @param exDate the new ex date
	 */
	public void setExDate(Long exDate) {
		this.exDate = exDate;
	}

	/**
	 * Sets the jackpot st.
	 *
	 * @param jackpotSt the new jackpot st
	 */
	public final void setJackpotSt(String jackpotSt) {
		this.jackpotSt = jackpotSt;
	}

	/**
	 * Sets the j credit st.
	 *
	 * @param jCreditSt the new j credit st
	 */
	public final void setjCreditSt(String jCreditSt) {
		this.jCreditSt = jCreditSt;
	}

	/**
	 * Sets the o id.
	 *
	 * @param oId the new o id
	 */
	public void setoId(Integer oId) {
		this.oId = oId;
	}

	/**
	 * Sets the q id.
	 *
	 * @param qId the new q id
	 */
	public final void setqId(Integer qId) {
		this.qId = qId;
	}

	/**
	 * Sets the r points.
	 *
	 * @param rPoints the new r points
	 */
	public void setrPoints(Double rPoints) {
		this.rPoints = rPoints;
	}

	/**
	 * Sets the r tix.
	 *
	 * @param rTix the new r tix
	 */
	public void setrTix(Integer rTix) {
		this.rTix = rTix;
	}

	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(String status) {
		this.status = status;
	}

}
