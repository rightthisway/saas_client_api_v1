/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.dvo;

import java.io.Serializable;

/**
 * The Class CustContDtlsDVO.
 */
public class CustContDtlsDVO implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -5784188659086743834L;

	/** The cl id. */
	private String clId;

	/** The cu id. */
	private String cuId;

	/** The co id. */
	private String coId;

	/** The lq no. */
	private Integer lqNo;

	/** The is lq crt. */
	private Boolean isLqCrt;

	/** The is lq life. */
	private Boolean isLqLife;

	/** The ans rwd type. */
	private String ansRwdType;

	/** The ans rwds. */
	private Double ansRwds;

	/** The cu ans. */
	private Integer cuAns;

	/** The cu life. */
	private Integer cuLife;

	/** The sfl rwds. */
	private Double sflRwds;

	/**
	 * Instantiates a new cust cont dtls DVO.
	 */
	public CustContDtlsDVO() {

	}

	/**
	 * Instantiates a new cust cont dtls DVO.
	 *
	 * @param clId       the client Id
	 * @param cuId       the cu id
	 * @param coId       the co id
	 * @param lqNo       the lq no
	 * @param isLqCrt    the is lq crt
	 * @param isLqLife   the is lq life
	 * @param ansRwdType the ans rwd type
	 * @param ansRwds    the ans rwds
	 * @param cuAns      the cu ans
	 * @param cuLife     the cu life
	 * @param sflRwds    the sfl rwds
	 */
	public CustContDtlsDVO(String clId, String cuId, String coId, Integer lqNo, Boolean isLqCrt, Boolean isLqLife,
			String ansRwdType, Double ansRwds, Integer cuAns, Integer cuLife, Double sflRwds) {
		super();
		this.clId = clId;
		this.cuId = cuId;
		this.coId = coId;
		this.lqNo = lqNo;
		this.isLqCrt = isLqCrt;
		this.isLqLife = isLqLife;
		this.ansRwdType = ansRwdType;
		this.ansRwds = ansRwds;
		this.cuAns = cuAns;
		this.cuLife = cuLife;
		this.sflRwds = sflRwds;
	}

	/**
	 * Gets the ans rwds.
	 *
	 * @return the ans rwds
	 */
	public Double getAnsRwds() {
		if (ansRwds == null) {
			ansRwds = 0.0;
		}
		return ansRwds;
	}

	/**
	 * Gets the ans rwd type.
	 *
	 * @return the ans rwd type
	 */
	public String getAnsRwdType() {
		return ansRwdType;
	}

	/**
	 * Gets the cl id.
	 *
	 * @return the cl id
	 */
	public String getClId() {
		return clId;
	}

	/**
	 * Gets the co id.
	 *
	 * @return the co id
	 */
	public String getCoId() {
		return coId;
	}

	/**
	 * Gets the cu ans.
	 *
	 * @return the cu ans
	 */
	public Integer getCuAns() {
		if (cuAns == null) {
			cuAns = 0;
		}
		return cuAns;
	}

	/**
	 * Gets the cu id.
	 *
	 * @return the cu id
	 */
	public String getCuId() {
		return cuId;
	}

	/**
	 * Gets the cu life.
	 *
	 * @return the cu life
	 */
	public Integer getCuLife() {
		if (cuLife == null) {
			cuLife = 0;
		}
		return cuLife;
	}

	/**
	 * Gets the checks if is lq crt.
	 *
	 * @return the checks if is lq crt
	 */
	public Boolean getIsLqCrt() {
		if (isLqCrt == null) {
			isLqCrt = false;
		}
		return isLqCrt;
	}

	/**
	 * Gets the checks if is lq life.
	 *
	 * @return the checks if is lq life
	 */
	public Boolean getIsLqLife() {
		if (isLqLife == null) {
			isLqLife = false;
		}
		return isLqLife;
	}

	/**
	 * Gets the lq no.
	 *
	 * @return the lq no
	 */
	public Integer getLqNo() {
		return lqNo;
	}

	/**
	 * Gets the sfl rwds.
	 *
	 * @return the sfl rwds
	 */
	public Double getSflRwds() {
		if (sflRwds == null) {
			sflRwds = 0.0;
		}
		return sflRwds;
	}

	/**
	 * Sets the ans rwds.
	 *
	 * @param ansRwds the new ans rwds
	 */
	public void setAnsRwds(Double ansRwds) {
		this.ansRwds = ansRwds;
	}

	/**
	 * Sets the ans rwd type.
	 *
	 * @param ansRwdType the new ans rwd type
	 */
	public void setAnsRwdType(String ansRwdType) {
		this.ansRwdType = ansRwdType;
	}

	/**
	 * Sets the cl id.
	 *
	 * @param clId the new cl id
	 */
	public void setClId(String clId) {
		this.clId = clId;
	}

	/**
	 * Sets the co id.
	 *
	 * @param coId the new co id
	 */
	public void setCoId(String coId) {
		this.coId = coId;
	}

	/**
	 * Sets the cu ans.
	 *
	 * @param cuAns the new cu ans
	 */
	public void setCuAns(Integer cuAns) {
		this.cuAns = cuAns;
	}

	/**
	 * Sets the cu id.
	 *
	 * @param cuId the new cu id
	 */
	public void setCuId(String cuId) {
		this.cuId = cuId;
	}

	/**
	 * Sets the cu life.
	 *
	 * @param cuLife the new cu life
	 */
	public void setCuLife(Integer cuLife) {
		this.cuLife = cuLife;
	}

	/**
	 * Sets the checks if is lq crt.
	 *
	 * @param isLqCrt the new checks if is lq crt
	 */
	public void setIsLqCrt(Boolean isLqCrt) {
		this.isLqCrt = isLqCrt;
	}

	/**
	 * Sets the checks if is lq life.
	 *
	 * @param isLqLife the new checks if is lq life
	 */
	public void setIsLqLife(Boolean isLqLife) {
		this.isLqLife = isLqLife;
	}

	/**
	 * Sets the lq no.
	 *
	 * @param lqNo the new lq no
	 */
	public void setLqNo(Integer lqNo) {
		this.lqNo = lqNo;
	}

	/**
	 * Sets the sfl rwds.
	 *
	 * @param sflRwds the new sfl rwds
	 */
	public void setSflRwds(Double sflRwds) {
		this.sflRwds = sflRwds;
	}

}
