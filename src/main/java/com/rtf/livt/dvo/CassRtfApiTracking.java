/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.dvo;

import java.util.Date;
import java.util.UUID;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * The Class CassRtfApiTracking.
 */
@XStreamAlias("Cust")
public class CassRtfApiTracking {

	/** The cl id. */
	private String clId;

	/** The id. */
	private UUID id;

	/** The start date. */
	private Date startDate;

	/** The end date. */
	private Date endDate;

	/** The start date str. */
	private String startDateStr;

	/** The end date str. */
	private String endDateStr;

	/** The cust IP addr. */
	private String custIPAddr;

	/** The api name. */
	private String apiName;

	/** The plat form. */
	private String platForm;

	/** The device type. */
	private String deviceType;

	/** The session id. */
	private String sessionId;

	/** The action result. */
	private String actionResult;

	/** The cust id. */
	private String custId;

	/** The contest id. */
	private String contestId;

	/** The description. */
	private String description;

	/** The device info. */
	private String deviceInfo;

	/** The app verion. */
	private String appVerion;

	/** The fb callback time. */
	private String fbCallbackTime;

	/** The device api start time. */
	private String deviceApiStartTime;

	/** The quest sl no. */
	private String questSlNo;

	/** The res status. */
	private Integer resStatus;

	/** The retry count. */
	private Integer retryCount;

	/** The ans count. */
	private Integer ansCount;

	/** The node id. */
	private Integer nodeId;

	/**
	 * Instantiates a new cass rtf api tracking.
	 *
	 * @param clientId     the client id
	 * @param id           the id
	 * @param startDate    the start date
	 * @param endDate      the end date
	 * @param custIPAddr   the cust IP addr
	 * @param apiName      the api name
	 * @param platForm     the plat form
	 * @param deviceType   the device type
	 * @param sessionId    the session id
	 * @param actionResult the action result
	 * @param custId       the cust id
	 * @param contestId    the contest id
	 * @param description  the description
	 * @param deviceInfo   the device info
	 * @param appVerion    the app verion
	 * @param resStatus    the res status
	 * @param questionSlNo the question sl no
	 * @param nodeId       the node id
	 */
	public CassRtfApiTracking(String clientId, UUID id, Date startDate, Date endDate, String custIPAddr, String apiName,
			String platForm, String deviceType, String sessionId, String actionResult, String custId, String contestId,
			String description, String deviceInfo, String appVerion, Integer resStatus, String questionSlNo,
			Integer nodeId) {
		super();
		this.clId = clientId;
		this.id = id;
		this.startDate = startDate;
		this.endDate = endDate;
		this.custIPAddr = custIPAddr;
		this.apiName = apiName;
		this.platForm = platForm;
		this.deviceType = deviceType;
		this.sessionId = sessionId;
		this.actionResult = actionResult;
		this.custId = custId;
		this.contestId = contestId;
		this.description = description;
		this.deviceInfo = deviceInfo;
		this.appVerion = appVerion;
		this.resStatus = resStatus;
		this.questSlNo = questionSlNo;
		this.nodeId = nodeId;
	}

	/**
	 * Instantiates a new cass rtf api tracking.
	 *
	 * @param clientId           the client id
	 * @param id                 the id
	 * @param startDate          the start date
	 * @param endDate            the end date
	 * @param custIPAddr         the cust IP addr
	 * @param apiName            the api name
	 * @param platForm           the plat form
	 * @param deviceType         the device type
	 * @param sessionId          the session id
	 * @param actionResult       the action result
	 * @param custId             the cust id
	 * @param contestId          the contest id
	 * @param description        the description
	 * @param deviceInfo         the device info
	 * @param appVerion          the app verion
	 * @param deviceApiStartTime the device api start time
	 * @param fbCallbackTime     the fb callback time
	 * @param resStatus          the res status
	 * @param questionSlNo       the question sl no
	 * @param nodeId             the node id
	 */
	public CassRtfApiTracking(String clientId, UUID id, Date startDate, Date endDate, String custIPAddr, String apiName,
			String platForm, String deviceType, String sessionId, String actionResult, String custId, String contestId,
			String description, String deviceInfo, String appVerion, String deviceApiStartTime, String fbCallbackTime,
			Integer resStatus, String questionSlNo, Integer nodeId) {
		super();
		this.clId = clientId;
		this.id = id;
		this.startDate = startDate;
		this.endDate = endDate;
		this.custIPAddr = custIPAddr;
		this.apiName = apiName;
		this.platForm = platForm;
		this.deviceType = deviceType;
		this.sessionId = sessionId;
		this.actionResult = actionResult;
		this.custId = custId;
		this.contestId = contestId;
		this.description = description;
		this.deviceInfo = deviceInfo;
		this.appVerion = appVerion;
		this.deviceApiStartTime = deviceApiStartTime;
		this.fbCallbackTime = fbCallbackTime;
		this.resStatus = resStatus;
		this.questSlNo = questionSlNo;
		this.nodeId = nodeId;

	}

	/**
	 * Instantiates a new cass rtf api tracking.
	 *
	 * @param clientId           the client id
	 * @param id                 the id
	 * @param startDate          the start date
	 * @param endDate            the end date
	 * @param custIPAddr         the cust IP addr
	 * @param apiName            the api name
	 * @param platForm           the plat form
	 * @param deviceType         the device type
	 * @param sessionId          the session id
	 * @param actionResult       the action result
	 * @param custId             the cust id
	 * @param contestId          the contest id
	 * @param description        the description
	 * @param deviceInfo         the device info
	 * @param appVerion          the app verion
	 * @param deviceApiStartTime the device api start time
	 * @param fbCallbackTime     the fb callback time
	 * @param resStatus          the res status
	 * @param questionSlNo       the question sl no
	 * @param retryCount         the retry count
	 * @param ansCount           the ans count
	 * @param nodeId             the node id
	 */
	public CassRtfApiTracking(String clientId, UUID id, Date startDate, Date endDate, String custIPAddr, String apiName,
			String platForm, String deviceType, String sessionId, String actionResult, String custId, String contestId,
			String description, String deviceInfo, String appVerion, String deviceApiStartTime, String fbCallbackTime,
			Integer resStatus, String questionSlNo, Integer retryCount, Integer ansCount, Integer nodeId) {
		super();
		this.clId = clientId;
		this.id = id;
		this.startDate = startDate;
		this.endDate = endDate;
		this.custIPAddr = custIPAddr;
		this.apiName = apiName;
		this.platForm = platForm;
		this.deviceType = deviceType;
		this.sessionId = sessionId;
		this.actionResult = actionResult;
		this.custId = custId;
		this.contestId = contestId;
		this.description = description;
		this.deviceInfo = deviceInfo;
		this.appVerion = appVerion;
		this.deviceApiStartTime = deviceApiStartTime;
		this.fbCallbackTime = fbCallbackTime;
		this.resStatus = resStatus;
		this.questSlNo = questionSlNo;
		this.retryCount = retryCount;
		this.ansCount = ansCount;
		this.nodeId = nodeId;

	}

	/**
	 * Instantiates a new cass rtf api tracking.
	 *
	 * @param clientId           the client id
	 * @param id                 the id
	 * @param startDateStr       the start date str
	 * @param endDateStr         the end date str
	 * @param custIPAddr         the cust IP addr
	 * @param apiName            the api name
	 * @param platForm           the plat form
	 * @param deviceType         the device type
	 * @param sessionId          the session id
	 * @param actionResult       the action result
	 * @param custId             the cust id
	 * @param contestId          the contest id
	 * @param description        the description
	 * @param deviceInfo         the device info
	 * @param appVerion          the app verion
	 * @param deviceApiStartTime the device api start time
	 * @param fbCallbackTime     the fb callback time
	 * @param resStatus          the res status
	 * @param questionSlNo       the question sl no
	 * @param retryCount         the retry count
	 * @param ansCount           the ans count
	 * @param nodeId             the node id
	 */
	public CassRtfApiTracking(String clientId, UUID id, String startDateStr, String endDateStr, String custIPAddr,
			String apiName, String platForm, String deviceType, String sessionId, String actionResult, String custId,
			String contestId, String description, String deviceInfo, String appVerion, String deviceApiStartTime,
			String fbCallbackTime, Integer resStatus, String questionSlNo, Integer retryCount, Integer ansCount,
			Integer nodeId) {
		super();
		this.clId = clientId;
		this.id = id;
		this.startDateStr = startDateStr;
		this.endDateStr = endDateStr;
		this.custIPAddr = custIPAddr;
		this.apiName = apiName;
		this.platForm = platForm;
		this.deviceType = deviceType;
		this.sessionId = sessionId;
		this.actionResult = actionResult;
		this.custId = custId;
		this.contestId = contestId;
		this.description = description;
		this.deviceInfo = deviceInfo;
		this.appVerion = appVerion;
		this.deviceApiStartTime = deviceApiStartTime;
		this.fbCallbackTime = fbCallbackTime;
		this.resStatus = resStatus;
		this.questSlNo = questionSlNo;
		this.retryCount = retryCount;
		this.ansCount = ansCount;
		this.nodeId = nodeId;

	}

	/**
	 * Gets the action result.
	 *
	 * @return the action result
	 */
	public String getActionResult() {
		if (null == actionResult) {
			actionResult = "";
		}
		return actionResult;
	}

	/**
	 * Gets the ans count.
	 *
	 * @return the ans count
	 */
	public Integer getAnsCount() {
		if (ansCount == null) {
			ansCount = 0;
		}
		return ansCount;
	}

	/**
	 * Gets the api name.
	 *
	 * @return the api name
	 */
	public String getApiName() {
		return apiName;
	}

	/**
	 * Gets the app verion.
	 *
	 * @return the app verion
	 */
	public String getAppVerion() {
		if (null == appVerion) {
			appVerion = "";
		}
		return appVerion;
	}

	/**
	 * Gets the cl id.
	 *
	 * @return the cl id
	 */
	public String getClId() {
		if (clId == null) {
			clId = "-1";
		}
		return clId;
	}

	/**
	 * Gets the contest id.
	 *
	 * @return the contest id
	 */
	public String getContestId() {
		if (null == contestId) {
			contestId = "";
		}
		return contestId;
	}

	/**
	 * Gets the cust id.
	 *
	 * @return the cust id
	 */
	public String getCustId() {
		if (custId == null) {
			custId = "-1";
		}
		return custId;
	}

	/**
	 * Gets the cust IP addr.
	 *
	 * @return the cust IP addr
	 */
	public String getCustIPAddr() {
		if (null == custIPAddr) {
			custIPAddr = "";
		}
		return custIPAddr;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		if (null == description) {
			description = "";
		}
		return description;
	}

	/**
	 * Gets the device api start time.
	 *
	 * @return the device api start time
	 */
	public String getDeviceApiStartTime() {
		if (null == deviceApiStartTime) {
			deviceApiStartTime = "";
		}
		return deviceApiStartTime;
	}

	/**
	 * Gets the device info.
	 *
	 * @return the device info
	 */
	public String getDeviceInfo() {
		if (null == deviceInfo) {
			deviceInfo = "";
		}
		return deviceInfo;
	}

	/**
	 * Gets the device type.
	 *
	 * @return the device type
	 */
	public String getDeviceType() {
		if (null == deviceType) {
			deviceType = "";
		}
		return deviceType;
	}

	/**
	 * Gets the end date.
	 *
	 * @return the end date
	 */
	public Date getEndDate() {
		return endDate;
	}

	/**
	 * Gets the end date str.
	 *
	 * @return the end date str
	 */
	public String getEndDateStr() {
		return endDateStr;
	}

	/**
	 * Gets the fb callback time.
	 *
	 * @return the fb callback time
	 */
	public String getFbCallbackTime() {
		if (null == fbCallbackTime) {
			fbCallbackTime = "";
		}
		return fbCallbackTime;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public UUID getId() {
		return id;
	}

	/**
	 * Gets the node id.
	 *
	 * @return the node id
	 */
	public Integer getNodeId() {
		if (nodeId == null) {
			nodeId = 0;
		}
		return nodeId;
	}

	/**
	 * Gets the plat form.
	 *
	 * @return the plat form
	 */
	public String getPlatForm() {
		if (null == platForm) {
			platForm = "";
		}
		return platForm;
	}

	/**
	 * Gets the quest sl no.
	 *
	 * @return the quest sl no
	 */
	public String getQuestSlNo() {
		if (questSlNo == null) {
			questSlNo = "0";
		}
		return questSlNo;
	}

	/**
	 * Gets the res status.
	 *
	 * @return the res status
	 */
	public Integer getResStatus() {
		if (resStatus == null) {
			resStatus = -1;
		}
		return resStatus;
	}

	/**
	 * Gets the retry count.
	 *
	 * @return the retry count
	 */
	public Integer getRetryCount() {
		if (retryCount == null) {
			retryCount = 0;
		}
		return retryCount;
	}

	/**
	 * Gets the session id.
	 *
	 * @return the session id
	 */
	public String getSessionId() {
		if (null == sessionId) {
			sessionId = "";
		}
		return sessionId;
	}

	/**
	 * Gets the start date.
	 *
	 * @return the start date
	 */
	public Date getStartDate() {
		return startDate;
	}

	/**
	 * Gets the start date str.
	 *
	 * @return the start date str
	 */
	public String getStartDateStr() {
		return startDateStr;
	}

	/**
	 * Sets the action result.
	 *
	 * @param actionResult the new action result
	 */
	public void setActionResult(String actionResult) {
		this.actionResult = actionResult;
	}

	/**
	 * Sets the ans count.
	 *
	 * @param ansCount the new ans count
	 */
	public void setAnsCount(Integer ansCount) {
		this.ansCount = ansCount;
	}

	/**
	 * Sets the api name.
	 *
	 * @param apiName the new api name
	 */
	public void setApiName(String apiName) {
		this.apiName = apiName;
	}

	/**
	 * Sets the app verion.
	 *
	 * @param appVerion the new app verion
	 */
	public void setAppVerion(String appVerion) {
		this.appVerion = appVerion;
	}

	/**
	 * Sets the cl id.
	 *
	 * @param clId the new cl id
	 */
	public void setClId(String clId) {
		this.clId = clId;
	}

	/**
	 * Sets the contest id.
	 *
	 * @param contestId the new contest id
	 */
	public void setContestId(String contestId) {
		this.contestId = contestId;
	}

	/**
	 * Sets the cust id.
	 *
	 * @param custId the new cust id
	 */
	public void setCustId(String custId) {
		this.custId = custId;
	}

	/**
	 * Sets the cust IP addr.
	 *
	 * @param custIPAddr the new cust IP addr
	 */
	public void setCustIPAddr(String custIPAddr) {
		this.custIPAddr = custIPAddr;
	}

	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Sets the device api start time.
	 *
	 * @param deviceApiStartTime the new device api start time
	 */
	public void setDeviceApiStartTime(String deviceApiStartTime) {
		this.deviceApiStartTime = deviceApiStartTime;
	}

	/**
	 * Sets the device info.
	 *
	 * @param deviceInfo the new device info
	 */
	public void setDeviceInfo(String deviceInfo) {
		this.deviceInfo = deviceInfo;
	}

	/**
	 * Sets the device type.
	 *
	 * @param deviceType the new device type
	 */
	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	/**
	 * Sets the end date.
	 *
	 * @param endDate the new end date
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	/**
	 * Sets the end date str.
	 *
	 * @param endDateStr the new end date str
	 */
	public void setEndDateStr(String endDateStr) {
		this.endDateStr = endDateStr;
	}

	/**
	 * Sets the fb callback time.
	 *
	 * @param fbCallbackTime the new fb callback time
	 */
	public void setFbCallbackTime(String fbCallbackTime) {
		this.fbCallbackTime = fbCallbackTime;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(UUID id) {
		this.id = id;
	}

	/**
	 * Sets the node id.
	 *
	 * @param nodeId the new node id
	 */
	public void setNodeId(Integer nodeId) {
		this.nodeId = nodeId;
	}

	/**
	 * Sets the plat form.
	 *
	 * @param platForm the new plat form
	 */
	public void setPlatForm(String platForm) {
		this.platForm = platForm;
	}

	/**
	 * Sets the quest sl no.
	 *
	 * @param questSlNo the new quest sl no
	 */
	public void setQuestSlNo(String questSlNo) {
		this.questSlNo = questSlNo;
	}

	/**
	 * Sets the res status.
	 *
	 * @param resStatus the new res status
	 */
	public void setResStatus(Integer resStatus) {
		this.resStatus = resStatus;
	}

	/**
	 * Sets the retry count.
	 *
	 * @param retryCount the new retry count
	 */
	public void setRetryCount(Integer retryCount) {
		this.retryCount = retryCount;
	}

	/**
	 * Sets the session id.
	 *
	 * @param sessionId the new session id
	 */
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	/**
	 * Sets the start date.
	 *
	 * @param startDate the new start date
	 */
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	/**
	 * Sets the start date str.
	 *
	 * @param startDateStr the new start date str
	 */
	public void setStartDateStr(String startDateStr) {
		this.startDateStr = startDateStr;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "CassRtfApiTracking [clId=" + clId + ", id=" + id + ", startDate=" + startDate + ", endDate=" + endDate
				+ ", startDateStr=" + startDateStr + ", endDateStr=" + endDateStr + ", custIPAddr=" + custIPAddr
				+ ", apiName=" + apiName + ", platForm=" + platForm + ", deviceType=" + deviceType + ", sessionId="
				+ sessionId + ", actionResult=" + actionResult + ", custId=" + custId + ", contestId=" + contestId
				+ ", description=" + description + ", deviceInfo=" + deviceInfo + ", appVerion=" + appVerion
				+ ", fbCallbackTime=" + fbCallbackTime + ", deviceApiStartTime=" + deviceApiStartTime + ", questSlNo="
				+ questSlNo + ", resStatus=" + resStatus + ", retryCount=" + retryCount + ", ansCount=" + ansCount
				+ ", nodeId=" + nodeId + "]";
	}

}
