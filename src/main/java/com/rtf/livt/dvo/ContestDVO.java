/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.dvo;

import java.io.Serializable;

import com.rtf.saas.util.DateFormatUtil;

/**
 * The Class ContestDVO.
 */
public class ContestDVO implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 2334492184390909172L;

	/** The co id. */
	private String coId;

	/** The cl id. */
	private String clId;

	/** The img U. */
	private String imgU;

	/** The seq no. */
	private Integer seqNo;

	/** The cat. */
	private String cat;

	/** The sub cat. */
	private String subCat;

	/** The cl img U. */
	private String clImgU;

	/** The st date. */
	private Long stDate;

	/** The name. */
	private String name;

	/** The ext name. */
	private String extName;

	/** The cr by. */
	private String crBy;

	/** The cr date. */
	private Long crDate;

	/** The is act. */
	private Integer isAct;

	/** The q size. */
	private Integer qSize;

	/** The up date. */
	private Long upDate;

	/** The up by. */
	private String upBy;

	/** The ans type. */
	private String ansType;

	/** The co type. */
	private String coType;

	/** The is elimination. */
	private Boolean isElimination;

	/** The is split summary. */
	private Boolean isSplitSummary;

	/** The sum rwd type. */
	private String sumRwdType;

	/** The sum rwd val. */
	private Double sumRwdVal;

	/** The is lot enbl. */
	private Boolean isLotEnbl;

	/** The win rwd type. */
	private String winRwdType;

	/** The win rwd val. */
	private Double winRwdVal;

	/** The que rwd type. */
	private String queRwdType;

	/** The parti rwd type. */
	private String partiRwdType;

	/** The parti rwd val. */
	private Double partiRwdVal;

	/** The winner count. */
	private Integer winnerCount;

	/** The thm img mob. */
	private String thmImgMob;

	/** The thm img desk. */
	private String thmImgDesk;

	/** The play game img. */
	private String playGameImg;

	/** The thm id. */
	private Integer thmId;

	/** The thm color. */
	private String thmColor;

	/** The last action. */
	private String lastAction;

	/** The last que. */
	private Integer lastQue;

	/** The mig status. */
	private String migStatus;

	/** The is pwd. */
	private Boolean isPwd;

	/** The con pwd. */
	private String conPwd;

	/** The st date time str. */
	private String stDateTimeStr;

	/** The status. */
	private String status;

	/** The st hour. */
	private String stHour;

	/** The st minutes. */
	private String stMinutes;

	/** The cont running no. */
	private Integer contRunningNo;
	
	
	/** The contest host image URL. */
	private String hstImgUrl;
	
	/** The contest hosted by */
	private String hostedBy;

	/**
	 * Gets the ans type.
	 *
	 * @return the ans type
	 */
	public String getAnsType() {
		return ansType;
	}

	/**
	 * Gets the cat.
	 *
	 * @return the cat
	 */
	public String getCat() {
		return cat;
	}

	/**
	 * Gets the cl id.
	 *
	 * @return the cl id
	 */
	public String getClId() {
		return clId;
	}

	/**
	 * Gets the cl img U.
	 *
	 * @return the cl img U
	 */
	public String getClImgU() {
		return clImgU;
	}

	/**
	 * Gets the co id.
	 *
	 * @return the co id
	 */
	public String getCoId() {
		return coId;
	}

	/**
	 * Gets the con pwd.
	 *
	 * @return the con pwd
	 */
	public String getConPwd() {
		return conPwd;
	}

	/**
	 * Gets the cont running no.
	 *
	 * @return the cont running no
	 */
	public Integer getContRunningNo() {
		if (contRunningNo == null) {
			contRunningNo = 0;
		}
		return contRunningNo;
	}

	/**
	 * Gets the co type.
	 *
	 * @return the co type
	 */
	public String getCoType() {
		return coType;
	}

	/**
	 * Gets the cr by.
	 *
	 * @return the cr by
	 */
	public String getCrBy() {
		return crBy;
	}

	/**
	 * Gets the cr date.
	 *
	 * @return the cr date
	 */
	public Long getCrDate() {
		return crDate;
	}

	/**
	 * Gets the ext name.
	 *
	 * @return the ext name
	 */
	public String getExtName() {
		return extName;
	}

	/**
	 * Gets the img U.
	 *
	 * @return the img U
	 */
	public String getImgU() {
		return imgU;
	}

	/**
	 * Gets the checks if is act.
	 *
	 * @return the checks if is act
	 */
	public Integer getIsAct() {
		return isAct;
	}

	/**
	 * Gets the checks if is elimination.
	 *
	 * @return the checks if is elimination
	 */
	public Boolean getIsElimination() {
		return isElimination;
	}

	/**
	 * Gets the checks if is lot enbl.
	 *
	 * @return the checks if is lot enbl
	 */
	public Boolean getIsLotEnbl() {
		return isLotEnbl;
	}

	/**
	 * Gets the checks if is pwd.
	 *
	 * @return the checks if is pwd
	 */
	public Boolean getIsPwd() {
		return isPwd;
	}

	/**
	 * Gets the checks if is split summary.
	 *
	 * @return the checks if is split summary
	 */
	public Boolean getIsSplitSummary() {
		return isSplitSummary;
	}

	/**
	 * Gets the last action.
	 *
	 * @return the last action
	 */
	public String getLastAction() {
		return lastAction;
	}

	/**
	 * Gets the last que.
	 *
	 * @return the last que
	 */
	public Integer getLastQue() {
		return lastQue;
	}

	/**
	 * Gets the mig status.
	 *
	 * @return the mig status
	 */
	public String getMigStatus() {
		return migStatus;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Gets the parti rwd type.
	 *
	 * @return the parti rwd type
	 */
	public String getPartiRwdType() {
		return partiRwdType;
	}

	/**
	 * Gets the parti rwd val.
	 *
	 * @return the parti rwd val
	 */
	public Double getPartiRwdVal() {
		if (partiRwdVal == null) {
			partiRwdVal = 0.0;
		}
		return partiRwdVal;
	}

	/**
	 * Gets the play game img.
	 *
	 * @return the play game img
	 */
	public String getPlayGameImg() {
		return playGameImg;
	}

	/**
	 * Gets the q size.
	 *
	 * @return the q size
	 */
	public Integer getqSize() {
		return qSize;
	}

	/**
	 * Gets the que rwd type.
	 *
	 * @return the que rwd type
	 */
	public String getQueRwdType() {
		return queRwdType;
	}

	/**
	 * Gets the seq no.
	 *
	 * @return the seq no
	 */
	public Integer getSeqNo() {
		return seqNo;
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Gets the st date.
	 *
	 * @return the st date
	 */
	public Long getStDate() {
		return stDate;
	}

	/**
	 * Gets the st date time str.
	 *
	 * @return the st date time str
	 */
	public String getStDateTimeStr() {
		stDateTimeStr = DateFormatUtil.getNextContestStartTimeMsg(stDate);
		return stDateTimeStr;
	}

	/**
	 * Gets the st hour.
	 *
	 * @return the st hour
	 */
	public String getStHour() {
		return stHour;
	}

	/**
	 * Gets the st minutes.
	 *
	 * @return the st minutes
	 */
	public String getStMinutes() {
		return stMinutes;
	}

	/**
	 * Gets the sub cat.
	 *
	 * @return the sub cat
	 */
	public String getSubCat() {
		return subCat;
	}

	/**
	 * Gets the sum rwd type.
	 *
	 * @return the sum rwd type
	 */
	public String getSumRwdType() {
		return sumRwdType;
	}

	/**
	 * Gets the sum rwd val.
	 *
	 * @return the sum rwd val
	 */
	public Double getSumRwdVal() {
		return sumRwdVal;
	}

	/**
	 * Gets the thm color.
	 *
	 * @return the thm color
	 */
	public String getThmColor() {
		return thmColor;
	}

	/**
	 * Gets the thm id.
	 *
	 * @return the thm id
	 */
	public Integer getThmId() {
		return thmId;
	}

	/**
	 * Gets the thm img desk.
	 *
	 * @return the thm img desk
	 */
	public String getThmImgDesk() {
		return thmImgDesk;
	}

	/**
	 * Gets the thm img mob.
	 *
	 * @return the thm img mob
	 */
	public String getThmImgMob() {
		return thmImgMob;
	}

	/**
	 * Gets the up by.
	 *
	 * @return the up by
	 */
	public String getUpBy() {
		return upBy;
	}

	/**
	 * Gets the up date.
	 *
	 * @return the up date
	 */
	public Long getUpDate() {
		return upDate;
	}

	/**
	 * Gets the winner count.
	 *
	 * @return the winner count
	 */
	public Integer getWinnerCount() {
		return winnerCount;
	}

	/**
	 * Gets the win rwd type.
	 *
	 * @return the win rwd type
	 */
	public String getWinRwdType() {
		return winRwdType;
	}

	/**
	 * Gets the win rwd val.
	 *
	 * @return the win rwd val
	 */
	public Double getWinRwdVal() {
		return winRwdVal;
	}

	/**
	 * Sets the ans type.
	 *
	 * @param ansType the new ans type
	 */
	public void setAnsType(String ansType) {
		this.ansType = ansType;
	}

	/**
	 * Sets the cat.
	 *
	 * @param cat the new cat
	 */
	public void setCat(String cat) {
		this.cat = cat;
	}

	/**
	 * Sets the cl id.
	 *
	 * @param clId the new cl id
	 */
	public void setClId(String clId) {
		this.clId = clId;
	}

	/**
	 * Sets the cl img U.
	 *
	 * @param clImgU the new cl img U
	 */
	public void setClImgU(String clImgU) {
		this.clImgU = clImgU;
	}

	/**
	 * Sets the co id.
	 *
	 * @param coId the new co id
	 */
	public void setCoId(String coId) {
		this.coId = coId;
	}

	/**
	 * Sets the con pwd.
	 *
	 * @param conPwd the new con pwd
	 */
	public void setConPwd(String conPwd) {
		this.conPwd = conPwd;
	}

	/**
	 * Sets the cont running no.
	 *
	 * @param contRunningNo the new cont running no
	 */
	public void setContRunningNo(Integer contRunningNo) {
		this.contRunningNo = contRunningNo;
	}

	/**
	 * Sets the co type.
	 *
	 * @param coType the new co type
	 */
	public void setCoType(String coType) {
		this.coType = coType;
	}

	/**
	 * Sets the cr by.
	 *
	 * @param crBy the new cr by
	 */
	public void setCrBy(String crBy) {
		this.crBy = crBy;
	}

	/**
	 * Sets the cr date.
	 *
	 * @param crDate the new cr date
	 */
	public void setCrDate(Long crDate) {
		this.crDate = crDate;
	}

	/**
	 * Sets the ext name.
	 *
	 * @param extName the new ext name
	 */
	public void setExtName(String extName) {
		this.extName = extName;
	}

	/**
	 * Sets the img U.
	 *
	 * @param imgU the new img U
	 */
	public void setImgU(String imgU) {
		this.imgU = imgU;
	}

	/**
	 * Sets the checks if is act.
	 *
	 * @param isAct the new checks if is act
	 */
	public void setIsAct(Integer isAct) {
		this.isAct = isAct;
	}

	/**
	 * Sets the checks if is elimination.
	 *
	 * @param isElimination the new checks if is elimination
	 */
	public void setIsElimination(Boolean isElimination) {
		this.isElimination = isElimination;
	}

	/**
	 * Sets the checks if is lot enbl.
	 *
	 * @param isLotEnbl the new checks if is lot enbl
	 */
	public void setIsLotEnbl(Boolean isLotEnbl) {
		this.isLotEnbl = isLotEnbl;
	}

	/**
	 * Sets the checks if is pwd.
	 *
	 * @param isPwd the new checks if is pwd
	 */
	public void setIsPwd(Boolean isPwd) {
		this.isPwd = isPwd;
	}

	/**
	 * Sets the checks if is split summary.
	 *
	 * @param isSplitSummary the new checks if is split summary
	 */
	public void setIsSplitSummary(Boolean isSplitSummary) {
		this.isSplitSummary = isSplitSummary;
	}

	/**
	 * Sets the last action.
	 *
	 * @param lastAction the new last action
	 */
	public void setLastAction(String lastAction) {
		this.lastAction = lastAction;
	}

	/**
	 * Sets the last que.
	 *
	 * @param lastQue the new last que
	 */
	public void setLastQue(Integer lastQue) {
		this.lastQue = lastQue;
	}

	/**
	 * Sets the mig status.
	 *
	 * @param migStatus the new mig status
	 */
	public void setMigStatus(String migStatus) {
		this.migStatus = migStatus;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Sets the parti rwd type.
	 *
	 * @param partiRwdType the new parti rwd type
	 */
	public void setPartiRwdType(String partiRwdType) {
		this.partiRwdType = partiRwdType;
	}

	/**
	 * Sets the parti rwd val.
	 *
	 * @param partiRwdVal the new parti rwd val
	 */
	public void setPartiRwdVal(Double partiRwdVal) {
		this.partiRwdVal = partiRwdVal;
	}

	/**
	 * Sets the play game img.
	 *
	 * @param playGameImg the new play game img
	 */
	public void setPlayGameImg(String playGameImg) {
		this.playGameImg = playGameImg;
	}

	/**
	 * Sets the q size.
	 *
	 * @param qSize the new q size
	 */
	public void setqSize(Integer qSize) {
		this.qSize = qSize;
	}

	/**
	 * Sets the que rwd type.
	 *
	 * @param queRwdType the new que rwd type
	 */
	public void setQueRwdType(String queRwdType) {
		this.queRwdType = queRwdType;
	}

	/**
	 * Sets the seq no.
	 *
	 * @param seqNo the new seq no
	 */
	public void setSeqNo(Integer seqNo) {
		this.seqNo = seqNo;
	}

	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * Sets the st date.
	 *
	 * @param stDate the new st date
	 */
	public void setStDate(Long stDate) {
		this.stDate = stDate;
	}

	/**
	 * Sets the st date time str.
	 *
	 * @param stDateTimeStr the new st date time str
	 */
	public void setStDateTimeStr(String stDateTimeStr) {
		this.stDateTimeStr = stDateTimeStr;
	}

	/**
	 * Sets the st hour.
	 *
	 * @param stHour the new st hour
	 */
	public void setStHour(String stHour) {
		this.stHour = stHour;
	}

	/**
	 * Sets the st minutes.
	 *
	 * @param stMinutes the new st minutes
	 */
	public void setStMinutes(String stMinutes) {
		this.stMinutes = stMinutes;
	}

	/**
	 * Sets the sub cat.
	 *
	 * @param subCat the new sub cat
	 */
	public void setSubCat(String subCat) {
		this.subCat = subCat;
	}

	/**
	 * Sets the sum rwd type.
	 *
	 * @param sumRwdType the new sum rwd type
	 */
	public void setSumRwdType(String sumRwdType) {
		this.sumRwdType = sumRwdType;
	}

	/**
	 * Sets the sum rwd val.
	 *
	 * @param sumRwdVal the new sum rwd val
	 */
	public void setSumRwdVal(Double sumRwdVal) {
		this.sumRwdVal = sumRwdVal;
	}

	/**
	 * Sets the thm color.
	 *
	 * @param thmColor the new thm color
	 */
	public void setThmColor(String thmColor) {
		this.thmColor = thmColor;
	}

	/**
	 * Sets the thm id.
	 *
	 * @param thmId the new thm id
	 */
	public void setThmId(Integer thmId) {
		this.thmId = thmId;
	}

	/**
	 * Sets the thm img desk.
	 *
	 * @param thmImgDesk the new thm img desk
	 */
	public void setThmImgDesk(String thmImgDesk) {
		this.thmImgDesk = thmImgDesk;
	}

	/**
	 * Sets the thm img mob.
	 *
	 * @param thmImgMob the new thm img mob
	 */
	public void setThmImgMob(String thmImgMob) {
		this.thmImgMob = thmImgMob;
	}

	/**
	 * Sets the up by.
	 *
	 * @param upBy the new up by
	 */
	public void setUpBy(String upBy) {
		this.upBy = upBy;
	}

	/**
	 * Sets the up date.
	 *
	 * @param upDate the new up date
	 */
	public void setUpDate(Long upDate) {
		this.upDate = upDate;
	}

	/**
	 * Sets the up date time str.
	 *
	 * @param upDateTimeStr the new up date time str
	 */
	/*
	 * public String getStDateStr() { stDateStr =
	 * DateFormatUtil.getMMDDYYYYString(stDate); return stDateStr; } public void
	 * setStDateStr(String stDateStr) { this.stDateStr = stDateStr; } public String
	 * getCrDateTimeStr() { crDateTimeStr =
	 * DateFormatUtil.getMMDDYYYYHHMMSSString(crDate); return crDateTimeStr; }
	 * public void setCrDateTimeStr(String crDateTimeStr) { this.crDateTimeStr =
	 * crDateTimeStr; } public String getUpDateTimeStr() { upDateTimeStr =
	 * DateFormatUtil.getMMDDYYYYHHMMSSString(upDate); return upDateTimeStr; }
	 */
	public void setUpDateTimeStr(String upDateTimeStr) {
	}

	/**
	 * Sets the winner count.
	 *
	 * @param winnerCount the new winner count
	 */
	public void setWinnerCount(Integer winnerCount) {
		this.winnerCount = winnerCount;
	}

	/**
	 * Sets the win rwd type.
	 *
	 * @param winRwdType the new win rwd type
	 */
	public void setWinRwdType(String winRwdType) {
		this.winRwdType = winRwdType;
	}

	/**
	 * Sets the win rwd val.
	 *
	 * @param winRwdVal the new win rwd val
	 */
	public void setWinRwdVal(Double winRwdVal) {
		this.winRwdVal = winRwdVal;
	}
	
	
	/**
	 * Gets contest host image URL. 
	 *
	 * @return hstImgUrl
	 */
	public String getHstImgUrl() {
		/*if(hstImgUrl==null || hstImgUrl.isEmpty()){
			hstImgUrl ="http://34.202.149.190:8083/saasimages/uploads/A.png";
		}*/
		return hstImgUrl;
	}

	
	/**
	 * Sets contest host image URL. 
	 *
	 * @param hstImgUrl
	 */
	public void setHstImgUrl(String hstImgUrl) {
		this.hstImgUrl = hstImgUrl;
	}

	/**
	 * Gets contest hosted By.
	 *
	 * @return hostedBy
	 */
	public String getHostedBy() {
		return hostedBy;
	}
	
	

	/**
	 * Sets contest hosted By.
	 *
	 * @param hostedBy
	 */
	public void setHostedBy(String hostedBy) {
		this.hostedBy = hostedBy;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
	
	
	
}
