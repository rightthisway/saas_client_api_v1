/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.dvo;

import java.io.Serializable;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * The Class CustContAnswersCountDVO.
 */
@XStreamAlias("CustContAnswersCount")
public class CustContAnswersCountDVO implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -1402320683731633377L;

	/** The cl id. */
	private String clId;

	/** The co id. */
	private String coId;

	/** The q no. */
	private Integer qNo;

	/** The option. */
	private String option;

	/** The count. */
	private Long count;

	/**
	 * Instantiates a new cust cont answers count DVO.
	 */
	public CustContAnswersCountDVO() {

	}

	/**
	 * Instantiates a new cust cont answers count DVO.
	 *
	 * @param clId   the cl id
	 * @param coId   the co id
	 * @param qNo    the q no
	 * @param option the option
	 * @param count  the count
	 */
	public CustContAnswersCountDVO(String clId, String coId, Integer qNo, String option, Long count) {
		super();
		this.clId = clId;
		this.coId = coId;
		this.qNo = qNo;
		this.option = option;
		this.count = count;
	}

	/**
	 * Instantiates a new cust cont answers count DVO.
	 *
	 * @param clId  the cl id
	 * @param coId  the co id
	 * @param count the count
	 */
	public CustContAnswersCountDVO(String clId, String coId, Long count) {
		super();
		this.clId = clId;
		this.coId = coId;
		this.count = count;
	}

	/**
	 * Gets the cl id.
	 *
	 * @return the cl id
	 */
	public String getClId() {
		return clId;
	}

	/**
	 * Gets the co id.
	 *
	 * @return the co id
	 */
	public String getCoId() {
		return coId;
	}

	/**
	 * Gets the count.
	 *
	 * @return the count
	 */
	public Long getCount() {
		return count;
	}

	/**
	 * Gets the option.
	 *
	 * @return the option
	 */
	public String getOption() {
		return option;
	}

	/**
	 * Gets the q no.
	 *
	 * @return the q no
	 */
	public Integer getqNo() {
		return qNo;
	}

	/**
	 * Sets the cl id.
	 *
	 * @param clId the new cl id
	 */
	public void setClId(String clId) {
		this.clId = clId;
	}

	/**
	 * Sets the co id.
	 *
	 * @param coId the new co id
	 */
	public void setCoId(String coId) {
		this.coId = coId;
	}

	/**
	 * Sets the count.
	 *
	 * @param count the new count
	 */
	public void setCount(Long count) {
		this.count = count;
	}

	/**
	 * Sets the option.
	 *
	 * @param option the new option
	 */
	public void setOption(String option) {
		this.option = option;
	}

	/**
	 * Sets the q no.
	 *
	 * @param qNo the new q no
	 */
	public void setqNo(Integer qNo) {
		this.qNo = qNo;
	}

}
