/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.dvo;

import java.util.Date;

/**
 * The Class LivtContestQuestionDVO.
 */
public class LivtContestQuestionDVO {

	/** The conqsnid. */
	private Integer conqsnid;

	/** The clintid. */
	private String clintid;

	/** The conid. */
	private String conid;

	/** The qsnseqno. */
	private Integer qsnseqno;

	/** The qsntext. */
	private String qsntext;

	/** The opa. */
	private String opa;

	/** The opb. */
	private String opb;

	/** The opc. */
	private String opc;

	/** The opd. */
	private String opd;

	/** The corans. */
	private String corans;

	/** The qsnorient. */
	private String qsnorient;

	/** The qbid. */
	private Integer qbid;

	/** The ans rwd val. */
	private Double ansRwdVal;

	/** The mj rwd type. */
	private String mjRwdType;

	/** The mj rwd val. */
	private Double mjRwdVal;

	/** The mj winners count. */
	private Integer mjWinnersCount;

	/** The q dif level. */
	private String qDifLevel;

	/** The enc key. */
	private String encKey;

	/** The coupcde. */
	private String coupcde;

	/** The coupondisc type. */
	private String coupondiscType;

	/** The creby. */
	private String creby;

	/** The credate. */
	private Date credate;

	/** The updby. */
	private String updby;

	/** The upddate. */
	private Date upddate;

	/**
	 * Gets the ans rwd val.
	 *
	 * @return the ans rwd val
	 */
	public Double getAnsRwdVal() {
		if (ansRwdVal == null) {
			ansRwdVal = 0.0;
		}
		return ansRwdVal;
	}

	/**
	 * Gets the clintid.
	 *
	 * @return the clintid
	 */
	public String getClintid() {
		return clintid;
	}

	/**
	 * Gets the conid.
	 *
	 * @return the conid
	 */
	public String getConid() {
		return conid;
	}

	/**
	 * Gets the conqsnid.
	 *
	 * @return the conqsnid
	 */
	public Integer getConqsnid() {
		return conqsnid;
	}

	/**
	 * Gets the corans.
	 *
	 * @return the corans
	 */
	public String getCorans() {
		return corans;
	}

	/**
	 * Gets the coupcde.
	 *
	 * @return the coupcde
	 */
	public String getCoupcde() {
		return coupcde;
	}

	/**
	 * Gets the coupondisc type.
	 *
	 * @return the coupondisc type
	 */
	public String getCoupondiscType() {
		return coupondiscType;
	}

	/**
	 * Gets the creby.
	 *
	 * @return the creby
	 */
	public String getCreby() {
		return creby;
	}

	/**
	 * Gets the credate.
	 *
	 * @return the credate
	 */
	public Date getCredate() {
		return credate;
	}

	/**
	 * Gets the enc key.
	 *
	 * @return the enc key
	 */
	/*
	 * public String getCredateStr() { credateStr =
	 * DateFormatUtil.getMMDDYYYYHHMMSSString(credate); return credateStr; } public
	 * void setCredateStr(String credateStr) { this.credateStr = credateStr; }
	 * public String getUpddateStr() { upddateStr =
	 * DateFormatUtil.getMMDDYYYYHHMMSSString(upddate); return upddateStr; } public
	 * void setUpddateStr(String upddateStr) { this.upddateStr = upddateStr; }
	 */
	public String getEncKey() {
		return encKey;
	}

	/**
	 * Gets the mj rwd type.
	 *
	 * @return the mj rwd type
	 */
	public String getMjRwdType() {
		return mjRwdType;
	}

	/**
	 * Gets the mj rwd val.
	 *
	 * @return the mj rwd val
	 */
	public Double getMjRwdVal() {
		if (mjRwdVal == null) {
			mjRwdVal = 0.0;
		}
		return mjRwdVal;
	}

	/**
	 * Gets the mj winners count.
	 *
	 * @return the mj winners count
	 */
	public Integer getMjWinnersCount() {
		if (mjWinnersCount == null) {
			mjWinnersCount = 0;
		}
		return mjWinnersCount;
	}

	/**
	 * Gets the opa.
	 *
	 * @return the opa
	 */
	public String getOpa() {
		return opa;
	}

	/**
	 * Gets the opb.
	 *
	 * @return the opb
	 */
	public String getOpb() {
		return opb;
	}

	/**
	 * Gets the opc.
	 *
	 * @return the opc
	 */
	public String getOpc() {
		return opc;
	}

	/**
	 * Gets the opd.
	 *
	 * @return the opd
	 */
	public String getOpd() {
		return opd;
	}

	/**
	 * Gets the qbid.
	 *
	 * @return the qbid
	 */
	public Integer getQbid() {
		return qbid;
	}

	/**
	 * Gets the q dif level.
	 *
	 * @return the q dif level
	 */
	public String getqDifLevel() {
		return qDifLevel;
	}

	/**
	 * Gets the qsnorient.
	 *
	 * @return the qsnorient
	 */
	public String getQsnorient() {
		return qsnorient;
	}

	/**
	 * Gets the qsnseqno.
	 *
	 * @return the qsnseqno
	 */
	public Integer getQsnseqno() {
		return qsnseqno;
	}

	/**
	 * Gets the qsntext.
	 *
	 * @return the qsntext
	 */
	public String getQsntext() {
		return qsntext;
	}

	/**
	 * Gets the updby.
	 *
	 * @return the updby
	 */
	public String getUpdby() {
		return updby;
	}

	/**
	 * Gets the upddate.
	 *
	 * @return the upddate
	 */
	public Date getUpddate() {
		return upddate;
	}

	/**
	 * Sets the ans rwd val.
	 *
	 * @param ansRwdVal the new ans rwd val
	 */
	public void setAnsRwdVal(Double ansRwdVal) {
		this.ansRwdVal = ansRwdVal;
	}

	/**
	 * Sets the clintid.
	 *
	 * @param clintid the new clintid
	 */
	public void setClintid(String clintid) {
		this.clintid = clintid;
	}

	/**
	 * Sets the conid.
	 *
	 * @param conid the new conid
	 */
	public void setConid(String conid) {
		this.conid = conid;
	}

	/**
	 * Sets the conqsnid.
	 *
	 * @param conqsnid the new conqsnid
	 */
	public void setConqsnid(Integer conqsnid) {
		this.conqsnid = conqsnid;
	}

	/**
	 * Sets the corans.
	 *
	 * @param corans the new corans
	 */
	public void setCorans(String corans) {
		this.corans = corans;
	}

	/**
	 * Sets the coupcde.
	 *
	 * @param coupcde the new coupcde
	 */
	public void setCoupcde(String coupcde) {
		this.coupcde = coupcde;
	}

	/**
	 * Sets the coupondisc type.
	 *
	 * @param coupondiscType the new coupondisc type
	 */
	public void setCoupondiscType(String coupondiscType) {
		this.coupondiscType = coupondiscType;
	}

	/**
	 * Sets the creby.
	 *
	 * @param creby the new creby
	 */
	public void setCreby(String creby) {
		this.creby = creby;
	}

	/**
	 * Sets the credate.
	 *
	 * @param credate the new credate
	 */
	public void setCredate(Date credate) {
		this.credate = credate;
	}

	/**
	 * Sets the enc key.
	 *
	 * @param encKey the new enc key
	 */
	public void setEncKey(String encKey) {
		this.encKey = encKey;
	}

	/**
	 * Sets the mj rwd type.
	 *
	 * @param mjRwdType the new mj rwd type
	 */
	public void setMjRwdType(String mjRwdType) {
		this.mjRwdType = mjRwdType;
	}

	/**
	 * Sets the mj rwd val.
	 *
	 * @param mjRwdVal the new mj rwd val
	 */
	public void setMjRwdVal(Double mjRwdVal) {
		this.mjRwdVal = mjRwdVal;
	}

	/**
	 * Sets the mj winners count.
	 *
	 * @param mjWinnersCount the new mj winners count
	 */
	public void setMjWinnersCount(Integer mjWinnersCount) {
		this.mjWinnersCount = mjWinnersCount;
	}

	/**
	 * Sets the opa.
	 *
	 * @param opa the new opa
	 */
	public void setOpa(String opa) {
		this.opa = opa;
	}

	/**
	 * Sets the opb.
	 *
	 * @param opb the new opb
	 */
	public void setOpb(String opb) {
		this.opb = opb;
	}

	/**
	 * Sets the opc.
	 *
	 * @param opc the new opc
	 */
	public void setOpc(String opc) {
		this.opc = opc;
	}

	/**
	 * Sets the opd.
	 *
	 * @param opd the new opd
	 */
	public void setOpd(String opd) {
		this.opd = opd;
	}

	/**
	 * Sets the qbid.
	 *
	 * @param qbid the new qbid
	 */
	public void setQbid(Integer qbid) {
		this.qbid = qbid;
	}

	/**
	 * Sets the q dif level.
	 *
	 * @param qDifLevel the new q dif level
	 */
	public void setqDifLevel(String qDifLevel) {
		this.qDifLevel = qDifLevel;
	}

	/**
	 * Sets the qsnorient.
	 *
	 * @param qsnorient the new qsnorient
	 */
	public void setQsnorient(String qsnorient) {
		this.qsnorient = qsnorient;
	}

	/**
	 * Sets the qsnseqno.
	 *
	 * @param qsnseqno the new qsnseqno
	 */
	public void setQsnseqno(Integer qsnseqno) {
		this.qsnseqno = qsnseqno;
	}

	/**
	 * Sets the qsntext.
	 *
	 * @param qsntext the new qsntext
	 */
	public void setQsntext(String qsntext) {
		this.qsntext = qsntext;
	}

	/**
	 * Sets the updby.
	 *
	 * @param updby the new updby
	 */
	public void setUpdby(String updby) {
		this.updby = updby;
	}

	/**
	 * Sets the upddate.
	 *
	 * @param upddate the new upddate
	 */
	public void setUpddate(Date upddate) {
		this.upddate = upddate;
	}

}
