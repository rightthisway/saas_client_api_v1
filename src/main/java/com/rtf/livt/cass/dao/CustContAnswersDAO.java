/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.cass.dao;

import java.util.ArrayList;
import java.util.List;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.SimpleStatement;
import com.datastax.driver.core.Statement;
import com.datastax.driver.core.exceptions.QueryExecutionException;
import com.datastax.driver.core.exceptions.QueryValidationException;
import com.rtf.livt.dvo.CustContAnswersDVO;
import com.rtf.saas.db.CassandraConnector;

/**
 * The Class CustContAnswersDAO.
 */
public class CustContAnswersDAO {

	/**
	 * Delete cust cont answers data by client id.
	 *
	 * @param clientId the client id
	 */
	public static void deleteCustContAnswersDataByClientId(String clientId) {
		CassandraConnector.getSession().executeAsync("DELETE FROM pt_livvx_customer_answer where clintid=?", clientId);
	}

	/**
	 * Gets the customer contest answers by contest id.
	 *
	 * @param clientId  the client id
	 * @param contestId the contest id
	 * @return the customer contest answers by contest id
	 */
	public static List<CustContAnswersDVO> getCustContAnswersByContId(String clientId, String contestId) {
		final ResultSet results = CassandraConnector.getSession().execute(
				"SELECT * from pt_livvx_customer_answer where clintid = ? and  conid = ?", clientId, contestId);
		List<CustContAnswersDVO> custContAnsList = new ArrayList<CustContAnswersDVO>();
		if (results != null) {
			for (Row row : results) {
				custContAnsList.add(new CustContAnswersDVO(row.getString("clintid"), row.getString("custid"),
						row.getString("conid"), row.getInt("conqsnid"), row.getInt("qsnseqno"),
						row.getString("custans"), row.getBool("iscrtans"), row.getBool("islife"),
						row.getDouble("ansrwdval"), row.getTimestamp("credate"), row.getTimestamp("upddate"),
						row.getString("fb_callback_time"), row.getString("anstime"), row.getInt("rt_count"),
						row.getBool("autocreated"), row.getString("ansrwdtype")));
			}
		}
		return custContAnsList;
	}

	/**
	 * Gets the customer contest answers by cont idand question no.
	 *
	 * @param clientId   the client id
	 * @param contestId  the contest id
	 * @param questionNo the question no
	 * @return the cust cont answers by cont idand question no
	 */
	public static List<CustContAnswersDVO> getCustContAnswersByContIdandQuestionNo(String clientId, String contestId,
			Integer questionNo) {
		final ResultSet results = CassandraConnector.getSession().execute(
				"SELECT clintid,custid,conqsnid,ans,iscrt,islife from pt_livvx_customer_answer WHERE clintid=? and conid=? and qsnseqno=?",
				clientId, contestId, questionNo);// conid,qsnseqno,//conid=? and
		List<CustContAnswersDVO> custContAnsList = new ArrayList<CustContAnswersDVO>();

		if (results != null) {
			for (Row row : results) {
				custContAnsList.add(new CustContAnswersDVO(row.getString("clintid"), row.getString("custid"), contestId,
						row.getInt("conqsnid"), questionNo, row.getString("ans"), row.getBool("iscrt"),
						row.getBool("islife")));
			}
		}
		return custContAnsList;
	}

	/**
	 * Save customer contest Answer Records
	 *
	 * @param obj the obj
	 */
	public static void save(CustContAnswersDVO obj) {
		Statement statement = new SimpleStatement(
				"INSERT INTO pt_livvx_customer_answer(clintid,conid,custid,conqsnid,qsnseqno,custans,iscrtans,islife,"
						+ "ansrwdval,fb_callback_time,anstime,rt_count,autocreated,ansrwdtype,credate) "// ,upddate,curwds,culife
						+ "VALUES (?,?,?, ?,?, ?,?, ?,?, ?,?, ?,?,?,toTimestamp(now()))",
				obj.getClId(), obj.getCoId(), obj.getCuId(), obj.getqId(), obj.getqNo(), obj.getAns(), obj.getIsCrt(),
				obj.getIsLife(), obj.getaRwds(), obj.getFbCallbackTime(), obj.getAnswerTime(), obj.getRetryCount(),
				obj.getIsAutoCreated(), obj.getaRwdType());// ,obj.getUpDate(),obj.getCuRwds(),obj.getCuLife()

		try {
			CassandraConnector.getSession().executeAsync(statement);
		} catch (QueryExecutionException qex) {
			qex.printStackTrace();

		} catch (QueryValidationException qvx) {
			qvx.printStackTrace();
		}
	}

	/**
	 * Save customer contest Answers with  apply life.
	 *
	 * @param obj the obj
	 * @throws Exception the exception
	 */
	public static void saveForApplyLife(CustContAnswersDVO obj) throws Exception {
		Statement statement = new SimpleStatement(
				"INSERT INTO pt_livvx_customer_answer(clintid,custid,conid,conqsnid,qsnseqno,iscrt,islife,"
						+ "credate,upddate,arwds,curwds,culife,autocreated) VALUES (?,?,?, ?,?, ?,?, ?,?, ?,?, ?,?)",
				obj.getClId(), obj.getCuId(), obj.getCoId(), obj.getqId(), obj.getqNo(), obj.getIsCrt(),
				obj.getIsLife(), obj.getCrDate(), obj.getUpDate(), obj.getaRwds(), obj.getCuRwds(), obj.getCuLife(),
				obj.getIsAutoCreated());

		CassandraConnector.getSession().executeAsync(statement);
	}

}