/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.cass.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.exceptions.DriverException;
import com.rtf.livt.dvo.ContestDVO;
import com.rtf.saas.db.CassandraConnector;
import com.rtf.saas.exception.RTFDataAccessException;
import com.rtf.saas.util.GenUtil;

/**
 * The Class LiveContestCassDAO.
 */
public class LiveContestCassDAO {

	/**
	 * Delete contest Master Record 
	 *
	 * @param clientId the client id
	 * @param coId     the contest id
	 * @return true, if successful
	 * @throws Exception the exception
	 */
	public static boolean deleteContestCass(String clientId, String coId) throws Exception {
		boolean isUpdated = false;
		StringBuffer sql = new StringBuffer();
		sql.append("delete from  pa_livvx_contest_mstr  ");
		sql.append(" WHERE clintid=? AND conid=?");
		try {
			CassandraConnector.getSession().execute(sql.toString(), new Object[] { clientId, coId });
			isUpdated = true;
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		} finally {
		}
		return isUpdated;
	}

	/**
	 * Gets the contest Master Record  by contest id.
	 *
	 * @param clientId  the client id
	 * @param contestId the contest id
	 * @return the contest by contest id
	 * @throws Exception the exception
	 */
	public static ContestDVO getContestByContestId(String clientId, String contestId) throws Exception {
		ResultSet resultSet = null;
		ContestDVO contestDVO = null;
		try {
			resultSet = CassandraConnector.getSession().execute(
					"SELECT * from  pa_livvx_contest_mstr  WHERE clintid = ? AND conid=? ", clientId, contestId);
			if (resultSet != null) {
				for (Row rs : resultSet) {
					contestDVO = new ContestDVO();
					Date date = null;
					contestDVO.setCoId(rs.getString("conid"));
					contestDVO.setClId(rs.getString("clintid"));
					contestDVO.setAnsType(rs.getString("anstype"));
					contestDVO.setCoType(rs.getString("contype"));
					contestDVO.setClImgU(rs.getString("brndimgurl"));
					contestDVO.setImgU(rs.getString("cardimgurl"));
					contestDVO.setSeqNo(rs.getInt("cardseqno"));
					contestDVO.setCat(rs.getString("cattype"));
					contestDVO.setName(rs.getString("conname"));
					date = rs.getTimestamp("consrtdate");
					contestDVO.setStDate(date != null ? date.getTime() : null);
					contestDVO.setIsElimination(rs.getBool("iselimtype"));
					contestDVO.setqSize(rs.getInt("noofqns"));
					contestDVO.setSubCat(rs.getString("subcattype"));
					contestDVO.setThmColor(rs.getString("bgthemcolor"));
					contestDVO.setThmImgDesk(rs.getString("bgthemimgurl"));
					contestDVO.setThmImgMob(rs.getString("bgthemimgurlmob"));
					contestDVO.setPlayGameImg(rs.getString("playimgurl"));
					contestDVO.setThmId(rs.getInt("bgthembankid"));
					contestDVO.setIsSplitSummary(rs.getBool("issumrysplitable"));
					contestDVO.setIsLotEnbl(rs.getBool("islotryenabled"));
					contestDVO.setSumRwdType(rs.getString("sumryrwdtype"));
					contestDVO.setSumRwdVal(rs.getDouble("sumryrwdval"));
					contestDVO.setWinRwdType(rs.getString("lotryrwdtype"));
					contestDVO.setWinRwdVal(rs.getDouble("lotryrwdval"));
					contestDVO.setQueRwdType(rs.getString("qsnrwdtype"));
					contestDVO.setPartiRwdType(rs.getString("participantrwdtype"));
					contestDVO.setPartiRwdVal(rs.getDouble("participantrwdval"));
					contestDVO.setLastAction(rs.getString("lastaction"));
					contestDVO.setLastQue(rs.getInt("lastqsn"));
					contestDVO.setMigStatus(rs.getString("migrstatus"));
					contestDVO.setExtName(rs.getString("extconname"));
					contestDVO.setContRunningNo(rs.getInt("conrunningno"));
					contestDVO.setIsPwd(rs.getBool("isPwd"));
					contestDVO.setConPwd(rs.getString("conpwd"));

				}
			}
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		} finally {
		}
		return contestDVO;
	}

	
	
	/**
	 * Gets the contest Master Record  by contest id.
	 *
	 * @param clientId  the client id
	 * @param contestId the contest id
	 * @return the contest by contest id
	 * @throws Exception the exception
	 */
	public static ContestDVO getLiveContestByClientId(String clientId) throws Exception {
		ResultSet resultSet = null;
		ContestDVO contestDVO = null;
		try {
			resultSet = CassandraConnector.getSession().execute(
					"SELECT * from  pa_livvx_contest_mstr  WHERE clintid = ? ", clientId);
			if (resultSet != null) {
				for (Row rs : resultSet) {
					Integer status = rs.getInt("isconactive");
					if(status == null || status != 2){
						continue;
					}
					contestDVO = new ContestDVO();
					Date date = null;
					contestDVO.setCoId(rs.getString("conid"));
					contestDVO.setClId(rs.getString("clintid"));
					contestDVO.setAnsType(rs.getString("anstype"));
					contestDVO.setCoType(rs.getString("contype"));
					contestDVO.setClImgU(rs.getString("brndimgurl"));
					contestDVO.setImgU(rs.getString("cardimgurl"));
					contestDVO.setSeqNo(rs.getInt("cardseqno"));
					contestDVO.setCat(rs.getString("cattype"));
					contestDVO.setName(rs.getString("conname"));
					date = rs.getTimestamp("consrtdate");
					contestDVO.setStDate(date != null ? date.getTime() : null);
					contestDVO.setIsElimination(rs.getBool("iselimtype"));
					contestDVO.setqSize(rs.getInt("noofqns"));
					contestDVO.setSubCat(rs.getString("subcattype"));
					contestDVO.setThmColor(rs.getString("bgthemcolor"));
					contestDVO.setThmImgDesk(rs.getString("bgthemimgurl"));
					contestDVO.setThmImgMob(rs.getString("bgthemimgurlmob"));
					contestDVO.setPlayGameImg(rs.getString("playimgurl"));
					contestDVO.setThmId(rs.getInt("bgthembankid"));
					contestDVO.setIsSplitSummary(rs.getBool("issumrysplitable"));
					contestDVO.setIsLotEnbl(rs.getBool("islotryenabled"));
					contestDVO.setSumRwdType(rs.getString("sumryrwdtype"));
					contestDVO.setSumRwdVal(rs.getDouble("sumryrwdval"));
					contestDVO.setWinRwdType(rs.getString("lotryrwdtype"));
					contestDVO.setWinRwdVal(rs.getDouble("lotryrwdval"));
					contestDVO.setQueRwdType(rs.getString("qsnrwdtype"));
					contestDVO.setPartiRwdType(rs.getString("participantrwdtype"));
					contestDVO.setPartiRwdVal(rs.getDouble("participantrwdval"));
					contestDVO.setLastAction(rs.getString("lastaction"));
					contestDVO.setLastQue(rs.getInt("lastqsn"));
					contestDVO.setMigStatus(rs.getString("migrstatus"));
					contestDVO.setExtName(rs.getString("extconname"));
					contestDVO.setContRunningNo(rs.getInt("conrunningno"));
					contestDVO.setIsPwd(rs.getBool("isPwd"));
					contestDVO.setConPwd(rs.getString("conpwd"));

				}
			}
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		} finally {
		}
		return contestDVO;
	}
	
	
	
	/**
	 * Save contest Master Record.
	 *
	 * @param co the contest Object
	 * @return true, if successful
	 * @throws Exception the exception
	 */
	public static boolean saveContestCass(ContestDVO co) throws Exception {
		boolean isInserted = false;
		StringBuffer sql = new StringBuffer();
		sql.append(
				"INSERT INTO pa_livvx_contest_mstr (clintid,conid,cattype,subcattype,conname,consrtdate,contype,anstype");
		sql.append(",iselimtype,noofqns,issumrysplitable,sumryrwdtype,sumryrwdval,islotryenabled");
		sql.append(",lotryrwdtype,lotryrwdval,brndimgurl,cardimgurl,cardseqno");
		sql.append(",bgthemcolor,bgthemimgurl,bgthemimgurlmob,bgthembankid,playimgurl");
		sql.append(
				",qsnrwdtype,participantrwdtype,participantrwdval,migrstatus,lastqsn,lastaction,extconname,isconactive,conrunningno) ");
		sql.append("values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

		try {
			CassandraConnector.getSession().execute(sql.toString(),
					new Object[] { co.getClId(), co.getCoId(), co.getCat(), co.getSubCat(), co.getName(),
							new Date(co.getStDate()), co.getCoType(), co.getAnsType(), co.getIsElimination(),
							co.getqSize(), co.getIsSplitSummary(), co.getSumRwdType(), co.getSumRwdVal(),
							co.getIsLotEnbl(), co.getWinRwdType(), co.getWinRwdVal(), co.getClImgU(), co.getImgU(),
							co.getSeqNo(), co.getThmColor(), co.getThmImgDesk(), co.getThmImgMob(), co.getThmId(),
							co.getPlayGameImg(), co.getQueRwdType(), co.getPartiRwdType(), co.getPartiRwdVal(),
							co.getMigStatus(), co.getLastQue(), co.getLastAction(), co.getExtName(), co.getIsAct(),
							co.getContRunningNo() });
			isInserted = true;
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		} finally {
		}
		return isInserted;
	}

	/**
	 * Update contest Master Record 
	 *
	 * @param co contest Object
	 * @return true, if successful
	 * @throws Exception the exception
	 */
	public static boolean updateContestCass(ContestDVO co) throws Exception {
		boolean isUpdated = false;
		StringBuffer sql = new StringBuffer();
		sql.append("UPDATE pa_livvx_contest_mstr SET ");
		sql.append("cattype=?,subcattype=? ,conname=? ,consrtdate=?");
		sql.append(",contype=? ,anstype=? ,iselimtype=?");
		sql.append(",noofqns=? ,issumrysplitable=? ,sumryrwdtype=?");
		sql.append(",sumryrwdval=? ,islotryenabled=? ,lotryrwdtype=?");
		sql.append(",lotryrwdval=?  ,brndimgurl=?");
		sql.append(",cardimgurl=? ,cardseqno=? ,bgthemcolor=?");
		sql.append(",bgthemimgurl=? ,bgthemimgurlmob=? ,bgthembankid=?");
		sql.append(",playimgurl=? ,qsnrwdtype=?");
		sql.append(",participantrwdtype=? ,participantrwdval=?, extconname=?, isconactive=? ");
		sql.append("WHERE clintid=? AND conid=?");
		try {
			CassandraConnector.getSession().execute(sql.toString(),
					new Object[] { co.getCat(), co.getSubCat(), co.getName(), new Date(co.getStDate()), co.getCoType(),
							co.getAnsType(), co.getIsElimination(), co.getqSize(), co.getIsSplitSummary(),
							co.getSumRwdType(), co.getSumRwdVal(), co.getIsLotEnbl(), co.getWinRwdType(),
							co.getWinRwdVal(), co.getClImgU(), co.getImgU(), co.getSeqNo(), co.getThmColor(),
							co.getThmImgDesk(), co.getThmImgMob(), co.getThmId(), co.getPlayGameImg(),
							co.getQueRwdType(), co.getPartiRwdType(), co.getPartiRwdVal(), co.getExtName(),
							co.getIsAct(), co.getClId(), co.getCoId() });
			isUpdated = true;
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		} finally {
		}
		return isUpdated;
	}

	/**
	 * Update contest Master Record Status.
	 *
	 * @param co contest Object
	 * @return true, if successful
	 * @throws Exception the exception
	 */
	public static boolean updateContestState(ContestDVO co) throws Exception {
		boolean isUpdated = false;
		StringBuffer sql = new StringBuffer();
		sql.append("update pa_livvx_contest_mstr set ");
		sql.append("isconactive=?, lastqsn=?, lastaction=? ");
		sql.append("WHERE clintid=? AND conid=?");
		try {
			CassandraConnector.getSession().execute(sql.toString(),
					new Object[] { co.getIsAct(), co.getLastQue(), co.getLastAction(), co.getClId(), co.getCoId() });
			isUpdated = true;
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		} finally {
		}
		return isUpdated;
	}

	/**
	 * Update contest status.
	 *
	 * @param co contest Object
	 * @return true, if successful
	 * @throws Exception the exception
	 */
	public static boolean updateContestStatus(ContestDVO co) throws Exception {
		boolean isUpdated = false;
		StringBuffer sql = new StringBuffer();
		sql.append("update pa_livvx_contest_mstr set ");
		sql.append("isconactive=? ");
		sql.append("WHERE clintid=? AND conid=?");
		try {
			CassandraConnector.getSession().execute(sql.toString(),
					new Object[] { co.getIsAct(), co.getClId(), co.getCoId() });
			isUpdated = true;
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		} finally {
		}
		return isUpdated;
	}
	
	
	
	
	
	
	/**
	 * Gets the contest Master Record  by contest id.
	 *
	 * @param clientId  the client id
	 * @param contestId the contest id
	 * @return the contest by contest id
	 * @throws Exception the exception
	 */
	public static List<ContestDVO> getAllDisplayContest(String clientId) throws Exception {
		ResultSet resultSet = null;
		List<ContestDVO> list = new ArrayList<ContestDVO>();
		ContestDVO contestDVO = null;
		try {
			resultSet = CassandraConnector.getSession().execute(
					"SELECT * from  pa_livvx_contest_host  WHERE clintid = ?", clientId);
			if (resultSet != null) {
				for (Row rs : resultSet) {
					contestDVO = new ContestDVO();
					Date date = null;
					contestDVO.setCoId(rs.getString("conid"));
					contestDVO.setClId(rs.getString("clintid"));
					contestDVO.setCat(rs.getString("caty"));
					contestDVO.setName(rs.getString("conname"));
					date = rs.getTimestamp("consrtdate");
					contestDVO.setStDate(date != null ? date.getTime() : null);
					contestDVO.setSubCat(rs.getString("subcaty"));
					contestDVO.setHostedBy(rs.getString("hostname"));
					contestDVO.setHstImgUrl(rs.getString("hostimg"));
					list.add(contestDVO);
				}
			}
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		} 
		return list;
	}
	
	
	public static ContestDVO getCurrentLiveContestByClientId(String clientId) throws Exception {
		ResultSet resultSet = null;
		ContestDVO contestDVO = null;
		try {
			resultSet = CassandraConnector.getSession().execute(
					"SELECT conid,isconactive from  pa_livvx_contest_mstr  WHERE clintid = ? ", clientId);
			if (resultSet != null) {
				for (Row rs : resultSet) {
					Integer status = rs.getInt("isconactive");
					if(status == null || status != 2){
						continue;
					}
					contestDVO = new ContestDVO();				
					contestDVO.setCoId(rs.getString("conid"));
					
					
					/*
					 * contestDVO.setClId(rs.getString("clintid"));
					 * contestDVO.setAnsType(rs.getString("anstype"));
					 * contestDVO.setCoType(rs.getString("contype"));
					 * contestDVO.setClImgU(rs.getString("brndimgurl"));
					 * contestDVO.setImgU(rs.getString("cardimgurl"));
					 * contestDVO.setSeqNo(rs.getInt("cardseqno"));
					 * contestDVO.setCat(rs.getString("cattype"));
					 * contestDVO.setName(rs.getString("conname")); date =
					 * rs.getTimestamp("consrtdate"); contestDVO.setStDate(date != null ?
					 * date.getTime() : null);
					 * contestDVO.setIsElimination(rs.getBool("iselimtype"));
					 * contestDVO.setqSize(rs.getInt("noofqns"));
					 * contestDVO.setSubCat(rs.getString("subcattype"));
					 * contestDVO.setThmColor(rs.getString("bgthemcolor"));
					 * contestDVO.setThmImgDesk(rs.getString("bgthemimgurl"));
					 * contestDVO.setThmImgMob(rs.getString("bgthemimgurlmob"));
					 * contestDVO.setPlayGameImg(rs.getString("playimgurl"));
					 * contestDVO.setThmId(rs.getInt("bgthembankid"));
					 * contestDVO.setIsSplitSummary(rs.getBool("issumrysplitable"));
					 * contestDVO.setIsLotEnbl(rs.getBool("islotryenabled"));
					 * contestDVO.setSumRwdType(rs.getString("sumryrwdtype"));
					 * contestDVO.setSumRwdVal(rs.getDouble("sumryrwdval"));
					 * contestDVO.setWinRwdType(rs.getString("lotryrwdtype"));
					 * contestDVO.setWinRwdVal(rs.getDouble("lotryrwdval"));
					 * contestDVO.setQueRwdType(rs.getString("qsnrwdtype"));
					 * contestDVO.setPartiRwdType(rs.getString("participantrwdtype"));
					 * contestDVO.setPartiRwdVal(rs.getDouble("participantrwdval"));
					 * contestDVO.setLastAction(rs.getString("lastaction"));
					 * contestDVO.setLastQue(rs.getInt("lastqsn"));
					 * contestDVO.setMigStatus(rs.getString("migrstatus"));
					 * contestDVO.setExtName(rs.getString("extconname"));
					 * contestDVO.setContRunningNo(rs.getInt("conrunningno"));
					 * contestDVO.setIsPwd(rs.getBool("isPwd"));
					 * contestDVO.setConPwd(rs.getString("conpwd"));
					 */

				}
			}
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		} finally {
		}
		return contestDVO;
	}
	
	

}
