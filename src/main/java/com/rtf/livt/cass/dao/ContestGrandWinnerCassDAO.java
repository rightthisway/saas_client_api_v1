/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.cass.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.SimpleStatement;
import com.datastax.driver.core.Statement;
import com.rtf.livt.dvo.ContestGrandWinnerDVO;
import com.rtf.saas.db.CassandraConnector;

/**
 * The Class ContestGrandWinnerCassDAO.
 */
public class ContestGrandWinnerCassDAO {

	/**
	 * Delete contest grand winner data by client id.
	 *
	 * @param clientId the client id
	 */
	public static void deleteContestGrandWinnerDataByClientId(String clientId) {
		CassandraConnector.getSession().executeAsync("DELETE FROM pt_livvx_contest_grand_winners where clintid=?",
				clientId);
	}

	/**
	 * Gets the contest grand winners by contest id.
	 *
	 * @param clientId  the client id
	 * @param contestId the contest id
	 * @return the contest grand winners by contest id
	 */
	public static List<ContestGrandWinnerDVO> getContestGrandWinnersByContestId(String clientId, String contestId) {
		final ResultSet results = CassandraConnector.getSession().execute(
				"SELECT * from pt_livvx_contest_grand_winners WHERE clintid=? and conid=? ", clientId, contestId);
		List<ContestGrandWinnerDVO> contestWinners = new ArrayList<ContestGrandWinnerDVO>();

		if (results != null) {
			for (Row row : results) {
				contestWinners.add(new ContestGrandWinnerDVO(row.getString("clintid"), row.getString("conid"),
						row.getString("custid"), row.getString("rwdtype"), row.getDouble("rwdval")));
			}
		}
		return contestWinners;
	}

	/**
	 * Gets the contest grand winners by contest id 
	 *
	 * @param clientId  the client id
	 * @param contestId the contest id
	 * @return the contest grand winners by contest id for migration
	 */
	public static List<ContestGrandWinnerDVO> getContestGrandWinnersByContestIdForMigration(String clientId,
			String contestId) {
		final ResultSet results = CassandraConnector.getSession().execute(
				"SELECT * from pt_livvx_contest_grand_winners WHERE clintid=? and conid=? ", clientId, contestId);
		List<ContestGrandWinnerDVO> contestWinners = new ArrayList<ContestGrandWinnerDVO>();

		if (results != null) {
			for (Row row : results) {
				contestWinners.add(new ContestGrandWinnerDVO(row.getString("clintid"), row.getString("conid"),
						row.getString("custid"), row.getString("rwdtype"), row.getDouble("rwdval"),
						row.getTimestamp("crdated"), null));
			}
		}
		return contestWinners;
	}

	/**
	 * Gets the contest grand winners Record by contest id.
	 *
	 * @param clientId  the client id
	 * @param contestId the contest id
	 * @return the contest grand winners map by contest id
	 */
	public static Map<String, ContestGrandWinnerDVO> getContestGrandWinnersMapByContestId(String clientId,
			String contestId) {
		final ResultSet results = CassandraConnector.getSession().execute(
				"SELECT * from pt_livvx_contest_grand_winners WHERE clintid=? and conid=? ", clientId, contestId);
		Map<String, ContestGrandWinnerDVO> contestWinnersMap = new HashMap<String, ContestGrandWinnerDVO>();

		if (results != null) {
			for (Row row : results) {
				String cuId = row.getString("custid");
				contestWinnersMap.put(cuId, new ContestGrandWinnerDVO(row.getString("clintid"), row.getString("conid"),
						cuId, row.getString("rwdtype"), row.getDouble("rwdval")));
			}
		}
		return contestWinnersMap;
	}

	/**
	 * Save Contest GrandWinner Object .
	 *
	 * @param obj the obj
	 */
	public static void save(ContestGrandWinnerDVO obj) {

		Statement statement = new SimpleStatement(
				"INSERT INTO pt_livvx_contest_grand_winners" + "  (clintid,conid,custid,rwdtype,rwdval,crdated ) "
						+ " VALUES (?,?, ?,?, ?,?,toTimestamp(now()) )",
				obj.getClId(), obj.getCoId(), obj.getCuId(), obj.getRwdType(), obj.getRwdVal());

		CassandraConnector.getSession().execute(statement);
	}

	/**
	 * Save all GrandWinner Object.
	 *
	 * @param grandWinners the grand winners
	 */
	public static void saveAll(List<ContestGrandWinnerDVO> grandWinners) {

		for (ContestGrandWinnerDVO obj : grandWinners) {
			Statement statement = new SimpleStatement(
					"INSERT INTO pt_livvx_contest_grand_winners" + "  (clintid,conid,custid,rwdtype,rwdval,crdated ) "
							+ " VALUES (?,?,?,?,?,toTimestamp(now()) )",
					obj.getClId(), obj.getCoId(), obj.getCuId(), obj.getRwdType(), obj.getRwdVal());
			CassandraConnector.getSession().execute(statement);
		}
	}

}