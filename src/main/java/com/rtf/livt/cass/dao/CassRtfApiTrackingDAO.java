/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.cass.dao;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.SimpleStatement;
import com.datastax.driver.core.Statement;
import com.rtf.livt.dvo.CassRtfApiTracking;
import com.rtf.saas.db.CassandraConnector;

/**
 * The Class CassRtfApiTrackingDAO.
 */
/*
 *
 * CREATE TABLE saasprodclientadminwcid.pt_livvx_rtf_api_tracking ( id uuid
 * PRIMARY KEY, actresult text, anscnt int, apiname text, appver text, clintid
 * text, conid text, crdatetime timestamp, custid text, custipaddr text,
 * description text, deviceapitime timestamp, deviceinfo text, devicetype text,
 * enddatetime timestamp, fbcallbacktime timestamp, nodeid int, platform text,
 * qsnno text, resstatus int, rtcnt int, sessionid text, srtdatetime timestamp )
 */
public class CassRtfApiTrackingDAO {

	/** The df. */
	public static SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");

	/**
	 * Delete tracking data by client id.
	 *
	 * @param clientId the client id
	 */
	public static void deleteTrackingDataByClientId(String clientId) {
		CassandraConnector.getSession().executeAsync("DELETE FROM pt_livvx_rtf_api_tracking where clintid=?", clientId);
	}

	/**
	 * Gets the all clients tracking data.
	 *
	 * @param clientId the client id
	 * @return the all clients tracking data
	 */
	public static List<CassRtfApiTracking> getAllClientsTrackingData(String clientId) {
		List<CassRtfApiTracking> list = new ArrayList<CassRtfApiTracking>();
		ResultSet results = CassandraConnector.getSession()
				.execute("SELECT * from pt_livvx_rtf_api_tracking where clintid=?", clientId);
		if (results != null) {
			for (Row row : results) {
				list.add(new CassRtfApiTracking(row.getString("clintid"), row.getUUID("id"),
						row.getTimestamp("srtdatetime"), row.getTimestamp("enddatetime"), row.getString("custipaddr"),
						row.getString("apiname"), row.getString("platform"), row.getString("devicetype"),
						row.getString("sessionid"), row.getString("actresult"), row.getString("custid"),
						row.getString("conid"), row.getString("description"), row.getString("deviceinfo"),
						row.getString("appver"), row.getString("deviceapitime"), row.getString("fbcallbacktime"),
						row.getInt("resstatus"), row.getString("qsnno"), row.getInt("rtcnt"), row.getInt("anscnt"),
						row.getInt("nodeid")));
			}
		}
		return list;
	}

	/**
	 * Save.
	 *
	 * @param obj the obj
	 */
	public void save(CassRtfApiTracking obj) {
		try {

			Statement statement = new SimpleStatement(
					"INSERT INTO pt_livvx_rtf_api_tracking(clintid,id,srtdatetime,enddatetime,custipaddr,apiname,platform,devicetype,sessionid,"
							+ "actresult,custid,conid,description,deviceinfo,appver,resstatus,qsnno,nodeid) VALUES (?,?, ?,?, ?,?, ?,?, ?,?, ?,?, ?,?,?,?,?,?)",
					obj.getClId(), obj.getId(), df.format(obj.getStartDate()), df.format(obj.getEndDate()),
					obj.getCustIPAddr(), obj.getApiName(), obj.getPlatForm(), obj.getDeviceType(), obj.getSessionId(),
					obj.getActionResult(), obj.getCustId(), obj.getContestId(), obj.getDescription(),
					obj.getDeviceInfo(), null != obj.getAppVerion() ? obj.getAppVerion() : "", obj.getResStatus(),
					obj.getQuestSlNo(), obj.getNodeId());
			CassandraConnector.getSession().executeAsync(statement);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Save for device time tracking.
	 *
	 * @param obj the obj
	 */
	public void saveForDeviceTimeTracking(CassRtfApiTracking obj) {
		try {
			CassandraConnector.getSession().executeAsync(
					"INSERT INTO pt_livvx_rtf_api_tracking(clintid,id,srtdatetime,enddatetime,custipaddr,apiname,platform,devicetype,sessionid,"
							+ "actresult,custid,conid,description,deviceinfo,appver,deviceapitime,fbcallbacktime,resstatus,qsnno,rtcnt,anscnt,nodeid) VALUES (?,?, ?,?, ?,?, ?,?, ?,?, ?,?, ?,?,?,?,?,?,?,?,?,?)",
					obj.getClId(), obj.getId(), obj.getStartDate(), obj.getEndDate(), obj.getCustIPAddr(),
					obj.getApiName(), obj.getPlatForm(), obj.getDeviceType(), obj.getSessionId(), obj.getActionResult(),
					obj.getCustId(), obj.getContestId(), obj.getDescription(), obj.getDeviceInfo(),
					null != obj.getAppVerion() ? obj.getAppVerion() : "", obj.getDeviceApiStartTime(),
					obj.getFbCallbackTime(), obj.getResStatus(), obj.getQuestSlNo(), obj.getRetryCount(),
					obj.getAnsCount(), obj.getNodeId());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}