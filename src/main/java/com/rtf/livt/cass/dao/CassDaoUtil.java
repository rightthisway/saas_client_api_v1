package com.rtf.livt.cass.dao;

import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.exceptions.DriverException;
import com.rtf.saas.db.CassandraConnector;

public class CassDaoUtil {

	public static Integer getCustContDtlsByCustId(String clientId, String nodeid)
			throws Exception {
		final ResultSet results = CassandraConnector.getSession().execute(
				"SELECT clintid,nodeid,usrcnt "
						+ " from pc_livvx_gen_userid WHERE clintid=? and nodeid=? ",
				clientId, nodeid);
		Integer startCount = null;
		if (results != null) {
			Row row = results.one();
			if (row != null) {				
				startCount = row.getInt("usrcnt");			
			}
		}
		return startCount;
	}
	
	
	public static Long getCustContDtlsLastGeneratedSequence(String clientId, String nodeid)
			throws Exception {
		Long startCount = null;
		try {
			final ResultSet results = CassandraConnector.getSession().execute(
					"SELECT clintid,nodeid,usrcntr from pc_livvx_gen_usrcntr WHERE clintid=? and nodeid=? ",
					clientId, nodeid);
			if (results != null) {
				Row row = results.one();
				if (row != null) {
					startCount = row.getLong("usrcntr");
				}
			}
		} catch (final DriverException de) {
			de.printStackTrace();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return startCount;
	}
	
	
	public static void updateNextCounterNumber(String clId, String nodeId) {
		Session session = CassandraConnector.getSession();
		try {
			String sql = " update pc_livvx_gen_usrcntr set usrcntr=usrcntr+1 where clintid=? and nodeid=?";
			PreparedStatement statement = session.prepare(sql.toString());
			BoundStatement boundStatement = statement.bind( clId, nodeId);
			session.executeAsync(boundStatement);
		} catch (final DriverException de) {
			de.printStackTrace();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

}
