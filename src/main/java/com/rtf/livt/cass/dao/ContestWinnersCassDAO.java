/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.cass.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.datastax.driver.core.BatchStatement;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.SimpleStatement;
import com.rtf.livt.dvo.ContestWinnerDVO;
import com.rtf.saas.db.CassandraConnector;

/**
 * The Class ContestWinnersCassDAO.
 */
public class ContestWinnersCassDAO {

	/**
	 * Batch Insert / Update winner details.
	 *
	 * @param winners          the winners
	 * @param rewardsPerWinner the rewards per winner
	 * @param rwdType          the rwd type
	 * @return the boolean
	 */
	public static Boolean batchUpsertWinnerDetails(List<ContestWinnerDVO> winners, Double rewardsPerWinner,
			String rwdType) {
		Boolean isUpdated = Boolean.FALSE;
		Session session = CassandraConnector.getSession();
		BatchStatement batchStatement = new BatchStatement();
		PreparedStatement preparedStatement = session
				.prepare("INSERT INTO pt_livvx_contest_winner (clintid,custid,conid, rwdtype,rwdval ) "
						+ " VALUES (?,?, ?,?,?)");
		for (ContestWinnerDVO obj : winners) {
			batchStatement.add(
					preparedStatement.bind(obj.getClId(), obj.getCuId(), obj.getCoId(), rwdType, rewardsPerWinner));
		}
		try {
			session.execute(batchStatement);
			isUpdated = Boolean.TRUE;
		} catch (Exception ex) {
			ex.printStackTrace();
			isUpdated = Boolean.FALSE;
		}
		return isUpdated;
	}

	/**
	 * Delete contest winner data by client id.
	 *
	 * @param clientId the client id
	 */
	public static void deleteContestWinnerDataByClientId(String clientId) {
		CassandraConnector.getSession().executeAsync("DELETE FROM pt_livvx_contest_winner where clintid=?", clientId);
	}

	/**
	 * Gets the contest winners by contest id and Client Id.
	 *
	 * @param clientId  the client id
	 * @param contestId the contest id
	 * @return the contest winners by contest id
	 */
	public static List<ContestWinnerDVO> getContestWinnersByContestId(String clientId, String contestId) {
		ResultSet results = CassandraConnector.getSession().execute(
				"SELECT clintid,conid,custid,rwdtype,rwdval from pt_livvx_contest_winner  WHERE clintid=? and conid=? ",
				clientId, contestId);
		List<ContestWinnerDVO> contestWinners = new ArrayList<ContestWinnerDVO>();

		if (results != null) {
			for (Row row : results) {
				contestWinners.add(new ContestWinnerDVO(row.getString("clintid"), row.getString("conid"),
						row.getString("custid"), row.getString("rwdtype"), row.getDouble("rwdval")));
			}
		}
		return contestWinners;
	}

	/**
	 * Gets the contest winners for migration.
	 *
	 * @param clientId  the client id
	 * @param contestId the contest id
	 * @return the contest winners for migration
	 */
	public static List<ContestWinnerDVO> getContestWinnersForMigration(String clientId, String contestId) {
		ResultSet results = CassandraConnector.getSession().execute(
				"SELECT clintid,conid,custid,rwdtype,rwdval,credate,upddate from pt_livvx_contest_winner  WHERE clintid=? and conid=? ",
				clientId, contestId);
		List<ContestWinnerDVO> contestWinners = new ArrayList<ContestWinnerDVO>();

		if (results != null) {
			for (Row row : results) {
				contestWinners.add(new ContestWinnerDVO(row.getString("clintid"), row.getString("conid"),
						row.getString("custid"), row.getString("rwdtype"), row.getDouble("rwdval"),
						row.getTimestamp("credate"), row.getTimestamp("upddate")));
			}
		}
		return contestWinners;
	}

	/**
	 * Gets the contest winners map by contest id.
	 *
	 * @param clientId  the client id
	 * @param contestId the contest id
	 * @return the contest winners map by contest id
	 */
	public static Map<String, ContestWinnerDVO> getContestWinnersMapByContestId(String clientId, String contestId) {
		ResultSet results = CassandraConnector.getSession().execute(
				"SELECT custid,clintid,conid,rwdtype,rwdval,credate,upddate  from pt_livvx_contest_winner  WHERE clintid=? and conid=? ",
				clientId, contestId);
		Map<String, ContestWinnerDVO> winnerMap = new HashMap<String, ContestWinnerDVO>();
		if (results != null) {
			for (Row row : results) {
				String cuId = row.getString("custid");
				winnerMap.put(cuId, new ContestWinnerDVO(row.getString("clintid"), row.getString("conid"), cuId,
						row.getString("rwdtype"), row.getDouble("rwdval")));
			}
		}
		return winnerMap;
	}

	/**
	 * Save Contest Winner Object
	 *
	 * @param obj the obj
	 */
	public static void save(ContestWinnerDVO obj) {
		SimpleStatement statement = new SimpleStatement(
				"INSERT INTO pt_livvx_contest_winner (clintid,conid,custid, rwdtype,rwdval,credate ) "
						+ " VALUES (?,?, ?,?,?,toTimestamp(now()))",
				obj.getClId(), obj.getCoId(), obj.getCuId(), obj.getRwdType(), obj.getRwdVal());
		CassandraConnector.getSession().executeAsync(statement);
	}

	/*
	 * public void updateContestWinnersRewards(List<CassContestWinners> winners) {
	 *
	 * for (CassContestWinners winner : winners) { SimpleStatement statement = new
	 * SimpleStatement("UPDATE liv_contest_winners SET rwdpnts=? WHERE conid=? and cuid=?)"
	 * ,new Date(),winner.getCoId(),winner.getCuId());
	 * CassandraConnector.getSession().executeAsync(statement); }
	 *
	 * }
	 */

	/**
	 * Update contest winners rewards.
	 *
	 * @param clientId         the client id
	 * @param contestId        the contest id
	 * @param rewardsPerWinner the rewards per winner
	 * @param rwdType          the rwd type
	 */
	public static void updateContestWinnersRewards(String clientId, String contestId, Double rewardsPerWinner,
			String rwdType) {

		SimpleStatement statement = new SimpleStatement(
				" UPDATE pt_livvx_contest_winner SET rwdval=?, rwdtype=? WHERE conid=? and clintid=? )",
				rewardsPerWinner, rwdType, contestId, clientId);
		CassandraConnector.getSession().executeAsync(statement);
	}

}