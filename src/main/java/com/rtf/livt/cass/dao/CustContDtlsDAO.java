/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.cass.dao;

import java.util.ArrayList;
import java.util.List;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.SimpleStatement;
import com.datastax.driver.core.Statement;
import com.datastax.driver.core.exceptions.QueryExecutionException;
import com.datastax.driver.core.exceptions.QueryValidationException;
import com.rtf.livt.dvo.CustContDtlsDVO;
import com.rtf.saas.db.CassandraConnector;

/**
 * The Class CustContDtlsDAO.
 */
/*
 * CREATE TABLE saasprodclientadminwcid.pt_livvx_customer_contest_dtls ( clintid
 * text, conid text, custid text, ansrwdtype text, ansrwdval double, custans
 * int, custlife int, lqcrt boolean, lqlife boolean, lqno int, sflrwds double,
 * PRIMARY KEY (clintid, conid, custid) ) WITH CLUSTERING ORDER BY (conid ASC,
 * custid ASC)
 *
 */
public class CustContDtlsDAO {

	/**
	 * Delete customer contest details data by client id. *
	 *
	 * @param clientId the client id
	 */
	public static void deleteCustContDtlsDataByClientId(String clientId) {
		CassandraConnector.getSession().executeAsync("DELETE FROM pt_livvx_customer_contest_dtls where clintid=?",
				clientId);
	}

	/**
	 * Gets all Objects for customer contest details
	 *
	 * @param clientId the client id
	 * @param coId     the contest id
	 * @return the CustContDtlsDVO customer contest details Object
	 */
	public static List<CustContDtlsDVO> getAllCustContDtls(String clientId, String coId) {
		final ResultSet results = CassandraConnector.getSession()
				.execute(
						" SELECT clintid,conid,custid,lqno,lqcrt,lqlife,custlife,custans,ansrwdtype,ansrwdval,sflrwds"
								+ " from pt_livvx_customer_contest_dtls WHERE  clintid=? and conid = ? ",
						clientId, coId);
		List<CustContDtlsDVO> custContDtlsList = new ArrayList<CustContDtlsDVO>();
		if (results != null) {
			for (Row row : results) {
				custContDtlsList.add(new CustContDtlsDVO(row.getString("clintid"), row.getString("custid"),
						row.getString("conid"), row.getInt("lqno"), row.getBool("lqcrt"), row.getBool("lqlife"),
						row.getString("ansrwdtype"), row.getDouble("ansrwdval"), row.getInt("custans"),
						row.getInt("custlife"), row.getDouble("sflrwds")));
			}
		}
		return custContDtlsList;
	}

	/**
	 * Gets the customer contest details
	 *
	 * @param clientId   the client id
	 * @param coId       the contest id
	 * @param customerId the customer id
	 * @return the   CustContDtlsDVO customer contest details Object
	 * @throws Exception the exception
	 */
	public static CustContDtlsDVO getCustContDtlsByCustId(String clientId, String coId, String customerId)
			throws Exception {
		final ResultSet results = CassandraConnector.getSession().execute(
				"SELECT clintid,conid,custid,lqno,lqcrt,lqlife,custlife,custans,ansrwdtype,ansrwdval,sflrwds "
						+ " from pt_livvx_customer_contest_dtls WHERE clintid=? and conid=? and custid = ? ",
				clientId, coId, customerId);

		CustContDtlsDVO custContDtls = null;
		if (results != null) {
			Row row = results.one();
			if (row != null) {
				custContDtls = new CustContDtlsDVO(row.getString("clintid"), row.getString("custid"),
						row.getString("conid"), row.getInt("lqno"), row.getBool("lqcrt"), row.getBool("lqlife"),
						row.getString("ansrwdtype"), row.getDouble("ansrwdval"), row.getInt("custans"),
						row.getInt("custlife"), row.getDouble("sflrwds"));
			}
		}
		return custContDtls;
	}

	/**
	 * Create CustContDtlsDVO customer contest details Object.
	 *
	 * @param obj the obj
	 * @throws Exception the exception
	 */
	public static void save(CustContDtlsDVO obj) throws Exception {
		Statement statement = new SimpleStatement(
				"INSERT INTO pt_livvx_customer_contest_dtls(clintid,conid,custid,lqno,lqcrt,lqlife,custlife,custans,ansrwdtype,ansrwdval,sflrwds)"
						+ " VALUES (?,?,?, ?,?, ?,?, ?,?,?,?)",
				obj.getClId(), obj.getCoId(), obj.getCuId(), obj.getLqNo(), obj.getIsLqCrt(), obj.getIsLqLife(),
				obj.getCuLife(), obj.getCuAns(), obj.getAnsRwdType(), obj.getAnsRwds(), obj.getSflRwds());

		try {
			CassandraConnector.getSession().executeAsync(statement);
		} catch (QueryExecutionException qex) {
			qex.printStackTrace();
		} catch (QueryValidationException qvx) {
			qvx.printStackTrace();
		}
	}

}