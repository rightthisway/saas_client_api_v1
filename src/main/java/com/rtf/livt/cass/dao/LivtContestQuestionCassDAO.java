/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.cass.dao;

import java.util.ArrayList;
import java.util.List;

import com.datastax.driver.core.BatchStatement;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.exceptions.DriverException;
import com.rtf.livt.dvo.LivtContestQuestionDVO;
import com.rtf.ott.dvo.CustomerAnswerDVO;
import com.rtf.saas.db.CassandraConnector;
import com.rtf.saas.exception.RTFDataAccessException;
import com.rtf.saas.util.GenUtil;

/**
 * The Class LivtContestQuestionCassDAO.
 */
public class LivtContestQuestionCassDAO {

	/**
	 * Delete contest questions Object.
	 *
	 * @param obj the contest questions Object.
	 * @return the customer answer DVO
	 * @throws Exception the exception
	 */
	public static CustomerAnswerDVO deleteContestquestions(LivtContestQuestionDVO obj) throws Exception {

		try {
			String keySpace = CassandraConnector.getRtfSassCassKeySpace();
			CassandraConnector.getSession().execute(
					" delete from  " + keySpace
							+ "pa_livvx_contest_question where clintid=? and conid=? and conqsnid=? ",
					obj.getClintid(), obj.getConid(), obj.getConqsnid());

		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		} finally {
		}
		return null;
	}

	/**
	 * Gets the all contest questions by contest id.
	 *
	 * @param clientId  the client id
	 * @param contestId the contest id
	 * @return the all contest questions by contest id
	 * @throws Exception the exception
	 */
	public static List<LivtContestQuestionDVO> getAllContestQuestionsByContestId(String clientId, String contestId)
			throws Exception {

		ResultSet resultSet = null;
		List<LivtContestQuestionDVO> list = new ArrayList<LivtContestQuestionDVO>();
		try {
			String keySpace = CassandraConnector.getRtfSassCassKeySpace();
			resultSet = CassandraConnector.getSession().execute(
					"SELECT * from  " + keySpace + "pa_livvx_contest_question  " + "WHERE clintid=? and conid=? ",
					clientId, contestId);

			LivtContestQuestionDVO dvo = null;
			if (resultSet != null) {
				for (Row row : resultSet) {
					dvo = new LivtContestQuestionDVO();
					dvo.setClintid(row.getString("clintid"));
					dvo.setConid(row.getString("conid"));
					dvo.setConqsnid(row.getInt("conqsnid"));
					dvo.setQsntext(row.getString("qsntext"));
					dvo.setOpa(row.getString("ansopta"));
					dvo.setOpb(row.getString("ansoptb"));
					dvo.setOpc(row.getString("ansoptc"));
					dvo.setOpd(row.getString("ansoptd"));
					dvo.setCorans(row.getString("corans"));
					dvo.setQbid(row.getInt("qsnbnkid"));
					dvo.setQsnorient(row.getString("qsnorient"));
					dvo.setQsnseqno(row.getInt("qsnseqno"));
					dvo.setAnsRwdVal(row.getDouble("ansrwdval"));
					dvo.setqDifLevel(row.getString("difficultlevel"));
					dvo.setMjRwdType(row.getString("jackpottype"));
					dvo.setMjWinnersCount(row.getInt("maxwinners"));
					dvo.setMjRwdVal((double) row.getInt("maxqtyperwinner"));
					dvo.setEncKey(row.getString("enckey"));
					dvo.setCoupcde(row.getString("couponcode"));
					dvo.setCoupondiscType(row.getString("discountype"));
					list.add(dvo);

				}
			}
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		} finally {
		}
		return list;
	}

	/**
	 * Gets the contest question By contest id and question id.
	 *
	 * @param clientId   the client id
	 * @param conId      the contest id
	 * @param questionId the question id
	 * @return the contest questioncontest id and question id
	 * @throws Exception the exception
	 */

	public static LivtContestQuestionDVO getContestQuestioncontestIdAndQuestionId(String clientId, String conId,
			Integer questionId) throws Exception {

		ResultSet resultSet = null;
		LivtContestQuestionDVO dvo = null;

		try {
			String keySpace = CassandraConnector.getRtfSassCassKeySpace();
			resultSet = CassandraConnector.getSession().execute("SELECT * from  " + keySpace
					+ "pa_livvx_contest_question  " + "WHERE clintid=? and conid=? and conqsnid = ? ", clientId, conId,
					questionId);

			if (resultSet != null) {
				dvo = new LivtContestQuestionDVO();
				for (Row row : resultSet) {
					dvo.setClintid(row.getString("clintid"));
					dvo.setConid(row.getString("conid"));
					dvo.setConqsnid(row.getInt("conqsnid"));
					dvo.setQsntext(row.getString("qsntext"));
					dvo.setOpa(row.getString("ansopta"));
					dvo.setOpb(row.getString("ansoptb"));
					dvo.setOpc(row.getString("ansoptc"));
					dvo.setOpd(row.getString("ansoptd"));
					dvo.setCorans(row.getString("corans"));
					dvo.setQbid(row.getInt("qsnbnkid"));
					dvo.setQsnorient(row.getString("qsnorient"));
					dvo.setQsnseqno(row.getInt("qsnseqno"));				
					dvo.setAnsRwdVal(row.getDouble("ansrwdval"));
					dvo.setqDifLevel(row.getString("difficultlevel"));
					dvo.setMjRwdType(row.getString("jackpottype"));
					dvo.setMjWinnersCount(row.getInt("maxwinners"));
					dvo.setMjRwdVal((double) row.getInt("maxqtyperwinner"));
					dvo.setEncKey(row.getString("enckey"));
					dvo.setCoupcde(row.getString("couponcode"));
					dvo.setCoupondiscType(row.getString("discountype"));
				}
			}

		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		} finally {
		}
		return dvo;
	}

	/**
	 * Gets the contest question contest id and question seq no.
	 *
	 * @param clientId      the client id
	 * @param conId         the contest id
	 * @param questionSeqNo the question sequence no
	 * @return the contest question contest id and question sequence no
	 * @throws Exception the exception
	 */
	public static LivtContestQuestionDVO getContestQuestioncontestIdAndQuestionSeqNo(String clientId, String conId,
			Integer questionSeqNo) throws Exception {

		ResultSet resultSet = null;
		LivtContestQuestionDVO dvo = null;

		try {
			String keySpace = CassandraConnector.getRtfSassCassKeySpace();
			resultSet = CassandraConnector.getSession().execute("SELECT * from  " + keySpace
					+ "pa_livvx_contest_question  " + "WHERE clintid=? and conid=? and qsnseqno = ? ", clientId, conId,
					questionSeqNo);
			dvo = new LivtContestQuestionDVO();
			if (resultSet != null) {
				for (Row row : resultSet) {
					dvo.setClintid(row.getString("clintid"));
					dvo.setConid(row.getString("conid"));
					dvo.setConqsnid(row.getInt("conqsnid"));
					dvo.setQsntext(row.getString("qsntext"));
					dvo.setOpa(row.getString("ansopta"));
					dvo.setOpb(row.getString("ansoptb"));
					dvo.setOpc(row.getString("ansoptc"));
					dvo.setOpd(row.getString("ansoptd"));
					dvo.setCorans(row.getString("corans"));
					dvo.setQbid(row.getInt("qsnbnkid"));
					dvo.setQsnorient(row.getString("qsnorient"));
					dvo.setQsnseqno(row.getInt("qsnseqno"));
					dvo.setAnsRwdVal(row.getDouble("ansrwdval"));
					dvo.setqDifLevel(row.getString("difficultlevel"));
					dvo.setMjRwdType(row.getString("jackpottype"));
					dvo.setMjWinnersCount(row.getInt("maxwinners"));
					dvo.setMjRwdVal((double) row.getInt("maxqtyperwinner"));
					dvo.setEncKey(row.getString("enckey"));
					dvo.setCoupcde(row.getString("couponcode"));
					dvo.setCoupondiscType(row.getString("discountype"));
				}
			}

		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		} finally {
		}
		return dvo;
	}

	/**
	 * Save contestquestions Object.
	 *
	 * @param obj the  LivtContestQuestionDVO object
	 * @return the customer answer DVO
	 * @throws Exception the exception
	 */
	public static CustomerAnswerDVO saveContestquestions(LivtContestQuestionDVO obj) throws Exception {

		try {
			String keySpace = CassandraConnector.getRtfSassCassKeySpace();
			CassandraConnector.getSession().execute(" insert into " + keySpace
					+ "pa_livvx_contest_question  (clintid,conid,conqsnid,qsntext,ansopta,ansoptb,"
					+ "  ansoptc,ansoptd,corans,qsnbnkid,qsnorient,qsnseqno,ansrwdval,difficultlevel,jackpottype,maxqtyperwinner,maxwinners,"
					+ " creby,credate,enckey)" + " values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,toTimestamp(now()),?)  ",
					obj.getClintid(), obj.getConid(), obj.getConqsnid(), obj.getQsntext(), obj.getOpa(), obj.getOpb(),
					obj.getOpc(), obj.getOpd(), obj.getCorans(), obj.getQbid(), obj.getQsnorient(), obj.getQsnseqno(),
					obj.getAnsRwdVal(), obj.getqDifLevel(), obj.getMjRwdType(), obj.getMjRwdVal().intValue(),
					obj.getMjWinnersCount(), obj.getCreby(), obj.getEncKey());

		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		} finally {
		}
		return null;
	}

	/**
	 * Update LivtContestQuestionDVO Object.
	 *
	 * @param obj the obj
	 * @return the customer answer DVO
	 * @throws Exception the exception
	 */
	public static CustomerAnswerDVO updateContestquestions(LivtContestQuestionDVO obj) throws Exception {

		try {
			String keySpace = CassandraConnector.getRtfSassCassKeySpace();
			CassandraConnector.getSession().execute(" update " + keySpace
					+ "pa_livvx_contest_question set qsntext=?,ansopta=?,ansoptb=?,"
					+ "    ansoptc=?,ansoptd=?,corans=?,qsnbnkid=?,qsnorient=?,qsnseqno=?,"
					+ " ansrwdval=?,difficultlevel=?,jackpottype=?,maxqtyperwinner=?,maxwinners=?,updby=?,upddate=toTimestamp(now()) "
					+ " where clintid=? and conid=? and conqsnid=?  ", // and cattype=? and subcattype=?
					obj.getQsntext(), obj.getOpa(), obj.getOpb(), obj.getOpc(), obj.getOpd(), obj.getCorans(),
					obj.getQbid(), obj.getQsnorient(), obj.getQsnseqno(), obj.getAnsRwdVal(), obj.getqDifLevel(),
					obj.getMjRwdType(), obj.getMjRwdVal().intValue(), obj.getMjWinnersCount(), obj.getUpdby(),
					obj.getClintid(), obj.getConid(), obj.getConqsnid());// obj.getCattyp(),obj.getSubcattyp()
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		} finally {
		}
		return null;
	}

	/**
	 * Update LivtContestQuestionDVO Object.
	 *
	 * @param obj the obj
	 * @return the customer answer DVO
	 * @throws Exception the exception
	 */
	public static CustomerAnswerDVO updateContestquestionsseqNo(LivtContestQuestionDVO obj) throws Exception {

		try {
			String keySpace = CassandraConnector.getRtfSassCassKeySpace();
			CassandraConnector.getSession()
					.execute(" update " + keySpace
							+ "pa_livvx_contest_question set qsnseqno=? where clintid=? and conid=? and conqsnid=? ", // and
																														// cattype=?
																														// and
																														// subcattype=?
							obj.getQsnseqno(), obj.getClintid(), obj.getConid(), obj.getConqsnid());// ,obj.getCattyp(),obj.getSubcattyp()
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		} finally {
		}
		return null;
	}

	/**
	 * Update contest questions Object :  LivtContestQuestionDVO
	 *
	 * @param objList the LivtContestQuestionDVO list
	 * @throws Exception the exception
	 */
	public static void updateContestQuestionsSeqNo(List<LivtContestQuestionDVO> objList) throws Exception {

		try {
			String keySpace = CassandraConnector.getRtfSassCassKeySpace();
			Session session = CassandraConnector.getSession();
			BatchStatement batchStatement = new BatchStatement();
			PreparedStatement preparedStatement = session.prepare(" update " + keySpace
					+ "pa_livvx_contest_question set qsnseqno=? where clintid=? and conid=? and conqsnid=? ");
			for (LivtContestQuestionDVO obj : objList) {
				batchStatement.add(
						preparedStatement.bind(obj.getQsnseqno(), obj.getClintid(), obj.getConid(), obj.getConqsnid()));
			}

			session.execute(batchStatement);
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		} finally {

		}
	}

}
