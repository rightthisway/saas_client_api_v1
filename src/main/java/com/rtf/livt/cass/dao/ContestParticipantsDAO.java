/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.cass.dao;

import java.util.ArrayList;
import java.util.List;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.SimpleStatement;
import com.datastax.driver.core.Statement;
import com.datastax.driver.core.exceptions.QueryExecutionException;
import com.datastax.driver.core.exceptions.QueryValidationException;
import com.rtf.livt.dto.LiveCustDetails;
import com.rtf.livt.dvo.ContestParticipantsDVO;
import com.rtf.saas.db.CassandraConnector;

/**
 * The Class ContestParticipantsDAO.
 */
/*
 * CREATE TABLE saasprodclientadminwcid.pt_livvx_contest_participants ( clintid
 * text, conid text, custid text, exdate timestamp, ipadd text, jndate
 * timestamp, plform text, status text, uid text, PRIMARY KEY (clintid, conid,
 * custid) ) WITH CLUSTERING ORDER BY (conid ASC, custid ASC)
 */
public class ContestParticipantsDAO {

	/**
	 * Delete contest participants by customer id and contest id and Client id.
	 *
	 * @param clId       the client Id
	 * @param contestId  the contest id
	 * @param customerId the customer id
	 */
	public static void deleteContestParticipantsByCustomerIdandContestId(String clId, String contestId,
			String customerId) {
		SimpleStatement statement = new SimpleStatement(
				"DELETE from pt_livvx_contest_participants WHERE clintid=? and conid=? and custid=? )", clId, contestId,
				customerId);
		CassandraConnector.getSession().executeAsync(statement);
	}

	/**
	 * Delete contest participants data by client id.
	 *
	 * @param clientId the client id
	 */
	public static void deleteContestParticipantsDataByClientId(String clientId) {
		CassandraConnector.getSession().executeAsync("DELETE FROM pt_livvx_contest_participants where clintid=?",
				clientId);
	}

	/**
	 * Gets the all live customer list By contest id and Client id
	 *
	 * @param clientId  the client id
	 * @param contestId the contest id
	 * @return the all live customer list for cache
	 */
	public static List<LiveCustDetails> getAllLiveCustomerListForCache(String clientId, String contestId) {
		final ResultSet results = CassandraConnector.getSession().execute(
				"SELECT custid,uid from pt_livvx_contest_participants  WHERE clintid=? and conid=? and status='ACTIVE' LIMIT 100",
				clientId, contestId);
		List<LiveCustDetails> participants = new ArrayList<LiveCustDetails>();

		if (results != null) {
			for (Row row : results) {
				participants.add(new LiveCustDetails(clientId, row.getString("custid"), row.getString("uid")));
			}
		}
		return participants;
	}

	/**
	 * Gets the all participants.
	 *
	 * @return the all participants
	 */
	public static List<ContestParticipantsDVO> getAllParticipants() {
		final ResultSet results = CassandraConnector.getSession().execute(
				"SELECT conid,clintid,custid,plform,ipadd,jndate,exdate,status,uid,timedur from pt_livvx_contest_participants");
		List<ContestParticipantsDVO> participants = new ArrayList<ContestParticipantsDVO>();

		if (results != null) {
			for (Row row : results) {
				participants.add(new ContestParticipantsDVO(row.getString("conid"), row.getString("clintid"),
						row.getString("custid"), row.getString("plform"), row.getString("ipadd"),
						row.getTimestamp("jndate"), row.getTimestamp("exdate"), row.getString("status"),
						row.getString("uid"),row.getInt("timedur")));
			}
		}
		return participants;
	}

	/**
	 * Gets the all participants By contest id and Client id
	 *
	 * @param clId      the client id
	 * @param contestId the contest id
	 * @return the all participants by contest id
	 */
	public static List<ContestParticipantsDVO> getAllParticipantsByContestId(String clId, String contestId) {
		final ResultSet results = CassandraConnector.getSession().execute(
				"SELECT conid,clintid,custid,plform,ipadd,jndate,exdate,status,uid,timedur from pt_livvx_contest_participants  WHERE clintid = ? and  conid=? ",
				clId, contestId);
		List<ContestParticipantsDVO> participants = new ArrayList<ContestParticipantsDVO>();

		if (results != null) {
			for (Row row : results) {
				participants.add(new ContestParticipantsDVO(row.getString("conid"), row.getString("clintid"),
						row.getString("custid"), row.getString("plform"), row.getString("ipadd"),
						row.getTimestamp("jndate"), row.getTimestamp("exdate"), row.getString("status"),
						row.getString("uid"),row.getInt("timedur")));
			}
		}
		return participants;
	}

	/**
	 * Gets the contest participant by contest id and customer id and Client id
	 *
	 * @param clId       the client Id
	 * @param contestId  the contest id
	 * @param customerId the customer id
	 * @return the contest participant by contest id and customer id
	 */
	public static ContestParticipantsDVO getContestParticipantByContestIdAndCustomerId(String clId, String contestId,
			String customerId) {
		final ResultSet results = CassandraConnector.getSession().execute(
				"SELECT conid,clintid,custid,plform,ipadd,jndate,exdate,status,uid,timedur from pt_livvx_contest_participants  WHERE clintid = ? and  conid=? and custid=? ",
				clId, contestId, customerId);
		ContestParticipantsDVO participants = null;

		if (results != null) {
			for (Row row : results) {
				participants = new ContestParticipantsDVO(row.getString("conid"), row.getString("clintid"),
						row.getString("custid"), row.getString("plform"), row.getString("ipadd"),
						row.getTimestamp("jndate"), row.getTimestamp("exdate"), row.getString("status"),
						row.getString("uid"),row.getInt("timedur"));
			}
		}
		return participants;
	}

	/**
	 * Gets the contest participant by contest id and customer id 
	 *
	 * @param clId       the client Id
	 * @param contestId  the contest id
	 * @param customerId the customer id
	 * @return the contest participant by contest id and customer id for val
	 */
	public static ContestParticipantsDVO getContestParticipantByContestIdAndCustomerIdForVal(String clId,
			String contestId, String customerId) {
		final ResultSet results = CassandraConnector.getSession().execute(
				"SELECT jndate,exdate,status,uid,timedur from pt_livvx_contest_participants  WHERE clintid = ? and  conid=? and custid=? ",
				clId, contestId, customerId);
		ContestParticipantsDVO participants = null;

		if (results != null) {
			for (Row row : results) {
				participants = new ContestParticipantsDVO(contestId, clId, customerId, null, null,
						row.getTimestamp("jndate"), row.getTimestamp("exdate"), row.getString("status"),
						row.getString("uid"),row.getInt("timedur"));
			}
		}
		return participants;
	}

	/**
	 * Gets the live customer count.
	 *
	 * @param clientId  the client id
	 * @param contestId the contest id
	 * @return the live customer count
	 */
	public static Integer getLiveCustomerCount(String clientId, String contestId) {
		Integer count = 0;
		ResultSet results = CassandraConnector.getSession().execute(
				"SELECT count(*) as cCount from pt_livvx_contest_participants  WHERE clintid=? and conid=? and status='ACTIVE' ",
				clientId, contestId);

		if (results != null) {
			Row row = results.one();
			if (row != null) {
				Long value = row.getLong("cCount");
				count = value.intValue();
			}
		}
		return count;
	}

	/**
	 * Save.
	 *
	 * @param obj the obj
	 */
	public static void save(ContestParticipantsDVO obj) {
		Statement statement = new SimpleStatement(
				"INSERT INTO pt_livvx_contest_participants (clintid,custid,conid,ipadd,plform,jndate,exdate,status,uid) "
						+ " VALUES (?,?, ?,?, ?,?, ?,?,?)",
				obj.getClId(), obj.getCuId(), obj.getCoId(), obj.getIpAdd(), obj.getPfm(), obj.getJnDate(),
				obj.getExDate(), obj.getStatus(), obj.getuId());

		CassandraConnector.getSession().executeAsync(statement);
	}

	/**
	 * Save for join.
	 *
	 * @param obj the obj
	 * @throws Exception the exception
	 */
	public static void saveForJoin(ContestParticipantsDVO obj) throws Exception {
		Statement statement = new SimpleStatement(
				"INSERT INTO pt_livvx_contest_participants (clintid,custid,conid,ipadd,plform,jndate,status,uid) "
						+ " VALUES (?,?, ?,?, ?,?, ?,?)",
				obj.getClId(), obj.getCuId(), obj.getCoId(), obj.getIpAdd(), obj.getPfm(), obj.getJnDate(),
				obj.getStatus(), obj.getuId());

		try {
			CassandraConnector.getSession().executeAsync(statement);
		} catch (QueryExecutionException qex) {
			qex.printStackTrace();
		} catch (QueryValidationException qvx) {
			qvx.printStackTrace();
		}
	}

	/**
	 * Update for exit contest.
	 *
	 * @param obj the obj
	 */
	public static void updateForExitContest(ContestParticipantsDVO obj) {
		Statement statement = null;
		if(obj.getTdur() > 0 ) {
			 statement = new SimpleStatement(
						"UPDATE pt_livvx_contest_participants set status = ? , exdate=?,timedur=?  where clintid=? and conid=? and custid=? ",
						obj.getStatus(), obj.getExDate(), obj.getTdur(), obj.getClId(), obj.getCoId(), obj.getCuId());
		}else {
			 statement = new SimpleStatement(
						"UPDATE pt_livvx_contest_participants set status = ? , exdate=? where clintid=? and conid=? and custid=? ",
						obj.getStatus(), obj.getExDate(), obj.getClId(), obj.getCoId(), obj.getCuId());
		}
		
		CassandraConnector.getSession().executeAsync(statement);
	}

	/**
	 * Update for exit contest with user id.
	 *
	 * @param obj the obj
	 */
	public static void updateForExitContestWithUserId(ContestParticipantsDVO obj) {
		Statement statement = null;
		if(obj.getTdur() > 0 ) {
			statement = new SimpleStatement(
					"UPDATE pt_livvx_contest_participants set status = ? , exdate=?, uid=? , timedur=? where clintid=? and conid=? and custid=? ",
					obj.getStatus(), obj.getExDate(), obj.getuId(), obj.getTdur(), obj.getClId(), obj.getCoId(), obj.getCuId());			
		}else {
			statement = new SimpleStatement(
					"UPDATE pt_livvx_contest_participants set status = ? , exdate=?, uid=?  where clintid=? and conid=? and custid=? ",
					obj.getStatus(), obj.getExDate(), obj.getuId(),  obj.getClId(), obj.getCoId(), obj.getCuId());		
			
		}		
		
		CassandraConnector.getSession().executeAsync(statement);

	}
	
	
	/**
	 * Update Timer for Live.
	 *
	 * @param obj the obj
	 * @throws Exception the exception
	 */
	public static void updateTimerForLive(ContestParticipantsDVO obj) throws Exception {
		if( obj.getTdur() <=0) {
			return;
		}
		Statement statement = new SimpleStatement(
				"UPDATE pt_livvx_contest_participants set  timedur=? where clintid=? and conid=? and custid=? ",
				 obj.getTdur(), obj.getClId(), obj.getCoId(), obj.getCuId());
		CassandraConnector.getSession().executeAsync(statement);
	}

}