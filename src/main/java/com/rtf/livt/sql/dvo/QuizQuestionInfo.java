/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.sql.dvo;

import com.rtf.livt.util.Error;
import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * The Class QuizQuestionInfo.
 */
@XStreamAlias("QuizQuestionInfo")
public class QuizQuestionInfo {

	/** The status. */
	private Integer status;

	/** The error. */
	private Error error;

	/** The quiz contest question. */
	private QuizContestQuestions quizContestQuestion;

	/** The message. */
	private String message;

	/** The no of questions. */
	private Integer noOfQuestions;

	/** The pq life count. */
	private Integer pqLifeCount;

	/**
	 * Gets the error.
	 *
	 * @return the error
	 */
	public Error getError() {
		return error;
	}

	/**
	 * Gets the message.
	 *
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * Gets the no of questions.
	 *
	 * @return the no of questions
	 */
	public Integer getNoOfQuestions() {
		if (noOfQuestions == null) {
			noOfQuestions = 0;
		}
		return noOfQuestions;
	}

	/**
	 * Gets the pq life count.
	 *
	 * @return the pq life count
	 */
	public Integer getPqLifeCount() {
		if (pqLifeCount == null) {
			pqLifeCount = 0;
		}
		return pqLifeCount;
	}

	/**
	 * Gets the quiz contest question.
	 *
	 * @return the quiz contest question
	 */
	public QuizContestQuestions getQuizContestQuestion() {
		return quizContestQuestion;
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public Integer getStatus() {
		return status;
	}

	/**
	 * Sets the error.
	 *
	 * @param error the new error
	 */
	public void setError(Error error) {
		this.error = error;
	}

	/**
	 * Sets the message.
	 *
	 * @param message the new message
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * Sets the no of questions.
	 *
	 * @param noOfQuestions the new no of questions
	 */
	public void setNoOfQuestions(Integer noOfQuestions) {
		this.noOfQuestions = noOfQuestions;
	}

	/**
	 * Sets the pq life count.
	 *
	 * @param pqLifeCount the new pq life count
	 */
	public void setPqLifeCount(Integer pqLifeCount) {
		this.pqLifeCount = pqLifeCount;
	}

	/**
	 * Sets the quiz contest question.
	 *
	 * @param quizContestQuestion the new quiz contest question
	 */
	public void setQuizContestQuestion(QuizContestQuestions quizContestQuestion) {
		this.quizContestQuestion = quizContestQuestion;
	}

	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

}
