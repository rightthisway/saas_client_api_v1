/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.sql.dvo;

import java.io.Serializable;
import java.util.Date;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * The Class QuizContestQuestions.
 */
@XStreamAlias("QuizContestQuestions")
public class QuizContestQuestions implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 2902119316625610695L;

	/** The cl id. */
	private String clId;

	/** The id. */
	private Integer id;

	/** The contest id. */
	private Integer contestId;

	/** The question S no. */
	private Integer questionSNo;

	/** The question. */
	private String question;

	/** The option A. */
	private String optionA;

	/** The option B. */
	private String optionB;

	/** The option C. */
	private String optionC;

	/** The question rewards. */
	// private String optionD;
	private Double questionRewards;

	/** The created date time. */
	@JsonIgnore
	private Date createdDateTime;

	/** The created by. */
	@JsonIgnore
	private String createdBy;

	/** The updated date time. */
	@JsonIgnore
	private Date updatedDateTime;

	/** The updated by. */
	@JsonIgnore
	private String updatedBy;

	/** The answer. */
	@JsonIgnore
	private String answer;

	/** The is answer count computed. */
	private Boolean isAnswerCountComputed = false;

	/** The no of jackpot winners. */
	@JsonIgnore
	private Integer noOfJackpotWinners;

	/**
	 * Gets the answer.
	 *
	 * @return the answer
	 */
	public String getAnswer() {
		return answer;
	}

	/**
	 * Gets the cl id.
	 *
	 * @return the cl id
	 */
	public String getClId() {
		return clId;
	}

	/**
	 * Gets the contest id.
	 *
	 * @return the contest id
	 */
	public Integer getContestId() {
		return contestId;
	}

	/**
	 * Gets the created by.
	 *
	 * @return the created by
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * Gets the created date time.
	 *
	 * @return the created date time
	 */
	public Date getCreatedDateTime() {
		return createdDateTime;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * Gets the checks if is answer count computed.
	 *
	 * @return the checks if is answer count computed
	 */
	public Boolean getIsAnswerCountComputed() {
		if (isAnswerCountComputed == null) {
			isAnswerCountComputed = false;
		}
		return isAnswerCountComputed;
	}

	/**
	 * Gets the no of jackpot winners.
	 *
	 * @return the no of jackpot winners
	 */
	public Integer getNoOfJackpotWinners() {
		return noOfJackpotWinners;
	}

	/**
	 * Gets the option A.
	 *
	 * @return the option A
	 */
	public String getOptionA() {
		return optionA;
	}

	/**
	 * Gets the option B.
	 *
	 * @return the option B
	 */
	public String getOptionB() {
		return optionB;
	}

	/**
	 * Gets the option C.
	 *
	 * @return the option C
	 */
	public String getOptionC() {
		return optionC;
	}

	/**
	 * Gets the question.
	 *
	 * @return the question
	 */
	public String getQuestion() {
		return question;
	}

	/**
	 * Gets the question rewards.
	 *
	 * @return the question rewards
	 */
	public Double getQuestionRewards() {
		return questionRewards;
	}

	/**
	 * Gets the question S no.
	 *
	 * @return the question S no
	 */
	public Integer getQuestionSNo() {
		return questionSNo;
	}

	/**
	 * Gets the updated by.
	 *
	 * @return the updated by
	 */
	public String getUpdatedBy() {
		return updatedBy;
	}

	/**
	 * Gets the updated date time.
	 *
	 * @return the updated date time
	 */
	public Date getUpdatedDateTime() {
		return updatedDateTime;
	}

	/**
	 * Sets the answer.
	 *
	 * @param answer the new answer
	 */
	public void setAnswer(String answer) {
		this.answer = answer;
	}

	/**
	 * Sets the cl id.
	 *
	 * @param clId the new cl id
	 */
	public void setClId(String clId) {
		this.clId = clId;
	}

	/**
	 * Sets the contest id.
	 *
	 * @param contestId the new contest id
	 */
	public void setContestId(Integer contestId) {
		this.contestId = contestId;
	}

	/**
	 * Sets the created by.
	 *
	 * @param createdBy the new created by
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * Sets the created date time.
	 *
	 * @param createdDateTime the new created date time
	 */
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * Sets the checks if is answer count computed.
	 *
	 * @param isAnswerCountComputed the new checks if is answer count computed
	 */
	public void setIsAnswerCountComputed(Boolean isAnswerCountComputed) {
		this.isAnswerCountComputed = isAnswerCountComputed;
	}

	/**
	 * Sets the no of jackpot winners.
	 *
	 * @param noOfJackpotWinners the new no of jackpot winners
	 */
	public void setNoOfJackpotWinners(Integer noOfJackpotWinners) {
		this.noOfJackpotWinners = noOfJackpotWinners;
	}

	/**
	 * Sets the option A.
	 *
	 * @param optionA the new option A
	 */
	public void setOptionA(String optionA) {
		this.optionA = optionA;
	}

	/**
	 * Sets the option B.
	 *
	 * @param optionB the new option B
	 */
	public void setOptionB(String optionB) {
		this.optionB = optionB;
	}

	/**
	 * Sets the option C.
	 *
	 * @param optionC the new option C
	 */
	public void setOptionC(String optionC) {
		this.optionC = optionC;
	}

	/**
	 * Sets the question.
	 *
	 * @param question the new question
	 */
	public void setQuestion(String question) {
		this.question = question;
	}

	/**
	 * Sets the question rewards.
	 *
	 * @param questionRewards the new question rewards
	 */
	public void setQuestionRewards(Double questionRewards) {
		this.questionRewards = questionRewards;
	}

	/**
	 * Sets the question S no.
	 *
	 * @param questionSNo the new question S no
	 */
	public void setQuestionSNo(Integer questionSNo) {
		this.questionSNo = questionSNo;
	}

	/**
	 * Sets the updated by.
	 *
	 * @param updatedBy the new updated by
	 */
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	/**
	 * Sets the updated date time.
	 *
	 * @param updatedDateTime the new updated date time
	 */
	public void setUpdatedDateTime(Date updatedDateTime) {
		this.updatedDateTime = updatedDateTime;
	}

}
