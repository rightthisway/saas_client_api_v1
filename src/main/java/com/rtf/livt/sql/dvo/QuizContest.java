/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.sql.dvo;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * The Class QuizContest.
 */
@XStreamAlias("QuizContest")
public class QuizContest implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -2929742733685777129L;

	/** The dt. */
	static SimpleDateFormat dt = new SimpleDateFormat("MM/dd/yy");

	/** The time ft. */
	static SimpleDateFormat timeFt = new SimpleDateFormat("hh:mmaa z");

	/** The cl id. */
	private String clId;

	/** The id. */
	private Integer id;

	/** The contest name. */
	private String contestName;

	/** The contest start date time. */
	@JsonIgnore
	private Date contestStartDateTime;

	/** The status. */
	@JsonIgnore
	private String status;

	/** The participants count. */
	@JsonIgnore
	private Integer participantsCount;

	/** The winners count. */
	@JsonIgnore
	private Integer winnersCount;

	/** The ticket winners count. */
	@JsonIgnore
	private Integer ticketWinnersCount;

	/** The point winners count. */
	@JsonIgnore
	private Integer pointWinnersCount;

	/** The created date time. */
	@JsonIgnore
	private Date createdDateTime;

	/** The created by. */
	@JsonIgnore
	private String createdBy;

	/** The updated date time. */
	@JsonIgnore
	private Date updatedDateTime;

	/** The updated by. */
	@JsonIgnore
	private String updatedBy;

	/** The contest type. */
	@JsonIgnore
	private String contestType;

	/** The max free ticket winners. */
	private Integer maxFreeTicketWinners;

	/** The free tickets per winner. */
	private Integer freeTicketsPerWinner;

	/** The points per winner. */
	@JsonIgnore
	private Double pointsPerWinner;

	/** The no of questions. */
	private Integer noOfQuestions;

	/** The total rewards. */
	private Double totalRewards;

	/** The rewards per question. */
	@JsonIgnore
	private Double rewardsPerQuestion;

	/** The is customer stats updated. */
	@JsonIgnore
	private Boolean isCustomerStatsUpdated;

	/** The contest start date. */
	private String contestStartDate;

	/** The contest start time. */
	private String contestStartTime;

	/** The promo code. */
	@JsonIgnore
	private String promoCode;

	/** The promo ref id. */
	@JsonIgnore
	private Integer promoRefId;

	/** The promo ref type. */
	@JsonIgnore
	private String promoRefType;

	/** The promo ref name. */
	@JsonIgnore
	private String promoRefName;

	/** The discount percentage. */
	@JsonIgnore
	private Double discountPercentage;

	/** The promo expiry date. */
	@JsonIgnore
	private Date promoExpiryDate;

	/** The single ticket price. */
	@JsonIgnore
	private Double singleTicketPrice;

	/** The zone. */
	@JsonIgnore
	private String zone;

	/** The last action. */
	@JsonIgnore
	private String lastAction;

	/** The last question no. */
	@JsonIgnore
	private Integer lastQuestionNo;

	/** The extended name. */
	@JsonIgnore
	private String extendedName;

	/** The contest mode. */
	@JsonIgnore
	private String contestMode;

	/** The contest pwd. */
	@JsonIgnore
	private String contestPwd;

	/**
	 * Gets the cl id.
	 *
	 * @return the cl id
	 */
	public String getClId() {
		return clId;
	}

	/**
	 * Gets the contest mode.
	 *
	 * @return the contest mode
	 */
	public String getContestMode() {
		return contestMode;
	}

	/**
	 * Gets the contest name.
	 *
	 * @return the contest name
	 */
	public String getContestName() {
		return contestName;
	}

	/**
	 * Gets the contest pwd.
	 *
	 * @return the contest pwd
	 */
	public String getContestPwd() {
		return contestPwd;
	}

	/**
	 * Gets the contest start date.
	 *
	 * @return the contest start date
	 */
	public String getContestStartDate() {
		if (contestStartDate != null) {
			return contestStartDate;
		}
		if (getContestStartDateTime() != null) {
			contestStartDate = dt.format(getContestStartDateTime());
		}
		return contestStartDate;
	}

	/**
	 * Gets the contest start date time.
	 *
	 * @return the contest start date time
	 */
	public Date getContestStartDateTime() {
		return contestStartDateTime;
	}

	/**
	 * Gets the contest start time.
	 *
	 * @return the contest start time
	 */
	public String getContestStartTime() {
		if (contestStartTime != null) {
			return contestStartTime;
		}
		if (getContestStartDateTime() != null) {

			try {
				Date today = dt.parse(dt.format(new Date()));
				Calendar cal = Calendar.getInstance();
				cal.add(Calendar.DAY_OF_MONTH, 1);
				Date tomorrow = dt.parse(dt.format(new Date(cal.getTimeInMillis())));
				Date contestDate = dt.parse(dt.format(getContestStartDateTime()));
				if (contestDate.compareTo(today) == 0) {
					contestStartTime = "Today " + timeFt.format(getContestStartDateTime());
					return contestStartTime;
				} else if (contestDate.compareTo(tomorrow) == 0) {
					contestStartTime = "Tomorrow " + timeFt.format(getContestStartDateTime());
					return contestStartTime;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			contestStartTime = dt.format(getContestStartDateTime()) + " " + timeFt.format(getContestStartDateTime());
		}
		return contestStartTime;
	}

	/**
	 * Gets the contest type.
	 *
	 * @return the contest type
	 */
	public String getContestType() {
		return contestType;
	}

	/**
	 * Gets the created by.
	 *
	 * @return the created by
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * Gets the created date time.
	 *
	 * @return the created date time
	 */
	public Date getCreatedDateTime() {
		return createdDateTime;
	}

	/**
	 * Gets the discount percentage.
	 *
	 * @return the discount percentage
	 */
	public Double getDiscountPercentage() {
		return discountPercentage;
	}

	/**
	 * Gets the extended name.
	 *
	 * @return the extended name
	 */
	public String getExtendedName() {
		return extendedName;
	}

	/**
	 * Gets the free tickets per winner.
	 *
	 * @return the free tickets per winner
	 */
	public Integer getFreeTicketsPerWinner() {
		return freeTicketsPerWinner;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * Gets the checks if is customer stats updated.
	 *
	 * @return the checks if is customer stats updated
	 */
	public Boolean getIsCustomerStatsUpdated() {
		return isCustomerStatsUpdated;
	}

	/**
	 * Gets the last action.
	 *
	 * @return the last action
	 */
	public String getLastAction() {
		return lastAction;
	}

	/**
	 * Gets the last question no.
	 *
	 * @return the last question no
	 */
	public Integer getLastQuestionNo() {
		return lastQuestionNo;
	}

	/**
	 * Gets the max free ticket winners.
	 *
	 * @return the max free ticket winners
	 */
	public Integer getMaxFreeTicketWinners() {
		return maxFreeTicketWinners;
	}

	/**
	 * Gets the no of questions.
	 *
	 * @return the no of questions
	 */
	public Integer getNoOfQuestions() {
		return noOfQuestions;
	}

	/**
	 * Gets the participants count.
	 *
	 * @return the participants count
	 */
	public Integer getParticipantsCount() {
		return participantsCount;
	}

	/**
	 * Gets the points per winner.
	 *
	 * @return the points per winner
	 */
	public Double getPointsPerWinner() {
		return pointsPerWinner;
	}

	/**
	 * Gets the point winners count.
	 *
	 * @return the point winners count
	 */
	public Integer getPointWinnersCount() {
		return pointWinnersCount;
	}

	/**
	 * Gets the promo code.
	 *
	 * @return the promo code
	 */
	public String getPromoCode() {
		return promoCode;
	}

	/**
	 * Gets the promo expiry date.
	 *
	 * @return the promo expiry date
	 */
	public Date getPromoExpiryDate() {
		return promoExpiryDate;
	}

	/**
	 * Gets the promo ref id.
	 *
	 * @return the promo ref id
	 */
	public Integer getPromoRefId() {
		return promoRefId;
	}

	/**
	 * Gets the promo ref name.
	 *
	 * @return the promo ref name
	 */
	public String getPromoRefName() {
		return promoRefName;
	}

	/**
	 * Gets the promo ref type.
	 *
	 * @return the promo ref type
	 */
	public String getPromoRefType() {
		return promoRefType;
	}

	/**
	 * Gets the rewards per question.
	 *
	 * @return the rewards per question
	 */
	public Double getRewardsPerQuestion() {
		return rewardsPerQuestion;
	}

	/**
	 * Gets the single ticket price.
	 *
	 * @return the single ticket price
	 */
	public Double getSingleTicketPrice() {
		return singleTicketPrice;
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Gets the ticket winners count.
	 *
	 * @return the ticket winners count
	 */
	public Integer getTicketWinnersCount() {
		return ticketWinnersCount;
	}

	/**
	 * Gets the total rewards.
	 *
	 * @return the total rewards
	 */
	public Double getTotalRewards() {
		return totalRewards;
	}

	/**
	 * Gets the updated by.
	 *
	 * @return the updated by
	 */
	public String getUpdatedBy() {
		return updatedBy;
	}

	/**
	 * Gets the updated date time.
	 *
	 * @return the updated date time
	 */
	public Date getUpdatedDateTime() {
		return updatedDateTime;
	}

	/**
	 * Gets the winners count.
	 *
	 * @return the winners count
	 */
	public Integer getWinnersCount() {
		return winnersCount;
	}

	/**
	 * Gets the zone.
	 *
	 * @return the zone
	 */
	public String getZone() {
		return zone;
	}

	/**
	 * Sets the cl id.
	 *
	 * @param clId the new cl id
	 */
	public void setClId(String clId) {
		this.clId = clId;
	}

	/**
	 * Sets the contest mode.
	 *
	 * @param contestMode the new contest mode
	 */
	public void setContestMode(String contestMode) {
		this.contestMode = contestMode;
	}

	/**
	 * Sets the contest name.
	 *
	 * @param contestName the new contest name
	 */
	public void setContestName(String contestName) {
		this.contestName = contestName;
	}

	/**
	 * Sets the contest pwd.
	 *
	 * @param contestPwd the new contest pwd
	 */
	public void setContestPwd(String contestPwd) {
		this.contestPwd = contestPwd;
	}

	/**
	 * Sets the contest start date.
	 *
	 * @param contestStartDate the new contest start date
	 */
	public void setContestStartDate(String contestStartDate) {
		this.contestStartDate = contestStartDate;
	}

	/**
	 * Sets the contest start date time.
	 *
	 * @param contestStartDateTime the new contest start date time
	 */
	public void setContestStartDateTime(Date contestStartDateTime) {
		this.contestStartDateTime = contestStartDateTime;
	}

	/**
	 * Sets the contest start time str.
	 *
	 * @param contestStartTime the new contest start time str
	 */
	public void setContestStartTimeStr(String contestStartTime) {
		this.contestStartTime = contestStartTime;
	}

	/**
	 * Sets the contest type.
	 *
	 * @param contestType the new contest type
	 */
	public void setContestType(String contestType) {
		this.contestType = contestType;
	}

	/**
	 * Sets the created by.
	 *
	 * @param createdBy the new created by
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * Sets the created date time.
	 *
	 * @param createdDateTime the new created date time
	 */
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}

	/**
	 * Sets the discount percentage.
	 *
	 * @param discountPercentage the new discount percentage
	 */
	public void setDiscountPercentage(Double discountPercentage) {
		this.discountPercentage = discountPercentage;
	}

	/**
	 * Sets the extended name.
	 *
	 * @param extendedName the new extended name
	 */
	public void setExtendedName(String extendedName) {
		this.extendedName = extendedName;
	}

	/**
	 * Sets the free tickets per winner.
	 *
	 * @param freeTicketsPerWinner the new free tickets per winner
	 */
	public void setFreeTicketsPerWinner(Integer freeTicketsPerWinner) {
		this.freeTicketsPerWinner = freeTicketsPerWinner;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * Sets the checks if is customer stats updated.
	 *
	 * @param isCustomerStatsUpdated the new checks if is customer stats updated
	 */
	public void setIsCustomerStatsUpdated(Boolean isCustomerStatsUpdated) {
		this.isCustomerStatsUpdated = isCustomerStatsUpdated;
	}

	/**
	 * Sets the last action.
	 *
	 * @param lastAction the new last action
	 */
	public void setLastAction(String lastAction) {
		this.lastAction = lastAction;
	}

	/**
	 * Sets the last question no.
	 *
	 * @param lastQuestionNo the new last question no
	 */
	public void setLastQuestionNo(Integer lastQuestionNo) {
		this.lastQuestionNo = lastQuestionNo;
	}

	/**
	 * Sets the max free ticket winners.
	 *
	 * @param maxFreeTicketWinners the new max free ticket winners
	 */
	public void setMaxFreeTicketWinners(Integer maxFreeTicketWinners) {
		this.maxFreeTicketWinners = maxFreeTicketWinners;
	}

	/**
	 * Sets the no of questions.
	 *
	 * @param noOfQuestions the new no of questions
	 */
	public void setNoOfQuestions(Integer noOfQuestions) {
		this.noOfQuestions = noOfQuestions;
	}

	/**
	 * Sets the participants count.
	 *
	 * @param participantsCount the new participants count
	 */
	public void setParticipantsCount(Integer participantsCount) {
		this.participantsCount = participantsCount;
	}

	/**
	 * Sets the points per winner.
	 *
	 * @param pointsPerWinner the new points per winner
	 */
	public void setPointsPerWinner(Double pointsPerWinner) {
		this.pointsPerWinner = pointsPerWinner;
	}

	/**
	 * Sets the point winners count.
	 *
	 * @param pointWinnersCount the new point winners count
	 */
	public void setPointWinnersCount(Integer pointWinnersCount) {
		this.pointWinnersCount = pointWinnersCount;
	}

	/**
	 * Sets the promo code.
	 *
	 * @param promoCode the new promo code
	 */
	public void setPromoCode(String promoCode) {
		this.promoCode = promoCode;
	}

	/**
	 * Sets the promo expiry date.
	 *
	 * @param promoExpiryDate the new promo expiry date
	 */
	public void setPromoExpiryDate(Date promoExpiryDate) {
		this.promoExpiryDate = promoExpiryDate;
	}

	/**
	 * Sets the promo ref id.
	 *
	 * @param promoRefId the new promo ref id
	 */
	public void setPromoRefId(Integer promoRefId) {
		this.promoRefId = promoRefId;
	}

	/**
	 * Sets the promo ref name.
	 *
	 * @param promoRefName the new promo ref name
	 */
	public void setPromoRefName(String promoRefName) {
		this.promoRefName = promoRefName;
	}

	/**
	 * Sets the promo ref type.
	 *
	 * @param promoRefType the new promo ref type
	 */
	public void setPromoRefType(String promoRefType) {
		this.promoRefType = promoRefType;
	}

	/**
	 * Sets the rewards per question.
	 *
	 * @param rewardsPerQuestion the new rewards per question
	 */
	public void setRewardsPerQuestion(Double rewardsPerQuestion) {
		this.rewardsPerQuestion = rewardsPerQuestion;
	}

	/**
	 * Sets the single ticket price.
	 *
	 * @param singleTicketPrice the new single ticket price
	 */
	public void setSingleTicketPrice(Double singleTicketPrice) {
		this.singleTicketPrice = singleTicketPrice;
	}

	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * Sets the ticket winners count.
	 *
	 * @param ticketWinnersCount the new ticket winners count
	 */
	public void setTicketWinnersCount(Integer ticketWinnersCount) {
		this.ticketWinnersCount = ticketWinnersCount;
	}

	/**
	 * Sets the total rewards.
	 *
	 * @param totalRewards the new total rewards
	 */
	public void setTotalRewards(Double totalRewards) {
		this.totalRewards = totalRewards;
	}

	/**
	 * Sets the updated by.
	 *
	 * @param updatedBy the new updated by
	 */
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	/**
	 * Sets the updated date time.
	 *
	 * @param updatedDateTime the new updated date time
	 */
	public void setUpdatedDateTime(Date updatedDateTime) {
		this.updatedDateTime = updatedDateTime;
	}

	/**
	 * Sets the winners count.
	 *
	 * @param winnersCount the new winners count
	 */
	public void setWinnersCount(Integer winnersCount) {
		this.winnersCount = winnersCount;
	}

	/**
	 * Sets the zone.
	 *
	 * @param zone the new zone
	 */
	public void setZone(String zone) {
		this.zone = zone;
	}

}
