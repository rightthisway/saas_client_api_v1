/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.sql.dvo;

import java.util.List;

import com.rtf.common.dvo.ClientCustomerDVO;
import com.rtf.livt.util.Error;
import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * The Class QuizContestInfo.
 */
@XStreamAlias("QuizContestInfo")
public class QuizContestInfo {

	/** The status. */
	private Integer status;

	/** The error. */
	private Error error;

	/** The Contest id. */
	private Integer ContestId;

	/** The contest name. */
	private String contestName;

	/** The contest start date. */
	private String contestStartDate;

	/** The contest start time. */
	private String contestStartTime;

	/** The contest price text. */
	private String contestPriceText;

	/** The max free tix winners. */
	// private Integer noOfQuestions;
	private Integer maxFreeTixWinners;

	/** The message. */
	private String message;

	/** The video source url. */
	private String videoSourceUrl;
	/** The is contest started. */
	private Boolean isContestStarted;

	/** The free tickets per winner. */
	private Integer freeTicketsPerWinner = 0;

	/** The contest total rewards. */
	private Double contestTotalRewards = 0.0;

	/** The total active rewards. */
	private Double totalActiveRewards = 0.0;

	/** The c promo codes count. */
	private Integer cPromoCodesCount = 0;

	/** The grand price text. */
	private String grandPriceText;

	/** The grand winer expiry text. */
	private String grandWinerExpiryText;

	/** The pending requests. */
	private Integer pendingRequests = 0;

	/** The customer. */
	private ClientCustomerDVO customer;

	/** The customer profile pic web view. */
	private String customerProfilePicWebView;

	/** The show join button. */
	private Boolean showJoinButton = false;

	/** The join button label. */
	private String joinButtonLabel;

	/** The join button popup msg. */
	private String joinButtonPopupMsg;

	/** The is existing contestant. */
	private Boolean isExistingContestant = false;

	/** The contest list. */
	List<QuizContest> contestList;

	/**
	 * Gets the contest id.
	 *
	 * @return the contest id
	 */
	public Integer getContestId() {
		if (ContestId == null) {
			ContestId = 0;
		}
		return ContestId;
	}

	/**
	 * Gets the contest list.
	 *
	 * @return the contest list
	 */
	public List<QuizContest> getContestList() {
		return contestList;
	}

	/**
	 * Gets the contest name.
	 *
	 * @return the contest name
	 */
	public String getContestName() {
		if (contestName == null) {
			contestName = "";
		}
		return contestName;
	}

	/**
	 * Gets the contest price text.
	 *
	 * @return the contest price text
	 */
	public String getContestPriceText() {
		if (contestPriceText == null) {
			contestPriceText = "";
		}
		return contestPriceText;
	}

	/**
	 * Gets the contest start date.
	 *
	 * @return the contest start date
	 */
	public String getContestStartDate() {
		if (contestStartDate == null) {
			contestStartDate = "";
		}
		return contestStartDate;
	}

	/**
	 * Gets the contest start time.
	 *
	 * @return the contest start time
	 */
	public String getContestStartTime() {
		if (contestStartTime == null) {
			contestStartTime = "";
		}
		return contestStartTime;
	}

	/**
	 * Gets the contest total rewards.
	 *
	 * @return the contest total rewards
	 */
	public Double getContestTotalRewards() {
		if (contestTotalRewards == null) {
			contestTotalRewards = 0.0;
		}
		return contestTotalRewards;
	}

	/**
	 * Gets the c promo codes count.
	 *
	 * @return the c promo codes count
	 */
	public Integer getcPromoCodesCount() {
		if (cPromoCodesCount == null) {
			cPromoCodesCount = 0;
		}
		return cPromoCodesCount;
	}

	/**
	 * Gets the customer.
	 *
	 * @return the customer
	 */
	public ClientCustomerDVO getCustomer() {
		return customer;
	}

	/**
	 * Gets the customer profile pic web view.
	 *
	 * @return the customer profile pic web view
	 */
	public String getCustomerProfilePicWebView() {
		if (customerProfilePicWebView == null) {
			customerProfilePicWebView = "";
		}
		return customerProfilePicWebView;
	}

	/**
	 * Gets the error.
	 *
	 * @return the error
	 */
	public Error getError() {
		return error;
	}

	/**
	 * Gets the free tickets per winner.
	 *
	 * @return the free tickets per winner
	 */
	public Integer getFreeTicketsPerWinner() {
		if (freeTicketsPerWinner == null) {
			freeTicketsPerWinner = 0;
		}
		return freeTicketsPerWinner;
	}

	/**
	 * Gets the grand price text.
	 *
	 * @return the grand price text
	 */
	public String getGrandPriceText() {
		if (grandPriceText == null) {
			grandPriceText = "";
		}
		return grandPriceText;
	}

	/**
	 * Gets the grand winer expiry text.
	 *
	 * @return the grand winer expiry text
	 */
	public String getGrandWinerExpiryText() {
		if (grandWinerExpiryText == null) {
			grandWinerExpiryText = "";
		}
		return grandWinerExpiryText;
	}

	/**
	 * Gets the checks if is contest started.
	 *
	 * @return the checks if is contest started
	 */

	public Boolean getIsContestStarted() {
		if (isContestStarted == null) {
			isContestStarted = false;
		}
		return isContestStarted;
	}

	/**
	 * Gets the checks if is existing contestant.
	 *
	 * @return the checks if is existing contestant
	 */
	public Boolean getIsExistingContestant() {
		if (isExistingContestant == null) {
			isExistingContestant = false;
		}
		return isExistingContestant;
	}

	/**
	 * Gets the join button label.
	 *
	 * @return the join button label
	 */
	public String getJoinButtonLabel() {
		if (joinButtonLabel == null) {
			joinButtonLabel = "";
		}
		return joinButtonLabel;
	}

	/**
	 * Gets the join button popup msg.
	 *
	 * @return the join button popup msg
	 */
	public String getJoinButtonPopupMsg() {
		if (joinButtonPopupMsg == null) {
			joinButtonPopupMsg = "";
		}
		return joinButtonPopupMsg;
	}

	/**
	 * Gets the max free tix winners.
	 *
	 * @return the max free tix winners
	 */
	public Integer getMaxFreeTixWinners() {
		if (maxFreeTixWinners == null) {
			maxFreeTixWinners = 0;
		}
		return maxFreeTixWinners;
	}

	/**
	 * Gets the message.
	 *
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * Gets the pending requests.
	 *
	 * @return the pending requests
	 */
	public Integer getPendingRequests() {
		if (pendingRequests == null) {
			pendingRequests = 0;
		}
		return pendingRequests;
	}

	/**
	 * Gets the show join button.
	 *
	 * @return the show join button
	 */
	public Boolean getShowJoinButton() {
		if (showJoinButton == null) {
			showJoinButton = false;
		}
		return showJoinButton;
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public Integer getStatus() {
		return status;
	}

	/**
	 * Gets the total active rewards.
	 *
	 * @return the total active rewards
	 */
	public Double getTotalActiveRewards() {
		if (totalActiveRewards == null) {
			totalActiveRewards = 0.0;
		}
		return totalActiveRewards;
	}

	/**
	 * Gets the video source url.
	 *
	 * @return the video source url
	 */
	public String getVideoSourceUrl() {
		if (videoSourceUrl == null) {
			videoSourceUrl = "";
		}
		return videoSourceUrl;
	}

	/**
	 * Sets the contest id.
	 *
	 * @param contestId the new contest id
	 */
	public void setContestId(Integer contestId) {
		ContestId = contestId;
	}

	/**
	 * Sets the contest list.
	 *
	 * @param contestList the new contest list
	 */
	public void setContestList(List<QuizContest> contestList) {
		this.contestList = contestList;
	}

	/**
	 * Sets the contest name.
	 *
	 * @param contestName the new contest name
	 */
	public void setContestName(String contestName) {
		this.contestName = contestName;
	}

	/**
	 * Sets the contest price text.
	 *
	 * @param contestPriceText the new contest price text
	 */
	public void setContestPriceText(String contestPriceText) {
		this.contestPriceText = contestPriceText;
	}

	/**
	 * Sets the contest start date.
	 *
	 * @param contestStartDate the new contest start date
	 */
	public void setContestStartDate(String contestStartDate) {
		this.contestStartDate = contestStartDate;
	}

	/**
	 * Sets the contest start time.
	 *
	 * @param contestStartTime the new contest start time
	 */
	public void setContestStartTime(String contestStartTime) {
		this.contestStartTime = contestStartTime;
	}

	/**
	 * Sets the contest total rewards.
	 *
	 * @param contestTotalRewards the new contest total rewards
	 */
	public void setContestTotalRewards(Double contestTotalRewards) {
		this.contestTotalRewards = contestTotalRewards;
	}

	/**
	 * Sets the c promo codes count.
	 *
	 * @param cPromoCodesCount the new c promo codes count
	 */
	public void setcPromoCodesCount(Integer cPromoCodesCount) {
		this.cPromoCodesCount = cPromoCodesCount;
	}

	/**
	 * Sets the customer.
	 *
	 * @param customer the new customer
	 */
	public void setCustomer(ClientCustomerDVO customer) {
		this.customer = customer;
	}

	/**
	 * Sets the customer profile pic web view.
	 *
	 * @param customerProfilePicWebView the new customer profile pic web view
	 */
	public void setCustomerProfilePicWebView(String customerProfilePicWebView) {
		this.customerProfilePicWebView = customerProfilePicWebView;
	}

	/**
	 * Sets the error.
	 *
	 * @param error the new error
	 */
	public void setError(Error error) {
		this.error = error;
	}

	/**
	 * Sets the free tickets per winner.
	 *
	 * @param freeTicketsPerWinner the new free tickets per winner
	 */
	public void setFreeTicketsPerWinner(Integer freeTicketsPerWinner) {
		this.freeTicketsPerWinner = freeTicketsPerWinner;
	}

	/**
	 * Sets the grand price text.
	 *
	 * @param grandPriceText the new grand price text
	 */
	public void setGrandPriceText(String grandPriceText) {
		this.grandPriceText = grandPriceText;
	}

	/**
	 * Sets the grand winer expiry text.
	 *
	 * @param grandWinerExpiryText the new grand winer expiry text
	 */
	public void setGrandWinerExpiryText(String grandWinerExpiryText) {
		this.grandWinerExpiryText = grandWinerExpiryText;
	}

	/**
	 * Sets the checks if is contest started.
	 *
	 * @param isContestStarted the new checks if is contest started
	 */
	public void setIsContestStarted(Boolean isContestStarted) {
		this.isContestStarted = isContestStarted;
	}

	/**
	 * Sets the checks if is existing contestant.
	 *
	 * @param isExistingContestant the new checks if is existing contestant
	 */
	public void setIsExistingContestant(Boolean isExistingContestant) {
		this.isExistingContestant = isExistingContestant;
	}

	/**
	 * Sets the join button label.
	 *
	 * @param joinButtonLabel the new join button label
	 */
	public void setJoinButtonLabel(String joinButtonLabel) {
		this.joinButtonLabel = joinButtonLabel;
	}

	/**
	 * Sets the join button popup msg.
	 *
	 * @param joinButtonPopupMsg the new join button popup msg
	 */
	public void setJoinButtonPopupMsg(String joinButtonPopupMsg) {
		this.joinButtonPopupMsg = joinButtonPopupMsg;
	}

	/**
	 * Sets the max free tix winners.
	 *
	 * @param maxFreeTixWinners the new max free tix winners
	 */
	public void setMaxFreeTixWinners(Integer maxFreeTixWinners) {
		this.maxFreeTixWinners = maxFreeTixWinners;
	}

	/**
	 * Sets the message.
	 *
	 * @param message the new message
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * Sets the pending requests.
	 *
	 * @param pendingRequests the new pending requests
	 */
	public void setPendingRequests(Integer pendingRequests) {
		this.pendingRequests = pendingRequests;
	}

	/**
	 * Sets the show join button.
	 *
	 * @param showJoinButton the new show join button
	 */
	public void setShowJoinButton(Boolean showJoinButton) {
		this.showJoinButton = showJoinButton;
	}

	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	/**
	 * Sets the total active rewards.
	 *
	 * @param totalActiveRewards the new total active rewards
	 */
	public void setTotalActiveRewards(Double totalActiveRewards) {
		this.totalActiveRewards = totalActiveRewards;
	}

	/**
	 * Sets the video source url.
	 *
	 * @param videoSourceUrl the new video source url
	 */
	public void setVideoSourceUrl(String videoSourceUrl) {
		this.videoSourceUrl = videoSourceUrl;
	}

}
