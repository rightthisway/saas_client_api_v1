/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.sql.dvo;

import java.io.Serializable;

/**
 * The Class RtfConfigContestClusterNodes.
 */
public class RtfConfigContestClusterNodes implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 9092548455988075566L;

	/** The id. */
	private Integer id;

	/** The url. */
	private String url;

	/** The status. */
	private Integer status;

	/** The directory path. */
	private String directoryPath;

	/** The clint id. */
	private String clintId;

	/** The shared id. */
	private String sharedId;

	/** The is primary node. */
	private Boolean isPrimaryNode;

	/**
	 * Instantiates a new rtf config contest cluster nodes.
	 */
	public RtfConfigContestClusterNodes() {
	}

	/**
	 * Instantiates a new rtf config contest cluster nodes.
	 *
	 * @param id            the id
	 * @param url           the url
	 * @param status        the status
	 * @param directoryPath the directory path
	 */
	public RtfConfigContestClusterNodes(Integer id, String url, Integer status, String directoryPath) {
		this.id = id;
		this.url = url;
		this.status = status;
		this.directoryPath = directoryPath;
	}

	/**
	 * Gets the clint id.
	 *
	 * @return the clint id
	 */
	public String getClintId() {
		return clintId;
	}

	/**
	 * Gets the directory path.
	 *
	 * @return the directory path
	 */
	public String getDirectoryPath() {
		return directoryPath;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * Gets the checks if is primary node.
	 *
	 * @return the checks if is primary node
	 */
	public Boolean getIsPrimaryNode() {
		return isPrimaryNode;
	}

	/**
	 * Gets the shared id.
	 *
	 * @return the shared id
	 */
	public String getSharedId() {
		return sharedId;
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public Integer getStatus() {
		return status;
	}

	/**
	 * Gets the url.
	 *
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * Sets the clint id.
	 *
	 * @param clintId the new clint id
	 */
	public void setClintId(String clintId) {
		this.clintId = clintId;
	}

	/**
	 * Sets the directory path.
	 *
	 * @param directoryPath the new directory path
	 */
	public void setDirectoryPath(String directoryPath) {
		this.directoryPath = directoryPath;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * Sets the checks if is primary node.
	 *
	 * @param isPrimaryNode the new checks if is primary node
	 */
	public void setIsPrimaryNode(Boolean isPrimaryNode) {
		this.isPrimaryNode = isPrimaryNode;
	}

	/**
	 * Sets the shared id.
	 *
	 * @param sharedId the new shared id
	 */
	public void setSharedId(String sharedId) {
		this.sharedId = sharedId;
	}

	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	/**
	 * Sets the url.
	 *
	 * @param url the new url
	 */
	public void setUrl(String url) {
		this.url = url;
	}

}