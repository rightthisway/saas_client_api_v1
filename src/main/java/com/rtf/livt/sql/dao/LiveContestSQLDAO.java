/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.sql.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.rtf.common.dspl.dvo.LiveContestDVO;
import com.rtf.common.util.DatabaseConnections;
import com.rtf.livt.dvo.ContestDVO;

/**
 * The Class LiveContestSQLDAO.
 */
public class LiveContestSQLDAO {

	/**
	 * Gets the active contest cards.
	 *
	 * @param clId the Client ID
	 * @return the active contest cards
	 * @throws Exception the exception
	 */
	public static List<ContestDVO> getActiveContestCards(String clId) throws Exception {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<ContestDVO> list = new ArrayList<ContestDVO>();

		StringBuffer sb = new StringBuffer("SELECT c.clintid AS clintid, c.conid AS conid ");
		sb.append(", c.cattype AS cattype, c.subcattype AS subcattype ");
		sb.append(", c.conname AS conname, c.consrtdate AS consrtdate ");
		sb.append(", c.contype AS contype, c.anstype AS anstype ");
		sb.append(", c.iselimtype AS iselimtype, c.noofqns AS noofqns ");
		sb.append(", c.issumrysplitable AS issumrysplitable, c.sumryrwdtype AS sumryrwdtype ");
		sb.append(", c.sumryrwdval AS sumryrwdval, c.islotryenabled AS islotryenabled ");
		sb.append(", c.lotryrwdtype AS lotryrwdtype, c.lotryrwdval AS lotryrwdval ");
		sb.append(", c.isconactive AS isconactive, c.brndimgurl AS brndimgurl ");
		sb.append(", c.cardimgurl AS cardimgurl, c.cardseqno AS cardseqno ");
		sb.append(", c.bgthemcolor AS bgthemcolor, c.bgthemimgurl AS bgthemimgurl ");
		sb.append(", c.bgthemimgurlmob AS bgthemimgurlmob, c.bgthembankid AS bgthembankid ");
		sb.append(", c.playimgurl AS playimgurl, c.creby AS creby " + ", c.credate AS credate, c.updby AS updby ");
		sb.append(", c.upddate AS upddate, c.qsnrwdtype AS qsnrwdtype ");
		sb.append(", c.participantrwdtype AS participantrwdtype, c.participantrwdval AS participantrwdval ");
		sb.append(", c.migrstatus AS migrstatus, c.lastqsn AS lastqsn, c.extconname as extconname,c.hostname as hostName,c.hostimg as hostImg  ");
		sb.append(", c.lastaction AS lastaction, s.isconactivetext AS status, c.ngrndwnrs as ngrndwnrs ");
		sb.append("FROM pa_livvx_contest_mstr c join sd_contest_isactive s on c.isconactive=s.isconactive ");
		sb.append(
				"WHERE c.clintid = ? AND c.isconactive in (1,2)  AND (c.consrtdate > getDate() OR  c.isconactive =2) ORDER BY c.consrtdate");
		try {
			conn = DatabaseConnections.getRtfSaasConnection();
			ps = conn.prepareStatement(sb.toString());
			ps.setString(1, clId);
			rs = ps.executeQuery();

			while (rs.next()) {
				if (list.size() >= 2) {
					continue;
				}
				ContestDVO contestDVO = new ContestDVO();
				Date date = null;
				contestDVO.setCoId(rs.getString("conid"));
				contestDVO.setClId(rs.getString("clintid"));
				contestDVO.setAnsType(rs.getString("anstype"));
				contestDVO.setCoType(rs.getString("contype"));
				contestDVO.setClImgU(rs.getString("brndimgurl"));
				contestDVO.setImgU(rs.getString("cardimgurl"));
				contestDVO.setSeqNo(rs.getInt("cardseqno"));
				contestDVO.setCat(rs.getString("cattype"));
				contestDVO.setName(rs.getString("conname"));
				date = rs.getTimestamp("consrtdate");
				contestDVO.setStDate(date != null ? date.getTime() : null);
				contestDVO.setCrBy(rs.getString("creby"));
				date = rs.getTimestamp("credate");
				contestDVO.setCrDate(date != null ? date.getTime() : null);
				contestDVO.setIsAct(rs.getInt("isconactive"));
				contestDVO.setStatus(rs.getString("status"));
				contestDVO.setIsElimination(rs.getBoolean("iselimtype"));
				contestDVO.setqSize(rs.getInt("noofqns"));
				contestDVO.setSubCat(rs.getString("subcattype"));
				contestDVO.setUpBy(rs.getString("updby"));
				contestDVO.setThmColor(rs.getString("bgthemcolor"));
				contestDVO.setThmImgDesk(rs.getString("bgthemimgurl"));
				contestDVO.setThmImgMob(rs.getString("bgthemimgurlmob"));
				contestDVO.setPlayGameImg(rs.getString("playimgurl"));
				contestDVO.setThmId(rs.getInt("bgthembankid"));
				date = rs.getTimestamp("upddate");
				contestDVO.setUpDate(date != null ? date.getTime() : null);
				contestDVO.setIsSplitSummary(rs.getBoolean("issumrysplitable"));
				contestDVO.setIsLotEnbl(rs.getBoolean("islotryenabled"));
				contestDVO.setSumRwdType(rs.getString("sumryrwdtype"));
				contestDVO.setSumRwdVal(rs.getDouble("sumryrwdval"));
				contestDVO.setWinRwdType(rs.getString("lotryrwdtype"));
				contestDVO.setWinRwdVal(rs.getDouble("lotryrwdval"));
				contestDVO.setQueRwdType(rs.getString("qsnrwdtype"));
				contestDVO.setPartiRwdType(rs.getString("participantrwdtype"));
				contestDVO.setPartiRwdVal(rs.getDouble("participantrwdval"));
				contestDVO.setLastAction(rs.getString("lastaction"));
				contestDVO.setLastQue(rs.getInt("lastqsn"));
				contestDVO.setMigStatus(rs.getString("migrstatus"));
				contestDVO.setExtName(rs.getString("extconname"));
				contestDVO.setWinnerCount(rs.getInt("ngrndwnrs"));
				contestDVO.setHostedBy(rs.getString("hostName"));
				contestDVO.setHstImgUrl(rs.getString("hostImg"));
				list.add(contestDVO);
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return list;
	}

	/**
	 * Gets the all active contest cards.
	 *
	 * @param clId the Client ID
	 * @return the all active contest cards
	 * @throws Exception the exception
	 */
	public static List<LiveContestDVO> getAllActiveContestCards(String clId) throws Exception {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<LiveContestDVO> list = new ArrayList<LiveContestDVO>();
		StringBuffer sb = new StringBuffer(" SELECT conid AS conid  , conname AS conname, consrtdate AS consrtdate " ) ; 
		sb.append( ", extconname as extconname FROM pa_livvx_contest_mstr  " ) ; 
		sb.append( "WHERE clintid = ? AND isconactive in (1,2)  AND (consrtdate > getDate() OR  isconactive =2) ORDER BY consrtdate" ) ;
		try {
			conn = DatabaseConnections.getRtfSaasConnection();
			ps = conn.prepareStatement(sb.toString());
			ps.setString(1, clId);
			rs = ps.executeQuery();

			while (rs.next()) {
				if (list.size() >= 5) {
					break;
				}
				LiveContestDVO contestDVO = new LiveContestDVO();
				Date date = null;
				contestDVO.setCoId(rs.getString("conid"));
				contestDVO.setName(rs.getString("conname"));
				date = rs.getTimestamp("consrtdate");
				contestDVO.setStDate(date != null ? date.getTime() : null);
				contestDVO.setExtName(rs.getString("extconname"));
				list.add(contestDVO);
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return list;
	}

	/**
	 * Gets the contest by contest id.
	 *
	 * @param clientId  the client id
	 * @param contestId the contest id
	 * @return the contest by contest id
	 * @throws Exception the exception
	 */
	public static ContestDVO getContestByContestId(String clientId, String contestId) throws Exception {
		ContestDVO contestDVO = null;
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sql = "SELECT *  from  pa_livvx_contest_mstr  WHERE clintid = ? AND conid = ? ";
		try {
			conn = DatabaseConnections.getRtfSaasConnection();
			ps = conn.prepareStatement(sql);
			ps.setString(1, clientId);
			ps.setString(2, contestId);
			rs = ps.executeQuery();

			while (rs.next()) {
				contestDVO = new ContestDVO();
				Date date = null;
				contestDVO.setCoId(rs.getString("conid"));
				contestDVO.setClId(rs.getString("clintid"));
				contestDVO.setAnsType(rs.getString("anstype"));
				contestDVO.setCoType(rs.getString("contype"));
				contestDVO.setSeqNo(rs.getInt("cardseqno"));
				contestDVO.setName(rs.getString("conname"));
				date = rs.getTimestamp("consrtdate");
				contestDVO.setStDate(date != null ? date.getTime() : null);
				contestDVO.setIsAct(rs.getInt("isconactive"));
				contestDVO.setIsElimination(rs.getBoolean("iselimtype"));
				contestDVO.setqSize(rs.getInt("noofqns"));
				contestDVO.setIsSplitSummary(rs.getBoolean("issumrysplitable"));
				contestDVO.setIsLotEnbl(rs.getBoolean("islotryenabled"));
				contestDVO.setSumRwdType(rs.getString("sumryrwdtype"));
				contestDVO.setSumRwdVal(rs.getDouble("sumryrwdval"));
				contestDVO.setWinRwdType(rs.getString("lotryrwdtype"));
				contestDVO.setWinRwdVal(rs.getDouble("lotryrwdval"));
				contestDVO.setQueRwdType(rs.getString("qsnrwdtype"));
				contestDVO.setPartiRwdType(rs.getString("participantrwdtype"));
				contestDVO.setPartiRwdVal(rs.getDouble("participantrwdval"));
				contestDVO.setLastAction(rs.getString("lastaction"));
				contestDVO.setLastQue(rs.getInt("lastqsn"));
				contestDVO.setMigStatus(rs.getString("migrstatus"));
				contestDVO.setExtName(rs.getString("extconname"));
				contestDVO.setWinnerCount(rs.getInt("ngrndwnrs"));
				contestDVO.setContRunningNo(rs.getInt("conrunningno"));

			}
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return contestDVO;
	}

	/**
	 * Gets the live contest.
	 *
	 * @param clientId the client id
	 * @return the live contest
	 * @throws Exception the exception
	 */
	public static ContestDVO getLiveContest(String clientId) throws Exception {
		ContestDVO contestDVO = null;
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sql = "SELECT * from  pa_livvx_contest_mstr  WHERE clintid = ? AND isconactive = ? ";
		try {
			conn = DatabaseConnections.getRtfSaasConnection();
			ps = conn.prepareStatement(sql);
			ps.setString(1, clientId);
			ps.setInt(2, 2);
			rs = ps.executeQuery();

			while (rs.next()) {
				contestDVO = new ContestDVO();
				Date date = null;
				contestDVO.setCoId(rs.getString("conid"));
				contestDVO.setClId(rs.getString("clintid"));
				contestDVO.setAnsType(rs.getString("anstype"));
				contestDVO.setCoType(rs.getString("contype"));
				contestDVO.setSeqNo(rs.getInt("cardseqno"));
				contestDVO.setName(rs.getString("conname"));
				date = rs.getTimestamp("consrtdate");
				contestDVO.setStDate(date != null ? date.getTime() : null);
				contestDVO.setIsAct(rs.getInt("isconactive"));
				contestDVO.setIsElimination(rs.getBoolean("iselimtype"));
				contestDVO.setqSize(rs.getInt("noofqns"));
				contestDVO.setIsSplitSummary(rs.getBoolean("issumrysplitable"));
				contestDVO.setIsLotEnbl(rs.getBoolean("islotryenabled"));
				contestDVO.setSumRwdType(rs.getString("sumryrwdtype"));
				contestDVO.setSumRwdVal(rs.getDouble("sumryrwdval"));
				contestDVO.setWinRwdType(rs.getString("lotryrwdtype"));
				contestDVO.setWinRwdVal(rs.getDouble("lotryrwdval"));
				contestDVO.setQueRwdType(rs.getString("qsnrwdtype"));
				contestDVO.setPartiRwdType(rs.getString("participantrwdtype"));
				contestDVO.setPartiRwdVal(rs.getDouble("participantrwdval"));
				contestDVO.setLastAction(rs.getString("lastaction"));
				contestDVO.setLastQue(rs.getInt("lastqsn"));
				contestDVO.setMigStatus(rs.getString("migrstatus"));
				contestDVO.setExtName(rs.getString("extconname"));
				contestDVO.setWinnerCount(rs.getInt("ngrndwnrs"));
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return contestDVO;
	}

}
