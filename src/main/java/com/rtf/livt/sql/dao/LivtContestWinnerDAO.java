/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.sql.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.rtf.common.util.DatabaseConnections;
import com.rtf.livt.dvo.ContestWinnerDVO;

/**
 * The Class LivtContestWinnerDAO.
 */
public class LivtContestWinnerDAO {

	/**
	 * Gets the all winners by contest id.
	 *
	 * @param clientId  the client id
	 * @param contestId the contest id
	 * @return the all winners by co id
	 * @throws Exception the exception
	 */
	public static List<ContestWinnerDVO> getAllWinnersByCoId(String clientId, String contestId) throws Exception {
		List<ContestWinnerDVO> winners = new ArrayList<ContestWinnerDVO>();
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sql = "SELECT * "
				+ "from  pt_livvx_contest_winner  WHERE clintid = ? AND conid = ?  AND migrate=? ORDER BY custid";
		try {
			conn = DatabaseConnections.getRtfSaasConnection();
			ps = conn.prepareStatement(sql);
			ps.setString(1, clientId);
			ps.setString(2, contestId);
			ps.setBoolean(3, false);
			rs = ps.executeQuery();

			while (rs.next()) {
				winners.add(new ContestWinnerDVO(rs.getString("clintid"), rs.getString("conid"), rs.getString("custid"),
						rs.getString("rwdtype"), rs.getDouble("rwdval")));
			}

		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return winners;
	}

	/**
	 * Gets the contest winners customer id by contest id.
	 *
	 * @param clientId  the client id
	 * @param contestId the contest id
	 * @return the contest winners customer id by contest id
	 */
	public static List<String> getContestWinnersCustomerIdByContestId(String clientId, String contestId) {
		List<String> winners = new ArrayList<String>();
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sql = "SELECT custid " + "from  pt_livvx_contest_winner  WHERE clintid = ? AND conid = ? ";
		try {
			conn = DatabaseConnections.getRtfSaasConnection();
			ps = conn.prepareStatement(sql);
			ps.setString(1, clientId);
			ps.setString(2, contestId);
			rs = ps.executeQuery();

			while (rs.next()) {
				winners.add(rs.getString("custid"));
			}

		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return winners;
	}

	/**
	 * Update winner migration status.
	 *
	 * @param clId   the client id
	 * @param coId   the contest id
	 * @param custId the customer id
	 * @return true, if successful
	 */
	public static boolean updateWinnerMigrationStatus(String clId, String coId, String custId) {
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			String sqlQuery = "UPDATE pt_livvx_contest_winner SET migrate=? WHERE clintid=? AND conid=? AND custid=?";
			conn = DatabaseConnections.getRtfSaasConnection();
			ps = conn.prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS);
			ps.setBoolean(1, true);
			ps.setString(2, clId);
			ps.setString(3, coId);
			ps.setString(4, custId);
			int affectedRows = ps.executeUpdate();
			if (affectedRows > 0) {
				return true;
			}
			return false;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return false;
	}
}
