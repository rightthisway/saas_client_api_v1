/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.sql.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.rtf.common.util.DatabaseConnections;
import com.rtf.livt.sql.dvo.QuizContest;
import com.rtf.livt.sql.dvo.QuizContestQuestions;
import com.rtf.livt.sql.dvo.RtfConfigContestClusterNodes;

/**
 * The Class SQLDaoUtil.
 */
public class SQLDaoUtil {

	/**
	 * Gets the all rtf config cluster node details.
	 *
	 * @return the RtfConfigContestClusterNodes  rtf config cluster node details
	 */
	public static List<RtfConfigContestClusterNodes> getAllRtfConfigClusterNodeDetails() {

		String sql = " select * from pc_livvx_contest_cluster_nodes_conf order by id";
		List<RtfConfigContestClusterNodes> rtfClusterNodeList = new ArrayList<RtfConfigContestClusterNodes>();
		try {

			Connection conn = DatabaseConnections.getRtfSaasConnection();
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				RtfConfigContestClusterNodes clusterNode = new RtfConfigContestClusterNodes();
				clusterNode.setId(rs.getInt("id"));
				clusterNode.setClintId(rs.getString("clintid"));
				clusterNode.setUrl(rs.getString("url"));
				clusterNode.setStatus(rs.getInt("status"));
				clusterNode.setDirectoryPath(rs.getString("directory_path"));
				clusterNode.setIsPrimaryNode(rs.getBoolean("isprimarynode"));
				rtfClusterNodeList.add(clusterNode);
			}
			rs.close();
			stmt.close();
			DatabaseConnections.closeConnection(conn);
			return rtfClusterNodeList;
		} catch (Exception e) {

			e.printStackTrace();
		}
		return rtfClusterNodeList;
	}

	/**
	 * Gets the all rtf config cluster node details for budweiser.
	 *
	 * @return the RtfConfigContestClusterNodes the  config cluster node details for budweiser
	 */
	public static List<RtfConfigContestClusterNodes> getAllRtfConfigClusterNodeDetailsBudweiser() {

		String sql = " select * from pc_livvx_contest_cluster_nodes_conf_budweiser where status=1 order by id";
		List<RtfConfigContestClusterNodes> rtfClusterNodeList = new ArrayList<RtfConfigContestClusterNodes>();
		try {

			Connection conn = DatabaseConnections.getRtfSaasConnection();
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				RtfConfigContestClusterNodes clusterNode = new RtfConfigContestClusterNodes();
				clusterNode.setId(rs.getInt("id"));
				clusterNode.setClintId(rs.getString("clintid"));
				clusterNode.setUrl(rs.getString("url"));
				clusterNode.setStatus(rs.getInt("status"));
				clusterNode.setDirectoryPath(rs.getString("directory_path"));
				clusterNode.setIsPrimaryNode(rs.getBoolean("isprimarynode"));
				clusterNode.setSharedId(rs.getString("srdid"));
				rtfClusterNodeList.add(clusterNode);
			}
			rs.close();
			stmt.close();
			DatabaseConnections.closeConnection(conn);
			return rtfClusterNodeList;
		} catch (Exception e) {

			e.printStackTrace();
		}
		return rtfClusterNodeList;
	}

	/**
	 * Gets the contest questions.
	 *
	 * @param contestId the contest id
	 * @return the contest questions
	 */
	public static List<QuizContestQuestions> getContestQuestions(String contestId) {

		String sql = "select * from liv_contest_questions where contest_id  = " + contestId
				+ " order by question_sl_no";
		QuizContestQuestions qzqs = null;
		List<QuizContestQuestions> qzqsList = new ArrayList<QuizContestQuestions>();
		try {
			Connection conn = DatabaseConnections.getRtfSaasConnection();
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			qzqs = new QuizContestQuestions();
			while (rs.next()) {
				qzqs = new QuizContestQuestions();
				qzqs.setClId(rs.getString("clintid"));
				qzqs.setId(rs.getInt("id"));
				qzqs.setContestId(rs.getInt("contest_id"));
				qzqs.setQuestionSNo(rs.getInt("question_sl_no"));

				qzqs.setQuestion(rs.getString("question"));
				qzqs.setOptionA(rs.getString("option_a"));
				qzqs.setOptionB(rs.getString("option_b"));
				qzqs.setOptionC(rs.getString("option_c"));
				qzqs.setAnswer(rs.getString("answer"));

				qzqs.setCreatedDateTime(rs.getDate("created_datetime"));
				qzqs.setUpdatedDateTime(rs.getDate("updated_datetime"));
				rs.getString("created_by");
				rs.getString("updated_by");
				rs.getString("temp_option");
				qzqs.setQuestionRewards(rs.getDouble("question_reward"));
				qzqs.setNoOfJackpotWinners(rs.getInt("no_of_winner"));
				qzqsList.add(qzqs);

			}

			rs.close();
			stmt.close();
			DatabaseConnections.closeConnection(conn);
		} catch (SQLException se) {

			se.printStackTrace();
		} catch (Exception e) {

			e.printStackTrace();
		}

		return qzqsList;

	}

	/**
	 * Gets the current started contest.
	 *
	 * @param contestType the contest type
	 * @return the current started contest
	 */
	public static QuizContest getCurrentStartedContest(String contestType) {

		String sql = "select * from liv_Contest where status ='STARTED' and contest_type ='" + contestType + "'"
				+ "  order by contest_start_datetime ";
		QuizContest qz = null;
		try {
			Connection conn = DatabaseConnections.getRtfSaasConnection();
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			qz = new QuizContest();
			while (rs.next()) {

				qz.setId(rs.getInt("id"));
				qz.setContestName(rs.getString("contest_name"));
				qz.setContestStartDateTime(rs.getDate("contest_start_datetime"));
				qz.setParticipantsCount(rs.getInt("participants_count"));
				qz.setWinnersCount(rs.getInt("winners_count"));
				qz.setTicketWinnersCount(rs.getInt("ticket_winners_count"));
				qz.setPointWinnersCount(rs.getInt("point_winners_count"));
				qz.setStatus(rs.getString("status"));
				qz.setCreatedDateTime(rs.getDate("created_datetime"));
				qz.setUpdatedDateTime(rs.getDate("updated_datetime"));
				qz.setCreatedBy(rs.getString("created_by"));
				rs.getString("updated_by");
				qz.setMaxFreeTicketWinners(rs.getInt("eligible_free_ticket_winners"));
				qz.setFreeTicketsPerWinner(rs.getInt("free_tickets_per_winner"));
				qz.setPointsPerWinner(rs.getDouble("points_per_winner"));
				qz.setNoOfQuestions(rs.getInt("no_of_questions"));
				qz.setContestType(rs.getString("contest_type"));
				qz.setTotalRewards(rs.getDouble("total_rewards"));
				qz.setContestMode(rs.getString("contest_mode"));
				qz.setContestPwd(rs.getString("contest_password"));
				qz.setRewardsPerQuestion(rs.getDouble("rewards_per_question"));
				qz.setZone(rs.getString("zone"));

				qz.setPromoCode(rs.getString("promotional_code"));
				qz.setDiscountPercentage(rs.getDouble("discount_percentage"));
				qz.setPromoRefId(rs.getInt("promo_ref_id"));
				qz.setPromoRefName(rs.getString("promo_ref_name"));
				qz.setPromoExpiryDate(rs.getDate("promo_expiry_date"));
				qz.setPromoRefType(rs.getString("promo_ref_type"));
				qz.setLastQuestionNo(rs.getInt("last_question_no"));
				qz.setLastAction(rs.getString("last_action"));
				qz.setSingleTicketPrice(rs.getDouble("single_tix_price"));
				qz.setExtendedName(rs.getString("extended_name"));
			}

			rs.close();
			stmt.close();
			DatabaseConnections.closeConnection(conn);
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return qz;

	}

	/**
	 * Gets the quiz contest for id.
	 *
	 * @param contestId the contest id
	 * @return the quiz contest for id
	 */
	public static QuizContest getQuizContestForId(String contestId) {

		String sql = "select * from liv_contest where id  = " + contestId;
		QuizContest qz = null;
		try {
			Connection conn = DatabaseConnections.getRtfSaasConnection();
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				qz = new QuizContest();
				qz.setClId(rs.getString("clintid"));
				qz.setId(rs.getInt("id"));
				qz.setContestName(rs.getString("contest_name"));
				qz.setContestStartDateTime(rs.getDate("contest_start_datetime"));
				qz.setParticipantsCount(rs.getInt("participants_count"));
				qz.setWinnersCount(rs.getInt("winners_count"));
				qz.setTicketWinnersCount(rs.getInt("ticket_winners_count"));
				qz.setPointWinnersCount(rs.getInt("point_winners_count"));
				qz.setStatus(rs.getString("status"));
				qz.setCreatedDateTime(rs.getDate("created_datetime"));
				qz.setUpdatedDateTime(rs.getDate("updated_datetime"));
				qz.setCreatedBy(rs.getString("created_by"));
				rs.getString("updated_by");
				qz.setMaxFreeTicketWinners(rs.getInt("eligible_free_ticket_winners"));
				qz.setFreeTicketsPerWinner(rs.getInt("free_tickets_per_winner"));
				qz.setPointsPerWinner(rs.getDouble("points_per_winner"));
				qz.setNoOfQuestions(rs.getInt("no_of_questions"));
				qz.setContestType(rs.getString("contest_type"));
				qz.setTotalRewards(rs.getDouble("total_rewards"));
				qz.setContestMode(rs.getString("contest_mode"));
				qz.setContestPwd(rs.getString("contest_password"));
				qz.setRewardsPerQuestion(rs.getDouble("rewards_per_question"));
				qz.setZone(rs.getString("zone"));
				qz.setPromoCode(rs.getString("promotional_code"));
				qz.setDiscountPercentage(rs.getDouble("discount_percentage"));
				qz.setPromoRefId(rs.getInt("promo_ref_id"));
				qz.setPromoRefName(rs.getString("promo_ref_name"));
				qz.setPromoExpiryDate(rs.getDate("promo_expiry_date"));
				qz.setPromoRefType(rs.getString("promo_ref_type"));
				qz.setLastQuestionNo(rs.getInt("last_question_no"));
				qz.setLastAction(rs.getString("last_action"));
				qz.setSingleTicketPrice(rs.getDouble("single_tix_price"));
				qz.setExtendedName(rs.getString("extended_name"));
			}

			rs.close();
			stmt.close();
			DatabaseConnections.closeConnection(conn);

		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return qz;

	}
	
	
	
	public static List<RtfConfigContestClusterNodes> getAllRtfConfigClusterNodeDetail() {

		String sql = " select * from pc_livvx_contest_cluster_nodes_conf where status= 1 order by id";
		List<RtfConfigContestClusterNodes> rtfClusterNodeList = new ArrayList<RtfConfigContestClusterNodes>();
		try {

			Connection conn = DatabaseConnections.getRtfSaasConnection();
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				RtfConfigContestClusterNodes clusterNode = new RtfConfigContestClusterNodes();
				clusterNode.setId(rs.getInt("id"));
				clusterNode.setClintId(rs.getString("clintid"));
				clusterNode.setUrl(rs.getString("url"));
				clusterNode.setStatus(rs.getInt("status"));
				clusterNode.setDirectoryPath(rs.getString("directory_path"));
				clusterNode.setIsPrimaryNode(rs.getBoolean("isprimarynode"));
			    clusterNode.setSharedId(rs.getString("nodeid"));
				rtfClusterNodeList.add(clusterNode);
			}
			rs.close();
			stmt.close();
			DatabaseConnections.closeConnection(conn);
			return rtfClusterNodeList;
		} catch (Exception e) {

			e.printStackTrace();
		}
		return rtfClusterNodeList;
	}
	
	public static RtfConfigContestClusterNodes getRtfConfigClusterNodeDetailforPortnIP(String ipPort) {

		String sql = " select  top 1 *  from pc_livvx_contest_cluster_nodes_conf with (nolock) where directory_path='"+ipPort + "' and  status= 1 order by id";
		RtfConfigContestClusterNodes clusterNode = null;
		try {
			Connection conn = DatabaseConnections.getRtfSaasConnection();
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				clusterNode = new RtfConfigContestClusterNodes();
				clusterNode.setId(rs.getInt("id"));
				clusterNode.setClintId(rs.getString("clintid"));
				clusterNode.setUrl(rs.getString("url"));
				clusterNode.setStatus(rs.getInt("status"));
				clusterNode.setDirectoryPath(rs.getString("directory_path"));
				clusterNode.setIsPrimaryNode(rs.getBoolean("isprimarynode"));
			    clusterNode.setSharedId(rs.getString("nodeid"));
			}
			rs.close();
			stmt.close();
			DatabaseConnections.closeConnection(conn);
			return clusterNode;
		} catch (Exception e) {

			e.printStackTrace();
		}
		return clusterNode;
	}
	public static void migrateLiveContestDataToReportDB(String cliId, String coId, int contrunNo) throws Exception {

		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;	
		try {
			String SPsql = "exec spImportLiveContestData  ?,?,?"; 
			conn = DatabaseConnections.getReportDBConnection();
			stmt = conn.prepareStatement(SPsql);
			stmt.setString(1, cliId);
			stmt.setString(2, coId);
			stmt.setInt(3, 1);			
			rs = stmt.executeQuery();
			ResultSetMetaData rsmetadata = 	rs.getMetaData();
			int    columnCount    =    rsmetadata.getColumnCount();	
			
			for   (int    i=1;   i<=columnCount;    i++ ) {
				  String name   =   rsmetadata.getColumnName(i);	
				  System.out.println(" Column Name for all import SP[] " + name);
				}
				/*
				 * while (rs.next()) { varId = rs.getInt("slrpivarcom_id");
				 * System.out.println(varId); }
				 */

		} catch (Exception ex) {
			ex.printStackTrace();		
		} finally {
			try {
				rs.close();
				stmt.close();
				conn.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	//	return dto;
	}
	

}
