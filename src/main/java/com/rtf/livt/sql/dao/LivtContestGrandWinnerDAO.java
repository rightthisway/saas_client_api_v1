/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.sql.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.rtf.common.util.DatabaseConnections;
import com.rtf.livt.dvo.ContestGrandWinnerDVO;

/**
 * The Class LivtContestGrandWinnerDAO.
 */
public class LivtContestGrandWinnerDAO {

	/**
	 * Gets all winners by contest id.
	 *
	 * @param clientId  the client id
	 * @param contestId the contest id
	 * @return the grandwinner object by contest id
	 * @throws Exception the exception
	 */
	public static List<ContestGrandWinnerDVO> getAllWinnersByCoId(String clientId, String contestId) throws Exception {
		List<ContestGrandWinnerDVO> winners = new ArrayList<ContestGrandWinnerDVO>();
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer sb = new StringBuffer( "SELECT *  " );
		sb.append( " from  pt_livvx_contest_grand_winners  WHERE clintid = ? AND conid = ?  AND migrate=? ORDER BY custid");
		try {
			conn = DatabaseConnections.getRtfSaasConnection();
			ps = conn.prepareStatement(sb.toString());
			ps.setString(1, clientId);
			ps.setString(2, contestId);
			ps.setBoolean(3, false);
			rs = ps.executeQuery();

			while (rs.next()) {
				winners.add(new ContestGrandWinnerDVO(rs.getString("clintid"), rs.getString("conid"),
						rs.getString("custid"), rs.getString("rwdtype"), rs.getDouble("rwdval")));
			}

		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return winners;
	}

	/**
	 * Update winner migration status.
	 *
	 * @param clId   the client id
	 * @param coId   the contest id
	 * @param custId the customer id
	 * @return true, if successful
	 */
	public static boolean updateWinnerMigrationStatus(String clId, String coId, String custId) {
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			String sqlQuery = "UPDATE pt_livvx_contest_grand_winners SET migrate=? WHERE clintid=? AND conid=? AND custid=?";
			conn = DatabaseConnections.getRtfSaasConnection();
			ps = conn.prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS);
			ps.setBoolean(1, true);
			ps.setString(2, clId);
			ps.setString(3, coId);
			ps.setString(4, custId);
			int affectedRows = ps.executeUpdate();
			if (affectedRows > 0) {
				return true;
			}
			return false;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return false;
	}
}
