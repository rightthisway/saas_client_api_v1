/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.sql.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.rtf.common.util.DatabaseConnections;
import com.rtf.livt.sql.dvo.LivtContestMigrationStatsDVO;

/**
 * The Class LivtContestMigrationSQLDAO.
 */
public class LivtContestMigrationSQLDAO {

	/**
	 * Gets the contest migration status by job name.
	 *
	 * @param clientId      the client id
	 * @param contestId     the contest id
	 * @param contRunningNo the contest running no
	 * @param jobName       the job name
	 * @return the contest migration stats by job name
	 * @throws Exception the exception
	 */
	public static LivtContestMigrationStatsDVO getContestMigrationStatsByJobName(String clientId, String contestId,
			Integer contRunningNo, String jobName) throws Exception {
		LivtContestMigrationStatsDVO contMigStats = null;
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer sb = new StringBuffer(
				"SELECT clintid,conid,conrunningno,jobname,cass_count,sql_count,status,start_time,end_time ");
		sb.append(
				"from  pm_livvx_contest_migration_dtls  WHERE clintid = ? AND conid = ? AND conrunningno=?  AND jobname=? ");
		try {
			conn = DatabaseConnections.getRtfSaasConnection();
			ps = conn.prepareStatement(sb.toString());
			ps.setString(1, clientId);
			ps.setString(2, contestId);
			ps.setInt(3, contRunningNo);
			ps.setString(4, jobName);
			rs = ps.executeQuery();

			while (rs.next()) {
				contMigStats = new LivtContestMigrationStatsDVO();
				contMigStats.setCoId(rs.getString("conid"));
				contMigStats.setClId(rs.getString("clintid"));
				contMigStats.setContRunningNo(rs.getInt("conrunningno"));
				contMigStats.setJobName(rs.getString("jobname"));
				contMigStats.setStatus(rs.getString("status"));
				contMigStats.setCaasDataCount(rs.getInt("cass_count"));
				contMigStats.setSqlDataCount(rs.getInt("sql_count"));
				contMigStats.setStartTime(rs.getTimestamp("start_time"));
				contMigStats.setEndDate(rs.getTimestamp("end_time"));

			}
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return contMigStats;
	}

	/**
	 * Save contest migration status.
	 *
	 * @param clientId the client id
	 * @param obj      the LivtContestMigrationStatsDVO object
	 * @return the integer
	 */
	public static Integer saveContestMigrationStats(String clientId, LivtContestMigrationStatsDVO obj) {
		Integer id = null;
		Connection conn = null;
		PreparedStatement statement = null;

		StringBuffer sb = new StringBuffer(
				"insert into pm_livvx_contest_migration_dtls(clintid,conid,conrunningno,jobname,cass_count,sql_count,status,start_time,end_time)"
						+ "values(?,?,?,?,?,?,?,getdate(),null)");

		try {
			conn = DatabaseConnections.getRtfSaasConnection();
			statement = conn.prepareStatement(sb.toString(), Statement.RETURN_GENERATED_KEYS);
			statement.setString(1, obj.getClId());
			statement.setString(2, obj.getCoId());
			statement.setInt(3, obj.getContRunningNo());
			statement.setString(4, obj.getJobName());
			statement.setInt(5, obj.getCaasDataCount());
			statement.setInt(6, obj.getSqlDataCount());
			statement.setString(7, obj.getStatus());

			int affectedRows = statement.executeUpdate();
			if (affectedRows == 0) {
				throw new SQLException("Creating contest Migration dtls record failed, no rows affected.");
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				statement.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return id;

	}

	/**
	 * Update contest migration status.
	 *
	 * @param clientId the client id
	 * @param obj      the LivtContestMigrationStatsDVO Object
	 * @return the integer
	 */
	public static Integer updateContestMigrationStatus(String clientId, LivtContestMigrationStatsDVO obj) {
		Integer affectedRows = null;
		Connection conn = null;
		PreparedStatement statement = null;
		StringBuffer sb = new StringBuffer(
				"update pm_livvx_contest_migration_dtls set cass_count=?,sql_count=?,status=?,end_time=getdate()");
		sb.append(" where clintid=? and conid=? and conrunningno = ? and jobname=?");

		try {
			conn = DatabaseConnections.getRtfSaasConnection();
			statement = conn.prepareStatement(sb.toString());
			statement.setInt(1, obj.getCaasDataCount());
			statement.setInt(2, obj.getSqlDataCount());
			statement.setString(3, obj.getStatus());
			statement.setString(4, obj.getClId());
			statement.setString(5, obj.getCoId());
			statement.setInt(6, obj.getContRunningNo());
			statement.setString(7, obj.getJobName());

			affectedRows = statement.executeUpdate();
			if (affectedRows == 0) {
				throw new SQLException("updating contest Migration dtls record failed, no rows affected.");
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				// rs.close();
				statement.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return affectedRows;

	}
}
