/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.sql.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.rtf.common.util.DatabaseConnections;
import com.rtf.livt.dvo.CustContAnswersDVO;

/**
 * The Class LivtCustomerAnswerDAO.
 */
public class LivtCustomerAnswerDAO {

	/**
	 * Gets the all correct answer by co id.
	 *
	 * @param clientId  the client id
	 * @param contestId the contest id
	 * @return the all correct answer by contest id
	 * @throws Exception the exception
	 */
	public static List<CustContAnswersDVO> getAllCorrectAnswerByCoId(String clientId, String contestId)
			throws Exception {
		List<CustContAnswersDVO> answers = new ArrayList<CustContAnswersDVO>();
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sql = "SELECT * "
				+ "from  pt_livvx_customer_answer  WHERE clintid = ? AND conid = ?  AND iscrtans=? AND migrate=? ORDER BY custid";
		try {
			conn = DatabaseConnections.getRtfSaasConnection();
			ps = conn.prepareStatement(sql);
			ps.setString(1, clientId);
			ps.setString(2, contestId);
			ps.setBoolean(3, true);
			ps.setBoolean(4, false);
			rs = ps.executeQuery();

			while (rs.next()) {
				answers.add(new CustContAnswersDVO(rs.getString("clintid"), rs.getString("custid"),
						rs.getString("conid"), rs.getInt("conqsnid"), rs.getInt("qsnseqno"), rs.getString("custans"),
						rs.getBoolean("iscrtans"), rs.getBoolean("islife"), rs.getDouble("ansrwdval"),
						rs.getTimestamp("credate"), rs.getTimestamp("upddate"), rs.getString("fb_callback_time"),
						rs.getString("anstime"), rs.getInt("rt_count"), rs.getBoolean("autocreated"),
						rs.getString("ansrwdtype")));

			}
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return answers;
	}

	/**
	 * Update cust answer migration status.
	 *
	 * @param clId   the client id
	 * @param coId   the contest id
	 * @param custId the customer id
	 * @return true, if successful
	 */
	public static boolean updateCustAnswerMigrationStatus(String clId, String coId, String custId) {
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			String sqlQuery = "UPDATE pt_livvx_customer_answer SET migrate=? WHERE clintid=? AND conid=? AND custid=?";
			conn = DatabaseConnections.getRtfSaasConnection();
			ps = conn.prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS);
			ps.setBoolean(1, true);
			ps.setString(2, clId);
			ps.setString(3, coId);
			ps.setString(4, custId);
			int affectedRows = ps.executeUpdate();
			if (affectedRows > 0) {
				return true;
			}
			return false;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return false;
	}

}
