/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.dto;

import com.rtf.common.dvo.ClientCustomerDVO;
import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * The Class DashboardInfo.
 */
@XStreamAlias("DbdInfo")
public class DashboardInfo {

	/** The sts. */
	private Integer sts;

	/** The err. */
	private CassError err;

	/** The msg. */
	private String msg;

	/** The cust. */
	private ClientCustomerDVO cust;

	/** The h rwds. */
	private Boolean hRwds;

	/** The sflq no. */
	private Integer sflqNo;

	/**
	 * Gets the cust.
	 *
	 * @return the cust
	 */
	public ClientCustomerDVO getCust() {
		return cust;
	}

	/**
	 * Gets the err.
	 *
	 * @return the err
	 */
	public CassError getErr() {
		return err;
	}

	/**
	 * Gets the h rwds.
	 *
	 * @return the h rwds
	 */
	public Boolean gethRwds() {
		if (hRwds == null) {
			hRwds = false;
		}
		return hRwds;
	}

	/**
	 * Gets the msg.
	 *
	 * @return the msg
	 */
	public String getMsg() {
		return msg;
	}

	/**
	 * Gets the sflq no.
	 *
	 * @return the sflq no
	 */
	public Integer getSflqNo() {
		if (sflqNo == null) {
			sflqNo = 0;
		}
		return sflqNo;
	}

	/**
	 * Gets the sts.
	 *
	 * @return the sts
	 */
	public Integer getSts() {
		return sts;
	}

	/**
	 * Sets the cust.
	 *
	 * @param cust the new cust
	 */
	public void setCust(ClientCustomerDVO cust) {
		this.cust = cust;
	}

	/**
	 * Sets the err.
	 *
	 * @param err the new err
	 */
	public void setErr(CassError err) {
		this.err = err;
	}

	/**
	 * Sets the h rwds.
	 *
	 * @param hRwds the new h rwds
	 */
	public void sethRwds(Boolean hRwds) {
		this.hRwds = hRwds;
	}

	/**
	 * Sets the msg.
	 *
	 * @param msg the new msg
	 */
	public void setMsg(String msg) {
		this.msg = msg;
	}

	/**
	 * Sets the sflq no.
	 *
	 * @param sflqNo the new sflq no
	 */
	public void setSflqNo(Integer sflqNo) {
		this.sflqNo = sflqNo;
	}

	/**
	 * Sets the sts.
	 *
	 * @param sts the new sts
	 */
	public void setSts(Integer sts) {
		this.sts = sts;
	}

}
