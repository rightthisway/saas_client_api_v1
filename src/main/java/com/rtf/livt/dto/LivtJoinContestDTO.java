/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.dto;

import com.rtf.saas.arch.RtfSaasBaseDTO;

/**
 * The Class LivtJoinContestDTO.
 */
public class LivtJoinContestDTO extends RtfSaasBaseDTO {

	/** The lq no. */
	private Integer lqNo;

	/** The is lst crt. */
	private Boolean isLstCrt;

	/** The is life used. */
	private Boolean isLifeUsed = false;

	/** The ca rwd type. */
	private String caRwdType;

	/** The ca rwds. */
	private Double caRwds;

	/** The q live. */
	private Integer qLive;// customer quiz lives

	/** The c ans cnt. */
	// private Integer sflqNo=0;
	private Integer cAnsCnt;// Correct Answer Count

	/**
	 * Gets the c ans cnt.
	 *
	 * @return the c ans cnt
	 */
	public Integer getcAnsCnt() {
		if (cAnsCnt == null) {
			cAnsCnt = 0;
		}
		return cAnsCnt;
	}

	/**
	 * Gets the ca rwds.
	 *
	 * @return the ca rwds
	 */
	public Double getCaRwds() {
		if (caRwds == null) {
			caRwds = 0.0;
		}
		return caRwds;
	}

	/**
	 * Gets the ca rwd type.
	 *
	 * @return the ca rwd type
	 */
	public String getCaRwdType() {
		return caRwdType;
	}

	/**
	 * Gets the checks if is life used.
	 *
	 * @return the checks if is life used
	 */
	public Boolean getIsLifeUsed() {
		return isLifeUsed;
	}

	/**
	 * Gets the checks if is lst crt.
	 *
	 * @return the checks if is lst crt
	 */
	public Boolean getIsLstCrt() {
		return isLstCrt;
	}

	/**
	 * Gets the lq no.
	 *
	 * @return the lq no
	 */
	public Integer getLqNo() {
		if (lqNo == null) {
			lqNo = 0;
		}
		return lqNo;
	}

	/**
	 * Gets the q live.
	 *
	 * @return the q live
	 */
	public Integer getqLive() {
		if (qLive == null) {
			qLive = 0;
		}
		return qLive;
	}

	/**
	 * Sets the c ans cnt.
	 *
	 * @param cAnsCnt the new c ans cnt
	 */
	public void setcAnsCnt(Integer cAnsCnt) {
		this.cAnsCnt = cAnsCnt;
	}

	/**
	 * Sets the ca rwds.
	 *
	 * @param caRwds the new ca rwds
	 */
	public void setCaRwds(Double caRwds) {
		this.caRwds = caRwds;
	}

	/**
	 * Sets the ca rwd type.
	 *
	 * @param caRwdType the new ca rwd type
	 */
	public void setCaRwdType(String caRwdType) {
		this.caRwdType = caRwdType;
	}

	/**
	 * Sets the checks if is life used.
	 *
	 * @param isLifeUsed the new checks if is life used
	 */
	public void setIsLifeUsed(Boolean isLifeUsed) {
		this.isLifeUsed = isLifeUsed;
	}

	/**
	 * Sets the checks if is lst crt.
	 *
	 * @param isLstCrt the new checks if is lst crt
	 */
	public void setIsLstCrt(Boolean isLstCrt) {
		this.isLstCrt = isLstCrt;
	}

	/**
	 * Sets the lq no.
	 *
	 * @param lqNo the new lq no
	 */
	public void setLqNo(Integer lqNo) {
		this.lqNo = lqNo;
	}

	/**
	 * Sets the q live.
	 *
	 * @param qLive the new q live
	 */
	public void setqLive(Integer qLive) {
		this.qLive = qLive;
	}

}
