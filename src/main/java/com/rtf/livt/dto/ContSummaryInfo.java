/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.dto;

import java.util.List;

import com.rtf.livt.dvo.CassContestWinners;
import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * The Class ContSummaryInfo.
 */
@XStreamAlias("ContSummaryInfo")
public class ContSummaryInfo {

	/** The sts. */
	private Integer sts;

	/** The err. */
	private CassError err;

	/** The msg. */
	private String msg;

	/** The w count. */
	private Integer wCount;

	/** The winners. */
	List<CassContestWinners> winners;

	/**
	 * Gets the err.
	 *
	 * @return the err
	 */
	public CassError getErr() {
		return err;
	}

	/**
	 * Gets the msg.
	 *
	 * @return the msg
	 */
	public String getMsg() {
		return msg;
	}

	/**
	 * Gets the sts.
	 *
	 * @return the sts
	 */
	public Integer getSts() {
		return sts;
	}

	/**
	 * Gets the w count.
	 *
	 * @return the w count
	 */
	public Integer getwCount() {
		if (wCount == null) {
			wCount = 0;
		}
		return wCount;
	}

	/**
	 * Gets the winners.
	 *
	 * @return the winners
	 */
	public List<CassContestWinners> getWinners() {
		return winners;
	}

	/**
	 * Sets the err.
	 *
	 * @param err the new err
	 */
	public void setErr(CassError err) {
		this.err = err;
	}

	/**
	 * Sets the msg.
	 *
	 * @param msg the new msg
	 */
	public void setMsg(String msg) {
		this.msg = msg;
	}

	/**
	 * Sets the sts.
	 *
	 * @param sts the new sts
	 */
	public void setSts(Integer sts) {
		this.sts = sts;
	}

	/**
	 * Sets the w count.
	 *
	 * @param wCount the new w count
	 */
	public void setwCount(Integer wCount) {
		this.wCount = wCount;
	}

	/**
	 * Sets the winners.
	 *
	 * @param winners the new winners
	 */
	public void setWinners(List<CassContestWinners> winners) {
		this.winners = winners;
	}

}
