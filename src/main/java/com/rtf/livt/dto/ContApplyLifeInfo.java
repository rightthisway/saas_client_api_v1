/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.dto;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * The Class ContApplyLifeInfo.
 */
@XStreamAlias("ContApplyLifeLineInfo")
public class ContApplyLifeInfo {

	/** The sts. */
	private Integer sts;

	/** The err. */
	private CassError err;

	/** The msg. */
	private String msg;

	/** The is life used. */
	private Boolean isLifeUsed = false;

	/**
	 * Gets the err.
	 *
	 * @return the err
	 */
	public CassError getErr() {
		return err;
	}

	/**
	 * Gets the checks if is life used.
	 *
	 * @return the checks if is life used
	 */
	public Boolean getIsLifeUsed() {
		return isLifeUsed;
	}

	/**
	 * Gets the msg.
	 *
	 * @return the msg
	 */
	public String getMsg() {
		return msg;
	}

	/**
	 * Gets the sts.
	 *
	 * @return the sts
	 */
	public Integer getSts() {
		return sts;
	}

	/**
	 * Sets the err.
	 *
	 * @param err the new err
	 */
	public void setErr(CassError err) {
		this.err = err;
	}

	/**
	 * Sets the checks if is life used.
	 *
	 * @param isLifeUsed the new checks if is life used
	 */
	public void setIsLifeUsed(Boolean isLifeUsed) {
		this.isLifeUsed = isLifeUsed;
	}

	/**
	 * Sets the msg.
	 *
	 * @param msg the new msg
	 */
	public void setMsg(String msg) {
		this.msg = msg;
	}

	/**
	 * Sets the sts.
	 *
	 * @param sts the new sts
	 */
	public void setSts(Integer sts) {
		this.sts = sts;
	}

}
