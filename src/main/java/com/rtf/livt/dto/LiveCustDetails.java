/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.dto;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.rtf.common.dvo.ClientCustomerDVO;

/**
 * The Class LiveCustDetails.
 */
public class LiveCustDetails implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -4855665791789397025L;

	/** The cl id. */
	private String clId;

	/** The cu id. */
	private String cuId;

	/** The u id. */
	private String uId;

	/** The img P. */
	@JsonIgnore
	private String imgP;

	/** The img U. */
	private String imgU;

	/**
	 * Instantiates a new live cust details.
	 */
	public LiveCustDetails() {

	}

	/**
	 * Instantiates a new live cust details.
	 *
	 * @param customer the customer
	 */
	public LiveCustDetails(ClientCustomerDVO customer) {
		// this.cuId = customer.getId();
		// this.uId = customer.getuId();
		// this.imgP = customer.getImgP();
	}

	/**
	 * Instantiates a new live cust details.
	 *
	 * @param clId the cl id
	 * @param cuId the cu id
	 * @param uId  the u id
	 */
	public LiveCustDetails(String clId, String cuId, String uId) {
		this.clId = clId;
		this.cuId = cuId;
		this.uId = uId;
	}

	/**
	 * Instantiates a new live cust details.
	 *
	 * @param clId the cl id
	 * @param cuId the cu id
	 * @param uId  the u id
	 * @param imgP the img P
	 */
	public LiveCustDetails(String clId, String cuId, String uId, String imgP) {
		this.clId = clId;
		this.cuId = cuId;
		this.uId = uId;
		this.imgP = imgP;
	}

	/**
	 * Gets the cl id.
	 *
	 * @return the cl id
	 */
	public String getClId() {
		return clId;
	}

	/**
	 * Gets the cu id.
	 *
	 * @return the cu id
	 */
	public String getCuId() {
		return cuId;
	}

	/**
	 * Gets the img P.
	 *
	 * @return the img P
	 */
	public String getImgP() {
		return imgP;
	}

	/**
	 * Gets the img U.
	 *
	 * @return the img U
	 */
	public String getImgU() {
		if (imgU == null) {
			imgU = "";
		} else {
			// imgU = URLUtil.profilePicWebURByImageName(this.imgP);
			// imgU = URLUtil.profilePicForSummary(this.imgP);
		}
		return imgU;
	}

	/**
	 * Gets the u id.
	 *
	 * @return the u id
	 */
	public String getuId() {
		if (uId == null) {
			uId = "";
		}
		return uId;
	}

	/**
	 * Sets the cl id.
	 *
	 * @param clId the new cl id
	 */
	public void setClId(String clId) {
		this.clId = clId;
	}

	/**
	 * Sets the cu id.
	 *
	 * @param cuId the new cu id
	 */
	public void setCuId(String cuId) {
		this.cuId = cuId;
	}

	/**
	 * Sets the img P.
	 *
	 * @param imgP the new img P
	 */
	public void setImgP(String imgP) {
		this.imgP = imgP;
	}

	/**
	 * Sets the img U.
	 *
	 * @param imgU the new img U
	 */
	public void setImgU(String imgU) {
		this.imgU = imgU;
	}

	/**
	 * Sets the u id.
	 *
	 * @param uId the new u id
	 */
	public void setuId(String uId) {
		this.uId = uId;
	}

}