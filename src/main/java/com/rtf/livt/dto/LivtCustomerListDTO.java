/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.dto;

import java.util.List;

import com.rtf.saas.arch.RtfSaasBaseDTO;

/**
 * The Class LivtCustomerListDTO.
 */
public class LivtCustomerListDTO extends RtfSaasBaseDTO {

	/** The cust list. */
	List<LiveCustDetails> custList;

	/**
	 * Gets the cust list.
	 *
	 * @return the cust list
	 */
	public List<LiveCustDetails> getCustList() {
		return custList;
	}

	/**
	 * Sets the cust list.
	 *
	 * @param custList the new cust list
	 */
	public void setCustList(List<LiveCustDetails> custList) {
		this.custList = custList;
	}

}
