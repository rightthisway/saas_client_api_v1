/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.dto;

import java.io.Serializable;
import java.text.SimpleDateFormat;

/**
 * The Class ContestGrandWinner.
 */
public class ContestGrandWinner implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -6871377683509001082L;

	/** The id. */
	private Integer id;

	/** The contest id. */
	private Integer contestId;

	/** The customer id. */
	private Integer customerId;

	/** The reward tickets. */
	private Integer rewardTickets;

	/** The expiry date str. */
	private String expiryDateStr;

	/** The contest name. */
	private String contestName;

	/** The date time format 1. */
	private SimpleDateFormat dateTimeFormat1 = new SimpleDateFormat("MM/dd/yyyy hh:mm aa");

	/**
	 * Gets the contest id.
	 *
	 * @return the contest id
	 */
	public final Integer getContestId() {
		return contestId;
	}

	/**
	 * Gets the contest name.
	 *
	 * @return the contest name
	 */
	public final String getContestName() {
		return contestName;
	}

	/**
	 * Gets the customer id.
	 *
	 * @return the customer id
	 */
	public final Integer getCustomerId() {
		return customerId;
	}

	/**
	 * Gets the date time format 1.
	 *
	 * @return the date time format 1
	 */
	public final SimpleDateFormat getDateTimeFormat1() {
		return dateTimeFormat1;
	}

	/**
	 * Gets the expiry date str.
	 *
	 * @return the expiry date str
	 */
	public final String getExpiryDateStr() {
		return expiryDateStr;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public final Integer getId() {
		return id;
	}

	/**
	 * Gets the reward tickets.
	 *
	 * @return the reward tickets
	 */
	public final Integer getRewardTickets() {
		return rewardTickets;
	}

	/**
	 * Sets the contest id.
	 *
	 * @param contestId the new contest id
	 */
	public final void setContestId(Integer contestId) {
		this.contestId = contestId;
	}

	/**
	 * Sets the contest name.
	 *
	 * @param contestName the new contest name
	 */
	public final void setContestName(String contestName) {
		this.contestName = contestName;
	}

	/**
	 * Sets the customer id.
	 *
	 * @param customerId the new customer id
	 */
	public final void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	/**
	 * Sets the date time format 1.
	 *
	 * @param dateTimeFormat1 the new date time format 1
	 */
	public final void setDateTimeFormat1(SimpleDateFormat dateTimeFormat1) {
		this.dateTimeFormat1 = dateTimeFormat1;
	}

	/**
	 * Sets the expiry date str.
	 *
	 * @param expiryDateStr the new expiry date str
	 */
	public final void setExpiryDateStr(String expiryDateStr) {
		this.expiryDateStr = expiryDateStr;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public final void setId(Integer id) {
		this.id = id;
	}

	/**
	 * Sets the reward tickets.
	 *
	 * @param rewardTickets the new reward tickets
	 */
	public final void setRewardTickets(Integer rewardTickets) {
		this.rewardTickets = rewardTickets;
	}
}
