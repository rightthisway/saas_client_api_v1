/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.dto;

import java.io.Serializable;
import java.util.UUID;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * The Class HallOfFameDtls.
 */
@XStreamAlias("HallOfFameDtls")
public class HallOfFameDtls implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -8171512510557324504L;

	/** The id. */
	@JsonIgnore
	private UUID id;

	/** The cu id. */
	private Integer cuId;

	/** The r tix. */
	private Integer rTix;

	/** The r points. */
	private Double rPoints;

	/** The img P. */
	@JsonIgnore
	private String imgP;// profile image path

	/** The img U. */
	private String imgU;// profile image URL

	/** The u id. */
	private String uId;

	/** The r points st. */
	private String rPointsSt;

	/** The r rank. */
	private Integer rRank;

	/**
	 * Instantiates a new hall of fame dtls.
	 */
	public HallOfFameDtls() {
	}

	/**
	 * Instantiates a new hall of fame dtls.
	 *
	 * @param id      the id
	 * @param cuId    the cu id
	 * @param rTix    the r tix
	 * @param rPoints the r points
	 */
	public HallOfFameDtls(UUID id, Integer cuId, Integer rTix, Double rPoints) {
		this.id = id;
		this.cuId = cuId;
		this.rTix = rTix;
		this.rPoints = rPoints;
	}

	/**
	 * Gets the cu id.
	 *
	 * @return the cu id
	 */
	public Integer getCuId() {
		return cuId;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public UUID getId() {
		return id;
	}

	/**
	 * Gets the img P.
	 *
	 * @return the img P
	 */
	public String getImgP() {
		return imgP;
	}

	/**
	 * Gets the img U.
	 *
	 * @return the img U
	 */
	public String getImgU() {
		// imgU = URLUtil.profilePicWebURByImageName(this.imgP);
		// imgU = URLUtil.profilePicForSummary(this.imgP);
		return imgU;
	}

	/**
	 * Gets the r points.
	 *
	 * @return the r points
	 */
	public Double getrPoints() {
		return rPoints;
	}

	/**
	 * Gets the r points st.
	 *
	 * @return the r points st
	 */
	public String getrPointsSt() {
		if (rPoints != null) {
			try {
				// rPointsSt = TicketUtil.getRoundedValueString(rPoints);
			} catch (Exception e) {
				rPointsSt = "0.00";
				e.printStackTrace();
			}
		} else {
			rPointsSt = "0.00";
		}
		return rPointsSt;
	}

	/**
	 * Gets the r rank.
	 *
	 * @return the r rank
	 */
	public Integer getrRank() {
		if (rRank == null) {
			rRank = 0;
		}
		return rRank;
	}

	/**
	 * Gets the r tix.
	 *
	 * @return the r tix
	 */
	public Integer getrTix() {
		if (rTix == null) {
			rTix = 0;
		}
		return rTix;
	}

	/**
	 * Gets the u id.
	 *
	 * @return the u id
	 */
	public String getuId() {
		return uId;
	}

	/**
	 * Sets the cu id.
	 *
	 * @param cuId the new cu id
	 */
	public void setCuId(Integer cuId) {
		this.cuId = cuId;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(UUID id) {
		this.id = id;
	}

	/**
	 * Sets the img P.
	 *
	 * @param imgP the new img P
	 */
	public void setImgP(String imgP) {
		this.imgP = imgP;
	}

	/**
	 * Sets the img U.
	 *
	 * @param imgU the new img U
	 */
	public void setImgU(String imgU) {
		this.imgU = imgU;
	}

	/**
	 * Sets the r points.
	 *
	 * @param rPoints the new r points
	 */
	public void setrPoints(Double rPoints) {
		this.rPoints = rPoints;
	}

	/**
	 * Sets the r points st.
	 *
	 * @param rPointsSt the new r points st
	 */
	public void setrPointsSt(String rPointsSt) {
		this.rPointsSt = rPointsSt;
	}

	/**
	 * Sets the r rank.
	 *
	 * @param rRank the new r rank
	 */
	public void setrRank(Integer rRank) {
		this.rRank = rRank;
	}

	/**
	 * Sets the r tix.
	 *
	 * @param rTix the new r tix
	 */
	public void setrTix(Integer rTix) {
		this.rTix = rTix;
	}

	/**
	 * Sets the u id.
	 *
	 * @param uId the new u id
	 */
	public void setuId(String uId) {
		this.uId = uId;
	}

}
