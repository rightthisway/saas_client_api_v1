/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.dto;

import java.util.Date;
import java.util.List;

/**
 * The Class LivtWinnerUploadDTO.
 */
public class LivtWinnerUploadDTO {

	/** The cl id. */
	private String clId;

	/** The msg. */
	private String msg;

	/** The sts. */
	private Integer sts;

	/** The co id. */
	private String coId;

	/** The con start dat. */
	private Date conStartDat;

	/** The cr no. */
	private Integer crNo;

	/** The cust list. */
	List<String> custList;

	/**
	 * Gets the cl id.
	 *
	 * @return the cl id
	 */
	public String getClId() {
		return clId;
	}

	/**
	 * Gets the co id.
	 *
	 * @return the co id
	 */
	public String getCoId() {
		return coId;
	}

	/**
	 * Gets the con start dat.
	 *
	 * @return the con start dat
	 */
	public Date getConStartDat() {
		return conStartDat;
	}

	/**
	 * Gets the cr no.
	 *
	 * @return the cr no
	 */
	public Integer getCrNo() {
		return crNo;
	}

	/**
	 * Gets the cust list.
	 *
	 * @return the cust list
	 */
	public List<String> getCustList() {
		return custList;
	}

	/**
	 * Gets the msg.
	 *
	 * @return the msg
	 */
	public String getMsg() {
		return msg;
	}

	/**
	 * Gets the sts.
	 *
	 * @return the sts
	 */
	public Integer getSts() {
		return sts;
	}

	/**
	 * Sets the cl id.
	 *
	 * @param clId the new cl id
	 */
	public void setClId(String clId) {
		this.clId = clId;
	}

	/**
	 * Sets the co id.
	 *
	 * @param coId the new co id
	 */
	public void setCoId(String coId) {
		this.coId = coId;
	}

	/**
	 * Sets the con start dat.
	 *
	 * @param conStartDat the new con start dat
	 */
	public void setConStartDat(Date conStartDat) {
		this.conStartDat = conStartDat;
	}

	/**
	 * Sets the cr no.
	 *
	 * @param crNo the new cr no
	 */
	public void setCrNo(Integer crNo) {
		this.crNo = crNo;
	}

	/**
	 * Sets the cust list.
	 *
	 * @param custList the new cust list
	 */
	public void setCustList(List<String> custList) {
		this.custList = custList;
	}

	/**
	 * Sets the msg.
	 *
	 * @param msg the new msg
	 */
	public void setMsg(String msg) {
		this.msg = msg;
	}

	/**
	 * Sets the sts.
	 *
	 * @param sts the new sts
	 */
	public void setSts(Integer sts) {
		this.sts = sts;
	}

}
