/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.dto;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * The Class CassError.
 */
@XStreamAlias("Err")
public class CassError {

	/** The desc. */
	private String desc;

	/** The e code. */
	private String eCode;

	/**
	 * Gets the desc.
	 *
	 * @return the desc
	 */
	public String getDesc() {
		return desc;
	}

	/**
	 * Gets the e code.
	 *
	 * @return the e code
	 */
	public String geteCode() {
		return eCode;
	}

	/**
	 * Sets the desc.
	 *
	 * @param desc the new desc
	 */
	public void setDesc(String desc) {
		this.desc = desc;
	}

	/**
	 * Sets the e code.
	 *
	 * @param eCode the new e code
	 */
	public void seteCode(String eCode) {
		this.eCode = eCode;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "Error [des=" + desc + "]";
	}

}
