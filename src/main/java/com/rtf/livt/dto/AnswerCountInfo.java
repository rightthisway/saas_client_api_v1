/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.dto;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * The Class AnswerCountInfo.
 */
@XStreamAlias("QuizAnswerCountDetails")
public class AnswerCountInfo {

	/** The sts. */
	private Integer sts;

	/** The err. */
	private CassError err;

	/** The msg. */
	private String msg;

	/** The c answer. */
	private String cAnswer;

	/** The a count. */
	private Integer aCount = 0;

	/** The b count. */
	private Integer bCount = 0;

	/** The c count. */
	private Integer cCount = 0;

	/** The qu rwds. */
	// private Integer optionDCount=0;
	private Double quRwds = 0.0;

	/** The life count. */
	private Integer lifeCount = 0;

	/** The cust list. */
	List<LiveCustDetails> custList;

	/**
	 * Gets the a count.
	 *
	 * @return the a count
	 */
	public Integer getaCount() {
		if (aCount == null) {
			aCount = 0;
		}
		return aCount;
	}

	/**
	 * Gets the b count.
	 *
	 * @return the b count
	 */
	public Integer getbCount() {
		if (bCount == null) {
			bCount = 0;
		}
		return bCount;
	}

	/**
	 * Gets the c answer.
	 *
	 * @return the c answer
	 */
	public String getcAnswer() {
		if (cAnswer == null) {
			cAnswer = "";
		}
		return cAnswer;
	}

	/**
	 * Gets the c count.
	 *
	 * @return the c count
	 */
	public Integer getcCount() {
		if (cCount == null) {
			cCount = 0;
		}
		return cCount;
	}

	/**
	 * Gets the cust list.
	 *
	 * @return the cust list
	 */
	public List<LiveCustDetails> getCustList() {
		return custList;
	}

	/**
	 * Gets the err.
	 *
	 * @return the err
	 */
	public CassError getErr() {
		return err;
	}

	/**
	 * Gets the life count.
	 *
	 * @return the life count
	 */
	public Integer getLifeCount() {
		if (lifeCount == null) {
			lifeCount = 0;
		}
		return lifeCount;
	}

	/**
	 * Gets the msg.
	 *
	 * @return the msg
	 */
	public String getMsg() {
		return msg;
	}

	/**
	 * Gets the qu rwds.
	 *
	 * @return the qu rwds
	 */
	public Double getQuRwds() {
		if (quRwds == null) {
			quRwds = 0.0;
		}
		return quRwds;
	}

	/**
	 * Gets the sts.
	 *
	 * @return the sts
	 */
	public Integer getSts() {
		return sts;
	}

	/**
	 * Sets the a count.
	 *
	 * @param aCount the new a count
	 */
	public void setaCount(Integer aCount) {
		this.aCount = aCount;
	}

	/**
	 * Sets the b count.
	 *
	 * @param bCount the new b count
	 */
	public void setbCount(Integer bCount) {
		this.bCount = bCount;
	}

	/**
	 * Sets the c answer.
	 *
	 * @param cAnswer the new c answer
	 */
	public void setcAnswer(String cAnswer) {
		this.cAnswer = cAnswer;
	}

	/**
	 * Sets the c count.
	 *
	 * @param cCount the new c count
	 */
	public void setcCount(Integer cCount) {
		this.cCount = cCount;
	}

	/**
	 * Sets the cust list.
	 *
	 * @param custList the new cust list
	 */
	public void setCustList(final List<LiveCustDetails> custList) {
		this.custList = custList;
	}

	/**
	 * Sets the err.
	 *
	 * @param err the new err
	 */
	public void setErr(CassError err) {
		this.err = err;
	}

	/**
	 * Sets the life count.
	 *
	 * @param lifeCount the new life count
	 */
	public void setLifeCount(Integer lifeCount) {
		this.lifeCount = lifeCount;
	}

	/**
	 * Sets the msg.
	 *
	 * @param msg the new msg
	 */
	public void setMsg(String msg) {
		this.msg = msg;
	}

	/**
	 * Sets the qu rwds.
	 *
	 * @param quRwds the new qu rwds
	 */
	public void setQuRwds(Double quRwds) {
		this.quRwds = quRwds;
	}

	/**
	 * Sets the sts.
	 *
	 * @param sts the new sts
	 */
	public void setSts(Integer sts) {
		this.sts = sts;
	}

}
