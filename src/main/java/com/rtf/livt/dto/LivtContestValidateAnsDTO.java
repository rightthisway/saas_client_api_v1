/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.dto;

import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * The Class LivtContestValidateAnsDTO.
 */
@XStreamAlias("LivtContestValidateAnsDTO")
public class LivtContestValidateAnsDTO extends RtfSaasBaseDTO {

	/** The is crt. */
	private Boolean isCrt = false;

	/**
	 * Gets the checks if is crt.
	 *
	 * @return the checks if is crt
	 */
	public Boolean getIsCrt() {
		return isCrt;
	}

	/**
	 * Sets the checks if is crt.
	 *
	 * @param isCrt the new checks if is crt
	 */
	public void setIsCrt(Boolean isCrt) {
		this.isCrt = isCrt;
	}

}
