/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.livt.dto;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * The Class ContWinnerRewardsInfo.
 */
@XStreamAlias("ContWinnerRewardsInfo")
public class ContWinnerRewardsInfo {

	/** The sts. */
	private Integer sts;

	/** The err. */
	private CassError err;

	/** The msg. */
	private String msg;

	/** The winner rwds. */
	private Double winnerRwds;

	/**
	 * Gets the err.
	 *
	 * @return the err
	 */
	public CassError getErr() {
		return err;
	}

	/**
	 * Gets the msg.
	 *
	 * @return the msg
	 */
	public String getMsg() {
		return msg;
	}

	/**
	 * Gets the sts.
	 *
	 * @return the sts
	 */
	public Integer getSts() {
		return sts;
	}

	/**
	 * Gets the winner rwds.
	 *
	 * @return the winner rwds
	 */
	public Double getWinnerRwds() {
		if (winnerRwds == null) {
			winnerRwds = 0.0;
		}
		return winnerRwds;
	}

	/**
	 * Sets the err.
	 *
	 * @param err the new err
	 */
	public void setErr(CassError err) {
		this.err = err;
	}

	/**
	 * Sets the msg.
	 *
	 * @param msg the new msg
	 */
	public void setMsg(String msg) {
		this.msg = msg;
	}

	/**
	 * Sets the sts.
	 *
	 * @param sts the new sts
	 */
	public void setSts(Integer sts) {
		this.sts = sts;
	}

	/**
	 * Sets the winner rwds.
	 *
	 * @param winnerRwds the new winner rwds
	 */
	public void setWinnerRwds(Double winnerRwds) {
		this.winnerRwds = winnerRwds;
	}

}
