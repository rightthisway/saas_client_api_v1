/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.ott.dto;

import com.rtf.saas.arch.RtfSaasBaseDTO;

/**
 * The Class OTTPlayDetailsDTO.
 */
public class OTTPlayDetailsDTO extends RtfSaasBaseDTO {

	/** The co id. */
	private String coId;

	/** The cu id. */
	private String cuId;

	/** The cl id. */
	private String clId;

	/** The thm img desk. */
	private String thmImgDesk;

	/** The thm img mob. */
	private String thmImgMob;

	/** The thm color. */
	private String thmColor;

	/** The play img. */
	private String playImg;

	/**
	 * Gets the cl id.
	 *
	 * @return the cl id
	 */
	@Override
	public String getClId() {
		return clId;
	}

	/**
	 * Gets the co id.
	 *
	 * @return the co id
	 */
	@Override
	public String getCoId() {
		return coId;
	}

	/**
	 * Gets the cu id.
	 *
	 * @return the cu id
	 */
	public String getCuId() {
		return cuId;
	}

	/**
	 * Gets the play img.
	 *
	 * @return the play img
	 */
	public String getPlayImg() {
		return playImg;
	}

	/**
	 * Gets the thm color.
	 *
	 * @return the thm color
	 */
	public String getThmColor() {
		return thmColor;
	}

	/**
	 * Gets the thm img desk.
	 *
	 * @return the thm img desk
	 */
	public String getThmImgDesk() {
		return thmImgDesk;
	}

	/**
	 * Gets the thm img mob.
	 *
	 * @return the thm img mob
	 */
	public String getThmImgMob() {
		return thmImgMob;
	}

	/**
	 * Sets the cl id.
	 *
	 * @param clId the new cl id
	 */
	@Override
	public void setClId(String clId) {
		this.clId = clId;
	}

	/**
	 * Sets the co id.
	 *
	 * @param coId the new co id
	 */
	@Override
	public void setCoId(String coId) {
		this.coId = coId;
	}

	/**
	 * Sets the cu id.
	 *
	 * @param cuId the new cu id
	 */
	public void setCuId(String cuId) {
		this.cuId = cuId;
	}

	/**
	 * Sets the play img.
	 *
	 * @param playImg the new play img
	 */
	public void setPlayImg(String playImg) {
		this.playImg = playImg;
	}

	/**
	 * Sets the thm color.
	 *
	 * @param thmColor the new thm color
	 */
	public void setThmColor(String thmColor) {
		this.thmColor = thmColor;
	}

	/**
	 * Sets the thm img desk.
	 *
	 * @param thmImgDesk the new thm img desk
	 */
	public void setThmImgDesk(String thmImgDesk) {
		this.thmImgDesk = thmImgDesk;
	}

	/**
	 * Sets the thm img mob.
	 *
	 * @param thmImgMob the new thm img mob
	 */
	public void setThmImgMob(String thmImgMob) {
		this.thmImgMob = thmImgMob;
	}

}
