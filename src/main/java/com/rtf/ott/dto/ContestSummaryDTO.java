/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.ott.dto;

import com.rtf.saas.arch.RtfSaasBaseDTO;

/**
 * The Class ContestSummaryDTO.
 */
public class ContestSummaryDTO extends RtfSaasBaseDTO {

	/** The cl id. */
	private String clId;

	/** The cu id. */
	private String cuId;

	/** The co id. */
	private String coId;

	/** The rwd type. */
	private String rwdType;

	/** The rwd val. */
	private Double rwdVal;

	/** The player id. */
	private String playerId;

	/** The rwd img. */
	private String rwdImg;

	/** The is show win img. */
	private Boolean isShowWinImg;

	/** The win img. */
	private String winImg;

	/**
	 * Gets the cl id.
	 *
	 * @return the cl id
	 */
	@Override
	public String getClId() {
		return clId;
	}

	/**
	 * Gets the co id.
	 *
	 * @return the co id
	 */
	@Override
	public String getCoId() {
		return coId;
	}

	/**
	 * Gets the cu id.
	 *
	 * @return the cu id
	 */
	public String getCuId() {
		return cuId;
	}

	/**
	 * Gets the checks if is show win img.
	 *
	 * @return the checks if is show win img
	 */
	public Boolean getIsShowWinImg() {
		return isShowWinImg;
	}

	/**
	 * Gets the player id.
	 *
	 * @return the player id
	 */
	public String getPlayerId() {
		return playerId;
	}

	/**
	 * Gets the rwd img.
	 *
	 * @return the rwd img
	 */
	public String getRwdImg() {
		return rwdImg;
	}

	/**
	 * Gets the rwd type.
	 *
	 * @return the rwd type
	 */
	public String getRwdType() {
		return rwdType;
	}

	/**
	 * Gets the rwd val.
	 *
	 * @return the rwd val
	 */
	public Double getRwdVal() {
		return rwdVal;
	}

	/**
	 * Gets the win img.
	 *
	 * @return the win img
	 */
	public String getWinImg() {
		return winImg;
	}

	/**
	 * Sets the cl id.
	 *
	 * @param clId the new cl id
	 */
	@Override
	public void setClId(String clId) {
		this.clId = clId;
	}

	/**
	 * Sets the co id.
	 *
	 * @param coId the new co id
	 */
	@Override
	public void setCoId(String coId) {
		this.coId = coId;
	}

	/**
	 * Sets the cu id.
	 *
	 * @param cuId the new cu id
	 */
	public void setCuId(String cuId) {
		this.cuId = cuId;
	}

	/**
	 * Sets the checks if is show win img.
	 *
	 * @param isShowWinImg the new checks if is show win img
	 */
	public void setIsShowWinImg(Boolean isShowWinImg) {
		this.isShowWinImg = isShowWinImg;
	}

	/**
	 * Sets the player id.
	 *
	 * @param playerId the new player id
	 */
	public void setPlayerId(String playerId) {
		this.playerId = playerId;
	}

	/**
	 * Sets the rwd img.
	 *
	 * @param rwdImg the new rwd img
	 */
	public void setRwdImg(String rwdImg) {
		this.rwdImg = rwdImg;
	}

	/**
	 * Sets the rwd type.
	 *
	 * @param rwdType the new rwd type
	 */
	public void setRwdType(String rwdType) {
		this.rwdType = rwdType;
	}

	/**
	 * Sets the rwd val.
	 *
	 * @param rwdVal the new rwd val
	 */
	public void setRwdVal(Double rwdVal) {
		this.rwdVal = rwdVal;
	}

	/**
	 * Sets the win img.
	 *
	 * @param winImg the new win img
	 */
	public void setWinImg(String winImg) {
		this.winImg = winImg;
	}

}
