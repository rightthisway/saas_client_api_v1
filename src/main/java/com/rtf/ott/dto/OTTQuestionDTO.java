/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.ott.dto;

import com.rtf.saas.arch.RtfSaasBaseDTO;

/**
 * The Class OTTQuestionDTO.
 */
public class OTTQuestionDTO extends RtfSaasBaseDTO {

	/** The q id. */
	private Integer qId;

	/** The q. */
	private String q;

	/** The o 1. */
	private String o1;

	/** The o 2. */
	private String o2;

	/** The o 3. */
	private String o3;

	/** The o 4. */
	private String o4;

	/** The q slno. */
	private Integer qSlno;

	/** The q ornt. */
	private String qOrnt;

	/** The c ans. */
	private String cAns;

	/** The qb id. */
	private Integer qbId;

	/** The conqb id. */
	private Integer conqbId;

	/**
	 * Gets the c ans.
	 *
	 * @return the c ans
	 */
	public String getcAns() {
		return cAns;
	}

	/**
	 * Gets the conqb id.
	 *
	 * @return the conqb id
	 */
	public Integer getConqbId() {
		return conqbId;
	}

	/**
	 * Gets the o1.
	 *
	 * @return the o1
	 */
	public String getO1() {
		return o1;
	}

	/**
	 * Gets the o2.
	 *
	 * @return the o2
	 */
	public String getO2() {
		return o2;
	}

	/**
	 * Gets the o3.
	 *
	 * @return the o3
	 */
	public String getO3() {
		return o3;
	}

	/**
	 * Gets the o4.
	 *
	 * @return the o4
	 */
	public String getO4() {
		return o4;
	}

	/**
	 * Gets the q.
	 *
	 * @return the q
	 */
	public String getQ() {
		return q;
	}

	/**
	 * Gets the qb id.
	 *
	 * @return the qb id
	 */
	public Integer getQbId() {
		return qbId;
	}

	/**
	 * Gets the q id.
	 *
	 * @return the q id
	 */
	public Integer getqId() {
		return qId;
	}

	/**
	 * Gets the q ornt.
	 *
	 * @return the q ornt
	 */
	public String getqOrnt() {
		return qOrnt;
	}

	/**
	 * Gets the q slno.
	 *
	 * @return the q slno
	 */
	public Integer getqSlno() {
		return qSlno;
	}

	/**
	 * Sets the c ans.
	 *
	 * @param cAns the new c ans
	 */
	public void setcAns(String cAns) {
		this.cAns = cAns;
	}

	/**
	 * Sets the conqb id.
	 *
	 * @param conqbId the new conqb id
	 */
	public void setConqbId(Integer conqbId) {
		this.conqbId = conqbId;
	}

	/**
	 * Sets the o1.
	 *
	 * @param o1 the new o1
	 */
	public void setO1(String o1) {
		this.o1 = o1;
	}

	/**
	 * Sets the o2.
	 *
	 * @param o2 the new o2
	 */
	public void setO2(String o2) {
		this.o2 = o2;
	}

	/**
	 * Sets the o3.
	 *
	 * @param o3 the new o3
	 */
	public void setO3(String o3) {
		this.o3 = o3;
	}

	/**
	 * Sets the o4.
	 *
	 * @param o4 the new o4
	 */
	public void setO4(String o4) {
		this.o4 = o4;
	}

	/**
	 * Sets the q.
	 *
	 * @param q the new q
	 */
	public void setQ(String q) {
		this.q = q;
	}

	/**
	 * Sets the qb id.
	 *
	 * @param qbId the new qb id
	 */
	public void setQbId(Integer qbId) {
		this.qbId = qbId;
	}

	/**
	 * Sets the q id.
	 *
	 * @param qId the new q id
	 */
	public void setqId(Integer qId) {
		this.qId = qId;
	}

	/**
	 * Sets the q ornt.
	 *
	 * @param qOrnt the new q ornt
	 */
	public void setqOrnt(String qOrnt) {
		this.qOrnt = qOrnt;
	}

	/**
	 * Sets the q slno.
	 *
	 * @param qSlno the new q slno
	 */
	public void setqSlno(Integer qSlno) {
		this.qSlno = qSlno;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "OTTQuestionDTO [qId=" + qId + ", q=" + q + ", o1=" + o1 + ", o2=" + o2 + ", o3=" + o3 + ", o4=" + o4
				+ ", qSlno=" + qSlno + ", qOrnt=" + qOrnt + ", cAns=" + cAns + ", qbId=" + qbId + "]";
	}

}
