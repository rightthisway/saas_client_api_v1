/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.ott.dto;

import com.rtf.saas.arch.RtfSaasBaseDTO;

/**
 * The Class PlayGameDTO.
 */
public class PlayGameDTO extends RtfSaasBaseDTO {

	/** The cl id. */
	private String clId;

	/** The cu id. */
	private String cuId;

	/** The co id. */
	private String coId;

	/** The player id. */
	private String playerId;

	/** The q size. */
	private Integer qSize;

	/** The is elm. */
	private Boolean isElm;

	/** The elm msg. */
	private String elmMsg;

	/** The elm img. */
	private String elmImg;

	/** The thm img. */
	private String thmImg;

	/** The thm color. */
	private String thmColor;

	/** The wait timer. */
	private Integer waitTimer;

	/** The que timer. */
	private Integer queTimer;

	/** The ans timer. */
	private Integer ansTimer;

	/** The pop timer. */
	private Integer popTimer;

	/**
	 * Gets the ans timer.
	 *
	 * @return the ans timer
	 */
	public Integer getAnsTimer() {
		return ansTimer;
	}

	/**
	 * Gets the cl id.
	 *
	 * @return the cl id
	 */
	@Override
	public String getClId() {
		return clId;
	}

	/**
	 * Gets the co id.
	 *
	 * @return the co id
	 */
	@Override
	public String getCoId() {
		return coId;
	}

	/**
	 * Gets the cu id.
	 *
	 * @return the cu id
	 */
	public String getCuId() {
		return cuId;
	}

	/**
	 * Gets the elm img.
	 *
	 * @return the elm img
	 */
	public String getElmImg() {
		return elmImg;
	}

	/**
	 * Gets the elm msg.
	 *
	 * @return the elm msg
	 */
	public String getElmMsg() {
		return elmMsg;
	}

	/**
	 * Gets the checks if is elm.
	 *
	 * @return the checks if is elm
	 */
	public Boolean getIsElm() {
		return isElm;
	}

	/**
	 * Gets the player id.
	 *
	 * @return the player id
	 */
	public String getPlayerId() {
		return playerId;
	}

	/**
	 * Gets the pop timer.
	 *
	 * @return the pop timer
	 */
	public Integer getPopTimer() {
		return popTimer;
	}

	/**
	 * Gets the q size.
	 *
	 * @return the q size
	 */
	public Integer getqSize() {
		return qSize;
	}

	/**
	 * Gets the que timer.
	 *
	 * @return the que timer
	 */
	public Integer getQueTimer() {
		return queTimer;
	}

	/**
	 * Gets the thm color.
	 *
	 * @return the thm color
	 */
	public String getThmColor() {
		return thmColor;
	}

	/**
	 * Gets the thm img.
	 *
	 * @return the thm img
	 */
	public String getThmImg() {
		return thmImg;
	}

	/**
	 * Gets the wait timer.
	 *
	 * @return the wait timer
	 */
	public Integer getWaitTimer() {
		return waitTimer;
	}

	/**
	 * Sets the ans timer.
	 *
	 * @param ansTimer the new ans timer
	 */
	public void setAnsTimer(Integer ansTimer) {
		this.ansTimer = ansTimer;
	}

	/**
	 * Sets the cl id.
	 *
	 * @param clId the new cl id
	 */
	@Override
	public void setClId(String clId) {
		this.clId = clId;
	}

	/**
	 * Sets the co id.
	 *
	 * @param coId the new co id
	 */
	@Override
	public void setCoId(String coId) {
		this.coId = coId;
	}

	/**
	 * Sets the cu id.
	 *
	 * @param cuId the new cu id
	 */
	public void setCuId(String cuId) {
		this.cuId = cuId;
	}

	/**
	 * Sets the elm img.
	 *
	 * @param elmImg the new elm img
	 */
	public void setElmImg(String elmImg) {
		this.elmImg = elmImg;
	}

	/**
	 * Sets the elm msg.
	 *
	 * @param elmMsg the new elm msg
	 */
	public void setElmMsg(String elmMsg) {
		this.elmMsg = elmMsg;
	}

	/**
	 * Sets the checks if is elm.
	 *
	 * @param isElm the new checks if is elm
	 */
	public void setIsElm(Boolean isElm) {
		this.isElm = isElm;
	}

	/**
	 * Sets the player id.
	 *
	 * @param playerId the new player id
	 */
	public void setPlayerId(String playerId) {
		this.playerId = playerId;
	}

	/**
	 * Sets the pop timer.
	 *
	 * @param popTimer the new pop timer
	 */
	public void setPopTimer(Integer popTimer) {
		this.popTimer = popTimer;
	}

	/**
	 * Sets the q size.
	 *
	 * @param qSize the new q size
	 */
	public void setqSize(Integer qSize) {
		this.qSize = qSize;
	}

	/**
	 * Sets the que timer.
	 *
	 * @param queTimer the new que timer
	 */
	public void setQueTimer(Integer queTimer) {
		this.queTimer = queTimer;
	}

	/**
	 * Sets the thm color.
	 *
	 * @param thmColor the new thm color
	 */
	public void setThmColor(String thmColor) {
		this.thmColor = thmColor;
	}

	/**
	 * Sets the thm img.
	 *
	 * @param thmImg the new thm img
	 */
	public void setThmImg(String thmImg) {
		this.thmImg = thmImg;
	}

	/**
	 * Sets the wait timer.
	 *
	 * @param waitTimer the new wait timer
	 */
	public void setWaitTimer(Integer waitTimer) {
		this.waitTimer = waitTimer;
	}
}
