/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.ott.dto;

import java.util.List;

import com.rtf.ott.dvo.ContestDVO;
import com.rtf.saas.arch.RtfSaasBaseDTO;

/**
 * The Class OTTContestDTO.
 */
public class OTTContestDTO extends RtfSaasBaseDTO {

	/** The contest mstr DVO. */
	private ContestDVO contestMstrDVO;

	/** The ott cotests. */
	private List<ContestDVO> ottCotests;

	/** The snw contest. */
	private com.rtf.snw.dvo.ContestDVO snwContest;

	/**
	 * Gets the contest mstr DVO.
	 *
	 * @return the contest mstr DVO
	 */
	public ContestDVO getContestMstrDVO() {
		return contestMstrDVO;
	}

	/**
	 * Gets the ott cotests.
	 *
	 * @return the ott cotests
	 */
	public List<ContestDVO> getOttCotests() {
		return ottCotests;
	}

	/**
	 * Gets the snw contest.
	 *
	 * @return the snw contest
	 */
	public com.rtf.snw.dvo.ContestDVO getSnwContest() {
		return snwContest;
	}

	/**
	 * Sets the contest mstr DVO.
	 *
	 * @param contestMstrDVO the new contest mstr DVO
	 */
	public void setContestMstrDVO(ContestDVO contestMstrDVO) {
		this.contestMstrDVO = contestMstrDVO;
	}

	/**
	 * Sets the ott cotests.
	 *
	 * @param ottCotestsS the new ott cotests
	 */
	public void setOttCotests(List<ContestDVO> ottCotestsS) {
		this.ottCotests = ottCotestsS;
	}

	/**
	 * Sets the snw contest.
	 *
	 * @param snwContest the new snw contest
	 */
	public void setSnwContest(com.rtf.snw.dvo.ContestDVO snwContest) {
		this.snwContest = snwContest;
	}

}
