/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.ott.dto;

import com.rtf.saas.arch.RtfSaasBaseDTO;

/**
 * The Class OTTValidateAnswerDTO.
 */
public class OTTValidateAnswerDTO extends RtfSaasBaseDTO {

	/** The is crt ans. */
	private Boolean isCrtAns;

	/** The is ele. */
	private Boolean isEle;

	/** The ele msg. */
	private String eleMsg;

	/** The ele img. */
	private String eleImg;

	/** The rwd txt. */
	private String rwdTxt;

	/** The rwd dsc. */
	private String rwdDsc;

	/** The rwd img. */
	private String rwdImg;

	/** The q slno. */
	private Integer qSlno;

	/** The hnxt qst. */
	private Boolean hnxtQst;

	/** The crt ans. */
	private String crtAns;

	/**
	 * Gets the crt ans.
	 *
	 * @return the crt ans
	 */
	public String getCrtAns() {
		return crtAns;
	}

	/**
	 * Gets the ele img.
	 *
	 * @return the ele img
	 */
	public String getEleImg() {
		return eleImg;
	}

	/**
	 * Gets the ele msg.
	 *
	 * @return the ele msg
	 */
	public String getEleMsg() {
		return eleMsg;
	}

	/**
	 * Gets the hnxt qst.
	 *
	 * @return the hnxt qst
	 */
	public Boolean getHnxtQst() {
		return hnxtQst;
	}

	/**
	 * Gets the checks if is crt ans.
	 *
	 * @return the checks if is crt ans
	 */
	public Boolean getIsCrtAns() {
		return isCrtAns;
	}

	/**
	 * Gets the checks if is ele.
	 *
	 * @return the checks if is ele
	 */
	public Boolean getIsEle() {
		return isEle;
	}

	/**
	 * Gets the q slno.
	 *
	 * @return the q slno
	 */
	public Integer getqSlno() {
		return qSlno;
	}

	/**
	 * Gets the rwd dsc.
	 *
	 * @return the rwd dsc
	 */
	public String getRwdDsc() {
		return rwdDsc;
	}

	/**
	 * Gets the rwd img.
	 *
	 * @return the rwd img
	 */
	public String getRwdImg() {
		return rwdImg;
	}

	/**
	 * Gets the rwd txt.
	 *
	 * @return the rwd txt
	 */
	public String getRwdTxt() {
		return rwdTxt;
	}

	/**
	 * Sets the crt ans.
	 *
	 * @param crtAns the new crt ans
	 */
	public void setCrtAns(String crtAns) {
		this.crtAns = crtAns;
	}

	/**
	 * Sets the ele img.
	 *
	 * @param eleImg the new ele img
	 */
	public void setEleImg(String eleImg) {
		this.eleImg = eleImg;
	}

	/**
	 * Sets the ele msg.
	 *
	 * @param eleMsg the new ele msg
	 */
	public void setEleMsg(String eleMsg) {
		this.eleMsg = eleMsg;
	}

	/**
	 * Sets the hnxt qst.
	 *
	 * @param hnxtQst the new hnxt qst
	 */
	public void setHnxtQst(Boolean hnxtQst) {
		this.hnxtQst = hnxtQst;
	}

	/**
	 * Sets the checks if is crt ans.
	 *
	 * @param isCrtAns the new checks if is crt ans
	 */
	public void setIsCrtAns(Boolean isCrtAns) {
		this.isCrtAns = isCrtAns;
	}

	/**
	 * Sets the checks if is ele.
	 *
	 * @param isEle the new checks if is ele
	 */
	public void setIsEle(Boolean isEle) {
		this.isEle = isEle;
	}

	/**
	 * Sets the q slno.
	 *
	 * @param qSlno the new q slno
	 */
	public void setqSlno(Integer qSlno) {
		this.qSlno = qSlno;
	}

	/**
	 * Sets the rwd dsc.
	 *
	 * @param rwdDsc the new rwd dsc
	 */
	public void setRwdDsc(String rwdDsc) {
		this.rwdDsc = rwdDsc;
	}

	/**
	 * Sets the rwd img.
	 *
	 * @param rwdImg the new rwd img
	 */
	public void setRwdImg(String rwdImg) {
		this.rwdImg = rwdImg;
	}

	/**
	 * Sets the rwd txt.
	 *
	 * @param rwdTxt the new rwd txt
	 */
	public void setRwdTxt(String rwdTxt) {
		this.rwdTxt = rwdTxt;
	}

}
