/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.ott.dvo;

/**
 * The Class ContestRewardsDVO.
 */

public class ContestRewardsDVO {

	/** The cl id. */
	private String clId;

	/** The conid. */
	private String conid;

	/** The rwdtype. */
	private String rwdtype;

	/** The rwdvalue. */
	private Double rwdvalue;

	/** The isactive. */
	private boolean isactive;

	/**
	 * Gets the cl id.
	 *
	 * @return the cl id
	 */
	public String getClId() {
		return clId;
	}

	/**
	 * Gets the conid.
	 *
	 * @return the conid
	 */
	public String getConid() {
		return conid;
	}

	/**
	 * Gets the isactive.
	 *
	 * @return the isactive
	 */
	public boolean getIsactive() {
		return isactive;
	}

	/**
	 * Gets the rwdtype.
	 *
	 * @return the rwdtype
	 */
	public String getRwdtype() {
		return rwdtype;
	}

	/**
	 * Gets the rwdvalue.
	 *
	 * @return the rwdvalue
	 */
	public Double getRwdvalue() {
		return rwdvalue;
	}

	/**
	 * Sets the cl id.
	 *
	 * @param clId the new cl id
	 */
	public void setClId(String clId) {
		this.clId = clId;
	}

	/**
	 * Sets the conid.
	 *
	 * @param conid the new conid
	 */
	public void setConid(String conid) {
		this.conid = conid;
	}

	/**
	 * Sets the isactive.
	 *
	 * @param isactive the new isactive
	 */
	public void setIsactive(boolean isactive) {
		this.isactive = isactive;
	}

	/**
	 * Sets the rwdtype.
	 *
	 * @param rwdtype the new rwdtype
	 */
	public void setRwdtype(String rwdtype) {
		this.rwdtype = rwdtype;
	}

	/**
	 * Sets the rwdvalue.
	 *
	 * @param rwdvalue the new rwdvalue
	 */
	public void setRwdvalue(Double rwdvalue) {
		this.rwdvalue = rwdvalue;
	}

}
