/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.ott.dvo;

import java.io.Serializable;

/**
 * The Class ContestDVO.
 */
public class ContestDVO implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The co id. */
	private String coId;

	/** The img U. */
	private String imgU;

	/** The cl id. */
	private String clId;

	/** The seq no. */
	private Integer seqNo;

	/** The cat. */
	private String cat;

	/** The sub cat. */
	private String subCat;

	/** The cl img U. */
	private String clImgU;

	/** The st date. */
	private Long stDate;

	/** The en date. */
	private Long enDate;

	/** The name. */
	private String name;

	/** The cr by. */
	private String crBy;

	/** The cr date. */
	private Long crDate;

	/** The is cl img. */
	private String isClImg;

	/** The is act. */
	private Integer isAct;

	/** The q size. */
	private Integer qSize;

	/** The up date. */
	private Long upDate;

	/** The up by. */
	private String upBy;

	/** The ans type. */
	private String ansType;

	/** The is elimination. */
	private Boolean isElimination;

	/** The rwd type. */
	private String rwdType;

	/** The rwd val. */
	private Double rwdVal;

	/** The q mode. */
	private String qMode;

	/** The thm img mob. */
	private String thmImgMob;

	/** The thm img desk. */
	private String thmImgDesk;

	/** The play game img. */
	private String playGameImg;

	/** The thm color. */
	private String thmColor;

	/**
	 * Gets the ans type.
	 *
	 * @return the ans type
	 */
	public String getAnsType() {
		return ansType;
	}

	/**
	 * Gets the cat.
	 *
	 * @return the cat
	 */
	public String getCat() {
		return cat;
	}

	/**
	 * Gets the cl id.
	 *
	 * @return the cl id
	 */
	public String getClId() {
		return clId;
	}

	/**
	 * Gets the cl img U.
	 *
	 * @return the cl img U
	 */
	public String getClImgU() {
		return clImgU;
	}

	/**
	 * Gets the co id.
	 *
	 * @return the co id
	 */
	public String getCoId() {
		return coId;
	}

	/**
	 * Gets the cr by.
	 *
	 * @return the cr by
	 */
	public String getCrBy() {
		return crBy;
	}

	/**
	 * Gets the cr date.
	 *
	 * @return the cr date
	 */
	public Long getCrDate() {
		return crDate;
	}

	/**
	 * Gets the en date.
	 *
	 * @return the en date
	 */
	public Long getEnDate() {
		return enDate;
	}

	/**
	 * Gets the img U.
	 *
	 * @return the img U
	 */
	public String getImgU() {
		return imgU;
	}

	/**
	 * Gets the checks if is act.
	 *
	 * @return the checks if is act
	 */
	public Integer getIsAct() {
		return isAct;
	}

	/**
	 * Gets the checks if is cl img.
	 *
	 * @return the checks if is cl img
	 */
	public String getIsClImg() {
		return isClImg;
	}

	/**
	 * Gets the checks if is elimination.
	 *
	 * @return the checks if is elimination
	 */
	public Boolean getIsElimination() {
		if (isElimination == null)
			isElimination = Boolean.FALSE;
		return isElimination;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Gets the play game img.
	 *
	 * @return the play game img
	 */
	public String getPlayGameImg() {
		return playGameImg;
	}

	/**
	 * Gets the q mode.
	 *
	 * @return the q mode
	 */
	public String getqMode() {
		return qMode;
	}

	/**
	 * Gets the q size.
	 *
	 * @return the q size
	 */
	public Integer getqSize() {
		return qSize;
	}

	/**
	 * Gets the rwd type.
	 *
	 * @return the rwd type
	 */
	public String getRwdType() {
		return rwdType;
	}

	/**
	 * Gets the rwd val.
	 *
	 * @return the rwd val
	 */
	public Double getRwdVal() {
		return rwdVal;
	}

	/**
	 * Gets the seq no.
	 *
	 * @return the seq no
	 */
	public Integer getSeqNo() {
		return seqNo;
	}

	/**
	 * Gets the st date.
	 *
	 * @return the st date
	 */
	public Long getStDate() {
		return stDate;
	}

	/**
	 * Gets the sub cat.
	 *
	 * @return the sub cat
	 */
	public String getSubCat() {
		return subCat;
	}

	/**
	 * Gets the thm color.
	 *
	 * @return the thm color
	 */
	public String getThmColor() {
		return thmColor;
	}

	/**
	 * Gets the thm img desk.
	 *
	 * @return the thm img desk
	 */
	public String getThmImgDesk() {
		return thmImgDesk;
	}

	/**
	 * Gets the thm img mob.
	 *
	 * @return the thm img mob
	 */
	public String getThmImgMob() {
		return thmImgMob;
	}

	/**
	 * Gets the up by.
	 *
	 * @return the up by
	 */
	public String getUpBy() {
		return upBy;
	}

	/**
	 * Gets the up date.
	 *
	 * @return the up date
	 */
	public Long getUpDate() {
		return upDate;
	}

	/**
	 * Sets the ans type.
	 *
	 * @param ansType the new ans type
	 */
	public void setAnsType(String ansType) {
		this.ansType = ansType;
	}

	/**
	 * Sets the cat.
	 *
	 * @param cat the new cat
	 */
	public void setCat(String cat)

	{
		this.cat = cat;
	}

	/**
	 * Sets the cl id.
	 *
	 * @param clId the new cl id
	 */
	public void setClId(String clId) {
		this.clId = clId;
	}

	/**
	 * Sets the cl img U.
	 *
	 * @param clImgU the new cl img U
	 */
	public void setClImgU(String clImgU) {
		this.clImgU = clImgU;
	}

	/**
	 * Sets the co id.
	 *
	 * @param coId the new co id
	 */
	public void setCoId(String coId) {
		this.coId = coId;
	}

	/**
	 * Sets the cr by.
	 *
	 * @param crBy the new cr by
	 */
	public void setCrBy(String crBy) {
		this.crBy = crBy;
	}

	/**
	 * Sets the cr date.
	 *
	 * @param crDate the new cr date
	 */
	public void setCrDate(Long crDate) {
		this.crDate = crDate;
	}

	/**
	 * Sets the en date.
	 *
	 * @param enDate the new en date
	 */
	public void setEnDate(Long enDate) {
		this.enDate = enDate;
	}

	/**
	 * Sets the img U.
	 *
	 * @param imgU the new img U
	 */
	public void setImgU(String imgU) {
		this.imgU = imgU;
	}

	/**
	 * Sets the checks if is act.
	 *
	 * @param isAct the new checks if is act
	 */
	public void setIsAct(Integer isAct) {
		this.isAct = isAct;
	}

	/**
	 * Sets the checks if is cl img.
	 *
	 * @param isClImg the new checks if is cl img
	 */
	public void setIsClImg(String isClImg) {
		this.isClImg = isClImg;
	}

	/**
	 * Sets the checks if is elimination.
	 *
	 * @param isElimination the new checks if is elimination
	 */
	public void setIsElimination(Boolean isElimination) {
		this.isElimination = isElimination;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Sets the play game img.
	 *
	 * @param playGameImg the new play game img
	 */
	public void setPlayGameImg(String playGameImg) {
		this.playGameImg = playGameImg;
	}

	/**
	 * Sets the q mode.
	 *
	 * @param qMode the new q mode
	 */
	public void setqMode(String qMode) {
		this.qMode = qMode;
	}

	/**
	 * Sets the q size.
	 *
	 * @param qSize the new q size
	 */
	public void setqSize(Integer qSize) {
		this.qSize = qSize;
	}

	/**
	 * Sets the rwd type.
	 *
	 * @param rwdType the new rwd type
	 */
	public void setRwdType(String rwdType) {
		this.rwdType = rwdType;
	}

	/**
	 * Sets the rwd val.
	 *
	 * @param rwdVal the new rwd val
	 */
	public void setRwdVal(Double rwdVal) {
		this.rwdVal = rwdVal;
	}

	/**
	 * Sets the seq no.
	 *
	 * @param seqNo the new seq no
	 */
	public void setSeqNo(Integer seqNo) {
		this.seqNo = seqNo;
	}

	/**
	 * Sets the st date.
	 *
	 * @param stDate the new st date
	 */
	public void setStDate(Long stDate) {
		this.stDate = stDate;
	}

	/**
	 * Sets the sub cat.
	 *
	 * @param subCat the new sub cat
	 */
	public void setSubCat(String subCat) {
		this.subCat = subCat;
	}

	/**
	 * Sets the thm color.
	 *
	 * @param thmColor the new thm color
	 */
	public void setThmColor(String thmColor) {
		this.thmColor = thmColor;
	}

	/**
	 * Sets the thm img desk.
	 *
	 * @param thmImgDesk the new thm img desk
	 */
	public void setThmImgDesk(String thmImgDesk) {
		this.thmImgDesk = thmImgDesk;
	}

	/**
	 * Sets the thm img mob.
	 *
	 * @param thmImgMob the new thm img mob
	 */
	public void setThmImgMob(String thmImgMob) {
		this.thmImgMob = thmImgMob;
	}

	/**
	 * Sets the up by.
	 *
	 * @param upBy the new up by
	 */
	public void setUpBy(String upBy) {
		this.upBy = upBy;
	}

	/**
	 * Sets the up date.
	 *
	 * @param upDate the new up date
	 */
	public void setUpDate(Long upDate) {
		this.upDate = upDate;
	}
}
