/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.ott.dvo;

/**
 * The Class CustomerAnswerDVO.
 */

public class CustomerAnswerDVO {

	/** The cl id. */
	private String clId;

	/** The conid. */
	private String conid;

	/** The custid. */
	private String custid;

	/** The custplayid. */
	private String custplayid;

	/** The custans. */
	private String custans;

	/** The conname. */
	private String conname;

	/** The qsnbnkans. */
	private String qsnbnkans;

	/** The rwdtype. */
	private String rwdtype;

	/** The qsnbnkid. */
	private Integer qsnbnkid;

	/** The qsnseqno. */
	private Integer qsnseqno;

	/** The consrtdate. */
	private long consrtdate;

	/** The credate. */
	private long credate;

	/** The upddate. */
	private long upddate;

	/** The iscrtans. */
	private boolean iscrtans;

	/** The rwdvalue. */
	private Double rwdvalue;

	/** The conqsnid. */
	private Integer conqsnid;

	/**
	 * Gets the cl id.
	 *
	 * @return the cl id
	 */
	public String getClId() {
		return clId;
	}

	/**
	 * Gets the conid.
	 *
	 * @return the conid
	 */
	public String getConid() {
		return conid;
	}

	/**
	 * Gets the conname.
	 *
	 * @return the conname
	 */
	public String getConname() {
		return conname;
	}

	/**
	 * Gets the conqsnid.
	 *
	 * @return the conqsnid
	 */
	public Integer getConqsnid() {
		return conqsnid;
	}

	/**
	 * Gets the consrtdate.
	 *
	 * @return the consrtdate
	 */
	public long getConsrtdate() {
		return consrtdate;
	}

	/**
	 * Gets the credate.
	 *
	 * @return the credate
	 */
	public long getCredate() {
		return credate;
	}

	/**
	 * Gets the custans.
	 *
	 * @return the custans
	 */
	public String getCustans() {
		return custans;
	}

	/**
	 * Gets the custid.
	 *
	 * @return the custid
	 */
	public String getCustid() {
		return custid;
	}

	/**
	 * Gets the custplayid.
	 *
	 * @return the custplayid
	 */
	public String getCustplayid() {
		return custplayid;
	}

	/**
	 * Gets the qsnbnkans.
	 *
	 * @return the qsnbnkans
	 */
	public String getQsnbnkans() {
		return qsnbnkans;
	}

	/**
	 * Gets the qsnbnkid.
	 *
	 * @return the qsnbnkid
	 */
	public Integer getQsnbnkid() {
		return qsnbnkid;
	}

	/**
	 * Gets the qsnseqno.
	 *
	 * @return the qsnseqno
	 */
	public Integer getQsnseqno() {
		return qsnseqno;
	}

	/**
	 * Gets the rwdtype.
	 *
	 * @return the rwdtype
	 */
	public String getRwdtype() {
		return rwdtype;
	}

	/**
	 * Gets the rwdvalue.
	 *
	 * @return the rwdvalue
	 */
	public Double getRwdvalue() {
		if (rwdvalue == null) {
			rwdvalue = 0.0;
		}
		return rwdvalue;
	}

	/**
	 * Gets the upddate.
	 *
	 * @return the upddate
	 */
	public long getUpddate() {
		return upddate;
	}

	/**
	 * Checks if is iscrtans.
	 *
	 * @return true, if is iscrtans
	 */
	public boolean isIscrtans() {
		return iscrtans;
	}

	/**
	 * Sets the cl id.
	 *
	 * @param clId the new cl id
	 */
	public void setClId(String clId) {
		this.clId = clId;
	}

	/**
	 * Sets the conid.
	 *
	 * @param conid the new conid
	 */
	public void setConid(String conid) {
		this.conid = conid;
	}

	/**
	 * Sets the conname.
	 *
	 * @param conname the new conname
	 */
	public void setConname(String conname) {
		this.conname = conname;
	}

	/**
	 * Sets the conqsnid.
	 *
	 * @param conqsnid the new conqsnid
	 */
	public void setConqsnid(Integer conqsnid) {
		this.conqsnid = conqsnid;
	}

	/**
	 * Sets the consrtdate.
	 *
	 * @param consrtdate the new consrtdate
	 */
	public void setConsrtdate(long consrtdate) {
		this.consrtdate = consrtdate;
	}

	/**
	 * Sets the credate.
	 *
	 * @param credate the new credate
	 */
	public void setCredate(long credate) {
		this.credate = credate;
	}

	/**
	 * Sets the custans.
	 *
	 * @param custans the new custans
	 */
	public void setCustans(String custans) {
		this.custans = custans;
	}

	/**
	 * Sets the custid.
	 *
	 * @param custid the new custid
	 */
	public void setCustid(String custid) {
		this.custid = custid;
	}

	/**
	 * Sets the custplayid.
	 *
	 * @param custplayid the new custplayid
	 */
	public void setCustplayid(String custplayid) {
		this.custplayid = custplayid;
	}

	/**
	 * Sets the iscrtans.
	 *
	 * @param iscrtans the new iscrtans
	 */
	public void setIscrtans(boolean iscrtans) {
		this.iscrtans = iscrtans;
	}

	/**
	 * Sets the qsnbnkans.
	 *
	 * @param qsnbnkans the new qsnbnkans
	 */
	public void setQsnbnkans(String qsnbnkans) {
		this.qsnbnkans = qsnbnkans;
	}

	/**
	 * Sets the qsnbnkid.
	 *
	 * @param qsnbnkid the new qsnbnkid
	 */
	public void setQsnbnkid(Integer qsnbnkid) {
		this.qsnbnkid = qsnbnkid;
	}

	/**
	 * Sets the qsnseqno.
	 *
	 * @param qsnseqno the new qsnseqno
	 */
	public void setQsnseqno(Integer qsnseqno) {
		this.qsnseqno = qsnseqno;
	}

	/**
	 * Sets the rwdtype.
	 *
	 * @param rwdtype the new rwdtype
	 */
	public void setRwdtype(String rwdtype) {
		this.rwdtype = rwdtype;
	}

	/**
	 * Sets the rwdvalue.
	 *
	 * @param rwdvalue the new rwdvalue
	 */
	public void setRwdvalue(Double rwdvalue) {
		this.rwdvalue = rwdvalue;
	}

	/**
	 * Sets the upddate.
	 *
	 * @param upddate the new upddate
	 */
	public void setUpddate(long upddate) {
		this.upddate = upddate;
	}

}
