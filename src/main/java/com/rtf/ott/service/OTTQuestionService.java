/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.ott.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.rtf.common.service.CustomerRewardService;
import com.rtf.common.util.Constants;
import com.rtf.ott.dao.ContestDAO;
import com.rtf.ott.dao.ContestPlayerTrackingDAO;
import com.rtf.ott.dao.ContestQuestionDAO;
import com.rtf.ott.dao.CustomerAnswerDAO;
import com.rtf.ott.dao.QuestionBankDAO;
import com.rtf.ott.dto.OTTQuestionDTO;
import com.rtf.ott.dto.OTTValidateAnswerDTO;
import com.rtf.ott.dvo.ContestDVO;
import com.rtf.ott.dvo.ContestPlayerTrackingDVO;
import com.rtf.ott.dvo.ContestQuestionDVO;
import com.rtf.ott.dvo.CustomerAnswerDVO;
import com.rtf.ott.dvo.QuestionBankDVO;
import com.rtf.ott.util.OTTDataMigrationUtil;
import com.rtf.saas.exception.ContestConfigException;
import com.rtf.saas.exception.SaasProcessException;
import com.rtf.saas.util.GenUtil;

/**
 * The Class OTTQuestionService.
 */
public class OTTQuestionService {

	/**
	 * Cust answered questionsby contest question id.
	 *
	 * @param clintId  the Client ID
	 * @param custId   the Customer id
	 * @param coId     the Contest ID 
	 * @param playId  the play id
	 * @return the list
	 * @throws Exception the exception
	 */
	public static List<Integer> custAnsweredQuestionsbyContestQuestionId(String clintId, String custId, String coId,
			String playId) throws Exception {

		List<Integer> queIdList = CustomerAnswerDAO.getCustomerQueAnsweredByContestQuestionsIds(clintId, custId, coId,
				playId);
		return queIdList;
	}

	/**
	 * Cust answered questionsby question bank id.
	 *
	 * @param clintId  the Client ID
	 * @param custId   the Customer id
	 * @param coId     the Contest ID 
	 * @param playId  the play id
	 * @return the list
	 * @throws Exception the exception
	 */
	public static List<Integer> custAnsweredQuestionsbyQuestionBankId(String clintId, String custId, String coId,
			String playId) throws Exception {

		List<Integer> queIdList = CustomerAnswerDAO.getCustomerQueAnsweredByQuestionsIds(clintId, custId, coId, playId);
		return queIdList;
	}

	/**
	 * Fetch OTT question info.
	 *
	 * @param oTTQuestionDTO the o TT question DTO
	 * @return the OTT question DTO
	 */

	public static OTTQuestionDTO fetchOTTQuestionInfo(OTTQuestionDTO oTTQuestionDTO) {

		try {
			if (oTTQuestionDTO == null)
				throw new SaasProcessException("OTTQuestionDTO is null");
			String contestId = oTTQuestionDTO.getCoId();
			if (contestId == null)
				throw new SaasProcessException("contestId is null");
			ContestDVO cdvo = ContestDAO.getContestByContestId(oTTQuestionDTO.getClId(), contestId);
			if (cdvo == null)
				throw new SaasProcessException("No Contest for Contest Id " + contestId);
			String coCategory = cdvo.getCat();
			if (coCategory == null)
				throw new ContestConfigException("No Contest Category for Contest Id " + contestId);
			Integer maxQuestions = cdvo.getqSize();
			if (maxQuestions == null)
				throw new ContestConfigException("No Max Questions Defined for Contest Id " + contestId);
			String fixedOrRandom = cdvo.getqMode();
			if (fixedOrRandom == null)
				throw new ContestConfigException("No Max Questions Defined for Contest Id " + contestId);
			ContestPlayerTrackingDVO cPlayTrck = ContestPlayerTrackingDAO.getContestTrackingByCustContPlayId(
					oTTQuestionDTO.getClId(), contestId, oTTQuestionDTO.getCuID(), oTTQuestionDTO.getPlayId());

			if (cPlayTrck == null)
				throw new SaasProcessException("No PlayTrackId generated:Invalid Request from [custId][Contest Id] ["
						+ oTTQuestionDTO.getCuID() + " ] [" + contestId + "]");

			if (cdvo.getqMode() == null)
				throw new ContestConfigException(
						"Question mode (FIXED/RANDOM) not Defined for Contest Id " + contestId);

			Integer reqQseqNum = oTTQuestionDTO.getqSlno();
			if (reqQseqNum == null)
				throw new SaasProcessException("Something wrong with the first Qustion SeqNum request recieve..");

			Integer qCountInTracker = cPlayTrck.getqCnt();
			if (qCountInTracker == null)
				qCountInTracker = 0;

			if (reqQseqNum == 1 && qCountInTracker != 0) {
				throw new SaasProcessException(
						"Something wrong with the first request recieve as Tracker count is not null..");
			}

			if (reqQseqNum > 1 && qCountInTracker <= 0) {
				throw new SaasProcessException(
						"Previous Answer is not registered.. something wrong as tracker count is null");
			}

			if (reqQseqNum > 1 && (reqQseqNum - qCountInTracker) != 1) {
				throw new SaasProcessException(
						"Previous Answer is not registered something wrong as tracker count diff is not equal to 1 [reqQseqNum][qCountInTracker] "
								+ reqQseqNum + "[-]" + qCountInTracker);
			}

			Map<Integer, QuestionBankDVO> qBankMap = null;
			if (Constants.CONTEST_QUESTION_MODE_RANDOM.equals(cdvo.getqMode())) {
				String coSubCategory = cdvo.getSubCat();
				oTTQuestionDTO = populateRandomQuestionFromGeneralQBank(qBankMap, cdvo.getAnsType(), coCategory,
						coSubCategory, oTTQuestionDTO);
				if (oTTQuestionDTO == null || oTTQuestionDTO.getQbId() == null)
					throw new ContestConfigException("No Questions configured for Contest Id " + contestId);

			} else if (Constants.CONTEST_QUESTION_MODE_FIXED.equals(cdvo.getqMode())) {

				oTTQuestionDTO = populateQuestionFromContestQBank(contestId, reqQseqNum, oTTQuestionDTO);

				if (oTTQuestionDTO == null || oTTQuestionDTO.getConqbId() == null)
					throw new ContestConfigException("No Questions configured for Contest Id " + contestId);

			} else {

				throw new ContestConfigException("Incorrect Question Mode configured for Contest Id " + contestId);
			}

			// insert answer table record ..
			insertCustomerQuestionAnswerDetails(oTTQuestionDTO, cdvo);

			// update playtracker. with p-type and que count
			updatePlayTrackerStats(oTTQuestionDTO, cPlayTrck, cdvo);

			oTTQuestionDTO.setSts(1);

		} catch (ContestConfigException cex) {
			oTTQuestionDTO.setSts(0);
			cex.printStackTrace();
		} catch (Exception ex) {
			oTTQuestionDTO.setSts(0);
			ex.printStackTrace();
		}
		return oTTQuestionDTO;

	}

	/**
	 * Fetch question played count for play id.
	 *
	 * @param oTTQuestionDTO the o TT question DTO
	 * @return the integer
	 * @throws Exception the exception
	 */
	public static Integer fetchQuestionPlayedCountForPlayId(OTTQuestionDTO oTTQuestionDTO) throws Exception {

		Integer qCount = 0;
		try {
			List<Integer> qIdList = CustomerAnswerDAO.custAnsweredQuestionsbyQuestionPlayId(oTTQuestionDTO.getClId(),
					oTTQuestionDTO.getCuID(), oTTQuestionDTO.getCoId(), oTTQuestionDTO.getPlayId());
			qCount = qIdList.size();
		} catch (Exception ex) {
			ex.printStackTrace();
			qCount = 0;
		}
		return qCount;
	}

	/**
	 * Insert customer question answer details.
	 *
	 * @param oTTQuestionDTO the o TT question DTO
	 * @param cdvo           the cdvo
	 * @throws Exception the exception
	 */
	private static void insertCustomerQuestionAnswerDetails(OTTQuestionDTO oTTQuestionDTO, ContestDVO cdvo)
			throws Exception {
		try {
			CustomerAnswerDVO customerAns = new CustomerAnswerDVO();
			customerAns.setConid(oTTQuestionDTO.getCoId());
			customerAns.setCustplayid(oTTQuestionDTO.getPlayId());
			customerAns.setCustid(oTTQuestionDTO.getCuID());
			customerAns.setConname(cdvo.getName());
			customerAns.setConsrtdate(cdvo.getStDate());
			customerAns.setCredate(new Date().getTime());
			customerAns.setCustans(null);
			customerAns.setIscrtans(false);
			customerAns.setQsnbnkans(oTTQuestionDTO.getcAns());
			customerAns.setQsnbnkid(oTTQuestionDTO.getQbId());
			customerAns.setQsnseqno(oTTQuestionDTO.getqSlno());
			customerAns.setUpddate(new Date().getTime());
			customerAns.setClId(oTTQuestionDTO.getClId());
			customerAns.setQsnbnkans(oTTQuestionDTO.getcAns());
			customerAns.setConqsnid(oTTQuestionDTO.getConqbId());

			CustomerAnswerDAO.insertCustomerQuenAnswerDetails(customerAns);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new SaasProcessException("Error Inserting answer Records for [contest][PLAYID] "
					+ oTTQuestionDTO.getCoId() + "-" + oTTQuestionDTO.getPlayId());
		}

	}

	/**
	 * Populate question from contest Q bank.
	 *
	 * @param contestId      the contest id
	 * @param reqQseqNum     the req qseq num
	 * @param oTTQuestionDTO the oTT question DTO
	 * @return the OTT question DTO
	 * @throws Exception the exception
	 */
	private static OTTQuestionDTO populateQuestionFromContestQBank(String contestId, Integer reqQseqNum,
			OTTQuestionDTO oTTQuestionDTO) throws Exception {

		List<Integer> qIdList = custAnsweredQuestionsbyContestQuestionId(oTTQuestionDTO.getClId(),
				oTTQuestionDTO.getCuID(), oTTQuestionDTO.getCoId(), oTTQuestionDTO.getPlayId());

		Map<Integer, ContestQuestionDVO> qMap = ContestQuestionDAO.getAllContestQuestins(oTTQuestionDTO.getClId(),
				contestId);

		if (qIdList == null)
			qIdList = new ArrayList<Integer>();
		List<Integer> conQuestIdList = new ArrayList<Integer>(qMap.keySet());
		conQuestIdList.removeAll(qIdList);
		if (conQuestIdList == null || conQuestIdList.size() == 0)
			throw new SaasProcessException("No more Questions available for customer " + oTTQuestionDTO.getCuID());
		Collections.shuffle(conQuestIdList);
		ContestQuestionDVO dvo = qMap.get(conQuestIdList.get(0));

		try {

			oTTQuestionDTO.setQ(dvo.getQtx());
			oTTQuestionDTO.setO1(dvo.getOpa());
			oTTQuestionDTO.setO2(dvo.getOpb());
			oTTQuestionDTO.setO3(dvo.getOpc());
			oTTQuestionDTO.setO4(dvo.getOpd());
			oTTQuestionDTO.setqOrnt(dvo.getQsnorient());
			oTTQuestionDTO.setqId(dvo.getConqsnid());
			oTTQuestionDTO.setConqbId(dvo.getConqsnid());
			oTTQuestionDTO.setcAns(dvo.getCans());

		} catch (Exception ex) {
			ex.printStackTrace();
			throw new SaasProcessException(GenUtil.getExceptionAsString(ex), ex);
		}
		return oTTQuestionDTO;
	}

	/**
	 * Populate random question from general Q bank.
	 *
	 * @param qBankMap       the q bank map
	 * @param ansType        the ans type
	 * @param coCategory      the Category Type
	 * @param coSubCategory   the Sub  Category Type
	 * @param oTTQuestionDTO the o TT question DTO
	 * @return the OTT question DTO
	 * @throws Exception the exception
	 */
	private static OTTQuestionDTO populateRandomQuestionFromGeneralQBank(Map<Integer, QuestionBankDVO> qBankMap,
			String ansType, String coCategory, String coSubCategory, OTTQuestionDTO oTTQuestionDTO) throws Exception {

		try {
			if (GenUtil.isEmptyString(coCategory))
				throw new SaasProcessException("Error. No Category Defined.");

			if (GenUtil.isEmptyString(coSubCategory)) {
				qBankMap = QuestionBankDAO.getAllOLTXQuestionsBasedOnCategory(oTTQuestionDTO.getClId(), ansType,
						coCategory);
			} else {
				qBankMap = QuestionBankDAO.getAllOLTXQuestionsBasedOnCategoryandSubCat(oTTQuestionDTO.getClId(),
						ansType, coCategory, coSubCategory);
			}

			List<Integer> qIdList = custAnsweredQuestionsbyQuestionBankId(oTTQuestionDTO.getClId(),
					oTTQuestionDTO.getCuID(), oTTQuestionDTO.getCoId(), oTTQuestionDTO.getPlayId());
			if (qIdList == null)
				qIdList = new ArrayList<Integer>();
			List<Integer> queBankIdList = new ArrayList<Integer>(qBankMap.keySet());
			queBankIdList.removeAll(qIdList);
			if (queBankIdList == null || queBankIdList.size() == 0)
				queBankIdList = new ArrayList<Integer>(qBankMap.keySet());
			Collections.shuffle(queBankIdList);
			QuestionBankDVO ques = qBankMap.get(queBankIdList.get(0));
			oTTQuestionDTO.setQ(ques.getQtx());
			oTTQuestionDTO.setO1(ques.getOpa());
			oTTQuestionDTO.setO2(ques.getOpb());
			oTTQuestionDTO.setO3(ques.getOpc());
			oTTQuestionDTO.setO4(ques.getOpd());
			oTTQuestionDTO.setqOrnt(ques.getqOrientn());
			oTTQuestionDTO.setqId(ques.getQbid());
			oTTQuestionDTO.setQbId(ques.getQbid());
			oTTQuestionDTO.setcAns(ques.getCans());
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new SaasProcessException("Error getting questions form question bank");
		}
		return oTTQuestionDTO;
	}

	/**
	 * Update customer quen answer details.
	 *
	 * @param custAns the cust ans
	 * @throws Exception the exception
	 */
	public static void updateCustomerQuenAnswerDetails(CustomerAnswerDVO custAns) throws Exception {
		try {
			CustomerAnswerDAO.updateCustomerQuenAnswerDetails(custAns);

			if (custAns.getRwdtype() != null && custAns.getRwdvalue() != null && custAns.getRwdvalue() > 0) {
				CustomerRewardService.creditCustomerRewards(custAns.getClId(), custAns.getCustid(),
						custAns.getRwdtype(), custAns.getRwdvalue());
				OTTDataMigrationUtil.migrateOTTAnswerRewards(custAns.getClId(), custAns.getCustid(), custAns.getConid(),
						custAns.getCustplayid());
			}
		} catch (Exception ex) {
			throw new SaasProcessException(ex.getLocalizedMessage());
		}
	}

	/**
	 * Update play tracker stats.
	 *
	 * @param oTTQuestionDTO the o TT question DTO
	 * @param cPlayTrck      the c play trck
	 * @param cdvo           the cdvo
	 * @throws Exception the exception
	 */
	private static void updatePlayTrackerStats(OTTQuestionDTO oTTQuestionDTO, ContestPlayerTrackingDVO cPlayTrck,
			ContestDVO cdvo) throws Exception {
		try {
			Boolean isupdated = ContestPlayerTrackingDAO.updateContestTracking(cPlayTrck, oTTQuestionDTO.getqSlno(),
					Constants.CONTEST_TRACK_PTYPE_QUESTION);
			if (isupdated == false)
				throw new SaasProcessException(
						"Error updating contest tracking for answer count and process type Play Id"
								+ cPlayTrck.getPlayerId());
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new SaasProcessException("Error updating contest tracking");

		}
	}

	/**
	 * Validate answers.
	 *
	 * @param ottAnsDTO  the ott ans DTO
	 * @param questionId the question id
	 * @param qSlno      the q slno
	 * @param cAns       the c ans
	 * @return the OTT validate answer DTO
	 */
	public static OTTValidateAnswerDTO validateAnswers(OTTValidateAnswerDTO ottAnsDTO, Integer questionId,
			Integer qSlno, String cAns) {

		try {
			if (ottAnsDTO == null)
				throw new SaasProcessException("OTTQuestionDTO is null");
			String contestId = ottAnsDTO.getCoId();
			if (contestId == null)
				throw new SaasProcessException("contestId is null");
			ContestDVO cdvo = ContestDAO.getContestByContestId(ottAnsDTO.getClId(), contestId);
			if (cdvo == null)
				throw new SaasProcessException("No Contest for Contest Id " + contestId);

			ContestPlayerTrackingDVO cPlayTrck = ContestPlayerTrackingDAO.getContestTrackingByCustContPlayId(
					ottAnsDTO.getClId(), contestId, ottAnsDTO.getCuID(), ottAnsDTO.getPlayId());

			if (cPlayTrck == null)
				throw new SaasProcessException("No PlayTrackId generated:Invalid Request from [custId][Contest Id] ["
						+ ottAnsDTO.getCuID() + " ] [" + contestId + "]");

			CustomerAnswerDVO custAns = CustomerAnswerDAO.getCustomerAnswers(ottAnsDTO.getClId(), ottAnsDTO.getCoId(),
					ottAnsDTO.getPlayId(), ottAnsDTO.getCuID(), questionId, qSlno);
			if (custAns == null)
				throw new SaasProcessException(
						"No cust answer generated:Invalid Request from [custId][Contest Id][playid][qid][qslno] ["
								+ ottAnsDTO.getCuID() + " ] [" + contestId + "] [" + ottAnsDTO.getPlayId() + " ] ["
								+ questionId + "][" + qSlno + " ]");

			if (custAns.getCustans() != null) {
				throw new SaasProcessException(
						"custoemr already answered:Invalid Request from [custId][Contest Id][playid][qid][qslno] ["
								+ ottAnsDTO.getCuID() + " ] [" + contestId + "] [" + ottAnsDTO.getPlayId() + " ] ["
								+ questionId + "][" + qSlno + " ]");
			}

		} catch (ContestConfigException cex) {
			ottAnsDTO.setSts(0);
			cex.printStackTrace();
		} catch (Exception ex) {
			ottAnsDTO.setSts(0);
			ex.printStackTrace();
		}
		return ottAnsDTO;

	}

}
