/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.ott.service;

import java.util.Collections;
import java.util.List;

import com.rtf.common.dspl.dvo.OttContestDVO;
import com.rtf.common.service.CustomerRewardService;
import com.rtf.ott.dao.ContestDAO;
import com.rtf.ott.dao.ContestPlayerTrackingDAO;
import com.rtf.ott.dao.ContestWinnerDAO;
import com.rtf.ott.dao.CustomerAnswerDAO;
import com.rtf.ott.dvo.ContestDVO;
import com.rtf.ott.dvo.ContestPlayerTrackingDVO;
import com.rtf.ott.dvo.ContestWinnerDVO;
import com.rtf.ott.dvo.CustomerAnswerDVO;
import com.rtf.ott.util.ContestPlayerTrackingComp;
import com.rtf.ott.util.OTTDataMigrationUtil;

/**
 * The Class OTTService.
 */
public class OTTService {

	/**
	 * Fetch OTT contest info.
	 *
	 * @param clientId  the client id
	 * @param contestId the contest id
	 * @return the contest DVO
	 * @throws Exception the exception
	 */
	public static ContestDVO fetchOTTContestInfo(String clientId, String contestId) throws Exception {
		ContestDVO contestMstrDVO = null;
		try {
			contestMstrDVO = ContestDAO.getContestByContestId(clientId, contestId);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return contestMstrDVO;
	}

	/**
	 * Gets the all contest cards.
	 *
	 * @param clientId the client id
	 * @return the all contest cards
	 * @throws Exception the exception
	 */
	public static List<OttContestDVO> getAllContestCards(String clientId) throws Exception {
		List<OttContestDVO> list = null;
		try {
			list = ContestDAO.getAllContestCards(clientId);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return list;
	}

	/**
	 * Gets the all live contest.
	 *
	 * @param clientId the client id
	 * @return the all live contest
	 * @throws Exception the exception
	 */
	public static List<ContestDVO> getAllLiveContest(String clientId) throws Exception {
		List<ContestDVO> list = null;
		try {
			list = ContestDAO.getAllLiveContest(clientId);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return list;
	}

	/**
	 * Gets the contest tracking by play id.
	 *
	 * @param clintId the Client ID
	 * @param coId    the Contest ID 
	 * @param cuId    the Customer id
	 * @param playId  the play id
	 * @return the contest tracking by play id
	 */
	public static ContestPlayerTrackingDVO getContestTrackingByPlayId(String clintId, String coId, String cuId,
			String playId) {
		try {
			ContestPlayerTrackingDVO tracking = ContestPlayerTrackingDAO.getContestTrackingByPlayId(clintId, coId, cuId,
					playId);
			return tracking;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Gets the customer correct answers.
	 *
	 * @param clintId  the Client ID
	 * @param coId     the Contest ID 
	 * @param cuId    the Customer id
	 * @param playId  the play id
	 * @return the customer correct answers
	 */
	public static List<CustomerAnswerDVO> getCustomerCorrectAnswers(String clintId, String coId, String cuId,
			String playId) {
		try {
			List<CustomerAnswerDVO> trackings = CustomerAnswerDAO.getCustomerCorrectAnswers(clintId, coId, cuId,
					playId);
			return trackings;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Gets the last contest tracking by customer.
	 *
	 * @param clintId  the Client ID
	 * @param coId     the Contest ID 
	 * @param cuId    the Customer id
	 * @return the last contest tracking by customer
	 */
	public static ContestPlayerTrackingDVO getLastContestTrackingByCustomer(String clintId, String coId, String cuId) {
		try {
			List<ContestPlayerTrackingDVO> trackings = ContestPlayerTrackingDAO.getContestTrackingByCustomer(clintId,
					coId, cuId);
			if (trackings != null && !trackings.isEmpty()) {
				Collections.sort(trackings, new ContestPlayerTrackingComp());
				ContestPlayerTrackingDVO track = trackings.get(0);
				return track;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Gets the rounded reward value.
	 *
	 * @param val the value
	 * @return the rounded reward value
	 */
	public static String getRoundedRwdVal(Double val) {

		Integer intVal = val.intValue();
		if (val > intVal) {
			return String.format("%.2f", val);
		} else {
			return "" + intVal;
		}

	}

	/**
	 * Save contest tracking.
	 *
	 * @param tracking the tracking
	 * @return the boolean
	 */
	public static Boolean saveContestTracking(ContestPlayerTrackingDVO tracking) {
		try {
			ContestPlayerTrackingDAO.saveContestTracking(tracking);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * Save contest winner.
	 *
	 * @param winner the winner
	 * @return the boolean
	 */
	public static Boolean saveContestWinner(ContestWinnerDVO winner) {
		try {
			ContestWinnerDAO.saveContestWinner(winner);
			if (winner.getRwdType() != null && winner.getRwdVal() != null && winner.getRwdVal() > 0) {
				CustomerRewardService.creditCustomerRewards(winner.getClId(), winner.getCuId(), winner.getRwdType(),
						winner.getRwdVal());
				OTTDataMigrationUtil.migrateOTTWinnerRewards(winner.getClId(), winner.getCuId(), winner.getCoId(),
						winner.getPlayId());
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * Update contest tracking.
	 *
	 * @param tracking the tracking
	 * @return the boolean
	 */
	public static Boolean updateContestTracking(ContestPlayerTrackingDVO tracking) {
		try {
			ContestPlayerTrackingDAO.updateContestTracking(tracking);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

}
