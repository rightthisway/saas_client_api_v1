/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.ott.util;

import java.util.ArrayList;
import java.util.List;

import com.rtf.common.dao.RTFDataMigrationDAO;
import com.rtf.common.dvo.RTFCustomerDVO;
import com.rtf.ott.dao.ContestWinnerDAO;
import com.rtf.ott.dao.CustomerAnswerDAO;
import com.rtf.ott.dvo.ContestWinnerDVO;
import com.rtf.ott.dvo.CustomerAnswerDVO;

/**
 * The Class OTTDataMigrationUtil.
 */
public class OTTDataMigrationUtil {

	/**
	 * Migrate OTT answer rewards.
	 *
	 * @param clId   the cl id
	 * @param cuId   the cu id
	 * @param coId   the co id
	 * @param playId the play id
	 */
	public static void migrateOTTAnswerRewards(String clId, String cuId, String coId, String playId) {
		if (clId != null && !clId.equalsIgnoreCase("PROD2020REWARDTHEFAN")) {
			return;
		}
		Double totalPoints = 0.00;
		cuId = cuId.trim();
		List<RTFCustomerDVO> customers = new ArrayList<RTFCustomerDVO>();
		try {
			List<CustomerAnswerDVO> answers = CustomerAnswerDAO.getCustomerCorrectAnswersToMigrate(clId, coId, cuId,
					playId);
			for (CustomerAnswerDVO ans : answers) {
				if (ans.getRwdtype() != null && ans.getRwdtype().equalsIgnoreCase("POINTS")) {
					totalPoints = totalPoints + (ans.getRwdvalue() != null ? ans.getRwdvalue() : 0);
				}
			}

			if (totalPoints <= 0) {
				for (CustomerAnswerDVO ans : answers) {
					if (ans.getRwdtype() != null && ans.getRwdtype().equalsIgnoreCase("POINTS")) {
						CustomerAnswerDAO.updateCustomerAnswerMigrationStatus(ans);
					}
				}
				return;
			}
			RTFCustomerDVO cust = RTFDataMigrationDAO.getRTFCustId(cuId);
			if (cust == null || cust.getId() == null) {
				return;
			}
			cust.setRtfPoints(cust.getRtfPoints() + totalPoints.intValue());
			String insQuery = RTFDataMigrationDAO.insertRTFPointsForTracking(cust, totalPoints.intValue(), coId,
					"SAAS-OTT-Q");
			String updQuery = RTFDataMigrationDAO.updateCustomerRTFPoints(cust);

			Integer result = RTFDataMigrationDAO.updateSaaSRewardsToRTF(insQuery);
			if (result != null && result > 0) {
				result = RTFDataMigrationDAO.updateSaaSRewardsToRTF(updQuery);
				for (CustomerAnswerDVO ans : answers) {
					CustomerAnswerDAO.updateCustomerAnswerMigrationStatus(ans);
				}
			}
			customers.add(cust);
			RTFDataMigrationDAO.updateDataToCass(customers);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Migrate OTT winner rewards.
	 *
	 * @param clId   the cl id
	 * @param cuId   the cu id
	 * @param coId   the co id
	 * @param playId the play id
	 */
	public static void migrateOTTWinnerRewards(String clId, String cuId, String coId, String playId) {
		if (clId != null && !clId.equalsIgnoreCase("PROD2020REWARDTHEFAN")) {
			return;
		}
		Double totalPoints = 0.00;
		String cuIdlcl = cuId.trim();
		List<RTFCustomerDVO> customers = new ArrayList<RTFCustomerDVO>();
		try {

			ContestWinnerDVO winner = ContestWinnerDAO.getContestWinner(clId, cuIdlcl, coId, playId, "POINTS");
			if (winner != null && winner.getRwdType() != null && winner.getRwdType().equalsIgnoreCase("POINTS")) {
				totalPoints = totalPoints + (winner.getRwdVal() != null ? winner.getRwdVal() : 0);
			}

			if (totalPoints <= 0) {
				if (winner != null) {
					if (winner.getRwdType() != null && winner.getRwdType().equalsIgnoreCase("POINTS")) {
						ContestWinnerDAO.updateWinnerMigrationStatus(winner);
					}
				}
				return;
			}
			RTFCustomerDVO cust = RTFDataMigrationDAO.getRTFCustId(cuIdlcl);

			if (cust == null || cust.getId() == null) {
				return;
			}
			cust.setRtfPoints(cust.getRtfPoints() + totalPoints.intValue());
			String insQuery = RTFDataMigrationDAO.insertRTFPointsForTracking(cust, totalPoints.intValue(), coId,
					"SAAS-OTT-W");
			String updQuery = RTFDataMigrationDAO.updateCustomerRTFPoints(cust);

			Integer result = RTFDataMigrationDAO.updateSaaSRewardsToRTF(insQuery);
			if (result != null && result > 0) {
				result = RTFDataMigrationDAO.updateSaaSRewardsToRTF(updQuery);
				if (winner != null) {
					ContestWinnerDAO.updateWinnerMigrationStatus(winner);
				}
			}
			customers.add(cust);
			RTFDataMigrationDAO.updateDataToCass(customers);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
