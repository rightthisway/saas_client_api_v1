/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.ott.util;

import java.util.Comparator;

import com.rtf.ott.dvo.ContestPlayerTrackingDVO;

/**
 * The Class ContestPlayerTrackingComp.
 */
public class ContestPlayerTrackingComp implements Comparator<ContestPlayerTrackingDVO> {

	/**
	 * Compare.
	 *
	 * @param o1 the o 1
	 * @param o2 the o 2
	 * @return the int
	 */
	@Override
	public int compare(ContestPlayerTrackingDVO o1, ContestPlayerTrackingDVO o2) {
		if (o1.getPlayTime() > o2.getPlayTime()) {
			return -1;
		} else if (o1.getPlayTime() < o2.getPlayTime()) {
			return 1;
		} else {
			return 0;
		}
	}

}
