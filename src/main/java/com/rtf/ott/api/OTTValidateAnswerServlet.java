/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.ott.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;

import com.rtf.common.dao.RewardTypeDAO;
import com.rtf.common.service.SaaSApiTrackingService;
import com.rtf.common.util.Messages;
import com.rtf.ott.dao.ContestDAO;
import com.rtf.ott.dao.ContestPlayerTrackingDAO;
import com.rtf.ott.dao.ContestRewardsDAO;
import com.rtf.ott.dao.CustomerAnswerDAO;
import com.rtf.ott.dto.OTTValidateAnswerDTO;
import com.rtf.ott.dvo.ContestDVO;
import com.rtf.ott.dvo.ContestPlayerTrackingDVO;
import com.rtf.ott.dvo.ContestRewardsDVO;
import com.rtf.ott.dvo.CustomerAnswerDVO;
import com.rtf.ott.service.OTTQuestionService;
import com.rtf.ott.service.OTTService;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GenUtil;
import com.rtf.saas.util.GsonUtil;
import com.rtf.saas.util.PluginProdTypeEnums;
import com.rtf.saas.util.SAASMessages;

/**
 * The Class OTTValidateAnswerServlet.
 */
@WebServlet("/OttValAns.json")
public class OTTValidateAnswerServlet extends RtfSaasBaseServlet {

	/** The Constant serialVersionUID. */

	private static final long serialVersionUID = 3784813588514510146L;

	/**
	 * Generate Http Response for Client.Response, data is sent in JSON format.
	 *
	 * @param request        the HttpServlet request
	 * @param response       the HttpServlet response
	 * @param rtfSaasBaseDTO RTF Base Data Transfer Object
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("ottresp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();

	}

	/**
	 * Process request.Method to Validate Answer for Text Trivia Question
	 *
	 * @param request  the HttpServletRequest
	 * @param response the HttpServletResponse
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		OTTValidateAnswerDTO resDto = new OTTValidateAnswerDTO();

		Timestamp stTime = GenUtil.getCurrentTimeStamp();
		String actMsg = StringUtils.EMPTY, deviceInfo = StringUtils.EMPTY;
		try {

			String cuIdStr = request.getParameter("cuId");
			String clientIdStr = request.getParameter("clientId");
			String qIdStr = request.getParameter("qId");
			String playIdStr = request.getParameter("playId");
			String contestIdStr = request.getParameter("coId");
			String qSlnoStr = request.getParameter("qslno");
			String answer = request.getParameter("ans");

			resDto.setClId(clientIdStr);
			resDto.setCuID(cuIdStr);
			resDto.setPlayId(playIdStr);
			resDto.setCoId(contestIdStr);

			ContestDVO contest = ContestDAO.getContestByContestId(clientIdStr, contestIdStr);
			if (contest == null) {
				resDto.setSts(0);
				setClientMessage(resDto, SAASMessages.GEN_ERR_MSG, null);
				actMsg = Messages.NO_CONTEST + contestIdStr;
				generateResponse(request, response, resDto);
				return;
			}

			ContestPlayerTrackingDVO contPlayTrack = ContestPlayerTrackingDAO
					.getContestTrackingByCustContPlayId(clientIdStr, contestIdStr, cuIdStr, playIdStr);
			if (contPlayTrack == null) {
				resDto.setSts(0);
				setClientMessage(resDto, SAASMessages.GEN_ERR_MSG, null);
				actMsg = Messages.NO_PLAYID + playIdStr;
				generateResponse(request, response, resDto);
				return;
			}

			CustomerAnswerDVO custAns = CustomerAnswerDAO.getCustomerAnswers(clientIdStr, contestIdStr, playIdStr,
					cuIdStr, Integer.parseInt(qIdStr), Integer.parseInt(qSlnoStr));
			if (custAns == null) {
				resDto.setSts(0);
				setClientMessage(resDto, SAASMessages.GEN_ERR_MSG, null);
				actMsg = Messages.NO_ANSWERS_FOUND + " coid: " + contestIdStr + ":cuid:" + cuIdStr + ":plid:"
						+ playIdStr + ":qid:" + qIdStr + ":qslno:" + qSlnoStr;
				generateResponse(request, response, resDto);
				return;
			}

			if (custAns.getCustans() != null) {
				resDto.setSts(0);
				setClientMessage(resDto, SAASMessages.GEN_ERR_MSG, null);
				actMsg = Messages.ALREADY_ANSWERED_EARLIER + " coid: " + contestIdStr + ":cuid:" + cuIdStr + ":plid:"
						+ playIdStr + ":qid:" + qIdStr + ":qslno:" + qSlnoStr;
				generateResponse(request, response, resDto);
				return;
			}

			Boolean isCrtAns = false;
			String crtAns = custAns.getQsnbnkans();
			if (answer != null) {
				if (custAns.getQsnbnkans() == null || custAns.getQsnbnkans().equals(answer)) {
					crtAns = answer;
					isCrtAns = true;
				}
			}

			if (isCrtAns) {
				List<ContestRewardsDVO> contRewards = ContestRewardsDAO.getActiveContestRewardsByContestId(clientIdStr,
						contestIdStr);
				if (contRewards != null && contRewards.size() > 0) {
					Collections.shuffle(contRewards);
					ContestRewardsDVO contReward = contRewards.get(0);

					custAns.setRwdtype(contReward.getRwdtype());
					custAns.setRwdvalue(contReward.getRwdvalue());

					resDto.setRwdDsc(Messages.OTT_CONGRATS_WON);
					resDto.setRwdTxt(OTTService.getRoundedRwdVal(custAns.getRwdvalue()) + " " + custAns.getRwdtype());
					if (custAns.getRwdtype() != null) {
						String rewardImageUrl = RewardTypeDAO.getRewardTypeImageUrl(clientIdStr, custAns.getRwdtype());
						if (rewardImageUrl != null) {
							resDto.setRwdImg(rewardImageUrl);
						} else {
							resDto.setRwdImg("http://34.202.149.190:8083/saasimages/points.png");
						}
					}

				} else {
					resDto.setRwdDsc(Messages.OTT_MSG1);
					resDto.setRwdTxt(Messages.OTT_MSG1);
				}

				resDto.setIsCrtAns(true);
				resDto.setIsEle(false);

			} else {
				custAns.setRwdtype(null);
				custAns.setRwdvalue(null);
				resDto.setIsCrtAns(false);
				if (contest.getIsElimination()) {
					resDto.setIsEle(true);
					resDto.setEleImg(Messages.OTT_ELIMINATION_MSG1);
				}
			}
			if (contest.getqSize() > Integer.parseInt(qSlnoStr)) {
				resDto.setHnxtQst(true);
			}
			resDto.setCrtAns(crtAns);
			custAns.setIscrtans(isCrtAns);

			OTTQuestionService.updateCustomerQuenAnswerDetails(custAns);

			resDto.setSts(1);
			setClientMessage(resDto, null, SAASMessages.GEN_SUCCESS_MSG);
			generateResponse(request, response, resDto);
			return;

		} catch (Exception e) {
			e.printStackTrace();
			resDto.setSts(0);
			setClientMessage(resDto, SAASMessages.GEN_ERR_MSG, null);
			generateResponse(request, response, resDto);
			return;
		} finally {

			/*
			 * SaaSApiTrackingService.insertApiTrackingDetails(resDto.getClId(),
			 * resDto.getCuID(), PluginProdTypeEnums.OTT.name(),
			 * GenUtil.getServPath(request), resDto.getCoId(), actMsg,
			 * GenUtil.getIp(request), "pfm", "sessionId", GenUtil.getDesc(request), //
			 * session & description stTime, GenUtil.getCurrentTimeStamp(), // Start Time &
			 * End Time 1, resDto.getSts(), deviceInfo); // Cluster Node Id & response
			 * status
			 */		}

	}

}
