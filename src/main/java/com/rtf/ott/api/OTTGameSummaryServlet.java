/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.ott.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;

import com.rtf.common.dao.RewardTypeDAO;
import com.rtf.common.service.SaaSApiTrackingService;
import com.rtf.common.util.Messages;
import com.rtf.ott.dto.ContestSummaryDTO;
import com.rtf.ott.dvo.ContestDVO;
import com.rtf.ott.dvo.ContestPlayerTrackingDVO;
import com.rtf.ott.dvo.ContestWinnerDVO;
import com.rtf.ott.dvo.CustomerAnswerDVO;
import com.rtf.ott.service.OTTService;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GenUtil;
import com.rtf.saas.util.GsonUtil;
import com.rtf.saas.util.PluginProdTypeEnums;

/**
 * The Class OTTGameSummaryServlet.
 */
@WebServlet("/ottgamesummary.json")
public class OTTGameSummaryServlet extends RtfSaasBaseServlet {

	/**
	 *
	 */
	private static final long serialVersionUID = -1425158745479757679L;

	/**
	 * Generate Http Response for Client.Response, data is sent in JSON format.
	 *
	 * @param request        the HttpServlet request
	 * @param response       the HttpServlet response
	 * @param rtfSaasBaseDTO RTF Base Data Transfer Object
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("ottresp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();

	}

	/**
	 * Process request.Method to Fetch Summary Result of Text Trivia
	 *
	 * @param request  the HttpServletRequest
	 * @param response the HttpServletResponse
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String clId = request.getParameter("clId");
		String coId = request.getParameter("coId");
		String cuId = request.getParameter("cuId");
		String qId = request.getParameter("qId");
		String plId = request.getParameter("plId");
		ContestSummaryDTO respDTO = new ContestSummaryDTO();
		respDTO.setSts(0);
		respDTO.setCoId(coId);
		respDTO.setClId(clId);
		respDTO.setCuId(cuId);
		respDTO.setPlayerId(plId);
		respDTO.setIsShowWinImg(false);
		respDTO.setWinImg(StringUtils.EMPTY);
		Timestamp stTime = GenUtil.getCurrentTimeStamp();
		String actMsg = StringUtils.EMPTY;
		//String deviceInfo = StringUtils.EMPTY;
		try {
			if (clId == null || clId.isEmpty()) {
				setClientMessage(respDTO, Messages.INVALID_CLIENT_ID, null);
				actMsg = Messages.MANDATORY_PARAM_CLIENT_ID;
				generateResponse(request, response, respDTO);
				return;
			}
			if (coId == null || coId.isEmpty()) {
				setClientMessage(respDTO, Messages.MANDATORY_PARAM_CONTEST_ID, null);
				actMsg = Messages.MANDATORY_PARAM_CONTEST_ID;
				generateResponse(request, response, respDTO);
				return;
			}
			if (cuId == null || cuId.isEmpty()) {
				setClientMessage(respDTO, Messages.MANDATORY_PARAM_CUST_ID, null);
				actMsg = Messages.MANDATORY_PARAM_CUST_ID;
				generateResponse(request, response, respDTO);
				return;
			}
			if (qId == null || qId.isEmpty()) {
				setClientMessage(respDTO, Messages.MANDATORY_PARAM_QUE_ID, null);
				actMsg = Messages.MANDATORY_PARAM_QUE_ID;
				generateResponse(request, response, respDTO);
				return;
			}
			if (plId == null || plId.isEmpty()) {
				setClientMessage(respDTO, Messages.MANDATORY_PARAM_PLAY_ID, null);
				actMsg = Messages.MANDATORY_PARAM_PLAY_ID;
				generateResponse(request, response, respDTO);
				return;
			}
			ContestDVO contest = OTTService.fetchOTTContestInfo(clId, coId);
			if (contest == null) {
				setClientMessage(respDTO, Messages.NO_CONTEST, null);
				actMsg = Messages.NO_CONTEST + coId;
				generateResponse(request, response, respDTO);
				return;
			}
			ContestPlayerTrackingDVO tracking = OTTService.getContestTrackingByPlayId(clId, coId, cuId, plId);
			if (tracking == null) {
				setClientMessage(respDTO, Messages.NO_PLAYID, null);
				actMsg = Messages.NO_PLAYID + plId;
				generateResponse(request, response, respDTO);
				return;
			}
			tracking.setSts(Messages.STATUS_COMPLETED);
			OTTService.updateContestTracking(tracking);
			List<CustomerAnswerDVO> answers = OTTService.getCustomerCorrectAnswers(clId, coId, cuId, plId);
			if (contest.getqSize() != answers.size()) {
				respDTO.setSts(1);
				setClientMessage(respDTO, null, Messages.OTT_ELIMINATION);
				generateResponse(request, response, respDTO);
				return;
			}

			if (contest.getRwdType() == null || contest.getRwdType().isEmpty()) {
				respDTO.setSts(1);
				setClientMessage(respDTO, null, Messages.OTT_CONGRATS);
				generateResponse(request, response, respDTO);
				return;
			} else {
				respDTO.setSts(1);
				ContestWinnerDVO winner = new ContestWinnerDVO();
				winner.setCoId(coId);
				winner.setCuId(cuId);
				winner.setClId(contest.getClId());
				winner.setPlayId(plId);
				winner.setCoName(contest.getName());
				winner.setRwdType(contest.getRwdType());
				winner.setRwdVal(contest.getRwdVal());
				winner.setSts("ACTIVE");
				respDTO.setRwdType(contest.getRwdType());
				respDTO.setRwdVal(contest.getRwdVal());
				OTTService.saveContestWinner(winner);

				String rewardImageUrl = RewardTypeDAO.getRewardTypeImageUrl(clId, contest.getRwdType());
				if (rewardImageUrl != null) {
					respDTO.setRwdImg(rewardImageUrl);
				} else {
					respDTO.setRwdImg("http://34.202.149.190:8083/saasimages/points.png");
				}
				respDTO.setIsShowWinImg(true);
				setClientMessage(respDTO, null, Messages.OTT_CONGRATS_WON);
				generateResponse(request, response, respDTO);
				return;
			}
		} catch (Exception e) {
			setClientMessage(respDTO, null, null);
			generateResponse(request, response, respDTO);
		}

		finally {

			/*
			 * SaaSApiTrackingService.insertApiTrackingDetails(respDTO.getClId(),
			 * respDTO.getCuID(), PluginProdTypeEnums.OTT.name(),
			 * GenUtil.getServPath(request), respDTO.getCoId(), actMsg,
			 * GenUtil.getIp(request), "pfm", "sessionId", GenUtil.getDesc(request), //
			 * session & description stTime, GenUtil.getCurrentTimeStamp(), // Start Time &
			 * End Time 1, respDTO.getSts(), deviceInfo); // Cluster Node Id & response
			 * status
			 */		}

		return;
	}

}
