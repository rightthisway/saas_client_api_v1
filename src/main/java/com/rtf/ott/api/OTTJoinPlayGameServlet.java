/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.ott.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;

import com.rtf.common.dvo.ClientConfigDVO;
import com.rtf.common.service.ClientConfigService;
import com.rtf.common.util.Constants;
import com.rtf.common.util.Messages;
import com.rtf.ott.dto.PlayGameDTO;
import com.rtf.ott.dvo.ContestDVO;
import com.rtf.ott.dvo.ContestPlayerTrackingDVO;
import com.rtf.ott.service.OTTService;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GenUtil;
import com.rtf.saas.util.GsonUtil;

/**
 * The Class OTTJoinPlayGameServlet.
 */
@WebServlet("/ottjoingame.json")
public class OTTJoinPlayGameServlet extends RtfSaasBaseServlet {

	/** The Constant serialVersionUID. */

	private static final long serialVersionUID = 3784813588514510146L;

	/**
	 * Generate Http Response for Client.Response, data is sent in JSON format.
	 *
	 * @param request        the HttpServlet request
	 * @param response       the HttpServlet response
	 * @param rtfSaasBaseDTO RTF Base Data Transfer Object
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("ottresp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();

	}

	/**
	 * Process request.Method to Allow Customer to Join and initiate a Text Trivia
	 * Contest
	 *
	 * @param request  the HttpServletRequest
	 * @param response the HttpServletResponse
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String clId = request.getParameter("clId");
		String coId = request.getParameter("coId");
		String cuId = request.getParameter("cuId");
		PlayGameDTO respDTO = new PlayGameDTO();

		respDTO.setSts(0);
		respDTO.setCoId(coId);
		respDTO.setClId(clId);
		respDTO.setCuId(cuId);

		Timestamp stTime = GenUtil.getCurrentTimeStamp();
		String actMsg = StringUtils.EMPTY, deviceInfo = StringUtils.EMPTY;

		try {
			if (clId == null || clId.isEmpty()) {
				setClientMessage(respDTO, Messages.INVALID_CLIENT_ID, null);
				actMsg = Messages.MANDATORY_PARAM_CLIENT_ID;
				generateResponse(request, response, respDTO);
				return;
			}

			if (coId == null || coId.isEmpty()) {
				setClientMessage(respDTO, Messages.MANDATORY_PARAM_CONTEST_ID, null);
				actMsg = Messages.MANDATORY_PARAM_CONTEST_ID;
				generateResponse(request, response, respDTO);
				return;
			}

			if (cuId == null || cuId.isEmpty()) {
				setClientMessage(respDTO, Messages.MANDATORY_PARAM_CUST_ID, null);
				actMsg = Messages.MANDATORY_PARAM_CUST_ID;
				generateResponse(request, response, respDTO);
				return;
			}

			ContestPlayerTrackingDVO tracking = OTTService.getLastContestTrackingByCustomer(clId, coId, cuId);
			ContestDVO contest = OTTService.fetchOTTContestInfo(clId, coId);
			ClientConfigDVO queWaitTimer = ClientConfigService.getContestConfigByKey(clId, Constants.OTT,
					Constants.QUESTION_WAIT_TIMER);
			ClientConfigDVO queTimer = ClientConfigService.getContestConfigByKey(clId, Constants.OTT,
					Constants.QUESTION_TIMER);
			ClientConfigDVO ansTimer = ClientConfigService.getContestConfigByKey(clId, Constants.OTT,
					Constants.ANSWER_TIMER);
			ClientConfigDVO ansPrizePopupTimer = ClientConfigService.getContestConfigByKey(clId, Constants.OTT,
					Constants.ANSWER_PRIZE_POPUP_TIMER);
			Integer queWaitTime = 5;
			Integer queTime = 10;
			Integer ansTime = 5;
			Integer ansPrizePopupTime = 3;
			if (queWaitTimer != null) {
				try {
					queWaitTime = Integer.parseInt(queWaitTimer.getValue());
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			if (queTimer != null) {
				try {
					queTime = Integer.parseInt(queTimer.getValue());
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			if (ansTimer != null) {
				try {
					ansTime = Integer.parseInt(ansTimer.getValue());
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			if (ansPrizePopupTimer != null) {
				try {
					ansPrizePopupTime = Integer.parseInt(ansPrizePopupTimer.getValue());
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			if (contest == null) {
				setClientMessage(respDTO, Messages.INVALID_CONTEST_ID, null);
				actMsg = Messages.NO_CONTEST + coId;
				generateResponse(request, response, respDTO);
				return;
			}
			if (tracking == null) {
				tracking = new ContestPlayerTrackingDVO();
				tracking.setCoId(coId);
				tracking.setCuId(cuId);
				tracking.setPlayTime(new Date().getTime());
				tracking.setqCnt(0);
				tracking.setSts(Messages.LIVT_CONTEST_STARTED);
				tracking.setClId(clId);
				tracking.setPlayerId(clId + "_" + cuId + "_" + tracking.getPlayTime());
				Boolean isInserted = OTTService.saveContestTracking(tracking);
				if (isInserted) {
					respDTO.setPlayerId(tracking.getPlayerId());
					respDTO.setWaitTimer(queWaitTime);
					respDTO.setQueTimer(queTime);
					respDTO.setAnsTimer(ansTime);
					respDTO.setPopTimer(ansPrizePopupTime);
					respDTO.setSts(1);
					respDTO.setqSize(contest.getqSize());
					if (contest.getIsElimination()) {
						respDTO.setElmMsg(Messages.OTT_ELIMINATION_MSG1);
						respDTO.setElmImg("http://34.202.149.190:8083/saasimages/elimination.png");
					}
					respDTO.setIsElm(contest.getIsElimination());
					generateResponse(request, response, respDTO);
					return;
				}
			}

			
			  if (contest.getqMode().equalsIgnoreCase("FIXED") && tracking != null) {
			  setClientMessage(respDTO, Messages.OTT_NO_REPEAT, null);
			  generateResponse(request, response, respDTO); return; }
			 

			ClientConfigDVO config = ClientConfigService.getContestConfigByKey(clId, Constants.OTT,
					Constants.CONTEST_INTERVAL_TIME);

			int difference = 60;
			if (config != null) {
				try {
					difference = Integer.parseInt(config.getValue());
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			long today = new Date().getTime();
			long diff = today - tracking.getPlayTime();
			long diffInMinuts = TimeUnit.MILLISECONDS.toMinutes(diff);
			
			System.out.println("server today " + today);
			
			System.out.println("diff   " + diff);
			
			System.out.println("diffInMinuts " + diffInMinuts);

			if (difference < diffInMinuts) {
				tracking = new ContestPlayerTrackingDVO();
				tracking.setCoId(coId);
				tracking.setCuId(cuId);
				tracking.setPlayTime(new Date().getTime());
				tracking.setqCnt(0);
				tracking.setSts(Messages.LIVT_CONTEST_STARTED);
				tracking.setClId(clId);
				tracking.setPlayerId(clId + "_" + cuId + "_" + tracking.getPlayTime());
				Boolean isInserted = OTTService.saveContestTracking(tracking);
				if (isInserted) {
					respDTO.setPlayerId(tracking.getPlayerId());
					respDTO.setWaitTimer(queWaitTime);
					respDTO.setQueTimer(queTime);
					respDTO.setAnsTimer(ansTime);
					respDTO.setPopTimer(ansPrizePopupTime);
					respDTO.setqSize(contest.getqSize());
					if (contest.getIsElimination()) {
						respDTO.setElmMsg(Messages.OTT_ELIMINATION_MSG1);
						respDTO.setElmImg("http://34.202.149.190:8083/saasimages/elimination.png");
					}
					respDTO.setIsElm(contest.getIsElimination());
					respDTO.setSts(1);
					generateResponse(request, response, respDTO);
					return;
				}
			} else {
				if (diffInMinuts > 0) {
					setClientMessage(respDTO,
							Messages.NEXT_INTERVAL_MSG1 + (difference - diffInMinuts) + Messages.NEXT_INTERVAL_MSG2,
							null);
					actMsg = Messages.CUST_MIN_PLAY_GAP + (difference - diffInMinuts) + Messages.NEXT_INTERVAL_MSG2;
					generateResponse(request, response, respDTO);
					return;
				} else {
					setClientMessage(respDTO, Messages.CUST_MIN_PLAY_GAP + difference + Messages.NEXT_INTERVAL_MSG2,
							null);
					actMsg = Messages.CUST_MIN_PLAY_GAP + difference + Messages.NEXT_INTERVAL_MSG2;
					generateResponse(request, response, respDTO);
					return;
				}

			}
		} catch (Exception ex) {
			setClientMessage(respDTO, null, null);
			generateResponse(request, response, respDTO);
		} finally {
			/*
			 * SaaSApiTrackingService.insertApiTrackingDetails(respDTO.getClId(),
			 * respDTO.getCuID(), PluginProdTypeEnums.OTT.name(),
			 * GenUtil.getServPath(request), respDTO.getCoId(), actMsg,
			 * GenUtil.getIp(request), "pfm", "sessionId", GenUtil.getDesc(request), //
			 * session & description stTime, GenUtil.getCurrentTimeStamp(), // Start Time &
			 * End Time 1, respDTO.getSts(), deviceInfo); // Cluster Node Id & response
			 * status
			 */		}
		return;
	}

}
