/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.ott.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;

import com.rtf.common.util.Messages;
import com.rtf.ott.dto.OTTQuestionDTO;
import com.rtf.ott.service.OTTQuestionService;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GenUtil;
import com.rtf.saas.util.GsonUtil;
import com.rtf.saas.util.SAASMessages;

/**
 * The Class OTTFetchQuestionServlet.
 */
@WebServlet("/ottgq.json")
public class OTTFetchQuestionServlet extends RtfSaasBaseServlet {

	/** The Constant serialVersionUID. */

	private static final long serialVersionUID = 3784813588514510146L;

	/**
	 * Generate Http Response for Client.Response, data is sent in JSON format.
	 *
	 * @param request        the HttpServlet request
	 * @param response       the HttpServlet response
	 * @param rtfSaasBaseDTO RTF Base Data Transfer Object
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("ottresp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();

	}

	/**
	 * Process request.Method to Fect Question for Offline Text Trivia
	 *
	 * @param request  the HttpServletRequest
	 * @param response the HttpServletResponse
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		OTTQuestionDTO dto = new OTTQuestionDTO();
		String clientIdStr;
		String cuIdStr;
		String contestIdStr;
		Timestamp stTime = GenUtil.getCurrentTimeStamp();
		String actMsg = StringUtils.EMPTY;
		//String deviceInfo = StringUtils.EMPTY;
		try {
			cuIdStr = request.getParameter("cuId");
			clientIdStr = request.getParameter("clientId");
			String playIdStr = request.getParameter("playId");
			contestIdStr = request.getParameter("coId");
			String qSlnoStr = request.getParameter("qslno");

			Integer qlNum = null;

			if (GenUtil.isEmptyString(cuIdStr)) {
				setClientMessage(dto, SAASMessages.GEN_ERR_MSG, null);
				generateResponse(request, response, dto);
				actMsg = Messages.MANDATORY_PARAM_CUST_ID;
				return;

			}
			if (GenUtil.isEmptyString(clientIdStr)) {
				setClientMessage(dto, SAASMessages.GEN_ERR_MSG, null);
				generateResponse(request, response, dto);
				actMsg = Messages.MANDATORY_PARAM_CLIENT_ID;
				return;

			}
			if (GenUtil.isEmptyString(playIdStr)) {
				setClientMessage(dto, SAASMessages.GEN_ERR_MSG, null);
				generateResponse(request, response, dto);
				actMsg = Messages.MANDATORY_PARAM_PLAY_ID;
				return;

			}
			if (GenUtil.isEmptyString(contestIdStr)) {
				setClientMessage(dto, SAASMessages.GEN_ERR_MSG, null);
				generateResponse(request, response, dto);
				actMsg = Messages.MANDATORY_PARAM_CONTEST_ID;
				return;

			}
			try {
				qlNum = Integer.parseInt(qSlnoStr);
			} catch (Exception ex) {
				setClientMessage(dto, SAASMessages.GEN_ERR_MSG, null);
				generateResponse(request, response, dto);
				actMsg = Messages.NUMBER_RECD_AS_STRING + "-qslno";
				return;
			}
			dto.setCuID(cuIdStr);
			dto.setClId(clientIdStr);
			dto.setCoId(contestIdStr);
			dto.setPlayId(playIdStr);
			dto.setqSlno(qlNum);
			dto = OTTQuestionService.fetchOTTQuestionInfo(dto);
			setClientMessage(dto, SAASMessages.GEN_ERR_MSG, null);
			generateResponse(request, response, dto);
		} catch (Exception ex) {
			dto.setSts(0);
			setClientMessage(dto, SAASMessages.GEN_ERR_MSG, null);
			generateResponse(request, response, dto);
		} finally {
			/*
			 * SaaSApiTrackingService.insertApiTrackingDetails(dto.getClId(), dto.getCuID(),
			 * PluginProdTypeEnums.OTT.name(), GenUtil.getServPath(request), dto.getCoId(),
			 * actMsg, GenUtil.getIp(request), "pfm", "sessionId", GenUtil.getDesc(request),
			 * // session & description stTime, GenUtil.getCurrentTimeStamp(), // Start Time
			 * & End Time 1, dto.getSts(), deviceInfo); // Cluster Node Id & response status
			 */
		}

	}

}
