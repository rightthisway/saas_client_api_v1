/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.ott.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rtf.common.util.Messages;
import com.rtf.ott.dto.OTTPlayDetailsDTO;
import com.rtf.ott.dvo.ContestDVO;
import com.rtf.ott.service.OTTService;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GenUtil;
import com.rtf.saas.util.GsonUtil;

/**
 * The Class OTTPlayDetailsServlet.
 */
@WebServlet("/ottgamedetails.json")
public class OTTPlayDetailsServlet extends RtfSaasBaseServlet {

	/**
	 *
	 */
	private static final long serialVersionUID = -316120440106902266L;

	/**
	 *  Generate Http Response for Client.Response, data is sent in JSON format.
	 *
* @param request        the  HttpServlet request
* @param response        the  HttpServlet response
	 * @param rtfSaasBaseDTO RTF Base Data Transfer Object
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("ottresp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();
	}

	/**
	 * Process request.
	 *
	 * @param request  the HttpServletRequest
	 * @param response the HttpServletResponse
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String clId = request.getParameter("clId");
		String coId = request.getParameter("coId");
		String cuId = request.getParameter("cuId");

		OTTPlayDetailsDTO respDTO = new OTTPlayDetailsDTO();
		respDTO.setSts(0);
		respDTO.setClId(clId);
		respDTO.setCoId(coId);
		respDTO.setCuId(cuId);
		Timestamp stTime = GenUtil.getCurrentTimeStamp();
		String actMsg = "", deviceInfo = "";
		try {
			if (clId == null || clId.isEmpty()) {
				setClientMessage(respDTO, Messages.INVALID_CLIENT_ID, null);
				actMsg = Messages.MANDATORY_PARAM_CLIENT_ID;
				generateResponse(request, response, respDTO);
				return;
			}

			if (coId == null || coId.isEmpty()) {
				setClientMessage(respDTO, "Invalid Contest id.", null);
				actMsg = Messages.MANDATORY_PARAM_CONTEST_ID;
				generateResponse(request, response, respDTO);
				return;
			}

			if (cuId == null || cuId.isEmpty()) {
				setClientMessage(respDTO, "Invalid Customer id.", null);
				actMsg = Messages.MANDATORY_PARAM_CUST_ID;
				generateResponse(request, response, respDTO);
				return;
			}

			ContestDVO contest = OTTService.fetchOTTContestInfo(clId, coId);
			if (contest == null) {
				setClientMessage(respDTO, "Contest not found.", null);
				actMsg = Messages.NO_CONTEST + coId;
				generateResponse(request, response, respDTO);
				return;
			}

			respDTO.setPlayImg(contest.getPlayGameImg());
			respDTO.setThmColor(contest.getThmColor());
			respDTO.setThmImgDesk(contest.getThmImgDesk());
			respDTO.setThmImgMob(contest.getThmImgMob());
			respDTO.setSts(1);
			generateResponse(request, response, respDTO);
			return;

		} catch (Exception e) {
			setClientMessage(respDTO, null, null);
			generateResponse(request, response, respDTO);
		} finally {
			/*
			 * SaaSApiTrackingService.insertApiTrackingDetails(respDTO.getClId(),
			 * respDTO.getCuID(), PluginProdTypeEnums.OTT.name(),
			 * GenUtil.getServPath(request), respDTO.getCoId(), actMsg,
			 * GenUtil.getIp(request), "pfm", "sessionId", GenUtil.getDesc(request), //
			 * session & description stTime, GenUtil.getCurrentTimeStamp(), // Start Time &
			 * End Time 1, respDTO.getSts(), deviceInfo); // Cluster Node Id & response
			 * status
			 */
		}
		return;
	}

}
