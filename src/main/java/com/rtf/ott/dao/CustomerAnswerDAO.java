/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.ott.dao;

import java.util.ArrayList;
import java.util.List;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.exceptions.DriverException;
import com.rtf.ott.dvo.CustomerAnswerDVO;
import com.rtf.saas.db.CassandraConnector;
import com.rtf.saas.exception.RTFDataAccessException;
import com.rtf.saas.util.GenUtil;

/**
 * The Class CustomerAnswerDAO.
 */
public class CustomerAnswerDAO {

	/**
	 * Fetch Customer answered questions by question play id.
	 *
	 * @param clId    the Client ID
	 * @param custId  the Customer id
	 * @param coId    the Contest ID 
	 * @param playId the play id
	 * @return the list
	 * @throws Exception the exception
	 */
	public static List<Integer> custAnsweredQuestionsbyQuestionPlayId(String clId, String custId, String coId,
			String playId) throws Exception {
		ResultSet resultSet = null;
		List<Integer> qbIdList = null;
		try {
			resultSet = CassandraConnector.getSession().execute(
					" SELECT qsnbnkid from  pt_ofltx_customer_answer  "
							+ " WHERE clintid=? and conid = ? AND  custid=? and custplayid = ?  ",
					clId, coId, custId, playId);

			if (resultSet != null) {
				qbIdList = new ArrayList<Integer>();
				for (Row row : resultSet) {
					qbIdList.add(row.getInt("qsnbnkid"));

				}
			}
		} catch (final DriverException de) {
			de.printStackTrace();
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		}
		return qbIdList;

	}

	/**
	 * Gets the customer answers.
	 *
	 * @param clintId     the Client ID
	 * @param contestId  the contest id
	 * @param playId     the play id
	 * @param custId      the Customer id
	 * @param questionId the question id
	 * @param qSlno      the q slno
	 * @return the customer answers
	 * @throws Exception the exception
	 */
	public static CustomerAnswerDVO getCustomerAnswers(String clintId, String contestId, String playId, String custId,
			Integer questionId, Integer qSlno) throws Exception {
		ResultSet resultSet = null;
		CustomerAnswerDVO customerAnswers = null;
		try {
			resultSet = CassandraConnector.getSession().execute(
					"SELECT * from  pt_ofltx_customer_answer  "
							+ " WHERE clintid=? and conid=? AND custid=?  and custplayid=? and qsnseqno=?",
					clintId, contestId, custId, playId, qSlno);
			if (resultSet != null) {
				for (Row row : resultSet) {
					customerAnswers = new CustomerAnswerDVO();
					customerAnswers.setConid(row.getString("conid"));
					customerAnswers.setCustid(row.getString("custid"));
					customerAnswers.setQsnbnkid(row.getInt("qsnbnkid"));
					customerAnswers.setClId(row.getString("clintid"));
					customerAnswers.setCustans(row.getString("custans"));
					customerAnswers.setCustplayid(row.getString("custplayid"));
					customerAnswers.setIscrtans(row.getBool("iscrtans"));
					customerAnswers.setQsnbnkans(row.getString("qsnbnkans"));
					customerAnswers.setQsnseqno(row.getInt("qsnseqno"));
					customerAnswers.setRwdtype(row.getString("rwdtype"));
					customerAnswers.setRwdvalue(row.getDouble("rwdval"));
				}
			}
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		}
		return customerAnswers;
	}

	/**
	 * Gets the customer correct answers.
	 *
	 * @param clintId    the Client ID
	 * @param contestId the contest id
	 * @param custId     the Customer id
	 * @param playId    the play id
	 * @return the customer correct answers
	 * @throws Exception the exception
	 */
	public static List<CustomerAnswerDVO> getCustomerCorrectAnswers(String clintId, String contestId, String custId,
			String playId) throws Exception {
		ResultSet resultSet = null;
		List<CustomerAnswerDVO> customerAnswers = new ArrayList<CustomerAnswerDVO>();
		try {
			resultSet = CassandraConnector.getSession().execute(
					"SELECT * from  pt_ofltx_customer_answer  "
							+ " WHERE clintId=? AND custid=? and conid=? and custplayid=?",
					clintId, custId, contestId, playId);
			if (resultSet != null) {
				for (Row row : resultSet) {
					Boolean isCrct = row.getBool("iscrtans");
					if (isCrct != null && isCrct) {
						CustomerAnswerDVO customerAnswer = new CustomerAnswerDVO();
						customerAnswer.setConid(row.getString("conid"));
						customerAnswer.setCustid(row.getString("custid"));
						customerAnswer.setQsnbnkid(row.getInt("qsnbnkid"));
						customerAnswer.setClId(row.getString("clintid"));
						customerAnswer.setCustans(row.getString("custans"));
						customerAnswer.setCustplayid(row.getString("custplayid"));
						customerAnswer.setIscrtans(isCrct);
						customerAnswer.setQsnbnkans(row.getString("qsnbnkans"));
						customerAnswer.setQsnseqno(row.getInt("qsnseqno"));
						customerAnswer.setRwdtype(row.getString("rwdtype"));
						customerAnswer.setRwdvalue(row.getDouble("rwdval"));
						customerAnswers.add(customerAnswer);
					}
				}
			}
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		}
		return customerAnswers;
	}

	/**
	 * Gets the customer correct answers to migrate.
	 *
	 * @param clintId    the Client ID
	 * @param contestId the contest id
	 * @param custId     the Customer id
	 * @param playId    the play id
	 * @return the customer correct answers to migrate
	 * @throws Exception the exception
	 */
	public static List<CustomerAnswerDVO> getCustomerCorrectAnswersToMigrate(String clintId, String contestId,
			String custId, String playId) throws Exception {
		ResultSet resultSet = null;
		List<CustomerAnswerDVO> customerAnswers = new ArrayList<CustomerAnswerDVO>();
		try {
			resultSet = CassandraConnector.getSession().execute(
					"SELECT * from  pt_ofltx_customer_answer  "
							+ " WHERE clintId=? AND custid=? and conid=? and custplayid=?",
					clintId, custId, contestId, playId);
			if (resultSet != null) {
				for (Row row : resultSet) {
					Boolean isCrct = row.getBool("iscrtans");
					Boolean isMig = false;
					if (isCrct != null && isCrct) {
						CustomerAnswerDVO customerAnswer = new CustomerAnswerDVO();
						customerAnswer.setConid(row.getString("conid"));
						customerAnswer.setCustid(row.getString("custid"));
						customerAnswer.setQsnbnkid(row.getInt("qsnbnkid"));
						customerAnswer.setClId(row.getString("clintid"));
						customerAnswer.setCustans(row.getString("custans"));
						customerAnswer.setCustplayid(row.getString("custplayid"));
						customerAnswer.setIscrtans(isCrct);
						customerAnswer.setQsnbnkans(row.getString("qsnbnkans"));
						customerAnswer.setQsnseqno(row.getInt("qsnseqno"));
						customerAnswer.setRwdtype(row.getString("rwdtype"));
						customerAnswer.setRwdvalue(row.getDouble("rwdval"));
						isMig = row.getBool("migrate");
						if (isMig == null || !isMig) {
							customerAnswers.add(customerAnswer);
						}
					}
				}
			}
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		} finally {
		}
		return customerAnswers;
	}

	/**
	 * Gets the customer que answered by contest questions ids.
	 *
	 * @param clId       the client Id
	 * @param custId      the Customer id
	 * @param coId        the Contest ID 
	 * @param custPlayId the customer play id
	 * @return the customer question answered by contest questions ids
	 * @throws Exception the exception
	 */
	public static List<Integer> getCustomerQueAnsweredByContestQuestionsIds(String clId, String custId, String coId,
			String custPlayId) throws Exception {
		ResultSet resultSet = null;
		List<Integer> qbIdList = null;
		try {
			resultSet = CassandraConnector.getSession().execute(
					" SELECT conqsnid from  pt_ofltx_customer_answer  "
							+ " WHERE clintid=? and conid = ? AND  custid=? and custplayid = ? ",
					clId, coId, custId, custPlayId);

			if (resultSet != null) {
				qbIdList = new ArrayList<Integer>();
				for (Row row : resultSet) {
					qbIdList.add(row.getInt("conqsnid"));

				}
			}
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		}
		return qbIdList;
	}

	/**
	 * Gets the customer question answered by questions ids.
	 *
	 * @param clId       the client Id
	 * @param custId      the Customer id
	 * @param coId        the Contest ID 
	 * @param custPlayId the cust play id
	 * @return the customer que answered by questions ids
	 * @throws Exception the exception
	 */
	public static List<Integer> getCustomerQueAnsweredByQuestionsIds(String clId, String custId, String coId,
			String custPlayId) throws Exception {
		ResultSet resultSet = null;
		List<Integer> qbIdList = null;
		try {
			resultSet = CassandraConnector.getSession().execute(
					" SELECT qsnbnkid from  pt_ofltx_customer_answer  "
							+ " WHERE clintid=? and conid = ? AND  custid=? and custplayid = ? ",
					clId, coId, custId, custPlayId);

			if (resultSet != null) {
				qbIdList = new ArrayList<Integer>();
				for (Row row : resultSet) {
					qbIdList.add(row.getInt("qsnbnkid"));

				}
			}
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		}
		return qbIdList;
	}

	/**
	 * Insert customer question answer details.
	 *
	 * @param obj the CustomerAnswerDVO obj
	 * @return the customer answer DVO
	 * @throws Exception the exception
	 */
	public static CustomerAnswerDVO insertCustomerQuenAnswerDetails(CustomerAnswerDVO obj) throws Exception {

		try {
			CassandraConnector.getSession().execute(" INSERT INTO pt_ofltx_customer_answer  "
					+ "(clintid,conid,custid,custplayid, custans, " + " conname ,qsnbnkans ,rwdtype, qsnbnkid, "
					+ " qsnseqno ,  consrtdate ,credate , upddate ,  " + " iscrtans, rwdval , conqsnid) VALUES "
					+ " (?,?,?, ?, ?, " + " ?,?,?,?, " + " ?, toTimestamp(now()), toTimestamp(now()), ?," + " ?,?,?) ",
					obj.getClId(), obj.getConid(), obj.getCustid(), obj.getCustplayid(), obj.getCustans(),
					obj.getConname(), obj.getQsnbnkans(), obj.getRwdtype(), obj.getQsnbnkid(), obj.getQsnseqno(),
					obj.getUpddate(), obj.isIscrtans(), obj.getRwdvalue(), obj.getConqsnid());

		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		}
		return obj;
	}

	/**
	 * Update customer answer migration status.
	 *
	 * @param obj the CustomerAnswerDVO object
	 * @return the boolean
	 * @throws Exception the exception
	 */
	public static Boolean updateCustomerAnswerMigrationStatus(CustomerAnswerDVO obj) throws Exception {
		Boolean isUpd = false;
		try {
			CassandraConnector.getSession().execute(
					" UPDATE pt_ofltx_customer_answer set " + " migrate = ? , upddate = toTimestamp(now())   "
							+ " WHERE clintid=? and conid=? AND custid=?  and custplayid=? and qsnseqno=?",
					true, obj.getClId(), obj.getConid(), obj.getCustid(), obj.getCustplayid(), obj.getQsnseqno());
			isUpd = true;
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		}
		return isUpd;
	}

	/**
	 * Update customer quen answer details.
	 *
	 * @param obj the obj
	 * @return the customer answer DVO
	 * @throws Exception the exception
	 */
	public static CustomerAnswerDVO updateCustomerQuenAnswerDetails(CustomerAnswerDVO obj) throws Exception {

		try {
			CassandraConnector.getSession().execute(
					" UPDATE pt_ofltx_customer_answer set " + " custans = ? , iscrtans = ? , "
							+ " rwdtype = ?  , rwdval = ? , upddate = toTimestamp(now())   "
							+ " WHERE clintid=? and conid=? AND custid=?  and custplayid=? and qsnseqno=?",
					obj.getCustans(), obj.isIscrtans(), obj.getRwdtype(), obj.getRwdvalue(), obj.getClId(),
					obj.getConid(), obj.getCustid(), obj.getCustplayid(), obj.getQsnseqno());
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		}
		return obj;
	}

}
