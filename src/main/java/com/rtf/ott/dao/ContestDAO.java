/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.ott.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.exceptions.DriverException;
import com.rtf.common.dspl.dvo.OttContestDVO;
import com.rtf.ott.dvo.ContestDVO;
import com.rtf.saas.db.CassandraConnector;
import com.rtf.saas.exception.RTFDataAccessException;
import com.rtf.saas.util.GenUtil;

/**
 * The Class ContestDAO.
 */
public class ContestDAO {

	/**
	 * Gets the all contest cards.
	 *
	 * @param clintId  the Client ID
	 * @return the all contest cards
	 * @throws Exception the exception
	 */
	public static List<OttContestDVO> getAllContestCards(String clintId) throws Exception {
		ResultSet resultSet = null;
		List<OttContestDVO> contests = new ArrayList<OttContestDVO>();
		try {
			resultSet = CassandraConnector.getSession().execute("SELECT * from  pa_ofltx_contest_mstr  WHERE clintid=?",
					clintId);
			if (resultSet != null) {
				for (Row row : resultSet) {
					OttContestDVO contestDVO = new OttContestDVO();
					contestDVO.setCoId(row.getString("conid"));
					contestDVO.setSeqNo(row.getInt("cardseqno"));
					contestDVO.setName(row.getString("conname"));
					contests.add(contestDVO);
				}
			}
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		}
		return contests;
	}

	/**
	 * Gets the all live contest.
	 *
	 * @param clintId  the Client ID
	 * @return the all live contest
	 * @throws Exception the exception
	 */
	public static List<ContestDVO> getAllLiveContest(String clintId) throws Exception {
		ResultSet resultSet = null;
		List<ContestDVO> contests = new ArrayList<ContestDVO>();
		try {
			resultSet = CassandraConnector.getSession().execute("SELECT * from  pa_ofltx_contest_mstr  WHERE clintid=?",
					clintId);
			if (resultSet != null) {
				for (Row row : resultSet) {
					ContestDVO contestDVO = new ContestDVO();
					Date date = null;
					contestDVO.setCoId(row.getString("conid"));
					contestDVO.setClId(row.getString("clintid"));
					contestDVO.setAnsType(row.getString("anstype"));
					contestDVO.setClImgU(row.getString("brndimgurl"));
					contestDVO.setImgU(row.getString("cardimgurl"));
					contestDVO.setSeqNo(row.getInt("cardseqno"));
					contestDVO.setCat(row.getString("cattype"));
					date = row.getTimestamp("conenddate");
					contestDVO.setEnDate(date != null ? date.getTime() : null);
					contestDVO.setName(row.getString("conname"));
					date = row.getTimestamp("consrtdate");
					contestDVO.setStDate(date != null ? date.getTime() : null);
					contestDVO.setIsElimination(row.getBool("iselimtype"));
					contestDVO.setqSize(row.getInt("noofqns"));
					contestDVO.setqMode(row.getString("qsnmode"));
					contestDVO.setRwdType(row.getString("rwdtype"));
					contestDVO.setRwdVal(row.getDouble("rwdval"));
					contestDVO.setSubCat(row.getString("subcattype"));
					contestDVO.setThmColor(row.getString("bgthemcolor"));
					contestDVO.setThmImgDesk(row.getString("bgthemimgurl"));
					contestDVO.setThmImgMob(row.getString("bgthemimgurlmob"));
					contests.add(contestDVO);
				}
			}
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		}
		return contests;
	}

	/**
	 * Gets the contest by contest id.
	 *
	 * @param clintId    the Client ID
	 * @param contestId the contest id
	 * @return the contest by contest id
	 * @throws Exception the exception
	 */
	public static ContestDVO getContestByContestId(String clintId, String contestId) throws Exception {
		ResultSet resultSet = null;
		ContestDVO contestDVO = null;
		try {
			resultSet = CassandraConnector.getSession()
					.execute("SELECT * from  pa_ofltx_contest_mstr  WHERE clintid=? AND conid=?", clintId, contestId);
			if (resultSet != null) {
				for (Row row : resultSet) {
					contestDVO = new ContestDVO();
					Date date = null;
					contestDVO.setCoId(row.getString("conid"));
					contestDVO.setClId(row.getString("clintid"));
					contestDVO.setAnsType(row.getString("anstype"));
					contestDVO.setClImgU(row.getString("brndimgurl"));
					contestDVO.setImgU(row.getString("cardimgurl"));
					contestDVO.setSeqNo(row.getInt("cardseqno"));
					contestDVO.setCat(row.getString("cattype"));
					date = row.getTimestamp("conenddate");
					contestDVO.setEnDate(date != null ? date.getTime() : null);
					contestDVO.setName(row.getString("conname"));
					date = row.getTimestamp("consrtdate");
					contestDVO.setStDate(date != null ? date.getTime() : null);
					contestDVO.setCrBy(row.getString("creby"));
					date = row.getTimestamp("credate");
					contestDVO.setCrDate(date != null ? date.getTime() : null);
					contestDVO.setIsAct(row.getInt("isconactive"));
					contestDVO.setIsElimination(row.getBool("iselimtype"));
					contestDVO.setqSize(row.getInt("noofqns"));
					contestDVO.setqMode(row.getString("qsnmode"));
					contestDVO.setRwdType(row.getString("rwdtype"));
					contestDVO.setRwdVal(row.getDouble("rwdval"));
					contestDVO.setSubCat(row.getString("subcattype"));
					contestDVO.setUpBy(row.getString("updby"));
					contestDVO.setThmColor(row.getString("bgthemcolor"));
					contestDVO.setThmImgDesk(row.getString("bgthemimgurl"));
					contestDVO.setThmImgMob(row.getString("bgthemimgurlmob"));
					contestDVO.setPlayGameImg(row.getString("playimgurl"));
					date = row.getTimestamp("upddate");
					contestDVO.setUpDate(date != null ? date.getTime() : null);

				}
			}
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		}
		return contestDVO;
	}

}
