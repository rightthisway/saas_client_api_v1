/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.ott.dao;

import java.util.HashMap;
import java.util.Map;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.exceptions.DriverException;
import com.rtf.ott.dvo.ContestQuestionDVO;
import com.rtf.ott.dvo.QuestionBankDVO;
import com.rtf.saas.db.CassandraConnector;
import com.rtf.saas.exception.RTFDataAccessException;
import com.rtf.saas.util.GenUtil;

/**
 * The Class ContestQuestionDAO.
 */
public class ContestQuestionDAO {

	/**
	 * Gets the all Offline Text contest questins.
	 *
	 * @param clId      the Client  ID
	 * @param contestId the contest id
	 * @return the all contest questins
	 */
	public static Map<Integer, ContestQuestionDVO> getAllContestQuestins(String clId, String contestId)
			throws Exception {

		ResultSet resultSet = null;

		ContestQuestionDVO dvo = null;
		Map<Integer, ContestQuestionDVO> queMap = null;
		try {
			resultSet = CassandraConnector.getSession().execute(
					"SELECT * from  pa_ofltx_contest_question_only_active  " + "WHERE clintid=?   and conid = ?  ",
					clId, contestId);

			if (resultSet != null) {
				queMap = new HashMap<Integer, ContestQuestionDVO>();
				for (Row row : resultSet) {
					dvo = new ContestQuestionDVO();
					Integer conQId = row.getInt("conqsnid");
					dvo.setConqsnid(conQId);
					dvo.setClId(row.getString("clintid"));
					dvo.setIsactive(row.getBool("isactive"));
					dvo.setConid(row.getString("conid"));
					dvo.setQtx(row.getString("qsntext"));
					dvo.setOpa(row.getString("ansopta"));
					dvo.setOpb(row.getString("ansoptb"));
					dvo.setOpc(row.getString("ansoptc"));
					dvo.setOpd(row.getString("ansoptd"));

					dvo.setCans(row.getString("corans"));
					dvo.setQsnorient(row.getString("qsnorient"));
					dvo.setRwdtype(row.getString("rwdtype"));
					dvo.setRwdvalue(row.getDouble("rwdval"));
					dvo.setQsnseqno(row.getInt("qsnseqno"));
					queMap.put(conQId, dvo);

				}
			}

		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		}
		return queMap;
	}

	/**
	 * Gets the all Offline Text Trivia questions based on category.
	 *
	 * @param clintId the Client  ID
	 * @param catType  the Category Type
	 * @return the all OLTX questions based on category
	 * @throws Exception the exception
	 */
	public static Map<Integer, QuestionBankDVO> getAllOLTXQuestionsBasedOnCategory(String clintId, String catType)
			throws Exception {

		ResultSet resultSet = null;
		Map<Integer, QuestionBankDVO> qmap = null;
		try {
			resultSet = CassandraConnector.getSession().execute(
					"SELECT * from  pa_ofltx_question_bank_only_active  WHERE clintid=? AND isact=? and cattype = ?",
					clintId, true, catType);

			qmap = new HashMap<Integer, QuestionBankDVO>();
			if (resultSet != null) {
				for (Row row : resultSet) {
					QuestionBankDVO questionBankDVO = new QuestionBankDVO();
					Integer qbId = row.getInt("qsnbnkid");
					questionBankDVO.setQbid(qbId);
					questionBankDVO.setClId(row.getString("clintid"));
					questionBankDVO.setIsact(row.getBool("isact"));
					questionBankDVO.setQtx(row.getString("qnstext"));
					questionBankDVO.setOpa(row.getString("ansopta"));
					questionBankDVO.setOpb(row.getString("ansoptb"));
					questionBankDVO.setOpc(row.getString("ansoptc"));
					questionBankDVO.setOpd(row.getString("ansoptd"));
					questionBankDVO.setCans(row.getString("corans"));
					questionBankDVO.setCtyp(row.getString("cattype"));
					questionBankDVO.setSctyp(row.getString("subcattype"));
					questionBankDVO.setqOrientn(row.getString("qsnorient"));
					qmap.put(qbId, questionBankDVO);
				}
			}
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		}
		return qmap;
	}

	/**
	 * Gets the all OLTX questions based on catn sub category.
	 *
	 * @param clintId the Client  ID
	 * @param catType  the Category Type
	 * @param subCat   the Sub  Category Type
	 * @return the all OLTX questions based on catn sub category
	 * @throws Exception the exception
	 */
	public static Map<Integer, QuestionBankDVO> getAllOLTXQuestionsBasedOnCatnSubCategory(String clintId,
			String catType, String subCat) throws Exception {

		ResultSet resultSet = null;
		Map<Integer, QuestionBankDVO> qmap = null;

		try {
			resultSet = CassandraConnector.getSession().execute(
					"SELECT * from  pa_ofltx_question_bank_only_active  WHERE clintid=? AND isact=? and cattype = ? and subcattype = ? ",
					clintId, true, catType, subCat);

			qmap = new HashMap<Integer, QuestionBankDVO>();
			if (resultSet != null) {
				for (Row row : resultSet) {
					QuestionBankDVO questionBankDVO = new QuestionBankDVO();
					Integer qbId = row.getInt("qsnbnkid");
					questionBankDVO.setQbid(qbId);
					questionBankDVO.setClId(row.getString("clintid"));
					questionBankDVO.setIsact(row.getBool("isact"));
					questionBankDVO.setQtx(row.getString("qnstext"));
					questionBankDVO.setOpa(row.getString("ansopta"));
					questionBankDVO.setOpb(row.getString("ansoptb"));
					questionBankDVO.setOpc(row.getString("ansoptc"));
					questionBankDVO.setOpd(row.getString("ansoptd"));
					questionBankDVO.setCans(row.getString("corans"));
					questionBankDVO.setCtyp(row.getString("cattype"));
					questionBankDVO.setSctyp(row.getString("subcattype"));
					qmap.put(qbId, questionBankDVO);
				}
			}
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		}
		return qmap;
	}

	/**
	 * Gets the contest question by sequence number.
	 *
	 * @param clId   the Client  ID
	 * @param conId   the Contest ID 
	 * @param qSlNum the question serial number
	 * 	 * @return the contest question by seq num
	 * @throws Exception the exception
	 */

	public static ContestQuestionDVO getContestQuestionBySeqNum(String clId, String conId, Integer qSlNum)
			throws Exception {

		ResultSet resultSet = null;
		ContestQuestionDVO dvo = null;

		try {
			resultSet = CassandraConnector.getSession().execute(
					"SELECT * from  pa_ofltx_contest_question_only_active  " + "WHERE clintid=?   and conid = ?  ",
					clId, conId);

			dvo = new ContestQuestionDVO();
			if (resultSet != null) {
				for (Row row : resultSet) {
					Integer qbId = row.getInt("qsnbnkid");
					dvo.setQbid(qbId);
					dvo.setClId(row.getString("clintid"));
					dvo.setIsactive(row.getBool("isactive"));
					dvo.setConid(row.getString("conid"));
					dvo.setQtx(row.getString("qsntext"));
					dvo.setOpa(row.getString("ansopta"));
					dvo.setOpb(row.getString("ansoptb"));
					dvo.setOpc(row.getString("ansoptc"));
					dvo.setOpd(row.getString("ansoptd"));
					dvo.setConqsnid(row.getInt("conqsnid"));
					dvo.setCans(row.getString("corans"));
					dvo.setQsnorient(row.getString("qsnorient"));
					dvo.setRwdtype(row.getString("rwdtype"));
					dvo.setRwdvalue(row.getDouble("rwdval"));
					dvo.setQsnseqno(row.getInt("qsnseqno"));

				}
			}

		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		}
		return dvo;
	}

}
