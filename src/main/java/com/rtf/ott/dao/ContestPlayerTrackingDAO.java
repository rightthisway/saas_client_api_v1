/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.ott.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.exceptions.DriverException;
import com.rtf.ott.dvo.ContestPlayerTrackingDVO;
import com.rtf.saas.db.CassandraConnector;
import com.rtf.saas.exception.RTFDataAccessException;
import com.rtf.saas.util.GenUtil;

/**
 * The Class ContestPlayerTrackingDAO.
 */
public class ContestPlayerTrackingDAO {

	/**
	 * Gets the contest tracking by cust cont play id.
	 *
	 * @param clintId  the Client ID
	 * @param coId     the Contest ID 
	 * @param cuId     the Customer id
	 * @param playId  the play id
	 * @return the contest tracking by cust cont play id
	 * @throws Exception the exception
	 */
	public static ContestPlayerTrackingDVO getContestTrackingByCustContPlayId(String clintId, String coId, String cuId,
			String playId) throws Exception {
		ContestPlayerTrackingDVO tracking = null;
		try {
			ResultSet resultSet = CassandraConnector.getSession().execute(
					"SELECT * from  pt_ofltx_customer_played_tracking  WHERE clintid=? AND conid = ? AND custid=? AND custplayid=?",
					new Object[] { clintId, coId, cuId, playId });
			if (resultSet != null) {
				tracking = new ContestPlayerTrackingDVO();
				for (Row row : resultSet) {

					Date date = null;
					tracking.setCoId(row.getString("conid"));
					tracking.setClId(row.getString("clintid"));
					tracking.setCuId(row.getString("custid"));
					tracking.setPlayerId(row.getString("custplayid"));
					date = row.getTimestamp("played_dttm");
					tracking.setPlayTime(date != null ? date.getTime() : null);
					tracking.setqCnt(row.getInt("qsncnt"));
					tracking.setSts(row.getString("status"));

				}
			}
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		}
		return tracking;
	}

	/**
	 * Gets the contest tracking by customer.
	 *
	 * @param clintId  the Client ID
	 * @param coId     the Contest ID 
	 * @param cuId     the Customer id
	 * @return the contest tracking by customer
	 * @throws Exception the exception
	 */
	public static List<ContestPlayerTrackingDVO> getContestTrackingByCustomer(String clintId, String coId, String cuId)
			throws Exception {
		List<ContestPlayerTrackingDVO> trackings = new ArrayList<ContestPlayerTrackingDVO>();
		try {
			ResultSet resultSet = CassandraConnector.getSession().execute(
					"SELECT * from  pt_ofltx_customer_played_tracking  WHERE clintid=? AND conid = ? AND custid=? ",
					new Object[] { clintId, coId, cuId });
			if (resultSet != null) {
				for (Row row : resultSet) {
					ContestPlayerTrackingDVO tracking = new ContestPlayerTrackingDVO();
					Date date = null;
					tracking.setCoId(row.getString("conid"));
					tracking.setClId(row.getString("clintid"));
					tracking.setCuId(row.getString("custid"));
					tracking.setPlayerId(row.getString("custplayid"));
					date = row.getTimestamp("played_dttm");
					tracking.setPlayTime(date != null ? date.getTime() : null);
					tracking.setqCnt(row.getInt("qsncnt"));
					tracking.setSts(row.getString("status"));
					trackings.add(tracking);
				}
			}
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		}
		return trackings;
	}

	/**
	 * Gets the contest tracking Object  By play id.
	 *
	 * @param clintId  the Client ID
	 * @param coId     the Contest ID 
	 * @param cuId     the Customer id
	 * @param playId  the play id
	 * @return the contest tracking by play id
	 * @throws Exception the exception
	 */
	public static ContestPlayerTrackingDVO getContestTrackingByPlayId(String clintId, String coId, String cuId,
			String playId) throws Exception {
		ContestPlayerTrackingDVO tracking = null;
		try {
			ResultSet resultSet = CassandraConnector.getSession().execute(
					"SELECT * from  pt_ofltx_customer_played_tracking  WHERE clintid=? AND conid = ? AND custid=? AND custplayid = ? ",
					new Object[] { clintId, coId, cuId, playId });
			if (resultSet != null) {
				for (Row row : resultSet) {
					Date date = null;
					tracking = new ContestPlayerTrackingDVO();
					tracking.setCoId(row.getString("conid"));
					tracking.setClId(row.getString("clintid"));
					tracking.setCuId(row.getString("custid"));
					tracking.setPlayerId(row.getString("custplayid"));
					tracking.setPlayTime(date != null ? date.getTime() : null);
					tracking.setqCnt(row.getInt("qsncnt"));
					tracking.setSts(row.getString("status"));
				}
			}
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		}
		return tracking;
	}

	/**
	 * Save contest tracking.
	 *
	 * @param t the ContestPlayerTrackingDVO
	 * @return the boolean
	 * @throws Exception the exception
	 */
	public static Boolean saveContestTracking(ContestPlayerTrackingDVO t) throws Exception {
		try {
			CassandraConnector.getSession().execute(
				//	"INSERT INTO pt_ofltx_customer_played_tracking (clintid,conid,custid,custplayid,played_dttm,qsncnt,status)  VALUES (?,?,?, ?,toTimestamp(now()),?,?)",
					"INSERT INTO pt_ofltx_customer_played_tracking (clintid,conid,custid,custplayid,played_dttm,qsncnt,status)  VALUES (?,?,?, ?,?,?,?)",
					t.getClId(), t.getCoId(), t.getCuId(), t.getPlayerId(), new Date().getTime(),t.getqCnt(), t.getSts());
			return true;
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		}
	}

	/**
	 * Update contest tracking.
	 *
	 * @param t the ContestPlayerTrackingDVO
	 * @return the boolean
	 * @throws Exception the exception
	 */
	public static Boolean updateContestTracking(ContestPlayerTrackingDVO t) throws Exception {
		try {
			CassandraConnector.getSession().execute(
					"UPDATE pt_ofltx_customer_played_tracking set status=? WHERE clintid=? AND conid = ? AND custid=? AND custplayid = ? ",
					new Object[] { t.getSts(), t.getClId(), t.getCoId(), t.getCuId(), t.getPlayerId() });
			return true;
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		}
	}

	/**
	 * Update contest tracking.
	 *
	 * @param t     the ContestPlayerTrackingDVO
	 * @param qCnt  the question cnt
	 * @param pType the type
	 * @return the boolean
	 * @throws Exception the exception
	 */
	public static Boolean updateContestTracking(ContestPlayerTrackingDVO t, Integer qCnt, String pType)
			throws Exception {
		try {
			CassandraConnector.getSession().execute(
					"UPDATE pt_ofltx_customer_played_tracking "
							+ " set qsncnt = ? , ptype = ? , played_dttm = toTimestamp(now())  WHERE "
							+ "clintid=? AND conid = ? and  custid = ? and  custplayid = ?  ",
					qCnt, pType, t.getClId(), t.getCoId(), t.getCuId(), t.getPlayerId());

			return true;
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		}
	}

}
