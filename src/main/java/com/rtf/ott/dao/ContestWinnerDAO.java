/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.ott.dao;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.exceptions.DriverException;
import com.rtf.ott.dvo.ContestWinnerDVO;
import com.rtf.saas.db.CassandraConnector;
import com.rtf.saas.exception.RTFDataAccessException;
import com.rtf.saas.util.GenUtil;

/**
 * The Class ContestWinnerDAO.
 */
public class ContestWinnerDAO {

	/**
	 * Gets the contest winner Object.
	 *
	 * @param clId     the Client ID
	 * @param cuId     the Customer id
	 * @param coId    the Contest ID 
	 * @param playId  the play id
	 * @param rwdType the Reward Type
	 * @return the contest winner
	 * @throws Exception the exception
	 */
	public static ContestWinnerDVO getContestWinner(String clId, String cuId, String coId, String playId,
			String rwdType) throws Exception {
		ResultSet resultSet = null;
		ContestWinnerDVO dvo = null;
		try {
			resultSet = CassandraConnector.getSession().execute(
					"SELECT * from  pt_ofltx_contest_winner  "
							+ " WHERE clintId=? and conid=? AND custid=? and rwdtype=? and custplayid=? ",
					clId, coId, cuId, rwdType, playId);
			if (resultSet != null) {
				for (Row row : resultSet) {
					dvo = new ContestWinnerDVO();
					dvo.setClId(row.getString("clintid"));
					dvo.setCoId(row.getString("conid"));
					dvo.setCoName(row.getString("conname"));
					dvo.setCuId(row.getString("custid"));
					dvo.setRwdType(row.getString("rwdtype"));
					dvo.setRwdVal(row.getDouble("rwdval"));
					dvo.setSts(row.getString("status"));
					dvo.setPlayId(row.getString("custplayid"));
				}
			}
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		}
		return dvo;
	}

	/**
	 * Save contest winner.
	 *
	 * @param w the ContestWinnerDVO Object
	 * @return the boolean
	 * @throws Exception the exception
	 */
	public static Boolean saveContestWinner(ContestWinnerDVO w) throws Exception {
		try {
			CassandraConnector.getSession().execute(
					"INSERT INTO pt_ofltx_contest_winner (clintid,conid,custid,rwdtype,conname,consrtdate,creby,credate,rwdval,status,updby,upddate,custplayid)  "
							+ "VALUES (?,?,?, ?,?,?,?,toTimestamp(now()), ?,?,?,?,?)",
					w.getClId(), w.getCoId(), w.getCuId(), w.getRwdType(), w.getCoName(), w.getCoDate(), w.getCrBy(),
					w.getRwdVal(), w.getSts(), w.getUpBy(), w.getUpDate(), w.getPlayId());
			return true;
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		}
	}

	/**
	 * Update winner migration status.
	 *
	 * @param w the ContestWinnerDVO Object
	 * @return the boolean
	 * @throws Exception the exception
	 */
	public static Boolean updateWinnerMigrationStatus(ContestWinnerDVO w) throws Exception {
		try {
			CassandraConnector.getSession().execute(
					"UPDATE pt_ofltx_contest_winner SET migrate= ?  WHERE "
							+ "clintid=? and conid=? and custid=? and rwdtype=? and custplayid=?",
					true, w.getClId(), w.getCoId(), w.getCuId(), w.getRwdType(), w.getPlayId());
			return true;
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		}
	}

}
