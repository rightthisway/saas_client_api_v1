/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.ott.dao;

import java.util.HashMap;
import java.util.Map;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.exceptions.DriverException;
import com.rtf.ott.dvo.QuestionBankDVO;
import com.rtf.saas.db.CassandraConnector;
import com.rtf.saas.exception.RTFDataAccessException;
import com.rtf.saas.util.GenUtil;

/**
 * The Class QuestionBankDAO.
 */
public class QuestionBankDAO {

	/**
	 * Gets the all Offline Text Trivia questions.
	 *
	 * @param clintId  the Client ID
	 * @return the all OLTX questions
	 * @throws Exception the exception
	 */
	public static Map<Integer, QuestionBankDVO> getAllOLTXQuestions(String clintId) throws Exception {

		ResultSet resultSet = null;
		Map<Integer, QuestionBankDVO> qmap = null;

		try {
			resultSet = CassandraConnector.getSession().execute(
					"SELECT * from  pa_ofltx_question_bank_only_active  WHERE clintid=? AND isact=? ", clintId, true);

			qmap = new HashMap<Integer, QuestionBankDVO>();
			if (resultSet != null) {
				for (Row row : resultSet) {
					QuestionBankDVO questionBankDVO = new QuestionBankDVO();
					Integer qbId = row.getInt("qsnbnkid");
					questionBankDVO.setQbid(qbId);
					questionBankDVO.setIsact(row.getBool("isact"));
					questionBankDVO.setClId(row.getString("clintId"));
					questionBankDVO.setQtx(row.getString("qnstext"));
					questionBankDVO.setOpa(row.getString("ansopta"));
					questionBankDVO.setOpb(row.getString("ansoptb"));
					questionBankDVO.setOpc(row.getString("ansoptc"));
					questionBankDVO.setOpd(row.getString("ansoptd"));
					questionBankDVO.setCans(row.getString("corans"));
					questionBankDVO.setCtyp(row.getString("cattype"));
					questionBankDVO.setSctyp(row.getString("subcattype"));
					qmap.put(qbId, questionBankDVO);
				}
			}
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		}
		return qmap;
	}

	/**
	 * Gets the all OLTX questions based on category.
	 *
	 * @param clintId  the Client ID
	 * @param ansType the answer type
	 * @param catType  the Category Type
	 * @return the all OLTX questions based on category
	 * @throws Exception the exception
	 */
	public static Map<Integer, QuestionBankDVO> getAllOLTXQuestionsBasedOnCategory(String clintId, String ansType,
			String catType) throws Exception {

		ResultSet resultSet = null;
		Map<Integer, QuestionBankDVO> qmap = null;

		try {
			resultSet = CassandraConnector.getSession().execute(
					"SELECT * from  pa_ofltx_question_bank_only_active  WHERE clintid=? and anstype=?  and cattype = ?",
					clintId, ansType, catType);

			qmap = new HashMap<Integer, QuestionBankDVO>();
			if (resultSet != null) {
				for (Row row : resultSet) {
					QuestionBankDVO questionBankDVO = new QuestionBankDVO();
					Integer qbId = row.getInt("qsnbnkid");
					questionBankDVO.setQbid(qbId);
					questionBankDVO.setIsact(row.getBool("isactive"));
					questionBankDVO.setClId(row.getString("clintid"));
					questionBankDVO.setQtx(row.getString("qsntext"));
					questionBankDVO.setOpa(row.getString("ansopta"));
					questionBankDVO.setOpb(row.getString("ansoptb"));
					questionBankDVO.setOpc(row.getString("ansoptc"));
					questionBankDVO.setOpd(row.getString("ansoptd"));
					questionBankDVO.setCans(row.getString("corans"));
					questionBankDVO.setCtyp(row.getString("cattype"));
					questionBankDVO.setSctyp(row.getString("subcattype"));
					questionBankDVO.setqOrientn(row.getString("qsnorient"));
					questionBankDVO.setQsnmode(row.getString("qsnmode"));

					qmap.put(qbId, questionBankDVO);
				}
			}
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		}
		return qmap;
	}

	/**
	 * Gets the all OLTX questions based on categoryand sub cat.
	 *
	 * @param clintId     the Client ID
	 * @param ansType    the ans type
	 * @param catType     the Category Type
	 * @param subCatType  the Sub  Category Type type
	 * @return the all OLTX questions based on categoryand sub cat
	 * @throws Exception the exception
	 */
	public static Map<Integer, QuestionBankDVO> getAllOLTXQuestionsBasedOnCategoryandSubCat(String clintId,
			String ansType, String catType, String subCatType) throws Exception {

		ResultSet resultSet = null;
		Map<Integer, QuestionBankDVO> qmap = null;

		try {
			resultSet = CassandraConnector.getSession().execute(
					"SELECT * from  pa_ofltx_question_bank_only_active  WHERE clintid=? AND anstype=? and cattype = ? and subcattype = ? ",
					clintId, ansType, catType, subCatType);

			qmap = new HashMap<Integer, QuestionBankDVO>();
			if (resultSet != null) {
				for (Row row : resultSet) {
					QuestionBankDVO questionBankDVO = new QuestionBankDVO();
					Integer qbId = row.getInt("qsnbnkid");
					questionBankDVO.setQbid(qbId);
					questionBankDVO.setClId(row.getString("clintid"));
					questionBankDVO.setQtx(row.getString("qsntext"));
					questionBankDVO.setOpa(row.getString("ansopta"));
					questionBankDVO.setOpb(row.getString("ansoptb"));
					questionBankDVO.setOpc(row.getString("ansoptc"));
					questionBankDVO.setOpd(row.getString("ansoptd"));
					questionBankDVO.setCans(row.getString("corans"));
					questionBankDVO.setCtyp(row.getString("cattype"));
					questionBankDVO.setSctyp(row.getString("subcattype"));
					questionBankDVO.setqOrientn(row.getString("qsnorient"));
					questionBankDVO.setQsnmode(row.getString("qsnmode"));
					qmap.put(qbId, questionBankDVO);
				}
			}
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		}
		return qmap;
	}

	/**
	 * Gets the all OLTX questions based on catn sub category.
	 *
	 * @param clintId  the Client ID
	 * @param catType  the Category Type
	 * @param subCat   the Sub  Category Type
	 * @return the all OLTX questions based on catn sub category
	 * @throws Exception the exception
	 */
	public static Map<Integer, QuestionBankDVO> getAllOLTXQuestionsBasedOnCatnSubCategory(String clintId,
			String catType, String subCat) throws Exception {

		ResultSet resultSet = null;
		Map<Integer, QuestionBankDVO> qmap = null;

		try {
			resultSet = CassandraConnector.getSession().execute(
					"SELECT * from  pa_ofltx_question_bank_only_active  WHERE clintid=? AND isact=? and cattype = ? and subcattype = ? ",
					clintId, true, catType, subCat);

			qmap = new HashMap<Integer, QuestionBankDVO>();
			if (resultSet != null) {
				for (Row row : resultSet) {
					QuestionBankDVO questionBankDVO = new QuestionBankDVO();
					Integer qbId = row.getInt("qsnbnkid");
					questionBankDVO.setQbid(qbId);
					questionBankDVO.setClId(row.getString("clintid"));
					questionBankDVO.setIsact(row.getBool("isact"));
					questionBankDVO.setQtx(row.getString("qnstext"));
					questionBankDVO.setOpa(row.getString("ansopta"));
					questionBankDVO.setOpb(row.getString("ansoptb"));
					questionBankDVO.setOpc(row.getString("ansoptc"));
					questionBankDVO.setOpd(row.getString("ansoptd"));
					questionBankDVO.setCans(row.getString("corans"));
					questionBankDVO.setCtyp(row.getString("cattype"));
					questionBankDVO.setSctyp(row.getString("subcattype"));
					qmap.put(qbId, questionBankDVO);
				}
			}
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		}
		return qmap;
	}

}
