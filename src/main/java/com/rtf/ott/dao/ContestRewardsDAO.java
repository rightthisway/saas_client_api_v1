/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.ott.dao;

import java.util.ArrayList;
import java.util.List;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.exceptions.DriverException;
import com.rtf.ott.dvo.ContestRewardsDVO;
import com.rtf.saas.db.CassandraConnector;
import com.rtf.saas.exception.RTFDataAccessException;
import com.rtf.saas.util.GenUtil;

/**
 * The Class ContestRewardsDAO.
 */
public class ContestRewardsDAO {

	/**
	 * Gets the active contest rewards by contest id.
	 *
	 * @param clId       the Client ID
	 * @param contestId the contest id
	 * @return the active contest rewards by contest id
	 * @throws Exception the exception
	 */
	public static List<ContestRewardsDVO> getActiveContestRewardsByContestId(String clId, String contestId)
			throws Exception {
		ResultSet resultSet = null;
		List<ContestRewardsDVO> contestRewards = new ArrayList<ContestRewardsDVO>();
		ContestRewardsDVO contestRewardDVO = null;
		try {
			resultSet = CassandraConnector.getSession()
					.execute("SELECT * from  pa_ofltx_contest_rewards  WHERE clintid=? and conid=?", clId, contestId);
			if (resultSet != null) {
				for (Row row : resultSet) {
					contestRewardDVO = new ContestRewardsDVO();
					contestRewardDVO.setClId(row.getString("clintid"));
					contestRewardDVO.setIsactive(row.getBool("isactive"));
					contestRewardDVO.setConid(row.getString("conid"));
					contestRewardDVO.setRwdtype(row.getString("rwdtype"));
					contestRewardDVO.setRwdvalue(row.getDouble("rwdval"));
					if (contestRewardDVO.getRwdvalue() != null && contestRewardDVO.getRwdvalue() > 0) {
						contestRewards.add(contestRewardDVO);
					}
				}
			}
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		}
		return contestRewards;
	}

	/**
	 * Insert contest rewards.
	 *
	 * @param obj the ContestRewardsDVO Object
	 * @return the contest rewards DVO
	 * @throws Exception the exception
	 */
	public static ContestRewardsDVO insertContestRewards(ContestRewardsDVO obj) throws Exception {
		try {
			CassandraConnector.getSession().execute(
					" INSERT INTO pa_ofltx_contest_rewards  " + "(clintid,isactive,conid,rwdtype,rwdvalue) VALUES "
							+ " (?,?,?, ?, ?) ",
					obj.getClId(), obj.getIsactive(), obj.getConid(), obj.getRwdtype(), obj.getRwdvalue());

		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		}
		return obj;
	}

}
