/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.snw.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GenUtil;
import com.rtf.saas.util.GsonUtil;
import com.rtf.saas.util.SAASMessages;
import com.rtf.snw.dto.SNWQuestionAnswerDTO;
import com.rtf.snw.service.SNWQuestionService;

/**
 * The Class SNWFetchQuestionServlet.
 */
@WebServlet("/snwgq.json")
public class SNWFetchQuestionServlet extends RtfSaasBaseServlet {

	/**
	 *
	 */
	private static final long serialVersionUID = 1241862044004612457L;

	/**
	 * Generate Http Response for Client.Response, data is sent in JSON format.
	 *
	 * @param request        the HttpServlet request
	 * @param response       the HttpServlet response
	 * @param rtfSaasBaseDTO RTF Base Data Transfer Object
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("ottresp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();

	}

	/**
	 * Process request. Method used to Fetch Question for Scratch and Win Trivia
	 *
	 * @param request  the HttpServletRequest
	 * @param response the HttpServletResponse
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		SNWQuestionAnswerDTO dto = new SNWQuestionAnswerDTO();

		try {
			String cuId = request.getParameter("cuId");
			String clientId = request.getParameter("clientId");
			String coId = request.getParameter("coId");

			if (GenUtil.isEmptyString(cuId)) {
				setClientMessage(dto, SAASMessages.GEN_ERR_MSG, null);
				generateResponse(request, response, dto);
				return;
			}
			if (GenUtil.isEmptyString(clientId)) {
				setClientMessage(dto, SAASMessages.GEN_ERR_MSG, null);
				generateResponse(request, response, dto);
				return;
			}
			if (GenUtil.isEmptyString(coId)) {
				setClientMessage(dto, SAASMessages.GEN_ERR_MSG, null);
				generateResponse(request, response, dto);
				return;
			}

			dto.setClId(clientId);
			dto.setCoId(coId);
			dto.setCuID(cuId);

			dto = SNWQuestionService.fetchQuestionInfo(dto);
			setClientMessage(dto, SAASMessages.GEN_ERR_MSG, null);
			generateResponse(request, response, dto);
		} catch (Exception ex) {
			dto.setSts(0);
			setClientMessage(dto, SAASMessages.GEN_ERR_MSG, null);
			generateResponse(request, response, dto);
		}

	}

}
