/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.snw.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rtf.common.dao.RewardTypeDAO;
import com.rtf.common.util.Messages;
import com.rtf.ott.dto.OTTValidateAnswerDTO;
import com.rtf.ott.service.OTTService;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GenUtil;
import com.rtf.saas.util.GsonUtil;
import com.rtf.saas.util.SAASMessages;
import com.rtf.snw.dao.SNWContestRewardDAO;
import com.rtf.snw.dao.SNWQuestionAnswerDAO;
import com.rtf.snw.dvo.ContestDVO;
import com.rtf.snw.dvo.SNWContestRewardDVO;
import com.rtf.snw.dvo.SNWCustomerAnswerDVO;
import com.rtf.snw.service.SNWContestService;
import com.rtf.snw.service.SNWQuestionService;

/**
 * The Class SNWValidateAnswerServlet.
 */
@WebServlet("/SnwValAns.json")
public class SNWValidateAnswerServlet extends RtfSaasBaseServlet {

	/** The Constant serialVersionUID. */

	private static final long serialVersionUID = 3784813588514510146L;

	/**
	 * Generate Http Response for Client.Response, data is sent in JSON format.
	 *
	 * @param request        the HttpServlet request
	 * @param response       the HttpServlet response
	 * @param rtfSaasBaseDTO RTF Base Data Transfer Object
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("snwresp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();

	}

	/**
	 * Process request.Method to validate Answer for Scratch and Win Trivia.
	 *
	 * @param request  the HttpServletRequest
	 * @param response the HttpServletResponse
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		OTTValidateAnswerDTO resDto = new OTTValidateAnswerDTO();

		try {

			String cuIdStr = request.getParameter("cuId");
			String clientIdStr = request.getParameter("clientId");
			request.getParameter("qId");
			String playIdStr = request.getParameter("playId");
			String contestIdStr = request.getParameter("coId");			
			String answer = request.getParameter("ans");

			resDto.setClId(clientIdStr);
			resDto.setCuID(cuIdStr);
			resDto.setPlayId(playIdStr);
			resDto.setCoId(contestIdStr);

			ContestDVO contest = SNWContestService.fetchContestInfoById(resDto.getClId(), resDto.getCoId());
			if (contest == null || GenUtil.isEmptyString(contest.getCoId())) {
				resDto.setSts(0);
				setClientMessage(resDto, SAASMessages.GEN_ERR_MSG, null);
				generateResponse(request, response, resDto);
				return;
			}

			SNWCustomerAnswerDVO custAns = SNWQuestionAnswerDAO.getCustomerAnswers(clientIdStr, contestIdStr, cuIdStr,
					playIdStr);
			if (custAns == null) {
				resDto.setSts(0);
				setClientMessage(resDto, SAASMessages.GEN_ERR_MSG, null);
				generateResponse(request, response, resDto);
				return;
			}

			if (custAns.getCustans() != null) {
				resDto.setSts(0);
				setClientMessage(resDto, SAASMessages.GEN_ERR_MSG, null);
				generateResponse(request, response, resDto);
				return;
			}

			Boolean isCrtAns = false;
			String crtAns = custAns.getQsnbnkans();
			if (answer != null) {
				if (custAns.getQsnbnkans() == null || custAns.getQsnbnkans().equals(answer)) {
					crtAns = answer;
					isCrtAns = true;
				}
			}

			if (isCrtAns) {
				List<SNWContestRewardDVO> contRewards = SNWContestRewardDAO
						.getActiveContestRewardsByContestId(clientIdStr, contestIdStr);
				if (contRewards != null && contRewards.size() > 0) {
					Collections.shuffle(contRewards);
					SNWContestRewardDVO contReward = contRewards.get(0);

					custAns.setRwdtype(contReward.getType());
					custAns.setRwdvalue(contReward.getRwdVal());

					resDto.setRwdDsc(Messages.SNW_CONGRATS_WON);
					resDto.setRwdTxt(OTTService.getRoundedRwdVal(custAns.getRwdvalue()) + " " + custAns.getRwdtype());
					if (custAns.getRwdtype() != null) {
						String rewardImageUrl = RewardTypeDAO.getRewardTypeImageUrl(clientIdStr, custAns.getRwdtype());
						if (rewardImageUrl != null) {
							resDto.setRwdImg(rewardImageUrl);
						}
					}

				} else {
					resDto.setRwdDsc(Messages.SNW_MSG1);
					resDto.setRwdTxt(Messages.SNW_MSG2);
				}

				resDto.setIsCrtAns(true);
				custAns.setIscrtans(true);
				resDto.setIsEle(false);

			} else {
				custAns.setRwdtype(null);
				custAns.setRwdvalue(null);
				custAns.setIscrtans(false);
				resDto.setIsCrtAns(false);
				resDto.setRwdDsc(Messages.SNW_MSG3);
				resDto.setRwdTxt(Messages.SNW_MSG4);
			}
			resDto.setHnxtQst(false);
			resDto.setCrtAns(crtAns);

			SNWQuestionService.updateCustomerQuenAnswerDetails(custAns);

			resDto.setSts(1);
			setClientMessage(resDto, null, SAASMessages.GEN_SUCCESS_MSG);
			generateResponse(request, response, resDto);
			return;

		} catch (Exception e) {
			e.printStackTrace();
			resDto.setSts(0);
			setClientMessage(resDto, SAASMessages.GEN_ERR_MSG, null);
			generateResponse(request, response, resDto);
			return;
		}

	}

}
