/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.snw.dto;

import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.snw.dvo.SNWCustomerAnswerDVO;

/**
 * The Class SNWQuestionAnswerDTO.
 */
public class SNWQuestionAnswerDTO extends RtfSaasBaseDTO {

	/** The cu ansdvo. */
	private SNWCustomerAnswerDVO cuAnsdvo;

	/**
	 * Gets the cu ansdvo.
	 *
	 * @return the cu ansdvo
	 */
	public SNWCustomerAnswerDVO getCuAnsdvo() {
		return cuAnsdvo;
	}

	/**
	 * Sets the cu ansdvo.
	 *
	 * @param cuAnsdvo the new cu ansdvo
	 */
	public void setCuAnsdvo(SNWCustomerAnswerDVO cuAnsdvo) {
		this.cuAnsdvo = cuAnsdvo;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "SNWQuestionAnswerDTO [cuAnsdvo=" + cuAnsdvo + ", getPlayId()=" + getPlayId() + ", getCoId()="
				+ getCoId() + ", toString()=" + super.toString() + ", getMsg()=" + getMsg() + ", getSts()=" + getSts()
				+ ", getCuID()=" + getCuID() + ", getClId()=" + getClId() + ", getClass()=" + getClass()
				+ ", hashCode()=" + hashCode() + "]";
	}

}
