/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.snw.dvo;

/**
 * The Class SNWCustomerAnswerDVO.
 */
public class SNWCustomerAnswerDVO {

	/** The clintid. */
	private String clintid;

	/** The conid. */
	private String conid;

	/** The custid. */
	private String custid;

	/** The custplayid. */
	private String custplayid;

	/** The conqsnid. */
	private Integer conqsnid;

	/** The credate. */
	private long credate;

	/** The custans. */
	private String custans;

	/** The iscrtans. */
	private boolean iscrtans;

	/** The qsnbnkans. */
	private String qsnbnkans;

	/** The qsnbnkid. */
	private Integer qsnbnkid;

	/** The rwdtype. */
	private String rwdtype;

	/** The rwdvalue. */
	private Double rwdvalue;

	/** The upddate. */
	private long upddate;

	/** The curr DB time. */
	private long currDBTime;

	/** The qtx. */
	private String qtx;

	/** The opa. */
	private String opa;

	/** The opb. */
	private String opb;

	/** The opc. */
	private String opc;

	/** The opd. */
	private String opd;

	/** The q orientn. */
	private String qOrientn;

	/** The cans. */
	private String cans;

	/**
	 * Gets the cans.
	 *
	 * @return the cans
	 */
	public String getCans() {
		return cans;
	}

	/**
	 * Gets the clintid.
	 *
	 * @return the clintid
	 */
	public String getClintid() {
		return clintid;
	}

	/**
	 * Gets the conid.
	 *
	 * @return the conid
	 */
	public String getConid() {
		return conid;
	}

	/**
	 * Gets the conqsnid.
	 *
	 * @return the conqsnid
	 */
	public Integer getConqsnid() {
		return conqsnid;
	}

	/**
	 * Gets the credate.
	 *
	 * @return the credate
	 */
	public long getCredate() {
		return credate;
	}

	/**
	 * Gets the curr DB time.
	 *
	 * @return the curr DB time
	 */
	public long getCurrDBTime() {
		return currDBTime;
	}

	/**
	 * Gets the custans.
	 *
	 * @return the custans
	 */
	public String getCustans() {
		return custans;
	}

	/**
	 * Gets the custid.
	 *
	 * @return the custid
	 */
	public String getCustid() {
		return custid;
	}

	/**
	 * Gets the custplayid.
	 *
	 * @return the custplayid
	 */
	public String getCustplayid() {
		return custplayid;
	}

	/**
	 * Gets the opa.
	 *
	 * @return the opa
	 */
	public String getOpa() {
		return opa;
	}

	/**
	 * Gets the opb.
	 *
	 * @return the opb
	 */
	public String getOpb() {
		return opb;
	}

	/**
	 * Gets the opc.
	 *
	 * @return the opc
	 */
	public String getOpc() {
		return opc;
	}

	/**
	 * Gets the opd.
	 *
	 * @return the opd
	 */
	public String getOpd() {
		return opd;
	}

	/**
	 * Gets the q orientn.
	 *
	 * @return the q orientn
	 */
	public String getqOrientn() {
		return qOrientn;
	}

	/**
	 * Gets the qsnbnkans.
	 *
	 * @return the qsnbnkans
	 */
	public String getQsnbnkans() {
		return qsnbnkans;
	}

	/**
	 * Gets the qsnbnkid.
	 *
	 * @return the qsnbnkid
	 */
	public Integer getQsnbnkid() {
		return qsnbnkid;
	}

	/**
	 * Gets the qtx.
	 *
	 * @return the qtx
	 */
	public String getQtx() {
		return qtx;
	}

	/**
	 * Gets the rwdtype.
	 *
	 * @return the rwdtype
	 */
	public String getRwdtype() {
		return rwdtype;
	}

	/**
	 * Gets the rwdvalue.
	 *
	 * @return the rwdvalue
	 */
	public Double getRwdvalue() {
		return rwdvalue;
	}

	/**
	 * Gets the upddate.
	 *
	 * @return the upddate
	 */
	public long getUpddate() {
		return upddate;
	}

	/**
	 * Checks if is iscrtans.
	 *
	 * @return true, if is iscrtans
	 */
	public boolean isIscrtans() {
		return iscrtans;
	}

	/**
	 * Sets the cans.
	 *
	 * @param cans the new cans
	 */
	public void setCans(String cans) {
		this.cans = cans;
	}

	/**
	 * Sets the clintid.
	 *
	 * @param clintid the new clintid
	 */
	public void setClintid(String clintid) {
		this.clintid = clintid;
	}

	/**
	 * Sets the conid.
	 *
	 * @param conid the new conid
	 */
	public void setConid(String conid) {
		this.conid = conid;
	}

	/**
	 * Sets the conqsnid.
	 *
	 * @param conqsnid the new conqsnid
	 */
	public void setConqsnid(Integer conqsnid) {
		this.conqsnid = conqsnid;
	}

	/**
	 * Sets the credate.
	 *
	 * @param credate the new credate
	 */
	public void setCredate(long credate) {
		this.credate = credate;
	}

	/**
	 * Sets the curr DB time.
	 *
	 * @param currDBTime the new curr DB time
	 */
	public void setCurrDBTime(long currDBTime) {
		this.currDBTime = currDBTime;
	}

	/**
	 * Sets the custans.
	 *
	 * @param custans the new custans
	 */
	public void setCustans(String custans) {
		this.custans = custans;
	}

	/**
	 * Sets the custid.
	 *
	 * @param custid the new custid
	 */
	public void setCustid(String custid) {
		this.custid = custid;
	}

	/**
	 * Sets the custplayid.
	 *
	 * @param custplayid the new custplayid
	 */
	public void setCustplayid(String custplayid) {
		this.custplayid = custplayid;
	}

	/**
	 * Sets the iscrtans.
	 *
	 * @param iscrtans the new iscrtans
	 */
	public void setIscrtans(boolean iscrtans) {
		this.iscrtans = iscrtans;
	}

	/**
	 * Sets the opa.
	 *
	 * @param opa the new opa
	 */
	public void setOpa(String opa) {
		this.opa = opa;
	}

	/**
	 * Sets the opb.
	 *
	 * @param opb the new opb
	 */
	public void setOpb(String opb) {
		this.opb = opb;
	}

	/**
	 * Sets the opc.
	 *
	 * @param opc the new opc
	 */
	public void setOpc(String opc) {
		this.opc = opc;
	}

	/**
	 * Sets the opd.
	 *
	 * @param opd the new opd
	 */
	public void setOpd(String opd) {
		this.opd = opd;
	}

	/**
	 * Sets the q orientn.
	 *
	 * @param qOrientn the new q orientn
	 */
	public void setqOrientn(String qOrientn) {
		this.qOrientn = qOrientn;
	}

	/**
	 * Sets the qsnbnkans.
	 *
	 * @param qsnbnkans the new qsnbnkans
	 */
	public void setQsnbnkans(String qsnbnkans) {
		this.qsnbnkans = qsnbnkans;
	}

	/**
	 * Sets the qsnbnkid.
	 *
	 * @param qsnbnkid the new qsnbnkid
	 */
	public void setQsnbnkid(Integer qsnbnkid) {
		this.qsnbnkid = qsnbnkid;
	}

	/**
	 * Sets the qtx.
	 *
	 * @param qtx the new qtx
	 */
	public void setQtx(String qtx) {
		this.qtx = qtx;
	}

	/**
	 * Sets the rwdtype.
	 *
	 * @param rwdtype the new rwdtype
	 */
	public void setRwdtype(String rwdtype) {
		this.rwdtype = rwdtype;
	}

	/**
	 * Sets the rwdvalue.
	 *
	 * @param rwdvalue the new rwdvalue
	 */
	public void setRwdvalue(Double rwdvalue) {
		this.rwdvalue = rwdvalue;
	}

	/**
	 * Sets the upddate.
	 *
	 * @param upddate the new upddate
	 */
	public void setUpddate(long upddate) {
		this.upddate = upddate;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "SNWCustomerAnswerDVO [clintid=" + clintid + ", conid=" + conid + ", custid=" + custid + ", custplayid="
				+ custplayid + ", conqsnid=" + conqsnid + ", credate=" + credate + ", custans=" + custans
				+ ", iscrtans=" + iscrtans + ", qsnbnkans=" + qsnbnkans + ", qsnbnkid=" + qsnbnkid + ", rwdtype="
				+ rwdtype + ", rwdvalue=" + rwdvalue + ", upddate=" + upddate + ", currDBTime=" + currDBTime + ", qtx="
				+ qtx + ", opa=" + opa + ", opb=" + opb + ", opc=" + opc + ", opd=" + opd + ", qOrientn=" + qOrientn
				+ ", cans=" + cans + "]";
	}

}
