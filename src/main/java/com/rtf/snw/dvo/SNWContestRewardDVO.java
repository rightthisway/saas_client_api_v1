/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.snw.dvo;

import java.io.Serializable;

/**
 * The Class SNWContestRewardDVO.
 */
public class SNWContestRewardDVO implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 6874287382644962327L;

	/** The cl id. */
	private String clId;

	/** The co id. */
	private String coId;

	/** The type. */
	private String type;

	/** The rwd val. */
	private Double rwdVal;

	/** The is act. */
	private Boolean isAct;

	/** The c by. */
	private String cBy;

	/** The u by. */
	private String uBy;

	/** The c date. */
	private Long cDate;

	/** The u date. */
	private Long uDate;

	// private String crDateTimeStr;
	// private String upDateTimeStr;

	/**
	 * Gets the c by.
	 *
	 * @return the c by
	 */
	public String getcBy() {
		return cBy;
	}

	/**
	 * Gets the c date.
	 *
	 * @return the c date
	 */
	public Long getcDate() {
		return cDate;
	}

	/**
	 * Gets the cl id.
	 *
	 * @return the cl id
	 */
	public String getClId() {
		return clId;
	}

	/**
	 * Gets the co id.
	 *
	 * @return the co id
	 */
	public String getCoId() {
		return coId;
	}

	/**
	 * Gets the checks if is act.
	 *
	 * @return the checks if is act
	 */
	public Boolean getIsAct() {
		return isAct;
	}

	/**
	 * Gets the rwd val.
	 *
	 * @return the rwd val
	 */
	public Double getRwdVal() {
		return rwdVal;
	}

	/**
	 * Gets the type.
	 *
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * Gets the u by.
	 *
	 * @return the u by
	 */
	public String getuBy() {
		return uBy;
	}

	/**
	 * Gets the u date.
	 *
	 * @return the u date
	 */
	public Long getuDate() {
		return uDate;
	}

	/**
	 * Sets the c by.
	 *
	 * @param cBy the new c by
	 */
	public void setcBy(String cBy) {
		this.cBy = cBy;
	}

	/**
	 * Sets the c date.
	 *
	 * @param cDate the new c date
	 */
	public void setcDate(Long cDate) {
		this.cDate = cDate;
	}

	/**
	 * Sets the cl id.
	 *
	 * @param clId the new cl id
	 */
	public void setClId(String clId) {
		this.clId = clId;
	}

	/**
	 * Sets the co id.
	 *
	 * @param coId the new co id
	 */
	public void setCoId(String coId) {
		this.coId = coId;
	}

	/**
	 * Sets the checks if is act.
	 *
	 * @param isAct the new checks if is act
	 */
	public void setIsAct(Boolean isAct) {
		this.isAct = isAct;
	}

	/**
	 * Sets the rwd val.
	 *
	 * @param rwdVal the new rwd val
	 */
	public void setRwdVal(Double rwdVal) {
		this.rwdVal = rwdVal;
	}

	/**
	 * Sets the type.
	 *
	 * @param type the new type
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * Sets the u by.
	 *
	 * @param uBy the new u by
	 */
	public void setuBy(String uBy) {
		this.uBy = uBy;
	}

	/**
	 * Sets the u date.
	 *
	 * @param uDate the new u date
	 */
	public void setuDate(Long uDate) {
		this.uDate = uDate;
	}
	/*
	 * public String getCrDateTimeStr() { crDateTimeStr =
	 * DateFormatUtil.getMMDDYYYYHHMMSSString(cDate); return crDateTimeStr; } public
	 * void setCrDateTimeStr(String crDateTimeStr) { this.crDateTimeStr =
	 * crDateTimeStr; } public String getUpDateTimeStr() { upDateTimeStr =
	 * DateFormatUtil.getMMDDYYYYHHMMSSString(uDate); return upDateTimeStr; } public
	 * void setUpDateTimeStr(String upDateTimeStr) { this.upDateTimeStr =
	 * upDateTimeStr; }
	 */

}
