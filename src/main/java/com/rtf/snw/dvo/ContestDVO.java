/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.snw.dvo;

import java.io.Serializable;

/**
 * The Class ContestDVO.
 */
public class ContestDVO implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The cl id. */
	private String clId;

	/** The co id. */
	private String coId;

	/** The mstqsnbnkid. */
	private Integer mstqsnbnkid;

	/** The ans type. */
	private String ansType;

	/** The bgthembankid. */
	private Integer bgthembankid;

	/** The thm color. */
	private String thmColor;

	/** The bgthemimgurldes. */
	private String bgthemimgurldes;

	/** The thm img mob. */
	private String thmImgMob;

	/** The thm img desk. */
	private String thmImgDesk;

	/** The brndimgurl. */
	private String brndimgurl;

	/** The img U. */
	private String imgU;

	/** The seq no. */
	private Integer seqNo;

	/** The cat. */
	private String cat;

	/** The en date. */
	private Long enDate;

	/** The name. */
	private String name;

	/** The st date. */
	private Long stDate;

	/** The cr by. */
	private String crBy;

	/** The cr date. */
	private Long crDate;

	/** The is act. */
	private Integer isAct;

	/** The play game img. */
	private String playGameImg;

	/** The playinterval. */
	private Integer playinterval;

	/** The q mode. */
	private String qMode;

	/** The sub cat. */
	private String subCat;

	/** The up date. */
	private Long upDate;

	/** The up by. */
	private String upBy;

	/**
	 * Gets the ans type.
	 *
	 * @return the ans type
	 */
	public String getAnsType() {
		return ansType;
	}

	/**
	 * Gets the bgthembankid.
	 *
	 * @return the bgthembankid
	 */
	public Integer getBgthembankid() {
		return bgthembankid;
	}

	/**
	 * Gets the bgthemimgurldes.
	 *
	 * @return the bgthemimgurldes
	 */
	public String getBgthemimgurldes() {
		return bgthemimgurldes;
	}

	/**
	 * Gets the brndimgurl.
	 *
	 * @return the brndimgurl
	 */
	public String getBrndimgurl() {
		return brndimgurl;
	}

	/**
	 * Gets the cat.
	 *
	 * @return the cat
	 */
	public String getCat() {
		return cat;
	}

	/**
	 * Gets the cl id.
	 *
	 * @return the cl id
	 */
	public String getClId() {
		return clId;
	}

	/**
	 * Gets the co id.
	 *
	 * @return the co id
	 */
	public String getCoId() {
		return coId;
	}

	/**
	 * Gets the cr by.
	 *
	 * @return the cr by
	 */
	public String getCrBy() {
		return crBy;
	}

	/**
	 * Gets the cr date.
	 *
	 * @return the cr date
	 */
	public Long getCrDate() {
		return crDate;
	}

	/**
	 * Gets the en date.
	 *
	 * @return the en date
	 */
	public Long getEnDate() {
		return enDate;
	}

	/**
	 * Gets the img U.
	 *
	 * @return the img U
	 */
	public String getImgU() {
		return imgU;
	}

	/**
	 * Gets the checks if is act.
	 *
	 * @return the checks if is act
	 */
	public Integer getIsAct() {
		return isAct;
	}

	/**
	 * Gets the mstqsnbnkid.
	 *
	 * @return the mstqsnbnkid
	 */
	public Integer getMstqsnbnkid() {
		return mstqsnbnkid;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Gets the play game img.
	 *
	 * @return the play game img
	 */
	public String getPlayGameImg() {
		return playGameImg;
	}

	/**
	 * Gets the playinterval.
	 *
	 * @return the playinterval
	 */
	public Integer getPlayinterval() {
		return playinterval;
	}

	/**
	 * Gets the q mode.
	 *
	 * @return the q mode
	 */
	public String getqMode() {
		return qMode;
	}

	/**
	 * Gets the seq no.
	 *
	 * @return the seq no
	 */
	public Integer getSeqNo() {
		return seqNo;
	}

	/**
	 * Gets the st date.
	 *
	 * @return the st date
	 */
	public Long getStDate() {
		return stDate;
	}

	/**
	 * Gets the sub cat.
	 *
	 * @return the sub cat
	 */
	public String getSubCat() {
		return subCat;
	}

	/**
	 * Gets the thm color.
	 *
	 * @return the thm color
	 */
	public String getThmColor() {
		return thmColor;
	}

	/**
	 * Gets the thm img desk.
	 *
	 * @return the thm img desk
	 */
	public String getThmImgDesk() {
		return thmImgDesk;
	}

	/**
	 * Gets the thm img mob.
	 *
	 * @return the thm img mob
	 */
	public String getThmImgMob() {
		return thmImgMob;
	}

	/**
	 * Gets the up by.
	 *
	 * @return the up by
	 */
	public String getUpBy() {
		return upBy;
	}

	/**
	 * Gets the up date.
	 *
	 * @return the up date
	 */
	public Long getUpDate() {
		return upDate;
	}

	/**
	 * Sets the ans type.
	 *
	 * @param ansType the new ans type
	 */
	public void setAnsType(String ansType) {
		this.ansType = ansType;
	}

	/**
	 * Sets the bgthembankid.
	 *
	 * @param bgthembankid the new bgthembankid
	 */
	public void setBgthembankid(Integer bgthembankid) {
		this.bgthembankid = bgthembankid;
	}

	/**
	 * Sets the bgthemimgurldes.
	 *
	 * @param bgthemimgurldes the new bgthemimgurldes
	 */
	public void setBgthemimgurldes(String bgthemimgurldes) {
		this.bgthemimgurldes = bgthemimgurldes;
	}

	/**
	 * Sets the brndimgurl.
	 *
	 * @param brndimgurl the new brndimgurl
	 */
	public void setBrndimgurl(String brndimgurl) {
		this.brndimgurl = brndimgurl;
	}

	/**
	 * Sets the cat.
	 *
	 * @param cat the new cat
	 */
	public void setCat(String cat) {
		this.cat = cat;
	}

	/**
	 * Sets the cl id.
	 *
	 * @param clId the new cl id
	 */
	public void setClId(String clId) {
		this.clId = clId;
	}

	/**
	 * Sets the co id.
	 *
	 * @param coId the new co id
	 */
	public void setCoId(String coId) {
		this.coId = coId;
	}

	/**
	 * Sets the cr by.
	 *
	 * @param crBy the new cr by
	 */
	public void setCrBy(String crBy) {
		this.crBy = crBy;
	}

	/**
	 * Sets the cr date.
	 *
	 * @param crDate the new cr date
	 */
	public void setCrDate(Long crDate) {
		this.crDate = crDate;
	}

	/**
	 * Sets the en date.
	 *
	 * @param enDate the new en date
	 */
	public void setEnDate(Long enDate) {
		this.enDate = enDate;
	}

	/**
	 * Sets the img U.
	 *
	 * @param imgU the new img U
	 */
	public void setImgU(String imgU) {
		this.imgU = imgU;
	}

	/**
	 * Sets the checks if is act.
	 *
	 * @param isAct the new checks if is act
	 */
	public void setIsAct(Integer isAct) {
		this.isAct = isAct;
	}

	/**
	 * Sets the mstqsnbnkid.
	 *
	 * @param mstqsnbnkid the new mstqsnbnkid
	 */
	public void setMstqsnbnkid(Integer mstqsnbnkid) {
		this.mstqsnbnkid = mstqsnbnkid;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Sets the play game img.
	 *
	 * @param playGameImg the new play game img
	 */
	public void setPlayGameImg(String playGameImg) {
		this.playGameImg = playGameImg;
	}

	/**
	 * Sets the playinterval.
	 *
	 * @param playinterval the new playinterval
	 */
	public void setPlayinterval(Integer playinterval) {
		this.playinterval = playinterval;
	}

	/**
	 * Sets the q mode.
	 *
	 * @param qMode the new q mode
	 */
	public void setqMode(String qMode) {
		this.qMode = qMode;
	}

	/**
	 * Sets the seq no.
	 *
	 * @param seqNo the new seq no
	 */
	public void setSeqNo(Integer seqNo) {
		this.seqNo = seqNo;
	}

	/**
	 * Sets the st date.
	 *
	 * @param stDate the new st date
	 */
	public void setStDate(Long stDate) {
		this.stDate = stDate;
	}

	/**
	 * Sets the sub cat.
	 *
	 * @param subCat the new sub cat
	 */
	public void setSubCat(String subCat) {
		this.subCat = subCat;
	}

	/**
	 * Sets the thm color.
	 *
	 * @param thmColor the new thm color
	 */
	public void setThmColor(String thmColor) {
		this.thmColor = thmColor;
	}

	/**
	 * Sets the thm img desk.
	 *
	 * @param thmImgDesk the new thm img desk
	 */
	public void setThmImgDesk(String thmImgDesk) {
		this.thmImgDesk = thmImgDesk;
	}

	/**
	 * Sets the thm img mob.
	 *
	 * @param thmImgMob the new thm img mob
	 */
	public void setThmImgMob(String thmImgMob) {
		this.thmImgMob = thmImgMob;
	}

	/**
	 * Sets the up by.
	 *
	 * @param upBy the new up by
	 */
	public void setUpBy(String upBy) {
		this.upBy = upBy;
	}

	/**
	 * Sets the up date.
	 *
	 * @param upDate the new up date
	 */
	public void setUpDate(Long upDate) {
		this.upDate = upDate;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "ContestDVO [clId=" + clId + ", coId=" + coId + ", mstqsnbnkid=" + mstqsnbnkid + ", ansType=" + ansType
				+ ", bgthembankid=" + bgthembankid + ", thmColor=" + thmColor + ", bgthemimgurldes=" + bgthemimgurldes
				+ ", thmImgMob=" + thmImgMob + ", thmImgDesk=" + thmImgDesk + ", brndimgurl=" + brndimgurl + ", imgU="
				+ imgU + ", seqNo=" + seqNo + ", cat=" + cat + ", enDate=" + enDate + ", name=" + name + ", stDate="
				+ stDate + ", crBy=" + crBy + ", crDate=" + crDate + ", isAct=" + isAct + ", playGameImg=" + playGameImg
				+ ", playinterval=" + playinterval + ", qMode=" + qMode + ", subCat=" + subCat + ", upDate=" + upDate
				+ ", upBy=" + upBy + "]";
	}

}
