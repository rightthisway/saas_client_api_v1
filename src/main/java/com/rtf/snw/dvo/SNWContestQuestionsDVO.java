/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.snw.dvo;

import java.util.Date;

/**
 * The Class SNWContestQuestionsDVO.
 */
public class SNWContestQuestionsDVO {

	/** The clintid. */
	private String clintid;

	/** The conqsnid. */
	private Integer conqsnid;

	/** The conid. */
	private String conid;

	/** The qsnseqno. */
	private Integer qsnseqno;

	/** The opa. */
	private String opa;

	/** The opb. */
	private String opb;

	/** The opc. */
	private String opc;

	/** The opd. */
	private String opd;

	/** The corans. */
	private String corans;

	/** The creby. */
	private String creby;

	/** The credate. */
	private Date credate;

	/** The qbid. */
	private Integer qbid;

	/** The qsnmode. */
	private String qsnmode;

	/** The qsnorient. */
	// DISPLAY ANSWER OPTIONS HORIZONTAL OR VERTICAL
	private String qsnorient;

	/** The qsntext. */
	private String qsntext;

	/** The updby. */
	private String updby;

	/** The upddate. */
	private Date upddate;

	/**
	 * Gets the clintid.
	 *
	 * @return the clintid
	 */
	public String getClintid() {
		return clintid;
	}

	/**
	 * Gets the conid.
	 *
	 * @return the conid
	 */
	public String getConid() {
		return conid;
	}

	/**
	 * Gets the conqsnid.
	 *
	 * @return the conqsnid
	 */
	public Integer getConqsnid() {
		return conqsnid;
	}

	/**
	 * Gets the corans.
	 *
	 * @return the corans
	 */
	public String getCorans() {
		return corans;
	}

	/**
	 * Gets the creby.
	 *
	 * @return the creby
	 */
	public String getCreby() {
		return creby;
	}

	/**
	 * Gets the credate.
	 *
	 * @return the credate
	 */
	public Date getCredate() {
		return credate;
	}

	/**
	 * Gets the opa.
	 *
	 * @return the opa
	 */
	public String getOpa() {
		return opa;
	}

	/**
	 * Gets the opb.
	 *
	 * @return the opb
	 */
	public String getOpb() {
		return opb;
	}

	/**
	 * Gets the opc.
	 *
	 * @return the opc
	 */
	public String getOpc() {
		return opc;
	}

	/**
	 * Gets the opd.
	 *
	 * @return the opd
	 */
	public String getOpd() {
		return opd;
	}

	/**
	 * Gets the qbid.
	 *
	 * @return the qbid
	 */
	public Integer getQbid() {
		return qbid;
	}

	/**
	 * Gets the qsnmode.
	 *
	 * @return the qsnmode
	 */
	public String getQsnmode() {
		return qsnmode;
	}

	/**
	 * Gets the qsnorient.
	 *
	 * @return the qsnorient
	 */
	public String getQsnorient() {
		return qsnorient;
	}

	/**
	 * Gets the qsnseqno.
	 *
	 * @return the qsnseqno
	 */
	public Integer getQsnseqno() {
		return qsnseqno;
	}

	/**
	 * Gets the qsntext.
	 *
	 * @return the qsntext
	 */
	public String getQsntext() {
		return qsntext;
	}

	/**
	 * Gets the updby.
	 *
	 * @return the updby
	 */
	public String getUpdby() {
		return updby;
	}

	/**
	 * Gets the upddate.
	 *
	 * @return the upddate
	 */
	public Date getUpddate() {
		return upddate;
	}

	/**
	 * Sets the clintid.
	 *
	 * @param clintid the new clintid
	 */
	public void setClintid(String clintid) {
		this.clintid = clintid;
	}

	/**
	 * Sets the conid.
	 *
	 * @param conid the new conid
	 */
	public void setConid(String conid) {
		this.conid = conid;
	}

	/**
	 * Sets the conqsnid.
	 *
	 * @param conqsnid the new conqsnid
	 */
	public void setConqsnid(Integer conqsnid) {
		this.conqsnid = conqsnid;
	}

	/**
	 * Sets the corans.
	 *
	 * @param corans the new corans
	 */
	public void setCorans(String corans) {
		this.corans = corans;
	}

	/**
	 * Sets the creby.
	 *
	 * @param creby the new creby
	 */
	public void setCreby(String creby) {
		this.creby = creby;
	}

	/**
	 * Sets the credate.
	 *
	 * @param credate the new credate
	 */
	public void setCredate(Date credate) {
		this.credate = credate;
	}

	/**
	 * Sets the opa.
	 *
	 * @param opa the new opa
	 */
	public void setOpa(String opa) {
		this.opa = opa;
	}

	/**
	 * Sets the opb.
	 *
	 * @param opb the new opb
	 */
	public void setOpb(String opb) {
		this.opb = opb;
	}

	/**
	 * Sets the opc.
	 *
	 * @param opc the new opc
	 */
	public void setOpc(String opc) {
		this.opc = opc;
	}

	/**
	 * Sets the opd.
	 *
	 * @param opd the new opd
	 */
	public void setOpd(String opd) {
		this.opd = opd;
	}

	/**
	 * Sets the qbid.
	 *
	 * @param qbid the new qbid
	 */
	public void setQbid(Integer qbid) {
		this.qbid = qbid;
	}

	/**
	 * Sets the qsnmode.
	 *
	 * @param qsnmode the new qsnmode
	 */
	public void setQsnmode(String qsnmode) {
		this.qsnmode = qsnmode;
	}

	/**
	 * Sets the qsnorient.
	 *
	 * @param qsnorient the new qsnorient
	 */
	public void setQsnorient(String qsnorient) {
		this.qsnorient = qsnorient;
	}

	/**
	 * Sets the qsnseqno.
	 *
	 * @param qsnseqno the new qsnseqno
	 */
	public void setQsnseqno(Integer qsnseqno) {
		this.qsnseqno = qsnseqno;
	}

	/**
	 * Sets the qsntext.
	 *
	 * @param qsntext the new qsntext
	 */
	public void setQsntext(String qsntext) {
		this.qsntext = qsntext;
	}

	/**
	 * Sets the updby.
	 *
	 * @param updby the new updby
	 */
	public void setUpdby(String updby) {
		this.updby = updby;
	}

	/**
	 * Sets the upddate.
	 *
	 * @param upddate the new upddate
	 */
	public void setUpddate(Date upddate) {
		this.upddate = upddate;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "SNWContestQuestionsDVO [clintid=" + clintid + ", conqsnid=" + conqsnid + ", conid=" + conid
				+ ", qsnseqno=" + qsnseqno + ", opa=" + opa + ", opb=" + opb + ", opc=" + opc + ", opd=" + opd
				+ ", corans=" + corans + ", creby=" + creby + ", credate=" + credate + ", qbid=" + qbid + ", qsnmode="
				+ qsnmode + ", qsnorient=" + qsnorient + ", qsntext=" + qsntext + ", updby=" + updby + ", upddate="
				+ upddate + "]";
	}

}
