/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.snw.service;

import com.rtf.common.dspl.dvo.SnwContestDVO;
import com.rtf.snw.dao.SNWContestDAO;
import com.rtf.snw.dvo.ContestDVO;

/**
 * The Class SNWContestService.
 */
public class SNWContestService {

	/**
	 * Fetch contest info by id.
	 *
	 * @param clientId  the client id
	 * @param contestId the contest id
	 * @return the contest DVO
	 * @throws Exception the exception
	 */
	public static ContestDVO fetchContestInfoById(String clientId, String contestId) throws Exception {
		ContestDVO contestMstrDVO = null;
		try {
			contestMstrDVO = SNWContestDAO.getContestByContestId(clientId, contestId);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return contestMstrDVO;
	}

	/**
	 * Gets the contest card details.
	 *
	 * @param clientId the client id
	 * @return the contest card
	 * @throws Exception the exception
	 */
	public static SnwContestDVO getContestCard(String clientId) throws Exception {
		SnwContestDVO contestMstrDVO = null;
		try {
			contestMstrDVO = SNWContestDAO.getContestCard(clientId);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return contestMstrDVO;
	}

	/**
	 * Gets the live contests by client id.
	 *
	 * @param clientId the client id
	 * @return the live contests by client id
	 * @throws Exception the exception
	 */
	public static ContestDVO getLiveContestsByClientId(String clientId) throws Exception {
		ContestDVO contestMstrDVO = null;
		try {
			contestMstrDVO = SNWContestDAO.getLiveContestsByClientId(clientId);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return contestMstrDVO;
	}

}
