/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.snw.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import com.rtf.common.dao.CommonQuestionBankDAO;
import com.rtf.common.dvo.CommonQuestionBankDVO;
import com.rtf.common.service.CustomerRewardService;
import com.rtf.common.util.Constants;
import com.rtf.saas.exception.SaasProcessException;
import com.rtf.saas.util.GenUtil;
import com.rtf.snw.dao.SNWContestQuestionDAO;
import com.rtf.snw.dao.SNWQuestionAnswerDAO;
import com.rtf.snw.dto.SNWQuestionAnswerDTO;
import com.rtf.snw.dvo.ContestDVO;
import com.rtf.snw.dvo.SNWContestQuestionsDVO;
import com.rtf.snw.dvo.SNWCustomerAnswerDVO;
import com.rtf.snw.util.SNWCustomerAnswerComparator;
import com.rtf.snw.util.SNWDataMigrationUtil;
import com.rtf.snw.util.SNWMessages;

/**
 * The Class SNWQuestionService.
 */
public class SNWQuestionService {

	/**
	 * Cust answered questionsby contest id.
	 *
	 * @param clintId the clint id
	 * @param custId  the cust id
	 * @param coId    the co id
	 * @return the list
	 * @throws Exception the exception
	 */
	public static List<Integer> custAnsweredQuestionsbyContestId(String clintId, String custId, String coId)
			throws Exception {

		List<Integer> queIdList = SNWQuestionAnswerDAO.getCustomerQueAnsweredByContestQuestionsIds(clintId, custId,
				coId);

		return queIdList;
	}

	/**
	 * Fecth Collection of Customer answered questions 
	 *
	 * @param clintId the clint id
	 * @param custId  the cust id
	 * @param coId    the co id
	 * @return the list
	 * @throws Exception the exception
	 */
	public static List<Integer> custAnsweredQuestionsbyQuestionBankId(String clintId, String custId, String coId)
			throws Exception {

		List<Integer> queIdList = SNWQuestionAnswerDAO.getCustomerQueAnsweredByQuestionsIds(clintId, custId, coId);

		return queIdList;
	}

	/**
	 * Fetch question info Object SNWQuestionAnswerDTO.
	 *
	 * @param dto the SNWQuestionAnswerDTO
	 * @return the SNW question answer DTO  SNWQuestionAnswerDTO  
	 * @throws Exception the exception
	 */
	public static SNWQuestionAnswerDTO fetchQuestionInfo(SNWQuestionAnswerDTO dto) throws Exception {

		ContestDVO contest = SNWContestService.fetchContestInfoById(dto.getClId(), dto.getCoId());
		if (contest == null || GenUtil.isEmptyString(contest.getCoId())) {
			dto.setSts(0);
			return dto;
		}
		Integer timeIntervalbetweenQuestions = contest.getPlayinterval();
		if (timeIntervalbetweenQuestions == null) {
			String msg = SNWMessages.SNW_TIME_INTERVAL_BTWN_PLAY + "Contest Id - " + dto.getCoId();
			dto.setMsg(msg);
			dto.setSts(0);
			return dto;
		}

		String qMode = contest.getqMode(); // FIXED or RANDOM
		SNWCustomerAnswerDVO dvo = new SNWCustomerAnswerDVO();
		dto.setCuAnsdvo(dvo);
		if (Constants.CONTEST_MODE_RANDOM.equals(qMode)) {
			populateRandomQuestionFromGeneralQBank(contest, dto);
		} else if (Constants.CONTEST_MODE_FIXED.equals(qMode)) {
			populateQuestionFromContestQuestionBank(contest, dto);
		}

		String playId = getPlayId(dto);
		SNWCustomerAnswerDVO ansdvo = dto.getCuAnsdvo();
		ansdvo.setCustplayid(playId);
		insertCustomerQuestionAnswerDetails(dto);
		dto.setSts(1);
		return dto;

	}

	/**
	 * Gets the latest customer answer details.
	 *
	 * @param custAnsList the SNWCustomerAnswerDVO list
	 * @return the latest customer answer details
	 */
	public static SNWCustomerAnswerDVO getLastestCustomerAnswerDets(List<SNWCustomerAnswerDVO> custAnsList) {
		try {

			if (custAnsList != null && !custAnsList.isEmpty()) {
				Collections.sort(custAnsList, new SNWCustomerAnswerComparator());
				SNWCustomerAnswerDVO dvo = custAnsList.get(0);
				return dvo;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Gets the play id.
	 *
	 * @param dto the dto
	 * @return the play id
	 */
	private static String getPlayId(SNWQuestionAnswerDTO dto) {
		return dto.getClId() + "_" + dto.getCuID() + "_" + System.currentTimeMillis();
	}

	/**
	 * Insert customer question answer details.
	 *
	 * @param dto the SNWQuestionAnswerDTO
	 * @throws Exception the exception
	 */
	private static void insertCustomerQuestionAnswerDetails(SNWQuestionAnswerDTO dto) throws Exception {
		try {

			SNWCustomerAnswerDVO ansdvo = dto.getCuAnsdvo();
			ansdvo.setClintid(dto.getClId());
			ansdvo.setConid(dto.getCoId());
			ansdvo.setCustid(dto.getCuID());

			SNWQuestionAnswerDAO.insertCustomerQuenAnswerDetails(ansdvo);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new SaasProcessException(
					"Error Inserting answer Records for [contest][PLAYID] " + dto.getCoId() + "-" + dto.getPlayId());

		}

	}

	/**
	 * Populate question from contest question bank.
	 *
	 * @param contest the contest
	 * @param dto     the SNWQuestionAnswerDTO
	 * @return the SNW question answer DTO
	 * @throws Exception the exception
	 */
	private static SNWQuestionAnswerDTO populateQuestionFromContestQuestionBank(ContestDVO contest,
			SNWQuestionAnswerDTO dto) throws Exception {
		try {
			Map<Integer, SNWContestQuestionsDVO> qMapContest = SNWContestQuestionDAO
					.getAllContestQuestionsByContestId(dto.getClId(), contest.getCoId());
			List<Integer> qIdList = custAnsweredQuestionsbyContestId(dto.getClId(), dto.getCuID(), contest.getCoId());
			if (qIdList == null)
				qIdList = new ArrayList<Integer>();
			List<Integer> queBankIdList = new ArrayList<Integer>(qMapContest.keySet());
			queBankIdList.removeAll(qIdList);
			if (queBankIdList == null || queBankIdList.size() == 0) {
				queBankIdList = new ArrayList<Integer>(qMapContest.keySet());
			}
			Collections.shuffle(queBankIdList);
			SNWContestQuestionsDVO dvo = qMapContest.get(queBankIdList.get(0));
			SNWCustomerAnswerDVO ansdvo = dto.getCuAnsdvo();
			ansdvo.setQtx(dvo.getQsntext());
			ansdvo.setOpa(dvo.getOpa());
			ansdvo.setOpb(dvo.getOpb());
			ansdvo.setOpc(dvo.getOpc());
			ansdvo.setOpd(dvo.getOpd());
			ansdvo.setqOrientn(dvo.getQsnorient());
			ansdvo.setConqsnid(dvo.getConqsnid());
			ansdvo.setQsnbnkans(dvo.getCorans());
			ansdvo.setQsnbnkid(0);
			dto.setCuAnsdvo(ansdvo);
		} catch (Exception ex) {
			dto.setSts(0);
			ex.printStackTrace();
			throw new SaasProcessException("Error getting questions form question bank");
		}
		return dto;

	}

	/**
	 * Populate random question from general Q bank.
	 *
	 * @param contest the contest
	 * @param dto     the SNWQuestionAnswerDTO
	 * @return the SNW question answer DTO
	 * @throws Exception the exception
	 */
	private static SNWQuestionAnswerDTO populateRandomQuestionFromGeneralQBank(ContestDVO contest,
			SNWQuestionAnswerDTO dto) throws Exception {
		try {			
			Map<Integer, CommonQuestionBankDVO> qMapRandom = CommonQuestionBankDAO.getAllQuestionsBasedOnMasterBankId(
					dto.getClId(), 1000, contest.getCat(), contest.getSubCat(), contest.getAnsType());
			List<Integer> qIdList = custAnsweredQuestionsbyQuestionBankId(dto.getClId(), dto.getCuID(),
					contest.getCoId());
			if (qIdList == null)
				qIdList = new ArrayList<Integer>();
			List<Integer> queBankIdList = new ArrayList<Integer>(qMapRandom.keySet());
			queBankIdList.removeAll(qIdList);
			if (queBankIdList == null || queBankIdList.size() == 0) {
				queBankIdList = new ArrayList<Integer>(qMapRandom.keySet());
			}
			Collections.shuffle(queBankIdList);
			CommonQuestionBankDVO dvo = qMapRandom.get(queBankIdList.get(0));
			SNWCustomerAnswerDVO ansdvo = dto.getCuAnsdvo();
			ansdvo.setQtx(dvo.getQtx());
			ansdvo.setOpa(dvo.getOpa());
			ansdvo.setOpb(dvo.getOpb());
			ansdvo.setOpc(dvo.getOpc());
			ansdvo.setOpd(dvo.getOpd());
			ansdvo.setqOrientn(dvo.getqOrientn());
			ansdvo.setQsnbnkid(dvo.getQbid());
			ansdvo.setQsnbnkans(dvo.getCans());
			ansdvo.setConqsnid(0);
			dto.setCuAnsdvo(ansdvo);
		} catch (Exception ex) {
			dto.setSts(0);
			ex.printStackTrace();
			throw new SaasProcessException("Error getting questions form question bank");

		}
		return dto;
	}

	/**
	 * Update customer question answer details.
	 *
	 * @param custAns the customer answer SNWCustomerAnswerDVO
	 * @throws Exception the exception
	 */
	public static void updateCustomerQuenAnswerDetails(SNWCustomerAnswerDVO custAns) throws Exception {

		SNWQuestionAnswerDAO.updateCustomerQuenAnswerDetails(custAns);

		if (custAns.getRwdtype() != null && custAns.getRwdvalue() != null && custAns.getRwdvalue() > 0) {
			CustomerRewardService.creditCustomerRewards(custAns.getClintid(), custAns.getCustid(), custAns.getRwdtype(),
					custAns.getRwdvalue());
			SNWDataMigrationUtil.migrateCustomerSNWRewards(custAns.getClintid(), custAns.getCustid(),
					custAns.getConid(), custAns.getCustplayid());
		}
	}

}
