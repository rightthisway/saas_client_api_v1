/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.snw.util;

import java.util.ArrayList;
import java.util.List;

import com.rtf.common.dao.RTFDataMigrationDAO;
import com.rtf.common.dvo.RTFCustomerDVO;
import com.rtf.snw.dao.SNWQuestionAnswerDAO;
import com.rtf.snw.dvo.SNWCustomerAnswerDVO;

/**
 * The Class SNWDataMigrationUtil.
 */
public class SNWDataMigrationUtil {

	/**
	 * Migrate customer SNW rewards.
	 *
	 * @param clId    the Client ID
	 * @param cuId    the Customer ID
	 * @param coId    the Contest ID 
	 * @param playId the play id
	 */
	public static void migrateCustomerSNWRewards(String clId, String cuId, String coId, String playId) {
		if (clId != null && !clId.equalsIgnoreCase("PROD2020REWARDTHEFAN")) {
			return;
		}
		Double totalPoints = 0.00;
		cuId = cuId.trim();
		List<RTFCustomerDVO> customers = new ArrayList<RTFCustomerDVO>();
		try {
			SNWCustomerAnswerDVO dvo = SNWQuestionAnswerDAO.getCustomerAnswer(clId, coId, cuId, playId);
			if (dvo == null) {
				return;
			}
			if (dvo.getRwdtype() != null && dvo.getRwdtype().equalsIgnoreCase("POINTS")) {
				totalPoints = totalPoints + (dvo.getRwdvalue() != null ? dvo.getRwdvalue() : 0);
			}

			if (totalPoints <= 0) {
				if (dvo.getRwdtype() != null && dvo.getRwdtype().equalsIgnoreCase("POINTS")) {
					SNWQuestionAnswerDAO.updateAnswerMigrationStatus(dvo);
				}
				return;
			}
			RTFCustomerDVO cust = RTFDataMigrationDAO.getRTFCustId(cuId);
			if (cust == null || cust.getId() == null) {
				return;
			}
			cust.setRtfPoints(cust.getRtfPoints() + totalPoints.intValue());
			String insQuery = RTFDataMigrationDAO.insertRTFPointsForTracking(cust, totalPoints.intValue(), coId,
					"SAAS-SNW");
			String updQuery = RTFDataMigrationDAO.updateCustomerRTFPoints(cust);

			Integer result = RTFDataMigrationDAO.updateSaaSRewardsToRTF(insQuery);
			if (result > 0) {
				result = RTFDataMigrationDAO.updateSaaSRewardsToRTF(updQuery);
				SNWQuestionAnswerDAO.updateAnswerMigrationStatus(dvo);
			}
			customers.add(cust);
			RTFDataMigrationDAO.updateDataToCass(customers);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
