/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.snw.util;

import java.util.Comparator;

import com.rtf.snw.dvo.SNWCustomerAnswerDVO;

/**
 * The Class SNWCustomerAnswerComparator.
 */
public class SNWCustomerAnswerComparator implements Comparator<SNWCustomerAnswerDVO> {

	/**
	 * Compare.
	 *
	 * @param o1 the o 1
	 * @param o2 the o 2
	 * @return the int
	 */
	@Override
	public int compare(SNWCustomerAnswerDVO o1, SNWCustomerAnswerDVO o2) {
		return Long.valueOf(o2.getCredate()).compareTo(Long.valueOf(o1.getCredate()));
	}

}
