/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.snw.util;

/**
 * The Class SNWMessages.
 */
public class SNWMessages {

	/** The Constant SNW_TIME_INTERVAL_FAIL. */
	public static final String SNW_TIME_INTERVAL_FAIL = "Please Try again in  ";

	/** The Constant SNW_TIME_INTERVAL_BTWN_PLAY. */
	public static final String SNW_TIME_INTERVAL_BTWN_PLAY = "Please Configure Time Interval Between Play for Contest  ";

}
