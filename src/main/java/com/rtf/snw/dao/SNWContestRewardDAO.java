/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.snw.dao;

import java.util.ArrayList;
import java.util.List;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.exceptions.DriverException;
import com.rtf.saas.db.CassandraConnector;
import com.rtf.saas.exception.RTFDataAccessException;
import com.rtf.saas.util.GenUtil;
import com.rtf.snw.dvo.SNWContestRewardDVO;

/**
 * The Class SNWContestRewardDAO.
 */
public class SNWContestRewardDAO {

	/**
	 * Gets the active contest rewards by contest id.
	 *
	 * @param clId      the Client ID
	 * @param contestId the contest id
	 * @return the active contest rewards by contest id
	 * @throws Exception the exception
	 */
	public static List<SNWContestRewardDVO> getActiveContestRewardsByContestId(String clId, String contestId)
			throws Exception {
		ResultSet resultSet = null;
		List<SNWContestRewardDVO> contestRewards = new ArrayList<SNWContestRewardDVO>();
		SNWContestRewardDVO contestRewardDVO = null;
		try {
			resultSet = CassandraConnector.getSession()
					.execute("SELECT * from  pa_snwtx_contest_rewards  WHERE clintid=? and conid=?", clId, contestId);
			if (resultSet != null) {
				for (Row row : resultSet) {
					contestRewardDVO = new SNWContestRewardDVO();
					contestRewardDVO.setClId(row.getString("clintid"));
					contestRewardDVO.setIsAct(row.getBool("isactive"));
					contestRewardDVO.setCoId(row.getString("conid"));
					contestRewardDVO.setType(row.getString("rwdtype"));
					contestRewardDVO.setRwdVal(row.getDouble("rwdval"));
					if (contestRewardDVO.getRwdVal() != null && contestRewardDVO.getRwdVal() > 0) {
						contestRewards.add(contestRewardDVO);
					}
				}
			}
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		}
		return contestRewards;
	}

	/**
	 * Gets the Collection of contest rewards Object .
	 *
	 * @param clintId the Client ID
	 * @param coId    the Contest ID
	 * @return the contest rewards
	 * @throws Exception the exception
	 */
	public static List<SNWContestRewardDVO> getContestRewards(String clintId, String coId) throws Exception {
		List<SNWContestRewardDVO> rewards = new ArrayList<SNWContestRewardDVO>();
		try {

		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		}
		return rewards;
	}

	/**
	 * Update contest rewards.
	 *
	 * @param rwds the Rewards
	 * @param coId the Contest ID
	 * @return true, if successful
	 * @throws Exception the exception
	 */
	public static boolean updateContestRewards(List<SNWContestRewardDVO> rwds, String coId) throws Exception {
		Boolean isUpdated = false;
		try {
			for (SNWContestRewardDVO r : rwds) {
				String sql = "";
				if (r.getCoId() == null) {
					sql = "INSERT INTO pa_snwtx_contest_rewards (clintid,conid,rwdtype,isactive,creby,credate,rwdval) values (?,?,?,?,?,toTimestamp(now()),?)";
					CassandraConnector.getSession().execute(sql,
							new Object[] { r.getClId(), coId, r.getType(), Boolean.TRUE, r.getcBy(), r.getRwdVal() });
				} else {
					sql = "update pa_snwtx_contest_rewards set rwdval=?,updby=?,upddate=toTimestamp(now()) WHERE clintid=? and conid=? and  rwdtype=?";
					CassandraConnector.getSession().execute(sql,
							new Object[] { r.getRwdVal(), r.getuBy(), r.getClId(), r.getCoId(), r.getType() });
				}
			}
			isUpdated = true;
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		}
		return isUpdated;
	}

}
