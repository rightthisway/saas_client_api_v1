/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.snw.dao;

import java.util.HashMap;
import java.util.Map;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.exceptions.DriverException;
import com.rtf.saas.db.CassandraConnector;
import com.rtf.saas.exception.RTFDataAccessException;
import com.rtf.saas.util.GenUtil;
import com.rtf.snw.dvo.SNWContestQuestionsDVO;

/**
 * The Class SNWContestQuestionDAO.
 */
public class SNWContestQuestionDAO {

	/**
	 * Gets the all contest questions by contest id.
	 *
	 * @param clientId  the client id
	 * @param contestId the contest id
	 * @return the all contest questions by contest id
	 * @throws Exception the exception
	 */
	public static Map<Integer, SNWContestQuestionsDVO> getAllContestQuestionsByContestId(String clientId,
			String contestId) throws Exception {

		ResultSet resultSet = null;
		Map<Integer, SNWContestQuestionsDVO> qMap = null;
		try {
			String keySpace = CassandraConnector.getRtfSassCassKeySpace();
			resultSet = CassandraConnector.getSession().execute(
					"SELECT * from  " + keySpace + "pa_snwtx_contest_question " + "WHERE clintid=? and conid=? ",
					clientId, contestId);

			SNWContestQuestionsDVO dvo = null;
			if (resultSet != null) {
				qMap = new HashMap<Integer, SNWContestQuestionsDVO>();
				for (Row row : resultSet) {
					Integer qid = null;
					dvo = new SNWContestQuestionsDVO();
					dvo.setClintid(row.getString("clintid"));
					dvo.setConid(row.getString("conid"));
					qid = row.getInt("conqsnid");
					dvo.setConqsnid(qid);
					dvo.setQsntext(row.getString("qsntext"));
					dvo.setOpa(row.getString("ansopta"));
					dvo.setOpb(row.getString("ansoptb"));
					dvo.setOpc(row.getString("ansoptc"));
					dvo.setOpd(row.getString("ansoptd"));
					dvo.setCorans(row.getString("corans"));
					dvo.setQbid(row.getInt("qsnbnkid"));
					dvo.setQsnorient(row.getString("qsnorient"));
					dvo.setQsnseqno(row.getInt("qsnseqno"));
					qMap.put(qid, dvo);
				}
			}
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		}
		return qMap;
	}

}
