/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.snw.dao;

import java.util.Date;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.exceptions.DriverException;
import com.rtf.common.dspl.dvo.SnwContestDVO;
import com.rtf.saas.db.CassandraConnector;
import com.rtf.saas.exception.RTFDataAccessException;
import com.rtf.saas.exception.SaasProcessException;
import com.rtf.saas.util.GenUtil;
import com.rtf.snw.dvo.ContestDVO;

/**
 * The Class SNWContestDAO.
 */
public class SNWContestDAO {

	/**
	 * Gets the contest Object  by contest id.
	 *
	 * @param clintId    the Client ID
	 * @param contestId the contest ID
	 * @return the contest object  ContestDVO
	 * @throws Exception the exception
	 */
	public static ContestDVO getContestByContestId(String clintId, String contestId) throws Exception {
		ResultSet resultSet;
		ContestDVO contestDVO = null;
		try {
			resultSet = CassandraConnector.getSession()
					.execute("SELECT * from  pa_snwtx_contest_mstr  WHERE clintid=? AND conid=?", clintId, contestId);

			if (resultSet != null) {
				for (Row row : resultSet) {
					contestDVO = new ContestDVO();
					Date date = null;
					contestDVO.setCoId(row.getString("conid"));
					contestDVO.setClId(row.getString("clintid"));
					contestDVO.setMstqsnbnkid(row.getInt("mstqsnbnkid"));
					contestDVO.setAnsType(row.getString("anstype"));
					contestDVO.setImgU(row.getString("cardimgurl"));
					contestDVO.setSeqNo(row.getInt("cardseqno"));
					contestDVO.setCat(row.getString("cattype"));
					date = row.getTimestamp("conenddate");
					contestDVO.setEnDate(date != null ? date.getTime() : null);
					contestDVO.setName(row.getString("conname"));
					date = row.getTimestamp("consrtdate");
					contestDVO.setStDate(date != null ? date.getTime() : null);
					contestDVO.setCrBy(row.getString("creby"));
					date = row.getTimestamp("credate");
					contestDVO.setCrDate(date != null ? date.getTime() : null);
					contestDVO.setIsAct(row.getInt("isconactive"));
					contestDVO.setqMode(row.getString("qsnmode"));
					contestDVO.setSubCat(row.getString("subcattype"));
					contestDVO.setUpBy(row.getString("updby"));
					contestDVO.setThmColor(row.getString("bgthemcolor"));
					contestDVO.setThmImgMob(row.getString("bgthemimgurlmob"));
					contestDVO.setPlayinterval(row.getInt("playinterval"));
					date = row.getTimestamp("upddate");
					contestDVO.setUpDate(date != null ? date.getTime() : null);

				}
			}
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		} finally {

		}
		return contestDVO;
	}

	/**
	 * Gets the contest card.
	 *
	 * @param clintId  the Client ID
	 * @return the contest card
	 * @throws Exception the exception
	 */
	public static SnwContestDVO getContestCard(String clintId) throws Exception {
		ResultSet resultSet = null;
		SnwContestDVO contestDVO = null;
		try {
			resultSet = CassandraConnector.getSession()
					.execute("SELECT * from  pa_snwtx_contest_mstr  WHERE clintid=? ", clintId);

			if (resultSet != null) {
				for (Row rs : resultSet) {
					contestDVO = new SnwContestDVO();
					contestDVO.setCoId(rs.getString("conid"));
					contestDVO.setName(rs.getString("conname"));
				}
			}
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new SaasProcessException("Error Fetching Contest Cards", de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new SaasProcessException("Error Fetching Contest Cards", ex);
		} finally {

		}
		return contestDVO;
	}

	/**
	 * Gets the live contests by client id.
	 *
	 * @param clintId  the Client ID
	 * @return the live contests object ContestDVO
	 * @throws Exception the exception
	 */
	public static ContestDVO getLiveContestsByClientId(String clintId) throws Exception {
		ResultSet resultSet = null;
		ContestDVO contestDVO = null;
		try {
			resultSet = CassandraConnector.getSession()
					.execute("SELECT * from  pa_snwtx_contest_mstr  WHERE clintid=? ", clintId);

			if (resultSet != null) {
				for (Row rs : resultSet) {
					contestDVO = new ContestDVO();
					Date date = null;
					contestDVO.setCoId(rs.getString("conid"));
					contestDVO.setClId(rs.getString("clintid"));
					contestDVO.setAnsType(rs.getString("anstype"));
					contestDVO.setImgU(rs.getString("cardimgurl"));
					contestDVO.setSeqNo(rs.getInt("cardseqno"));
					contestDVO.setCat(rs.getString("cattype"));
					date = rs.getTimestamp("conenddate");
					contestDVO.setEnDate(date != null ? date.getTime() : null);
					contestDVO.setName(rs.getString("conname"));
					contestDVO.setPlayinterval(rs.getInt("playinterval"));
					date = rs.getTimestamp("consrtdate");
					contestDVO.setStDate(date != null ? date.getTime() : null);
					contestDVO.setIsAct(rs.getInt("isconactive"));
					contestDVO.setqMode(rs.getString("qsnmode"));
					contestDVO.setSubCat(rs.getString("subcattype"));
					contestDVO.setThmColor(rs.getString("bgthemcolor"));
					contestDVO.setThmImgDesk(rs.getString("bgthemimgurldes"));
					contestDVO.setThmImgMob(rs.getString("bgthemimgurlmob"));
					contestDVO.setPlayGameImg(rs.getString("playimgurl"));
					date = rs.getTimestamp("upddate");
					contestDVO.setUpDate(date != null ? date.getTime() : null);

				}
			}
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new SaasProcessException(de.getLocalizedMessage());
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new SaasProcessException(ex.getLocalizedMessage());
		} finally {

		}
		return contestDVO;
	}

}