/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.snw.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.exceptions.DriverException;
import com.rtf.saas.db.CassandraConnector;
import com.rtf.saas.exception.RTFDataAccessException;
import com.rtf.saas.util.GenUtil;
import com.rtf.snw.dvo.SNWCustomerAnswerDVO;

/**
 * The Class SNWQuestionAnswerDAO.
 */
public class SNWQuestionAnswerDAO {

	/**
	 * Fetch customer played info list.
	 *
	 * @param cliId  the Client ID
	 * @param coId   the Contest ID 
	 * @param cuId   the Customer ID
	 * @return the list
	 * @throws Exception the exception
	 */
	public static List<SNWCustomerAnswerDVO> fetchCustomerPlayedInfoList(String cliId, String coId, String cuId)
			throws Exception {

		StringBuffer sql = new StringBuffer();
		sql.append(" select qsnbnkid , conqsnid, clintid,conid ,custplayid ,custid , credate,qsnbnkans ,rwdtype ,");
		sql.append(" rwdval , toTimestamp(now()) as currdbTime   from   pt_snwtx_customer_answer where ");
		sql.append(" clintid = ? and conid=? and custid= ?  ");

		ResultSet resultSet = null;
		SNWCustomerAnswerDVO dvo = null;
		List<SNWCustomerAnswerDVO> dvoList = null;

		try {
			resultSet = CassandraConnector.getSession().execute(sql.toString(), cliId, coId, cuId);

			if (resultSet != null) {
				dvoList = new ArrayList<SNWCustomerAnswerDVO>();
				for (Row row : resultSet) {
					dvo = new SNWCustomerAnswerDVO();
					Date date = null;
					dvo.setQsnbnkid(row.getInt("qsnbnkid"));
					dvo.setConqsnid(row.getInt("conqsnid"));
					dvo.setClintid(row.getString("clintid"));
					dvo.setConid(row.getString("conid"));
					dvo.setCustplayid(row.getString("custplayid"));
					dvo.setCustid(row.getString("custid"));
					date = row.getTimestamp("credate");
					dvo.setCredate(date != null ? date.getTime() : null);
					dvo.setQsnbnkans(row.getString("qsnbnkans"));
					dvo.setRwdtype(row.getString("rwdtype"));
					dvo.setRwdvalue(row.getDouble("rwdval"));
					date = row.getTimestamp("currdbTime");
					dvo.setCurrDBTime(date != null ? date.getTime() : null);
					dvoList.add(dvo);
				}
			}

		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		}
		return dvoList;
	}

	/**
	 * Gets the customer answer.
	 *
	 * @param cliId   the Client ID
	 * @param coId    the Contest ID 
	 * @param cuId    the Customer ID
	 * @param playId the play id
	 * @return the customer answer
	 * @throws Exception the exception
	 */
	public static SNWCustomerAnswerDVO getCustomerAnswer(String cliId, String coId, String cuId, String playId)
			throws Exception {

		StringBuffer sql = new StringBuffer();
		sql.append(" select *  from   pt_snwtx_customer_answer where ");
		sql.append(" clintid = ? and conid=? and custid= ? and custplayid=? ");

		ResultSet resultSet = null;
		SNWCustomerAnswerDVO dvo = null;

		try {
			resultSet = CassandraConnector.getSession().execute(sql.toString(), cliId, coId, cuId.trim(), playId);

			if (resultSet != null) {
				for (Row row : resultSet) {
					dvo = new SNWCustomerAnswerDVO();
					Date date = null;
					dvo.setQsnbnkid(row.getInt("qsnbnkid"));
					dvo.setConqsnid(row.getInt("conqsnid"));
					dvo.setClintid(row.getString("clintid"));
					dvo.setConid(row.getString("conid"));
					dvo.setCustplayid(row.getString("custplayid"));
					dvo.setCustid(row.getString("custid"));
					date = row.getTimestamp("credate");
					dvo.setCredate(date != null ? date.getTime() : null);
					dvo.setQsnbnkans(row.getString("qsnbnkans"));
					dvo.setRwdtype(row.getString("rwdtype"));
					dvo.setRwdvalue(row.getDouble("rwdval"));
				}
			}

		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		}
		return dvo;
	}

	/**
	 * Gets the customer answers.
	 *
	 * @param cliId   the Client ID
	 * @param coId    the Contest ID 
	 * @param cuId    the Customer ID
	 * @param playId the play id
	 * @return the customer answers
	 * @throws Exception the exception
	 */
	public static SNWCustomerAnswerDVO getCustomerAnswers(String cliId, String coId, String cuId, String playId)
			throws Exception {

		StringBuffer sql = new StringBuffer();
		sql.append(" select qsnbnkid , conqsnid, clintid,conid ,custplayid ,custid , credate,qsnbnkans ,rwdtype ,");
		sql.append(" rwdval , toTimestamp(now()) as currdbTime  from   pt_snwtx_customer_answer where ");
		sql.append(" clintid = ? and conid=? and custid= ? and custplayid=? ");

		ResultSet resultSet = null;
		SNWCustomerAnswerDVO dvo = null;

		try {
			resultSet = CassandraConnector.getSession().execute(sql.toString(), cliId, coId, cuId, playId);

			if (resultSet != null) {
				for (Row row : resultSet) {
					dvo = new SNWCustomerAnswerDVO();
					Date date = null;
					dvo.setQsnbnkid(row.getInt("qsnbnkid"));
					dvo.setConqsnid(row.getInt("conqsnid"));
					dvo.setClintid(row.getString("clintid"));
					dvo.setConid(row.getString("conid"));
					dvo.setCustplayid(row.getString("custplayid"));
					dvo.setCustid(row.getString("custid"));
					date = row.getTimestamp("credate");
					dvo.setCredate(date != null ? date.getTime() : null);
					dvo.setQsnbnkans(row.getString("qsnbnkans"));
					dvo.setRwdtype(row.getString("rwdtype"));
					dvo.setRwdvalue(row.getDouble("rwdval"));
					date = row.getTimestamp("currdbTime");
					dvo.setCurrDBTime(date != null ? date.getTime() : null);
				}
			}

		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		}
		return dvo;
	}

	/**
	 * Gets the customer que answered by contest questions ids.
	 *
	 * @param clintId  the Client ID
	 * @param custId   the Customer ID
	 * @param coId     the Contest ID 
	 * @return the customer que answered by contest questions ids
	 */
	public static List<Integer> getCustomerQueAnsweredByContestQuestionsIds(String clintId, String custId,
			String coId) {
		StringBuffer sql = new StringBuffer();
		sql.append(" select conqsnid  from   pt_snwtx_customer_answer where ");
		sql.append(" clintid = ? and conid=? and custid= ?  ");
		ResultSet resultSet = null;
		List<Integer> idList = null;
		try {
			resultSet = CassandraConnector.getSession().execute(sql.toString(), clintId, custId, coId);
			if (resultSet != null) {
				idList = new ArrayList<Integer>();
				for (Row row : resultSet) {
					idList.add(row.getInt("conqsnid"));
				}
			}

		} catch (final DriverException de) {
			de.printStackTrace();

		} catch (Exception ex) {
			ex.printStackTrace();

		} finally {

		}
		return idList;
	}

	/**
	 * Gets the customer que answered by questions ids.
	 *
	 * @param clintId  the Client ID
	 * @param custId   the Customer ID
	 * @param coId     the Contest ID 
	 * @return the customer que answered by questions ids
	 */
	public static List<Integer> getCustomerQueAnsweredByQuestionsIds(String clintId, String custId, String coId) {
		StringBuffer sql = new StringBuffer();
		sql.append(" select qsnbnkid  from   pt_snwtx_customer_answer where ");
		sql.append(" clintid = ? and conid=? and custid= ?  ");
		ResultSet resultSet = null;
		List<Integer> idList = null;
		try {
			resultSet = CassandraConnector.getSession().execute(sql.toString(), clintId, custId, coId);
			if (resultSet != null) {
				idList = new ArrayList<Integer>();
				for (Row row : resultSet) {
					idList.add(row.getInt("qsnbnkid"));
				}
			}

		} catch (final DriverException de) {
			de.printStackTrace();

		} catch (Exception ex) {
			ex.printStackTrace();

		} finally {

		}
		return idList;
	}

	/**
	 * Insert customer quen answer details.
	 *
	 * @param obj the obj
	 * @return true, if successful
	 * @throws Exception the exception
	 */
	public static boolean insertCustomerQuenAnswerDetails(SNWCustomerAnswerDVO obj) throws Exception {

		boolean sts = false;

		try {

			CassandraConnector.getSession().execute(
					" INSERT INTO pt_snwtx_customer_answer  " + "(clintid,conid,custid,custplayid,"
							+ " conqsnid ,  qsnbnkid , qsnbnkans ,   credate  ) " + " VALUES "
							+ " (?,?, ?, ?,  ?, ?, ?, toTimestamp(now())) ",
					obj.getClintid(), obj.getConid(), obj.getCustid(), obj.getCustplayid(), obj.getConqsnid(),
					obj.getQsnbnkid(), obj.getQsnbnkans());
			sts = true;
		} catch (final DriverException de) {
			de.printStackTrace();
			sts = false;
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		}
		return sts;
	}

	/**
	 * Update answer migration status.
	 *
	 * @param obj the obj
	 * @return the SNW customer answer DVO
	 * @throws Exception the exception
	 */
	public static SNWCustomerAnswerDVO updateAnswerMigrationStatus(SNWCustomerAnswerDVO obj) throws Exception {

		try {

			CassandraConnector.getSession().execute(
					" UPDATE pt_snwtx_customer_answer set " + " migrate= ? ,upddate = toTimestamp(now())   "
							+ " WHERE clintid=? and conid=? AND custid=?  and custplayid=? ",
					true, obj.getClintid(), obj.getConid(), obj.getCustid(), obj.getCustplayid());
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		}
		return obj;
	}

	/**
	 * Update customer quen answer details.
	 *
	 * @param obj the obj
	 * @return the SNW customer answer DVO
	 * @throws Exception the exception
	 */
	public static SNWCustomerAnswerDVO updateCustomerQuenAnswerDetails(SNWCustomerAnswerDVO obj) throws Exception {

		try {

			CassandraConnector.getSession().execute(
					" UPDATE pt_snwtx_customer_answer set " + " custans = ? , iscrtans = ? , "
							+ " rwdtype = ?  , rwdval = ? , upddate = toTimestamp(now())   "
							+ " WHERE clintid=? and conid=? AND custid=?  and custplayid=? ",
					obj.getCustans(), obj.isIscrtans(), obj.getRwdtype(), obj.getRwdvalue(), obj.getClintid(),
					obj.getConid(), obj.getCustid(), obj.getCustplayid());
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		}
		return obj;
	}

}
