/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.common.dao;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.exceptions.DriverException;
import com.rtf.common.dvo.ClientConfigDVO;
import com.rtf.saas.db.CassandraConnector;
import com.rtf.saas.exception.RTFDataAccessException;
import com.rtf.saas.util.GenUtil;

/**
 * The Class ClientConfigDAO.
 */
public class ClientConfigDAO {

	/**
	 * Gets the contest config by Primary key.
	 *
	 * @param clintId the clint id
	 * @param prodId  the prod id
	 * @param key     the key
	 * @return the contest config by key
	 * @throws Exception the exception
	 */
	public static ClientConfigDVO getContestConfigByKey(String clintId, String prodId, String key) throws Exception {
		ResultSet resultSet = null;
		ClientConfigDVO config = null;
		try {
			resultSet = CassandraConnector.getSession().execute(
					"SELECT * from  pa_commn_client_config  WHERE clintid=? AND prodrkeytype=? AND keyid = ?", clintId,
					prodId, key);
			if (resultSet != null) {
				for (Row row : resultSet) {
					config = new ClientConfigDVO();
					config.setClId(row.getString("clintid"));
					config.setProdId(row.getString("prodrkeytype"));
					config.setKey(row.getString("keyid"));
					config.setValue(row.getString("keyvalue"));
				}
			}
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		}
		return config;
	}
}
