/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.common.dao;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.exceptions.DriverException;
import com.rtf.common.dvo.RewardTypeDVO;
import com.rtf.saas.db.CassandraConnector;
import com.rtf.saas.exception.RTFDataAccessException;
import com.rtf.saas.util.GenUtil;

/**
 * The Class RewardTypeDAO.
 */

public class RewardTypeDAO {

	/**
	 * Gets the reward type Configured for a Client.
	 *
	 * @param clintId the clint id
	 * @param rwdType the rwd type
	 * @return the reward type
	 * @throws Exception the exception
	 */
	public static RewardTypeDVO getRewardType(String clintId, String rwdType) throws Exception {

		ResultSet resultSet = null;
		RewardTypeDVO dvo = null;
		try {
			String sql = "SELECT clintid,rwdtype,isactive,rwdimgurl,rwdunimesr,rwddesc from  sd_reward_type where clintid=? and rwdtype=?  ";
			resultSet = CassandraConnector.getSession().execute(sql, clintId, rwdType);

			if (resultSet != null) {
				for (Row row : resultSet) {
					dvo = new RewardTypeDVO();
					dvo.setClId(row.getString("clintid"));
					dvo.setRwdType(row.getString("rwdtype"));
					dvo.setIsactive(row.getBool("isactive"));
					dvo.setRwdimgurl(row.getString("rwdimgurl"));
					dvo.setRwddesc(row.getString("rwddesc"));
					dvo.setRwdunimesr(row.getString("rwdunimesr"));
				}
			}
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		} finally {

		}
		return dvo;
	}

	/**
	 * Gets the reward type image url.
	 *
	 * @param clintId the clint id
	 * @param rwdType the rwd type
	 * @return the reward type image url
	 * @throws Exception the exception
	 */
	public static String getRewardTypeImageUrl(String clintId, String rwdType) throws Exception {
		ResultSet resultSet = null;
		String rewardImgUrl = null;
		try {
			String sql = "SELECT rwdimgurl from  sd_reward_type where clintid=? and rwdtype=?  ";
			resultSet = CassandraConnector.getSession().execute(sql, clintId, rwdType);

			if (resultSet != null) {
				for (Row row : resultSet) {
					rewardImgUrl = row.getString("rwdimgurl");
				}
			}
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		} finally {

		}
		return rewardImgUrl;
	}
}
