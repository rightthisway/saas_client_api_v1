/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.common.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.rtf.common.dvo.RewardTypeDVO;
import com.rtf.common.util.DatabaseConnections;

/**
 * The Class RewardTypeSQLDAO.
 */
public class RewardTypeSQLDAO {
	/**
	 * Gets the reward type Record for a Given Client and Reward Type.
	 *
	 * @param clId  the cl id
	 * @param rType the r type
	 * @return the reward type
	 */
	public static RewardTypeDVO getRewardType(String clId, String rType) {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		RewardTypeDVO rwdType = null;
		String sql = "SELECT clintid,rwdtype,rwdunimesr from  sd_reward_type where clintid=? and rwdtype=? ";
		try {
			conn = DatabaseConnections.getRtfSaasConnection();
			ps = conn.prepareStatement(sql);
			ps.setString(1, clId);
			ps.setString(2, rType);
			rs = ps.executeQuery();
			while (rs.next()) {
				rwdType = new RewardTypeDVO();
				rwdType.setClId(rs.getString("clintid"));
				rwdType.setRwdType(rs.getString("rwdtype"));
				rwdType.setRwdunimesr("rwdunimesr");
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return rwdType;
	}

}
