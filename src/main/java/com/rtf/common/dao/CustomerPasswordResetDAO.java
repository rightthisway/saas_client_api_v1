/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.common.dao;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.exceptions.DriverException;
import com.rtf.client.signup.dvo.CustomerPasswordResetDVO;
import com.rtf.saas.db.CassandraConnector;
import com.rtf.saas.exception.RTFDataAccessException;
import com.rtf.saas.util.GenUtil;

/**
 * The Class CustomerPasswordResetDAO.
 */
public class CustomerPasswordResetDAO {

	/**
	 * Gets the customer password reset record.
	 *
	 * @param clintId the clint id
	 * @param custId  the cust id
	 * @param token   the token
	 * @return the cust pass reset record
	 * @throws Exception the exception
	 */
	public static CustomerPasswordResetDVO getCustPassResetRecord(String clintId, String custId, String token)
			throws Exception {
		ResultSet resultSet = null;
		CustomerPasswordResetDVO dvo = null;
		try {
			String sql = "SELECT * from pu_client_customer_pw_reset_tracking where clintid=? AND custid=? AND passtoken=?";
			resultSet = CassandraConnector.getSession().execute(sql, clintId, custId, token);
			if (resultSet != null) {
				for (Row row : resultSet) {
					dvo = new CustomerPasswordResetDVO();
					dvo.setClId(row.getString("clintid"));
					dvo.setBaseUrl(row.getString("passtokenurl"));
					dvo.setCustId(row.getString("custid"));
					dvo.setCustEmail(row.getString("custemail"));
					dvo.setCrDate(row.getTimestamp("credate"));
					dvo.setIsValidated(row.getBool("isvalidated"));
					dvo.setToken(row.getString("passtoken"));
					dvo.setValidateDate(row.getTimestamp("validateddatetime"));
				}
			}
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		} finally {

		}
		return dvo;
	}

	/**
	 * Save customer password reset record.
	 *
	 * @param reset the reset
	 * @return true, if successful
	 * @throws Exception the exception
	 */
	public static boolean saveCustPassResetRecord(CustomerPasswordResetDVO reset) throws Exception {
		boolean isUpdated = false;
		StringBuffer sql = new StringBuffer();
		sql.append(
				"INSERT INTO pu_client_customer_pw_reset_tracking (clintid,custid,custemail,passtokenurl,passtoken,credate,validateddatetime,isvalidated) ");
		sql.append("VALUES (?,?,?,?,?,toTimestamp(now()),?,?) ");
		try {
			CassandraConnector.getSession().execute(sql.toString(),
					new Object[] { reset.getClId(), reset.getCustId(), reset.getCustEmail(), reset.getBaseUrl(),
							reset.getToken(), reset.getValidateDate(), reset.getIsValidated() });
			isUpdated = true;
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		} finally {

		}
		return isUpdated;
	}

	/**
	 * Update customer password reset record.
	 *
	 * @param reset the reset
	 * @return true, if successful
	 * @throws Exception the exception
	 */
	public static boolean updateCustPassResetRecord(CustomerPasswordResetDVO reset) throws Exception {
		boolean isUpdated = false;
		StringBuffer sql = new StringBuffer();
		sql.append("UPDATE pu_client_customer_pw_reset_tracking SET ");
		sql.append("isvalidated=?,validateddatetime=toTimestamp(now()) ");
		sql.append("WHERE clintid=? AND custid=? AND passtoken=?");
		try {
			CassandraConnector.getSession().execute(sql.toString(),
					new Object[] { reset.getIsValidated(), reset.getClId(), reset.getCustId(), reset.getToken() });
			isUpdated = true;
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		} finally {

		}
		return isUpdated;
	}
}
