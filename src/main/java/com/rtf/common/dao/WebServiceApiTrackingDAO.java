/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.common.dao;

import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.exceptions.DriverException;
import com.rtf.common.dvo.WebServiceApiTrackingDVO;
import com.rtf.saas.db.CassandraConnector;

/**
 * The Class WebServiceApiTrackingDAO.
 */
public class WebServiceApiTrackingDAO {

	/**
	 * Gets the current time stamp.
	 *
	 * @return the current time stamp
	 */
	private static java.sql.Timestamp getCurrentTimeStamp() {

		java.util.Date today = new java.util.Date();
		return new java.sql.Timestamp(today.getTime());

	}

	/**
	 * Insert api tracking details for all Events in SaaS Client API
	 *
	 * @param dvo the dvo
	 * @throws Exception the exception
	 */
	public static void insertWSApiTracking(WebServiceApiTrackingDVO dvo) throws Exception {

		Session session = CassandraConnector.getSession();
		StringBuffer sql = new StringBuffer();
		sql.append(" insert into saas_prod_web_service_cust_track_cass ");
		sql.append(" (clintid,custid, apiname ,   ");
		sql.append(" hitdate , apistarttime , apiendtime , ");
		sql.append(" actresult , custipaddr , conid , platform ,");
		sql.append(" prodcode , description , sessionid  ");
		sql.append(" ,resstatus ,nodeid,deviceinfo ) ");
		sql.append(" values (?,?,?, toTimestamp(now()),?,?, ?,?,?,?, ?,?,?, ?,?,? )");
		try {
			PreparedStatement statement = session.prepare(sql.toString());
			BoundStatement boundStatement = statement.bind(dvo.getClintid(), dvo.getCustid(), dvo.getApiname(),
					dvo.getApistarttime(), dvo.getApiendtime(), dvo.getActresult(), dvo.getCustipaddr(), dvo.getConid(),
					dvo.getPlatform(), dvo.getProdcode(), dvo.getDescription(), dvo.getSessionid(), dvo.getResstatus(),
					dvo.getNodeid(), dvo.getDeviceinfo()

			);
			session.executeAsync(boundStatement);

		} catch (final DriverException de) {
			de.printStackTrace();
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {

		}

	}

}
