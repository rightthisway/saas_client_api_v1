/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.common.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.datastax.driver.core.BatchStatement;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.exceptions.DriverException;
import com.rtf.common.dvo.ClientCustomerDVO;
import com.rtf.saas.db.CassandraConnector;
import com.rtf.saas.exception.RTFDataAccessException;
import com.rtf.saas.util.GenUtil;

/**
 * The Class ClientCustomerDAO.
 */
public class ClientCustomerDAO {

	/**
	 * Gets the all client customer for validation.
	 *
	 * @param clintId the clint id
	 * @return the all client customer for validation
	 * @throws Exception the exception
	 */
	public static Map<String, String> getAllClientCustomerForValidation(String clintId) throws Exception {

		ResultSet resultSet = null;
		Map<String, String> custMap = new HashMap<String, String>();
		try {
			String sql = "SELECT custid,userid,enckey from pu_client_customer where clintid=? ";
			resultSet = CassandraConnector.getSession().execute(sql, clintId);
			if (resultSet != null) {
				for (Row row : resultSet) {
					custMap.put(row.getString("custId") + "_" + row.getString("custId"), row.getString("userid"));
				}
			}
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		} finally {

		}
		return custMap;
	}

	/**
	 * Gets the all customers from DB records across all Clients.
	 *
	 * @return the allcustomers
	 * @throws Exception the exception
	 */
	public static List<ClientCustomerDVO> getallcustomers() throws Exception {

		ResultSet resultSet = null;
		List<ClientCustomerDVO> list = new ArrayList<ClientCustomerDVO>();
		ClientCustomerDVO dvo = null;
		try {
			String sql = "SELECT * from  pu_client_customer  ";
			resultSet = CassandraConnector.getSession().execute(sql);
			if (resultSet != null) {
				for (Row row : resultSet) {
					dvo = new ClientCustomerDVO();
					dvo.setClId(row.getString("clintid"));
					dvo.setCuId(row.getString("custid"));
					dvo.setEmail(row.getString("custemail"));
					dvo.setFname(row.getString("custfname"));
					dvo.setLname(row.getString("custlname"));
					dvo.setPhone(row.getString("custphone"));
					dvo.setPurl(row.getString("custpicurl"));
					dvo.setUplby(row.getString("uplby"));
					dvo.setPassword(row.getString("password"));
					dvo.setSignUpType(row.getString("signuptype"));
					dvo.setSchoolName(row.getString("schoolname"));
					dvo.setTokenId(row.getString("tokenid"));
					dvo.setFbTokenId(row.getString("tokenidfb"));
					dvo.setUserId(row.getString("userid"));
					dvo.setFbUid(row.getString("fbuserid"));
					dvo.setUpldate(row.getTimestamp("upldate"));
					dvo.setCe(row.getString("enckey"));
					list.add(dvo);
				}
			}
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		} finally {

		}
		return list;
	}

	/**
	 * Gets the client customer by customer id.
	 *
	 * @param clintId the clint id
	 * @param custId  the cust id
	 * @return the client customer by customer id
	 * @throws Exception the exception
	 */
	public static ClientCustomerDVO getClientCustomerByCustomerId(String clintId, String custId) throws Exception {

		ResultSet resultSet = null;
		ClientCustomerDVO dvo = null;
		try {
			String sql = "SELECT * from  pu_client_customer where clintid=? and custid=?  ";
			resultSet = CassandraConnector.getSession().execute(sql, clintId, custId);

			if (resultSet != null) {
				for (Row row : resultSet) {
					dvo = new ClientCustomerDVO();
					dvo.setClId(row.getString("clintid"));
					dvo.setCuId(row.getString("custid"));
					dvo.setEmail(row.getString("custemail"));
					dvo.setFname(row.getString("custfname"));
					dvo.setLname(row.getString("custlname"));
					dvo.setPhone(row.getString("custphone"));
					dvo.setPurl(row.getString("custpicurl"));
					dvo.setUplby(row.getString("uplby"));
					dvo.setPassword(row.getString("password"));
					dvo.setSignUpType(row.getString("signuptype"));
					dvo.setSchoolName(row.getString("schoolname"));
					dvo.setTokenId(row.getString("tokenid"));
					dvo.setFbTokenId(row.getString("tokenidfb"));
					dvo.setFbUid(row.getString("fbuserid"));
					dvo.setUserId(row.getString("userid"));
					dvo.setFsConfId(row.getInt("fsconfid"));
					dvo.setCe(row.getString("enckey"));
					dvo.setCoId(row.getString("lstconid"));
				}
			}
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		} finally {

		}
		return dvo;
	}

	/**
	 * Gets the client customer by customer id and User id.
	 *
	 * @param clintId the clint id
	 * @param custId  the cust id
	 * @param uId     the u id
	 * @return the client customer by customer id and U id
	 * @throws Exception the exception
	 */
	public static ClientCustomerDVO getClientCustomerByCustomerIdAndUId(String clintId, String custId, String uId)
			throws Exception {

		ResultSet resultSet = null;
		ClientCustomerDVO dvo = null;
		try {
			String sql = "SELECT * from  pu_client_customer where clintid=? and custid=? AND userid=? ";
			resultSet = CassandraConnector.getSession().execute(sql, clintId, custId, uId);

			if (resultSet != null) {
				for (Row row : resultSet) {
					dvo = new ClientCustomerDVO();
					dvo.setClId(row.getString("clintid"));
					dvo.setCuId(row.getString("custid"));
					dvo.setEmail(row.getString("custemail"));
					dvo.setFname(row.getString("custfname"));
					dvo.setLname(row.getString("custlname"));
					dvo.setPhone(row.getString("custphone"));
					dvo.setPurl(row.getString("custpicurl"));
					dvo.setUplby(row.getString("uplby"));
					dvo.setPassword(row.getString("password"));
					dvo.setSignUpType(row.getString("signuptype"));
					dvo.setSchoolName(row.getString("schoolname"));
					dvo.setTokenId(row.getString("tokenid"));
					dvo.setFbTokenId(row.getString("tokenidfb"));
					dvo.setFbUid(row.getString("fbuserid"));
					dvo.setUserId(row.getString("userid"));
					dvo.setFsConfId(row.getInt("fsconfid"));
					dvo.setCe(row.getString("enckey"));
				}
			}
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		} finally {

		}
		return dvo;
	}

	/**
	 * Gets the client customer by email and Client Id
	 *
	 * @param clintId the clint id
	 * @param email   the email
	 * @return the client customer by email
	 * @throws Exception the exception
	 */
	public static ClientCustomerDVO getClientCustomerByEmail(String clintId, String email) throws Exception {

		ResultSet resultSet = null;
		ClientCustomerDVO dvo = null;
		try {
			String sql = "SELECT * from  pu_client_customer where clintid=? and custid=?  ";
			resultSet = CassandraConnector.getSession().execute(sql, clintId, email);
			if (resultSet != null) {
				for (Row row : resultSet) {
					dvo = new ClientCustomerDVO();
					dvo.setClId(row.getString("clintid"));
					dvo.setCuId(row.getString("custid"));
					dvo.setEmail(row.getString("custemail"));
					dvo.setPassword(row.getString("password"));
					dvo.setFname(row.getString("custfname"));
					dvo.setLname(row.getString("custlname"));
					dvo.setPhone(row.getString("custphone"));
					dvo.setPurl(row.getString("custpicurl"));
					dvo.setUplby(row.getString("uplby"));
					dvo.setUserId(row.getString("userid"));
					dvo.setCe(row.getString("enckey"));					
				}
			}
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		} finally {

		}
		return dvo;
	}

	/**
	 * Gets the client customer by customer id and Client Id
	 *
	 * @param clintId the clint id
	 * @param custId  the cust id
	 * @return the client customer for validation by customer id
	 * @throws Exception the exception
	 */
	public static ClientCustomerDVO getClientCustomerForValidationByCustomerId(String clintId, String custId)
			throws Exception {
		ResultSet resultSet = null;
		ClientCustomerDVO dvo = null;
		try {
			String sql = "SELECT userid,enckey from  pu_client_customer where clintid=? and custid=?  ";
			resultSet = CassandraConnector.getSession().execute(sql, clintId, custId);
			if (resultSet != null) {
				for (Row row : resultSet) {
					dvo = new ClientCustomerDVO();
					dvo.setClId(clintId);
					dvo.setCuId(custId);
					dvo.setUserId(row.getString("userid"));
					dvo.setCe(row.getString("enckey"));
				}
			}
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		} finally {

		}
		return dvo;
	}

	/**
	 * Gets the  customers By Client Id.
	 *
	 * @param clintId the clint id
	 * @return the client customers
	 * @throws Exception the exception
	 */
	public static List<ClientCustomerDVO> getClientCustomers(String clintId) throws Exception {

		ResultSet resultSet = null;
		ClientCustomerDVO dvo = null;
		List<ClientCustomerDVO> list = new ArrayList<ClientCustomerDVO>();
		try {
			String sql = "SELECT * from  pu_client_customer where clintid=? ";
			resultSet = CassandraConnector.getSession().execute(sql, clintId);
			if (resultSet != null) {
				for (Row row : resultSet) {
					dvo = new ClientCustomerDVO();
					dvo.setClId(row.getString("clintid"));
					dvo.setCuId(row.getString("custid"));
					dvo.setEmail(row.getString("custemail"));
					dvo.setFname(row.getString("custfname"));
					dvo.setLname(row.getString("custlname"));
					dvo.setPhone(row.getString("custphone"));
					dvo.setPurl(row.getString("custpicurl"));
					dvo.setUplby(row.getString("uplby"));
					dvo.setPassword(row.getString("password"));
					dvo.setSignUpType(row.getString("signuptype"));
					dvo.setSchoolName(row.getString("schoolname"));
					dvo.setTokenId(row.getString("tokenid"));
					dvo.setFbTokenId(row.getString("tokenidfb"));
					dvo.setFbUid(row.getString("fbuserid"));
					dvo.setUserId(row.getString("userid"));
					dvo.setFsConfId(row.getInt("fsconfid"));
					dvo.setCe(row.getString("enckey"));
					list.add(dvo);
				}
			}
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		} finally {

		}
		return list;
	}

	/**
	 * Gets the client customer By  user id and Client Id.
	 *
	 * @param clintId the clint id
	 * @param userId  the user id
	 * @return the client customer user id
	 * @throws Exception the exception
	 */
	public static String getClientCustomerUserId(String clintId, String userId) throws Exception {

		ResultSet resultSet = null;
		String userDbId = null;
		try {
			String sql = "SELECT userid from  pu_client_customer where clintid=? and userid=?  ";
			resultSet = CassandraConnector.getSession().execute(sql, clintId, userId);

			if (resultSet != null) {
				for (Row row : resultSet) {
					userDbId = row.getString("userid");
				}
			}
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		} finally {

		}
		return userDbId;
	}

	/**
	 * Gets the client Customer based on Client Id and Customer Id
	 *
	 * @param clintId the client  id
	 * @param custId  the customer id
	 * @return the client customer with password by customer id
	 * @throws Exception the exception
	 */
	public static ClientCustomerDVO getClientCustomerWithPasswordByCustomerId(String clintId, String custId)
			throws Exception {

		ResultSet resultSet = null;
		ClientCustomerDVO dvo = null;
		try {
			String sql = "SELECT * from  pu_client_customer where clintid=? and custid=?  ";
			resultSet = CassandraConnector.getSession().execute(sql, clintId, custId);

			if (resultSet != null) {
				for (Row row : resultSet) {
					dvo = new ClientCustomerDVO();
					dvo.setClId(row.getString("clintid"));
					dvo.setCuId(row.getString("custid"));
					dvo.setEmail(row.getString("custemail"));
					dvo.setFname(row.getString("custfname"));
					dvo.setLname(row.getString("custlname"));
					dvo.setPhone(row.getString("custphone"));
					dvo.setPurl(row.getString("custpicurl"));
					dvo.setUplby(row.getString("uplby"));
					dvo.setPassword(row.getString("password"));
					dvo.setSignUpType(row.getString("signuptype"));
					dvo.setSchoolName(row.getString("schoolname"));
					dvo.setTokenId(row.getString("tokenid"));
					dvo.setFbTokenId(row.getString("tokenidfb"));
					dvo.setUserId(row.getString("userid"));
					dvo.setFbUid(row.getString("fbuserid"));
					dvo.setFsConfId(row.getInt("fsconfid"));
					dvo.setCe(row.getString("enckey"));
				}
			}
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		} finally {

		}
		return dvo;
	}

	/**
	 * Gets the client customer by fb token id and Client Id.
	 *
	 * @param clintId   the clint id
	 * @param fbToeknId the fb toekn id
	 * @return the client customer with password by fb token id
	 * @throws Exception the exception
	 */
	public static ClientCustomerDVO getClientCustomerWithPasswordByFbTokenId(String clintId, String fbToeknId)
			throws Exception {

		ResultSet resultSet = null;
		ClientCustomerDVO dvo = null;
		try {
			String sql = "SELECT * from  pu_client_customer where clintid=? and tokenidfb =?  ";
			resultSet = CassandraConnector.getSession().execute(sql, clintId, fbToeknId);

			if (resultSet != null) {
				for (Row row : resultSet) {
					dvo = new ClientCustomerDVO();
					dvo.setClId(row.getString("clintid"));
					dvo.setCuId(row.getString("custid"));
					dvo.setEmail(row.getString("custemail"));
					dvo.setFname(row.getString("custfname"));
					dvo.setLname(row.getString("custlname"));
					dvo.setPhone(row.getString("custphone"));
					dvo.setPurl(row.getString("custpicurl"));
					dvo.setUplby(row.getString("uplby"));
					dvo.setPassword(row.getString("password"));
					dvo.setSignUpType(row.getString("signuptype"));
					dvo.setSchoolName(row.getString("schoolname"));
					dvo.setTokenId(row.getString("tokenid"));
					dvo.setFbTokenId(row.getString("tokenidfb"));
					dvo.setFbUid(row.getString("fbuserid"));
					dvo.setUserId(row.getString("userid"));
					dvo.setFsConfId(row.getInt("fsconfid"));
					dvo.setCe(row.getString("enckey"));					
				}
			}
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		} finally {

		}
		return dvo;
	}

	/**
	 * Gets the client customer  by fb user id and Client Id.
	 *
	 * @param clintId  the clint id
	 * @param fbUserId the fb user id
	 * @return the client customer with password by fb user id
	 * @throws Exception the exception
	 */
	public static ClientCustomerDVO getClientCustomerWithPasswordByFbUserId(String clintId, String fbUserId)
			throws Exception {

		ResultSet resultSet = null;
		ClientCustomerDVO dvo = null;
		try {
			String sql = "SELECT * from  pu_client_customer where clintid=? and fbuserid =?  ";
			resultSet = CassandraConnector.getSession().execute(sql, clintId, fbUserId);

			if (resultSet != null) {
				for (Row row : resultSet) {
					dvo = new ClientCustomerDVO();
					dvo.setClId(row.getString("clintid"));
					dvo.setCuId(row.getString("custid"));
					dvo.setEmail(row.getString("custemail"));
					dvo.setFname(row.getString("custfname"));
					dvo.setLname(row.getString("custlname"));
					dvo.setPhone(row.getString("custphone"));
					dvo.setPurl(row.getString("custpicurl"));
					dvo.setUplby(row.getString("uplby"));
					dvo.setPassword(row.getString("password"));
					dvo.setSignUpType(row.getString("signuptype"));
					dvo.setSchoolName(row.getString("schoolname"));
					dvo.setTokenId(row.getString("tokenid"));
					dvo.setFbTokenId(row.getString("tokenidfb"));
					dvo.setFbUid(row.getString("fbuserid"));
					dvo.setUserId(row.getString("userid"));
					dvo.setFsConfId(row.getInt("fsconfid"));
					dvo.setCe(row.getString("enckey"));
					// dvo.setUpldate(row.getTimestamp("upldate"));
				}
			}
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		} finally {

		}
		return dvo;
	}

	/**
	 * Save client customer.
	 * 
	 *
	 * @param obj the obj
	 * @return the boolean
	 * @throws Exception the exception
	 */
	public static Boolean saveClientCustomer(ClientCustomerDVO obj) throws Exception {
		try {
			CassandraConnector.getSession().execute(
					"INSERT INTO pu_client_customer (clintid,custid,custemail,custfname,custlname,custphone,"
							+ "custpicurl,uplby,upldate,password,schoolname,signuptype,tokenid,tokenidfb,fbuserid,userid,enckey,lstconid)  VALUES (?,?,?, ?,?,?,?,?,toTimestamp(now()),?,?,?,?,?,?,?,?,?)",
					obj.getClId(), obj.getCuId(), obj.getEmail(), obj.getFname(), obj.getLname(), obj.getPhone(),
					obj.getPurl(), obj.getUplby(), obj.getPassword(), obj.getSchoolName(), obj.getSignUpType(),
					obj.getTokenId(), obj.getFbTokenId(), obj.getFbUid(), obj.getUserId(), obj.getCe(),obj.getCoId());			
			return true;
		} catch (final DriverException de) {
			de.printStackTrace();
			// Start New Thread to log Exception in DataBase in the RTFDataAccessException
			// constructor. ...
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		}
	}

	/**
	 * Update client customer Details.
	 *
	 * @param obj the obj
	 * @return the boolean
	 * @throws Exception the exception
	 */
	public static Boolean updateClientCustomer(ClientCustomerDVO obj) throws Exception {
		try {
			CassandraConnector.getSession().execute(
					"UPDATE pu_client_customer set tokenid=?,signuptype=?,tokenidfb=?,fbuserid=?,enckey=? WHERE clintid=? AND custid=?", // ,upldate=toTimestamp(now())
					obj.getTokenId(), obj.getSignUpType(), obj.getFbTokenId(), obj.getFbUid(), obj.getCe(),
					obj.getClId(), obj.getCuId());			
			return true;
		} catch (final DriverException de) {
			de.printStackTrace();
			// Start New Thread to log Exception in DataBase in the RTFDataAccessException
			// constructor. ...
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		}
	}

	/**
	 * Update client customer firestore configuraion.
	 *
	 * @param obj the obj
	 * @return the boolean
	 * @throws Exception the exception
	 */
	public static Boolean updateClientCustomerFirestoreConfi(ClientCustomerDVO obj) throws Exception {
		try {
			CassandraConnector.getSession().execute(
					"UPDATE pu_client_customer set fsconfid=? WHERE clintid=? AND custid=?",
					obj.getFsConfId(), obj.getClId(), obj.getCuId());			
			return true;
		} catch (final DriverException de) {
			de.printStackTrace();
			// Start New Thread to log Exception in DataBase in the RTFDataAccessException
			// constructor. ...
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		}
	}

	/**
	 * Update client customer password.
	 *
	 * @param obj the obj
	 * @return the boolean
	 * @throws Exception the exception
	 */
	public static Boolean updateClientCustomerPassword(ClientCustomerDVO obj) throws Exception {
		try {
			CassandraConnector.getSession().execute(
					"UPDATE pu_client_customer set password=? WHERE clintid=? AND custid=?", obj.getPassword(),
					obj.getClId(), obj.getCuId());
			return true;
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		}
	}

	/**
	 * Update client customer school name.
	 *
	 * @param obj the obj
	 * @return the boolean
	 * @throws Exception the exception
	 */
	public static Boolean updateClientCustomerSchoolName(ClientCustomerDVO obj) throws Exception {
		try {
			CassandraConnector.getSession().execute(
					"UPDATE pu_client_customer set schoolname=? WHERE clintid=? AND custid=?", obj.getSchoolName(),
					obj.getClId(), obj.getCuId());
			return true;
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		}
	}

	/**
	 * Update first name User Id based on Customer Id for a given Client Id .
	 *
	 * @param obj the obj
	 * @return the boolean
	 * @throws Exception the exception
	 */
	public static Boolean updateFirstName(ClientCustomerDVO obj) throws Exception {
		try {
			CassandraConnector.getSession().execute(
					"UPDATE pu_client_customer set custfname=?,userid=? WHERE clintid=? AND custid=?", obj.getFname(),
					obj.getUserId(), obj.getClId(), obj.getCuId());
			return true;
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		}
	}

	/**
	 * Save all Client Customer Objects. Save Multiple Records based on List of Customer objects.
	 *
	 * @param objList the obj list
	 */
	public void saveAll(List<ClientCustomerDVO> objList) throws Exception {
		Session session = CassandraConnector.getSession();
		BatchStatement batchStatement = new BatchStatement();
		PreparedStatement preparedStatement = session
				.prepare("INSERT INTO pu_client_customer (clintid,custid,custemail,custfname,custlname,custphone,"
						+ "custpicurl,uplby,upldate,password,schoolname,signuptype,tokenid,tokenidfb,fbuserid,userid)  VALUES (?,?,?, ?,?,?,?,?,toTimestamp(now()),?,?,?,?,?,?,?)");

		for (ClientCustomerDVO obj : objList) {
			batchStatement.add(preparedStatement.bind(obj.getClId(), obj.getCuId(), obj.getEmail(), obj.getFname(),
					obj.getLname(), obj.getPhone(), obj.getPurl(), obj.getUplby(), obj.getPassword(),
					obj.getSchoolName(), obj.getSignUpType(), obj.getTokenId(), obj.getFbTokenId(), obj.getFbUid(),
					obj.getUserId()));

		}
		try {
			session.execute(batchStatement);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		}
	}

}
