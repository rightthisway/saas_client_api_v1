/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.common.dao;

import java.util.ArrayList;
import java.util.List;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.exceptions.DriverException;
import com.rtf.common.dvo.ClientCustomerEmailVerifyDVO;
import com.rtf.saas.db.CassandraConnector;
import com.rtf.saas.exception.RTFDataAccessException;
import com.rtf.saas.util.GenUtil;

/**
 * The Class ClientCustomerEmailVerifyDAO.
 */
public class ClientCustomerEmailVerifyDAO {

	/**
	 * Delete client customer from Records
	 *
	 * @param obj the obj
	 * @return the boolean
	 * @throws Exception the exception
	 */
	public static Boolean deleteClientCustomerEmailVerify(ClientCustomerEmailVerifyDVO obj) throws Exception {
		try {
			CassandraConnector.getSession().execute(
					"delete from pu_client_customer_fb_veri WHERE clintid=? AND tokenidfb=?", obj.getClId(),
					obj.getFbTokenId());
			return true;
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		}
	}

	/**
	 * Gets the all client customer  by customer id and fb userid.
	 *
	 * @return the all client customer email verify by customer idand fb userid
	 * @throws Exception the exception
	 */
	public static List<ClientCustomerEmailVerifyDVO> getAllClientCustomerEmailVerifyByCustomerIdandFbUserid()
			throws Exception {

		ResultSet resultSet = null;
		List<ClientCustomerEmailVerifyDVO> list = new ArrayList<ClientCustomerEmailVerifyDVO>();
		ClientCustomerEmailVerifyDVO dvo = null;
		try {
			String sql = "SELECT * from  pu_client_customer_fb_veri  ";
			resultSet = CassandraConnector.getSession().execute(sql);

			if (resultSet != null) {
				for (Row row : resultSet) {
					dvo = new ClientCustomerEmailVerifyDVO();
					dvo.setClId(row.getString("clintid"));
					dvo.setCuId(row.getString("custid"));
					dvo.setEmail(row.getString("custemail"));
					dvo.setPhone(row.getString("custphone"));
					dvo.setFbTokenId(row.getString("tokenidfb"));
					dvo.setFbUid(row.getString("fbuserid"));
					list.add(dvo);
				}
			}
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		} finally {

		}
		return list;
	}

	/**
	 * Gets the client customer By customer id and Token Id.
	 *
	 * @param clintId   the clint id
	 * @param fbTokenId the fb token id
	 * @return the client customer email verify by customer id
	 * @throws Exception the exception
	 */
	public static ClientCustomerEmailVerifyDVO getClientCustomerEmailVerifyByCustomerId(String clintId,
			String fbTokenId) throws Exception {

		ResultSet resultSet = null;
		ClientCustomerEmailVerifyDVO dvo = null;
		try {
			String sql = "SELECT * from  pu_client_customer_fb_veri where clintid=? and tokenidfb=?  ";
			resultSet = CassandraConnector.getSession().execute(sql, clintId, fbTokenId);

			if (resultSet != null) {
				for (Row row : resultSet) {
					dvo = new ClientCustomerEmailVerifyDVO();
					dvo.setClId(row.getString("clintid"));
					dvo.setCuId(row.getString("custid"));
					dvo.setEmail(row.getString("custemail"));
					dvo.setPhone(row.getString("custphone"));
					dvo.setFbTokenId(row.getString("tokenidfb"));
					dvo.setFbUid(row.getString("fbuserid"));
				}
			}
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		} finally {

		}
		return dvo;
	}

	/**
	 * Gets the client customer By customer id and fb userid.
	 *
	 * @param clintId  the clint id
	 * @param fbUserId the fb user id
	 * @return the client customer email verify by customer idand fb userid
	 * @throws Exception the exception
	 */
	public static ClientCustomerEmailVerifyDVO getClientCustomerEmailVerifyByCustomerIdandFbUserid(String clintId,
			String fbUserId) throws Exception {

		ResultSet resultSet = null;
		ClientCustomerEmailVerifyDVO dvo = null;
		try {
			String sql = "SELECT * from  pu_client_customer_fb_veri where clintid=? and fbuserid=?  ";
			resultSet = CassandraConnector.getSession().execute(sql, clintId, fbUserId);

			if (resultSet != null) {
				for (Row row : resultSet) {
					dvo = new ClientCustomerEmailVerifyDVO();
					dvo.setClId(row.getString("clintid"));
					dvo.setCuId(row.getString("custid"));
					dvo.setEmail(row.getString("custemail"));
					dvo.setPhone(row.getString("custphone"));
					dvo.setFbTokenId(row.getString("tokenidfb"));
					dvo.setFbUid(row.getString("fbuserid"));
				}
			}
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		} finally {

		}
		return dvo;
	}

	/**
	 * Save client customer 
	 *
	 * @param obj the obj
	 * @return the boolean
	 * @throws Exception the exception
	 */
	public static Boolean saveClientCustomerEmailVerify(ClientCustomerEmailVerifyDVO obj) throws Exception {
		try {
			CassandraConnector.getSession().execute(
					"INSERT INTO pu_client_customer_fb_veri (clintid,custid,custemail,custphone,tokenidfb,fbuserid,credate)  VALUES (?,?,?, ?,?,?,toTimestamp(now()))",
					obj.getClId(), obj.getCuId(), obj.getEmail(), obj.getPhone(), obj.getFbTokenId(), obj.getFbUid());

			return true;
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		}
	}

}
