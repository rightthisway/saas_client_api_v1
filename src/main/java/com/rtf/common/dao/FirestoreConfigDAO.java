/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.common.dao;

import java.util.ArrayList;
import java.util.List;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.exceptions.DriverException;
import com.rtf.common.dvo.ConfigDVO;
import com.rtf.saas.db.CassandraConnector;
import com.rtf.saas.exception.RTFDataAccessException;
import com.rtf.saas.util.GenUtil;

/**
 * The Class FirestoreConfigDAO.
 */
public class FirestoreConfigDAO {

	/**
	 * Gets All clients configuration details.
	 *
	 * @return the all client config
	 * @throws Exception the exception
	 */
	public static List<ConfigDVO> getAllClientConfig() throws Exception {
		ResultSet resultSet = null;
		List<ConfigDVO> lst = new ArrayList<ConfigDVO>();
		try {
			String sql = "SELECT * from  pa_comn_client_firestore_config ";
			resultSet = CassandraConnector.getSession().execute(sql);
			if (resultSet != null) {
				for (Row row : resultSet) {
					ConfigDVO dvo = new ConfigDVO();
					dvo.setId(row.getInt("fsconfid"));
					dvo.setClId(row.getString("clintid"));
					dvo.setFbcfg(row.getString("firestore_config"));
					dvo.setSaasweburl(row.getString("saasweburl"));
					dvo.setCustCount(row.getInt("custcnt"));
					dvo.setFsChatConf(row.getString("fs_chat_config"));
					lst.add(dvo);
				}
			}
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		} finally {

		}
		return lst;
	}

	/**
	 * Gets the Selected clients configuration details
	 *
	 * @param clintId the clint id
	 * @return the client config
	 * @throws Exception the exception
	 */
	public static List<ConfigDVO> getClientConfig(String clintId) throws Exception {
		ResultSet resultSet = null;
		ConfigDVO dvo = null;
		List<ConfigDVO> list = new ArrayList<ConfigDVO>();
		try {
			String sql = "SELECT * from  pa_comn_client_firestore_config where clintid=? ";
			resultSet = CassandraConnector.getSession().execute(sql, clintId);
			if (resultSet != null) {
				dvo = new ConfigDVO();
				for (Row row : resultSet) {
					dvo.setId(row.getInt("fsconfid"));
					dvo.setClId(row.getString("clintid"));
					dvo.setFbcfg(row.getString("firestore_config"));
					dvo.setSaasweburl(row.getString("saasweburl"));
					dvo.setCustCount(row.getInt("custcnt"));
					dvo.setFsChatConf(row.getString("fs_chat_config"));
					list.add(dvo);
				}
			}
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		} finally {

		}
		return list;
	}

	/**
	 * Gets the clients configuration details based on firestore Config ID
	 *
	 * @param clintId  the clint id
	 * @param fsConfId the fs conf id
	 * @return the client configfor customer
	 * @throws Exception the exception
	 */
	public static ConfigDVO getClientConfigforCustomer(String clintId, Integer fsConfId) throws Exception {
		ResultSet resultSet = null;
		ConfigDVO dvo = null;
		try {
			String sql = "SELECT * from  pa_comn_client_firestore_config where clintid=? and fsconfid=? ";
			resultSet = CassandraConnector.getSession().execute(sql, clintId, fsConfId);
			if (resultSet != null) {
				dvo = new ConfigDVO();
				for (Row row : resultSet) {
					dvo.setId(row.getInt("fsconfid"));
					dvo.setClId(row.getString("clintid"));
					dvo.setFbcfg(row.getString("firestore_config"));
					dvo.setSaasweburl(row.getString("saasweburl"));
					dvo.setCustCount(row.getInt("custcnt"));
					dvo.setFsChatConf(row.getString("fs_chat_config"));
				}
			}
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		} finally {

		}
		return dvo;
	}

	/**
	 * Update customer cumulative count assigned for a firestore (To manage cap  on max supported by Fire store) .
	 *
	 * @param config the config
	 * @return true, if successful
	 * @throws Exception the exception
	 */
	public static boolean updateCustCount(ConfigDVO config) throws Exception {
		boolean isUpdated = false;
		StringBuffer sql = new StringBuffer();
		sql.append("update pa_comn_client_firestore_config set ");
		sql.append("custcnt=? ");
		sql.append("WHERE clintid=? AND fsconfid=? AND saasweburl=?");
		try {
			CassandraConnector.getSession().execute(sql.toString(), new Object[] { config.getCustCount(),
					config.getClId(), config.getId(), config.getSaasweburl() == null ? "" : config.getSaasweburl() });
			isUpdated = true;
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		} finally {

		}
		return isUpdated;
	}
}
