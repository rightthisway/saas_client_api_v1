/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.common.dao;

import java.util.ArrayList;
import java.util.List;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.exceptions.DriverException;
import com.rtf.common.dvo.SchoolsListDVO;
import com.rtf.saas.db.CassandraConnector;
import com.rtf.saas.exception.RTFDataAccessException;
import com.rtf.saas.util.GenUtil;

/*
 * CREATE TABLE schooltriviaks.liv_client_school_list (
    school_name text PRIMARY KEY,
    school_description text,
    school_status text
)
*/

/**
 * The Class SchoolsListDAO.
 */
public class SchoolsListDAO {

	/**
	 * Delete School Details from Records
	 *
	 * @param obj the obj
	 * @return the boolean
	 * @throws Exception the exception
	 */
	public static Boolean deleteClientCustomer(SchoolsListDVO obj) throws Exception {
		try {
			CassandraConnector.getSession().execute("delete from liv_client_school_list where school_name=?",
					obj.getName());
			return true;
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		}
	}

	/**
	 * Gets  all active schools list.
	 *
	 * @return the all active schools list
	 * @throws Exception the exception
	 */
	public static List<SchoolsListDVO> getAllActiveSchoolsList() throws Exception {

		ResultSet resultSet = null;
		SchoolsListDVO dvo = null;
		List<SchoolsListDVO> list = new ArrayList<SchoolsListDVO>();
		try {
			String sql = "SELECT * from  liv_client_school_list where school_status='ACTIVE' ";
			resultSet = CassandraConnector.getSession().execute(sql);

			if (resultSet != null) {
				for (Row row : resultSet) {
					dvo = new SchoolsListDVO();
					dvo.setName(row.getString("school_name"));
					dvo.setDesc(row.getString("school_description"));
					list.add(dvo);
				}
			}
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		} finally {

		}
		return list;
	}

	/**
	 * Save client customer for a School as Client.
	 *
	 * @param obj the obj
	 * @return the boolean
	 * @throws Exception the exception
	 */
	public static Boolean saveClientCustomer(SchoolsListDVO obj) throws Exception {
		try {
			CassandraConnector.getSession()
					.execute("INSERT INTO liv_client_school_list (school_name,school_description,school_status)  "
							+ "VALUES (?,?,'ACTIVE')", obj.getName(), obj.getDesc());
			return true;
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		}
	}

}
