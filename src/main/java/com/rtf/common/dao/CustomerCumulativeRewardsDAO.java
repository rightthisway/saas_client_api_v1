/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.common.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.datastax.driver.core.BatchStatement;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.exceptions.DriverException;
import com.rtf.common.dvo.CustomerCumulativeRewardsDVO;
import com.rtf.saas.db.CassandraConnector;
import com.rtf.saas.exception.RTFDataAccessException;
import com.rtf.saas.util.GenUtil;

/**
 * The Class CustomerCumulativeRewardsDAO.
 */
public class CustomerCumulativeRewardsDAO {

	/**
	 * Batch update live rewards for  customer.
	 *
	 * @param custRwdsList the cust rwds list
	 * @return the boolean
	 */
	public static Boolean batchUpdateLiveRewards(List<CustomerCumulativeRewardsDVO> custRwdsList) {
		Boolean isUpdated = Boolean.FALSE;
		Session session = CassandraConnector.getSession();
		BatchStatement batchStatement = new BatchStatement();
		PreparedStatement preparedStatement = session.prepare(
				"update pt_customer_cumlative_rwds set livrwdval=?,upddate=toTimestamp(now()) where clintid=? and custid=? and rwdtype=?");

		for (CustomerCumulativeRewardsDVO obj : custRwdsList) {
			batchStatement.add(preparedStatement.bind(obj.getRwdVal(), obj.getClId(), obj.getCuId(), obj.getRwdType()));
		}
		try {
			session.execute(batchStatement);
			isUpdated = Boolean.TRUE;
		} catch (Exception ex) {
			ex.printStackTrace();
			isUpdated = Boolean.FALSE;
		}
		return isUpdated;
	}

	/**
	 * Gets the customer cumulative rewards.
	 *
	 * @param clintId the clint id
	 * @return the cust cumulative rewards
	 * @throws Exception the exception
	 */
	public static Map<String, CustomerCumulativeRewardsDVO> getCustCumulativeRewards(String clintId) throws Exception {

		ResultSet resultSet = null;
		CustomerCumulativeRewardsDVO dvo = null;
		Map<String, CustomerCumulativeRewardsDVO> map = new HashMap<String, CustomerCumulativeRewardsDVO>();
		try {
			String sql = "SELECT custid,rwdtype,rwdval,livrwdval from  pt_customer_cumlative_rwds where clintid=?   ";
			resultSet = CassandraConnector.getSession().execute(sql, clintId);

			if (resultSet != null) {
				for (Row row : resultSet) {
					dvo = new CustomerCumulativeRewardsDVO(clintId, row.getString("custid"), row.getString("rwdtype"),
							row.getDouble("rwdval"), row.getDouble("livrwdval"));
					map.put(dvo.getCuId() + "_" + dvo.getRwdType(), dvo);

				}
			}
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		} finally {

		}
		return map;
	}

	/**
	 * Gets the customer cumulative rewards by customer id and reward type.
	 *
	 * @param clintId the clint id
	 * @param custId  the cust id
	 * @param rwdType the rwd type
	 * @return the cust cumulative rewards by cust id and rwd type
	 * @throws Exception the exception
	 */
	public static CustomerCumulativeRewardsDVO getCustCumulativeRewardsByCustIdAndRwdType(String clintId, String custId,
			String rwdType) throws Exception {

		ResultSet resultSet = null;
		CustomerCumulativeRewardsDVO dvo = null;
		try {
			String sql = "SELECT * from  pt_customer_cumlative_rwds where clintid=? and custid=? and rwdtype=?  ";
			resultSet = CassandraConnector.getSession().execute(sql, clintId, custId, rwdType);

			if (resultSet != null) {
				for (Row row : resultSet) {
					dvo = new CustomerCumulativeRewardsDVO();
					dvo.setClId(row.getString("clintid"));
					dvo.setCuId(row.getString("custid"));
					dvo.setRwdType(row.getString("rwdtype"));
					dvo.setRwdVal(row.getDouble("rwdval"));
					dvo.setMigRwdVal(row.getDouble("migrwdval"));
				}
			}
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		} finally {

		}
		return dvo;
	}

	/**
	 * Save customer cumulative rewards.
	 *
	 * @param obj the obj
	 * @return the boolean
	 * @throws Exception the exception
	 */
	public static Boolean saveCustomerCumulativeRewards(CustomerCumulativeRewardsDVO obj) throws Exception {
		try {
			CassandraConnector.getSession().execute(
					"INSERT INTO pt_customer_cumlative_rwds (clintid,custid,rwdtype,rwdval,migrwdval,credate,upddate)  "
							+ "VALUES (?,?,?, ?,?,toTimestamp(now()),toTimestamp(now()))",
					obj.getClId(), obj.getCuId(), obj.getRwdType(), obj.getRwdVal(), obj.getMigRwdVal());

			return true;
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		}
	}

	/**
	 * Update customer cumulative rewards.
	 *
	 * @param obj the obj
	 * @return the boolean
	 * @throws Exception the exception
	 */
	public static Boolean UpdateCustomerCumulativeRewards(CustomerCumulativeRewardsDVO obj) throws Exception {
		try {
			CassandraConnector.getSession().execute(
					"update pt_customer_cumlative_rwds set rwdval=?,upddate=toTimestamp(now()) where clintid=? and custid=? and rwdtype=?",
					obj.getRwdVal(), obj.getClId(), obj.getCuId(), obj.getRwdType());
			return true;
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		}
	}
}
