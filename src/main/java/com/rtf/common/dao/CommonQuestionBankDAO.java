/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.common.dao;

import java.util.HashMap;
import java.util.Map;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.exceptions.DriverException;
import com.rtf.common.dvo.CommonQuestionBankDVO;
import com.rtf.common.util.Constants;
import com.rtf.saas.db.CassandraConnector;
import com.rtf.saas.exception.RTFDataAccessException;
import com.rtf.saas.util.GenUtil;

/**
 * The Class CommonQuestionBankDAO.
 */
public class CommonQuestionBankDAO {
	/**
	 * Gets the all questions based on master bank id.
	 *
	 * @param clintId               the clint id
	 * @param mstqsnbnkid           the mstqsnbnkid
	 * @param catType               the cat type
	 * @param subCatType            the sub cat type
	 * @param feedBackOrRegularType the feed back or regular type
	 * @return the all questions based on master bank id
	 * @throws Exception the exception
	 */
	public static Map<Integer, CommonQuestionBankDVO> getAllQuestionsBasedOnMasterBankId(String clintId,
			Integer mstqsnbnkid, String catType, String subCatType, String feedBackOrRegularType) throws Exception {

		ResultSet resultSet = null;
		Map<Integer, CommonQuestionBankDVO> qmap = null;
		try {
			String sql = "SELECT * from  pa_ofltx_question_bank_only_active  "
					+ " WHERE clintid=? AND anstype=?  and cattype = ?  ";
			if (!GenUtil.isEmptyString(subCatType)) {
				sql = sql + " and subcattype = ?  ";
				resultSet = CassandraConnector.getSession().execute(sql, clintId, feedBackOrRegularType, catType,
						subCatType);
			} else {
				resultSet = CassandraConnector.getSession().execute(sql, clintId, feedBackOrRegularType, catType);
			}
			qmap = new HashMap<Integer, CommonQuestionBankDVO>();
			if (resultSet != null) {
				for (Row row : resultSet) {
					String catTy = null;
					String subCatTy = null;
					String ansType = null;
					CommonQuestionBankDVO dvo = new CommonQuestionBankDVO();
					Integer qbId = row.getInt("qsnbnkid");
					dvo.setQbid(qbId);
					dvo.setClId(row.getString("clintid"));
					dvo.setIsact(true);
					dvo.setQtx(row.getString("qsntext"));
					dvo.setOpa(row.getString("ansopta"));
					dvo.setOpb(row.getString("ansoptb"));
					dvo.setOpc(row.getString("ansoptc"));
					dvo.setOpd(row.getString("ansoptd"));
					dvo.setCans(row.getString("corans"));
					catTy = row.getString("cattype");
					dvo.setCtyp(row.getString("cattype"));
					subCatTy = row.getString("subcattype");
					dvo.setSctyp(subCatTy);
					ansType = row.getString("anstype");
					dvo.setAnstype(ansType);
					dvo.setqOrientn(row.getString("qsnorient"));
					dvo.setQsnmode(row.getString("qsnmode"));
					if (catType.equals(catTy) && feedBackOrRegularType.equals(ansType)) {
						if (subCatType.equals(subCatTy) || Constants.ALL_SUBCATEGORY.equals(subCatType)) {
							qmap.put(qbId, dvo);
						}
					}

				}
			}
		} catch (final DriverException de) {
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		} finally {

		}
		return qmap;
	}

}
