/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.common.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.datastax.driver.core.exceptions.DriverException;
import com.rtf.common.dvo.RTFCustomerDVO;
import com.rtf.common.util.DatabaseConnections;
import com.rtf.saas.db.RTFCassandraConnector;

/**
 * The Class RTFDataMigrationDAO.
 */
public class RTFDataMigrationDAO {

	/**
	 * Updates the RTF  point master details
	 *
	 * @param cust the cust
	 * @return the point master upda query
	 */
	public static String updateCustomerRTFPoints(RTFCustomerDVO cust) {
		String query = "";
		try {
			query = "UPDATE customer set rtf_points=  " + cust.getRtfPoints() + " WHERE id=" + cust.getId();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return query;
	}

	/**
	 * Insert RTF point for Tracking.
	 *
	 * @param cust  the cust
	 * @param point the point
	 * @param coId  the co id
	 * @param crBy  the cr by
	 * @return the point tracking insert query
	 */
	public static String insertRTFPointsForTracking(RTFCustomerDVO cust, Integer point, String coId, String crBy) {
		String query = "";
		try {
			query = "INSERT INTO cust_loyalty_reward_info_tracking(customer_id,reward_points,created_by,created_Date,reward_type,contest_id,rtf_points) "
					+ "VALUES (" + cust.getId() + ",0.00,'" + crBy + "',getDate(),'" + coId + "',-1," + point + ")";
		} catch (Exception e) {
			e.printStackTrace();
		}
		return query;
	}

	/**
	 * Fetch The RTF Points for customer id.
	 *
	 * @param cuId the cu id
	 * @return the RTF cust id
	 */
	public static RTFCustomerDVO getRTFCustId(String cuId) {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		RTFCustomerDVO cust = null;
		String sql = "SELECT id as id,email as email,rtf_points as rtfpoints from customer where email = ?";
		try {
			conn = DatabaseConnections.getRtfDBConnection();
			ps = conn.prepareStatement(sql);
			ps.setString(1, cuId);
			rs = ps.executeQuery();
			while (rs.next()) {
				cust = new RTFCustomerDVO();
				cust.setId(rs.getInt("id"));
				cust.setEmail(rs.getString("email"));
				cust.setRtfPoints(rs.getInt("rtfpoints"));
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return cust;
	}

	/**
	 * Update data to cass.
	 */
	public static void updateDataToCass() {
		try {

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Update RTF Points data for Given Customer Id.
	 *
	 * @param customers the customers
	 * @return true, if successful
	 */
	public static boolean updateDataToCass(List<RTFCustomerDVO> customers) {
		boolean isUpdated = false;
		String sql = "update customer set rtf_points=? WHERE customer_id=?";

		try {
			for (RTFCustomerDVO cust : customers) {
				RTFCassandraConnector.getSession().execute(sql, new Object[] { cust.getRtfPoints(), cust.getId() });
			}
			isUpdated = true;
		} catch (final DriverException de) {
			de.printStackTrace();
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {			
		}
		return isUpdated;
	}

	/**
	 * Update SaaS  rewards to RTF.(For Only RTF Clients )
	 *
	 * @param sql the sql
	 * @return the integer
	 */
	public static Integer updateSaaSRewardsToRTF(String sql) {
		Connection conn = null;
		PreparedStatement ps = null;
		Integer isUpdated = null;
		try {
			conn = DatabaseConnections.getRtfDBConnection();
			ps = conn.prepareStatement(sql);
			isUpdated = ps.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return isUpdated;
	}

}
