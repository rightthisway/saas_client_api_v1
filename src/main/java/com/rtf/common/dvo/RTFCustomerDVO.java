/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.common.dvo;

/**
 * The Class RTFCustomerDVO.
 */
public class RTFCustomerDVO {

	/** The id. */
	private Integer id;

	/** The rtf points. */
	private Integer rtfPoints;

	/** The email. */
	private String email;

	/**
	 * Gets the email.
	 *
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * Gets the rtf points.
	 *
	 * @return the rtf points
	 */
	public Integer getRtfPoints() {
		return rtfPoints;
	}

	/**
	 * Sets the email.
	 *
	 * @param email the new email
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * Sets the rtf points.
	 *
	 * @param rtfPoints the new rtf points
	 */
	public void setRtfPoints(Integer rtfPoints) {
		this.rtfPoints = rtfPoints;
	}

}
