/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.common.dvo;

import java.io.Serializable;

/**
 * The Class RewardTypeDVO.
 */
/*
 *
 * CREATE TABLE saasprodclientadminwcid.sd_reward_type ( clintid text, rwdtype
 * text, creby text, credate timestamp, isactive boolean, rwddesc text,
 * rwdimgurl text, rwdunimesr text, updby text, upddate timestamp, PRIMARY KEY
 * (clintid, rwdtype) ) WITH CLUSTERING ORDER BY (rwdtype ASC)
 */
public class RewardTypeDVO implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -1248333205181575068L;

	/** The client id. */
	private String clId;

	/** The rwd type. */
	private String rwdType;

	/** The rwddesc. */
	private String rwddesc;

	/** The rwdimgurl. */
	private String rwdimgurl;

	/** The rwdunimesr. */
	private String rwdunimesr;

	/** The isactive. */
	private Boolean isactive;

	// private Long crBy;
	// private Long crDate;
	// private String updby;
	// private Date upddate;

	/**
	 * Gets the cl id.
	 *
	 * @return the cl id
	 */
	public String getClId() {
		return clId;
	}

	/**
	 * Gets the isactive.
	 *
	 * @return the isactive
	 */
	public Boolean getIsactive() {
		return isactive;
	}

	/**
	 * Gets the rwddesc.
	 *
	 * @return the rwddesc
	 */
	public String getRwddesc() {
		return rwddesc;
	}

	/**
	 * Gets the rwdimgurl.
	 *
	 * @return the rwdimgurl
	 */
	public String getRwdimgurl() {
		return rwdimgurl;
	}

	/**
	 * Gets the rwd type.
	 *
	 * @return the rwd type
	 */
	public String getRwdType() {
		return rwdType;
	}

	/**
	 * Gets the rwdunimesr.
	 *
	 * @return the rwdunimesr
	 */
	public String getRwdunimesr() {
		return rwdunimesr;
	}

	/**
	 * Sets the cl id.
	 *
	 * @param clId the new cl id
	 */
	public void setClId(String clId) {
		this.clId = clId;
	}

	/**
	 * Sets the isactive.
	 *
	 * @param isactive the new isactive
	 */
	public void setIsactive(Boolean isactive) {
		this.isactive = isactive;
	}

	/**
	 * Sets the rwddesc.
	 *
	 * @param rwddesc the new rwddesc
	 */
	public void setRwddesc(String rwddesc) {
		this.rwddesc = rwddesc;
	}

	/**
	 * Sets the rwdimgurl.
	 *
	 * @param rwdimgurl the new rwdimgurl
	 */
	public void setRwdimgurl(String rwdimgurl) {
		this.rwdimgurl = rwdimgurl;
	}

	/**
	 * Sets the rwd type.
	 *
	 * @param rwdType the new rwd type
	 */
	public void setRwdType(String rwdType) {
		this.rwdType = rwdType;
	}

	/**
	 * Sets the rwdunimesr.
	 *
	 * @param rwdunimesr the new rwdunimesr
	 */
	public void setRwdunimesr(String rwdunimesr) {
		this.rwdunimesr = rwdunimesr;
	}

}
