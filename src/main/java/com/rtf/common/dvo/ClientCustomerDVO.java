/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.common.dvo;

import java.io.Serializable;
import java.util.Date;

/**
 * The Class ClientCustomerDVO.
 */
/*
 * pu_client_customer ( clintid text, custid text, custemail text, custfname
 * text, custlname text, custphone text, custpicurl text, uplby text, upldate
 * timestamp, PRIMARY KEY (clintid, custid) ) WITH CLUSTERING ORDER BY (custid
 * ASC)
 */
public class ClientCustomerDVO implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 7592842876923806779L;

	/** The client id. */
	private String clId;

	/** The cu id. */
	private String cuId;

	/**The Contest ID.*/
	private String coId;
	
	/** The email. */
	private String email;

	/** The fname. */
	private String fname;

	/** The lname. */
	private String lname;

	/** The phone. */
	private String phone;

	/** The purl. */
	private String purl;

	/** The uplby. */
	private String uplby;

	/** The upldate. */
	private Date upldate;

	/** The password. */
	private String password;

	/** The sign up type. */
	private String signUpType;

	/** The token id. */
	private String tokenId;

	/** The school name. */
	private String schoolName;

	/** The user id. */
	private String userId;

	/** The fb token id. */
	private String fbTokenId;

	/** The fb uid. */
	private String fbUid;

	/** The fs conf id. */
	private Integer fsConfId;

	/** The ce. */
	private String ce;

	/**
	 * Gets the ce.
	 *
	 * @return the ce
	 */
	public String getCe() {
		return ce;
	}

	/**
	 * Gets the cl id.
	 *
	 * @return the cl id
	 */
	public String getClId() {
		return clId;
	}

	/**
	 * Gets the cu id.
	 *
	 * @return the cu id
	 */
	public String getCuId() {
		return cuId;
	}

	/**
	 * Gets the email.
	 *
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Gets the fb token id.
	 *
	 * @return the fb token id
	 */
	public String getFbTokenId() {
		return fbTokenId;
	}

	/**
	 * Gets the fb uid.
	 *
	 * @return the fb uid
	 */
	public String getFbUid() {
		return fbUid;
	}

	/**
	 * Gets the fname.
	 *
	 * @return the fname
	 */
	public String getFname() {
		return fname;
	}

	/**
	 * Gets the fs conf id.
	 *
	 * @return the fs conf id
	 */
	public Integer getFsConfId() {
		return fsConfId;
	}

	/**
	 * Gets the lname.
	 *
	 * @return the lname
	 */
	public String getLname() {
		return lname;
	}

	/**
	 * Gets the password.
	 *
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Gets the phone.
	 *
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * Gets the purl.
	 *
	 * @return the purl
	 */
	public String getPurl() {
		return purl;
	}

	/**
	 * Gets the school name.
	 *
	 * @return the school name
	 */
	public String getSchoolName() {
		return schoolName;
	}

	/**
	 * Gets the sign up type.
	 *
	 * @return the sign up type
	 */
	public String getSignUpType() {
		return signUpType;
	}

	/**
	 * Gets the token id.
	 *
	 * @return the token id
	 */
	public String getTokenId() {
		return tokenId;
	}

	/**
	 * Gets the uplby.
	 *
	 * @return the uplby
	 */
	public String getUplby() {
		return uplby;
	}

	/**
	 * Gets the upldate.
	 *
	 * @return the upldate
	 */
	public Date getUpldate() {
		return upldate;
	}

	/**
	 * Gets the user id.
	 *
	 * @return the user id
	 */
	public String getUserId() {
		if (userId != null && !userId.isEmpty()) {
			return userId;
		}
		// return cuId;
		return userId;
	}

	/**
	 * Sets the ce.
	 *
	 * @param ce the new ce
	 */
	public void setCe(String ce) {
		this.ce = ce;
	}

	/**
	 * Sets the cl id.
	 *
	 * @param clId the new cl id
	 */
	public void setClId(String clId) {
		this.clId = clId;
	}

	/**
	 * Sets the cu id.
	 *
	 * @param cuId the new cu id
	 */
	public void setCuId(String cuId) {
		this.cuId = cuId;
	}

	/**
	 * Sets the email.
	 *
	 * @param email the new email
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * Sets the fb token id.
	 *
	 * @param fbTokenId the new fb token id
	 */
	public void setFbTokenId(String fbTokenId) {
		this.fbTokenId = fbTokenId;
	}

	/**
	 * Sets the fb uid.
	 *
	 * @param fbUid the new fb uid
	 */
	public void setFbUid(String fbUid) {
		this.fbUid = fbUid;
	}

	/**
	 * Sets the fname.
	 *
	 * @param fname the new fname
	 */
	public void setFname(String fname) {
		this.fname = fname;
	}

	/**
	 * Sets the fs conf id.
	 *
	 * @param fsConfId the new fs conf id
	 */
	public void setFsConfId(Integer fsConfId) {
		this.fsConfId = fsConfId;
	}

	/**
	 * Sets the lname.
	 *
	 * @param lname the new lname
	 */
	public void setLname(String lname) {
		this.lname = lname;
	}

	/**
	 * Sets the password.
	 *
	 * @param password the new password
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * Sets the phone.
	 *
	 * @param phone the new phone
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * Sets the purl.
	 *
	 * @param purl the new purl
	 */
	public void setPurl(String purl) {
		this.purl = purl;
	}

	/**
	 * Sets the school name.
	 *
	 * @param schoolName the new school name
	 */
	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}

	/**
	 * Sets the sign up type.
	 *
	 * @param signUpType the new sign up type
	 */
	public void setSignUpType(String signUpType) {
		this.signUpType = signUpType;
	}

	/**
	 * Sets the token id.
	 *
	 * @param tokenId the new token id
	 */
	public void setTokenId(String tokenId) {
		this.tokenId = tokenId;
	}

	/**
	 * Sets the uplby.
	 *
	 * @param uplby the new uplby
	 */
	public void setUplby(String uplby) {
		this.uplby = uplby;
	}

	/**
	 * Sets the upldate.
	 *
	 * @param upldate the new upldate
	 */
	public void setUpldate(Date upldate) {
		this.upldate = upldate;
	}

	/**
	 * Sets the user id.
	 *
	 * @param userId the new user id
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * Sets the coId.
	 *
	 * @param coId the new coId
	 */
	public String getCoId() {
		return coId;
	}

	/**
	 * Sets the coId.
	 *
	 * @param coId the new coId
	 */
	public void setCoId(String coId) {
		this.coId = coId;
	}

	@Override
	public String toString() {
		return "ClientCustomerDVO [clId=" + clId + ", cuId=" + cuId + ", coId=" + coId + ", userId=" + userId
				+ ", fsConfId=" + fsConfId + ", ce=" + ce + "]";
	}

}
