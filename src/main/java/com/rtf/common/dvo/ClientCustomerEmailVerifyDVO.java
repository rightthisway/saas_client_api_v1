/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.common.dvo;

import java.io.Serializable;
import java.util.Date;

/**
 * The Class ClientCustomerEmailVerifyDVO.
 */
/*
 * pu_client_customer ( clintid text, custid text, custemail text, custfname
 * text, custlname text, custphone text, custpicurl text, uplby text, upldate
 * timestamp, PRIMARY KEY (clintid, custid) ) WITH CLUSTERING ORDER BY (custid
 * ASC)
 */
public class ClientCustomerEmailVerifyDVO implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -1231197070964023065L;

	/** The client id. */
	private String clId;

	/** The cu id. */
	private String cuId;

	/** The email. */
	private String email;

	/** The phone. */
	private String phone;

	/** The upldate. */
	private Date upldate;

	/** The sign up type. */
	private String signUpType;

	/** The fb token id. */
	private String fbTokenId;

	/** The fb uid. */
	private String fbUid;

	/**
	 * Gets the cl id.
	 *
	 * @return the cl id
	 */
	public String getClId() {
		return clId;
	}

	/**
	 * Gets the cu id.
	 *
	 * @return the cu id
	 */
	public String getCuId() {
		return cuId;
	}

	/**
	 * Gets the email.
	 *
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Gets the fb token id.
	 *
	 * @return the fb token id
	 */
	public String getFbTokenId() {
		return fbTokenId;
	}

	/**
	 * Gets the fb uid.
	 *
	 * @return the fb uid
	 */
	public String getFbUid() {
		return fbUid;
	}

	/**
	 * Gets the phone.
	 *
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * Gets the sign up type.
	 *
	 * @return the sign up type
	 */
	public String getSignUpType() {
		return signUpType;
	}

	/**
	 * Gets the upldate.
	 *
	 * @return the upldate
	 */
	public Date getUpldate() {
		return upldate;
	}

	/**
	 * Sets the cl id.
	 *
	 * @param clId the new cl id
	 */
	public void setClId(String clId) {
		this.clId = clId;
	}

	/**
	 * Sets the cu id.
	 *
	 * @param cuId the new cu id
	 */
	public void setCuId(String cuId) {
		this.cuId = cuId;
	}

	/**
	 * Sets the email.
	 *
	 * @param email the new email
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * Sets the fb token id.
	 *
	 * @param fbTokenId the new fb token id
	 */
	public void setFbTokenId(String fbTokenId) {
		this.fbTokenId = fbTokenId;
	}

	/**
	 * Sets the fb uid.
	 *
	 * @param fbUid the new fb uid
	 */
	public void setFbUid(String fbUid) {
		this.fbUid = fbUid;
	}

	/**
	 * Sets the phone.
	 *
	 * @param phone the new phone
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * Sets the sign up type.
	 *
	 * @param signUpType the new sign up type
	 */
	public void setSignUpType(String signUpType) {
		this.signUpType = signUpType;
	}

	/**
	 * Sets the upldate.
	 *
	 * @param upldate the new upldate
	 */
	public void setUpldate(Date upldate) {
		this.upldate = upldate;
	}

}
