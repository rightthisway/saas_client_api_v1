/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.common.dvo;

/**
 * The Class CommonQuestionBankDVO.
 */

public class CommonQuestionBankDVO {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Gets the serialversionuid.
	 *
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/** The client id. */
	private String clId;

	/** The mstqsnbnkid. */
	private String mstqsnbnkid;

	/** The qbid. */
	private Integer qbid;

	/** The opa. */
	private String opa;

	/** The opb. */
	private String opb;

	/** The opc. */
	private String opc;

	/** The opd. */
	private String opd;

	/** The anstype. */
	// FEEDBACK OR REGULAR
	private String anstype;

	/** The ctyp. */
	private String ctyp;

	/** The cans. */
	private String cans;

	/** The creby. */
	private String creby;

	/** The credate. */
	private long credate;

	/** The isact. */
	private Boolean isact;

	/** The qsnmode. */
	private String qsnmode;

	/** The q orientn. */
	// DISPLAY ANSWER OPTIONS HORIZONTAL OR VERTICAL
	private String qOrientn;

	/** The qtx. */
	private String qtx;

	/** The sctyp. */
	private String sctyp;

	/** The updby. */
	private String updby;

	/** The upddate. */
	private long upddate;

	/**
	 * Gets the anstype.
	 *
	 * @return the anstype
	 */
	public String getAnstype() {
		return anstype;
	}

	/**
	 * Gets the cans.
	 *
	 * @return the cans
	 */
	public String getCans() {
		return cans;
	}

	/**
	 * Gets the cl id.
	 *
	 * @return the cl id
	 */
	public String getClId() {
		return clId;
	}

	/**
	 * Gets the creby.
	 *
	 * @return the creby
	 */
	public String getCreby() {
		return creby;
	}

	/**
	 * Gets the credate.
	 *
	 * @return the credate
	 */
	public long getCredate() {
		return credate;
	}

	/**
	 * Gets the ctyp.
	 *
	 * @return the ctyp
	 */
	public String getCtyp() {
		return ctyp;
	}

	/**
	 * Gets the isact.
	 *
	 * @return the isact
	 */
	public Boolean getIsact() {
		return isact;
	}

	/**
	 * Gets the mstqsnbnkid.
	 *
	 * @return the mstqsnbnkid
	 */
	public String getMstqsnbnkid() {
		return mstqsnbnkid;
	}

	/**
	 * Gets the opa.
	 *
	 * @return the opa
	 */
	public String getOpa() {
		return opa;
	}

	/**
	 * Gets the opb.
	 *
	 * @return the opb
	 */
	public String getOpb() {
		return opb;
	}

	/**
	 * Gets the opc.
	 *
	 * @return the opc
	 */
	public String getOpc() {
		return opc;
	}

	/**
	 * Gets the opd.
	 *
	 * @return the opd
	 */
	public String getOpd() {
		return opd;
	}

	/**
	 * Gets the qbid.
	 *
	 * @return the qbid
	 */
	public Integer getQbid() {
		return qbid;
	}

	/**
	 * Gets the q orientn.
	 *
	 * @return the q orientn
	 */
	public String getqOrientn() {
		return qOrientn;
	}

	/**
	 * Gets the qsnmode.
	 *
	 * @return the qsnmode
	 */
	public String getQsnmode() {
		return qsnmode;
	}

	/**
	 * Gets the qtx.
	 *
	 * @return the qtx
	 */
	public String getQtx() {
		return qtx;
	}

	/**
	 * Gets the sctyp.
	 *
	 * @return the sctyp
	 */
	public String getSctyp() {
		return sctyp;
	}

	/**
	 * Gets the updby.
	 *
	 * @return the updby
	 */
	public String getUpdby() {
		return updby;
	}

	/**
	 * Gets the upddate.
	 *
	 * @return the upddate
	 */
	public long getUpddate() {
		return upddate;
	}

	/**
	 * Sets the anstype.
	 *
	 * @param anstype the new anstype
	 */
	public void setAnstype(String anstype) {
		this.anstype = anstype;
	}

	/**
	 * Sets the cans.
	 *
	 * @param cans the new cans
	 */
	public void setCans(String cans) {
		this.cans = cans;
	}

	/**
	 * Sets the cl id.
	 *
	 * @param clId the new cl id
	 */
	public void setClId(String clId) {
		this.clId = clId;
	}

	/**
	 * Sets the creby.
	 *
	 * @param creby the new creby
	 */
	public void setCreby(String creby) {
		this.creby = creby;
	}

	/**
	 * Sets the credate.
	 *
	 * @param credate the new credate
	 */
	public void setCredate(long credate) {
		this.credate = credate;
	}

	/**
	 * Sets the ctyp.
	 *
	 * @param ctyp the new ctyp
	 */
	public void setCtyp(String ctyp) {
		this.ctyp = ctyp;
	}

	/**
	 * Sets the isact.
	 *
	 * @param isact the new isact
	 */
	public void setIsact(Boolean isact) {
		this.isact = isact;
	}

	/**
	 * Sets the mstqsnbnkid.
	 *
	 * @param mstqsnbnkid the new mstqsnbnkid
	 */
	public void setMstqsnbnkid(String mstqsnbnkid) {
		this.mstqsnbnkid = mstqsnbnkid;
	}

	/**
	 * Sets the opa.
	 *
	 * @param opa the new opa
	 */
	public void setOpa(String opa) {
		this.opa = opa;
	}

	/**
	 * Sets the opb.
	 *
	 * @param opb the new opb
	 */
	public void setOpb(String opb) {
		this.opb = opb;
	}

	/**
	 * Sets the opc.
	 *
	 * @param opc the new opc
	 */
	public void setOpc(String opc) {
		this.opc = opc;
	}

	/**
	 * Sets the opd.
	 *
	 * @param opd the new opd
	 */
	public void setOpd(String opd) {
		this.opd = opd;
	}

	/**
	 * Sets the qbid.
	 *
	 * @param qbid the new qbid
	 */
	public void setQbid(Integer qbid) {
		this.qbid = qbid;
	}

	/**
	 * Sets the q orientn.
	 *
	 * @param qOrientn the new q orientn
	 */
	public void setqOrientn(String qOrientn) {
		this.qOrientn = qOrientn;
	}

	/**
	 * Sets the qsnmode.
	 *
	 * @param qsnmode the new qsnmode
	 */
	public void setQsnmode(String qsnmode) {
		this.qsnmode = qsnmode;
	}

	/**
	 * Sets the qtx.
	 *
	 * @param qtx the new qtx
	 */
	public void setQtx(String qtx) {
		this.qtx = qtx;
	}

	/**
	 * Sets the sctyp.
	 *
	 * @param sctyp the new sctyp
	 */
	public void setSctyp(String sctyp) {
		this.sctyp = sctyp;
	}

	/**
	 * Sets the updby.
	 *
	 * @param updby the new updby
	 */
	public void setUpdby(String updby) {
		this.updby = updby;
	}

	/**
	 * Sets the upddate.
	 *
	 * @param upddate the new upddate
	 */
	public void setUpddate(long upddate) {
		this.upddate = upddate;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "CommonQuestionBankDVO [clId=" + clId + ", mstqsnbnkid=" + mstqsnbnkid + ", qbid=" + qbid + ", opa="
				+ opa + ", opb=" + opb + ", opc=" + opc + ", opd=" + opd + ", anstype=" + anstype + ", ctyp=" + ctyp
				+ ", cans=" + cans + ", creby=" + creby + ", credate=" + credate + ", isact=" + isact + ", qsnmode="
				+ qsnmode + ", qOrientn=" + qOrientn + ", qtx=" + qtx + ", sctyp=" + sctyp + ", updby=" + updby
				+ ", upddate=" + upddate + "]";
	}

}
