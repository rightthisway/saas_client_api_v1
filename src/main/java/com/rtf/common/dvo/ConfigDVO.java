/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.common.dvo;

/**
 * The Class ConfigDVO.
 */
public class ConfigDVO {

	/** The id. */
	private Integer id;

	/** The client id. */
	private String clId;

	/** The fbcfg. */
	private String fbcfg;

	/** The fs chat conf. */
	private String fsChatConf;

	/** The saasweburl. */
	private String saasweburl;

	/** The cust count. */
	private Integer custCount;

	/**
	 * Gets the cl id.
	 *
	 * @return the cl id
	 */
	public String getClId() {
		return clId;
	}

	/**
	 * Gets the cust count.
	 *
	 * @return the cust count
	 */
	public Integer getCustCount() {
		return custCount;
	}

	/**
	 * Gets the fbcfg.
	 *
	 * @return the fbcfg
	 */
	public String getFbcfg() {
		return fbcfg;
	}

	/**
	 * Gets the fs chat conf.
	 *
	 * @return the fs chat conf
	 */
	public String getFsChatConf() {
		return fsChatConf;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * Gets the saasweburl.
	 *
	 * @return the saasweburl
	 */
	public String getSaasweburl() {
		return saasweburl;
	}

	/**
	 * Sets the cl id.
	 *
	 * @param clId the new cl id
	 */
	public void setClId(String clId) {
		this.clId = clId;
	}

	/**
	 * Sets the cust count.
	 *
	 * @param custCount the new cust count
	 */
	public void setCustCount(Integer custCount) {
		this.custCount = custCount;
	}

	/**
	 * Sets the fbcfg.
	 *
	 * @param fbcfg the new fbcfg
	 */
	public void setFbcfg(String fbcfg) {
		this.fbcfg = fbcfg;
	}

	/**
	 * Sets the fs chat conf.
	 *
	 * @param fsChatConf the new fs chat conf
	 */
	public void setFsChatConf(String fsChatConf) {
		this.fsChatConf = fsChatConf;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * Sets the saasweburl.
	 *
	 * @param saasweburl the new saasweburl
	 */
	public void setSaasweburl(String saasweburl) {
		this.saasweburl = saasweburl;
	}

}
