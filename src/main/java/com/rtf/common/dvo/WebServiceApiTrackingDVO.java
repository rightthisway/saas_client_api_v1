/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.common.dvo;

import java.sql.Timestamp;
import java.util.Date;

/**
 * The Class WebServiceApiTrackingDVO.
 */
public class WebServiceApiTrackingDVO {

	/** The client id. */

	private String clintid;

	/** The customer id. */
	private String custid;

	/** The apiname. */
	private String apiname;

	/** The hitdate. */
	private Timestamp hitdate;

	/** The actresult. */
	private String actresult;

	/** The custipaddr. */
	private String custipaddr;

	/** The conid. */
	private String conid;

	/** The platform. */
	private String platform;

	/** The prodcode. */
	private String prodcode;

	/** The resstatus. */
	private Integer resstatus;

	/** The nodeid. */
	private Integer nodeid;

	/** The description. */
	private String description;

	/** The sessionid. */
	private String sessionid;

	/** The apiendtime. */
	private Timestamp apiendtime;

	/** The apistarttime. */
	private Timestamp apistarttime;

	/** The appver. */
	private String appver;

	/** The deviceapitime. */
	private Date deviceapitime;

	/** The deviceinfo. */
	private String deviceinfo;

	/** The fbcallbacktime. */
	private Date fbcallbacktime;

	/**
	 * Gets the actresult.
	 *
	 * @return the actresult
	 */
	public String getActresult() {
		return actresult;
	}

	/**
	 * Gets the apiendtime.
	 *
	 * @return the apiendtime
	 */
	public Timestamp getApiendtime() {
		return apiendtime;
	}

	/**
	 * Gets the apiname.
	 *
	 * @return the apiname
	 */
	public String getApiname() {
		return apiname;
	}

	/**
	 * Gets the apistarttime.
	 *
	 * @return the apistarttime
	 */
	public Timestamp getApistarttime() {
		return apistarttime;
	}

	/**
	 * Gets the appver.
	 *
	 * @return the appver
	 */
	public String getAppver() {
		return appver;
	}

	/**
	 * Gets the clintid.
	 *
	 * @return the clintid
	 */
	public String getClintid() {
		return clintid;
	}

	/**
	 * Gets the conid.
	 *
	 * @return the conid
	 */
	public String getConid() {
		return conid;
	}

	/**
	 * Gets the custid.
	 *
	 * @return the custid
	 */
	public String getCustid() {
		return custid;
	}

	/**
	 * Gets the custipaddr.
	 *
	 * @return the custipaddr
	 */
	public String getCustipaddr() {
		return custipaddr;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Gets the deviceapitime.
	 *
	 * @return the deviceapitime
	 */
	public Date getDeviceapitime() {
		return deviceapitime;
	}

	/**
	 * Gets the deviceinfo.
	 *
	 * @return the deviceinfo
	 */
	public String getDeviceinfo() {
		return deviceinfo;
	}

	/**
	 * Gets the fbcallbacktime.
	 *
	 * @return the fbcallbacktime
	 */
	public Date getFbcallbacktime() {
		return fbcallbacktime;
	}

	/**
	 * Gets the hitdate.
	 *
	 * @return the hitdate
	 */
	public Timestamp getHitdate() {
		return hitdate;
	}

	/**
	 * Gets the nodeid.
	 *
	 * @return the nodeid
	 */
	public Integer getNodeid() {
		return nodeid;
	}

	/**
	 * Gets the platform.
	 *
	 * @return the platform
	 */
	public String getPlatform() {
		return platform;
	}

	/**
	 * Gets the prodcode.
	 *
	 * @return the prodcode
	 */
	public String getProdcode() {
		return prodcode;
	}

	/**
	 * Gets the resstatus.
	 *
	 * @return the resstatus
	 */
	public Integer getResstatus() {
		return resstatus;
	}

	/**
	 * Gets the sessionid.
	 *
	 * @return the sessionid
	 */
	public String getSessionid() {
		return sessionid;
	}

	/**
	 * Sets the actresult.
	 *
	 * @param actresult the new actresult
	 */
	public void setActresult(String actresult) {
		this.actresult = actresult;
	}

	/**
	 * Sets the apiendtime.
	 *
	 * @param apiendtime the new apiendtime
	 */
	public void setApiendtime(Timestamp apiendtime) {
		this.apiendtime = apiendtime;
	}

	/**
	 * Sets the apiname.
	 *
	 * @param apiname the new apiname
	 */
	public void setApiname(String apiname) {
		this.apiname = apiname;
	}

	/**
	 * Sets the apistarttime.
	 *
	 * @param apistarttime the new apistarttime
	 */
	public void setApistarttime(Timestamp apistarttime) {
		this.apistarttime = apistarttime;
	}

	/**
	 * Sets the appver.
	 *
	 * @param appver the new appver
	 */
	public void setAppver(String appver) {
		this.appver = appver;
	}

	/**
	 * Sets the clintid.
	 *
	 * @param clintid the new clintid
	 */
	public void setClintid(String clintid) {
		this.clintid = clintid;
	}

	/**
	 * Sets the conid.
	 *
	 * @param conid the new conid
	 */
	public void setConid(String conid) {
		this.conid = conid;
	}

	/**
	 * Sets the custid.
	 *
	 * @param custid the new custid
	 */
	public void setCustid(String custid) {
		this.custid = custid;
	}

	/**
	 * Sets the custipaddr.
	 *
	 * @param custipaddr the new custipaddr
	 */
	public void setCustipaddr(String custipaddr) {
		this.custipaddr = custipaddr;
	}

	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Sets the deviceapitime.
	 *
	 * @param deviceapitime the new deviceapitime
	 */
	public void setDeviceapitime(Date deviceapitime) {
		this.deviceapitime = deviceapitime;
	}

	/**
	 * Sets the deviceinfo.
	 *
	 * @param deviceinfo the new deviceinfo
	 */
	public void setDeviceinfo(String deviceinfo) {
		this.deviceinfo = deviceinfo;
	}

	/**
	 * Sets the fbcallbacktime.
	 *
	 * @param fbcallbacktime the new fbcallbacktime
	 */
	public void setFbcallbacktime(Date fbcallbacktime) {
		this.fbcallbacktime = fbcallbacktime;
	}

	/**
	 * Sets the hitdate.
	 *
	 * @param hitdate the new hitdate
	 */
	public void setHitdate(Timestamp hitdate) {
		this.hitdate = hitdate;
	}

	/**
	 * Sets the nodeid.
	 *
	 * @param nodeid the new nodeid
	 */
	public void setNodeid(Integer nodeid) {
		this.nodeid = nodeid;
	}

	/**
	 * Sets the platform.
	 *
	 * @param platform the new platform
	 */
	public void setPlatform(String platform) {
		this.platform = platform;
	}

	/**
	 * Sets the prodcode.
	 *
	 * @param prodcode the new prodcode
	 */
	public void setProdcode(String prodcode) {
		this.prodcode = prodcode;
	}

	/**
	 * Sets the resstatus.
	 *
	 * @param resstatus the new resstatus
	 */
	public void setResstatus(Integer resstatus) {
		this.resstatus = resstatus;
	}

	/**
	 * Sets the sessionid.
	 *
	 * @param sessionid the new sessionid
	 */
	public void setSessionid(String sessionid) {
		this.sessionid = sessionid;
	}

}
