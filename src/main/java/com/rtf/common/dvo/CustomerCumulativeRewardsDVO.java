/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.common.dvo;

import java.io.Serializable;

/**
 * The Class CustomerCumulativeRewardsDVO.
 */
/*
 *
 * CREATE TABLE saasprodclientadminwcid.pt_customer_cumlative_rwds ( clintid
 * text, custid text, rwdtype text, credate timestamp, migdate timestamp,
 * migrwdval double, rwdval double, upddate timestamp, PRIMARY KEY (clintid,
 * custid, rwdtype)
 */
public class CustomerCumulativeRewardsDVO implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -9156983403900578123L;

	/** The client id. */
	private String clId;

	/** The cu id. */
	private String cuId;

	/** The rwd type. */
	private String rwdType;

	/** The rwd val. */
	private Double rwdVal;

	/** The live rwd val. */
	private Double liveRwdVal;

	/** The cr date. */
	private Long crDate;

	/** The up date. */
	private Long upDate;

	/** The mig rwd val. */
	private Double migRwdVal;

	/** The mig date. */
	private Long migDate;

	/** The prev rwd val. */
	private Double prevRwdVal;

	/** The prev liv rwd val. */
	private Double prevLivRwdVal;

	/** The is edited. */
	private Boolean isEdited;

	/**
	 * Instantiates a new customer cumulative rewards DVO.
	 */
	public CustomerCumulativeRewardsDVO() {
	}

	/**
	 * Instantiates a new customer cumulative rewards DVO.
	 *
	 * @param clId       the client Id
	 * @param cuId       the cu id
	 * @param rwdType    the rwd type
	 * @param rwdVal     the rwd val
	 * @param liverwdVal the liverwd val
	 */
	public CustomerCumulativeRewardsDVO(String clId, String cuId, String rwdType, Double rwdVal, Double liverwdVal) {
		this.clId = clId;
		this.cuId = cuId;
		this.rwdType = rwdType;
		this.rwdVal = rwdVal;
		this.liveRwdVal = liverwdVal;
		this.prevRwdVal = rwdVal;
		this.prevLivRwdVal = liverwdVal;
	}

	/**
	 * Gets the cl id.
	 *
	 * @return the cl id
	 */
	public String getClId() {
		return clId;
	}

	/**
	 * Gets the cr date.
	 *
	 * @return the cr date
	 */
	public Long getCrDate() {
		return crDate;
	}

	/**
	 * Gets the cu id.
	 *
	 * @return the cu id
	 */
	public String getCuId() {
		return cuId;
	}

	/**
	 * Gets the checks if is edited.
	 *
	 * @return the checks if is edited
	 */
	public Boolean getIsEdited() {
		if (isEdited == null) {
			isEdited = false;
		}
		return isEdited;
	}

	/**
	 * Gets the live rwd val.
	 *
	 * @return the live rwd val
	 */
	public Double getLiveRwdVal() {
		if (liveRwdVal == null) {
			liveRwdVal = 0.0;
		}
		return liveRwdVal;
	}

	/**
	 * Gets the mig date.
	 *
	 * @return the mig date
	 */
	public Long getMigDate() {
		return migDate;
	}

	/**
	 * Gets the mig rwd val.
	 *
	 * @return the mig rwd val
	 */
	public Double getMigRwdVal() {
		return migRwdVal;
	}

	/**
	 * Gets the prev liv rwd val.
	 *
	 * @return the prev liv rwd val
	 */
	public Double getPrevLivRwdVal() {
		if (prevLivRwdVal == null) {
			prevLivRwdVal = 0.0;
		}
		return prevLivRwdVal;
	}

	/**
	 * Gets the prev rwd val.
	 *
	 * @return the prev rwd val
	 */
	public Double getPrevRwdVal() {
		if (prevRwdVal == null) {
			prevRwdVal = 0.0;
		}
		return prevRwdVal;
	}

	/**
	 * Gets the rwd type.
	 *
	 * @return the rwd type
	 */
	public String getRwdType() {
		return rwdType;
	}

	/**
	 * Gets the rwd val.
	 *
	 * @return the rwd val
	 */
	public Double getRwdVal() {
		if (rwdVal == null) {
			rwdVal = 0.0;
		}
		return rwdVal;
	}

	/**
	 * Gets the up date.
	 *
	 * @return the up date
	 */
	public Long getUpDate() {
		return upDate;
	}

	/**
	 * Sets the cl id.
	 *
	 * @param clId the new cl id
	 */
	public void setClId(String clId) {
		this.clId = clId;
	}

	/**
	 * Sets the cr date.
	 *
	 * @param crDate the new cr date
	 */
	public void setCrDate(Long crDate) {
		this.crDate = crDate;
	}

	/**
	 * Sets the cu id.
	 *
	 * @param cuId the new cu id
	 */
	public void setCuId(String cuId) {
		this.cuId = cuId;
	}

	/**
	 * Sets the checks if is edited.
	 *
	 * @param isEdited the new checks if is edited
	 */
	public void setIsEdited(Boolean isEdited) {
		this.isEdited = isEdited;
	}

	/**
	 * Sets the live rwd val.
	 *
	 * @param liveRwdVal the new live rwd val
	 */
	public void setLiveRwdVal(Double liveRwdVal) {
		this.liveRwdVal = liveRwdVal;
	}

	/**
	 * Sets the mig date.
	 *
	 * @param migDate the new mig date
	 */
	public void setMigDate(Long migDate) {
		this.migDate = migDate;
	}

	/**
	 * Sets the mig rwd val.
	 *
	 * @param migRwdVal the new mig rwd val
	 */
	public void setMigRwdVal(Double migRwdVal) {
		this.migRwdVal = migRwdVal;
	}

	/**
	 * Sets the prev liv rwd val.
	 *
	 * @param prevLivRwdVal the new prev liv rwd val
	 */
	public void setPrevLivRwdVal(Double prevLivRwdVal) {
		this.prevLivRwdVal = prevLivRwdVal;
	}

	/**
	 * Sets the prev rwd val.
	 *
	 * @param prevRwdVal the new prev rwd val
	 */
	public void setPrevRwdVal(Double prevRwdVal) {
		this.prevRwdVal = prevRwdVal;
	}

	/**
	 * Sets the rwd type.
	 *
	 * @param rwdType the new rwd type
	 */
	public void setRwdType(String rwdType) {
		this.rwdType = rwdType;
	}

	/**
	 * Sets the rwd val.
	 *
	 * @param rwdVal the new rwd val
	 */
	public void setRwdVal(Double rwdVal) {
		this.rwdVal = rwdVal;
	}

	/**
	 * Sets the up date.
	 *
	 * @param upDate the new up date
	 */
	public void setUpDate(Long upDate) {
		this.upDate = upDate;
	}

}
