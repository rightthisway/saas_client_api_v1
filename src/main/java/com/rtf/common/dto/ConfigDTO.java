/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.common.dto;

import com.rtf.common.dvo.ConfigDVO;
import com.rtf.saas.arch.RtfSaasBaseDTO;

/**
 * The Class ConfigDTO.
 */
public class ConfigDTO extends RtfSaasBaseDTO {

	/** The dvo. */
	private ConfigDVO dvo;

	/** The client id. */
	private String clId;

	/** The user id. */
	private String userId;

	/** The saasweburl. */
	private String saasweburl;

	/**
	 * Gets the cl id.
	 *
	 * @return the cl id
	 */
	@Override
	public String getClId() {
		return clId;
	}

	/**
	 * Gets the dvo.
	 *
	 * @return the dvo
	 */
	public ConfigDVO getDvo() {
		return dvo;
	}

	/**
	 * Gets the saasweburl.
	 *
	 * @return the saasweburl
	 */
	public String getSaasweburl() {
		return saasweburl;
	}

	/**
	 * Gets the user id.
	 *
	 * @return the user id
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * Sets the cl id.
	 *
	 * @param clId the new cl id
	 */
	@Override
	public void setClId(String clId) {
		this.clId = clId;
	}

	/**
	 * Sets the dvo.
	 *
	 * @param dvo the new dvo
	 */
	public void setDvo(ConfigDVO dvo) {
		this.dvo = dvo;
	}

	/**
	 * Sets the saasweburl.
	 *
	 * @param saasweburl the new saasweburl
	 */
	public void setSaasweburl(String saasweburl) {
		this.saasweburl = saasweburl;
	}

	/**
	 * Sets the user id.
	 *
	 * @param userId the new user id
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

}
