/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.common.dto;

import java.util.List;

import com.rtf.livt.dvo.ContestDVO;
import com.rtf.saas.arch.RtfSaasBaseDTO;

/**
 * The Class ContestCardDTO.
 */
public class ContestCardDTO extends RtfSaasBaseDTO {

	/** The contests. */
	private List<ContestDVO> contests;

	/** The ott contests. */
	private List<com.rtf.ott.dvo.ContestDVO> ottContests;

	/** The snw contest. */
	private com.rtf.snw.dvo.ContestDVO snwContest;

	/** The play btn text. */
	private String playBtnText;

	/** The is liv cont schdld. */
	private Boolean isLivContSchdld;

	/** The is ott cont schdld. */
	private Boolean isOttContSchdld;

	/** The is snw schdld. */
	private Boolean isSnwSchdld;

	/** The no liv cont msg. */
	private String noLivContMsg;

	/** The no ott cont msg. */
	private String noOttContMsg;

	/** The no snw cont msg. */
	private String noSnwContMsg;

	/**
	 * Gets the contests.
	 *
	 * @return the contests
	 */
	public List<ContestDVO> getContests() {
		return contests;
	}

	/**
	 * Gets the checks if is liv cont schdld.
	 *
	 * @return the checks if is liv cont schdld
	 */
	public Boolean getIsLivContSchdld() {
		return isLivContSchdld;
	}

	/**
	 * Gets the checks if is ott cont schdld.
	 *
	 * @return the checks if is ott cont schdld
	 */
	public Boolean getIsOttContSchdld() {
		return isOttContSchdld;
	}

	/**
	 * Gets the checks if is snw schdld.
	 *
	 * @return the checks if is snw schdld
	 */
	public Boolean getIsSnwSchdld() {
		return isSnwSchdld;
	}

	/**
	 * Gets the no liv cont msg.
	 *
	 * @return the no liv cont msg
	 */
	public String getNoLivContMsg() {
		return noLivContMsg;
	}

	/**
	 * Gets the no ott cont msg.
	 *
	 * @return the no ott cont msg
	 */
	public String getNoOttContMsg() {
		return noOttContMsg;
	}

	/**
	 * Gets the no snw cont msg.
	 *
	 * @return the no snw cont msg
	 */
	public String getNoSnwContMsg() {
		return noSnwContMsg;
	}

	/**
	 * Gets the ott contests.
	 *
	 * @return the ott contests
	 */
	public List<com.rtf.ott.dvo.ContestDVO> getOttContests() {
		return ottContests;
	}

	/**
	 * Gets the play btn text.
	 *
	 * @return the play btn text
	 */
	public String getPlayBtnText() {
		return playBtnText;
	}

	/**
	 * Gets the snw contest.
	 *
	 * @return the snw contest
	 */
	public com.rtf.snw.dvo.ContestDVO getSnwContest() {
		return snwContest;
	}

	/**
	 * Sets the contests.
	 *
	 * @param contests the new contests
	 */
	public void setContests(List<ContestDVO> contests) {
		this.contests = contests;
	}

	/**
	 * Sets the checks if is liv cont schdld.
	 *
	 * @param isLivContSchdld the new checks if is liv cont schdld
	 */
	public void setIsLivContSchdld(Boolean isLivContSchdld) {
		this.isLivContSchdld = isLivContSchdld;
	}

	/**
	 * Sets the checks if is ott cont schdld.
	 *
	 * @param isOttContSchdld the new checks if is ott cont schdld
	 */
	public void setIsOttContSchdld(Boolean isOttContSchdld) {
		this.isOttContSchdld = isOttContSchdld;
	}

	/**
	 * Sets the checks if is snw schdld.
	 *
	 * @param isSnwSchdld the new checks if is snw schdld
	 */
	public void setIsSnwSchdld(Boolean isSnwSchdld) {
		this.isSnwSchdld = isSnwSchdld;
	}

	/**
	 * Sets the no liv cont msg.
	 *
	 * @param noLivContMsg the new no liv cont msg
	 */
	public void setNoLivContMsg(String noLivContMsg) {
		this.noLivContMsg = noLivContMsg;
	}

	/**
	 * Sets the no ott cont msg.
	 *
	 * @param noOttContMsg the new no ott cont msg
	 */
	public void setNoOttContMsg(String noOttContMsg) {
		this.noOttContMsg = noOttContMsg;
	}

	/**
	 * Sets the no snw cont msg.
	 *
	 * @param noSnwContMsg the new no snw cont msg
	 */
	public void setNoSnwContMsg(String noSnwContMsg) {
		this.noSnwContMsg = noSnwContMsg;
	}

	/**
	 * Sets the ott contests.
	 *
	 * @param ottContests the new ott contests
	 */
	public void setOttContests(List<com.rtf.ott.dvo.ContestDVO> ottContests) {
		this.ottContests = ottContests;
	}

	/**
	 * Sets the play btn text.
	 *
	 * @param playBtnText the new play btn text
	 */
	public void setPlayBtnText(String playBtnText) {
		this.playBtnText = playBtnText;
	}

	/**
	 * Sets the snw contest.
	 *
	 * @param snwContest the new snw contest
	 */
	public void setSnwContest(com.rtf.snw.dvo.ContestDVO snwContest) {
		this.snwContest = snwContest;
	}

}
