/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.common.service;

import java.util.Date;

import com.rtf.common.dao.CustomerCumulativeRewardsDAO;
import com.rtf.common.dvo.CustomerCumulativeRewardsDVO;

/**
 * The Class CustomerRewardService.
 */
public class CustomerRewardService {

	/**
	 * Credit customer rewards.Cummulative Rewards are managed by this method
	 *
	 * @param clientId the client id
	 * @param custId   the cust id
	 * @param rwdType  the rwd type
	 * @param rwdVal   the rwd val
	 * @return the boolean
	 * @throws Exception the exception
	 */
	public static Boolean creditCustomerRewards(String clientId, String custId, String rwdType, Double rwdVal)
			throws Exception {

		CustomerCumulativeRewardsDVO cumRwds = CustomerCumulativeRewardsDAO
				.getCustCumulativeRewardsByCustIdAndRwdType(clientId, custId, rwdType);
		if (cumRwds != null) {
			Double tempRwdVal = cumRwds.getRwdVal() + rwdVal;
			cumRwds.setRwdVal(tempRwdVal);
			cumRwds.setUpDate(new Date().getTime());
			CustomerCumulativeRewardsDAO.UpdateCustomerCumulativeRewards(cumRwds);
		} else {
			cumRwds = new CustomerCumulativeRewardsDVO();
			cumRwds.setClId(clientId);
			cumRwds.setCuId(custId);
			cumRwds.setRwdType(rwdType);
			cumRwds.setRwdVal(rwdVal);
			cumRwds.setMigRwdVal(0.00);
			cumRwds.setCrDate(new Date().getTime());
			cumRwds.setUpDate(new Date().getTime());
			CustomerCumulativeRewardsDAO.saveCustomerCumulativeRewards(cumRwds);
		}
		return true;
	}

}
