/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.common.service;

import com.rtf.common.dao.ClientConfigDAO;
import com.rtf.common.dvo.ClientConfigDVO;

/**
 * The Class ClientConfigService.
 */
public class ClientConfigService {

	/**
	 * Gets the contest configuration by Product  key for a given Client.
	 *
	 * @param clientId the client id
	 * @param prodId   the prod id
	 * @param key      the key
	 * @return the contest config by key
	 */
	public static ClientConfigDVO getContestConfigByKey(String clientId, String prodId, String key) {
		try {
			return ClientConfigDAO.getContestConfigByKey(clientId, prodId, key);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
