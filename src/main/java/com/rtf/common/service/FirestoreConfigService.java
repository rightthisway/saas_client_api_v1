/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.common.service;

import java.util.ArrayList;
import java.util.List;

import com.rtf.common.dao.FirestoreConfigDAO;
import com.rtf.common.dvo.ConfigDVO;
import com.rtf.saas.util.GenUtil;

/**
 * The Class FirestoreConfigService.
 */
public class FirestoreConfigService {

	/**
	 * Gets the client FireStore config.
	 *
	 * @param clId       the client Id
	 * @param saasweburl the saasweburl
	 * @return the client config
	 * @throws Exception the exception
	 */
	public static List<ConfigDVO> getClientConfig(String clId, String saasweburl) throws Exception {
		List<ConfigDVO> dvos = null;
		try {
			if (GenUtil.isEmptyString(clId)) {
				List<ConfigDVO> lst = FirestoreConfigDAO.getAllClientConfig();
				if (lst == null || lst.isEmpty()) {
					return null;
				}
				dvos = matchSaasUrlForConfig(lst, saasweburl);
			} else {
				dvos = FirestoreConfigDAO.getClientConfig(clId);
			}
			if (dvos == null || dvos.isEmpty()) {
				return null;
			}
		} catch (Exception e) {
			throw e;
		}

		return dvos;

	}

	/**
	 * Gets the client configfor customer.
	 *
* @param clId     the client ID
	 * @param fsConfId the fs conf id
	 * @return the client configfor customer
	 * @throws Exception the exception
	 */
	public static ConfigDVO getClientConfigforCustomer(String clId, Integer fsConfId) throws Exception {
		ConfigDVO dvo = null;
		try {
			dvo = FirestoreConfigDAO.getClientConfigforCustomer(clId, fsConfId);
		} catch (Exception e) {
			throw e;
		}

		return dvo;

	}

	/**
	 * Match saas url for config.
	 *
	 * @param lst        the lst
	 * @param saasweburl the saasweburl
	 * @return the list
	 */
	private static List<ConfigDVO> matchSaasUrlForConfig(List<ConfigDVO> lst, String saasweburl) {
		// ConfigDVO dvo = null;
		List<ConfigDVO> list = new ArrayList<ConfigDVO>();
		for (ConfigDVO dbdvo : lst) {
			String dbweburl = dbdvo.getSaasweburl();
			if (GenUtil.isEmptyString(dbweburl))
				continue;
			if (dbweburl.equalsIgnoreCase(saasweburl) || dbweburl.contains(saasweburl)
					|| saasweburl.contains(dbweburl)) {
				list.add(dbdvo);
				// break;
			}
		}
		return list;
	}

	/**
	 * Update cust count.
	 *
	 * @param config the config
	 * @return true, if successful
	 * @throws Exception the exception
	 */
	public static boolean updateCustCount(ConfigDVO config) throws Exception {
		boolean isUpdated = false;
		try {
			isUpdated = FirestoreConfigDAO.updateCustCount(config);
		} catch (Exception e) {
			isUpdated = false;
			e.printStackTrace();
		}
		return isUpdated;
	}

}