/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.common.service;

import java.sql.Timestamp;

import com.rtf.common.dao.WebServiceApiTrackingDAO;
import com.rtf.common.dvo.WebServiceApiTrackingDVO;

/**
 * The Class SaaSApiTrackingService.
 */
public class SaaSApiTrackingService {

	/**
	 * Insert api tracking details.
	 *
	 * @param clientId     the client id
	 * @param customerId   the customer id
	 * @param prodcode     the prodcode
	 * @param apiName      the api name
	 * @param conid        the conid
	 * @param actresult    the actresult
	 * @param custipaddr   the custipaddr
	 * @param platform     the platform
	 * @param sessionId    the session id
	 * @param description  the description
	 * @param apistarttime the apistarttime
	 * @param apiendtime   the apiendtime
	 * @param nodeId       the node id
	 * @param respStatus   the resp status
	 * @param deviceInfo   the device info
	 */
	public static void insertApiTrackingDetails(String clientId, String customerId, String prodcode, String apiName,
			String conid, String actresult, String custipaddr, String platform, String sessionId, String description,
			Timestamp apistarttime, Timestamp apiendtime, Integer nodeId, Integer respStatus, String deviceInfo) {
		String custIdDb, clientIdDb = null;
		try {
			WebServiceApiTrackingDVO dvo = new WebServiceApiTrackingDVO();
			clientIdDb = clientId != null ? clientId : "-1";
			dvo.setClintid(clientIdDb);
			custIdDb = customerId != null ? customerId : "-1";
			dvo.setCustid(custIdDb);
			dvo.setProdcode(prodcode);
			dvo.setApiname(apiName);
			dvo.setConid(conid);

			dvo.setActresult(actresult);
			dvo.setCustipaddr(custipaddr);
			dvo.setPlatform(platform);

			dvo.setSessionid(sessionId);
			dvo.setDescription(description);

			dvo.setApistarttime(apistarttime);
			dvo.setApiendtime(apiendtime);

			dvo.setNodeid(nodeId);
			dvo.setResstatus(respStatus);
			dvo.setDescription(deviceInfo);

			WebServiceApiTrackingDAO.insertWSApiTracking(dvo);
		} catch (Exception ex) {
			ex.printStackTrace();			
		}

	}

}
