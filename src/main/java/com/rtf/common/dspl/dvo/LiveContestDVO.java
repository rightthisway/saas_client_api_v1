/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.common.dspl.dvo;

import com.rtf.saas.util.DateFormatUtil;

/**
 * The Class LiveContestDVO.
 */
public class LiveContestDVO {

	/** The co id. */
	private String coId;

	/** The name. */
	private String name;

	/** The ext name. */
	private String extName;

	/** The st date. */
	private Long stDate;

	/** The st date time str. */
	private String stDateTimeStr;

	/**
	 * Gets the co id.
	 *
	 * @return the co id
	 */
	public String getCoId() {
		return coId;
	}

	/**
	 * Gets the ext name.
	 *
	 * @return the ext name
	 */
	public String getExtName() {
		return extName;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Gets the st date.
	 *
	 * @return the st date
	 */
	public Long getStDate() {
		return stDate;
	}

	/**
	 * Gets the st date time str.
	 *
	 * @return the st date time str
	 */
	public String getStDateTimeStr() {
		stDateTimeStr = DateFormatUtil.getMMDDYYYYHHMMString(stDate);
		return stDateTimeStr;
	}

	/**
	 * Sets the co id.
	 *
	 * @param coId the new co id
	 */
	public void setCoId(String coId) {
		this.coId = coId;
	}

	/**
	 * Sets the ext name.
	 *
	 * @param extName the new ext name
	 */
	public void setExtName(String extName) {
		this.extName = extName;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Sets the st date.
	 *
	 * @param stDate the new st date
	 */
	public void setStDate(Long stDate) {
		this.stDate = stDate;
	}

	/**
	 * Sets the st date time str.
	 *
	 * @param stDateTimeStr the new st date time str
	 */
	public void setStDateTimeStr(String stDateTimeStr) {
		this.stDateTimeStr = stDateTimeStr;
	}

}
