/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.common.sql.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.List;

import com.rtf.common.dao.ClientCustomerDAO;
import com.rtf.common.dvo.ClientCustomerDVO;
import com.rtf.common.util.DatabaseConnections;

/**
 * The Class ClientCustomerSQLDAO.
 */
public class ClientCustomerSQLDAO {

	/**
	 * Gets the client custoemr by id with password.
	 *
	 * @param clientId   the client id
	 * @param customerId the customer id
	 * @return the client custoemr by id with password
	 * @throws Exception the exception
	 */
	public static ClientCustomerDVO getClientCustoemrByIdWithPassword(String clientId, String customerId)
			throws Exception {
		ClientCustomerDVO dvo = null;
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sql = "SELECT * from pu_client_customer WHERE clintid = ? AND custid = ? ";
		try {
			conn = DatabaseConnections.getRtfSaasConnection();
			ps = conn.prepareStatement(sql);
			ps.setString(1, clientId);
			ps.setString(2, customerId);
			rs = ps.executeQuery();

			while (rs.next()) {
				dvo = new ClientCustomerDVO();
				dvo.setClId(rs.getString("clintid"));
				dvo.setCuId(rs.getString("custid"));
				dvo.setEmail(rs.getString("custemail"));
				dvo.setFname(rs.getString("custfname"));
				dvo.setLname(rs.getString("custlname"));
				dvo.setPhone(rs.getString("custphone"));
				dvo.setPurl(rs.getString("custpicurl"));
				dvo.setUplby(rs.getString("uplby"));
				dvo.setPassword(rs.getString("password"));
				dvo.setSignUpType(rs.getString("signuptype"));
				dvo.setSchoolName(rs.getString("schoolname"));
				dvo.setTokenId(rs.getString("tokenid"));
				dvo.setFbTokenId(rs.getString("tokenidfb"));
				dvo.setFbUid(rs.getString("fbuserid"));
				dvo.setUserId(rs.getString("userid"));
				dvo.setUpldate(rs.getTimestamp("upldate"));

			}
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return dvo;
	}

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 * @throws Exception the exception
	 */
	public static void main(String[] args) throws Exception {
		List<ClientCustomerDVO> list = ClientCustomerDAO.getallcustomers();
		for (ClientCustomerDVO cassObj : list) {
			ClientCustomerDVO sqlObj = ClientCustomerSQLDAO.getClientCustoemrByIdWithPassword(cassObj.getClId(),
					cassObj.getCuId());
			if (sqlObj == null) {
				ClientCustomerSQLDAO.saveClientCustomer(cassObj.getClId(), cassObj);
			} else {

				ClientCustomerSQLDAO.updateClientCustomer(cassObj.getClId(), cassObj);
			}
		}

	}

	/**
	 * Save client customer.
	 *
	 * @param clientId the client id
	 * @param obj      the obj
	 * @return the integer
	 */
	public static Integer saveClientCustomer(String clientId, ClientCustomerDVO obj) {
		Integer id = null;
		Connection conn = null;
		PreparedStatement statement = null;
		String sql = "insert into pu_client_customer(clintid,custid,custemail,custfname,custlname,custphone,custpicurl,uplby,upldate,"
				+ " password,signuptype,schoolname,tokenid,fbuserid,tokenidfb,userid)"
				+ "values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

		try {
			conn = DatabaseConnections.getRtfSaasConnection();
			statement = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			statement.setString(1, obj.getClId());
			statement.setString(2, obj.getCuId());
			statement.setString(3, obj.getEmail());
			statement.setString(4, obj.getFname());
			statement.setString(5, obj.getLname());
			statement.setString(6, obj.getPhone());
			statement.setString(7, obj.getPurl());
			statement.setString(8, obj.getUplby());
			if (obj.getUpldate() != null) {
				statement.setTimestamp(9, new Timestamp(obj.getUpldate().getTime()));
			} else {
				statement.setString(9, null);
			}
			statement.setString(10, obj.getPassword());
			statement.setString(11, obj.getSignUpType());
			statement.setString(12, obj.getSchoolName());
			statement.setString(13, obj.getTokenId());
			statement.setString(14, obj.getFbUid());
			statement.setString(15, obj.getFbTokenId());
			statement.setString(16, obj.getUserId());
			int affectedRows = statement.executeUpdate();
			if (affectedRows == 0) {
				throw new SQLException("Creating Client Customer record failed, no rows affected.");
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				statement.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return id;

	}

	/**
	 * Update client customer.
	 *
	 * @param clientId the client id
	 * @param obj      the obj
	 * @return the integer
	 */
	public static Integer updateClientCustomer(String clientId, ClientCustomerDVO obj) {
		Integer affectedRows = null;
		Connection conn = null;
		PreparedStatement statement = null;
		String sql = "update pu_client_customer set custemail=?,custfname=?,custlname=?,custphone=?,custpicurl=?,uplby=?,"// ,upldate=?
				+ " password=?,signuptype=?,schoolname=?,tokenid=?,fbuserid=?,tokenidfb=?,userid=?"
				+ " where clintid=? and custid=? ";

		try {
			conn = DatabaseConnections.getRtfSaasConnection();
			statement = conn.prepareStatement(sql);
			statement.setString(1, obj.getEmail());
			statement.setString(2, obj.getFname());
			statement.setString(3, obj.getLname());
			statement.setString(4, obj.getPhone());
			statement.setString(5, obj.getPurl());
			statement.setString(6, obj.getUplby());
			statement.setString(7, obj.getPassword());
			statement.setString(8, obj.getSignUpType());
			statement.setString(9, obj.getSchoolName());
			statement.setString(10, obj.getTokenId());
			statement.setString(11, obj.getFbUid());
			statement.setString(12, obj.getFbTokenId());
			statement.setString(13, obj.getUserId());
			statement.setString(14, obj.getClId());
			statement.setString(15, obj.getCuId());
			affectedRows = statement.executeUpdate();
			if (affectedRows == 0) {
				throw new SQLException("updating Client Customer record failed, no rows affected.");
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {

				statement.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return affectedRows;

	}

	/**
	 * Update client customer password.
	 *
	 * @param clientId the client id
	 * @param obj      the obj
	 * @return the integer
	 */
	public static Integer updateClientCustomerPassword(String clientId, ClientCustomerDVO obj) {
		Integer affectedRows = null;
		Connection conn = null;
		PreparedStatement statement = null;
		String sql = "update pu_client_customer password=? where clintid=? and custid=? ";

		try {
			conn = DatabaseConnections.getRtfSaasConnection();
			statement = conn.prepareStatement(sql);
			statement.setString(1, obj.getPassword());
			statement.setString(2, obj.getClId());
			statement.setString(3, obj.getCuId());
			affectedRows = statement.executeUpdate();
			if (affectedRows == 0) {
				throw new SQLException("updating Client Customer school name record failed, no rows affected.");
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				statement.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return affectedRows;

	}

	/**
	 * Update client customer school name.
	 *
	 * @param clientId the client id
	 * @param obj      the obj
	 * @return the integer
	 */
	public static Integer updateClientCustomerSchoolName(String clientId, ClientCustomerDVO obj) {
		Integer affectedRows = null;
		Connection conn = null;
		PreparedStatement statement = null;

		String sql = "update pu_client_customer schoolname=? where clintid=? and custid=? ";
		try {
			conn = DatabaseConnections.getRtfSaasConnection();
			statement = conn.prepareStatement(sql);
			statement.setString(1, obj.getSchoolName());
			statement.setString(2, obj.getClId());
			statement.setString(3, obj.getCuId());

			affectedRows = statement.executeUpdate();
			if (affectedRows == 0) {
				throw new SQLException("updating Client Customer school name record failed, no rows affected.");
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				statement.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return affectedRows;

	}

	/**
	 * Update client customer signup details.
	 *
	 * @param clientId the client id
	 * @param obj      the obj
	 * @return the integer
	 */
	public static Integer updateClientCustomerSignupDetails(String clientId, ClientCustomerDVO obj) {
		Integer affectedRows = null;
		Connection conn = null;
		PreparedStatement statement = null;
		String sql = "update pu_client_customer signuptype=?,schoolname=?,tokenid=?,fbuserid=?,tokenidfb=?"
				+ " where clintid=? and custid=? ";
		try {
			conn = DatabaseConnections.getRtfSaasConnection();
			statement = conn.prepareStatement(sql);
			statement.setString(1, obj.getSignUpType());
			statement.setString(2, obj.getSchoolName());
			statement.setString(3, obj.getTokenId());
			statement.setString(4, obj.getFbUid());
			statement.setString(5, obj.getFbTokenId());
			statement.setString(6, obj.getClId());
			statement.setString(7, obj.getCuId());

			affectedRows = statement.executeUpdate();
			if (affectedRows == 0) {
				throw new SQLException("updating Client Customer Signup Details record failed, no rows affected.");
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				statement.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return affectedRows;

	}
}
