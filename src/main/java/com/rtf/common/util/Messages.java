/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.common.util;

/**
 * The Class Messages.
 */
public class Messages {

	/** The Constant MANDATORY_PARAM_CUST_ID. */
	public static final String MANDATORY_PARAM_CUST_ID = "Customer ID is Mandatory";

	/** The Constant MANDATORY_PARAM_CONTEST_ID. */
	public static final String MANDATORY_PARAM_CONTEST_ID = "Contest ID is Mandatory";

	/** The Constant MANDATORY_PARAM_CLIENT_ID. */
	public static final String MANDATORY_PARAM_CLIENT_ID = "Client ID is Mandatory";

	/** The Constant MANDATORY_PARAM_PLAY_ID. */
	public static final String MANDATORY_PARAM_PLAY_ID = "Play ID is Mandatory";

	/** The Constant MANDATORY_PARAM_QUE_SL_NO. */
	public static final String MANDATORY_PARAM_QUE_SL_NO = "Que Sl.No is Mandatory";
	
	/** The Constant Reward Type. */
	public static final String MANDATORY_PARAM_REWARD_TYPE="Reward Type is mandatory.";
	public static final String MANDATORY_PARAM_STATUS_CODE = "Status Code is mandatory.";
	public static final String MANDATORY_PARAM_CONT_RUNNING_NUM="Invalid contest running nummber";

	/** The Constant NUMBER_RECD_AS_STRING. */
	public static final String NUMBER_RECD_AS_STRING = "Integer Expected ";

	/** The Constant MANDATORY_PARAM_QUE_ID. */
	public static final String MANDATORY_PARAM_QUE_ID = "Question ID is Mandatory";

	/** The Constant NO_CONTEST. */
	public static final String NO_CONTEST = "No Contest Found for Given Contest Id-";

	/** The Constant NO_PLAYID. */
	public static final String NO_PLAYID = "No Player ID Created for Given ID-";

	/** The Constant NO_CONTEST_FOR_CLIENT. */
	public static final String NO_CONTEST_FOR_CLIENT = "No Contest Configured for ClientID-";

	/** The Constant CUST_MIN_PLAY_GAP. */
	public static final String CUST_MIN_PLAY_GAP = "Min. interval to wait - ";

	/** The Constant NO_ANSWERS_FOUND. */
	public static final String NO_ANSWERS_FOUND = "No Contest Answers found for Criteria-";

	/** The Constant ALREADY_ANSWERED_EARLIER. */
	public static final String ALREADY_ANSWERED_EARLIER = "Answered earlier for Same Question- ";
	
	public static final String INVALID_CLIENT_ID = "Invalid client id.";
	public static final String INVALID_MAX_DATE="Invalid maxdate .";
	public static final String INVALID_MAX_MIGRATION_DATE_FMT="Invalid Max Migration date time format.";	
	public static final String INVALID_CPN_CDE="Invalid Coupon Code:";
	public static final String INVALID_DISCOUNT_VAL = "Invalid discount value:";
	public static final String INVALID_CUSTOMER_ID = "Invalid Customer Id";
	public static final String INVALID_ENC_KY= "Invalid enc key:";
	public static final String INVALID_QUESTION_ID="Invalid Question Id:";
	public static final String INVALID_QUESTION_NO="Invalid Question No:";
	public static final String INVALID_QUESTION_NO_OR_ID="Question No or Id is Invalid:";
	public static final String INVALID_CONTEST_ID="Contest Id is Invalid.";
	public static final String INVALID_CONTEST_OR_QID="Contest Id or Question NO is Invalid";
	public static final String INVALID_PREV_MIG_DATE = "Invalid previous Migration time format.";
	public static final String INVALID_PROD_ID = "Invalid Product Id.";
	public static final String INVALID_MERC_PROD_ID = "Invalid Merchant Product Id.";
	
	public static final String NXT_CONTEST_ANNOUNCED = "Next contest will be announced shortly.";
	public static final String NXT_CONTEST_TB_ANNOUNCED = "Next Game to be announced";
	public static final String  BTN_PLAY_TXT = "PLAY";
	public static final String  SAASURL_CLIENTID_NULL="BOTH SAASURL AND CLIENT ID IS NULL";
	
	public static final String BTN_PLAY_TEXT = "PLAY GAME";	
	public static final String TEXT_TRIVIA = "TEXT";
	public static final String SCRATCH_TRIVIA="SNW";
	public static final String FEEDBACK_TYPE="FEEDBACK";
	public static final String DISCOUNT_CODE = "DISCOUNT CODE";
	public static final String QUEAND_ANSTYPE="QUANS";
	public static final String FORCESTOP_CONTEST = "FORCESTOP";
	public static final String LIVT_STATUS_EXIT="EXIT";
	public static final String LIVT_STATUS_ACTIVE="ACTIVE";
	public static final String STATUS_TEXT_NO= "NO";
	public static final String STATUS_TEXT_YES= "YES";
	public static final String STATUS_COMPLETED = "COMPLETED";
	
	
	
	
	public static final String ERROR_FIRESTORE_CONFIG_ASSIGN =  "Error occured while assigning firestore config details.";
	public static final String ERROR_NO_CUST_CONFIG = "Customer configuration details not found in system.";
	public static final String INVALID_CONFIGTYPE="Refresh Data Type is Mandatory.";
	public static final String CLIENTFIRESTORECONFIG = "CLIENTFIRESTORECONFIG";
	public static final String CACHE_UPD_SUCC="Cache refreshed Successfully.";
	
	
	public static final String ERROR_REWARD_LISTING="Error occurred while fetching reward listing.";
	public static final String ERROR_GENERAL_MESSAGE="Error Ocurred Processing";
	
	public static final String PROCESS_SUCESS_CPN_CODE_STATUS_UPDATE="Coupon Code Migration Status updated ";
	public static final String PROCESS_ERR__CPN_CODE_STATUS_UPDATE = "Error occurred while updating migration status of Coupon Code Listing.";

	public static final String PROCESS_SUCESS_CPN_CODE_CREATION="Coupon Code Created";
	public static final String PROCESS_ERR_CPN_CODE_CREATION="Error occurred while Creating Coupon Code.";
	public static final String PROCESS_SUCESS_CPN_CODE_FETCH ="Coupon Code Fetched";
	public static final String PROCESS_ERR_CPN_CODE_FETCH ="Error while fetching Coupon Code";
	public static final String PROCESS_SUCESS_RESET = "Contest Reset Done successfully";
	public static final String PROCESS_ERR_RESET = "Error occured during Contest Reset process";
	public static final String PROCESS_ERR_START_CONT="Error occured while processing Start Contest";
	public static final String PROCESS_SUCESS_LOAD_APP = "Application Values Loaded Successfully.";
	public static final String PROCESS_ERR_LOAD_APP = "Error Ocusrred while Loading of  Application Values";
	public static final String PROCESS_ERR_LOAD_DASHBOARD="Error occurred while Fetching Dashboard Information";
	//GENERAL 
	
	
	//LIVT CONTEST MESSAGES ..
	public static final String LIVT_MSG_CONTEST_NOT_START = "Contest Not Yet Started:";
	public static final String LIVT_MSG_CONTEST_STARTED  ="Quiz Contest Started.";
	public static final String LIVT_ERR_CUST_CNT="Error occured while Fetching Contest customers count.";
	public static final String LIVT_ERR_CON_QUE="Error occured while Fetching Question Info.";
	public static final String LIVT_ERR_CON_ANSWERCOUNT="Error occured while Fetching Answer Count Details.";
	public static final String LIVT_GEN_SUCC_MSG="Success:";	
	public static final String LIVT_CONTEST_STARTED="STARTED";
	public static final String LIVT_CONTEST_ENDED="Contest Ended.";
	public static final String LIVT_ERR_MIGRATION_PROCESS="Error processing Migration of data";
	public static final String LIVT_INCORRECT_PASS="Password you entered is incorrect.";
	public static final String LIVT_ERR_PASS_PROCESS="Something went wrong while authenticating user details.";
	public static final String LIVT_MIGRATION_IN_PROCESS="Migration of  Contest data is being processed.";
	public static final String LIVT_ANSWER_ALREADY_SELECTED="Answer is Already selected:";
	public static final String LIVT_ANSWER_PREV_WRONG_SEL="You had selected wrong answer for previous question ";
	public static final String LIVT_ANSWER_PREV_WRONG_SEL_CONTINE_WATCH="You had selected a wrong answer for one of the previous question. You are eliminated but can still keep watching the game.";
	public static final String LIVT_ERR_FETCH_CON_INFO="Error occured while Fetching Start Contest Information.";
	public static final String LIVT_ERR_FETCH_EXIT_STATUS="Error occurred while Fetching Exit contest Details.";
	public static final String LIVT_ERR_FETCH_JOINCO_DETS= "Error occurred while Fetching Join contest Details.";
	public static final String LIVT_ERR_TIMER_UPD_DETS= "Error occurred while updating Timer details.";
	public static final String LIVT_ERR_FETCH_CUST_LIST= "Error occurred while Fetching Live Customer List.";
	
	//OTT 
	
	public static final String OTT_ELIMINATION = "Unfortunately you did not answer all questions right or missed to answer, Better luck next time.";
	public static final String OTT_CONGRATS =  "Congratulations! you have completed the game.";
	public static final String OTT_CONGRATS_WON = "Congratulations! you have won";
	public static final String OTT_ELIMINATION_MSG1 = "Sorry you are eliminated, Better luck next time.";
	public static final String OTT_NO_REPEAT = "You have already played this game.";
	public static final String NEXT_INTERVAL_MSG1= "You can play game after ";
	public static final String NEXT_INTERVAL_MSG2=" Minutes";
	public static final String OTT_MSG1 = "We Love your Choice";
	public static final String OTT_MSG2 = "No Prize This Time.";
	
	//eCOM Products and Cart 
	
	public static final String ECOM_PRICE_MANDATORY="Price  is mandatory";
	public static final String ECOM_PRICE_INVALID = "Price format is Invalid";
	public static final String ECOM_PRODID_MANDATORY = "Prod Id  is mandatory";
	public static final String ECOM_SELLERID_MANDATORY  = "Seller Id is mandatory";
	public static final String ECOM_SELLERID_INVALID  ="Seller Id is Invalid";
	
	// SNW 
	public static final String SNW_MSG1 = "We Love your Choice";
	public static final String SNW_MSG2 = "No Prize This Time.";
	public static final String SNW_MSG3 = "Your answer is incorrect";
	public static final String SNW_MSG4 = "Better luck next time.";
	public static final String SNW_CONGRATS_WON = "Congratulations! you have won";
	public static final String LIVT_MSG_USERID_START_NUMBER = "USER ID start Number is mandatory";
	
}
