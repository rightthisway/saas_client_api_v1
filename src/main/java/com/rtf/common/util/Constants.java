/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.common.util;

/**
 * The Class Constants.
 */
public class Constants {

	/** The all subcategory. */
	public static String ALL_SUBCATEGORY = "ALL";

	/** The ans type feedback. */
	public static String ANS_TYPE_FEEDBACK = "FEEDBACK";

	/** The ans type regular. */
	public static String ANS_TYPE_REGULAR = "REGULAR";

	/** The contest mode fixed. */
	public static String CONTEST_MODE_FIXED = "FIXED";

	/** The contest mode random. */
	public static String CONTEST_MODE_RANDOM = "RANDOM";

	/** The Constant OTT. */
	public static final String OTT = "OTT";

	/** The Constant SNW. */
	public static final String SNW = "SNW";

	/** The Constant SHOP. */
	public static final String SHOP = "SVT";

	/** The Constant LIVETRIVIA. */
	public static final String LIVETRIVIA = "LVT";

	/** The Constant COMN. */
	public static final String COMN = "COMN";

	/** The Constant CONTEST_INTERVAL_TIME. */
	public static final String CONTEST_INTERVAL_TIME = "CONTEST_INTERVAL_TIMER";

	/** The Constant QUESTION_WAIT_TIMER. */
	public static final String QUESTION_WAIT_TIMER = "QUESTION_WAIT_TIMER";

	/** The Constant QUESTION_TIMER. */
	public static final String QUESTION_TIMER = "QUESTION_TIMER";

	/** The Constant ANSWER_TIMER. */
	public static final String ANSWER_TIMER = "ANSWER_TIMER";

	/** The Constant ANSWER_PRIZE_POPUP_TIMER. */
	public static final String ANSWER_PRIZE_POPUP_TIMER = "ANSWER_PRIZE_POPUP_TIMER";

	/** The Constant CONTEST_QUESTION_MODE_FIXED. */
	public static final String CONTEST_QUESTION_MODE_FIXED = "FIXED";

	/** The Constant CONTEST_QUESTION_MODE_RANDOM. */
	public static final String CONTEST_QUESTION_MODE_RANDOM = "RANDOM";

	/** The Constant CONTEST_TRACK_STARTED. */
	public static final String CONTEST_TRACK_STARTED = "STARTED";

	/** The Constant CONTEST_TRACK_COMPLETED. */
	public static final String CONTEST_TRACK_COMPLETED = "COMPLETED";

	/** The Constant CONTEST_TRACK_PTYPE_QUESTION. */
	public static final String CONTEST_TRACK_PTYPE_QUESTION = "Q";

	/** The Constant CONTEST_TRACK_PTYPE_ANSWER. */
	public static final String CONTEST_TRACK_PTYPE_ANSWER = "A";

}
