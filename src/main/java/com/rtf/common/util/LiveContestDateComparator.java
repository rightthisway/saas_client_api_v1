package com.rtf.common.util;

import java.util.Comparator;

import com.rtf.livt.dvo.ContestDVO;

public class LiveContestDateComparator implements Comparator<ContestDVO>{

	@Override
	public int compare(ContestDVO o1, ContestDVO o2) {
		return o1.getStDate().compareTo(o2.getStDate());
	}
}
