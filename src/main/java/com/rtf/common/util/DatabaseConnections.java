/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.common.util;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;

import javax.sql.DataSource;

/**
 * The Class DatabaseConnections.
 */
public class DatabaseConnections {

	/** The rtf saas data source. */
	public static DataSource rtfSaasDataSource = null;

	/** The rtf quiz data source. */
	public static DataSource rtfQuizDataSource = null;

	/** The zones data source. */
	public static DataSource zonesDataSource = null;

	/** The saas connection. */
	public static Connection saasConnection = null;

	/** The saas db url. */
	public static String saasDbUrl = null;

	/** The saas db user name. */
	public static String saasDbUserName = null;

	/** The saas db password. */
	public static String saasDbPassword = null;

	/** The rtf connection. */
	public static Connection rtfConnection = null;

	/** The rtf db url. */
	public static String rtfDbUrl = null;

	/** The rtf db user name. */
	public static String rtfDbUserName = null;

	/** The rtf db password. */
	public static String rtfDbPassword = null;
	
	
	public static String reportdbUrl  = null;
	public static String reportdbUserName = null;
	public static String reportdbPassword = null;	
	

	/**
	 * Close connection.
	 *
	 * @param con the con
	 */
	public static void closeConnection(Connection con) {
		try {
			// null check not required as pooled obj.
			con.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	/**
	 * Gets the callable statement.
	 *
	 * @param connnection the connnection
	 * @param sql         the sql
	 * @return the callable statement
	 */
	public static CallableStatement getCallableStatement(Connection connnection, String sql) {
		try {
			return connnection.prepareCall(sql);
		} catch (SQLException sqlEx) {
			sqlEx.printStackTrace();
			return null;
		}

	}

	/**
	 * Gets the result set.
	 *
	 * @param statement the statement
	 * @param sql       the sql
	 * @return the result set
	 */
	public static ResultSet getResultSet(Statement statement, String sql) {
		try {
			return statement.executeQuery(sql);
		} catch (SQLException sqlEx) {
			return null;
		}
	}

	/**
	 * Gets the rtf connection.
	 *
	 * @return the rtf connection
	 * @throws Exception the exception
	 */
	public static Connection getRtfConnection() throws Exception {
		return rtfQuizDataSource.getConnection();
	}

	/**
	 * Gets the rtf DB connection.
	 *
	 * @return the rtf DB connection
	 * @throws Exception the exception
	 */
	public static Connection getRtfDBConnection() throws Exception {
		// String dbUrl =
		// "jdbc:sqlserver://54.175.222.230:1433;database=ZonesApiPlatformV2_Sanbox;useUnicode=true;characterEncoding=UTF-8";
		// String dbUrl =
		// "jdbc:sqlserver://10.0.1.49:1433;database=ZonesApiPlatformV2_Sanbox;useUnicode=true;characterEncoding=UTF-8";
		// String dbUrl =
		// "jdbc:sqlserver://54.175.222.230:1433;database=ZonesApiPlatformV2;useUnicode=true;characterEncoding=UTF-8";
		/*
		 * rtfDbUrl =
		 * "jdbc:sqlserver://10.0.1.49:1433;database=ZonesApiPlatformV2;useUnicode=true;characterEncoding=UTF-8";
		 * rtfDbUserName = "tayo"; rtfDbPassword = "po01ro02ro30";
		 */
		// 54.209.112.185 //10.0.1.51
		try {
			String driver = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
			Class.forName(driver);
			return DriverManager.getConnection(rtfDbUrl, rtfDbUserName, rtfDbPassword);
		} catch (SQLException sqlEx) {
			sqlEx.printStackTrace();
			throw sqlEx;
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * Gets the rtf saas connection.
	 *
	 * @return the rtf saas connection
	 * @throws Exception the exception
	 */
	public static Connection getRtfSaasConnection() throws Exception {		
		
		/*
		 * saasDbUrl =
		 * "jdbc:sqlserver://34.233.251.129:1433;database=sandboxSaaSProdClientAdminWcid;useUnicode=true;characterEncoding=UTF-8";
		 * saasDbUserName = "tayo"; saasDbPassword = "po01ro02ro30";
		 */
		 
		
		// saasDbUrl =
		// "jdbc:sqlserver://10.0.0.31:1433;database=ProdSaaSTriviaManagePlay;useUnicode=true;characterEncoding=UTF-8";
		
		try {
			String driver = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
			Class.forName(driver);
			return DriverManager.getConnection(saasDbUrl, saasDbUserName, saasDbPassword);
		} catch (SQLException sqlEx) {
			sqlEx.printStackTrace();
			throw sqlEx;
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
	}
	
	public static Connection getReportDBConnection() throws Exception {		
		
		
		try {
			String driver = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
			Class.forName(driver);
			System.out.println("[ SAASAPI Connection for REPORT DB " + reportdbUrl + "--" + reportdbUserName);
			return DriverManager.getConnection(reportdbUrl, reportdbUserName, reportdbPassword);
		} catch (SQLException sqlEx) {
			sqlEx.printStackTrace();
			throw sqlEx;
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
	}
	
	
	
	
	
	

	/**
	 * Gets the zones api connection.
	 *
	 * @return the zones api connection
	 * @throws Exception the exception
	 */
	public static Connection getZonesApiConnection() throws Exception {
		return zonesDataSource.getConnection();
	}

	/**
	 * Load application values.
	 */
	public static void loadApplicationValues() {
		ResourceBundle resourceBundle = ResourceBundle.getBundle("commonsettings");
		saasDbUrl = resourceBundle.getString("saas.sql.db.url");
		saasDbUserName = resourceBundle.getString("saas.sql.db.user.name");
		saasDbPassword = resourceBundle.getString("saas.sql.db.password");

		rtfDbUrl = resourceBundle.getString("rtf.sql.db.url");
		rtfDbUserName = resourceBundle.getString("rtf.sql.db.user.name");
		rtfDbPassword = resourceBundle.getString("rtf.sql.db.password");
		
		
		
		reportdbUrl = resourceBundle.getString("reportdb.sql.db.url");
		reportdbUserName = resourceBundle.getString("reportdb.sql.db.user.name");
		reportdbPassword = resourceBundle.getString("reportdb.sql.db.password");
		

	}

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		// CassContestUtil.refreshStartContestData("");
	}

	/**
	 * Sets the rtf datasource.
	 *
	 * @param datasource the new rtf datasource
	 */
	public static void setRtfDatasource(DataSource datasource) {
		rtfQuizDataSource = datasource;
	}

	/**
	 * Sets the rtf saas datasource.
	 *
	 * @param datasource the new rtf saas datasource
	 */
	public static void setRtfSaasDatasource(DataSource datasource) {
		rtfSaasDataSource = datasource;
	}

	/**
	 * Sets the zones datasource.
	 *
	 * @param datasource the new zones datasource
	 */
	public static void setZonesDatasource(DataSource datasource) {
		zonesDataSource = datasource;
	}
}
