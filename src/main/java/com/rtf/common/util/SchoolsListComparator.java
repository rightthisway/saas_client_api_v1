/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.common.util;

import java.util.Comparator;

import com.rtf.common.dvo.SchoolsListDVO;

/**
 * The Class SchoolsListComparator.
 */
public class SchoolsListComparator implements Comparator<SchoolsListDVO> {

	/**
	 * Compare.
	 *
	 * @param o1 the o 1
	 * @param o2 the o 2
	 * @return the int
	 */
	@Override
	public int compare(SchoolsListDVO o1, SchoolsListDVO o2) {

		return o1.getName().compareTo(o2.getName());

	}

}
