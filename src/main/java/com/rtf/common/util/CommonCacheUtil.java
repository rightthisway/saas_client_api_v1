/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.common.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import com.rtf.common.dao.FirestoreConfigDAO;
import com.rtf.common.dao.RewardTypeSQLDAO;
import com.rtf.common.dvo.ConfigDVO;
import com.rtf.common.dvo.RewardTypeDVO;
import com.rtf.saas.db.CassandraConnector;

/**
 * The Class CommonCacheUtil.
 */
public class CommonCacheUtil {

	/** The client fs config by client ID map. */
	public static Map<String, List<ConfigDVO>> clientFsConfigByClientIDMap = new HashMap<String, List<ConfigDVO>>();

	/** The client fs config by url map. */
	public static Map<String, List<ConfigDVO>> clientFsConfigByUrlMap = new HashMap<String, List<ConfigDVO>>();

	/** The reward type map. */
	public static Map<String, Map<String, RewardTypeDVO>> rewardTypeMap = new HashMap<String, Map<String, RewardTypeDVO>>();

	/** The clientcustommap. */
	public static Map<String, String> clientcustommap = new HashMap<String, String>();
	
	public static String nodeId;
	

	/**
	 * Gets the all client firestore config url list.
	 *
	 * @return the all client firestore config url list
	 * @throws Exception the exception
	 */
	public static Map<String, List<ConfigDVO>> getAllClientFirestoreConfigUrlList() throws Exception {
		return clientFsConfigByUrlMap;
	}

	/**
	 * Gets the client firestore config from cache by client id.
	 *
	 * @param clientId the client id
	 * @return the client firestore config from cache by client id
	 * @throws Exception the exception
	 */
	public static List<ConfigDVO> getClientFirestoreConfigFromCacheByClientId(String clientId) throws Exception {
		return clientFsConfigByClientIDMap.get(clientId);
	}

	/**
	 * Gets the client firestore config from cache by url.
	 *
	 * @param url the url
	 * @return the client firestore config from cache by url
	 * @throws Exception the exception
	 */
	public static List<ConfigDVO> getClientFirestoreConfigFromCacheByUrl(String url) throws Exception {
		return clientFsConfigByUrlMap.get(url);
	}

	/**
	 * Gets the reward type from map.
	 *
	 * @param clId       the client Id
	 * @param rewardType the reward type
	 * @return the reward type from map
	 */
	public static RewardTypeDVO getRewardTypeFromMap(String clId, String rewardType) {
		try {
			if (rewardTypeMap.get(clId) != null) {
				Map<String, RewardTypeDVO> rMap = rewardTypeMap.get(clId);
				if (rMap.get(rewardType) != null) {
					return rMap.get(rewardType);
				} else {
					RewardTypeDVO rType = RewardTypeSQLDAO.getRewardType(clId, rewardType);
					rMap.put(rewardType, rType);
					rewardTypeMap.put(clId, rMap);
					return rType;
				}
			} else {
				Map<String, RewardTypeDVO> rMap = new HashMap<String, RewardTypeDVO>();
				RewardTypeDVO rType = RewardTypeSQLDAO.getRewardType(clId, rewardType);
				rMap.put(rewardType, rType);
				rewardTypeMap.put(clId, rMap);
				return rType;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Load application values.
	 *
	 * @throws Exception the exception
	 */
	public static void loadApplicationValues() throws Exception {
		CassandraConnector.loadApplicationValues();
		DatabaseConnections.loadApplicationValues();
		CommonCacheUtil.refreshClientFirestoreConfigCache();
		loadclientcustommap();
	}

	/**
	 * Load client customers to Cached Mmap.
	 *
	 * @throws Exception the exception
	 */
	private static void loadclientcustommap() throws Exception {
		try {
			ResourceBundle resourceBundle = ResourceBundle.getBundle("commonsettings");
			String rtfcartbaseapiurl = resourceBundle.getString("saas.client.rtfcartbaseapiurl");
			clientcustommap.put("rtfcartbaseapiurl", rtfcartbaseapiurl);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * Refresh client firestore config cache.
	 *
	 * @throws Exception the exception
	 */
	public static void refreshClientFirestoreConfigCache() throws Exception {

		List<ConfigDVO> list = FirestoreConfigDAO.getAllClientConfig();
		Map<String, List<ConfigDVO>> clientTempMap = new HashMap<String, List<ConfigDVO>>();
		Map<String, List<ConfigDVO>> urlTempMap = new HashMap<String, List<ConfigDVO>>();
		if (list != null) {
			for (ConfigDVO configDVO : list) {
				List<ConfigDVO> configList = clientTempMap.get(configDVO.getClId());
				if (configList == null) {
					configList = new ArrayList<ConfigDVO>();
				}
				configList.add(configDVO);
				clientTempMap.put(configDVO.getClId(), configList);

				if (configDVO.getSaasweburl() != null && !configDVO.getSaasweburl().isEmpty()) {
					List<ConfigDVO> urlConfigList = urlTempMap.get(configDVO.getSaasweburl());
					if (urlConfigList == null) {
						urlConfigList = new ArrayList<ConfigDVO>();
					}
					urlConfigList.add(configDVO);
					urlTempMap.put(configDVO.getSaasweburl(), urlConfigList);
				}
			}
			clientFsConfigByClientIDMap = new HashMap<String, List<ConfigDVO>>(clientTempMap);
			clientFsConfigByUrlMap = new HashMap<String, List<ConfigDVO>>(urlTempMap);
		}
	}

	/**
	 * Refresh client firestore configuration cache by client id.
	 *
	 * @param clientId the client id
	 * @throws Exception the exception
	 */
	public static void refreshClientFirestoreConfigCacheByClientId(String clientId) throws Exception {
		List<ConfigDVO> list = FirestoreConfigDAO.getClientConfig(clientId);
		if (list != null) {
			String url = null;
			List<ConfigDVO> tempList = new ArrayList<ConfigDVO>();
			for (ConfigDVO configDVO : list) {

				if (configDVO.getSaasweburl() != null && !configDVO.getSaasweburl().isEmpty()) {
					tempList.add(configDVO);
					url = configDVO.getSaasweburl();
				}
			}
			clientFsConfigByClientIDMap.put(clientId, list);
			if (url != null) {
				clientFsConfigByUrlMap.put(url, tempList);
			}

		}
	}
	
	public static String getNodeId() {		
		return nodeId;
	}
	

	public static void setNodeId(String nodeId) {		
		nodeId = nodeId;
	}

	

	

}