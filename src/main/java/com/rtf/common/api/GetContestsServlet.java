/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.common.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rtf.common.dspl.dto.ContestCardDTO;
import com.rtf.common.dspl.dvo.LiveContestDVO;
import com.rtf.common.dspl.dvo.OttContestDVO;
import com.rtf.common.dspl.dvo.SnwContestDVO;
import com.rtf.common.util.Messages;
import com.rtf.livt.sql.dao.LiveContestSQLDAO;
import com.rtf.ott.service.OTTService;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GsonUtil;
import com.rtf.saas.util.SAASMessages;
import com.rtf.snw.service.SNWContestService;

/**
 * The Class GetContestsServlet.
 */
@WebServlet("/getcontestcards.json")
public class GetContestsServlet extends RtfSaasBaseServlet {

	/**
	 *
	 */
	private static final long serialVersionUID = 9109910910610404790L;

	/**
	 *  Generate Http Response for Client.Response, data is sent in JSON format.
	 *
	 * @param request        the  HttpServlet request
	 * @param response        the  HttpServlet response
	 * @param rtfSaasBaseDTO RTF Base Data Transfer Object
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("cardresp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();

	}

	/**
	 * Process request.Fetch all configured Text Trivia for Client .For listing as cards on DashBoard
	 *
	 * @param request  the HttpServletRequest
	 * @param response the HttpServletResponse
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String clientIdStr = request.getParameter("clId");
		ContestCardDTO respDTO = new ContestCardDTO();
		respDTO.setClId(clientIdStr);
		respDTO.setSts(0);
		try {
			if (clientIdStr == null || clientIdStr.isEmpty()) {
				setClientMessage(respDTO, Messages.INVALID_CLIENT_ID, null);
				generateResponse(request, response, respDTO);
				return;
			}
			List<LiveContestDVO> contests = LiveContestSQLDAO.getAllActiveContestCards(clientIdStr);
			List<OttContestDVO> ottContests = OTTService.getAllContestCards(clientIdStr);
			SnwContestDVO snwContest = SNWContestService.getContestCard(clientIdStr);
			if (contests == null || contests.isEmpty()) {
				respDTO.setIsLivContSchdld(false);
				respDTO.setNoLivContMsg(Messages.NXT_CONTEST_ANNOUNCED);
			} else {
				respDTO.setIsLivContSchdld(true);
				respDTO.setPlayBtnText(Messages.BTN_PLAY_TXT);
			}
			if (ottContests == null || ottContests.isEmpty()) {
				respDTO.setIsOttContSchdld(false);
				respDTO.setNoOttContMsg(Messages.NXT_CONTEST_ANNOUNCED);
			} else {
				respDTO.setIsOttContSchdld(true);
			}
			if (snwContest == null) {
				respDTO.setIsSnwSchdld(false);
				respDTO.setNoSnwContMsg(Messages.NXT_CONTEST_ANNOUNCED);
			} else {
				respDTO.setIsSnwSchdld(true);
			}
			respDTO.setLivContests(contests);
			respDTO.setOttContests(ottContests);
			respDTO.setSnwContest(snwContest);
			respDTO.setSts(1);
			generateResponse(request, response, respDTO);
			return;
		} catch (Exception e) {
			e.printStackTrace();
			setClientMessage(respDTO, SAASMessages.GEN_ERR_MSG, null);
			generateResponse(request, response, respDTO);
			return;
		}
	}

}
