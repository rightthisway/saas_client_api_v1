/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.common.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;

import com.rtf.client.signup.service.CustomerSignupService;
import com.rtf.common.dao.ClientCustomerDAO;
import com.rtf.common.dto.ConfigDTO;
import com.rtf.common.dvo.ClientConfigDVO;
import com.rtf.common.dvo.ClientCustomerDVO;
import com.rtf.common.dvo.ConfigDVO;
import com.rtf.common.service.ClientConfigService;
import com.rtf.common.service.FirestoreConfigService;
import com.rtf.common.service.SaaSApiTrackingService;
import com.rtf.common.util.Messages;
import com.rtf.livt.util.LivtContestUtil;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GenUtil;
import com.rtf.saas.util.GsonUtil;
import com.rtf.saas.util.SAASMessages;

/**
 * The Class LoadClientSaaSConfigServlet.
 */
@WebServlet("/GetClientSaaSConfig.json")
public class LoadClientSaaSConfigServlet extends RtfSaasBaseServlet {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 3784813588514510146L;

	/**
	 *  Generate Http Response for Client.Response, data is sent in JSON format.
	 *
* @param request        the  HttpServlet request
* @param response        the  HttpServlet response
	 * @param rtfSaasBaseDTO RTF Base Data Transfer Object
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("commresp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();
	}

	/**
	 * Process request.Method to fetch Client based FireStore and Assign to Customer . 
	 * Register Customer if New Login . Update FireStore Count for Customer base
	 *
	 * @param request  the HttpServletRequest
	 * @param response the HttpServletResponse
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String clId;
		String cuId;
		String uId =StringUtils.EMPTY;		
		String type;
		String urlobj;
		String platform =StringUtils.EMPTY;
		Timestamp stTime = GenUtil.getCurrentTimeStamp();
		String actMsg =StringUtils.EMPTY;
		String deviceInfo = StringUtils.EMPTY;		
		ConfigDTO dto = new ConfigDTO();
		try {
			cuId = request.getParameter("cuId");			
			clId = request.getParameter("clId");
			uId = request.getParameter("uId");
			type = request.getParameter("type");
			urlobj = request.getParameter("weburl");
			platform = request.getParameter("pfm");
			if (GenUtil.isEmptyString(clId) && GenUtil.isEmptyString(urlobj)) {
				setClientMessage(dto, SAASMessages.GEN_ERR_MSG, null);
				generateResponse(request, response, dto);
				actMsg = Messages.SAASURL_CLIENTID_NULL;
				return;

			}
			uId = null;
			String nodeId = (String)getServletContext().getAttribute("nodeId");
			ServletContext appCtx = getServletContext();
			
			System.out.println("SAASCONFIG NODE ID IN APPCONTEXT IS "  +  (String) appCtx.getAttribute("nodeId") ) ;
			String coId = "";
			ClientCustomerDVO clientCustoemr = null;
			if (!GenUtil.isEmptyString(cuId)) {
				cuId = cuId.trim();
				coId = LivtContestUtil.getLiveRunningContestByClientIdForSaasConfig(clId);
				
				clientCustoemr = ClientCustomerDAO.getClientCustomerByCustomerId(clId, cuId);				
				System.out.println("SAASCONFIG CALLED :    CUSTID = "+cuId+"   |   UID = "+uId+"  |  COID = "+coId+"  |  CUSTOMER = "+clientCustoemr+"  |  CID = "+clId);
				
				  Boolean isAuto = true;
				  ClientConfigDVO dvo = ClientConfigService.getContestConfigByKey(clId, "LVT", "IS_USERID_AUTO_GENERATED");
					if(dvo == null || dvo.getValue().equalsIgnoreCase("NO")) {
						isAuto = false;		
					}
					
				
				if (clientCustoemr == null) {
					clientCustoemr = new ClientCustomerDVO();
					clientCustoemr.setClId(clId);					
					clientCustoemr.setCuId(cuId);
					clientCustoemr.setSignUpType("CLIENT");
					clientCustoemr.setUpldate(new Date());
					clientCustoemr.setSchoolName(null);	
					clientCustoemr.setCoId(coId);
							//2 CREATE CUST
					
					System.out.println("NEW CUSTOMER SAASCONFIG CALLED-22222 : Customer Object " + clientCustoemr);
					String ce = String.format("%06d", new Random().nextInt(100000));
					clientCustoemr.setCe(ce);
					if(isAuto) {
						CustomerSignupService.generateNewUserId(clientCustoemr,appCtx,coId);
					}
					ClientCustomerDAO.saveClientCustomer(clientCustoemr);
				}else{				
						if((clientCustoemr.getUserId() == null || clientCustoemr.getUserId().isEmpty()) && isAuto ){
							clientCustoemr.setCoId(coId);
							//3 CREATE CUST
							CustomerSignupService.generateNewUserId(clientCustoemr,appCtx,coId);
							ClientCustomerDAO.saveClientCustomer(clientCustoemr);							
							System.out.println(" EXISTING CURSTOMER SAASCONFIG CALLED-333 : Customer Object " + clientCustoemr);
						}
					}			
				dto.setCuID(cuId);
				dto.setUserId(cuId);				
				if(isAuto) {
					dto.setUserId(clientCustoemr.getUserId()==null?"":clientCustoemr.getUserId());
				}				
				
			}
			dto.setClId(clId);
			dto.setSaasweburl(urlobj);
			if (type != null && (type.equalsIgnoreCase(Messages.TEXT_TRIVIA) || type.equalsIgnoreCase(Messages.SCRATCH_TRIVIA))) {
				dto.setSts(1);
				generateResponse(request, response, dto);
				return;
			}
			ConfigDVO conf = null;
			List<ConfigDVO> configs = FirestoreConfigService.getClientConfig(clId, urlobj);

			if (configs == null || configs.isEmpty()) {
				setClientMessage(dto, SAASMessages.GEN_ERR_MSG, null);
				generateResponse(request, response, dto);
				return;
			}
			if (clientCustoemr == null) {
				conf = configs.get(0);
				dto.setSts(1);
				dto.setDvo(conf);
				generateResponse(request, response, dto);
				return;
			}
			if (clientCustoemr.getFsConfId() == null || clientCustoemr.getFsConfId() <= 0) {
				for (ConfigDVO config : configs) {
					if (config.getCustCount() == null || config.getCustCount() < 900000) {
						config.setCustCount(config.getCustCount() == null ? 0 : config.getCustCount() + 1);
						conf = config;
						break;
					}
				}
				if (conf == null) {
					conf = configs.get(0);
				}
				boolean isUpdated = FirestoreConfigService.updateCustCount(conf);
				if (!isUpdated) {
					dto.setSts(0);
					setClientMessage(dto, SAASMessages.GEN_ERR_MSG, null);
					generateResponse(request, response, dto);
					return;
				}
				clientCustoemr.setFsConfId(conf.getId());
				isUpdated = ClientCustomerDAO.updateClientCustomerFirestoreConfi(clientCustoemr);
				if (!isUpdated) {
					dto.setSts(0);
					setClientMessage(dto,Messages.ERROR_FIRESTORE_CONFIG_ASSIGN, null);
					generateResponse(request, response, dto);
					return;
				}
			} else {
				conf = FirestoreConfigService.getClientConfigforCustomer(clId, clientCustoemr.getFsConfId());
				if (conf == null) {
					dto.setSts(0);
					setClientMessage(dto, Messages.ERROR_NO_CUST_CONFIG, null);
					generateResponse(request, response, dto);
					return;
				}
			}
			dto.setSts(1);
			dto.setDvo(conf);
			generateResponse(request, response, dto);
		} catch (Exception ex) {
			dto.setSts(0);
			ex.printStackTrace();
			setClientMessage(dto, SAASMessages.GEN_ERR_MSG, null);
			generateResponse(request, response, dto);
		} finally {
			SaaSApiTrackingService.insertApiTrackingDetails(dto.getClId(), dto.getCuID(), "CON", "SAASWEBVIEW",
					dto.getCoId(), actMsg, GenUtil.getIp(request), platform, "sessionId", GenUtil.getDesc(request), // session
																													// &
																													// description
					stTime, GenUtil.getCurrentTimeStamp(), // Start Time & End Time
					1, dto.getSts(), deviceInfo); // Cluster Node Id & response status
		}
	}
}
