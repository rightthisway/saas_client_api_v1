/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.common.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;

import com.rtf.common.util.CommonCacheUtil;
import com.rtf.common.util.Messages;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GenUtil;
import com.rtf.saas.util.GsonUtil;
import com.rtf.saas.util.SAASMessages;

/**
 * The Class RefreshApplicationCacheServlet.
 */
@WebServlet("/printcachevalues.json")
public class AdminCacheCheckServlet extends RtfSaasBaseServlet {

	/** The Constant serialVersionUID. */

	private static final long serialVersionUID = 3784813588514510146L;

	/**
	 *  Generate Http Response for Client.Response, data is sent in JSON format.
	 *
* @param request        the  HttpServlet request
* @param response        the  HttpServlet response
	 * @param rtfSaasBaseDTO RTF Base Data Transfer Object
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("commresp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();

	}

	/**
	 * Process request.
	 *
	 * @param request  the HttpServletRequest
	 * @param response the HttpServletResponse
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {		
		
		ServletContext context=getServletContext();  
		
		Enumeration<String> enums = context.getAttributeNames();
		
		while(enums.hasMoreElements()) {
			String keys = enums.nextElement();			
			 System.out.println("* key is ** " + keys);			 
			 System.out.println("* value is ** " + context.getAttribute(keys));
		}
		
		String clId = null;
		String type;
		//Timestamp stTime = GenUtil.getCurrentTimeStamp();
		String actMsg =StringUtils.EMPTY;
		//String deviceInfo = StringUtils.EMPTY;
		String tempTestId = "";
		RtfSaasBaseDTO dto = new RtfSaasBaseDTO();
		try {
			clId = request.getParameter("clId");
			type = request.getParameter("type");
			if (GenUtil.isEmptyString(type)) {
				setClientMessage(dto, Messages.INVALID_CONFIGTYPE, null);
				generateResponse(request, response, dto);
				actMsg = Messages.INVALID_CONFIGTYPE;
				return;
			}
			if (type.equals(Messages.CLIENTFIRESTORECONFIG)) {
				CommonCacheUtil.refreshClientFirestoreConfigCache();
			}
			dto.setSts(1);
			dto.setMsg(Messages.CACHE_UPD_SUCC);
			generateResponse(request, response, dto);
		} catch (Exception ex) {
			dto.setSts(0);
			setClientMessage(dto, SAASMessages.GEN_ERR_MSG, null);
			generateResponse(request, response, dto);
		} finally {
			}
	}

}
