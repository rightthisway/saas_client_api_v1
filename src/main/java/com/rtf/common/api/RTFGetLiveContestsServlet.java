/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.common.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rtf.common.dto.ContestCardDTO;
import com.rtf.common.util.LiveContestDateComparator;
import com.rtf.common.util.Messages;
import com.rtf.livt.cass.dao.LiveContestCassDAO;
import com.rtf.livt.dvo.ContestDVO;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.DateFormatUtil;
import com.rtf.saas.util.GsonUtil;
import com.rtf.saas.util.SAASMessages;

/**
 * The Class RTFGetLiveContestsServlet.
 */
@WebServlet("/livgetcontestcards.json")
public class RTFGetLiveContestsServlet extends RtfSaasBaseServlet {

	/**
	 *
	 */
	private static final long serialVersionUID = -2987997689562665670L;

	/**
	 *  Generate Http Response for Client.Response, data is sent in JSON format.
	 *
* @param request        the  HttpServlet request
* @param response        the  HttpServlet response
	 * @param rtfSaasBaseDTO RTF Base Data Transfer Object
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("livresp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();

	}

	/**
	 * Process request.Fetch the Current Active Contest for Display on DashBoard
	 *
	 * @param request  the HttpServletRequest
	 * @param response the HttpServletResponse
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String clientIdStr = request.getParameter("clId");
		ContestCardDTO respDTO = new ContestCardDTO();
		respDTO.setClId(clientIdStr);
		respDTO.setSts(0);
		try {
			if (clientIdStr == null || clientIdStr.isEmpty()) {
				setClientMessage(respDTO, Messages.INVALID_CLIENT_ID, null);
				generateResponse(request, response, respDTO);
				return;
			}
			//List<ContestDVO> contests = LiveContestSQLDAO.getActiveContestCards(clientIdStr);
			List<ContestDVO> contests = LiveContestCassDAO.getAllDisplayContest(clientIdStr);
			//List<com.rtf.ott.dvo.ContestDVO> ottContests = OTTService.getAllLiveContest(clientIdStr);
			//com.rtf.snw.dvo.ContestDVO snwContest = SNWContestService.getLiveContestsByClientId(clientIdStr);
			if (contests == null || contests.isEmpty()) {
				ContestDVO c = new ContestDVO();
				c.setName(Messages.NXT_CONTEST_TB_ANNOUNCED);
				c.setCat("");
				respDTO.setPlayBtnText("");
				contests.add(c);
			} else {
				Collections.sort(contests, new LiveContestDateComparator());
				String cDate = DateFormatUtil.getMMDDYYYYString(contests.get(0).getStDate());
				String todaysDate = DateFormatUtil.getMMDDYYYYString(new Date().getTime());
				if (cDate.equalsIgnoreCase(todaysDate)) {
					respDTO.setPlayBtnText(Messages.BTN_PLAY_TEXT);
				} else {
					respDTO.setPlayBtnText("");
				}
			}

			/*if (ottContests == null || ottContests.isEmpty()) {
				com.rtf.ott.dvo.ContestDVO c = new com.rtf.ott.dvo.ContestDVO();
				c.setName(Messages.NXT_CONTEST_ANNOUNCED);
				ottContests.add(c);
			}*/

			respDTO.setContests(contests);
			respDTO.setOttContests(new ArrayList<com.rtf.ott.dvo.ContestDVO>());
			//respDTO.setSnwContest(snwContest);
			respDTO.setSts(1);
			generateResponse(request, response, respDTO);
			return;
		} catch (Exception e) {
			e.printStackTrace();
			setClientMessage(respDTO, SAASMessages.GEN_ERR_MSG, null);
			generateResponse(request, response, respDTO);
			return;
		}
	}

}
