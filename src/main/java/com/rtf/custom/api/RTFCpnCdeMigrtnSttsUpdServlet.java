/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.custom.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rtf.common.util.Messages;
import com.rtf.custom.dto.RTFCouponCodeDTO;
import com.rtf.custom.dvo.RTFCustCouponCodeDVO;
import com.rtf.custom.service.RTFCouponCodeService;
import com.rtf.livt.util.TextUtil;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GsonUtil;

/**
 * The Class RTFCpnCdeMigrtnSttsUpdServlet.
 */

@WebServlet("/livtcrcpncdemigstsupd.json")
public class RTFCpnCdeMigrtnSttsUpdServlet extends RtfSaasBaseServlet {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 *  Generate Http Response for Client.Response, data is sent in JSON format.
	 *
* @param request        the  HttpServlet request
* @param response        the  HttpServlet response
	 * @param rtfSaasBaseDTO RTF Base Data Transfer Object
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("livtresp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();

	}

	/**
	 * Process request.Method to update Migration status of coupopn code
	 *
	 * @param request  the HttpServletRequest
	 * @param response the HttpServletResponse
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		RTFCouponCodeDTO dto = new RTFCouponCodeDTO();
		dto.setSts(0);

		String clientIdStr = request.getParameter("clId");
		String prevFecthDatTimestr = request.getParameter("prvftchdte");
		String maxFecthDatTimestr = request.getParameter("maxdate");

		Long maxDateTime = null;
		Long prevFetchDateTime = null;

		try {

			if (TextUtil.isEmptyOrNull(clientIdStr)) {
				setClientMessage(dto, Messages.INVALID_CLIENT_ID, null, true);
				generateResponse(request, response, dto);
				return;
			}
			
			if (TextUtil.isEmptyOrNull(maxFecthDatTimestr)) {
				setClientMessage(dto, Messages.INVALID_MAX_DATE, null, true);
				generateResponse(request, response, dto);
				return;
			}

			try {
				maxDateTime = Long.valueOf(maxFecthDatTimestr);
			} catch (Exception ex) {
				setClientMessage(dto,Messages.INVALID_MAX_MIGRATION_DATE_FMT, null, true);
				generateResponse(request, response, dto);
				return;
			}

			if (prevFecthDatTimestr != null) {
				try {
					prevFetchDateTime = Long.valueOf(prevFecthDatTimestr);
					dto.setPrvftchdte(prevFetchDateTime);
				} catch (Exception ex) {
					// Ignore Exception ... Can be null ...
				}
			}
			new RTFCustCouponCodeDVO();
			dto.setClId(clientIdStr);
			dto.setMaxftchdte(maxDateTime);

			dto = RTFCouponCodeService.updateCouponCodeMigrationStatus(dto);
			if (dto.getSts() == 0) {
				setClientMessage(dto, null, null);
				generateResponse(request, response, dto);
				return;
			}
			setClientMessage(dto, Messages.PROCESS_SUCESS_CPN_CODE_STATUS_UPDATE, null, true);
			generateResponse(request, response, dto);
			return;

		} catch (Exception e) {
			e.printStackTrace();
			setClientMessage(dto, Messages.PROCESS_ERR__CPN_CODE_STATUS_UPDATE, null, true);
			generateResponse(request, response, dto);
			return;

		} finally {

		}

	}

}
