/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.custom.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rtf.common.dao.ClientCustomerDAO;
import com.rtf.common.dvo.ClientCustomerDVO;
import com.rtf.common.util.Messages;
import com.rtf.custom.dto.RTFCouponCodeDTO;
import com.rtf.custom.dvo.RTFCustCouponCodeDVO;
import com.rtf.livt.dvo.ContestDVO;
import com.rtf.livt.dvo.LivtContestQuestionDVO;
import com.rtf.livt.util.LivtContestUtil;
import com.rtf.livt.util.TextUtil;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GsonUtil;

/**
 * The Class RTFCreateCouponCodeServlet.
 */

@WebServlet("/livtcrcpncde.json")
public class RTFCreateCouponCodeServlet extends RtfSaasBaseServlet {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Generate Http Response for Client.Response, data is sent in JSON format.
	 *
	 * @param request        the HttpServlet request
	 * @param response       the HttpServlet response
	 * @param rtfSaasBaseDTO RTF Base Data Transfer Object
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("livtresp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();

	}

	/**
	 * Process request Method to createCoupon Code for a client's Customer
	 *
	 * @param request  the HttpServletRequest
	 * @param response the HttpServletResponse
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		RTFCouponCodeDTO dto = new RTFCouponCodeDTO();
		dto.setSts(0);
		Date start = new Date();
		String resMsg = "";
		String clientIdStr = request.getParameter("clId");
		String customerIdStr = request.getParameter("cuId");
		String contestIdStr = request.getParameter("coId");
		String questionNoStr = request.getParameter("qNo");
		String questionIdStr = request.getParameter("qId");
		String encKey = request.getParameter("qenck");
		String disc = request.getParameter("disc");
		String cpncde = request.getParameter("cpncde");

		//String fbTimeStr = request.getParameter("fbctm");// - forebase callback time
		//String apiHitStartTimeStr = request.getParameter("ahstm");// - api hit start time
		//String netspeedStr = request.getParameter("netspd");// - mobile net speed
		Integer questionNo = null;
		BigDecimal discount = null;
		String description = "";

		try {

			if (TextUtil.isEmptyOrNull(cpncde)) {
				resMsg = Messages.INVALID_CPN_CDE + customerIdStr;
				description = "qId=" + questionIdStr + ",qNo=" + questionNoStr + ",encKey=" + encKey + ",cuId="
						+ customerIdStr + ",coId=" + contestIdStr;
				setClientMessage(dto, Messages.INVALID_CPN_CDE, null, true);
				generateResponse(request, response, dto);
				return;

			}
			if (TextUtil.isEmptyOrNull(disc)) {
				resMsg = Messages.INVALID_DISCOUNT_VAL + disc;
				description = "qId=" + questionIdStr + ",qNo=" + questionNoStr + ",encKey=" + encKey + ",cuId="
						+ customerIdStr + ",coId=" + contestIdStr;
				setClientMessage(dto, Messages.INVALID_DISCOUNT_VAL, null, true);
				generateResponse(request, response, dto);
				return;

			}
			try {
				discount = new BigDecimal(disc);
			} catch (Exception ex) {
				resMsg = Messages.INVALID_DISCOUNT_VAL + disc;
				description = "qId=" + questionIdStr + ",qNo=" + questionNoStr + ",encKey=" + encKey + ",cuId="
						+ customerIdStr + ",coId=" + contestIdStr;
				setClientMessage(dto, Messages.INVALID_DISCOUNT_VAL, null, true);
				generateResponse(request, response, dto);
				return;

			}

			if (TextUtil.isEmptyOrNull(customerIdStr)) {
				resMsg = Messages.INVALID_CUSTOMER_ID + customerIdStr;
				description = "qId=" + questionIdStr + ",qNo=" + questionNoStr + ",encKey=" + encKey + ",cuId="
						+ customerIdStr + ",coId=" + contestIdStr;
				setClientMessage(dto, Messages.INVALID_CUSTOMER_ID, null, true);
				generateResponse(request, response, dto);
				return;

			}
			if (TextUtil.isEmptyOrNull(clientIdStr)) {
				resMsg = Messages.INVALID_CLIENT_ID + clientIdStr;
				description = "qId=" + questionIdStr + ",qNo=" + questionNoStr + ",encKey=" + encKey + ",cuId="
						+ customerIdStr + ",coId=" + contestIdStr;
				setClientMessage(dto, Messages.INVALID_CLIENT_ID, null, true);
				generateResponse(request, response, dto);
				return;
			}
			if (TextUtil.isEmptyOrNull(encKey)) {
				resMsg = Messages.INVALID_ENC_KY + encKey;
				description = "qId=" + questionIdStr + ",qNo=" + questionNoStr + ",encKey=" + encKey + ",cuId="
						+ customerIdStr + ",coId=" + contestIdStr;
				setClientMessage(dto, null, null, true);
				generateResponse(request, response, dto);
				return;
			}
			ClientCustomerDVO customer = ClientCustomerDAO.getClientCustomerForValidationByCustomerId(clientIdStr,
					customerIdStr);
			if (customer == null) {
				resMsg = Messages.INVALID_CUSTOMER_ID + customerIdStr;
				setClientMessage(dto, Messages.INVALID_CUSTOMER_ID, null, true);
				generateResponse(request, response, dto);
				return;
			}
			Integer questionId = null;
			try {
				questionId = Integer.parseInt(questionIdStr.trim());
			} catch (Exception e) {
				resMsg = Messages.INVALID_QUESTION_ID + questionIdStr;
				description = "qId=" + questionIdStr + ",qNo=" + questionNoStr + ",encKey=" + encKey + ",cuId="
						+ customerIdStr + ",coId=" + contestIdStr;
				setClientMessage(dto, Messages.INVALID_QUESTION_ID, null, true);
				generateResponse(request, response, dto);
				return;
			}

			try {
				questionNo = Integer.parseInt(questionNoStr.trim());
			} catch (Exception e) {
				resMsg = Messages.INVALID_QUESTION_ID + questionIdStr;
				description = "qId=" + questionIdStr + ",qNo=" + questionNoStr + ",encKey=" + encKey + ",cuId="
						+ customerIdStr + ",coId=" + contestIdStr;
				setClientMessage(dto, Messages.INVALID_QUESTION_ID, null, true);
				generateResponse(request, response, dto);
				return;
			}

			ContestDVO contest = LivtContestUtil.getCurrentContestByContestId(clientIdStr, contestIdStr);
			if (contest == null) {
				resMsg = Messages.INVALID_CONTEST_ID + contestIdStr;
				description = "qId=" + questionIdStr + ",qNo=" + questionNoStr + ",encKey=" + encKey + ",cuId="
						+ customerIdStr + ",coId=" + contestIdStr;
				setClientMessage(dto, Messages.INVALID_CONTEST_ID, null, true);
				generateResponse(request, response, dto);
				return;
			}
			LivtContestQuestionDVO quizQuestion = LivtContestUtil.getLivtContestQuestionById(clientIdStr, contestIdStr,
					questionId, questionNo);

			if (quizQuestion == null || !quizQuestion.getEncKey().equals(encKey)) {
				resMsg = Messages.INVALID_QUESTION_ID + questionIdStr + ":" + encKey;
				description = "qId=" + questionIdStr + ",qNo=" + questionNoStr + ",encKey=" + encKey + ",cuId="
						+ customerIdStr + ",coId=" + contestIdStr;

				setClientMessage(dto, Messages.INVALID_QUESTION_ID, null, true);
				generateResponse(request, response, dto);
				return;
			}

			RTFCustCouponCodeDVO dvo = new RTFCustCouponCodeDVO();
			dvo.setClId(clientIdStr);
			dvo.setCuId(customerIdStr);
			dvo.setCoId(contestIdStr);
			dvo.setqId(questionId);
			dvo.setqNo(questionNo);
			dvo.setWintyp(Messages.QUEAND_ANSTYPE);
			dvo.setDisc(discount);
			dvo.setCpncde(cpncde);
			dvo.setRunNo(contest.getContRunningNo());
			dto.setDvo(dvo);
			dto.setSts(1);
			if (dto.getSts() == 0) {
				setClientMessage(dto, null, null);
				generateResponse(request, response, dto);
				return;
			}
			setClientMessage(dto, Messages.PROCESS_SUCESS_CPN_CODE_CREATION, null, true);
			generateResponse(request, response, dto);
			return;

		} catch (Exception e) {
			description = "qId=" + questionIdStr + ",qNo=" + questionNoStr + ",cuId=" + customerIdStr + ",coId="
					+ contestIdStr;
			e.printStackTrace();
			resMsg = Messages.PROCESS_ERR_CPN_CODE_CREATION;
			setClientMessage(dto, resMsg, null, true);
			generateResponse(request, response, dto);
			return;

		} finally {
			/*
			 * String deviceInfo = netspeedStr;
			 * TrackingUtil.contestValidateAnswerAPITrackingForDeviceTimeTracking(
			 * clientIdStr, request.getHeader("x-platform"), null,
			 * request.getHeader("deviceId"), WebServiceActionType.CREATECOUPON, resMsg,
			 * questionNoStr, questionIdStr, cpncde, contestIdStr, customerIdStr, start, new
			 * Date(), request.getHeader("X-Forwarded-For"), apiHitStartTimeStr, fbTimeStr,
			 * deviceInfo, dto.getSts(), questionNoStr, null, null, description);
			 */

		}

	}

}
