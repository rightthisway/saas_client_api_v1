/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.custom.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rtf.common.util.Messages;
import com.rtf.custom.dto.GenRewardDTO;
import com.rtf.custom.service.GenericRewardMigrationService;
import com.rtf.custom.utils.Constants;
import com.rtf.livt.util.TextUtil;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GsonUtil;

/**
 * The Class GenericRewardTypeMigrtnStatusUpdateServlet.
 */

@WebServlet("/clconrwdtypsmigstsupd.json")
public class GenericRewardTypeMigrtnStatusUpdateServlet extends RtfSaasBaseServlet {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Generate Http Response for Client.Response, data is sent in JSON format.
	 *
	 * @param request  the HttpServletRequest
	 * @param response the HttpServletResponse
	 * @param dto      the dto
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	public void generateResponse(HttpServletRequest request, HttpServletResponse response, GenRewardDTO dto)
			throws ServletException, IOException {
		Map<String, GenRewardDTO> map = new HashMap<String, GenRewardDTO>();
		map.put("resp", dto);
		String respstr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(respstr);
		out.flush();

	}

	/**
	 * Generate Http Response for Client.Response, data is sent in JSON format.
	 *
	 * @param request        the HttpServlet request
	 * @param response       the HttpServlet response
	 * @param RtfSaasBaseDTO RTF Base Data Transfer Object
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO RtfSaasBaseDTO) throws ServletException, IOException {

	}

	/**
	 * Process request.Method to update the Migration Status of Customer Rewards
	 *
	 * @param request  the HttpServletRequest
	 * @param response the HttpServletResponse
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		GenRewardDTO dto = new GenRewardDTO();
		dto.setSts(0);

		String clientIdStr = request.getParameter("clId");
		String rwdtype = request.getParameter("rwdtype");
		String coId = request.getParameter("coId");
		String status = request.getParameter("status");
		String runNoStr = request.getParameter("runNo");
		Integer conRunNo = null;
		try {

			if (TextUtil.isEmptyOrNull(clientIdStr)) {
				dto.setMsg(Messages.MANDATORY_PARAM_CLIENT_ID);
				generateResponse(request, response, dto);
				return;
			}
			if (TextUtil.isEmptyOrNull(rwdtype)) {
				dto.setMsg(Messages.MANDATORY_PARAM_REWARD_TYPE);
				generateResponse(request, response, dto);
				return;
			}
			if (TextUtil.isEmptyOrNull(coId)) {
				dto.setMsg(Messages.MANDATORY_PARAM_CONTEST_ID);
				generateResponse(request, response, dto);
				return;
			}
			if (TextUtil.isEmptyOrNull(status)) {
				dto.setMsg(Messages.MANDATORY_PARAM_STATUS_CODE);
				generateResponse(request, response, dto);
				return;
			}
			try {
				conRunNo = Integer.parseInt(runNoStr);
			} catch (Exception ex) {
				dto.setMsg(Messages.MANDATORY_PARAM_CONT_RUNNING_NUM);
				generateResponse(request, response, dto);
				return;
			}
			String jobName = Constants.JOB_PREFIX + rwdtype;
			dto.setClId(clientIdStr);
			dto.setRwdty(rwdtype);
			Integer sts = GenericRewardMigrationService.updateMigrationJobNameEndStatus(clientIdStr, coId, jobName,
					status, conRunNo);
			dto.setSts(sts);
			generateResponse(request, response, dto);
			return;

		} catch (Exception e) {
			e.printStackTrace();
			dto.setMsg(Messages.ERROR_GENERAL_MESSAGE);
			generateResponse(request, response, dto);
			return;

		} finally {		
		}

	}

}
