/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.custom.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.rtf.custom.dto.ContestMigrationDtlsDVO;
import com.rtf.saas.exception.RTFDataAccessException;

/**
 * The Class ContestMigrationDtlsDAO.
 */
public class ContestMigrationDtlsDAO {

	/**
	 * Creates the contest migration jobs.
	 *
	 * @param conn     the connection Object
	 * @param clId     the client ID
	 * @param conId    the contest id
	 * @param conRunno the contest run number
	 * @param jobName  the job name
	 * @param status   the status
	 * @return the integer
	 * @throws Exception the exception
	 */
	public static Integer createContestMigrationJobs(Connection conn, String clId, String conId, Integer conRunno,
			String jobName, String status) throws Exception {
		PreparedStatement ps = null;
		String sql = " insert into pm_livvx_contest_migration_dtls(clintid,conid,conrunningno,jobname,status,start_time)"
				+ "values(?,?,?,?,?,getdate()) ";
		try {
			ps = conn.prepareStatement(sql);
			ps.setString(1, clId);
			ps.setString(2, conId);
			ps.setInt(3, conRunno);
			ps.setString(4, jobName);
			ps.setString(5, status);
			int affectedRows = ps.executeUpdate();
			if (affectedRows == 0) {
				throw new SQLException("Creating contest Migration dtls record failed, no rows affected.");
			}
			return affectedRows;
		} catch (SQLException se) {
			se.printStackTrace();
			throw se;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			try {
				ps.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}

	}

	/**
	 * Gets the pending contest migration job names based on status ='PENDING'.
	 *
	 * @param conn     the conn
	 * @param clientId the client id
	 * @param jobName  the job name
	 * @param status   the status
	 * @return the pending contest migration stats by job name
	 * @throws Exception the exception
	 */
	public static ContestMigrationDtlsDVO getPendingContestMigrationStatsByJobName(Connection conn, String clientId,
			String jobName, String status) throws Exception {
		ContestMigrationDtlsDVO contMigStats = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sql = "SELECT clintid,conid,conrunningno,jobname,cass_count,sql_count,status,start_time,end_time "
				+ "from  pm_livvx_contest_migration_dtls  WHERE clintid = ? AND jobname = ? and status = ? order by start_time  ";
		try {
			ps = conn.prepareStatement(sql);
			ps.setString(1, clientId);
			ps.setString(2, jobName);
			ps.setString(3, status);
			rs = ps.executeQuery();
			while (rs.next()) {
				contMigStats = new ContestMigrationDtlsDVO();
				contMigStats.setCoId(rs.getString("conid"));
				contMigStats.setClId(rs.getString("clintid"));
				contMigStats.setContRunningNo(rs.getInt("conrunningno"));
				contMigStats.setJobName(rs.getString("jobname"));
				contMigStats.setStatus(rs.getString("status"));
				contMigStats.setCaasDataCount(rs.getInt("cass_count"));
				contMigStats.setSqlDataCount(rs.getInt("sql_count"));
				contMigStats.setStartTime(rs.getTimestamp("start_time"));
				contMigStats.setEndDate(rs.getTimestamp("end_time"));
			}
		} catch (SQLException se) {
			se.printStackTrace();
			throw se;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			try {
				rs.close();
				ps.close();
				// DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return contMigStats;
	}

	/**
	 * Gets the pending contest migration jobs.
	 *
	 * @param conn          the conn
	 * @param clientId      the client id
	 * @param jobName       the job name
	 * @param status        the status
	 * @param contestId     the contest id
	 * @param contRunningNo the cont running no
	 * @return the pending contest migration stats by job name
	 * @throws Exception the exception
	 */
	public static ContestMigrationDtlsDVO getPendingContestMigrationStatsByJobName(Connection conn, String clientId,
			String jobName, String status, String contestId, Integer contRunningNo) throws Exception {
		ContestMigrationDtlsDVO contMigStats = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sql = "SELECT clintid,conid,conrunningno,jobname,cass_count,sql_count,status,start_time,end_time "
				+ "from  pm_livvx_contest_migration_dtls  WHERE clintid = ? AND jobname = ? and status = ? and conid=? and conrunningno=? order by start_time  ";
		try {
			ps = conn.prepareStatement(sql);
			ps.setString(1, clientId);
			ps.setString(2, jobName);
			ps.setString(3, status);
			ps.setString(4, contestId);
			ps.setInt(5, contRunningNo);
			rs = ps.executeQuery();
			while (rs.next()) {
				contMigStats = new ContestMigrationDtlsDVO();
				contMigStats.setCoId(rs.getString("conid"));
				contMigStats.setClId(rs.getString("clintid"));
				contMigStats.setContRunningNo(rs.getInt("conrunningno"));
				contMigStats.setJobName(rs.getString("jobname"));
				contMigStats.setStatus(rs.getString("status"));
				contMigStats.setCaasDataCount(rs.getInt("cass_count"));
				contMigStats.setSqlDataCount(rs.getInt("sql_count"));
				contMigStats.setStartTime(rs.getTimestamp("start_time"));
				contMigStats.setEndDate(rs.getTimestamp("end_time"));
			}
		} catch (SQLException se) {
			se.printStackTrace();
			throw se;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			try {
				rs.close();
				ps.close();
				// DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return contMigStats;
	}

	/**
	 * Update contest migration end status.
	 *
	 * @param conn     the conn
	 * @param clId     the client ID
	 * @param conId    the con id
	 * @param jobName  the job name
	 * @param status   the status
	 * @param conRunno the con runno
	 * @return the integer
	 */
	public static Integer updateContestMigrationEndStatus(Connection conn, String clId, String conId, String jobName,
			String status, Integer conRunno) throws Exception {
		Integer affectedRows = null;
		PreparedStatement statement = null;

		String sql = " update pm_livvx_contest_migration_dtls set status=?, end_time=getdate() "
				+ " where clintid=? and conid=? and conrunningno = ? and jobname=? ";
		try {
			// conn = DatabaseConnections.getRtfSaasConnection();
			statement = conn.prepareStatement(sql);
			statement.setString(1, status);
			statement.setString(2, clId);
			statement.setString(3, conId);
			statement.setInt(4, conRunno);
			statement.setString(5, jobName);
			affectedRows = statement.executeUpdate();
			if (affectedRows == 0) {
				throw new SQLException("updating contest Migration dtls record failed, no rows affected.");
			}
		} catch (SQLException se) {
			se.printStackTrace();
			throw new RTFDataAccessException("updating contest Migration dtls record failed, no rows affected.", se);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RTFDataAccessException("updating contest Migration dtls record failed, no rows affected.", e);
		} finally {
			try {
				statement.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return affectedRows;

	}

	/**
	 * Update contest migration status.
	 *
	 * @param conn     the conn
	 * @param clId     the client ID
	 * @param conId    the con id
	 * @param jobName  the job name
	 * @param status   the status
	 * @param conRunno the con runno
	 * @return the integer
	 */
	public static Integer updateContestMigrationStatus(Connection conn, String clId, String conId, String jobName,
			String status, Integer conRunno) throws Exception {
		Integer affectedRows = null;
		PreparedStatement statement = null;
		String sql = " update pm_livvx_contest_migration_dtls set status=?, start_time=getdate() "
				+ " where clintid=? and conid=? and conrunningno = ? and jobname=? ";
		try {
			// conn = DatabaseConnections.getRtfSaasConnection();
			statement = conn.prepareStatement(sql);
			statement.setString(1, status);
			statement.setString(2, clId);
			statement.setString(3, conId);
			statement.setInt(4, conRunno);
			statement.setString(5, jobName);
			affectedRows = statement.executeUpdate();
			if (affectedRows == 0) {
				throw new SQLException("updating contest Migration dtls record failed, no rows affected.");
			}
		} catch (SQLException se) {
			se.printStackTrace();
			throw new RTFDataAccessException("Error Updating Contest Migration status", se);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RTFDataAccessException("Error Updating Contest Migration status", e);
		} finally {
			try {
				statement.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return affectedRows;

	}
}
