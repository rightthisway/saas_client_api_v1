/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.custom.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.rtf.custom.dvo.GenRewardDVO;

/**
 * The Class CustomerContestRewardStatsDAO.
 */
public class CustomerContestRewardStatsDAO {

	/**
	 * Fetch customer rewards by Reward type for live contest.
	 *
	 * @param conn     the conn
	 * @param coId     the co id
	 * @param conRunNo the con run no
* @param clId     the client ID
	 * @param rwdType  the rwd type
	 * @return the list
	 * @throws Exception the exception
	 */
	public static List<GenRewardDVO> fetchCustomerRewardsByTypeForLiveContest(Connection conn, String coId,
			Integer conRunNo, String clId, String rwdType) throws Exception {
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<GenRewardDVO> lst = null;
		String sql = " SELECT custid,reward_type, (live_rewards-prev_live_rwds) as rwdval "
				+ " from  pt_livvx_customer_contest_rwds_stats  WHERE "
				+ " clintid = ? AND conid=? and conrunningno = ? and " + " reward_type = ? ";
		try {
			ps = conn.prepareStatement(sql);
			ps.setString(1, clId);
			ps.setString(2, coId);
			ps.setInt(3, conRunNo);
			ps.setString(4, rwdType);
			rs = ps.executeQuery();
			lst = new ArrayList<GenRewardDVO>();
			while (rs.next()) {
				GenRewardDVO dvo = new GenRewardDVO();
				dvo.setCuId(rs.getString("custid"));
				dvo.setRwdtyp(rs.getString("reward_type"));
				dvo.setRwdval(rs.getString("rwdval"));
				lst.add(dvo);
			}
			return lst;
		} catch (SQLException se) {
			se.printStackTrace();
			throw se;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			try {
				rs.close();
				ps.close();
				// DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}

}
