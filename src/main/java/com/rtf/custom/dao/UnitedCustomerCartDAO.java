/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.custom.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.datastax.driver.core.Row;
import com.datastax.driver.core.exceptions.DriverException;
import com.rtf.common.util.DatabaseConnections;
import com.rtf.common.util.StatusEnums;
import com.rtf.custom.dvo.UnitedCartDVO;
import com.rtf.saas.db.CassandraConnector;
import com.rtf.saas.exception.RTFDataAccessException;
import com.rtf.saas.util.GenUtil;


/**
 * The Class EcomCustomerCartDAO.
 */
public class UnitedCustomerCartDAO {

	/**
	 * Delete item in Customers cart.
	 *
	 * @param customerId the customer Id
	 * @param clientId   the Client Id
	 * @param prodId     the Product Id
	 * @return the integer
	 * @throws Exception the exception
	 */
	public static Integer deleteItemInCart(String customerId, String clientId, String prodId) throws Exception {
		Connection conn = null;
		Integer affectedRows = 0;
		PreparedStatement ps = null;
		StringBuffer sb = new StringBuffer(" update  pt_shopm_customer_shopping_cart set  cartstatus =?, updby = ? , ");
		sb.append(" upddate=getDate()  where clintid =  ? ");
		sb.append(" and mercprodid=? and custid = ? ");

		try {
			conn = DatabaseConnections.getRtfSaasConnection();
			ps = conn.prepareStatement(sb.toString());
			ps.setString(1, StatusEnums.DELETED.name());
			ps.setString(2, customerId);
			ps.setString(3, clientId);
			ps.setString(4, prodId);
			ps.setString(5, customerId);
			affectedRows = ps.executeUpdate();
			return affectedRows;
		} catch (SQLException se) {
			se.printStackTrace();
			throw new RTFDataAccessException("Deleting Cart Item Failed ", se);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RTFDataAccessException("Deleting Cart Item Failed ", e);
		} finally {
			try {
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}

	/**
	 * Fetch customer cart For United Client.
	 *
	 * @param clientId   the Client ID
	 * @param customerId the Customer ID
	 * @return the list of Customer Cart product details
	 * @throws Exception the exception
	 */
	public static List<UnitedCartDVO> fetchUnitedCustomerCart(String clientId, String customerId) throws Exception {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<UnitedCartDVO> cartList = null;
		StringBuffer sb = new StringBuffer(" select a.clintid,a.mercid,a.custid,a.mercprodid,a.conid,a.buyunits, ");
		sb.append(
				"  b.pname,b.psel_min_prc,b.preg_min_prc,b.pimg, b.price_type as priceType, b.pprc_dtl as pprc_dtl from ");
		sb.append("  pt_shopm_customer_shopping_cart a  join rtf_seller_product_items_mstr b on  ");
		sb.append("  a.clintid=b.clintid and a.mercid=b.sler_id and  a.mercprodid=b.slrpitms_id  ");
		sb.append("  where a.cartstatus='ACTIVE'   AND a.clintid=? and a.custid=?   ");
		try {
			conn = DatabaseConnections.getRtfSaasConnection();
			ps = conn.prepareStatement(sb.toString());
			ps.setString(1, clientId);
			ps.setString(2, customerId);
			rs = ps.executeQuery();
			cartList = new ArrayList<UnitedCartDVO>();
			while (rs.next()) {
				UnitedCartDVO cdvo = new UnitedCartDVO();
				cdvo.setMercId(rs.getInt("mercid"));
				cdvo.setCustId(rs.getString("custid"));
				cdvo.setProdid(rs.getString("mercprodid"));
				cdvo.setQty(rs.getInt("buyunits"));
				cdvo.setProdName(rs.getString("pname"));
				cdvo.setRegPrc(rs.getDouble("preg_min_prc"));
				cdvo.setSellPrc(rs.getDouble("psel_min_prc"));
				cdvo.setProdImgUrl(rs.getString("pimg"));
				cdvo.setProdPriceDets(rs.getString("pprc_dtl"));
				cdvo.setPrcType(rs.getString("priceType"));
				cartList.add(cdvo);
			}
			return cartList;
		} catch (SQLException se) {
			se.printStackTrace();
			throw new RTFDataAccessException("Error Fetching cart details ", se);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RTFDataAccessException("Error Fetching cart details ", e);
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}
	
	
	public static List<UnitedCartDVO> fetchUnitedCassCustomerCart(String clientId, String customerId) throws Exception {
		
				com.datastax.driver.core.ResultSet resultSet = null;
				List<UnitedCartDVO> cartList =new ArrayList<UnitedCartDVO> ();		
			try {
				resultSet = CassandraConnector.getSession().execute(
						"SELECT * from pt_shopm_customer_shopping_cart  WHERE clintid = ?  and  custid = ?  ", clientId,  customerId);
				if (resultSet != null) {				
					for (Row rs : resultSet) {
						UnitedCartDVO cdvo = new UnitedCartDVO();
						cdvo.setMercId(rs.getInt("sler_id"));
						cdvo.setCustId(rs.getString("custid"));
						cdvo.setProdid(rs.getString("itmsid"));
						cdvo.setQty(1);
						cdvo.setProdName(rs.getString("pname"));
						cdvo.setRegPrc(rs.getDouble("preg_min_prc"));
						cdvo.setSellPrc(rs.getDouble("psel_min_prc"));
						cdvo.setProdImgUrl(rs.getString("pimg"));
						cdvo.setProdPriceDets(rs.getString("pprc_dtl"));
						cdvo.setPrcType(rs.getString("price_type"));					
						cartList.add(cdvo);
					}
				}
				return cartList;
			} catch (final DriverException de) {
				de.printStackTrace();
				throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
			} catch (Exception ex) {
				ex.printStackTrace();
				throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
			} finally {
			}
		}
	
	public static Integer deleteUnitedCassItemInCart(String customerId, String clientId, String prodId) throws Exception {
		Integer sts = 0;
		try {
			CassandraConnector.getSession().execute(
					"delete from  pt_shopm_customer_shopping_cart where  clintid = ?  and  custid = ?  and itmsid=? ",
					clientId, customerId, prodId);
			sts = 1;
		} catch (final DriverException de) {
			de.printStackTrace();
			//throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			//throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		}
		return sts;
	}
	

}
