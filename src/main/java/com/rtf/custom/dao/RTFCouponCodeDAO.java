/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.custom.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import com.rtf.common.util.DatabaseConnections;
import com.rtf.custom.dvo.RTFCustCouponCodeDVO;
import com.rtf.saas.exception.RTFDataAccessException;

/**
 * The Class RTFCouponCodeDAO.
 */
public class RTFCouponCodeDAO {

	/**
	 * Creates the coupon code.
	 *
	 * @param dvo  the dvo
	 * @param conn the conn
	 * @return the RTF cust coupon code DVO
	 * @throws Exception the exception
	 */
	public static RTFCustCouponCodeDVO createCouponCode(RTFCustCouponCodeDVO dvo, Connection conn) throws Exception {

		PreparedStatement ps = null;
		String sql = " insert into  rtf_cust_coupon_code_mstr ( clintid ,conid ,custid,dicount ,couponcode ,qsnid"
				+ " ,winnertype ,credate,conrunningno ) " + " values(?,?,?,?,?,?,?,getdate(),?) ";

		try {
			ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, dvo.getClId());
			ps.setString(2, dvo.getCoId());
			ps.setString(3, dvo.getCuId());
			ps.setBigDecimal(4, dvo.getDisc());
			ps.setString(5, dvo.getCpncde());
			ps.setInt(6, dvo.getqId());
			ps.setString(7, dvo.getWintyp());
			ps.setInt(8, dvo.getRunNo());
			int affectedRows = ps.executeUpdate();
			if (affectedRows == 0) {
				throw new SQLException(
						"Creating Coupon Code record failed for Cust Id " + dvo.getCuId() + "  no rows affected.");
			}
			ResultSet generatedKeys;
			try {
				generatedKeys = ps.getGeneratedKeys();
				if (generatedKeys.next()) {
					dvo.setCccId(generatedKeys.getInt(1));
				} else {
					throw new SQLException(
							"Creating Coupon Code record failed for Cust Id " + dvo.getCuId() + " no ID obtained.");
				}
				generatedKeys.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			return dvo;
		} catch (SQLException se) {
			se.printStackTrace();
			throw new RTFDataAccessException("Creating Coupon Code record failed for Cust Id " + dvo.getCuId(), se);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RTFDataAccessException("Creating Coupon Code record failed for Cust Id " + dvo.getCuId(), e);
		} finally {
			try {
				ps.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}

	/**
	 * Fetch all coupon codesfor migration.
	 *
	 * @param dvo           the dvo
	 * @param conn          the conn
	 * @param prevfetchDate the prevfetch date
	 * @param maxDate       the max date
	 * @return the list
	 * @throws Exception the exception
	 */
	public static List<RTFCustCouponCodeDVO> fetchAllCouponCodesforMigration(RTFCustCouponCodeDVO dvo, Connection conn,
			Long prevfetchDate, Long maxDate) throws Exception {
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sql = " select cccId, clintid ,conid,custid ,dicount,couponcode "
				+ " ,qsnid,winnertype, credate ,ismigrated ,migdate from "
				+ " rtf_cust_coupon_code_mstr where  ismigrated = 0 and clintid=? and  credate <= ? ";
		if (prevfetchDate != null) {
			sql = sql + " and credate > ?   ";
		}
		List<RTFCustCouponCodeDVO> dvolist = new ArrayList<RTFCustCouponCodeDVO>();
		try {
			ps = conn.prepareStatement(sql);
			ps.setString(1, dvo.getClId());
			ps.setTimestamp(2, new Timestamp(maxDate));
			if (prevfetchDate != null) {
				ps.setTimestamp(3, new Timestamp(prevfetchDate));
			}
			rs = ps.executeQuery();
			while (rs.next()) {
				Date date = null;
				RTFCustCouponCodeDVO rsdvo = new RTFCustCouponCodeDVO();
				rsdvo.setCccId(rs.getInt("cccId"));
				rsdvo.setClId(rs.getString("clintid"));
				rsdvo.setCoId(rs.getString("conid"));
				rsdvo.setCuId(rs.getString("custid"));
				rsdvo.setDisc(rs.getBigDecimal("dicount"));
				rsdvo.setCpncde(rs.getString("couponcode"));
				rsdvo.setqId(rs.getInt("qsnid"));
				rsdvo.setWintyp(rs.getString("winnertype"));
				date = rs.getTimestamp("credate");
				rsdvo.setCreDate(date != null ? date.getTime() : null);
				rsdvo.setIsMig(rs.getBoolean("ismigrated"));
				date = rs.getTimestamp("migdate");
				rsdvo.setMigdte(date != null ? date.getTime() : null);
				dvolist.add(rsdvo);
			}
			return dvolist;
		} catch (SQLException se) {
			se.printStackTrace();
			throw se;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			try {
				rs.close();
				ps.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}

	/**
	 * Fetch all coupon codesfor migrationtest.
	 *
	 * @param dvo           the dvo
	 * @param conn          the conn
	 * @param prevfetchDate the prevfetch date
	 * @param maxDate       the max date
	 * @return the list
	 * @throws Exception the exception
	 */
	public static List<RTFCustCouponCodeDVO> fetchAllCouponCodesforMigrationtest(RTFCustCouponCodeDVO dvo,
			Connection conn, Long prevfetchDate, Long maxDate) throws Exception {
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sql = " select cccId, clintid ,conid,custid ,dicount,couponcode "
				+ " ,qsnid,winnertype, credate ,ismigrated ,migdate from "
				+ " rtf_cust_coupon_code_mstr order by credate  desc ";
		List<RTFCustCouponCodeDVO> dvolist = new ArrayList<RTFCustCouponCodeDVO>();
		try {
			conn = DatabaseConnections.getRtfSaasConnection();
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				Date date = null;
				RTFCustCouponCodeDVO rsdvo = new RTFCustCouponCodeDVO();
				rsdvo.setCccId(rs.getInt("cccId"));
				rsdvo.setClId(rs.getString("clintid"));
				rsdvo.setCoId(rs.getString("conid"));
				rsdvo.setCuId(rs.getString("custid"));
				rsdvo.setDisc(rs.getBigDecimal("dicount"));
				rsdvo.setCpncde(rs.getString("couponcode"));
				rsdvo.setqId(rs.getInt("qsnid"));
				rsdvo.setWintyp(rs.getString("winnertype"));
				date = rs.getTimestamp("credate");
				rsdvo.setCreDate(date != null ? date.getTime() : null);
				rsdvo.setIsMig(rs.getBoolean("ismigrated"));
				date = rs.getTimestamp("migdate");
				rsdvo.setMigdte(date != null ? date.getTime() : null);
				dvolist.add(rsdvo);
			}
			return dvolist;
		} catch (SQLException se) {
			se.printStackTrace();
			throw se;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			try {
				rs.close();
				ps.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}

	/**
	 * Fetch coupon codes given to customer by contest.
	 *
	 * @param dvo  the dvo
	 * @param conn the conn
	 * @return the RTF cust coupon code DVO
	 * @throws Exception the exception
	 */
	public static RTFCustCouponCodeDVO fetchCouponCodesGivenToCustomerByContest(RTFCustCouponCodeDVO dvo,
			Connection conn) throws Exception {
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sql = "  select cccId,clintid,conid,custid from   "
				+ "  rtf_cust_coupon_code_mstr with(nolock)  where  clintid=? and conid=? and custid=? and winnertype=? ";
		RTFCustCouponCodeDVO rsdvo = null;
		try {
			ps = conn.prepareStatement(sql);
			ps.setString(1, dvo.getClId());
			ps.setString(2, dvo.getCoId());
			ps.setString(3, dvo.getCuId());
			ps.setString(4, dvo.getWintyp());
			rs = ps.executeQuery();
			while (rs.next()) {
				rsdvo = new RTFCustCouponCodeDVO();
				rsdvo.setCccId(rs.getInt("cccId"));
				rsdvo.setClId(rs.getString("clintid"));
				rsdvo.setCoId(rs.getString("conid"));
				rsdvo.setCuId(rs.getString("custid"));
			}
			return rsdvo;
		} catch (SQLException se) {
			se.printStackTrace();
			throw se;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			try {
				rs.close();
				ps.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}

	/**
	 * Fetch max record date.
	 *
	 * @param conn the conn
	 * @return the long
	 * @throws Exception the exception
	 */
	public static Long fetchMaxRecordDate(Connection conn) throws Exception {
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sql = " select max(credate) as credate from " + " rtf_cust_coupon_code_mstr where  ismigrated = 0 ";
		try {
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
			Date date = null;
			while (rs.next()) {
				date = rs.getTimestamp("credate");
			}
			return date != null ? date.getTime() : null;
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

	/**
	 * Gets the customedate.
	 *
	 * @return the customedate
	 */
	public static Long getCustomedate() {
		Calendar c = new GregorianCalendar();
		c.add(Calendar.DATE, -1);
		c.set(Calendar.HOUR_OF_DAY, 14);
		c.set(Calendar.MINUTE, 23);
		c.set(Calendar.SECOND, 43);
		return c.getTimeInMillis();
	}

	/**
	 * Gets the todaystartof day.
	 *
	 * @return the todaystartof day
	 */
	public static Date getTodaystartofDay() {
		Calendar c = new GregorianCalendar();
		c.set(Calendar.HOUR_OF_DAY, 14);
		c.set(Calendar.MINUTE, 23);
		c.set(Calendar.SECOND, 43);
		Date d1 = c.getTime();
		return d1;
	}

	/**
	 * Update coupon codes for customer.
	 *
	 * @param conn    the conn
	 * @param dvo     the dvo
	 * @param clintId the clint id
	 * @return the integer
	 * @throws Exception the exception
	 */
	public static Integer updateCouponCodesForCustomer(Connection conn, RTFCustCouponCodeDVO dvo, String clintId)
			throws Exception {
		String sql = " update rtf_cust_coupon_code_mstr set couponcode=? ,qsnid = ?  "
				+ " where  clintid=? and conid=? and custid=? and winnertype=? ";

		Integer updCnt = 0;
		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement(sql);
			ps.setString(1, dvo.getCpncde());
			ps.setInt(2, dvo.getqId());
			ps.setString(3, clintId);
			ps.setString(4, dvo.getCoId());
			ps.setString(5, dvo.getCuId());
			ps.setString(6, dvo.getWintyp());
			updCnt = ps.executeUpdate();
			return updCnt;
		} catch (SQLException se) {
			se.printStackTrace();
			throw se;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			try {
				ps.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}

	}

	/**
	 * Update migrationm stats for coupon codes.
	 *
	 * @param conn          the conn
	 * @param prevfetchDate the prevfetch date
	 * @param maxDate       the max date
	 * @param clintId       the clint id
	 * @return the integer
	 * @throws Exception the exception
	 */
	public static Integer updateMigrationmStatsForCouponCodes(Connection conn, Long prevfetchDate, Long maxDate,
			String clintId) throws Exception {
		String sql = "update rtf_cust_coupon_code_mstr set ismigrated =1, migdate= getdate() where ismigrated =0 AND "
				+ " clintid=? and  credate <= ? ";
		if (prevfetchDate != null) {
			sql = sql + " and credate > ?   ";
		}
		Integer updCnt = 0;
		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement(sql);
			ps.setString(1, clintId);
			ps.setTimestamp(2, new Timestamp(maxDate));
			if (prevfetchDate != null) {
				ps.setTimestamp(3, new Timestamp(prevfetchDate));
			}
			updCnt = ps.executeUpdate();
			return updCnt;

		} catch (SQLException se) {
			se.printStackTrace();
			throw se;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			try {
				ps.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}

	}

}
