/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.custom.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * The Class CommonSQLDAOUtil.
 */
public class CommonSQLDAOUtil {

	/**
	 * Gets the contest questions rewards by contest id and Client Id.
	 *
	 * @param conn      the conn
	 * @param clientId  the client id
	 * @param contestId the contest id
	 * @return the contest questions rewards by contest id
	 * @throws Exception the exception
	 */
	public static List<String> getcontestquestionsRewardsBycontestId(Connection conn, String clientId, String contestId)
			throws Exception {

		String sql = "SELECT distinct(jackpottype) as jackpottype FROM pa_livvx_contest_question where  clintid=? and conid =?";
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<String> list = new ArrayList<String>();
		try {
			ps = conn.prepareStatement(sql);
			ps.setString(1, clientId);
			ps.setString(2, contestId);
			rs = ps.executeQuery();
			while (rs.next()) {
				list.add(rs.getString("jackpottype"));
			}
		} catch (SQLException se) {
			se.printStackTrace();
			throw se;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			try {
				rs.close();
				ps.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return list;
	}

}
