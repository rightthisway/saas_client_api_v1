/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.custom.service;

import java.util.List;

import com.rtf.custom.dao.UnitedCustomerCartDAO;
import com.rtf.custom.dvo.UnitedCartDVO;
import com.rtf.saas.exception.SaasProcessException;

/**
 * The Class RTFCouponCodeService.
 */
public class UnitedSaaSService {
	/**
	 * Fetch all products in customers cart For United Cart.
	 *
	 * @param dto the EcomCartDTO
	 * @return the EcomCartDTO cart DTO
	 * @throws Exception the exception
	 */
	public static List<UnitedCartDVO> fetchUnitedCustCartList(String clientId ,String customerId) throws Exception {
		List<UnitedCartDVO> lst = null;
		try {
				lst = UnitedCustomerCartDAO.fetchUnitedCassCustomerCart(clientId,customerId);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new SaasProcessException("Error Fetching Cart details for Customer [ " + ex.getLocalizedMessage() + " ] ");
		}
		return lst;
	}
	/**
	 * Delete cart Item For United Client.
	 * @param ids  the product ids
	 * @param cuId the Customer id
	 * @param clId the Client ID
	 * @return the boolean
	 * @throws Exception the exception
	 */
	public static Integer deleteCartItem( String customerId, String clientId , String prodId) throws Exception {	
		
		try {			
			return  UnitedCustomerCartDAO.deleteUnitedCassItemInCart(customerId,  clientId,  prodId);		
		} catch (Exception e) {
			e.printStackTrace();
			throw new SaasProcessException("Error Deleting Cart Item ", e);
		}	
	}


}
