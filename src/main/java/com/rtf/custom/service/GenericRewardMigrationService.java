/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.custom.service;

import java.sql.Connection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import com.rtf.common.util.DatabaseConnections;
import com.rtf.custom.dao.CommonSQLDAOUtil;
import com.rtf.custom.dao.ContestMigrationDtlsDAO;
import com.rtf.custom.dao.CustomerContestRewardStatsDAO;
import com.rtf.custom.dto.ContestMigrationDtlsDVO;
import com.rtf.custom.dto.GenRewardDTO;
import com.rtf.custom.dvo.GenRewardDVO;
import com.rtf.custom.utils.Constants;
import com.rtf.livt.dvo.ContestDVO;
import com.rtf.livt.util.LivtContestUtil;
import com.rtf.saas.exception.RTFDataAccessException;
import com.rtf.saas.exception.SaasProcessException;
import com.rtf.saas.util.GenUtil;

/**
 * The Class GenericRewardMigrationService.
 */
public class GenericRewardMigrationService {

	/**
	 * Creates the reward migration job names on end contest.
	 *
	 * @param clId the cl id
	 * @param coId the co id
	 * @throws Exception the exception
	 */
	public static void createRewardMigrationJobNamesOnEndContest(String clId, String coId) throws Exception {
		ContestDVO contest = LivtContestUtil.getCurrentContestByContestId(clId, coId);
		if (contest == null || contest.getCoId().isEmpty()) {
			throw new SaasProcessException("No Contest for id " + coId);
		}
		Integer runNo = contest.getContRunningNo();

		String queRwdType = contest.getQueRwdType();
		String particpantRwdType = contest.getPartiRwdType();
		String summaryRwdType = contest.getSumRwdType();
		String winnerRewardType = contest.getWinRwdType();
		String contestType = contest.getCoType();

		Set<String> rwdTypeSet = new HashSet<String>();
		rwdTypeSet.add(particpantRwdType);
		rwdTypeSet.add(summaryRwdType);
		rwdTypeSet.add(winnerRewardType);
		rwdTypeSet.add(queRwdType);
		Connection conn = DatabaseConnections.getRtfSaasConnection();

		if (Constants.CONTEST_MEGA.equalsIgnoreCase(contestType)
				|| Constants.CONTEST_MINI.equalsIgnoreCase(contestType)) {
			List<String> rwdTypeList = CommonSQLDAOUtil.getcontestquestionsRewardsBycontestId(conn, clId, coId);
			for (String rwdtype : rwdTypeList) {
				rwdTypeSet.add(rwdtype);
			}
		}
		Iterator<String> itr = rwdTypeSet.iterator();
		while (itr.hasNext()) {
			String rwdType = itr.next();
			String JOB_NAME = Constants.JOB_PREFIX + rwdType;
			String status = Constants.MIGRATION_STATUS_PENDING;
			Integer creCnt = ContestMigrationDtlsDAO.createContestMigrationJobs(conn, clId, coId, runNo, JOB_NAME,
					status);
			if (creCnt != 1) {
				throw new SaasProcessException(
						"ERROR Creating JOB NAME [" + JOB_NAME + " ] for Client [ " + clId + " ] ");
			}

		}
	}

	/**
	 * Fetch contest cust rewardsfor client data migration.
	 *
	 * @param dto the dto
	 * @return the gen reward DTO
	 * @throws Exception the exception
	 */
	public static GenRewardDTO fetchContestCustRewardsforClientDataMigration(GenRewardDTO dto) throws Exception {
		String clId = dto.getClId();
		String rwdTyp = dto.getRwdty();
		String jobName = Constants.JOB_PREFIX + rwdTyp;
		Connection conn = null;
		try {
			conn = DatabaseConnections.getRtfSaasConnection();
			ContestMigrationDtlsDVO migdetsdvo = ContestMigrationDtlsDAO.getPendingContestMigrationStatsByJobName(conn,
					clId, jobName, Constants.MIGRATION_STATUS_PENDING);
			if (migdetsdvo == null || !(migdetsdvo.getJobName().equals(jobName))) {
				dto.setSts(0);
				dto.setMsg(" NO PENDING JOBS FOR REWARDTYPE " + rwdTyp + " CLIENTID " + clId);
				return dto;
			}
			String coId = migdetsdvo.getCoId();
			Integer conRunNo = migdetsdvo.getContRunningNo();
			List<GenRewardDVO> lst = fetchcustomerRewardDetails(conn, rwdTyp, clId, coId, conRunNo);
			ContestMigrationDtlsDAO.updateContestMigrationStatus(conn, clId, coId, jobName,
					Constants.MIGRATION_STATUS_PROVIDED, conRunNo);
			dto.setLst(lst);
			dto.setSts(1);
			dto.setCoId(coId);
			dto.setConrunno(conRunNo);
			dto.setMsg(
					"Success: Customer Reward List size for reward type [ " + rwdTyp + " ] is  [" + lst.size() + " ]");
			return dto;
		} catch (Exception ex) {
			ex.printStackTrace();
			dto.setSts(0);
			dto.setMsg("Failed: Customer Reward List for " + ex.getLocalizedMessage());
		} finally {
			conn.close();
		}
		return dto;
	}

	/**
	 * Fetchcustomer reward details.
	 *
	 * @param conn     the conn
	 * @param rwdTyp   the rwd typ
* @param clId     the client ID
	 * @param coId     the co id
	 * @param conRunNo the con run no
	 * @return the list
	 * @throws Exception the exception
	 */
	public static List<GenRewardDVO> fetchcustomerRewardDetails(Connection conn, String rwdTyp, String clId,
			String coId, Integer conRunNo) throws Exception {
		List<GenRewardDVO> lst = CustomerContestRewardStatsDAO.fetchCustomerRewardsByTypeForLiveContest(conn, coId,
				conRunNo, clId, rwdTyp);
		return lst;

	}

	/**
	 * Update migration job name end status.
	 *
* @param clId     the client ID
	 * @param coId     the co id
	 * @param jobName  the job name
	 * @param status   the status
	 * @param conRunNo the con run no
	 * @return the integer
	 * @throws Exception the exception
	 */
	public static Integer updateMigrationJobNameEndStatus(String clId, String coId, String jobName, String status,
			Integer conRunNo) throws Exception {

		Integer cnt = 0;
		try {
			Connection conn = DatabaseConnections.getRtfSaasConnection();
			cnt = ContestMigrationDtlsDAO.updateContestMigrationEndStatus(conn, clId, coId, jobName, status, conRunNo);
			return cnt;
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		}

	}

}
