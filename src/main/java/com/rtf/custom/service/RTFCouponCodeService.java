/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.custom.service;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.rtf.common.util.CommonCacheUtil;
import com.rtf.common.util.DatabaseConnections;
import com.rtf.custom.dao.RTFCouponCodeDAO;
import com.rtf.custom.dto.RTFCouponCodeDTO;
import com.rtf.custom.dvo.RTFCustCouponCodeDVO;
import com.rtf.saas.exception.RTFDataAccessException;
import com.rtf.saas.exception.SaasProcessException;
import com.rtf.saas.util.GenUtil;
import com.rtf.shop.ecom.dvo.GenericResp;
import com.rtf.shop.ecom.util.GsonCustomConfig;
import com.rtf.shop.ecom.util.HTTPUtil;

/**
 * The Class RTFCouponCodeService.
 */
public class RTFCouponCodeService {

	/**
	 * Adds the coupon code to RTF.
	 *
	 * @param dvo the dvo
	 * @return the integer
	 * @throws Exception the exception
	 */
	private static Integer addCouponCodeToRTF(RTFCustCouponCodeDVO dvo) throws Exception {
		String data = null;
		String RTFAPI_BASEURL = CommonCacheUtil.clientcustommap.get("rtfcartbaseapiurl");
		if (GenUtil.isEmptyString(RTFAPI_BASEURL)) {
			throw new SaasProcessException("RTF BASE API URL NOT INITIALIZED ");
		}
		String RTFAPI_API_URL = RTFAPI_BASEURL + "SaveCustCode";
		// "http://34.202.149.190/sandbox/deleteCart";
		GenericResp dto = null;
		try {
			Map<String, String> dataMap = new HashMap<String, String>();
			dataMap.put("custId", dvo.getCuId());
			dataMap.put("code", dvo.getCpncde());
			dataMap.put("coId", dvo.getCoId());
			dataMap.put("qNo", String.valueOf(dvo.getqNo()));
			data = HTTPUtil.execute(dataMap, RTFAPI_API_URL);
			if (data == null)
				return 0;
			Gson gson = GsonCustomConfig.getGsonBuilder();
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			dto = gson.fromJson(((JsonObject) jsonObject.get("genericResp")), GenericResp.class);
			if (dto == null)
				return 0;
			return dto.getStatus();
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * Creates the coupon code.
	 *
	 * @param dto the dto
	 * @return the RTF coupon code DTO
	 * @throws Exception the exception
	 */
	public static RTFCouponCodeDTO createCouponCode(RTFCouponCodeDTO dto) throws Exception {
		try {
			Connection conn = DatabaseConnections.getRtfSaasConnection();
			RTFCustCouponCodeDVO dvo = RTFCouponCodeDAO.createCouponCode(dto.getDvo(), conn);
			if (dvo.getCccId() != null) {
				dto.setSts(1);
			}
			dto.setDvo(dvo);
			try {
				addCouponCodeToRTF(dvo);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			return dto;
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		}
	}

	/**
	 * Creates the or update coupon code.
	 *
	 * @param dvo the dvo
	 * @return the integer
	 * @throws Exception the exception
	 */
	public static Integer createOrUpdateCouponCode(RTFCustCouponCodeDVO dvo) throws Exception {
		Integer cpnCodeallocated = 0;
		Connection conn = null;
		try {

			conn = DatabaseConnections.getRtfSaasConnection();
			/*RTFCustCouponCodeDVO dvoalloted = RTFCouponCodeDAO.fetchCouponCodesGivenToCustomerByContest(dvo, conn);
			if (dvoalloted != null) {
				try {
					dvoalloted.setqId(dvo.getqId());
					dvoalloted.setCpncde(dvo.getCpncde());
					RTFCouponCodeDAO.updateCouponCodesForCustomer(conn, dvo, dvo.getClId());
					addCouponCodeToRTF(dvo);
					cpnCodeallocated = 1;
				} catch (Exception ex) {
					ex.printStackTrace();
					throw ex;
				}
			} else {*/
				try {
					dvo = RTFCouponCodeDAO.createCouponCode(dvo, conn);
					addCouponCodeToRTF(dvo);
					cpnCodeallocated = 1;
				} catch (Exception ex) {
					ex.printStackTrace();
					throw ex;
				}
			//}
			return cpnCodeallocated;
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		} finally {
			DatabaseConnections.closeConnection(conn);
		}
	}

	/**
	 * Fetch coupon codes for migration.
	 *
	 * @param dto the dto
	 * @return the RTF coupon code DTO
	 * @throws Exception the exception
	 */
	public static RTFCouponCodeDTO fetchCouponCodesForMigration(RTFCouponCodeDTO dto) throws Exception {
		RTFCustCouponCodeDVO dvo = dto.getDvo();
		Long prevfetchDate = dto.getPrvftchdte();
		Connection conn = null;
		try {
			conn = DatabaseConnections.getRtfSaasConnection();
			Long maxDate = RTFCouponCodeDAO.fetchMaxRecordDate(conn);
			List<RTFCustCouponCodeDVO> dvolst = new ArrayList<RTFCustCouponCodeDVO>();
			if (maxDate == null) {
				dto.setSts(0);
				dto.setMsg(" Max Date is Null ");
				dto.setDvolst(dvolst);
				return dto;
			}
			dvolst = RTFCouponCodeDAO.fetchAllCouponCodesforMigration(dvo, conn, prevfetchDate, maxDate);
			dto.setDvolst(dvolst);
			dto.setMaxftchdte(maxDate);
			dto.setSts(1);
			return dto;
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		} finally {
			try {
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}

	}

	/**
	 * Update coupon code migration status.
	 *
	 * @param dto the dto
	 * @return the RTF coupon code DTO
	 * @throws Exception the exception
	 */
	public static RTFCouponCodeDTO updateCouponCodeMigrationStatus(RTFCouponCodeDTO dto) throws Exception {
		Connection conn = null;
		try {
			conn = DatabaseConnections.getRtfSaasConnection();
			Integer updCnt = RTFCouponCodeDAO.updateMigrationmStatsForCouponCodes(conn, dto.getPrvftchdte(),
					dto.getMaxftchdte(), dto.getClId());

			if (updCnt == null || updCnt == 0) {
				dto.setSts(0);
				dto.setMsg(" NO COUPON CODE RECORDS MARKED AS MIGRATED BETWEEN DATES  [ "
						+ new Date(dto.getPrvftchdte()) + " ::::: " + new Date(dto.getMaxftchdte()) + " ] ");
				return dto;
			}
			dto.setUpdcnt(updCnt);
			dto.setSts(1);
			return dto;
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		} finally {
			try {
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}

	}

}
