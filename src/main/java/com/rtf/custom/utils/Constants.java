/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.custom.utils;

/**
 * The Class Constants.
 */
public class Constants {

	/** The migration status pending. */
	public static String MIGRATION_STATUS_PENDING = "PENDING";

	/** The migration status provided. */
	public static String MIGRATION_STATUS_PROVIDED = "PROVIDED";

	/** The migration status completed. */
	public static String MIGRATION_STATUS_COMPLETED = "COMPLETED";

	/** The migration status failed. */
	public static String MIGRATION_STATUS_FAILED = "FAILED";

	/** The job prefix. */
	public static String JOB_PREFIX = "CLIENT_RWD_";

	/** The contest mega. */
	public static String CONTEST_MEGA = "MEGA";

	/** The contest mini. */
	public static String CONTEST_MINI = "MINI";

	/** The migration status started. */
	public static String MIGRATION_STATUS_STARTED = "STARTED";

}
