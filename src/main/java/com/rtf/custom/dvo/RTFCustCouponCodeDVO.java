/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.custom.dvo;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * The Class RTFCustCouponCodeDVO.
 */
public class RTFCustCouponCodeDVO implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -8660607733645332453L;

	/** The ccc id. */
	private Integer cccId;

	/** The cl id. */
	private String clId;

	/** The cu id. */
	private String cuId;

	/** The co id. */
	private String coId;

	/** The disc. */
	private BigDecimal disc;

	/** The cpncde. */
	private String cpncde;

	/** The q no. */
	private Integer qNo;

	/** The enckey. */
	private String enckey;

	/** The wintyp. */
	private String wintyp;

	/** The is mig. */
	private Boolean isMig;

	/** The migdte. */
	private Long migdte;

	/** The q id. */
	private Integer qId;

	/** The is crt. */
	private Boolean isCrt = false;

	/** The cre date. */
	private Long creDate;

	/** The run no. */
	private Integer runNo;

	/** The cre datestr. */
	private String creDatestr;

	/**
	 * Gets the ccc id.
	 *
	 * @return the ccc id
	 */
	public Integer getCccId() {
		return cccId;
	}

	/**
	 * Gets the cl id.
	 *
	 * @return the cl id
	 */
	public String getClId() {
		return clId;
	}

	/**
	 * Gets the co id.
	 *
	 * @return the co id
	 */
	public String getCoId() {
		return coId;
	}

	/**
	 * Gets the cpncde.
	 *
	 * @return the cpncde
	 */
	public String getCpncde() {
		return cpncde;
	}

	/**
	 * Gets the cre date.
	 *
	 * @return the cre date
	 */
	public Long getCreDate() {
		return creDate;
	}

	/**
	 * Gets the cu id.
	 *
	 * @return the cu id
	 */
	public String getCuId() {
		return cuId;
	}

	/**
	 * Gets the disc.
	 *
	 * @return the disc
	 */
	public BigDecimal getDisc() {
		return disc;
	}

	/**
	 * Gets the enckey.
	 *
	 * @return the enckey
	 */
	public String getEnckey() {
		return enckey;
	}

	/**
	 * Gets the checks if is crt.
	 *
	 * @return the checks if is crt
	 */
	public Boolean getIsCrt() {
		return isCrt;
	}

	/**
	 * Gets the checks if is mig.
	 *
	 * @return the checks if is mig
	 */
	public Boolean getIsMig() {
		return isMig;
	}

	/**
	 * Gets the migdte.
	 *
	 * @return the migdte
	 */
	public Long getMigdte() {
		return migdte;
	}

	/**
	 * Gets the q id.
	 *
	 * @return the q id
	 */
	public Integer getqId() {
		return qId;
	}

	/**
	 * Gets the q no.
	 *
	 * @return the q no
	 */
	public Integer getqNo() {
		return qNo;
	}

	/**
	 * Gets the run no.
	 *
	 * @return the run no
	 */
	public Integer getRunNo() {
		return runNo;
	}

	/**
	 * Gets the wintyp.
	 *
	 * @return the wintyp
	 */
	public String getWintyp() {
		return wintyp;
	}

	/**
	 * Sets the ccc id.
	 *
	 * @param cccId the new ccc id
	 */
	public void setCccId(Integer cccId) {
		this.cccId = cccId;
	}

	/**
	 * Sets the cl id.
	 *
	 * @param clId the new cl id
	 */
	public void setClId(String clId) {
		this.clId = clId;
	}

	/**
	 * Sets the co id.
	 *
	 * @param coId the new co id
	 */
	public void setCoId(String coId) {
		this.coId = coId;
	}

	/**
	 * Sets the cpncde.
	 *
	 * @param cpncde the new cpncde
	 */
	public void setCpncde(String cpncde) {
		this.cpncde = cpncde;
	}

	/**
	 * Sets the cre date.
	 *
	 * @param creDate the new cre date
	 */
	public void setCreDate(Long creDate) {
		this.creDate = creDate;
	}

	/**
	 * Sets the cu id.
	 *
	 * @param cuId the new cu id
	 */
	public void setCuId(String cuId) {
		this.cuId = cuId;
	}

	/**
	 * Sets the disc.
	 *
	 * @param disc the new disc
	 */
	public void setDisc(BigDecimal disc) {
		this.disc = disc;
	}

	/**
	 * Sets the enckey.
	 *
	 * @param enckey the new enckey
	 */
	public void setEnckey(String enckey) {
		this.enckey = enckey;
	}

	/**
	 * Sets the checks if is crt.
	 *
	 * @param isCrt the new checks if is crt
	 */
	public void setIsCrt(Boolean isCrt) {
		this.isCrt = isCrt;
	}

	/**
	 * Sets the checks if is mig.
	 *
	 * @param isMig the new checks if is mig
	 */
	public void setIsMig(Boolean isMig) {
		this.isMig = isMig;
	}

	/**
	 * Sets the migdte.
	 *
	 * @param migdte the new migdte
	 */
	public void setMigdte(Long migdte) {
		this.migdte = migdte;
	}

	/**
	 * Sets the q id.
	 *
	 * @param qId the new q id
	 */
	public void setqId(Integer qId) {
		this.qId = qId;
	}

	/**
	 * Sets the q no.
	 *
	 * @param qNo the new q no
	 */
	public void setqNo(Integer qNo) {
		this.qNo = qNo;
	}

	/**
	 * Sets the run no.
	 *
	 * @param runNo the new run no
	 */
	public void setRunNo(Integer runNo) {
		this.runNo = runNo;
	}

	/**
	 * Sets the wintyp.
	 *
	 * @param wintyp the new wintyp
	 */
	public void setWintyp(String wintyp) {
		this.wintyp = wintyp;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "RTFCustCouponCodeDVO [cccId=" + cccId + ", clId=" + clId + ", cuId=" + cuId + ", coId=" + coId
				+ ", disc=" + disc + ", cpncde=" + cpncde + ", qNo=" + qNo + ", enckey=" + enckey + ", wintyp=" + wintyp
				+ ", isMig=" + isMig + ", migdte=" + migdte + ", qId=" + qId + ", isCrt=" + isCrt + ", creDate="
				+ creDate + ", creDatestr=" + creDatestr + "]";
	}

}
