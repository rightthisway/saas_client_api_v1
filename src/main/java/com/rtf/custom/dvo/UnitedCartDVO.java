/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.custom.dvo;

import com.rtf.saas.dvo.SaasBaseDVO;

/**
 * The Class EcomCartDVO.
 */
public class UnitedCartDVO extends SaasBaseDVO {

	/** The clint id. */
	private String clId;

	/** The merc id. */
	private Integer mercId;

	/** The cust id. */
	private String custId;

	/** The merc prodid. */
	private String prodid;
	
	/** The buy units. */
	private Integer qty;
	
	/** The prod name. */
	private String prodName;	

	/** The prod img url. */
	private String prodImgUrl;

	/** The prod desc. */
	private String prodDesc;

	/** The reg prc. */
	private Double regPrc;
	
	/** The sell prc. */
	private Double sellPrc;
	
	/** The prod Labels */
	private String prodPriceDets;
	
	/** The product price Type */
	private String prcType;
	

	public String getClId() {
		return clId;
	}

	public void setClId(String clId) {
		this.clId = clId;
	}

	public Integer getMercId() {
		return mercId;
	}

	public void setMercId(Integer mercId) {
		this.mercId = mercId;
	}

	public String getCustId() {
		return custId;
	}

	public void setCustId(String custId) {
		this.custId = custId;
	}

	public String getProdid() {
		return prodid;
	}

	public void setProdid(String prodid) {
		this.prodid = prodid;
	}

	public Integer getQty() {
		return qty;
	}

	public void setQty(Integer qty) {
		this.qty = qty;
	}

	public String getProdName() {
		return prodName;
	}

	public void setProdName(String prodName) {
		this.prodName = prodName;
	}

	public String getProdImgUrl() {
		return prodImgUrl;
	}

	public void setProdImgUrl(String prodImgUrl) {
		this.prodImgUrl = prodImgUrl;
	}

	public String getProdDesc() {
		return prodDesc;
	}

	public void setProdDesc(String prodDesc) {
		this.prodDesc = prodDesc;
	}

	public Double getRegPrc() {
		return regPrc;
	}

	public void setRegPrc(Double regPrc) {
		this.regPrc = regPrc;
	}

	public Double getSellPrc() {
		return sellPrc;
	}

	public void setSellPrc(Double sellPrc) {
		this.sellPrc = sellPrc;
	}	

	public String getProdPriceDets() {
		return prodPriceDets;
	}

	public void setProdPriceDets(String prodPriceDets) {
		this.prodPriceDets = prodPriceDets;
	}

	public String getPrcType() {
		return prcType;
	}

	public void setPrcType(String prcType) {
		this.prcType = prcType;
	}

}
