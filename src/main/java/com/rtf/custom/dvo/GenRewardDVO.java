/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.custom.dvo;

import java.io.Serializable;

/**
 * The Class GenRewardDVO.
 */
public class GenRewardDVO implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 8884339803101722010L;

	/** The cu id. */
	private String cuId;

	/** The rwdtyp. */
	private String rwdtyp;

	/** The rwdval. */
	private String rwdval;

	/**
	 * Gets the cu id.
	 *
	 * @return the cu id
	 */
	public String getCuId() {
		return cuId;
	}

	/**
	 * Gets the rwdtyp.
	 *
	 * @return the rwdtyp
	 */
	public String getRwdtyp() {
		return rwdtyp;
	}

	/**
	 * Gets the rwdval.
	 *
	 * @return the rwdval
	 */
	public String getRwdval() {
		return rwdval;
	}

	/**
	 * Sets the cu id.
	 *
	 * @param cuId the new cu id
	 */
	public void setCuId(String cuId) {
		this.cuId = cuId;
	}

	/**
	 * Sets the rwdtyp.
	 *
	 * @param rwdtyp the new rwdtyp
	 */
	public void setRwdtyp(String rwdtyp) {
		this.rwdtyp = rwdtyp;
	}

	/**
	 * Sets the rwdval.
	 *
	 * @param rwdval the new rwdval
	 */
	public void setRwdval(String rwdval) {
		this.rwdval = rwdval;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "GenRewardDVO [cuId=" + cuId + ", rwdtyp=" + rwdtyp + ", rwdval=" + rwdval + "]";
	}

}
