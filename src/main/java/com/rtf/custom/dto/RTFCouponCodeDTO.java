/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.custom.dto;

import java.util.List;

import com.rtf.custom.dvo.RTFCustCouponCodeDVO;
import com.rtf.saas.arch.RtfSaasBaseDTO;

/**
 * The Class RTFCouponCodeDTO.
 */
public class RTFCouponCodeDTO extends RtfSaasBaseDTO {

	/** The dvo. */
	private RTFCustCouponCodeDVO dvo;

	/** The dvolst. */
	private List<RTFCustCouponCodeDVO> dvolst;

	/** The prvftchdte. */
	private Long prvftchdte;

	/** The maxftchdte. */
	private Long maxftchdte;

	/** The updcnt. */
	private Integer updcnt;

	/**
	 * Gets the dvo.
	 *
	 * @return the dvo
	 */
	public RTFCustCouponCodeDVO getDvo() {
		return dvo;
	}

	/**
	 * Gets the dvolst.
	 *
	 * @return the dvolst
	 */
	public List<RTFCustCouponCodeDVO> getDvolst() {
		return dvolst;
	}

	/**
	 * Gets the maxftchdte.
	 *
	 * @return the maxftchdte
	 */
	public Long getMaxftchdte() {
		return maxftchdte;
	}

	/**
	 * Gets the prvftchdte.
	 *
	 * @return the prvftchdte
	 */
	public Long getPrvftchdte() {
		return prvftchdte;
	}

	/**
	 * Gets the updcnt.
	 *
	 * @return the updcnt
	 */
	public Integer getUpdcnt() {
		return updcnt;
	}

	/**
	 * Sets the dvo.
	 *
	 * @param dvo the new dvo
	 */
	public void setDvo(RTFCustCouponCodeDVO dvo) {
		this.dvo = dvo;
	}

	/**
	 * Sets the dvolst.
	 *
	 * @param dvolst the new dvolst
	 */
	public void setDvolst(List<RTFCustCouponCodeDVO> dvolst) {
		this.dvolst = dvolst;
	}

	/**
	 * Sets the maxftchdte.
	 *
	 * @param maxftchdte the new maxftchdte
	 */
	public void setMaxftchdte(Long maxftchdte) {
		this.maxftchdte = maxftchdte;
	}

	/**
	 * Sets the prvftchdte.
	 *
	 * @param prvftchdte the new prvftchdte
	 */
	public void setPrvftchdte(Long prvftchdte) {
		this.prvftchdte = prvftchdte;
	}

	/**
	 * Sets the updcnt.
	 *
	 * @param updcnt the new updcnt
	 */
	public void setUpdcnt(Integer updcnt) {
		this.updcnt = updcnt;
	}

}
