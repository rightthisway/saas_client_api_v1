/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.custom.dto;

import java.util.List;

import com.rtf.custom.dvo.UnitedCartDVO;
import com.rtf.saas.arch.RtfSaasBaseDTO;

/**
 * The Class UnitedCartDTO.
 */
public class UnitedCartDTO extends RtfSaasBaseDTO {
	
	/** The Cart lst. */
	private List<UnitedCartDVO> custcartlst;
	
	/** The API status */
	private Integer sts;
	
	/** The API status message */
	private String msg;
	
	
	public List<UnitedCartDVO> getCustcartlst() {
		return custcartlst;
	}
	public void setCustcartlst(List<UnitedCartDVO> custcartlst) {
		this.custcartlst = custcartlst;
	}
	public Integer getSts() {
		return sts;
	}
	public void setSts(Integer sts) {
		this.sts = sts;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}

	

}
