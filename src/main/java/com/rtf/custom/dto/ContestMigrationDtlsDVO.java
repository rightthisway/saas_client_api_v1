/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.custom.dto;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * The Class ContestMigrationDtlsDVO.
 */
public class ContestMigrationDtlsDVO implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 7567010625087385911L;

	/** The dt. */
	static SimpleDateFormat dt = new SimpleDateFormat("MM/dd/yy");

	/** The time ft. */
	static SimpleDateFormat timeFt = new SimpleDateFormat("hh:mmaa z");

	/** The cl id. */
	private String clId;

	/** The co id. */
	private String coId;

	/** The job name. */
	private String jobName;

	/** The cont running no. */
	private Integer contRunningNo;

	/** The caas data count. */
	private Integer caasDataCount;

	/** The sql data count. */
	private Integer sqlDataCount;

	/** The start time. */
	private Date startTime;

	/** The end date. */
	private Date endDate;

	/** The status. */
	private String status;

	/**
	 * Gets the caas data count.
	 *
	 * @return the caas data count
	 */
	public Integer getCaasDataCount() {
		if (caasDataCount == null) {
			caasDataCount = 0;
		}
		return caasDataCount;
	}

	/**
	 * Gets the cl id.
	 *
	 * @return the cl id
	 */
	public String getClId() {
		return clId;
	}

	/**
	 * Gets the co id.
	 *
	 * @return the co id
	 */
	public String getCoId() {
		return coId;
	}

	/**
	 * Gets the cont running no.
	 *
	 * @return the cont running no
	 */
	public Integer getContRunningNo() {
		return contRunningNo;
	}

	/**
	 * Gets the end date.
	 *
	 * @return the end date
	 */
	public Date getEndDate() {
		return endDate;
	}

	/**
	 * Gets the job name.
	 *
	 * @return the job name
	 */
	public String getJobName() {
		return jobName;
	}

	/**
	 * Gets the sql data count.
	 *
	 * @return the sql data count
	 */
	public Integer getSqlDataCount() {
		if (sqlDataCount == null) {
			sqlDataCount = 0;
		}
		return sqlDataCount;
	}

	/**
	 * Gets the start time.
	 *
	 * @return the start time
	 */
	public Date getStartTime() {
		return startTime;
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Sets the caas data count.
	 *
	 * @param caasDataCount the new caas data count
	 */
	public void setCaasDataCount(Integer caasDataCount) {
		this.caasDataCount = caasDataCount;
	}

	/**
	 * Sets the cl id.
	 *
	 * @param clId the new cl id
	 */
	public void setClId(String clId) {
		this.clId = clId;
	}

	/**
	 * Sets the co id.
	 *
	 * @param coId the new co id
	 */
	public void setCoId(String coId) {
		this.coId = coId;
	}

	/**
	 * Sets the cont running no.
	 *
	 * @param contRunningNo the new cont running no
	 */
	public void setContRunningNo(Integer contRunningNo) {
		this.contRunningNo = contRunningNo;
	}

	/**
	 * Sets the end date.
	 *
	 * @param endDate the new end date
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	/**
	 * Sets the job name.
	 *
	 * @param jobName the new job name
	 */
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	/**
	 * Sets the sql data count.
	 *
	 * @param sqlDataCount the new sql data count
	 */
	public void setSqlDataCount(Integer sqlDataCount) {
		this.sqlDataCount = sqlDataCount;
	}

	/**
	 * Sets the start time.
	 *
	 * @param startTime the new start time
	 */
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(String status) {
		this.status = status;
	}

}
