/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.custom.dto;

import java.util.List;

import com.rtf.custom.dvo.GenRewardDVO;

/**
 * The Class GenRewardDTO.
 */
public class GenRewardDTO {

	/** The cl id. */
	private String clId;

	/** The rwdty. */
	private String rwdty;

	/** The dvo. */
	private GenRewardDVO dvo;

	/** The lst. */
	private List<GenRewardDVO> lst;

	/** The msg. */
	private String msg;

	/** The sts. */
	private Integer sts;

	/** The co id. */
	private String coId;

	/** The conrunno. */
	private Integer conrunno;

	/**
	 * Gets the cl id.
	 *
	 * @return the cl id
	 */
	public String getClId() {
		return clId;
	}

	/**
	 * Gets the co id.
	 *
	 * @return the co id
	 */
	public String getCoId() {
		return coId;
	}

	/**
	 * Gets the conrunno.
	 *
	 * @return the conrunno
	 */
	public Integer getConrunno() {
		return conrunno;
	}

	/**
	 * Gets the dvo.
	 *
	 * @return the dvo
	 */
	public GenRewardDVO getDvo() {
		return dvo;
	}

	/**
	 * Gets the lst.
	 *
	 * @return the lst
	 */
	public List<GenRewardDVO> getLst() {
		return lst;
	}

	/**
	 * Gets the msg.
	 *
	 * @return the msg
	 */
	public String getMsg() {
		return msg;
	}

	/**
	 * Gets the rwdty.
	 *
	 * @return the rwdty
	 */
	public String getRwdty() {
		return rwdty;
	}

	/**
	 * Gets the sts.
	 *
	 * @return the sts
	 */
	public Integer getSts() {
		return sts;
	}

	/**
	 * Sets the cl id.
	 *
	 * @param clId the new cl id
	 */
	public void setClId(String clId) {
		this.clId = clId;
	}

	/**
	 * Sets the co id.
	 *
	 * @param coId the new co id
	 */
	public void setCoId(String coId) {
		this.coId = coId;
	}

	/**
	 * Sets the conrunno.
	 *
	 * @param conrunno the new conrunno
	 */
	public void setConrunno(Integer conrunno) {
		this.conrunno = conrunno;
	}

	/**
	 * Sets the dvo.
	 *
	 * @param dvo the new dvo
	 */
	public void setDvo(GenRewardDVO dvo) {
		this.dvo = dvo;
	}

	/**
	 * Sets the lst.
	 *
	 * @param lst the new lst
	 */
	public void setLst(List<GenRewardDVO> lst) {
		this.lst = lst;
	}

	/**
	 * Sets the msg.
	 *
	 * @param msg the new msg
	 */
	public void setMsg(String msg) {
		this.msg = msg;
	}

	/**
	 * Sets the rwdty.
	 *
	 * @param rwdty the new rwdty
	 */
	public void setRwdty(String rwdty) {
		this.rwdty = rwdty;
	}

	/**
	 * Sets the sts.
	 *
	 * @param sts the new sts
	 */
	public void setSts(Integer sts) {
		this.sts = sts;
	}

}
