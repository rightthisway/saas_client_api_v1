/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.shop.dto;

import java.util.List;
import java.util.Map;

import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.shop.dvo.CustomerCartDVO;

/**
 * The Class CustomerCartDTO.
 */
public class CustomerCartDTO extends RtfSaasBaseDTO {

	/** The dvo. */
	private CustomerCartDVO dvo;

	/** The lst. */
	private List<CustomerCartDVO> lst;

	/** The allcustcartmap. */
	private Map<String, List<CustomerCartDVO>> allcustcartmap;

	/**
	 * Gets the allcustcartmap.
	 *
	 * @return the allcustcartmap
	 */
	public Map<String, List<CustomerCartDVO>> getAllcustcartmap() {
		return allcustcartmap;
	}

	/**
	 * Gets the dvo.
	 *
	 * @return the dvo
	 */
	public CustomerCartDVO getDvo() {
		return dvo;
	}

	/**
	 * Gets the lst.
	 *
	 * @return the lst
	 */
	public List<CustomerCartDVO> getLst() {
		return lst;
	}

	/**
	 * Sets the allcustcartmap.
	 *
	 * @param allcustcartmap the allcustcartmap
	 */
	public void setAllcustcartmap(Map<String, List<CustomerCartDVO>> allcustcartmap) {
		this.allcustcartmap = allcustcartmap;
	}

	/**
	 * Sets the dvo.
	 *
	 * @param dvo the new dvo
	 */
	public void setDvo(CustomerCartDVO dvo) {
		this.dvo = dvo;
	}

	/**
	 * Sets the lst.
	 *
	 * @param lst the new lst
	 */
	public void setLst(List<CustomerCartDVO> lst) {
		this.lst = lst;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "CustomerCartDTO [dvo=" + dvo + ", lst=" + lst + ", allcustcartmap=" + allcustcartmap + "]";
	}

}
