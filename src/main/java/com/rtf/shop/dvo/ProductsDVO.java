/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.shop.dvo;

import com.rtf.saas.util.DateFormatUtil;

/**
 * The Class ProductsDVO.
 */
public class ProductsDVO {

	/** The clint id. */
	private String clintId;

	/** The merc id. */
	private Integer mercId;

	/** The merc prodid. */
	private Integer mercProdid;

	/** The prod id selrsite. */
	private String prodIdSelrsite;

	/** The prod cat. */
	private String prodCat;

	/** The prod subcat. */
	private String prodSubcat;

	/** The prod name. */
	private String prodName;

	/** The prod desc. */
	private String prodDesc;

	/** The unit value. */
	private Double unitValue;

	/** The price per unit. */
	private Double pricePerUnit;

	/** The unit type. */
	private String unitType;

	/** The prod img url. */
	private String prodImgUrl;

	/** The status. */
	private String status;

	/** The creby. */
	private String creby;

	/** The credate. */
	private java.util.Date credate;

	/** The updby. */
	private String updby;

	/** The upddate. */
	private java.util.Date upddate;

	/** The cr date time str. */
	private String crDateTimeStr;

	/** The up date time str. */
	private String upDateTimeStr;

	/**
	 * Gets the clint id.
	 *
	 * @return the clint id
	 */
	public String getClintId() {
		return clintId;
	}

	/**
	 * Gets the cr date time str.
	 *
	 * @return the cr date time str
	 */
	public String getCrDateTimeStr() {
		crDateTimeStr = DateFormatUtil.getDateMMDDYYYYYHHmmss(credate);
		return crDateTimeStr;
	}

	/**
	 * Gets the creby.
	 *
	 * @return the creby
	 */
	public String getCreby() {
		return creby;
	}

	/**
	 * Gets the credate.
	 *
	 * @return the credate
	 */
	public java.util.Date getCredate() {
		return credate;
	}

	/**
	 * Gets the merc id.
	 *
	 * @return the merc id
	 */
	public Integer getMercId() {
		return mercId;
	}

	/**
	 * Gets the merc prodid.
	 *
	 * @return the merc prodid
	 */
	public Integer getMercProdid() {
		return mercProdid;
	}

	/**
	 * Gets the price per unit.
	 *
	 * @return the price per unit
	 */
	public Double getPricePerUnit() {
		return pricePerUnit;
	}

	/**
	 * Gets the prod cat.
	 *
	 * @return the prod cat
	 */
	public String getProdCat() {
		return prodCat;
	}

	/**
	 * Gets the prod desc.
	 *
	 * @return the prod desc
	 */
	public String getProdDesc() {
		return prodDesc;
	}

	/**
	 * Gets the prod id selrsite.
	 *
	 * @return the prod id selrsite
	 */
	public String getProdIdSelrsite() {
		return prodIdSelrsite;
	}

	/**
	 * Gets the prod img url.
	 *
	 * @return the prod img url
	 */
	public String getProdImgUrl() {
		return prodImgUrl;
	}

	/**
	 * Gets the prod name.
	 *
	 * @return the prod name
	 */
	public String getProdName() {
		return prodName;
	}

	/**
	 * Gets the prod subcat.
	 *
	 * @return the prod subcat
	 */
	public String getProdSubcat() {
		return prodSubcat;
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Gets the unit type.
	 *
	 * @return the unit type
	 */
	public String getUnitType() {
		return unitType;

	}

	/**
	 * Gets the unit value.
	 *
	 * @return the unit value
	 */
	public Double getUnitValue() {
		return unitValue;
	}

	/**
	 * Gets the up date time str.
	 *
	 * @return the up date time str
	 */
	public String getUpDateTimeStr() {
		upDateTimeStr = DateFormatUtil.getDateMMDDYYYYYHHmmss(upddate);
		return upDateTimeStr;
	}

	/**
	 * Gets the updby.
	 *
	 * @return the updby
	 */
	public String getUpdby() {
		return updby;
	}

	/**
	 * Gets the upddate.
	 *
	 * @return the upddate
	 */
	public java.util.Date getUpddate() {
		return upddate;
	}

	/**
	 * Sets the clint id.
	 *
	 * @param clintId the new clint id
	 */
	public void setClintId(String clintId) {
		this.clintId = clintId;
	}

	/**
	 * Sets the cr date time str.
	 *
	 * @param crDateTimeStr the new cr date time str
	 */
	public void setCrDateTimeStr(String crDateTimeStr) {
		this.crDateTimeStr = crDateTimeStr;
	}

	/**
	 * Sets the creby.
	 *
	 * @param creby the new creby
	 */
	public void setCreby(String creby) {
		this.creby = creby;
	}

	/**
	 * Sets the credate.
	 *
	 * @param credate the new credate
	 */
	public void setCredate(java.util.Date credate) {
		this.credate = credate;
	}

	/**
	 * Sets the merc id.
	 *
	 * @param mercId the new merc id
	 */
	public void setMercId(Integer mercId) {
		this.mercId = mercId;
	}

	/**
	 * Sets the merc prodid.
	 *
	 * @param mercProdid the new merc prodid
	 */
	public void setMercProdid(Integer mercProdid) {
		this.mercProdid = mercProdid;
	}

	/**
	 * Sets the price per unit.
	 *
	 * @param pricePerUnit the new price per unit
	 */
	public void setPricePerUnit(Double pricePerUnit) {
		this.pricePerUnit = pricePerUnit;
	}

	/**
	 * Sets the prod cat.
	 *
	 * @param prodCat the new prod cat
	 */
	public void setProdCat(String prodCat) {
		this.prodCat = prodCat;
	}

	/**
	 * Sets the prod desc.
	 *
	 * @param prodDesc the new prod desc
	 */
	public void setProdDesc(String prodDesc) {
		this.prodDesc = prodDesc;
	}

	/**
	 * Sets the prod id selrsite.
	 *
	 * @param prodIdSelrsite the new prod id selrsite
	 */
	public void setProdIdSelrsite(String prodIdSelrsite) {
		this.prodIdSelrsite = prodIdSelrsite;
	}

	/**
	 * Sets the prod img url.
	 *
	 * @param prodImgUrl the new prod img url
	 */
	public void setProdImgUrl(String prodImgUrl) {
		this.prodImgUrl = prodImgUrl;
	}

	/**
	 * Sets the prod name.
	 *
	 * @param prodName the new prod name
	 */
	public void setProdName(String prodName) {
		this.prodName = prodName;
	}

	/**
	 * Sets the prod subcat.
	 *
	 * @param prodSubcat the new prod subcat
	 */
	public void setProdSubcat(String prodSubcat) {
		this.prodSubcat = prodSubcat;
	}

	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * Sets the unit type.
	 *
	 * @param unitType the new unit type
	 */
	public void setUnitType(String unitType) {
		this.unitType = unitType;
	}

	/**
	 * Sets the unit value.
	 *
	 * @param unitValue the new unit value
	 */
	public void setUnitValue(Double unitValue) {
		this.unitValue = unitValue;
	}

	/**
	 * Sets the up date time str.
	 *
	 * @param upDateTimeStr the new up date time str
	 */
	public void setUpDateTimeStr(String upDateTimeStr) {
		this.upDateTimeStr = upDateTimeStr;
	}

	/**
	 * Sets the updby.
	 *
	 * @param updby the new updby
	 */
	public void setUpdby(String updby) {
		this.updby = updby;
	}

	/**
	 * Sets the upddate.
	 *
	 * @param upddate the new upddate
	 */
	public void setUpddate(java.util.Date upddate) {
		this.upddate = upddate;
	}
}