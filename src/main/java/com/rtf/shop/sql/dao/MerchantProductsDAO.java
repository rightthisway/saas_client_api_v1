/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.shop.sql.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import com.rtf.common.util.DatabaseConnections;
import com.rtf.shop.dvo.ProductsDVO;

/**
 * The Class MerchantProductsDAO.
 */
public class MerchantProductsDAO {

	/**
	 * Gets the product by id.
	 *
	 * @param clientId the client id
	 * @param prodId   the prod id
	 * @return the product by id
	 * @throws Exception the exception
	 */
	public static ProductsDVO getProductById(String clientId, Integer prodId) throws Exception {
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		ProductsDVO vo = null;
		String sql = " SELECT * from  pa_shopm_merchant_products  with(nolock) where clintid = ? "
				+ "   and mercprodid=?  ";

		try {
			conn = DatabaseConnections.getRtfSaasConnection();

			ps = conn.prepareStatement(sql);
			ps.setString(1, clientId);
			ps.setInt(2, prodId);
			rs = ps.executeQuery();
			while (rs.next()) {
				vo = new ProductsDVO();
				Timestamp timestamp = null;
				vo.setClintId(rs.getString("clintid"));
				vo.setMercId(rs.getInt("mercid"));
				vo.setMercProdid(rs.getInt("mercProdid"));
				vo.setProdIdSelrsite(rs.getString("prodidselrsite"));
				vo.setProdCat(rs.getString("prodcat"));
				vo.setProdSubcat(rs.getString("prodsubcat"));
				vo.setProdName(rs.getString("prodname"));
				vo.setProdDesc(rs.getString("proddesc"));
				vo.setUnitValue(rs.getDouble("unitvalue"));
				vo.setPricePerUnit(rs.getDouble("priceperunit"));
				vo.setUnitType(rs.getString("unittype"));
				vo.setProdImgUrl(rs.getString("prodimageurl"));
				vo.setStatus(rs.getString("prodstatus"));
				vo.setCreby(rs.getString("creby"));
				vo.setUpdby(rs.getString("updby"));
				timestamp = rs.getTimestamp("credate");
				vo.setCredate(new java.util.Date(timestamp != null ? timestamp.getTime() : -10));
				timestamp = rs.getTimestamp("upddate");
				vo.setUpddate(new java.util.Date(timestamp != null ? timestamp.getTime() : -10));
			}
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return vo;
	}

}
