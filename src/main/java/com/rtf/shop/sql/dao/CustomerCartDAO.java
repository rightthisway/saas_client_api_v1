/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.shop.sql.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.rtf.common.util.DatabaseConnections;
import com.rtf.common.util.StatusEnums;
import com.rtf.saas.util.PaginationUtil;
import com.rtf.shop.dvo.CustomerCartDVO;

/**
 * The Class CustomerCartDAO.
 */
public class CustomerCartDAO {

	/**
	 * Fetch customer purchased products.
	 *
	 * @param cuId   the Customer ID
	 * @param clId   the Client ID
	 * @param pageNo the page no
	 * @return the list
	 * @throws Exception the exception
	 */
	public static List<CustomerCartDVO> fetchCustomerPurchasedProducts(String cuId, String clId, String pageNo)
			throws Exception {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<CustomerCartDVO> cartList = null;

		StringBuffer sb = new StringBuffer(
				"  select a.custid as custid, a.mercid as mercid, a.clintid as clintid, a.mercprodid as mercprodid ,a.buyunits as buyunits, ");
		sb.append(" b.priceperunit as buyprice ,b.unittype as unittype , ");
		sb.append(" b.prodname as prodname, b.prodidselrsite as prodidselrsite,  b.prodimageurl as prodimageurl, ");
		sb.append(" c.mrktplce as mrktplce, c.mercselrsite  from  pt_shopm_customer_shopping_cart a ");
		sb.append(" join pa_shopm_merchant_products b ");
		sb.append(" on a.mercid = b.mercid and a.clintid=b.clintid and a.mercprodid=b.mercprodid  ");
		sb.append(" join pr_shopm_client_merchant c on  a.mercid=c.mercid and a.clintid=c.clintid ");
		sb.append(" where a.cartstatus = ? and   a.clintid =  ? and a.custid = ?  order by prodname ");

		String pageQuery = PaginationUtil.getPaginationQuery(pageNo);
		sb.append(pageQuery);

		try {
			conn = DatabaseConnections.getRtfSaasConnection();
			ps = conn.prepareStatement(sb.toString());
			ps.setString(1, StatusEnums.PURCHASED.name());
			ps.setString(2, clId);
			ps.setString(3, cuId);
			rs = ps.executeQuery();
			cartList = new ArrayList<CustomerCartDVO>();
			while (rs.next()) {
				CustomerCartDVO cdvo = new CustomerCartDVO();
				cdvo.setMercProdid(rs.getInt("mercprodid"));
				cdvo.setBuyUnits(rs.getInt("buyunits"));
				cdvo.setBuyPrice(rs.getDouble("buyprice"));
				cdvo.setUnitType(rs.getString("unittype"));
				cdvo.setProdName(rs.getString("prodname"));
				cdvo.setProdIdSelrsite(rs.getString("prodidselrsite"));
				cdvo.setProdImgUrl(rs.getString("prodimageurl"));
				cdvo.setMrktPlce(rs.getString("mrktplce"));
				cdvo.setMercSelrSite(rs.getString("mercselrsite"));
				cdvo.setClintId(rs.getString("clintid"));
				cdvo.setMercId(rs.getInt("mercid"));
				cdvo.setCustId(rs.getString("custid"));
				cartList.add(cdvo);
			}
			return cartList;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return cartList;
	}

	/**
	 * Update cart as purchased.
	 *
	 * @param ids  the Product ID
	 * @param cuId the Customer ID
	 * @param clId the Client ID
	 * @return the integer
	 */
	public static Integer updateCartAsPurchased(List<Integer> ids, String cuId, String clId) {
		Connection conn = null;
		Integer affectedRows = 0;
		PreparedStatement ps = null;
		StringBuffer sb = new StringBuffer(" update  pt_shopm_customer_shopping_cart set  cartstatus =?, ");
		sb.append(" upddate=getDate()  where clintid =  ?  and mercprodid=? and custid = ? ");
		try {
			conn = DatabaseConnections.getRtfSaasConnection();
			ps = conn.prepareStatement(sb.toString());
			for (Integer id : ids) {
				ps.setString(1, StatusEnums.PURCHASED.name());
				ps.setString(2, clId);
				ps.setInt(3, id);
				ps.setString(4, cuId);
				affectedRows = +ps.executeUpdate();
			}
			return affectedRows;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return affectedRows;
	}

	/**
	 * Update multiple item status for Customer Cart.
	 *
	 * @param dvos   the CustomerCartDVO DVO
	 * @param status the status
	 * @return the integer
	 * @throws Exception the exception
	 */
	public static Integer updateCartMultiItemStatus(List<CustomerCartDVO> dvos, String status) throws Exception {
		Connection conn = null;
		Integer affectedRows = 0;
		PreparedStatement ps = null;
		StringBuffer sb = new StringBuffer(" update  pt_shopm_customer_shopping_cart set  cartstatus =?, updby = ? , ");
		sb.append(" upddate=getDate()  where clintid =  ? and mercid = ?  and mercprodid=? and custid = ? ");
		try {
			conn = DatabaseConnections.getRtfSaasConnection();
			ps = conn.prepareStatement(sb.toString());
			for (CustomerCartDVO dvo : dvos) {
				ps.setString(1, status);
				ps.setString(2, dvo.getUpdby());
				ps.setString(3, dvo.getClintId());
				ps.setInt(4, dvo.getMercId());
				ps.setInt(5, dvo.getMercProdid());
				ps.setString(6, dvo.getCustId());
				affectedRows = +ps.executeUpdate();
			}
			return affectedRows;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return affectedRows;
	}

	/**
	 * Update current contest cart item status.
	 *
	 * @param dvo    the CustomerCartDVO
	 * @param status the status
	 * @return the integer
	 * @throws Exception the exception
	 */
	public static Integer updateCurrentContestCartItemStatus(CustomerCartDVO dvo, String status) throws Exception {
		Connection conn = null;
		Integer affectedRows = 0;
		PreparedStatement ps = null;
		StringBuffer sb = new StringBuffer(" update  pt_shopm_customer_shopping_cart set  cartstatus =?, updby = ? , ");
		sb.append(" upddate=getDate()  where clintid =  ? and mercid = ? ");
		sb.append(" and mercprodid=? and custid = ? and conid=? ");
		try {
			conn = DatabaseConnections.getRtfSaasConnection();
			ps = conn.prepareStatement(sb.toString());
			ps.setString(1, status);
			ps.setString(2, dvo.getUpdby());
			ps.setString(3, dvo.getClintId());
			ps.setInt(4, dvo.getMercId());
			ps.setInt(5, dvo.getMercProdid());
			ps.setString(6, dvo.getCustId());
			ps.setString(7, dvo.getConId());
			affectedRows = ps.executeUpdate();
			return affectedRows;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return affectedRows;
	}

	/**
	 * Update item master cart.
	 *
	 * @param clId   the Client ID
	 * @param mercId the Merchant ID
	 * @param prodId the Product id
	 * @param cuId   the Customer ID
	 * @param status the status
	 * @param cau    the user Id
	 * @return the integer
	 * @throws Exception the exception
	 */
	public static Integer updateItemMasterCart(String clId, Integer mercId, Integer prodId, String cuId, String status,
			String cau) throws Exception {
		Connection conn = null;
		Integer affectedRows = 0;
		PreparedStatement ps = null;
		StringBuffer sb = new StringBuffer(" update  pt_shopm_customer_shopping_cart set  cartstatus =?, updby = ? , ");
		sb.append(" upddate=getDate()  where clintid =  ? and mercid = ?  and mercprodid=? and custid = ?");
		try {
			conn = DatabaseConnections.getRtfSaasConnection();
			ps = conn.prepareStatement(sb.toString());
			ps.setString(1, status);
			ps.setString(2, cau);
			ps.setString(3, clId);
			ps.setInt(4, mercId);
			ps.setInt(5, prodId);
			ps.setString(6, cuId);
			affectedRows = ps.executeUpdate();
			return affectedRows;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return affectedRows;
	}

	/**
	 * Update previous contest active cart item status.
	 *
	 * @param dvo    the CustomerCartDVO
	 * @param status the status
	 * @return the integer
	 * @throws Exception the exception
	 */
	public static Integer updatePreviousContestActiveCartItemStatus(CustomerCartDVO dvo, String status)
			throws Exception {
		Connection conn = null;
		Integer affectedRows = 0;
		PreparedStatement ps = null;
		StringBuffer sb = new StringBuffer(" update  pt_shopm_customer_shopping_cart set  cartstatus =?, updby = ? , ");
		sb.append(" upddate=getDate()  where clintid =  ? and mercid = ? ");
		sb.append(" and mercprodid=? and custid = ? and conid NOT IN (?) and cartstatus='ACTIVE'  ");
		try {
			conn = DatabaseConnections.getRtfSaasConnection();
			ps = conn.prepareStatement(sb.toString());
			ps.setString(1, status);
			ps.setString(2, dvo.getUpdby());
			ps.setString(3, dvo.getClintId());
			ps.setInt(4, dvo.getMercId());
			ps.setInt(5, dvo.getMercProdid());
			ps.setString(6, dvo.getCustId());
			ps.setString(7, dvo.getConId());
			affectedRows = ps.executeUpdate();
			return affectedRows;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return affectedRows;
	}

}
