/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved. 
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.shop.ecom.dao;

import java.util.Date;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.exceptions.DriverException;
import com.rtf.saas.db.CassandraConnector;
import com.rtf.saas.exception.RTFDataAccessException;
import com.rtf.saas.util.GenUtil;
import com.rtf.shop.ecom.dvo.EcomCartDVO;

/**
 * The Class ProductItemMasterCassDAO.
 */
public class ProductItemMasterCassDAO {

	/**
	 * Fetch product Item Master details and populate existing Cart POJO.
	 *
	 * @param dvo  the EcomCart dvo
	 * 
	 * @return the integer
	 * @throws Exception the exception
	 */
	
	public static EcomCartDVO getProductByItemsId(String clientId, Integer itmsid, EcomCartDVO  dvo) throws Exception {
		ResultSet resultSet = null;		
		try {
			resultSet = CassandraConnector.getSession().execute(
					"SELECT clintid ,itmsid,slrpitms_id,sler_id,pbrand ,pname ,pdesc , plong_desc, pimg,pprc_dtl, psel_min_prc ,price_type, preg_min_prc from  rtf_seller_product_items_mstr  WHERE clintid = ? AND itmsid=? ", clientId, itmsid);
			if (resultSet != null) {
				
				/*clintid ,itmsid,slrpitms_id,sler_id,");			
				sql.append("pbrand ,pname ,pdesc , plong_desc, pimg,");
				sql.append(" pprc_dtl, psel_min_prc ,price_type, preg_min_prc,");
				sql.append( "pstatus,lupd_by,lupd_date)"
						+ "
				 */				
				for (Row rs : resultSet) {
					dvo.setItmsId(rs.getInt("itmsid"));
					dvo.setProdIdSelrsite(rs.getString("slrpitms_id"));
					dvo.setMercId(rs.getInt("sler_id"));					
					dvo.setPbrand(rs.getString("pbrand"));
					dvo.setProdName(rs.getString("pname"));
					dvo.setProdDesc(rs.getString("pdesc"));
					dvo.setProdLongDesc(rs.getString("plong_desc"));
					dvo.setProdImgUrl(rs.getString("pimg"));
					dvo.setProdPriceDtl(rs.getString("pprc_dtl"));
					dvo.setSellPrc(rs.getDouble("psel_min_prc"));
					dvo.setPriceType(rs.getString("price_type"));
					dvo.setRegPrc(rs.getDouble("preg_min_prc"));
				}
			}else {
				return null;
			}
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		} finally {
		}
		return dvo;
	}
	
	
	
	
	
}
