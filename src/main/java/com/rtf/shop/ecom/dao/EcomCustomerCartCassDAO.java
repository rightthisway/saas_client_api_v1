/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.shop.ecom.dao;


import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.exceptions.DriverException;
import com.rtf.common.util.DatabaseConnections;
import com.rtf.common.util.StatusEnums;
import com.rtf.saas.db.CassandraConnector;
import com.rtf.saas.exception.RTFDataAccessException;
import com.rtf.saas.util.GenUtil;
import com.rtf.shop.ecom.dvo.EcomCartDVO;

/**
 * The Class EcomCustomerCartCassDAO.
 */
public class EcomCustomerCartCassDAO {

	/**
	 * Adds the Product Object to Customer cart.
	 *
	 * @param dvo    the EcomCartDVO
	 * @param status the status
	 * @return the integer
	 */
	
	
	public static Integer addToCustomerCart(EcomCartDVO dvo)  throws Exception{

	Integer status = 0;
	StringBuffer sql = new StringBuffer();
	sql.append(
		"INSERT INTO  pt_shopm_customer_shopping_cart (clintid ,conid,custid,slrpitms_id, itmsid , sler_id,");	
	sql.append(" buyprice,buyunits, preg_min_prc, psel_min_prc,price_type,pprc_dtl, ");
	sql.append("pbrand ,pname ,pdesc , plong_desc, pimg,");
	sql.append( "cartstatus,updby,upddate,credate,isplacedorder) ");
	
	sql.append("values (?,?,?,?,?,?") ;  
	sql.append(",?,?,?,?,?,?") ;  
	sql.append(",?,?,?,?,?") ;  
	sql.append(",?, ? , toTimestamp(now()),toTimestamp(now()),?  )");
	
	System.out.println(sql);
	try {
		CassandraConnector.getSession().execute(sql.toString(),
				new Object[] { dvo.getClintId(),dvo.getConId(),dvo.getCustId(),dvo.getProdIdSelrsite(),dvo.getItmsId(),dvo.getMercId() ,						
						dvo.getSellPrc(),dvo.getBuyUnits(),dvo.getRegPrc(),dvo.getSellPrc(),dvo.getPriceType(),dvo.getProdPriceDtl(),
						dvo.getPbrand(),dvo.getProdName(),dvo.getProdDesc(),dvo.getProdLongDesc(),dvo.getProdImgUrl(),
						StatusEnums.ACTIVE.name(),dvo.getUpdby(),Boolean.FALSE	
		});
		status = 1;
	} catch (final DriverException de) {
		de.printStackTrace();
		throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
	} catch (Exception ex) {
		ex.printStackTrace();
		throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
	}
	return status;
}
	
	/**
	 * Delete item in Customers cart.
	 *
	 * @param dvo    the EcomCartDVO
	 * @param status the status
	 * @return the integer
	 * @throws Exception the exception
	 */
	public static void deleteItemInCustomerCart(EcomCartDVO dvo) throws Exception {
		try {
			CassandraConnector.getSession().execute(
					"delete from  pt_shopm_customer_shopping_cart where  clintid = ? and conid = ?  and  custid = ?  and itmsid=? ",
					dvo.getClintId(), dvo.getConId(), dvo.getCustId(), dvo.getItmsId());
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		}
	}
	
	
	
	public static  List<EcomCartDVO> fetchCustomerCart(EcomCartDVO  pdvo) throws Exception {
		  ResultSet resultSet = null;	
		 List<EcomCartDVO> lst = new ArrayList<EcomCartDVO>();
		try {
			resultSet = CassandraConnector.getSession().execute(
					"SELECT * from pt_shopm_customer_shopping_cart  WHERE clintid = ? and conid = ?  and  custid = ?  ", pdvo.getClintId(), pdvo.getConId(), pdvo.getCustId());
			if (resultSet != null) {				
				for (Row rs : resultSet) {
					EcomCartDVO dvo = new EcomCartDVO();					
					dvo.setItmsId(rs.getInt("itmsid"));
					dvo.setMercProdid(rs.getString("slrpitms_id"));					
					dvo.setMercId(rs.getInt("sler_id"));					
					dvo.setPbrand(rs.getString("pbrand"));
					dvo.setProdName(rs.getString("pname"));
					dvo.setProdDesc(rs.getString("pdesc"));
					//dvo.setProdLongDesc(rs.getString("plong_desc"));
					dvo.setProdImgUrl(rs.getString("pimg"));
					dvo.setProdPriceDtl(rs.getString("pprc_dtl"));
					dvo.setSellPrc(rs.getDouble("psel_min_prc"));
					dvo.setPriceType(rs.getString("price_type"));
					dvo.setRegPrc(rs.getDouble("preg_min_prc"));
					lst.add(dvo);
				}
			}
			return lst;
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		} finally {
		}
	}

	
	
	
	public static  List<EcomCartDVO> fetchAllCustomerCart(String clId, String coId) throws Exception {
		  ResultSet resultSet = null;	
		 List<EcomCartDVO> lst = new ArrayList<EcomCartDVO>();
		try {
			resultSet = CassandraConnector.getSession().execute(
					"SELECT * from pt_shopm_customer_shopping_cart  WHERE clintid = ? and conid = ?  ", clId, coId);
			if (resultSet != null) {				
				for (Row rs : resultSet) {
					EcomCartDVO dvo = new EcomCartDVO();					
					dvo.setItmsId(rs.getInt("itmsid"));
					dvo.setMercProdid(rs.getString("slrpitms_id"));
					dvo.setCustId(rs.getString("custid"));
					dvo.setMercId(rs.getInt("sler_id"));					
					dvo.setPbrand(rs.getString("pbrand"));
					dvo.setProdName(rs.getString("pname"));
					dvo.setProdDesc(rs.getString("pdesc"));					
					dvo.setProdImgUrl(rs.getString("pimg"));
					dvo.setProdPriceDtl(rs.getString("pprc_dtl"));
					dvo.setSellPrc(rs.getDouble("psel_min_prc"));
					dvo.setPriceType(rs.getString("price_type"));
					dvo.setRegPrc(rs.getDouble("preg_min_prc"));					
					dvo.setCredate(rs.getTimestamp("credate"));
					lst.add(dvo);
				}
			}
			return lst;
		} catch (final DriverException de) {
			de.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(de), de);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RTFDataAccessException(GenUtil.getExceptionAsString(ex), ex);
		} finally {
		}
	}
	
	
	
}
