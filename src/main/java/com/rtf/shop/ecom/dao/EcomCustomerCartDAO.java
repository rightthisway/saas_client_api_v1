/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.shop.ecom.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.rtf.common.util.DatabaseConnections;
import com.rtf.common.util.StatusEnums;
import com.rtf.saas.util.PaginationUtil;
import com.rtf.shop.ecom.dvo.EcomCartDVO;

/**
 * The Class EcomCustomerCartDAO.
 */
public class EcomCustomerCartDAO {

	/**
	 * Adds the Product Object to Customer cart.
	 *
	 * @param dvo    the EcomCartDVO
	 * @param status the status
	 * @return the integer
	 */
	public static Integer addToCart(EcomCartDVO dvo, String status) {
		Connection conn = null;
		PreparedStatement ps = null;
		Integer affectedRows = 0;
		StringBuffer sb = new StringBuffer(
				"  insert into pt_shopm_customer_shopping_cart ( " + "   clintid,mercid,custid,mercprodid,conid ");
		sb.append(" , buyunits,buyprice,cartstatus,unittype " + " ,credate) values " + "  (? , ? , ? , ? , ? , ");
		sb.append("  ? , ? , ? , ? ," + " getDate()) ");
		try {
			conn = DatabaseConnections.getRtfSaasConnection();
			ps = conn.prepareStatement(sb.toString());
			ps.setString(1, dvo.getClintId());
			ps.setInt(2, dvo.getMercId());
			ps.setString(3, dvo.getCustId());
			ps.setString(4, dvo.getMercProdid());
			ps.setString(5, dvo.getConId());
			ps.setInt(6, dvo.getBuyUnits());
			ps.setDouble(7, dvo.getBuyPrice());
			ps.setString(8, status);
			ps.setString(9, dvo.getUnitType());
			affectedRows = ps.executeUpdate();
			return affectedRows;

		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return affectedRows;
	}

	/**
	 * Check if item exist as active status in cart records.
	 *
	 * @param dvo the EcomCartDVO
	 * @return the boolean
	 * @throws Exception the exception
	 */
	public static Boolean checkIfItemExistAsActiveInCart(EcomCartDVO dvo) throws Exception {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Boolean isExist = Boolean.FALSE;
		StringBuffer sb = new StringBuffer(
				" select cartstatus from  pt_shopm_customer_shopping_cart with(nolock) where  ");
		sb.append(" cartstatus = ? and clintid =  ? and mercid = ?  and mercprodid=? and custid = ? ");
		try {
			conn = DatabaseConnections.getRtfSaasConnection();
			ps = conn.prepareStatement(sb.toString());
			ps.setString(1, StatusEnums.ACTIVE.name());
			ps.setString(2, dvo.getClintId());
			ps.setInt(3, dvo.getMercId());
			ps.setString(4, dvo.getMercProdid());
			ps.setString(5, dvo.getCustId());
			rs = ps.executeQuery();
			while (rs.next()) {
				isExist = Boolean.TRUE;
			}
			return isExist;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return null;
	}

	/**
	 * Check if item exist for contest as active in cart.
	 *
	 * @param dvo the EcomCartDVO
	 * @return the boolean
	 * @throws Exception the exception
	 */
	public static Boolean checkIfItemExistForContestAsActiveInCart(EcomCartDVO dvo) throws Exception {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Boolean isExist = Boolean.FALSE;
		StringBuffer sb = new StringBuffer(
				" select cartstatus from  pt_shopm_customer_shopping_cart with(nolock) where  ");
		sb.append(" cartstatus = ? and   clintid =  ? and mercid = ? ");
		sb.append(" and mercprodid=? and custid = ? and conid = ?");
		try {
			conn = DatabaseConnections.getRtfSaasConnection();
			ps = conn.prepareStatement(sb.toString());
			ps.setString(1, StatusEnums.ACTIVE.name());
			ps.setString(2, dvo.getClintId());
			ps.setInt(3, dvo.getMercId());
			ps.setString(4, dvo.getMercProdid());
			ps.setString(5, dvo.getCustId());
			ps.setString(6, dvo.getConId());
			rs = ps.executeQuery();
			while (rs.next()) {
				isExist = Boolean.TRUE;
			}
			return isExist;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return null;
	}

	/**
	 * Delete item from master cart of Customer.
	 *
	 * @param clId   the Client ID
	 * @param mercId the Merchant ID
	 * @param prodId the Product ID
	 * @param cuId   the Customer id
	 * @return the integer
	 * @throws Exception the exception
	 */
	public static Integer deleteItemFromMasterCart(String clId, Integer mercId, Integer prodId, String cuId)
			throws Exception {
		Connection conn = null;
		Integer affectedRows = 0;
		PreparedStatement ps = null;
		StringBuffer sb = new StringBuffer(
				" Delete from  pt_shopm_customer_shopping_cart  where clintid =  ? and mercid = ?  and mercprodid=? and custid = ?");
		try {
			conn = DatabaseConnections.getRtfSaasConnection();
			ps = conn.prepareStatement(sb.toString());
			ps.setString(1, clId);
			ps.setInt(2, mercId);
			ps.setInt(3, prodId);
			ps.setString(4, cuId);
			affectedRows = ps.executeUpdate();
			return affectedRows;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return affectedRows;
	}

	/**
	 * Delete item in Customers cart.
	 *
	 * @param dvo    the EcomCartDVO
	 * @param status the status
	 * @return the integer
	 * @throws Exception the exception
	 */
	public static Integer deleteItemInCart(EcomCartDVO dvo, String status) throws Exception {
		Connection conn = null;
		Integer affectedRows = 0;
		PreparedStatement ps = null;
		StringBuffer sb = new StringBuffer(" update  pt_shopm_customer_shopping_cart set  cartstatus =?, updby = ? , ");
		sb.append(" upddate=getDate()  where clintid =  ? and mercid = ? ");
		sb.append(" and mercprodid=? and custid = ? and conid=? ");

		try {
			conn = DatabaseConnections.getRtfSaasConnection();
			ps = conn.prepareStatement(sb.toString());
			ps.setString(1, status);
			ps.setString(2, dvo.getUpdby());
			ps.setString(3, dvo.getClintId());
			ps.setInt(4, dvo.getMercId());
			ps.setString(5, dvo.getMercProdid());
			ps.setString(6, dvo.getCustId());
			ps.setString(7, dvo.getConId());
			affectedRows = ps.executeUpdate();
			return affectedRows;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return affectedRows;
	}

	/**
	 * Fetch all customer in cart.
	 *
	 * @return the map
	 * @throws Exception the exception
	 */
	public static Map<String, String> fetchAllCustomerInCart() throws Exception {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Map<String, String> cartCustMap = null;
		String sql = " select distinct (custid ) as custid  , clintid from  pt_shopm_customer_shopping_cart  where cartstatus =? ";
		try {
			conn = DatabaseConnections.getRtfSaasConnection();
			ps = conn.prepareStatement(sql);
			ps.setString(1, StatusEnums.ACTIVE.name());
			rs = ps.executeQuery();
			cartCustMap = new HashMap<String, String>();
			while (rs.next()) {
				cartCustMap.put(rs.getString("custid"), rs.getString("clintid"));
			}
			return cartCustMap;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return cartCustMap;
	}

	/**
	 * Fetch customer cart.
	 *
	 * @param dvo the EcomCartDVO
	 * @return the list
	 * @throws Exception the exception
	 */
	public static List<EcomCartDVO> fetchCustomerCart(EcomCartDVO dvo) throws Exception {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<EcomCartDVO> cartList = null;
		StringBuffer sb = new StringBuffer(" select a.clintid,a.mercid,a.custid,a.mercprodid,a.conid,a.buyunits,");
		sb.append("  b.pname,b.psel_min_prc,b.preg_min_prc,b.pprc_dtl as priceDtl,b.pimg, b.price_type as priceType from ");
		sb.append("  pt_shopm_customer_shopping_cart a   join rtf_seller_product_items_mstr b on ");
		sb.append("  a.clintid=b.clintid and a.mercid=b.sler_id and  a.mercprodid=b.slrpitms_id ");
		sb.append("  where a.cartstatus='ACTIVE'   AND a.clintid=? and a.custid=? and a.conid=? ");

		try {
			conn = DatabaseConnections.getRtfSaasConnection();
			ps = conn.prepareStatement(sb.toString());
			ps.setString(1, dvo.getClintId());
			ps.setString(2, dvo.getCustId());
			ps.setString(3, dvo.getConId());
			rs = ps.executeQuery();
			cartList = new ArrayList<EcomCartDVO>();
			while (rs.next()) {
				EcomCartDVO cdvo = new EcomCartDVO();
				cdvo.setClintId(rs.getString("clintid"));
				cdvo.setMercId(rs.getInt("mercid"));
				cdvo.setCustId(rs.getString("custid"));
				cdvo.setMercProdid(rs.getString("mercprodid"));
				cdvo.setConId(rs.getString("conid"));
				cdvo.setBuyUnits(rs.getInt("buyunits"));
				cdvo.setProdName(rs.getString("pname"));
				cdvo.setRegPrc(rs.getDouble("preg_min_prc"));
				cdvo.setSellPrc(rs.getDouble("psel_min_prc"));
				cdvo.setProdPriceDtl(rs.getString("priceDtl"));
				cdvo.setProdImgUrl(rs.getString("pimg"));
				cdvo.setPriceType(rs.getString("priceType"));

				cartList.add(cdvo);
			}
			return cartList;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return cartList;
	}

	/**
	 * Fetch customer purchased products.
	 *
	 * @param cuId   the Customer id
	 * @param clId   the Client ID
	 * @param pageNo the page no
	 * @return the list
	 * @throws Exception the exception
	 */
	public static List<EcomCartDVO> fetchCustomerPurchasedProducts(String cuId, String clId, String pageNo)
			throws Exception {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<EcomCartDVO> cartList = null;
		StringBuffer sb = new StringBuffer(
				" select a.custid as custid, a.mercid as mercid, a.clintid as clintid, a.mercprodid as mercprodid ,a.buyunits as buyunits, ");
		sb.append(
				" select a.custid as custid, a.mercid as mercid, a.clintid as clintid, a.mercprodid as mercprodid ,a.buyunits as buyunits, ");
		sb.append(" b.priceperunit as buyprice ,b.unittype as unittype , ");
		sb.append(
				" b.prodname as prodname, b.prodidselrsite as prodidselrsite, " + " b.prodimageurl as prodimageurl, ");
		sb.append(" c.mrktplce as mrktplce, c.mercselrsite  from " + " pt_shopm_customer_shopping_cart a ");
		sb.append(" join pa_shopm_merchant_products b ");
		sb.append(" on a.mercid = b.mercid and a.clintid=b.clintid and a.mercprodid=b.mercprodid  ");
		sb.append(" join pr_shopm_client_merchant c on  a.mercid=c.mercid and a.clintid=c.clintid ");
		sb.append(" where a.cartstatus = ? and  " + " a.clintid =  ? and a.custid = ?  order by prodname ");

		String pageQuery = PaginationUtil.getPaginationQuery(pageNo);
		sb.append(pageQuery);

		try {
			conn = DatabaseConnections.getRtfSaasConnection();
			ps = conn.prepareStatement(sb.toString());
			ps.setString(1, StatusEnums.PURCHASED.name());
			ps.setString(2, clId);
			ps.setString(3, cuId);
			rs = ps.executeQuery();
			cartList = new ArrayList<EcomCartDVO>();
			while (rs.next()) {
				EcomCartDVO cdvo = new EcomCartDVO();
				cdvo.setMercProdid(rs.getString("mercprodid"));
				cdvo.setBuyUnits(rs.getInt("buyunits"));
				cdvo.setBuyPrice(rs.getDouble("buyprice"));
				cdvo.setUnitType(rs.getString("unittype"));
				cdvo.setProdName(rs.getString("prodname"));
				cdvo.setProdIdSelrsite(rs.getString("prodidselrsite"));
				cdvo.setProdImgUrl(rs.getString("prodimageurl"));
				cdvo.setMrktPlce(rs.getString("mrktplce"));
				cdvo.setMercSelrSite(rs.getString("mercselrsite"));
				cdvo.setClintId(rs.getString("clintid"));
				cdvo.setMercId(rs.getInt("mercid"));
				cdvo.setCustId(rs.getString("custid"));
				cartList.add(cdvo);
			}
			return cartList;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return cartList;
	}

	/**
	 * Gets the all customer carts.
	 *
	 * @param dvo the EcomCartDVO
	 * @return the all customer carts
	 * @throws Exception the exception
	 */
	public static List<EcomCartDVO> getAllCustomerCarts(EcomCartDVO dvo) throws Exception {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<EcomCartDVO> cartList = null;

		String sql = " select * from pt_shopm_customer_shopping_cart where clintid =  ? and cartstatus = ?";

		try {
			conn = DatabaseConnections.getRtfSaasConnection();
			ps = conn.prepareStatement(sql);
			ps.setString(1, dvo.getClintId());
			ps.setString(2, StatusEnums.ACTIVE.name());
			rs = ps.executeQuery();
			cartList = new ArrayList<EcomCartDVO>();
			while (rs.next()) {
				EcomCartDVO cdvo = new EcomCartDVO();
				cdvo.setClintId(rs.getString("clintid"));
				cdvo.setMercId(rs.getInt("mercid"));
				cdvo.setCustId(rs.getString("custid"));
				cdvo.setMercProdid(rs.getString("mercprodid"));
				cdvo.setBuyUnits(rs.getInt("buyunits"));
				cdvo.setBuyPrice(rs.getDouble("buyprice"));
				cdvo.setStatus(rs.getString("cartstatus"));
				cdvo.setCredate(rs.getDate("credate"));
				cdvo.setUpddate(rs.getDate("upddate"));
				cdvo.setUpdby(rs.getString("updby"));
				cdvo.setUnitType(rs.getString("unittype"));
				cartList.add(cdvo);
			}
			return cartList;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return cartList;
	}

	/**
	 * Update cart as purchased.
	 *
	 * @param ids  the Product ids
	 * @param cuId the Customer id
	 * @param clId the Client ID
	 * @return the integer
	 */
	public static Integer updateCartAsPurchased(List<Integer> ids, String cuId, String clId) {
		Connection conn = null;
		Integer affectedRows = 0;
		PreparedStatement ps = null;
		String sql = " update  pt_shopm_customer_shopping_cart set " + " cartstatus =?, "
				+ " upddate=getDate()  where clintid =  ? " + " and mercprodid=? and custid = ? ";
		try {
			conn = DatabaseConnections.getRtfSaasConnection();
			ps = conn.prepareStatement(sql);
			for (Integer id : ids) {
				ps.setString(1, StatusEnums.PURCHASED.name());
				ps.setString(2, clId);
				ps.setInt(3, id);
				ps.setString(4, cuId);
				affectedRows = +ps.executeUpdate();
			}
			return affectedRows;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return affectedRows;
	}

	/**
	 * Update multiple item status in cart.
	 *
	 * @param dvos   the EcomCartDVO List
	 * @param status the status
	 * @return the integer
	 * @throws Exception the exception
	 */
	public static Integer updateCartMultiItemStatus(List<EcomCartDVO> dvos, String status) throws Exception {
		Connection conn = null;
		Integer affectedRows = 0;
		PreparedStatement ps = null;
		String sql = " update  pt_shopm_customer_shopping_cart set " + " cartstatus =?, updby = ? , "
				+ " upddate=getDate()  where clintid =  ? and mercid = ? " + " and mercprodid=? and custid = ? ";
		try {
			conn = DatabaseConnections.getRtfSaasConnection();
			ps = conn.prepareStatement(sql);
			for (EcomCartDVO dvo : dvos) {
				ps.setString(1, status);
				ps.setString(2, dvo.getUpdby());
				ps.setString(3, dvo.getClintId());
				ps.setInt(4, dvo.getMercId());
				ps.setString(5, dvo.getMercProdid());
				ps.setString(6, dvo.getCustId());
				affectedRows = +ps.executeUpdate();
			}
			return affectedRows;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return affectedRows;
	}

	/**
	 * Update current contest cart item status.
	 *
	 * @param dvo    he EcomCartDVO
	 * @param status the status
	 * @return the integer
	 * @throws Exception the exception
	 */
	public static Integer updateCurrentContestCartItemStatus(EcomCartDVO dvo, String status) throws Exception {
		Connection conn = null;
		Integer affectedRows = 0;
		PreparedStatement ps = null;
		StringBuffer sb = new StringBuffer(
				" update  pt_shopm_customer_shopping_cart set " + " cartstatus =?, updby = ? , ");
		sb.append(" upddate=getDate()  where clintid =  ? and mercid = ? ");
		sb.append(" and mercprodid=? and custid = ? and conid=? ");
		try {
			conn = DatabaseConnections.getRtfSaasConnection();
			ps = conn.prepareStatement(sb.toString());
			ps.setString(1, status);
			ps.setString(2, dvo.getUpdby());
			ps.setString(3, dvo.getClintId());
			ps.setInt(4, dvo.getMercId());
			ps.setString(5, dvo.getMercProdid());
			ps.setString(6, dvo.getCustId());
			ps.setString(7, dvo.getConId());
			affectedRows = ps.executeUpdate();
			return affectedRows;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return affectedRows;
	}

	/**
	 * Update item master cart.
	 *
	 * @param clId   the Client ID
	 * @param mercId the Merchant ID
	 * @param prodId the Product ID
	 * @param cuId   the Customer id
	 * @param status the status
	 * @param cau    The Login User Id
	 * @return the integer
	 * @throws Exception the exception
	 */
	public static Integer updateItemMasterCart(String clId, Integer mercId, Integer prodId, String cuId, String status,
			String cau) throws Exception {
		Connection conn = null;
		Integer affectedRows = 0;
		PreparedStatement ps = null;
		String sql = " update  pt_shopm_customer_shopping_cart set " + " cartstatus =?, updby = ? , "
				+ " upddate=getDate()  where clintid =  ? and mercid = ? " + " and mercprodid=? and custid = ?";
		try {
			conn = DatabaseConnections.getRtfSaasConnection();
			ps = conn.prepareStatement(sql);
			ps.setString(1, status);
			ps.setString(2, cau);
			ps.setString(3, clId);
			ps.setInt(4, mercId);
			ps.setInt(5, prodId);
			ps.setString(6, cuId);
			affectedRows = ps.executeUpdate();
			return affectedRows;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return affectedRows;
	}

	/**
	 * Update previous contest active cart item status.
	 *
	 * @param dvo    the EcomCartDVO
	 * @param status the status
	 * @return the integer
	 * @throws Exception the exception
	 */
	public static Integer updatePreviousContestActiveCartItemStatus(EcomCartDVO dvo, String status) throws Exception {
		Connection conn = null;
		Integer affectedRows = 0;
		PreparedStatement ps = null;
		String sql = " update  pt_shopm_customer_shopping_cart set " + " cartstatus =?, updby = ? , "
				+ " upddate=getDate()  where clintid =  ? and mercid = ? "
				+ " and mercprodid=? and custid = ? and conid NOT IN (?) and cartstatus='ACTIVE'  ";
		try {
			conn = DatabaseConnections.getRtfSaasConnection();
			ps = conn.prepareStatement(sql);
			ps.setString(1, status);
			ps.setString(2, dvo.getUpdby());
			ps.setString(3, dvo.getClintId());
			ps.setInt(4, dvo.getMercId());
			ps.setString(5, dvo.getMercProdid());
			ps.setString(6, dvo.getCustId());
			ps.setString(7, dvo.getConId());
			affectedRows = ps.executeUpdate();
			return affectedRows;
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				DatabaseConnections.closeConnection(conn);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return affectedRows;
	}

}
