/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.shop.ecom.dto;

import java.util.List;
import java.util.Map;

import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.shop.ecom.dvo.EcomCartDVO;

/**
 * The Class EcomCartDTO.
 */
public class EcomCartDTO extends RtfSaasBaseDTO {

	/** The dvo. */
	private EcomCartDVO dvo;

	/** The lst. */
	private List<EcomCartDVO> lst;

	/** The allcustcartmap. */
	private Map<String, List<EcomCartDVO>> allcustcartmap;

	/**
	 * Gets the allcustcartmap.
	 *
	 * @return the allcustcartmap
	 */
	public Map<String, List<EcomCartDVO>> getAllcustcartmap() {
		return allcustcartmap;
	}

	/**
	 * Gets the dvo.
	 *
	 * @return the dvo
	 */
	public EcomCartDVO getDvo() {
		return dvo;
	}

	/**
	 * Gets the lst.
	 *
	 * @return the lst
	 */
	public List<EcomCartDVO> getLst() {
		return lst;
	}

	/**
	 * Sets the allcustcartmap.
	 *
	 * @param allcustcartmap the allcustcartmap
	 */
	public void setAllcustcartmap(Map<String, List<EcomCartDVO>> allcustcartmap) {
		this.allcustcartmap = allcustcartmap;
	}

	/**
	 * Sets the dvo.
	 *
	 * @param dvo the new dvo
	 */
	public void setDvo(EcomCartDVO dvo) {
		this.dvo = dvo;
	}

	/**
	 * Sets the lst.
	 *
	 * @param lst the new lst
	 */
	public void setLst(List<EcomCartDVO> lst) {
		this.lst = lst;
	}

	@Override
	public String toString() {
		return "EcomCartDTO [dvo=" + dvo + ", lst=" + lst + ", allcustcartmap=" + allcustcartmap + "]";
	}

}
