/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.shop.ecom.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rtf.common.util.Messages;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GenUtil;
import com.rtf.saas.util.GsonUtil;
import com.rtf.shop.ecom.dto.EcomCartDTO;
import com.rtf.shop.ecom.dvo.EcomCartDVO;
import com.rtf.shop.ecom.service.EcomCustomerCartService;

/**
 * The Class EcomDeleteFromCartServlet.
 */
@WebServlet("/ecomdeletecustcart.json")
public class EcomDeleteFromCartServlet extends RtfSaasBaseServlet {

	/**
	 *
	 */
	private static final long serialVersionUID = 8809542928195170709L;

	/**
	 * Generate Http Response for Client.Response, data is sent in JSON format.
	 *
	 * @param request        the HttpServlet request
	 * @param response       the HttpServlet response
	 * @param rtfSaasBaseDTO RTF Base Data Transfer Object
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("resp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();
	}

	/**
	 * Process request. Delete Products From Customers Cart selected by Customer during Live Game Show
	 *
	 * @param request  the HttpServletRequest
	 * @param response the HttpServletResponse
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String clId = request.getParameter("clId");
		String prodIdStr = request.getParameter("itmsId"); // Master Prod Id ..
		String conId = request.getParameter("conId");
		String cuId = request.getParameter("cuId");
		String mercProdId = request.getParameter("mercProdId");
		//String slerIdstr = request.getParameter("slerId"); // Seller Id

		Integer prodId = null;

		EcomCartDTO dto = new EcomCartDTO();
		dto.setSts(0);
		dto.setClId(clId);
		try {

			if (GenUtil.isNullOrEmpty(clId)) {
				setClientMessage(dto, Messages.INVALID_CLIENT_ID, null);
				generateResponse(request, response, dto);
				return;
			}
			if (GenUtil.isNullOrEmpty(prodIdStr)) {
				setClientMessage(dto, Messages.ECOM_PRODID_MANDATORY, null);
				generateResponse(request, response, dto);
				return;
			}
			
			try {
				prodId = Integer.parseInt(prodIdStr);
			} catch (Exception ex) {
				setClientMessage(dto, Messages.INVALID_PROD_ID, null);
				generateResponse(request, response, dto);
				return;
			}
			
			if (GenUtil.isNullOrEmpty(mercProdId)) {
				setClientMessage(dto, Messages.INVALID_MERC_PROD_ID, null);
				generateResponse(request, response, dto);
				return;
			}
			
			
			if (GenUtil.isNullOrEmpty(conId)) {
				setClientMessage(dto, Messages.MANDATORY_PARAM_CONTEST_ID, null);
				generateResponse(request, response, dto);
				return;
			}
			if (GenUtil.isNullOrEmpty(cuId)) {
				setClientMessage(dto, Messages.MANDATORY_PARAM_CUST_ID, null);
				generateResponse(request, response, dto);
				return;
			}
			
			/*
			 * if (GenUtil.isNullOrEmpty(slerIdstr)) { setClientMessage(dto,
			 * Messages.ECOM_SELLERID_MANDATORY, null); generateResponse(request, response,
			 * dto); return; }
			 */
			/*
			 * try { slerId = Integer.parseInt(slerIdstr); } catch (Exception ex) {
			 * setClientMessage(dto, Messages.ECOM_SELLERID_INVALID, null);
			 * generateResponse(request, response, dto); return; }
			 */

			EcomCartDVO dvo = new EcomCartDVO();
			dvo.setClintId(clId);
			dvo.setConId(conId);
			dvo.setCustId(cuId);			
			dvo.setItmsId(prodId);
			dvo.setMercProdid(mercProdId);
			dto.setDvo(dvo);
			dto = EcomCustomerCartService.removeFromCustomerCart(dto);
			if (dto.getSts() == 0) {
				setClientMessage(dto, null, null);
				generateResponse(request, response, dto);
				return;
			}
			generateResponse(request, response, dto);
			return;
		} catch (Exception e) {
			e.printStackTrace();
			setClientMessage(dto, null, null);
			generateResponse(request, response, dto);
		}
		return;
	}
}