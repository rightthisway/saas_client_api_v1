/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.shop.ecom.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rtf.common.dvo.RewardTypeDVO;
import com.rtf.common.util.CommonCacheUtil;
import com.rtf.common.util.Messages;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.DateFormatUtil;
import com.rtf.saas.util.GenUtil;
import com.rtf.saas.util.GsonUtil;
import com.rtf.shop.ecom.dto.EcomCartDTO;
import com.rtf.shop.ecom.dvo.EcomCartDVO;
import com.rtf.shop.ecom.service.EcomCustomerCartService;

/**
 * The Class EcomListCustomerCartServlet.
 */
@WebServlet("/ecomlistcustcart.json")
public class EcomListCustomerCartServlet extends RtfSaasBaseServlet {

	/**
	 *
	 */
	private static final long serialVersionUID = 1058178961375994796L;

	/**
	 * Generate Http Response for Client.Response, data is sent in JSON format.
	 *
	 * @param request        the HttpServlet request
	 * @param response       the HttpServlet response
	 * @param rtfSaasBaseDTO RTF Base Data Transfer Object
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("shopresp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();
	}

	/**
	 * Process request.List Products from Customer Cart for current Live Contest 
	 *
	 * @param request  the HttpServletRequest
	 * @param response the HttpServletResponse
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String clId = request.getParameter("clId");
		String conId = request.getParameter("conId");
		String cuId = request.getParameter("cuId");

		EcomCartDTO dto = new EcomCartDTO();
		dto.setSts(0);
		dto.setClId(clId);
		try {

			if (GenUtil.isNullOrEmpty(clId)) {
				setClientMessage(dto, Messages.INVALID_CLIENT_ID, null);
				generateResponse(request, response, dto);
				return;
			}
			if (GenUtil.isNullOrEmpty(conId)) {
				setClientMessage(dto, Messages.MANDATORY_PARAM_CONTEST_ID, null);
				generateResponse(request, response, dto);
				return;
			}
			if (GenUtil.isNullOrEmpty(cuId)) {
				setClientMessage(dto, Messages.MANDATORY_PARAM_CUST_ID, null);
				generateResponse(request, response, dto);
				return;
			}

			EcomCartDVO dvo = new EcomCartDVO();
			dvo.setClintId(clId);
			dvo.setConId(conId);
			dvo.setCustId(cuId);
			dto.setDvo(dvo);
			dto = EcomCustomerCartService.listCustomerCartItems(dto);
			if (dto.getSts() == 0) {
				setClientMessage(dto, null, null);
				generateResponse(request, response, dto);
				return;
			}
			if (dto.getLst() != null) {
				for (EcomCartDVO cart : dto.getLst()) {
					if (cart.getPriceType() != null && !cart.getPriceType().isEmpty()) {
						RewardTypeDVO rType = CommonCacheUtil.getRewardTypeFromMap(cart.getClintId(),
								cart.getPriceType());
						if (rType != null && rType.getRwdunimesr().equalsIgnoreCase("CASH")) {
							cart.setPriceStr("$" + DateFormatUtil.getRoundedValueString(cart.getSellPrc()) + " "
									+ rType.getRwdType());
						} else {
							if (rType != null && rType.getRwdType().equalsIgnoreCase("Miles")) {
								cart.setPriceStr(DateFormatUtil.getRoundedValueStringWithComma(cart.getSellPrc()) + " "
										+ rType.getRwdType());
							} else {
								cart.setPriceStr(cart.getSellPrc().intValue() + " " + rType.getRwdType());
							}
						}
					} else {
						cart.setPriceStr("$" + DateFormatUtil.getRoundedValueString(cart.getSellPrc()));
					}
				}
			}

			generateResponse(request, response, dto);
			return;
		} catch (Exception e) {
			e.printStackTrace();
			setClientMessage(dto, null, null);
			generateResponse(request, response, dto);
		}
		return;
	}
}