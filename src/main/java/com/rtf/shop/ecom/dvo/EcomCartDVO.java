/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.shop.ecom.dvo;

import com.rtf.saas.dvo.SaasBaseDVO;

/**
 * The Class EcomCartDVO.
 */
public class EcomCartDVO extends SaasBaseDVO {

	/** The clint id. */
	private String clintId;

	/** The merc id. */
	private Integer mercId;

	/** The cust id. */
	private String custId;
	
	/** The con id. */
	private String conId;

	/** The merc prodid. */
	private String mercProdid;
	
	/** The Product Items Id */
	private Integer itmsId;	

	/** The buy units. */
	private Integer buyUnits;

	/** The buy price. */
	private Double buyPrice;

	/** The status. */
	private String status;

	/** The is placed order. */
	private boolean isPlacedOrder;

	/** The credate. */
	private java.util.Date credate;

	/** The updby. */
	private String updby;

	/** The upddate. */
	private java.util.Date upddate;

	/** The unit type. */
	private String unitType;

	/** The ccy. */
	private String ccy;

	/** The mrkt plce. */
	private String mrktPlce;

	/** The merc selr site. */
	private String mercSelrSite;

	/** The prod name. */
	private String prodName;

	/** The prod id selrsite. */
	private String prodIdSelrsite;

	

	/** The prdinv id. */
	private Integer prdinvId;

	
	/** The reg prc. */
	private Double regPrc;

	/** The sell prc. */
	private Double sellPrc;

	/** The price type. */
	private String priceType;
	
	/** The product price details. */
	private String prodPriceDtl;
	
	
	
	
	/** The prod img url. */
	private String prodImgUrl;
	/** The prod desc. */
	private String prodDesc;
	
	private String pbrand;
	private String prodLongDesc;
	
	
	
	
	
	

	/** The price str. */
	private String priceStr;
	
	
	

	/**
	 * Gets the buy price.
	 *
	 * @return the buy price
	 */
	public Double getBuyPrice() {
		return buyPrice;
	}

	/**
	 * Gets the buy units.
	 *
	 * @return the buy units
	 */
	public Integer getBuyUnits() {
		return buyUnits;
	}

	/**
	 * Gets the ccy.
	 *
	 * @return the ccy
	 */
	public String getCcy() {
		return ccy;
	}

	/**
	 * Gets the clint id.
	 *
	 * @return the clint id
	 */
	public String getClintId() {
		return clintId;
	}

	/**
	 * Gets the con id.
	 *
	 * @return the con id
	 */
	public String getConId() {
		return conId;
	}

	/**
	 * Gets the credate.
	 *
	 * @return the credate
	 */
	public java.util.Date getCredate() {
		return credate;
	}

	/**
	 * Gets the cust id.
	 *
	 * @return the cust id
	 */
	public String getCustId() {
		return custId;
	}

	/**
	 * Gets the merc id.
	 *
	 * @return the merc id
	 */
	public Integer getMercId() {
		return mercId;
	}

	/**
	 * Gets the merc prodid.
	 *
	 * @return the merc prodid
	 */
	public String getMercProdid() {
		return mercProdid;
	}

	/**
	 * Gets the merc selr site.
	 *
	 * @return the merc selr site
	 */
	public String getMercSelrSite() {
		return mercSelrSite;
	}

	/**
	 * Gets the mrkt plce.
	 *
	 * @return the mrkt plce
	 */
	public String getMrktPlce() {
		return mrktPlce;
	}

	/**
	 * Gets the prdinv id.
	 *
	 * @return the prdinv id
	 */
	public Integer getPrdinvId() {
		return prdinvId;
	}

	/**
	 * Gets the price str.
	 *
	 * @return the price str
	 */
	public String getPriceStr() {
		return priceStr;
	}

	/**
	 * Gets the price type.
	 *
	 * @return the price type
	 */
	public String getPriceType() {
		return priceType;
	}

	/**
	 * Gets the prod desc.
	 *
	 * @return the prod desc
	 */
	public String getProdDesc() {
		return prodDesc;
	}

	/**
	 * Gets the prod id selrsite.
	 *
	 * @return the prod id selrsite
	 */
	public String getProdIdSelrsite() {
		return prodIdSelrsite;
	}

	/**
	 * Gets the prod img url.
	 *
	 * @return the prod img url
	 */
	public String getProdImgUrl() {
		return prodImgUrl;
	}

	/**
	 * Gets the prod name.
	 *
	 * @return the prod name
	 */
	public String getProdName() {
		return prodName;
	}

	/**
	 * Gets the reg prc.
	 *
	 * @return the reg prc
	 */
	public Double getRegPrc() {
		return regPrc;
	}

	/**
	 * Gets the sell prc.
	 *
	 * @return the sell prc
	 */
	public Double getSellPrc() {
		return sellPrc;
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Gets the unit type.
	 *
	 * @return the unit type
	 */
	public String getUnitType() {
		return unitType;
	}

	/**
	 * Gets the updby.
	 *
	 * @return the updby
	 */
	public String getUpdby() {
		return updby;
	}

	/**
	 * Gets the upddate.
	 *
	 * @return the upddate
	 */
	public java.util.Date getUpddate() {
		return upddate;
	}

	/**
	 * Checks if is placed order.
	 *
	 * @return true, if is placed order
	 */
	public boolean isPlacedOrder() {
		return isPlacedOrder;
	}

	/**
	 * Sets the buy price.
	 *
	 * @param buyPrice the new buy price
	 */
	public void setBuyPrice(Double buyPrice) {
		this.buyPrice = buyPrice;
	}

	/**
	 * Sets the buy units.
	 *
	 * @param buyUnits the new buy units
	 */
	public void setBuyUnits(Integer buyUnits) {
		this.buyUnits = buyUnits;
	}

	/**
	 * Sets the ccy.
	 *
	 * @param ccy the new ccy
	 */
	public void setCcy(String ccy) {
		this.ccy = ccy;
	}

	/**
	 * Sets the clint id.
	 *
	 * @param clintId the new clint id
	 */
	public void setClintId(String clintId) {
		this.clintId = clintId;
	}

	/**
	 * Sets the con id.
	 *
	 * @param conId the new con id
	 */
	public void setConId(String conId) {
		this.conId = conId;
	}

	/**
	 * Sets the credate.
	 *
	 * @param credate the new credate
	 */
	public void setCredate(java.util.Date credate) {
		this.credate = credate;
	}

	/**
	 * Sets the cust id.
	 *
	 * @param custId the new cust id
	 */
	public void setCustId(String custId) {
		this.custId = custId;
	}

	/**
	 * Sets the merc id.
	 *
	 * @param mercId the new merc id
	 */
	public void setMercId(Integer mercId) {
		this.mercId = mercId;
	}

	/**
	 * Sets the merc prodid.
	 *
	 * @param mercProdid the new merc prodid
	 */
	public void setMercProdid(String mercProdid) {
		this.mercProdid = mercProdid;
	}

	/**
	 * Sets the merc selr site.
	 *
	 * @param mercSelrSite the new merc selr site
	 */
	public void setMercSelrSite(String mercSelrSite) {
		this.mercSelrSite = mercSelrSite;
	}

	/**
	 * Sets the mrkt plce.
	 *
	 * @param mrktPlce the new mrkt plce
	 */
	public void setMrktPlce(String mrktPlce) {
		this.mrktPlce = mrktPlce;
	}

	/**
	 * Sets the placed order.
	 *
	 * @param isPlacedOrder the new placed order
	 */
	public void setPlacedOrder(boolean isPlacedOrder) {
		this.isPlacedOrder = isPlacedOrder;
	}

	/**
	 * Sets the prdinv id.
	 *
	 * @param prdinvId the new prdinv id
	 */
	public void setPrdinvId(Integer prdinvId) {
		this.prdinvId = prdinvId;
	}

	/**
	 * Sets the price str.
	 *
	 * @param priceStr the new price str
	 */
	public void setPriceStr(String priceStr) {
		this.priceStr = priceStr;
	}

	/**
	 * Sets the price type.
	 *
	 * @param priceType the new price type
	 */
	public void setPriceType(String priceType) {
		this.priceType = priceType;
	}

	/**
	 * Sets the prod desc.
	 *
	 * @param prodDesc the new prod desc
	 */
	public void setProdDesc(String prodDesc) {
		this.prodDesc = prodDesc;
	}

	/**
	 * Sets the prod id selrsite.
	 *
	 * @param prodIdSelrsite the new prod id selrsite
	 */
	public void setProdIdSelrsite(String prodIdSelrsite) {
		this.prodIdSelrsite = prodIdSelrsite;
	}

	/**
	 * Sets the prod img url.
	 *
	 * @param prodImgUrl the new prod img url
	 */
	public void setProdImgUrl(String prodImgUrl) {
		this.prodImgUrl = prodImgUrl;
	}

	/**
	 * Sets the prod name.
	 *
	 * @param prodName the new prod name
	 */
	public void setProdName(String prodName) {
		this.prodName = prodName;
	}

	/**
	 * Sets the reg prc.
	 *
	 * @param regPrc the new reg prc
	 */
	public void setRegPrc(Double regPrc) {
		this.regPrc = regPrc;
	}

	/**
	 * Sets the sell prc.
	 *
	 * @param sellPrc the new sell prc
	 */
	public void setSellPrc(Double sellPrc) {
		this.sellPrc = sellPrc;
	}

	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * Sets the unit type.
	 *
	 * @param unitType the new unit type
	 */
	public void setUnitType(String unitType) {
		this.unitType = unitType;
	}

	/**
	 * Sets the updby.
	 *
	 * @param updby the new updby
	 */
	public void setUpdby(String updby) {
		this.updby = updby;
	}

	/**
	 * Sets the upddate.
	 *
	 * @param upddate the new upddate
	 */
	public void setUpddate(java.util.Date upddate) {
		this.upddate = upddate;
	}
	
	
	/**
	 * Gets product price details.
	 *
	 * @return prodPriceDtl
	 */
	public String getProdPriceDtl() {
		return prodPriceDtl;
	}

	/**
	 * Sets product price details.
	 *
	 * @param prodPriceDtl
	 */
	public void setProdPriceDtl(String prodPriceDtl) {
		this.prodPriceDtl = prodPriceDtl;
	}

	@Override
	public String toString() {
		return "EcomCartDVO [clintId=" + clintId + ", mercId=" + mercId + ", custId=" + custId + ", conId=" + conId
				+ ", mercProdid=" + mercProdid + ", itmsId=" + itmsId + ", buyUnits=" + buyUnits + ", buyPrice="
				+ buyPrice + ", status=" + status + ", isPlacedOrder=" + isPlacedOrder + ", credate=" + credate
				+ ", updby=" + updby + ", upddate=" + upddate + ", unitType=" + unitType + ", ccy=" + ccy
				+ ", mrktPlce=" + mrktPlce + ", mercSelrSite=" + mercSelrSite + ", prodName=" + prodName
				+ ", prodIdSelrsite=" + prodIdSelrsite + ", prdinvId=" + prdinvId + ", regPrc=" + regPrc + ", sellPrc="
				+ sellPrc + ", priceType=" + priceType + ", prodPriceDtl=" + prodPriceDtl + ", prodImgUrl=" + prodImgUrl
				+ ", prodDesc=" + prodDesc + ", pbrand=" + pbrand + ", prodLongDesc=" + prodLongDesc + ", priceStr="
				+ priceStr + "]";
	}

	public Integer getItmsId() {
		return itmsId;
	}

	public void setItmsId(Integer itmsId) {
		this.itmsId = itmsId;
	}

	public String getPbrand() {
		return pbrand;
	}

	public void setPbrand(String pbrand) {
		this.pbrand = pbrand;
	}

	public String getProdLongDesc() {
		return prodLongDesc;
	}

	public void setProdLongDesc(String prodLongDesc) {
		this.prodLongDesc = prodLongDesc;
	}

}
