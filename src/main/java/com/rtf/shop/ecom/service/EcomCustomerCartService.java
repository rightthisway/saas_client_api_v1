/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.shop.ecom.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.rtf.common.util.CommonCacheUtil;
import com.rtf.common.util.StatusEnums;
import com.rtf.saas.exception.SaasProcessException;
import com.rtf.saas.util.GenUtil;
import com.rtf.shop.ecom.dao.EcomCustomerCartCassDAO;
import com.rtf.shop.ecom.dao.EcomCustomerCartDAO;
import com.rtf.shop.ecom.dao.ProductItemMasterCassDAO;
import com.rtf.shop.ecom.dto.EcomCartDTO;
import com.rtf.shop.ecom.dvo.EcomCartDVO;
import com.rtf.shop.ecom.dvo.GenericResp;
import com.rtf.shop.ecom.util.GsonCustomConfig;
import com.rtf.shop.ecom.util.HTTPUtil;

/**
 * The Class EcomCustomerCartService.
 */
public class EcomCustomerCartService {
	/**
	 * Adds the to customer cart.
	 *
	 * @param dto the EcomCartDTO
	 * @return the EcomCartDTO DTO
	 * @throws Exception the exception
	 */
	public static EcomCartDTO addToCustomerCart(EcomCartDTO dto) throws Exception {
		Integer sts = 0;
		dto.setSts(sts);		
		try {			
			EcomCartDVO dvo = dto.getDvo();	
			System.out.println(" CART  BEFORE MERGE OF PROD MASTER DVO " + dvo);
			dvo = mergerProdDetailsFromCassProdMaster(dvo);
			
			System.out.println(" CART  AFTER MERGE OF PROD MASTER DVO " + dvo);
			if(dvo != null) {
				try {
				Integer updcnt = EcomCustomerCartCassDAO.addToCustomerCart(dvo);
				System.out.println(" CART  INSERTED  status [  " + updcnt  +  " ]  for  " + dvo);
				addToRTFCart(dvo);
				dto.setSts(1);		
				} catch (Exception ex) {
					ex.printStackTrace();
					throw new SaasProcessException("Error Processing Cart Addition", ex);
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new SaasProcessException("Error Processing Cart Addition", ex);
		}
		return dto;
	}

	private static EcomCartDVO mergerProdDetailsFromCassProdMaster(EcomCartDVO dvo) throws Exception {			
		dvo = ProductItemMasterCassDAO.getProductByItemsId(dvo.getClintId(), dvo.getItmsId(), dvo);		
		return dvo;
	}

	/**
	 * Adds the product to RTF cart.
	 *
	 * @param dvo the EcomCartDVO
	 * @return the affected record status integer
	 * @throws Exception the exception
	 */
	private static Integer addToRTFCart(EcomCartDVO dvo) throws Exception {

		System.out.println(" Add to Cart DVO " + dvo);
		String data = null;
		String RTFAPI_BASEURL = CommonCacheUtil.clientcustommap.get("rtfcartbaseapiurl");
		if (GenUtil.isEmptyString(RTFAPI_BASEURL)) {
			throw new SaasProcessException("RTF BASE API URL NOT INITIALIZED");
		}
		String RTFAPI_API_URL = RTFAPI_BASEURL + "SaveCustCart";
		GenericResp dto = null;
		try {

			Map<String, String> dataMap = new HashMap<String, String>();
			dataMap.put("custId", dvo.getCustId());
			dataMap.put("prodId", dvo.getProdIdSelrsite());
			dataMap.put("invId", "-1");
			dataMap.put("qty", "1");
			dataMap.put("price", String.valueOf(dvo.getSellPrc()));
			dataMap.put("configId", "configIdcarttransfer");
			data = HTTPUtil.execute(dataMap, RTFAPI_API_URL);
			if (data == null)
				return 0;
			Gson gson = GsonCustomConfig.getGsonBuilder();
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			dto = gson.fromJson(((JsonObject) jsonObject.get("genericResp")), GenericResp.class);

			if (dto == null)
				return 0;
			return dto.getStatus();
		} catch (Exception e) {
			throw new SaasProcessException("Error Processing Add to RTF Cart", e);

		}
	}

	/**
	 * Fetch all products in customers cart.
	 *
	 * @param dto the EcomCartDTO
	 * @return the EcomCartDTO cart DTO
	 * @throws Exception the exception
	 *//*
		 * public static EcomCartDTO fetchallCustomersCart(EcomCartDTO dto) throws
		 * Exception { Integer sts = 0; dto.setSts(sts); try { Map<String,
		 * List<EcomCartDVO>> allcartMap = new HashMap<String, List<EcomCartDVO>>();
		 * Map<String, String> allCustMap =
		 * EcomCustomerCartDAO.fetchAllCustomerInCart(); EcomCartDVO dvo = dto.getDvo();
		 * for (Map.Entry<String, String> entry : allCustMap.entrySet()) {
		 * dvo.setCustId(entry.getKey()); dvo.setClintId(entry.getValue());
		 * List<EcomCartDVO> lst = EcomCustomerCartDAO.fetchCustomerCart(dvo);
		 * allcartMap.put(dvo.getCustId(), lst); }
		 * 
		 * dto.setAllcustcartmap(allcartMap); dto.setSts(1); } catch (Exception ex) {
		 * ex.printStackTrace(); throw new
		 * SaasProcessException("Error Fetchibg Cart for Customer", ex); } return dto; }
		 */

	/**
	 * Fetch customer purchased products.
	 *
	 * @param cuId   the Customer id
	 * @param clId   the Client ID
	 * @param pageNo the page no
	 * @return the list
	 * @throws Exception the exception
	 */
	public static List<EcomCartDVO> fetchCustomerPurchasedProducts(String cuId, String clId, String pageNo)
			throws Exception {
		List<EcomCartDVO> list = null;
		try {
			list = EcomCustomerCartDAO.fetchCustomerPurchasedProducts(cuId, clId, pageNo);
		} catch (Exception e) {
			e.printStackTrace();
			throw new SaasProcessException("Error Processing Customer Purchased Products", e);
		}
		return list;
	}

	/**
	 * Gets the all customer carts.
	 *
	 * @param dto the EcomCartDTO DTO
	 * @return the all customer carts
	 * @throws Exception the exception
	 */
	/*
	 * public static EcomCartDTO getAllCustomerCarts(EcomCartDTO dto) throws
	 * Exception { Integer sts = 0; dto.setSts(sts); try { EcomCartDVO dvo =
	 * dto.getDvo(); List<EcomCartDVO> lst =
	 * EcomCustomerCartDAO.getAllCustomerCarts(dvo); dto.setLst(lst); dto.setSts(1);
	 * } catch (Exception ex) { ex.printStackTrace(); throw new
	 * SaasProcessException("Error fetching Customer Cart ", ex); } return dto; }
	 */

	/**
	 * List customer cart items.
	 *
	 * @param dto the EcomCartDTO
	 * @return the EcomCartDTO DTO
	 * @throws Exception the exception
	 */
	public static EcomCartDTO listCustomerCartItems(EcomCartDTO dto) throws Exception {
		Integer sts = 0;
		dto.setSts(sts);
		try {
			EcomCartDVO dvo = dto.getDvo();
			List<EcomCartDVO> lst = EcomCustomerCartCassDAO.fetchCustomerCart(dvo);
			dto.setLst(lst);
			dto.setSts(1);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new SaasProcessException("Error Listing  Cart Items", ex);
		}
		return dto;
	}

	/**
	 * Removes all Products from customer cart.
	 *
	 * @param dto the EcomCartDTO DTO
	 * @return the EcomCartDTO DTO
	 * @throws Exception the exception
	 */
	/*
	 * public static EcomCartDTO removeFromAllFromCustomerCart(EcomCartDTO dto)
	 * throws Exception { Integer sts = 0; dto.setSts(sts); try { List<EcomCartDVO>
	 * lst = dto.getLst(); EcomCustomerCartDAO.updateCartMultiItemStatus(lst,
	 * StatusEnums.DELETED.name()); dto.setSts(1); } catch (Exception ex) {
	 * ex.printStackTrace(); throw new
	 * SaasProcessException("Error Cart Removal for All Items", ex); } return dto; }
	 */
	/**
	 * Removes the Product from customer cart.
	 *
	 * @param dto the EcomCartDTO
	 * @return the EcomCartDTO DTO
	 * @throws Exception the exception
	 */
	public static EcomCartDTO removeFromCustomerCart(EcomCartDTO dto) throws Exception {
	
		
		Integer sts = 0;
		dto.setSts(sts);
		EcomCartDVO dvo = dto.getDvo();
		System.out.println(" REMOVE FROM Cart DVO " + dvo);
		try {
			EcomCustomerCartCassDAO.deleteItemInCustomerCart(dvo);
			dto.setSts(1);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new SaasProcessException("Error Processing Cart Removal", ex);
		}

		try {
			removeFromRTFCart(dvo);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new SaasProcessException("Error Processing Cart Removal", ex);
		}
		return dto;

	}

	/**
	 * Removes the from RTF cart.
	 *
	 * @param dvo the EcomCartDVO
	 * @return the integer
	 * @throws Exception the exception
	 */
	private static Integer removeFromRTFCart(EcomCartDVO dvo) throws Exception {

		String data = null;
		String RTFAPI_BASEURL = CommonCacheUtil.clientcustommap.get("rtfcartbaseapiurl");
		if (GenUtil.isEmptyString(RTFAPI_BASEURL)) {
			throw new SaasProcessException("RTF BASE API URL NOT INITIALIZED ");
		}
		String RTFAPI_API_URL = RTFAPI_BASEURL + "deleteCart";
		GenericResp dto = null;
		try {

			Map<String, String> dataMap = new HashMap<String, String>();
			dataMap.put("custId", dvo.getCustId());
			dataMap.put("prodId", dvo.getMercProdid());
			data = HTTPUtil.execute(dataMap, RTFAPI_API_URL);
			if (data == null)
				return 0;
			Gson gson = GsonCustomConfig.getGsonBuilder();
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			dto = gson.fromJson(((JsonObject) jsonObject.get("genericResp")), GenericResp.class);

			if (dto == null)
				return 0;
			return dto.getStatus();
		} catch (Exception e) {
			e.printStackTrace();
			throw new SaasProcessException("Error Processing Remove From RTF Cart", e);
		}
	}

	/**
	 * Update cart as purchased.
	 *
	 * @param ids  the product ids
	 * @param cuId the Customer id
	 * @param clId the Client ID
	 * @return the boolean
	 * @throws Exception the exception
	 */
	/*
	 * public static Boolean updateCartAsPurchased(List<Integer> ids, String cuId,
	 * String clId) throws Exception { Integer isUpdated = 0; try { isUpdated =
	 * EcomCustomerCartDAO.updateCartAsPurchased(ids, cuId, clId); if (isUpdated >
	 * 0) { return true; } } catch (Exception e) { e.printStackTrace(); throw new
	 * SaasProcessException("Error Updating Purchased Item in Cart", e); } return
	 * false; }
	 */

	/**
	 * Update item master cart.
	 *
	 * @param clId    the Client ID
	 * @param cuId    the Customer id
	 * @param prodIds the product ids
	 * @param action  the action
	 * @param cau     The Login User Id
	 * @return the boolean
	 * @throws Exception the exception
	 */
	/*
	 * public static Boolean updateItemMasterCart(String clId, String cuId,
	 * List<Integer> prodIds, String action, String cau) throws Exception { Integer
	 * updRow = 0; try { for (Integer prodId : prodIds) { ProductsDVO pdvo =
	 * MerchantProductsDAO.getProductById(clId, prodId); if (pdvo == null) {
	 * continue; } if (action.equalsIgnoreCase("PURCHASED")) { updRow =
	 * EcomCustomerCartDAO.updateItemMasterCart(clId, pdvo.getMercId(), prodId,
	 * cuId, StatusEnums.PURCHASED.name(), cau); } else if
	 * (action.equalsIgnoreCase("DELETED")) { updRow =
	 * EcomCustomerCartDAO.deleteItemFromMasterCart(clId, pdvo.getMercId(), prodId,
	 * cuId); } } if (updRow > 0) { return true; } } catch (Exception ex) {
	 * ex.printStackTrace(); throw new
	 * SaasProcessException("Error Updating cart Item", ex); } return false;
	 * 
	 * }
	 */
	
	
	public static void main(String[] args) throws Exception  {
		EcomCartDTO dto = new EcomCartDTO();
		
		EcomCartDVO dvo = new EcomCartDVO();
		dvo.setClintId("AMIT202002CID");
		dvo.setConId("TESTCONTEST123");
		dvo.setCustId("TESTLLOYD");
		//dvo.setMercId(slerId);
		dvo.setItmsId(1004);
		dvo.setBuyUnits(1);
		//dvo.setBuyPrice(buyprice);
		dto.setDvo(dvo);
		//addToCustomerCart( dto) ;
		dto = listCustomerCartItems(dto);		
		System.out.print(dto);
		
	}

}
