/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.shop.ecom.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.Socket;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLEngine;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509ExtendedTrustManager;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;

/**
 * The Class HTTPUtil.
 */
public class HTTPUtil {

	/**
	 * Execute.
	 *
	 * @param dataMap the data map
	 * @param url     the url
	 * @return the string
	 * @throws Exception the exception
	 */
	public static String execute(Map<String, String> dataMap, String url) throws Exception {
		try {
			/*
			 * fix for Exception in thread "main" javax.net.ssl.SSLHandshakeException:
			 * sun.security.validator.ValidatorException: PKIX path building failed:
			 * sun.security.provider.certpath.SunCertPathBuilderException: unable to find
			 * valid certification path to requested target
			 */
			TrustManager[] trustAllCerts = new TrustManager[] { new X509ExtendedTrustManager() {
				@Override
				public void checkClientTrusted(X509Certificate[] certs, String authType) {
				}

				@Override
				public void checkClientTrusted(X509Certificate[] xcs, String string, Socket socket)
						throws CertificateException {

				}

				@Override
				public void checkClientTrusted(X509Certificate[] xcs, String string, SSLEngine ssle)
						throws CertificateException {

				}

				@Override
				public void checkServerTrusted(X509Certificate[] certs, String authType) {
				}

				@Override
				public void checkServerTrusted(X509Certificate[] xcs, String string, Socket socket)
						throws CertificateException {

				}

				@Override
				public void checkServerTrusted(X509Certificate[] xcs, String string, SSLEngine ssle)
						throws CertificateException {

				}

				@Override
				public X509Certificate[] getAcceptedIssuers() {
					return null;
				}

			} };

			SSLContext context = SSLContext.getInstance("TLSv1.2");
			context.init(null, trustAllCerts, new java.security.SecureRandom());
			int timeout = 1;
			RequestConfig config = RequestConfig.custom().setConnectTimeout(timeout * 2000)
					.setConnectionRequestTimeout(timeout * 2000).setSocketTimeout(timeout * 1000).build();
			HttpClient httpClient = HttpClients.custom().setSslcontext(context).setDefaultRequestConfig(config).build();

			HttpPost post = new HttpPost(url);
			post.setHeader("Accept", "application/json");

			List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();

			for (Map.Entry<String, String> entry : dataMap.entrySet()) {
				urlParameters.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
			}

			post.setEntity(new UrlEncodedFormEntity(urlParameters, "UTF-8"));

			HttpResponse response = httpClient.execute(post);

			BufferedReader responseData = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

			String result = org.apache.commons.io.IOUtils.toString(responseData);
			return result;

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
