/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.shop.ecom.util;

import java.util.Date;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * The Class GsonCustomConfig.
 */
public class GsonCustomConfig {

	/**
	 * Gets the gson builder.
	 *
	 * @return the gson builder
	 */
	public static Gson getGsonBuilder() {
		Gson gson = new GsonBuilder().registerTypeAdapter(Date.class, new GsonCustomDateSerializer()).create();
		return gson;
	}
}
