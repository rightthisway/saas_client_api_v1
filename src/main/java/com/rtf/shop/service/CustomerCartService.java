/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.shop.service;

import java.util.List;

import com.rtf.shop.dvo.CustomerCartDVO;
import com.rtf.shop.sql.dao.CustomerCartDAO;

/**
 * The Class CustomerCartService.
 */
public class CustomerCartService {

	/**
	 * Fetch customer purchased products.
	 *
	 * @param cuId    the Contest ID 
	 * @param clId    the Client ID
	 * @param pageNo the page no
	 * @return the list
	 */
	public static List<CustomerCartDVO> fetchCustomerPurchasedProducts(String cuId, String clId, String pageNo) {
		List<CustomerCartDVO> list = null;
		try {
			list = CustomerCartDAO.fetchCustomerPurchasedProducts(cuId, clId, pageNo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	/**
	 * Update cart as purchased.
	 *
	 * @param ids  the Product ID
	 * @param cuId  the Contest ID 
	 * @param clId  the Client ID
	 * @return the boolean
	 */
	public static Boolean updateCartAsPurchased(List<Integer> ids, String cuId, String clId) {
		Integer isUpdated = 0;
		try {
			isUpdated = CustomerCartDAO.updateCartAsPurchased(ids, cuId, clId);
			if (isUpdated > 0) {
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}


}
