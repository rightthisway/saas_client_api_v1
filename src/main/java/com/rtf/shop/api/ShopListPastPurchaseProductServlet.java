/*
* ***************************************************************************************************
* Copyright (c) 2021 RTFLIVE. All rights reserved.
* SaaS Product Line
* Release : 1.00
* Author :  RTFLIVE
* ***************************************************************************************************
*/
package com.rtf.shop.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rtf.common.util.Messages;
import com.rtf.saas.arch.RtfSaasBaseDTO;
import com.rtf.saas.arch.RtfSaasBaseServlet;
import com.rtf.saas.util.GenUtil;
import com.rtf.saas.util.GsonUtil;
import com.rtf.shop.dto.CustomerCartDTO;
import com.rtf.shop.dvo.CustomerCartDVO;
import com.rtf.shop.service.CustomerCartService;

/**
 * The Class ShopListPastPurchaseProductServlet.
 */
@WebServlet("/shopplistpastpurchprod.json")
public class ShopListPastPurchaseProductServlet extends RtfSaasBaseServlet {

	/**
	 *
	 */
	private static final long serialVersionUID = -6439303418525980271L;

	/**
	 *  Generate Http Response for Client.Response, data is sent in JSON format.
	 *
* @param request        the  HttpServlet request
* @param response        the  HttpServlet response
	 * @param rtfSaasBaseDTO RTF Base Data Transfer Object
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void generateResponse(HttpServletRequest request, HttpServletResponse response,
			RtfSaasBaseDTO rtfSaasBaseDTO) throws ServletException, IOException {
		Map<String, RtfSaasBaseDTO> map = new HashMap<String, RtfSaasBaseDTO>();
		map.put("shopresp", rtfSaasBaseDTO);
		String ottrespStr = GsonUtil.getJasksonObjMapper().writeValueAsString(map);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		out.print(ottrespStr);
		out.flush();
	}

	/**
	 * Process request.
	 *
	 * @param request  the HttpServletRequest
	 * @param response the HttpServletResponse
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String clId = request.getParameter("clId");
		String userId = request.getParameter("cau");
		String cuId = request.getParameter("cuId");
		String pgNo = request.getParameter("pgNo");

		CustomerCartDTO dto = new CustomerCartDTO();
		dto.setSts(0);
		dto.setClId(clId);

		try {
			if (pgNo == null || pgNo.isEmpty()) {
				pgNo = "1";
			}
			if (GenUtil.isNullOrEmpty(clId)) {
				setClientMessage(dto, Messages.INVALID_CLIENT_ID, null);
				generateResponse(request, response, dto);
				return;
			}
			if (GenUtil.isNullOrEmpty(userId)) {
				setClientMessage(dto, "userId is mandatory", null);
				generateResponse(request, response, dto);
				return;
			}
			if (GenUtil.isNullOrEmpty(cuId)) {
				setClientMessage(dto, "CustId is mandatory", null);
				generateResponse(request, response, dto);
				return;
			}

			List<CustomerCartDVO> products = CustomerCartService.fetchCustomerPurchasedProducts(cuId, clId, pgNo);
			dto.setLst(products);
			dto.setSts(1);
			generateResponse(request, response, dto);
			return;
		} catch (Exception e) {
			e.printStackTrace();
			setClientMessage(dto, null, null);
			generateResponse(request, response, dto);
		}
		return;
	}
}
